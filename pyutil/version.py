# Copyright 2016 Dietrich Epp.
# This file is part of the Moria Gamedev Repository.  This file is licensed
# under the terms of the MIT license.  For more information, see LICENSE.txt.

class Version(object):
    """An simple version number with two components.

    Attributes:
    major -- The major version number.
    minor -- The minor version number.
    """
    __slots__ = ['major', 'minor']

    def __init__(self, major, minor):
        self.major = major
        self.minor = minor

    def __eq__(self, other):
        if isinstance(other, Version):
            return self.major == other.major and self.minor == other.minor
        return NotImplemented

    def __lt__(self, other):
        if not isinstance(other, Version):
            raise TypeError('Not a version.')
        return (self.major, self.minor) < (other.major, other.minor)
    def __le__(self, other):
        if not isinstance(other, Version):
            raise TypeError('Not a version.')
        return (self.major, self.minor) <= (other.major, other.minor)
    def __gt__(self, other):
        if not isinstance(other, Version):
            raise TypeError('Not a version.')
        return (self.major, self.minor) > (other.major, other.minor)
    def __ge__(self, other):
        if not isinstance(other, Version):
            raise TypeError('Not a version.')
        return (self.major, self.minor) >= (other.major, other.minor)

    def __hash__(self):
        return hash((self.major, self.minor))

    def __str__(self):
        return '{0.major}.{0.minor}'.format(self)

    def __repr__(self):
        return 'Version({0.major!r}, {0.minor!r})'.format(self)

    @classmethod
    def parse(class_, value):
        """Parse an OpenGL version number."""
        i = value.index('.')
        major = int(value[:i])
        minor = int(value[i+1:])
        if major <= 0 or minor < 0:
            raise ValueError('Invalid version.')
        return class_(major, minor)
