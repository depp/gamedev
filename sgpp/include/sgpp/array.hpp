/* Copyright 2013-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SGPP_ARRAY_HPP
#define SGPP_ARRAY_HPP

#include <limits>
#include <new>
#include <cstdlib>
#include <util>

#include "sg/util.h"
#include "sggl/common.h"
#include "log.hpp"

namespace SG {

// OpenGL attribute array class.
template<class T>
class Array {
private:
    friend class Buffer;

    T *m_data;
    int m_count;
    int m_alloc;

public:
    Array();
    Array(const Array &other) = delete;
    Array(Array &&other);
    ~Array();
    Array &operator=(const Array &other) = delete;
    Array &operator=(Array &&other);

    /// Get the array data.
    const T *data() const { return m_data; }
    /// Get the number of elements in the array.
    int size() const { return m_count; }
    /// Determine whether the array is empty.
    bool empty() const { return m_count == 0; }

    /// Set the number of elements in the array to zero.
    void clear();
    /// Reserve space for the given total number of elements.
    void reserve(std::size_t total);
    /// Insert the given number of elements and return a pointer to the first.
    T *insert(std::size_t count);
    /// Set the end of the array.  Must be within the array, or at the end.
    void set_end(T* end);
    /// Upload the array to an OpenGL buffer.
    void upload(GLenum usage);
};

template<class T>
inline Array<T>::Array()
    : m_data(nullptr), m_count(0), m_alloc(0) { }

template<class T>
inline Array<T>::Array(Array<T> &&other)
    : m_data(other.m_data), m_count(other.m_count), m_alloc(other.m_alloc) {
    other.m_data = nullptr;
    other.m_count = 0;
    other.m_alloc = 0;
}

template<class T>
inline Array<T>::~Array() {
    std::free(m_data);
}

template<class T>
Array<T> &Array<T>::operator=(Array<T> &&other) {
    if (this != &other) {
        std::swap(m_data, other.m_data);
        std::swap(m_count, other.m_count);
        std::swap(m_alloc, other.m_alloc);
    }
    return *this;
}

template<class T>
void Array<T>::clear() {
    m_count = 0;
}

template<class T>
void Array<T>::reserve(std::size_t total) {
    if (total < (std::size_t) m_alloc) {
        return;
    }
    std::size_t nalloc = (std::size_t) m_alloc * 2;
    if (total > nalloc) {
        nalloc = total;
    }
    if (!nalloc ||
        total >= (std::size_t) std::numeric_limits<int>::max() ||
        total >= std::numeric_limits<std::size_t>::max() / sizeof(T)) {
        Log::abort("out of memory");
    }
    T *newdata = static_cast<T *>(std::realloc(m_data, sizeof(T) * nalloc));
    if (!newdata) {
        Log::abort("out of memory");
    }
    m_data = newdata;
    m_alloc = (int) nalloc;
}

template<class T>
T *Array<T>::insert(std::size_t count) {
    if (count > m_alloc - m_count) {
        if (count > std::numeric_limits<std::size_t>::max() - m_count) {
            Log::abort("out of memory");
        }
        reserve(count + m_count);
    }
    int pos = m_count;
    m_count += count;
    return m_data + pos;
}

template<class T>
void Array<T>::set_end(T* end) {
    m_count = static_cast<int>(end - m_data);
}

template<class T>
void Array<T>::upload(GLenum usage) {
    glBufferData(GL_ARRAY_BUFFER, m_count * sizeof(T), m_data, usage);
}

}
#endif
