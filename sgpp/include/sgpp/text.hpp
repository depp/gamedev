/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SGPP_TEXT_HPP
#define SGPP_TEXT_HPP

#include "box.hpp"
#include "vec.hpp"
#include "vec.hpp"
#include "sggl/common.h"

#include <string>

namespace SG {
namespace Text {
class Core;
class Batch;
class Layout;
class LayoutBuilder;

////////////////////////////////////////////////////////////////////////

/// Font styles.
enum class FontStyle {
    NORMAL,
    ITALIC,
    OBLIQUE
};

/// A font descriptor: attributes used to specify a font.
struct FontDesc {
    /// The font family name.
    std::string family;

    /// The font size, in pixels.
    float size;

    /// The font weight, 100-900 (in multiples of 100).
    int weight;

    /// The font style.
    FontStyle style;

    FontDesc();
};

bool operator==(const FontDesc &x, const FontDesc &y);
bool operator!=(const FontDesc &x, const FontDesc &y);

/// Text alignment options.
enum class TextAlign {
    Left,
    Right,
    Center,
    Justify
};

/// Vertical box alignment options.
enum class VAlign {
    BASELINE,
    TOP,
    CENTER,
    BOTTOM
};

/// Horizontal box alignment options.
enum class HAlign {
    LEFT,
    CENTER,
    RIGHT
};

////////////////////////////////////////////////////////////////////////

/// Data used to draw a set of fonts.
struct FontData {
    /// The texture where glyphs are stored.  The texture object
    /// belongs to the system and should not be modified.
    GLuint texture;

    /// The size of the texture.
    IVec2 texsize;

    /// Conversion factor from texel coordinates to texture
    /// coordinates (equal to 1.0/texsize).
    Vec2 texscale;
};

////////////////////////////////////////////////////////////////////////

/// Text layout vertex array data.
struct ArrayData {
    /// The buffer object where vertex data is stored.
    GLuint buffer;

    /// The offset at which vertex data is stored.
    std::size_t offset;
};

////////////////////////////////////////////////////////////////////////

/// Data needed to render a layout.
struct LayoutData {
    /// The first vertex in the layout.
    GLint first;

    /// The number of vertexes in the layout.
    GLint count;
};

////////////////////////////////////////////////////////////////////////

/// A text layout system.  Font data and glyph images are shared
/// between all layouts that use the same system.
class System {
private:
    friend class Batch;
    friend class Layout;
    friend class LayoutBuilder;

    Core *m_core;

public:
    System();
    System(const System &) = delete;
    ~System();
    System &operator=(const System &) = delete;

    /// Get the font data.
    const FontData &font_data() const;

    /// Get the vertex array data.
    const ArrayData &array_data() const;

    // Synchronize all changes to the renderer.
    void sync();
};

////////////////////////////////////////////////////////////////////////

/// A range of Open GL vertexes.
struct Range {
    /// The index of the first vertex.
    GLint first;

    /// The number of vertexes.
    GLint count;
};

/// A batch of layout data which can be drawn to the screen.
class Batch {
private:
    Core *m_core;
    int m_index;

public:
    Batch();
    explicit Batch(const System &);
    Batch(const Batch &) = delete;
    Batch(Batch &&);
    ~Batch();
    Batch &operator=(const Batch &) = delete;
    Batch &operator=(Batch &&);

    Range range() const;

    /// Remove everything from the batch.
    void clear();

    /// Add a layout to the batch.  Returns a handle.  The same layout
    /// can be added multiple times, the handle will be different each
    /// time.
    int add(const Layout &layout);

    /// Add a layout to the batch.  Returns a handle.  The same layout
    /// can be added multiple times, the handle will be different each
    /// time.
    int add(const Layout &layout, IVec2 offset);

    /// Set the draw offset for a layout.
    void set(int layout, IVec2 offset);

    /// Remove a layout from the batch.
    void remove(int layout);
};

inline Batch::Batch()
    : m_core(nullptr),
      m_index(0) {
}

inline Batch::Batch(Batch &&other)
    : m_core(other.m_core),
      m_index(other.m_index) {
    other.m_core = nullptr;
    other.m_index = 0;
}

inline Batch &Batch::operator=(Batch &&other) {
    std::swap(m_core, other.m_core);
    std::swap(m_index, other.m_index);
    return *this;
}

inline void Batch::clear() {
    *this = Batch();
}

////////////////////////////////////////////////////////////////////////

/// Metrics for a text layout.
struct Metrics {
    /// The logical bounds of the text.
    IBox2 logical_bounds;

    /// The pixel bounds of the text bitmap, which may be larger or
    /// smaller than the logical bounds.
    IBox2 pixel_bounds;

    /// The location of the baseline.
    int baseline;

    /// Get the location of the given point.
    IVec2 point(HAlign halign, VAlign valign) const;
};

/// A text layout, which can be drawn to the screen as part of a
/// batch.
///
/// Note that in general, the pixel bounds may extend past the logical
/// bounds, or the pixel bounds may be smaller.  Most layout tasks
/// should be done using the logical bounds.
class Layout {
private:
    friend class Batch;
    friend class LayoutBuilder;

    Core *m_core;
    int m_index;
    Metrics m_metrics;

public:
    Layout();
    Layout(const Layout &);
    Layout(Layout &&);
    ~Layout();
    Layout &operator=(const Layout &);
    Layout &operator=(Layout &&);

    /// Get the layout metrics.
    const Metrics &metrics() const { return m_metrics; }

    /// Test whether the layout is empty.
    bool empty() const { return m_core == nullptr; }

    /// Remove all text from the layout.
    void clear() { *this = Layout(); }
};

inline Layout::Layout()
    : m_core(nullptr) {
}

inline Layout::Layout(Layout &&other)
    : m_core(other.m_core),
      m_index(other.m_index),
      m_metrics(other.m_metrics) {
    other.m_core = nullptr;
}

inline Layout &Layout::operator=(Layout &&other) {
    std::swap(m_core, other.m_core);
    std::swap(m_index, other.m_index);
    std::swap(m_metrics, other.m_metrics);
    return *this;
}

////////////////////////////////////////////////////////////////////////

/// A piece of text and associated styles which can be typeset as a
/// layout.
class LayoutBuilder {
private:
    std::vector<char32_t> m_text;
    FontDesc m_font_desc;
    int m_width;
    int m_style;
    void *m_hb_buffer;

public:
    LayoutBuilder();
    LayoutBuilder(const LayoutBuilder &) = delete;
    LayoutBuilder(LayoutBuilder &&);
    ~LayoutBuilder();
    LayoutBuilder &operator=(const LayoutBuilder &) = delete;
    LayoutBuilder &operator=(LayoutBuilder &&);

    /// Clear the flow, removing all text and resetting all styles.
    void clear();
    /// Add text to the end of the flow.
    void add_text(const std::string &text);
    /// End the current line.  This is exactly equivalent to adding
    /// U+2028.
    void end_line();
    /// End the current paragraph.  This is exactly equivalent to
    /// adding U+2029.
    void end_paragraph();
    /// Set the layout font.
    void set_font(const FontDesc &font_desc);
    /// Set the layout width, in pixels.
    void set_width(float width);
    /// Set the style index.
    void set_style(int style_index);
    /// Typeset the text as a layout.
    Layout typeset(System &system);
};

////////////////////////////////////////////////////////////////////////

}
}
#endif
