/* Copyright 2013-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SGPP_SHADER_HPP
#define SGPP_SHADER_HPP

#include <cstddef>
#include <string>
#include <utility>
#include <vector>

#include "sggl/2_0.h"

namespace SG {

/// Path to the directory where shaders are located.
extern std::string shader_path;

/// A field in an object which stores program attributes and uniform indexes.
struct ShaderField {
    /// The name of the uniform or attribute.
    const char *name;

    /// The offset of the corresponding GLint field in the structure.
    std::size_t offset;
};

/// The specification for a shader program.
struct ProgramSpec {
    /// The attribute bindings, in order.
    std::vector<const char *> attribs;

    /// The shader stages.
    std::vector<std::pair<GLenum, std::string>> shaders;
};

/// A wrapper for a dynamically loaded program object.
class ProgramObj {
private:
    GLuint m_prog;
    std::string m_name;

public:
    ProgramObj();
    ProgramObj(const ProgramObj &) = delete;
    ProgramObj(ProgramObj &&);
    ~ProgramObj();
    ProgramObj &operator=(const ProgramObj &) = delete;
    ProgramObj &operator=(ProgramObj &&);

    /// Get the program object.
    GLuint prog() const { return m_prog; }

    /// Get the program's name.
    const std::string &name() const { return m_name; }

    /// Test whether the program is loaded.
    bool is_loaded() const { return m_prog != 0; }

    /// Load the given program.
    bool load(const ProgramSpec &spec);

    /// Load the uniform locations into an object.
    void get_uniforms(void *object, const ShaderField *uniforms) const;

    /// Load the uniform locations into an object.
    ///
    /// The object should have a static member named FIELDS, which is
    /// a zero-terminated array of ShaderField records.
    template<class T>
    void get_uniforms(T &object);
};

inline ProgramObj::ProgramObj()
    : m_prog(0),
      m_name() {
}

inline ProgramObj::ProgramObj(ProgramObj &&other)
    : m_prog(other.m_prog),
      m_name(std::move(other.m_name)) {
    other.m_prog = 0;
}

inline ProgramObj::~ProgramObj() {
    if (m_prog) {
        gl_2_0::glDeleteProgram(m_prog);
    }
}

inline ProgramObj &ProgramObj::operator=(ProgramObj &&other) {
    std::swap(m_prog, other.m_prog);
    std::swap(m_name, other.m_name);
    return *this;
}

template<class T>
void ProgramObj::get_uniforms(T &object) {
    get_uniforms(&object, T::FIELDS);
}

/// A wrapper for a dynamically loaded program object, with
template<class T>
class Program {
private:
    ProgramObj m_prog;
    T m_fields;

public:
    Program() = default;
    Program(const Program &) = delete;
    Program(Program &&) = default;
    ~Program() = default;
    Program &operator=(const Program &) = delete;
    Program &operator=(Program &&) = default;

    /// Get the program object.
    GLuint prog() const { return m_prog.prog(); }

    /// Get the program's name.
    const std::string &name() const { return m_prog.name(); }

    /// Test whether the program is loaded.
    bool is_loaded() const { return m_prog.is_loaded(); }

    /// Get the program uniform locations.
    const T &operator*() const { return m_fields; }

    /// Get the program uniform locations.
    const T *operator->() const { return &m_fields; }

    /// Load the given program.
    bool load(const ProgramSpec &spec);

private:
    /// Respond to the program being updated.
    void update();
};

template<class T>
bool Program<T>::load(const ProgramSpec &spec) {
    if (!m_prog.load(spec)) {
        return false;
    }
    m_prog.get_uniforms<T>(m_fields);
    return true;
}

}
#endif
