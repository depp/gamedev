/* Copyright 2013-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SGPP_FILE_HPP
#define SGPP_FILE_HPP

#include <cstddef>
#include <string>

#include "sg/file.h"

namespace SG {

/**
 * @file sgpp/file.hpp
 *
 * @brief File loading interface.
 */

/// @brief Contents of a file loaded from disk.
///
/// The buffer is reference counted, so copying this object will not
/// copy the buffer.
class Data {
    sg_filedata *m_data;

public:
    Data();
    Data(const Data &other);
    Data(Data &&other);
    ~Data();
    Data &operator=(const Data &other);
    Data &operator=(Data &&other);

    /// Get a pointer to the buffer.
    const void *data() const { return m_data->data; }
    /// Get the number of bytes in the buffer.
    std::size_t size() const { return m_data->length; }
    /// Get the actual path to the file.
    const char *path() const { return m_data->path; }

    /// Read the contents of a file, return true if successful.
    bool read(const std::string &path, size_t maxsz);
    /// Read the contents of a file, return true if successful.
    bool read(const std::string &path, size_t maxsz,
              const char *extensions);
};

inline Data::Data() : m_data(&sg_filedata_empty) {
    sg_filedata_incref(m_data);
}

inline Data::Data(const Data &other) : m_data(other.m_data) {
    sg_filedata_incref(m_data);
}

inline Data::Data(Data &&other) : m_data(&sg_filedata_empty) {
    std::swap(m_data, other.m_data);
}

inline Data::~Data() {
    sg_filedata_decref(m_data);
}

inline Data &Data::operator=(const Data &other) {
    sg_filedata *p = other.m_data;
    sg_filedata_incref(p);
    sg_filedata_decref(m_data);
    m_data = p;
    return *this;
}

inline Data &Data::operator=(Data &&other) {
    std::swap(m_data, other.m_data);
    return *this;
}

/// A list of files.
class FileList {
private:
    sg_filelist m_list;

public:
    FileList();
    FileList(const FileList &) = delete;
    FileList(FileList &&);
    ~FileList();
    FileList &operator=(const FileList &) = delete;
    FileList &operator=(FileList &&);

    const sg_fileentry *begin() const;
    const sg_fileentry *end() const;
    std::size_t size() const;

    bool scan(const std::string &path, unsigned flags);
    bool scan(const std::string &path, unsigned flags,
              const char *extensions);
};

inline FileList::FileList() {
    m_list.files = nullptr;
    m_list.count = 0;
    m_list.data_ = nullptr;
}

inline FileList::FileList(FileList &&other) : m_list(other.m_list) {
    sg_filelist &l = other.m_list;
    l.files = nullptr;
    l.count = 0;
    l.data_ = nullptr;
}

inline FileList::~FileList() {
    if (m_list.files != nullptr) {
        sg_filelist_destroy(&m_list);
    }
}

inline FileList &FileList::operator=(FileList &&other) {
    std::swap(m_list, other.m_list);
    return *this;
}

inline const sg_fileentry *FileList::begin() const {
    return m_list.files;
}

inline const sg_fileentry *FileList::end() const {
    return m_list.files + m_list.count;
}

inline std::size_t FileList::size() const {
    return m_list.count;
}

}
#endif
