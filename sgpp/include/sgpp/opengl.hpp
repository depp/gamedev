/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SGPP_OPENGL_HPP
#define SGPP_OPENGL_HPP
#include "sg/opengl.h"
namespace SG {

/// An OpenGL debugging group.
///
/// The debugging group is pushed onto the stack when the object is
/// created, and popped when the object is destroyed.
class DebugGroup {
public:
    explicit DebugGroup(const char *name) {
        sg_gl_debugf.push(::SG_GL_SOURCE_APP, 0, -1, name);
    }
    DebugGroup(int id, const char *name) {
        sg_gl_debugf.push(::SG_GL_SOURCE_APP, id, -1, name);
    }
    ~DebugGroup() {
        sg_gl_debugf.pop();
    }
    DebugGroup(const DebugGroup &) = delete;
    DebugGroup &operator=(const DebugGroup &) = delete;
};

}
#endif
