/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SGPP_ERROR_HPP
#define SGPP_ERROR_HPP
#include "sg/error.h"
#include <utility>
namespace SG {

/// Error wrapper for dealing with C functions.
class Error {
private:
    sg_error *m_ptr;

public:
    Error();
    Error(const Error &) = delete;
    Error(Error &&);
    ~Error();
    Error &operator=(const Error &) = delete;
    Error &operator=(Error &&);

    explicit operator bool() const;

    sg_error *get() const;
    sg_error **ptr();
    void clear();
};

inline Error::Error() : m_ptr(nullptr) {}

inline Error::Error(Error &&other) : m_ptr(other.m_ptr) {
    other.m_ptr = nullptr;
}

inline Error::~Error() {
    sg_error_clear(&m_ptr);
}

inline Error &Error::operator=(Error &&other) {
    std::swap(m_ptr, other.m_ptr);
    return *this;
}

inline Error::operator bool() const {
    return m_ptr != nullptr;
}

inline sg_error *Error::get() const {
    return m_ptr;
}

inline sg_error **Error::ptr() {
    return &m_ptr;
}

inline void Error::clear() {
    sg_error_clear(&m_ptr);
}

}
#endif
