/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SGPP_IMAGE_HPP
#define SGPP_IMAGE_HPP

#include <string>
#include <utility>

#include "sg/pixbuf.h"
#include "sggl/common.h"
#include "vec.hpp"

namespace SG {
class Pixbuf;

/// An abstract image loaded from a file.
class Image {
    sg_image *m_image;

public:
    Image();
    Image(const Image &) = delete;
    Image(Image &&other);
    ~Image();
    Image &operator=(const Image &) = delete;
    Image &operator=(Image &&other);

    explicit operator bool() const { return m_image != nullptr; }

    /// Get the image's size.  The image must be loaded.
    IVec2 size() const { return IVec2{{m_image->width, m_image->height}}; }
    /// Get whether the image is color instead of grayscale.  The
    /// image must be loaded.
    bool is_color() const { return (m_image->flags & SG_IMAGE_COLOR) != 0; }
    /// Get whether the image has an alpha channel.
    bool has_alpha() const { return (m_image->flags & SG_IMAGE_ALPHA) != 0; }

    /// Load an image, returning true on success.
    bool load(const std::string &path);
    /// Draw the image into a pixel buffer, coordinates are from top left.
    void draw(Pixbuf &buf, int x, int y) const;
    /// Draw the image into a pixel buffer, coordinates are from top left.
    void draw(sg_pixbuf &buf, int x, int y) const;
};

/// An owned pixel buffer, as opposed to one which aliases other memory.
class Pixbuf {
    friend class Image;
    friend class Texture;
    sg_pixbuf m_pixbuf;

public:
    Pixbuf();
    Pixbuf(const Pixbuf &) = delete;
    Pixbuf(Pixbuf &&other);
    ~Pixbuf();
    Pixbuf &operator=(const Pixbuf &) = delete;
    Pixbuf &operator=(Pixbuf &&other);

    /// Allocate the buffer, uninitialized.
    void alloc(sg_pixbuf_format_t format, int width, int height);
    /// Allocate the buffer, filled with zeroes.
    void calloc(sg_pixbuf_format_t format, int width, int height);
    /// Clear the buffer.
    void clear();
    /// Write the buffer to a PNG file.
    bool write_png(const std::string &path) const;
    /// Get a poitner to the internal pixbuf structure.
    sg_pixbuf &pixbuf() { return m_pixbuf; }
};

/// A loaded OpenGL texture.
class Texture {
public:
    /// Flag for loading an sRGB texture (this is the default).
    static const unsigned TYPE_SRGB = 00;
    /// Flag for loading a normalized linear texture.
    static const unsigned TYPE_UNORM = 01;
    /// Flag for loading an integer linear texture.
    static const unsigned TYPE_UINT = 02;
    /// Flag for loading a normalized signed linear texture.
    static const unsigned TYPE_SNORM = 03;
    /// Flag for loading an integer signed linear texture.
    static const unsigned TYPE_SINT = 04;

    /// Mask for texture types.
    static const unsigned MASK_TYPE = 07;
    /// Flag for setting nearest neighbor filtering.
    static const unsigned FILTER_NEAREST = 010;
    /// Flag for disabling mipmap generation.
    static const unsigned NO_MIPMAP = 020;
    /// Flag for using clamping instead of wrapping.
    static const unsigned CLAMP = 040;

    /// The OpenGL texture object.
    GLuint tex;
    /// The size of the texture, in pixels.
    IVec2 texsize;
    /// The size of the image loaded in the texture, in pixels.
    IVec2 imgsize;
    /// The factor for conversion from pixels to texture coordinates.
    Vec2 texscale;

    Texture();
    Texture(const Texture &other) = delete;
    Texture(Texture &&);
    ~Texture();
    Texture &operator=(const Texture &) = delete;
    Texture &operator=(Texture &&other);

    /// Load an image from a file as a 1-dimensional texture.
    bool load_1d(const std::string &path, unsigned flags = 0);
    /// Load an image as a 1-dimensional texture.
    bool load_1d(const Image &image, unsigned flags = 0);
    /// Load a pixel buffer as a 1-dimensional texture.
    bool load_1d(const Pixbuf &pixbuf, unsigned flags = 0);

    /// Load an image from a file as a 2-dimensional texture.
    bool load_2d(const std::string &path, unsigned flags = 0);
    /// Load an image as a 2-dimensional texture.
    bool load_2d(const Image &image, unsigned flags = 0);
    /// Load a pixel buffer as a 2-dimensional texture.
    bool load_2d(const Pixbuf &pixbuf, unsigned flags = 0);

};

}
#endif
