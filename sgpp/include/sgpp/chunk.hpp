/* Copyright 2014 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SGPP_CHUNK_HPP
#define SGPP_CHUNK_HPP

#include <utility>

#include "file.hpp"
#include "range.hpp"

namespace SG {

/**
 * @file sgpp/chunk.hpp
 *
 * @brief Chunk file format interface.
 */

/// Object for reading chunked file formats.
///
/// Pointers returned by the reader alias the file data.
class ChunkReader {
public:
    /// The format's version number, (major, minor).
    typedef std::pair<int, int> Version;

    /// Raw data from one of the chunks.
    typedef std::pair<const void *, std::size_t> ChunkData;

private:
    struct Entry;

    Data m_data;
    const Entry *m_first, *m_last;

public:
    ChunkReader();
    ~ChunkReader();

    /// Read a chunked file, returns true if the file is well-formed.
    bool read(const Data &data);

    /// Get the file's magic cookie.
    const char *magic() const;

    /// Get the file's version.
    Version version() const;

    /// Get file chunk data.
    ChunkData get_data(const char *name) const;

    /// Get file chunk data as an object, or NULL if not found.
    template<class T>
    const T *get_object(const char *name) const;

    /// Get file chunk data as an array.
    template<class T>
    Range<T> get_array(const char *name) const;
};

/// Get file chunk data as an object, or NULL if not found.
template<class T>
const T *ChunkReader::get_object(const char *name) const {
    auto chunk = get_data(name);
    return chunk.second >= sizeof(T) ?
        reinterpret_cast<const T>(chunk.first) : nullptr;
}

/// Get file chunk data as an array.
template<class T>
Range<T> ChunkReader::get_array(const char *name) const {
    auto chunk = get_data(name);
    const T *first = reinterpret_cast<const T *>(chunk.first);
    std::size_t count = chunk.second / sizeof(T);
    return Range<T>(first, first + count);
}

}
#endif
