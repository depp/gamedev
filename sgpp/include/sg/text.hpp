/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_TEXT_HPP
#define SG_TEXT_HPP

/**
 * \file sg/text.hpp Text processing utilities.
 */

#include <string>
#include <vector>

namespace SG {
namespace Text {

/**
 * \brief Decode UTF-8 to a sequence of code points.
 *
 * This function never fails, unless there is an allocation failure.
 * Invalid input gets replaced with U+FFFD, and conversion continues.
 *
 * \param out The output code point vector, code points are appended
 * to this vector.
 * \param ptr A pointer to the input UTF-8 text.
 * \param size The size of the input, in bytes.
 * \return True if input was well-formed, false if some ill-formed
 * sequences were encountered and replaced with U+FFFD.
 */
bool utf8_decode(std::vector<char32_t> &out, const char *ptr, std::size_t size);

#if defined _WIN32 || defined DOXYGEN || defined TEST_WSTRING

/** Convert a wide string to a UTF-8 string (Windows only).
    \return True if input was well-formed, false otherwise.  */
bool from_wstring(std::string &out, const std::wstring &str);

/** Convert a wide string to a UTF-8 string (Windows only).
    \return True if input was well-formed, false otherwise.  */
bool from_wstring(std::string &out, const wchar_t *ptr);

/** Convert a wide string to a UTF-8 string (Windows only).
    \return True if input was well-formed, false otherwise.  */
bool from_wstring(std::string &out, const wchar_t *ptr, std::size_t size);

/** Convert a UTF-8 string to a wide string (Windows only).
    \return True if input was well-formed, false otherwise.  */
bool to_wstring(std::wstring &out, const std::string &str);

/** Convert a UTF-8 string to a wide string (Windows only).
    \return True if input was well-formed, false otherwise.  */
bool to_wstring(std::wstring &out, const char *ptr);

/** Convert a UTF-8 string to a wide string (Windows only).
    \return True if input was well-formed, false otherwise.  */
bool to_wstring(std::wstring &out, const char *ptr, std::size_t size);

#endif

}
}
#endif
