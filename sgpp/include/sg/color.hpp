/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_COLOR_HPP
#define SG_COLOR_HPP

#include "vec.hpp"

#include <cstddef>

namespace SG {

//! Data for DawnBringer's 16-color palette.
extern const unsigned char PALETTE_DB16[16][4];

//! Data for DawnBringer's 32-color palette.
extern const unsigned char PALETTE_DB32[32][4];

//! Conversion table from sRGB to linear RGB.
extern const float SRGB_TABLE[256];

//! A color palette.
class Palette {
private:
    const unsigned char (*m_pal)[4];
    int m_size;

public:
    Palette() : m_pal(nullptr), m_size(0) {}

    //! Create a palette with the given data and size.
    Palette(const unsigned char (*pal)[4], int size)
        : m_pal(pal), m_size(size) {}

    //! Get the number of colors in the palette.
    int size() const { return m_size; }

    //! Get a palette entry in sRGB.
    vec4 srgb(int index) const {
        vec4 r;
        for (int i = 0; i < 3; i++) {
            r[i] = static_cast<float>(m_pal[index][i]) * (1.0f / 255.0f);
        }
        r[3] = 1.0f;
        return r;
    }

    //! Get a palette entry in linear RGB.
    vec4 linear(int index) const {
        vec4 r;
        for (int i = 0; i < 3; i++) {
            r[i] = SRGB_TABLE[m_pal[index][i]];
        }
        r[3] = 1.0f;
        return r;
    }

    //! Get a packed palette entry in sRGB.
    unsigned packed(int index) const {
        return reinterpret_cast<const unsigned *>(m_pal)[index];
    }
};

//! Get DawnBringer's 16-color palette.
inline Palette palette_db16() {
    return Palette(PALETTE_DB16, 16);
}

//! Get DawnBringer's 32-color palette.
inline Palette palette_db32() {
    return Palette(PALETTE_DB32, 32);
}

//! Convert a color from sRGB to linear RGB.
vec4 from_srgb(const vec4 &c);

//! Convert a color from linear RGB to sRGB.
vec4 to_srgb(const vec4 &c);

//! Pack a color into 32 bits.
unsigned pack_color(const vec4 &c);

}
#endif
