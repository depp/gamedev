/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_OPENGL_H
#define SG_OPENGL_H
#include "config.h"
#include "sggl/common.h"
struct sg_error;

/**
 * @file opengl.h
 *
 * @brief OpenGL utilities.
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Sources for sg_gl_debugf.push().
 */
enum {
    /**
     * @brief Source for messages from the application.
     *
     * This is equal to ::GL_DEBUG_SOURCE_APPLICATION.
     */
    SG_GL_SOURCE_APP = 0x824a,

    /**
     * @brief Source for messages from a third-party application.
     *
     * This is equal to ::GL_DEBUG_SOURCE_THIRD_PARTY.
     */
    SG_GL_SOURCE_THIRD_PARTY = 0x8249
};

/**
 * @brief OpenGL debugging.
 *
 * These function pointers are never null, but if debugging is not
 * enabled, they will point to stub implementations.  If debugging is
 * enabled, they will point to the corresponding OpenGL functions.
 */
struct sg_gl_debugf {
    /**
     * @brief Value indicating whether debugging is enabled.
     */
    int enabled;

    /**
     * @brief Push a debug group into the command stream.
     *
     * This points to glPushDebugGroup() if debugging is enabled.
     */
    void
    (SG_GL_API *push)(
        GLenum source,
        GLuint id,
        GLsizei length,
        const char *message);

    /**
     * @brief Pop a debug group in the command stream.
     *
     * This points to glPopDebugGroup() if debugging is enabled.
     */
    void
    (SG_GL_API *pop)(void);
};

/**
 * @brief OpenGL debugging info.
 */
extern struct sg_gl_debugf sg_gl_debugf;

/**
 * @brief Check for OpenGL errors and log them.
 *
 * @param where A format string for error locations.
 * @return Zero if there were no errors, nonzero otherwise.
 */
int
sg_opengl_checkerror(const char *where, ...);

/**
 * @brief An OpenGL error.
 */
extern const struct sg_error_domain SG_ERROR_OPENGL;

/**
 * @brief Create an OpenGL error.
 *
 * @param err The error to initialize, can be null.
 */
void
sg_error_opengl(struct sg_error **err);

#ifdef __cplusplus
}
#endif
#endif
