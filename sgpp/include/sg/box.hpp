/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_BOX_HPP
#define SG_BOX_HPP

/**
 * \file sg/box.hpp Axis-aligned box types.
 */

#include "vec.hpp"

#include <algorithm>

namespace SG {

/// N-dimensional axis-aligned box.
template <typename T, int N>
struct GBox {
    //! The associated vector type.
    typedef GVec<T, N> Vec;

    //! Negative limit of box.
    GVec<T, N> mins;
    //! Positive limit of box.
    GVec<T, N> maxs;

    //! A box with zero volume.
    static GBox zero() { return GBox{Vec::zero(), Vec::zero()}; }

    //! Create the smallest box containing both boxes.
    static GBox union_(const GBox &a, const GBox &b);

    //! Create the largest box contained by both boxes.
    static GBox intersection(const GBox &a, const GBox &b);

    //! Test whether the box has zero volume.
    bool empty() const;

    //! Test whether this box contains the given vector.
    bool contains(const GVec<T, N> &v) const;

    //! Test Whether this box contains the given box.
    bool contains(const GBox &box) const;

    //! Get the volume of the box.
    int volume() const;
};

//! Translate the box by a vector.
template <typename T, int N>
GBox<T, N> operator+(const GBox<T, N> &b, const GVec<T, N> &v) {
    return GBox<T, N>{b.mins + v, b.maxs + v};
}

//! Translate the box by a vector.
template <typename T, int N>
GBox<T, N> &operator+=(const GBox<T, N> &b, const GVec<T, N> &v) {
    b.mins += v;
    b.maxs += v;
    return b;
}

//! Translate the box by a vector.
template <typename T, int N>
GBox<T, N> operator-(const GBox<T, N> &b, const GVec<T, N> &v) {
    return GBox<T, N>{b.mins - v, b.maxs - v};
}

//! Translate the box by a vector.
template <typename T, int N>
GBox<T, N> &operator-=(const GBox<T, N> &b, const GVec<T, N> &v) {
    b.mins -= v;
    b.maxs -= v;
    return b;
}

template <typename T, int N>
GBox<T, N> GBox<T, N>::union_(const GBox<T, N> &a, const GBox<T, N> &b) {
    GBox<T, N> r;
    for (int i = 0; i < N; i++) {
        r.mins[i] = std::min(a.mins[i], b.mins[i]);
        r.maxs[i] = std::max(a.maxs[i], b.maxs[i]);
    }
    return r;
}

template <typename T, int N>
GBox<T, N> GBox<T, N>::intersection(const GBox<T, N> &a, const GBox<T, N> &b) {
    GBox<T, N> r;
    for (int i = 0; i < N; i++) {
        r.mins[i] = std::max(a.mins[i], b.mins[i]);
        r.maxs[i] = std::min(a.maxs[i], b.maxs[i]);
    }
    return r;
}

template <typename T, int N>
bool GBox<T, N>::empty() const {
    for (int i = 0; i < N; i++)
        if (mins[i] >= maxs[i]) return true;
    return false;
}

template <typename T, int N>
bool GBox<T, N>::contains(const GVec<T, N> &v) const {
    for (int i = 0; i < N; i++) {
        if (mins[i] > v[i] || maxs[i] <= v[i]) {
            return false;
        }
    }
    return true;
}

template <typename T, int N>
bool GBox<T, N>::contains(const GBox<T, N> &box) const {
    for (int i = 0; i < N; i++) {
        if (mins[i] > box.mins[i] || maxs[i] < box.maxs[i]) {
            return false;
        }
    }
    return true;
}

template <typename T, int N>
int GBox<T, N>::volume() const {
    int v = 1;
    for (int i = 0; i < N; i++) {
        v *= maxs[i] - mins[i];
    }
    return v;
}

//! 2D 32-bit integer box.
typedef GBox<int, 2> ibox2;
//! 3D 32-bit integer box.
typedef GBox<int, 3> ibox3;

//! 2D singnle-precision floating point box.
typedef GBox<float, 2> box2;
//! 3D singnle-precision floating point box.
typedef GBox<float, 3> box3;

}
#endif
