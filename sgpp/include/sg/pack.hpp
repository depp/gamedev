/* Copyright 2013-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_PACK_HPP
#define SG_PACK_HPP

/**
 * \file sg/pack.hpp Bin packing.
 */

#include <utility>
#include <vector>

#include "vec.hpp"

namespace SG {

/**
 * \brief A packing of rectangles in a larger bin.
 *
 * The object tracks free space within the bin, so rectangles can be
 * added to the bin dynamically.
 */
class Packing {
public:
    struct RectRef;

    //! A rectangle to be packed in a bin.
    struct Rect {
        //! The rectangle size.  This is a packing algorithm input.
        svec2 size;

        //! The location of the rectangle's bottom left corner.  This
        //! is a packing algorithm output.
        svec2 loc;
    };

private:
    std::vector<svec2> m_nodes;
    ivec2 m_size;

public:
    Packing();

    //! Get the size of the bin.
    ivec2 size() const { return m_size; }

    /**
     * \brief Add rectangles to the packing.
     *
     * The input array is modified to store the location of the
     * rectangles.  This may fail if there is not enough space in the
     * bin, or if the algorithm cannot find a way to pack the
     * rectangles.
     *
     * \param rects Pointer to an array of rectangles to add.
     * \param count The number of rectangles to add.
     * \return True on success, false on failure.
     */
    bool pack(Rect *rects, std::size_t count);

    /**
     * \brief Create a new packing, automatically choosing the size.
     *
     * This will reset the packing, choose an appropriate size, and
     * add the given rectangles to the packing.  Larger sizes will be
     * chosen until packing succeeds.  Only powers of two will be used
     * for dimensions.
     *
     * \param rects Pointer to all rectangles to be included.
     * \param count The number of rectangles.
     * \param min_size The minimum bin size.
     * \param max_size The maximum bin size.
     * \return True on success, false on failure.
     */
    bool auto_pack(Rect *rects, std::size_t count, ivec2 min_size,
                   ivec2 max_size);

    //! Reset the packing to an empty bin with the given size.
    void reset(ivec2 size);

private:
    bool pack_impl(Rect *rects, const RectRef *refs, std::size_t count);
};

}
#endif
