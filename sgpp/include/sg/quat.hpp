/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_QUAT_HPP
#define SG_QUAT_HPP

/**
 * \file sg/quat.hpp Quaternion type.
 */

#include "vec.hpp"

namespace SG {

/**
 * \brief Floating-point quaternion type.
 */
struct quat {
    /**
     * \brief The quaternion components.
     *
     * The first component is real, the remaining components are
     * imaginary.
     */
    float v[4];

    //! Access a quaternion component by index.
    float operator[](int i) const { return v[i]; }

    //! Access a quaternion component by index.
    float &operator[](int i) { return v[i]; }

    //! Unit quaternion.
    static quat one() {
        return quat{{1.0f, 0.0f, 0.0f, 0.0f}};
    }

    //! Create a rotation quation.
    static quat axis_angle(vec3 axis, float angle);

    /**
     * \brief Create a rotation quaternion from angles.
     *
     * The resulting rotation rotates around the X axis, then the Y
     * axis, then the Z axis.  (Technically, Tait-Bryan angles.)  For
     * objects that are oriented in the direction of the X axis, with
     * the Z axis vertical, this gives the familiar roll, pitch, and
     * yaw angles (except that all angles are right-handed).
     */
    static quat euler_angles(vec3 angles);

    //! Transform a vector.
    vec3 transform(vec3 p) const;

    //! Get a copy of the quaternion normalized to unit length.
    quat normalized() const;

    /**
     * \brief Get the quaternion's conjugate.
     *
     * For rotation quaternions, which are unit length, this is the
     * quaternion's inverse.
     */
    quat conjugate() const { return quat{{v[0], -v[1], -v[2], -v[3]}}; }
};

//! Multiply two quaternions.
quat operator*(quat x, quat y);

//! Multiply one quaternion by another.
quat &operator*=(quat &x, quat y);

}
#endif
