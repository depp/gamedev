/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_UTIL_HPP
#define SG_UTIL_HPP

/**
 * \file sg/util.hpp Utility functions.
 */

#include "defs.hpp"

#include <cstddef>

namespace SG {

#if defined(__clang__)
# if __has_builtin(__builtin_bswap32)
#  define SG_SWAP32 __builtin_bswap32
# endif
# if __has_builtin(__builtin_bswap64)
#  define SG_SWAP64 __builtin_bswap64
# endif
#elif defined(__GNUC__)
# if __GNUC__ > 4 || (__GNUC__ >= 4 && __GNUC_MINOR__ >= 3)
#  define SG_SWAP32 __builtin_bswap32
#  define SG_SWAP64 __builtin_bswap64
# endif
#elif defined(_MSC_VER)
# include <stdlib.h>
# define SG_SWAP16 _byteswap_ushort
# define SG_SWAP32 _byteswap_ulong
# define SG_SWAP64 _byteswap_uint64
#endif

//! Swap the byte order of a 16-bit integer.
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned short swap16(unsigned short x);

//! Swap the byte order of a 32-bit integer.
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned swap32(unsigned x);

//! Swap the byte order of a 64-bit integer.
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned long long swap64(unsigned long long x);

#if defined(SG_SWAP16)
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned short swap16(unsigned short x) {
    return SG_SWAP16(x);
}
#elif defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__))
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned short swap16(unsigned short x) {
    __asm__("xchgb %b0,%h0" : "=Q"(x) : "0"(x));
    return x;
}
#elif defined(__GNUC__) && (defined(__powersg__) || defined(__psg__))
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned short swap16(unsigned short x) {
    unsigned tmp;
    __asm__("rlwimi %0,%2,8,16,23"
            : "=r"(tmp)
            : "0"(x >> 8), "r"(x));
    return (unsigned short) tmp;
}
#else
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned short swap16(unsigned short x) {
    return (unsigned short) ((x >> 8) | (x << 8));
}
#endif

#if defined(SG_SWAP32)
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned swap32(unsigned x) {
    return SG_SWAP32(x);
}
#elif defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__))
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned swap32(unsigned x) {
    __asm__("bswap %0" : "=r"(x) : "0"(x));
    return x;
}
#elif defined(__GNUC__) && (defined(__powersg__) || defined(__psg__))
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned swap32(unsigned x) {
    unsigned tmp;
    __asm__("rotlwi %0,%1,8" : "=r"(tmp) : "r"(x));
    __asm__("rlwimi %0,%2,24,0,7" : "=r"(tmp) : "0"(tmp), "r"(x));
    __asm__("rlwimi %0,%2,24,16,23" : "=r"(tmp) : "0"(tmp), "r"(x));
    return tmp;
}
#else
SG_ATTR_ARTIFICIAL SG_ATTR_CONST inline unsigned swap32(unsigned x) {
    return ((x & 0xff000000) >> 24) | ((x & 0x00ff0000) >> 8) |
           ((x & 0x0000ff00) << 8) | ((x & 0x000000ff) << 24);
}
#endif

#if defined(SG_SWAP64)
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned long long swap64(unsigned long long x) {
    return SG_SWAP64(x);
}
#elif defined(__GNUC__) && defined(__x86_64__)
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned long long swap64(unsigned long long x) {
    __asm__("bswapq %0" : "=r"(x) : "0"(x));
}
#else
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned long long swap64(unsigned long long x) {
    return (unsigned long long) swap32((unsigned) (x >> 32)) |
           ((unsigned long long) swap32((unsigned) x) << 32);
}
#endif

#if SG_BYTE_ORDER == SG_LITTLE_ENDIAN
# define swapbe16(x) swap16(x)
# define swapbe32(x) swap32(x)
# define swapbe64(x) swap64(x)
# define swaple16(x) (x)
# define swaple32(x) (x)
# define swaple64(x) (x)
#elif SG_BYTE_ORDER == SG_BIG_ENDIAN
# define swapbe16(x) (x)
# define swapbe32(x) (x)
# define swapbe64(x) (x)
# define swaple16(x) swap16(x)
# define swaple32(x) swap32(x)
# define swaple64(x) swap64(x)
#elif defined(DOXYGEN)
//! Swap the byte order of a 16-bit integer between native and big
/// endian.
# define swapbe16(x)
//! Swap the byte order of a 32-bit integer between native and big
/// endian.
# define swapbe32(x)
//! Swap the byte order of a 64-bit integer between native and big
/// endian.
# define swapbe64(x)
//! Swap the byte order of a 16-bit integer between native and little
/// endian.
# define swaple16(x)
//! Swap the byte order of a 32-bit integer between native and little
/// endian.
# define swaple32(x)
//! Swap the byte order of a 64-bit integer between native and little
/// endian.
# define swaple64(x)
#endif

//! Rotate a 32-bit integer to the left.
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned rotl32(unsigned x, unsigned k);

//! @brief Rotate a 32-bit integer to the right.
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned rotr32(unsigned x, unsigned k);

#if defined(_MSC_VER)

#include <stdlib.h>

SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned rotl32(unsigned x, unsigned k) {
    return _rotl(x, k);
}

SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned rotr32(unsigned x, unsigned k) {
    return _rotr(x, k);
}

#else

SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned rotl32(unsigned x, unsigned k) {
    return (x << (k & 31)) | (x >> ((0 - k) & 31));
}

SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned rotr32(unsigned x, unsigned k) {
    return (x >> (k & 31)) | (x << ((0 - k) & 31));
}

#endif

//! Round a number up to the next power of two.
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline unsigned round_up_pow2(unsigned x) {
    x -= 1;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return x + 1;
}

//! Round a number up to the next power of two.
SG_ATTR_ARTIFICIAL SG_ATTR_CONST
inline std::size_t round_up_pow2(std::size_t x) {
    x -= 1;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    // Any decent compiler will optimize this to x >> 32 or a nop, so
    // this will work correctly on both 32-bit and 64-bit systems and
    // it won't generate a warning for either.
    x |= (x >> 16) >> 16;
    return x + 1;
}

}
#endif
