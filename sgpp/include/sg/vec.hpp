/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_VEC_HPP
#define SG_VEC_HPP

/**
 * \file sg/vec.hpp Vector types.
 */

#include <cmath>
#include <type_traits>

namespace SG {

/**
 * \brief General vector type.
 *
 * \tparam T The underlying scalar type.
 * \tparam N The number of dimensions.
 */
template <typename T, int N>
struct GVec {
    static_assert(std::is_integral<T>::value ||
                      std::is_floating_point<T>::value,
                  "Vector must be integer or float.");
    static_assert(N >= 1 && N <= 4, "Vector size must be between 1 and 4.");

    //! The vector components.
    T v[N];

    //! Access a vector component by index.
    T operator[](int i) const { return v[i]; }

    //! Access a vector component by index.
    T &operator[](int i) { return v[i]; }

    //! Convert to another vector type with the same dimension.
    template <typename U>
    explicit operator GVec<U, N>() const {
        GVec<U, N> r;
        for (int i = 0; i < N; i++) {
            r.v[i] = static_cast<U>(v[i]);
        }
        return r;
    }

    //! The zero vector.
    static GVec zero() {
        GVec r;
        for (int i = 0; i < N; i++) {
            r.v[i] = static_cast<T>(0);
        }
        return r;
    }
};

//! Get the squared length of a vector.
template <typename T, int N>
T length2(GVec<T, N> v) {
    T a = 0;
    for (int i = 0; i < N; i++) {
        a += v[i] * v[i];
    }
    return a;
}

//! Get the length of a floating-point vector.
template <typename T, int N>
T length(GVec<T, N> v) {
    static_assert(std::is_floating_point<T>::value, "Must be floating point.");
    return std::sqrt(length2(v));
}

//! Get the squared distance between two vectors.
template <typename T, int N>
T distance2(GVec<T, N> u, GVec<T, N> v) {
    return length2(u - v);
}

//! Get the distance between two floating-point vectors.
template <typename T, int N>
T distance(GVec<T, N> u, GVec<T, N> v) {
    return length(u - v);
}

//! Get the dot product of two vectors.
template <typename T, int N>
T dot(GVec<T, N> u, GVec<T, N> v) {
    T a = 0;
    for (int i = 0; i < N; i++) {
        a += u.v[i] * v.v[i];
    }
    return a;
}

//! Interpolate linearly between two floating-point vectors.
template <typename T, int N>
GVec<T, N> mix(GVec<T, N> u, GVec<T, N> v, T a) {
    static_assert(std::is_floating_point<T>::value, "Must be floating point.");
    return u + (v - u) * a;
}

//! Normalize a floating-point vector to unit length.
template <typename T, int N>
GVec<T, N> normalize(GVec<T, N> v) {
    return v * (1 / length(v));
}

//! Test if two vectors are equal.
template <typename T, int N>
bool operator==(GVec<T, N> u, GVec<T, N> v) {
    for (int i = 0; i < N; i++) {
        if (u[i] != v[i]) {
            return false;
        }
    }
    return true;
}

//! Test if two vectors are inequal.
template <typename T, int N>
bool operator!=(GVec<T, N> u, GVec<T, N> v) {
    return !(u == v);
}

//! Return the vector unmodified.
template <typename T, int N>
GVec<T, N> operator+(GVec<T, N> u) {
    return u;
}

//! Add two vectors.
template <typename T, int N>
GVec<T, N> operator+(GVec<T, N> u, GVec<T, N> v) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++) {
        r[i] = u[i] + v[i];
    }
    return r;
}

//! Add a vector to another.
template <typename T, int N>
GVec<T, N> &operator+=(GVec<T, N> &u, GVec<T, N> v) {
    for (int i = 0; i < N; i++) {
        u[i] += v[i];
    }
    return u;
}

//! Negate a vector.
template <typename T, int N>
GVec<T, N> operator-(GVec<T, N> v) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++) {
        r[i] = -v[i];
    }
    return r;
}

//! Get the difference between two vectors.
template <typename T, int N>
GVec<T, N> operator-(GVec<T, N> u, GVec<T, N> v) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++) {
        r[i] = u[i] - v[i];
    }
    return r;
}

//! Subtract one vector from another.
template <typename T, int N>
GVec<T, N> &operator-=(GVec<T, N> &u, GVec<T, N> v) {
    for (int i = 0; i < N; i++) {
        u[i] -= v[i];
    }
    return u;
}

//! Scale a vector.
template <typename T, int N>
GVec<T, N> operator*(T a, GVec<T, N> v) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++) {
        r[i] = a * v[i];
    }
    return r;
}

//! Scale a vector.
template <typename T, int N>
GVec<T, N> operator*(GVec<T, N> v, T a) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++) {
        r[i] = v[i] * a;
    }
    return r;
}

//! Scale a vector.
template <typename T, int N>
GVec<T, N> &operator*=(GVec<T, N> &u, T a) {
    for (int i = 0; i < N; i++) {
        u[i] *= a;
    }
    return u;
}

//! Get the cross product of two 3D vectors.
template <typename T>
GVec<T, 3> cross(GVec<T, 3> u, GVec<T, 3> v) {
    GVec<T, 3> r;
    r.v[0] = u[1] * v[2] - u[2] * v[1];
    r.v[1] = u[2] * v[0] - u[0] * v[2];
    r.v[2] = u[0] * v[1] - u[1] * v[0];
    return r;
}

//! 2D single-precision floating point vector.
typedef GVec<float, 2> vec2;
//! 3D single-precision floating point vector.
typedef GVec<float, 3> vec3;
//! 4D single-precision floating point vector.
typedef GVec<float, 4> vec4;

//! 2D 32-bit integer vector.
typedef GVec<int, 2> ivec2;
//! 3D 32-bit integer vector.
typedef GVec<int, 3> ivec3;
//! 4D 32-bit integer vector.
typedef GVec<int, 4> ivec4;

//! 2D 16-bit integer vector.
typedef GVec<short, 2> svec2;
//! 3D 16-bit integer vector.
typedef GVec<short, 3> svec3;
//! 4D 16-bit integer vector.
typedef GVec<short, 4> svec4;

//! Get a pointer to the vector elements.
template<class T, int N>
const T *data(const GVec<T, N> &v) {
    return v.v;
}

}
#endif
