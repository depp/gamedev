/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_RANGE_HPP
#define SG_RANGE_HPP

/**
 * \file sg/range.hpp Range iteration utilities.
 */

#include <array>
#include <vector>

namespace SG {

/**
 * \brief Range object for iterating over an array or vector.
 *
 * This allows a range to be returned from a method as easily as a
 * pointer could be.
 */
template <class T>
class ArrayRange {
private:
    const T *m_first, *m_last;

public:
    //! Create an empty range.
    ArrayRange() : m_first(nullptr), m_last(nullptr) { }

    /**
     * \brief Create a range from a first and last element pointer.
     *
     * As is convention, the range is half-open.
     */
    ArrayRange(const T *first, const T *last) : m_first(first), m_last(last) {}

    //! Create a range from a vector.
    ArrayRange(const std::vector<T> &vec)
        : m_first(vec.data()), m_last(vec.data() + vec.size()) {}

    //! Create a range from an array.
    template <std::size_t N>
    ArrayRange(const T(&array)[N])
        : m_first(&array[0]), m_last(&array[N]) {}

    //! Create a range from an array.
    template <std::size_t N>
    ArrayRange(const std::array<T, N> &arr)
        : m_first(&arr[0]), m_last(&arr[N]) {};

    //! Get an iterator for the beginning of the range.
    const T *begin() const { return m_first; }

    //! Get an iterator for the end of the range.
    const T *end() const { return m_last; }

    //! Get the number of elements in the range.
    std::size_t size() const { return m_last - m_first; }

    //! Access an element in the range by index.
    const T &operator[](std::size_t i) const { return m_first[i]; }
};

}
#endif
