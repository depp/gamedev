/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_MAT_HPP
#define SG_MAT_HPP

/**
 * \file sg/mat.hpp Matrix type.
 */

#include "vec.hpp"
#include "quat.hpp"

namespace SG {

//! 3x3 floating-point matrix.
struct mat3 {
    //! Matrix elements, in column major order.
    float m[3][3];

    //! Transform a (column) vector using the matrix.
    vec3 transform(vec3 v) const;

    //! Create the identity matrix.
    static mat3 identity();

    //! Create a scale matrix.
    static mat3 scale(float a);

    //! Create a scale matrix.
    static mat3 scale(vec3 v);

    //! Create a rotation matrix.
    static mat3 rotation(quat q);
};

//! Multiply two matrixes.
mat3 operator*(const mat3 &x, const mat3 &y);

//! Multiply two matrixes.
mat3 &operator*=(mat3 &x, const mat3 &y);

//! Get a pointer to the matrix elements.
inline const float *data(const mat3 &m) {
    return &m.m[0][0];
}

//! 4x4 floating-point matrix.
struct mat4 {
    //! Matrix elements, in column major order.
    float m[4][4];

    //! Transform a (column) vector using the matrix.
    vec3 transform(vec3 v) const;

    //! Create the identity matrix.
    static mat4 identity();

    //! Create a translation matrix.
    static mat4 translation(vec3 v);

    //! Create a scale matrix.
    static mat4 scale(float a);

    //! Create a scale matrix.
    static mat4 scale(vec3 v);

    //! Create a rotation matrix.
    static mat4 rotation(quat q);

    //! Create a perspective projection matrix.
    static mat4 perspective(float mx, float my, float near, float far);
};

//! Multiply two matrixes.
mat4 operator*(const mat4 &x, const mat4 &y);

//! Multiply two matrixes.
mat4 &operator*=(mat4 &x, const mat4 &y);

//! Get a pointer to the matrix elements.
inline const float *data(const mat4 &m) {
    return &m.m[0][0];
}

}
#endif
