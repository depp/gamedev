/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef SG_REF_HPP
#define SG_REF_HPP

#include <utility>

/**
 * \file sg/ref.hpp Shared reference type.
 */

namespace SG {

/**
 * \brief Shared reference.
 *
 * The type must implement static incref and decref methods, which
 * take a pointer argument.  Compared to std::shared_reference, this
 * type implements intrusive shared references.
 */
template <class T>
class Ref {
private:
    T *m_ptr;

public:
    //! Create a null reference.
    Ref() : m_ptr(nullptr) {}

    //! Create a new reference from an object pointer.
    explicit Ref(T *p) : m_ptr(nullptr) {
        if (m_ptr) {
            T::incref(m_ptr);
        }
    }

    //! Copy a reference, sharing it.
    Ref(const Ref &other) : m_ptr(other.m_ptr) {
        if (m_ptr) {
            T::incref(m_ptr);
        }
    }

    //! Move a reference.
    Ref(Ref &&other) : m_ptr(other.m_ptr) { other.m_ptr = nullptr; }

    ~Ref() {
        if (m_ptr) {
            T::decref(m_ptr);
        }
    }

    //! Copy a reference into this one.
    Ref &operator=(const Ref &other) {
        T *p = m_ptr, *q = other.m_ptr;
        if (q) {
            T::incref(q);
        }
        if (p) {
            T::decref(p);
        }
        m_ptr = q;
        return *this;
    }

    //! Move a reference into this one.
    Ref &operator=(Ref &&other) {
        std::swap(m_ptr, other.m_ptr);
        return *this;
    }

    //! Access the referenced object.
    T &operator*() const { return *m_ptr; }

    //! Access members of the referenced object.
    T *operator->() const { return m_ptr; }

    //! Test whether the reference is not null.
    explicit operator bool() const { return m_ptr != nullptr; }

    //! Get a pointer to the referenced object.
    T *get() const { return m_ptr; }

    //! Reset the reference to the null reference.
    void reset() {
        if (m_ptr) {
            T::decref(m_ptr);
            m_ptr = nullptr;
        }
    }

    //! Replace the reference with a reference to the given object.
    void reset(T *p) {
        if (p) {
            T::incref(p);
        }
        if (m_ptr) {
            T::decref(m_ptr);
        }
        m_ptr = p;
    }
};

}
#endif
