# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
import os.path as ospath
import os
from .error import UserError

_SRCTYPE_EXTS = {
    'c': 'c',
    'c++': 'cpp cp cxx',
    'h': 'h',
    'h++': 'hpp hxx',
    'objc': 'm',
    'objc++': 'mm',
    'rc': 'rc',
    'xib': 'xib',
}

EXT_SRCTYPE = {
    '.' + ext: type
    for type, exts in _SRCTYPE_EXTS.items()
    for ext in exts.split()
}

SRCTYPE_EXT = {
    type: '.' + exts.split()[0]
    for type, exts in _SRCTYPE_EXTS.items()
}

del _SRCTYPE_EXTS

def get_root(paths):
    s = '..' + ospath.sep
    next_prefix = s
    count = 0
    for path in paths:
        while path.startswith(next_prefix):
            next_prefix += s
            count += 1
    if not count:
        return '.' + ospath.sep
    return ospath.sep.join(os.getcwd().split(ospath.sep)[-count:] + [''])

def join_path(base, *paths):
    """Join relative POSIX paths to a native base path.

    The base path specifies the root directory, and if it does not end
    with a slash (or backslash on Windows), then the directory containing
    the base is considered the root directory.  This is the same way that
    relative URIs are joined together.
    """
    dirname, basename = ospath.split(base)
    result = dirname or '.'
    is_dir = True
    for path in paths:
        if not path or path == '.':
            is_dir = True
            continue
        if path.startswith('/'):
            raise ValueError('Path must be relative.')
        for part in path.split('/'):
            if part == '..':
                is_dir = True
                if result == '.':
                    result = '..'
                else:
                    dirname, basename = ospath.split(result)
                    if basename == '..':
                        result = ospath.join(result, '..')
                    else:
                        result = dirname or '.'
            elif part == '.' or not part:
                is_dir = True
            else:
                is_dir = False
                if result == '.':
                    result = part
                else:
                    result = ospath.join(result, part)
    assert result
    if is_dir:
        return ospath.join(result, '')
    return result

def merge_tags(base, tags):
    """Merge two lists of tags.

    The second list of tags can contain "negative" tags, which start
    with `!` and remove the given tag from the set of tags.

    Args:
      base: The base list of tags.
      tags: The tags to add and remove.
    """
    result = list(base)
    for tag in tags:
        if tag.startswith('!'):
            tag = tag[1:]
            if tag not in result:
                raise ValueError('No such tag: {!r}'.format(tag))
            result.remove(tag)
        else:
            result.append(tag)
    return result

class TagSourceFile(object):
    """A record of an individual source file."""
    __slots__ = ['path', 'tags', 'sourcetype']

    def __init__(self, path, tags, *, sourcetype=None):
        self.path = path
        self.tags = tuple(tags)
        if sourcetype is None:
            ext = ospath.splitext(path)[1]
            try:
                self.sourcetype = EXT_SRCTYPE[ext]
            except KeyError:
                raise ValueError(
                    'Cannot determine file type: {}'.format(path))
        else:
            self.sourcetype = sourcetype

    def __repr__(self):
        return ('TagSourceFile({!r}, {!r}, sourcetype={!r})'
                .format(self.path, self.tags, self.sourcetype))

class SourceList(object):
    """A collection of source files."""
    __slots__ = ['sources', 'tags', '_base_path', '_base_tags', '_base']

    def __init__(self, *, base=None, sources=None, tags=None):
        self.sources = []
        self.tags = set()
        self._base_path = base
        self._base_tags = ['public', 'private']
        if tags is not None:
            self._base_tags = merge_tags(self._base_tags, tags)
        if sources is not None:
            self.add(sources)

    def add(self, sources, *, base=None, path=None, tags=None):
        """Add sources to the source list.

        Files are specified as a single string, with one file per line.
        Each line starts with a relative path, which is followed by
        a list of tags, separated by spaces.

        Args:
          sources: The source files, a single string.
          base: The base path, overrides the object's base path.
          path: The path for source files, relative to the base.
          tags: A list of tags to apply to all files.
        """
        base_tags = self._base_tags
        if tags is not None:
            base_tags = merge_tags(base_tags, tags)
        if base is not None:
            base_path = ospath.join(base, '')
        else:
            base_path = self._base_path
            if base_path is None:
                raise ValueError('Must specify base path')
        if path is not None:
            base_path = join_path(base_path, path, '')
        for line in sources.splitlines():
            fields = line.split()
            if not fields:
                continue
            file_path = join_path(base_path, fields[0])
            file_tags = merge_tags(base_tags, fields[1:])
            self.sources.append(TagSourceFile(file_path, file_tags))

if __name__ == '__main__':
    TESTS = '''
    abc/ .. ./
    ./ abc abc
    ./ abc/ abc/
    abc/def ../.. ../
    abc/def/ ../.. ./
    abc/def/ghi/ ../jkl/ abc/def/jkl/
    '''
    for test in TESTS.splitlines():
        test = test.split()
        if not test:
            continue
        base = test[0].replace('/', ospath.sep)
        expected = test[-1].replace('/', ospath.sep)
        paths = test[1:-1]
        print('join_path({}) = {!r}'.format(
            ', '.join(repr(x) for x in [base] + paths), expected
        ))
        result = join_path(base, *paths)
        if result != expected:
            print('Error: Got {!r}'.format(result))
            raise SystemExit(1)
