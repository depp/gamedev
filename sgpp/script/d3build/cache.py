# Copyright 2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
import collections
import os

class BuildCache(object):
    """A set of cached values.

    Values can be cached for a single build script invocation, or they
    can be cached across multiple invocations and expire when certain
    files are modified.
    """
    __slots__ = ['_items']

    def __init__(self, data=None):
        self._items = {}
        self.load(data)

    def load(self, data=None):
        """Load a previously saved cache.

        This will invalidate items whose dependencies have changed.
        """
        if data is None:
            return
        items, timestamps1 = data
        timestamps2 = {}
        for key, value, deps in items:
            ndeps = {}
            for dep in deps:
                ts1 = timestamps1.get(dep)
                ts2 = timestamps2.get(dep)
                if ts2 is None:
                    try:
                        ts2 = os.path.getmtime(dep)
                    except FileNotFoundError:
                        ts2 = None
                    timestamps2[dep] = ts2
                if ts1 != ts2:
                    break
                ndeps[dep] = ts2
            else:
                self._items[key] = value, ndeps

    def save(self):
        """Save the cache to a single value which can be pickled."""
        timestamps = {}
        for key, (value, deps) in self._items.items():
            if deps is None:
                continue
            for dep, ts in deps.items():
                try:
                    if timestamps[dep] >= ts:
                        continue
                except KeyError:
                    pass
                timestamps[dep] = ts
        freshitems = []
        freshdeps = set()
        for key, (value, deps) in self._items.items():
            if deps is None:
                continue
            for dep, ts in deps.items():
                if timestamps[dep] > ts:
                    break
            else:
                freshitems.append((key, value, list(deps)))
                freshdeps.update(deps)
        return freshitems, {dep: timestamps[dep] for dep in freshdeps}

    def get(self, key):
        """Get a value from the build cache.

        If the value is in the cache, then the cached value is
        returned.  Otherwise, None is returned.
        """
        try:
            record = self._items[key]
        except KeyError:
            return None
        return record[0]

    def set(self, key, value, deps=None):
        """Set a value in the build cache.

        If deps is None or not specified, then the result is cached
        only for this invocation of the build script.  Otherwise, deps
        is a list of files, and the value is cached across invocations
        until one of the listed files is modified.
        """
        if deps is not None:
            if isinstance(deps, str):
                raise TypeError('invalid dependencies')
            ndeps = {dep: os.path.getmtime(dep) for dep in deps}
        else:
            ndeps = None
        self._items[key] = value, ndeps
        return value

def cached(*, name=None, deps=None):
    """Decorator for functions which are cached per build or persistently.

    The function's first argument must be the build object, and the
    function must not be given any keyword arguments.  If deps is None
    or not specified, then the value is only cached for a single
    execution of the build script.  Otherwise, deps is a list of file
    paths, and the value is cached across executions until 'make
    config' is run or one of the files specified changes.
    """
    def wrapper(function):
        if name is None:
            kname = function.__module__, function.__name__
        else:
            kname = name
        def wrapped(build, *args):
            key = 'func', kname, args
            cache = build.cache
            value = cache.get(key)
            if value is None:
                value = function(build, *args)
                cache.set(key, value, deps=deps)
            return value
        return wrapped
    return wrapper

def _test():
    import tempfile, time

    def touch(*paths):
        for path in paths:
            with open(path, 'wb') as fp:
                pass

    def check(**values):
        for k, v in values.items():
            vv = cache.get(k)
            if v is None:
                success = vv is None
            else:
                success = v == vv
            if not success:
                raise Exception('Test failed: {} should be {}, but was {}'
                                .format(k, v, vv))

    # Simple test (no dependencies)
    cache = BuildCache()
    cache.set('a', 10)
    cache.set('b', 11, [])
    check(a=10, b=11, c=None, d=None)
    data = cache.save()
    cache = BuildCache()
    cache.load(data)
    cache.set('c', 12)
    check(a=None, b=11, c=12, d=None)
    del cache, data

    # Dependency test
    with tempfile.TemporaryDirectory() as dirpath:
        p = [os.path.join(dirpath, str(n)) for n in range(5)]
        cache = BuildCache()

        # Values:
        # a: temporary, then changed
        # b: persistent
        # c: persistent, dependency not changed
        # d: persistent, dependency changed after save
        # e: persistent, dependency changed before save
        # f: helper for e
        # g: persistent, dependency deleted
        # h: persistent, overridden with temporary
        # i: two dependencies, neither changed
        # j: two dependencies, one changed

        # Files:
        # 0: not changed
        # 1: changed before save
        # 2: changed after save
        # 3: deleted
        # 4: not changed

        touch(p[0], p[1], p[2], p[3], p[4])
        cache.set('a', 0)
        cache.set('a', 1)
        cache.set('b', 2, [])
        cache.set('c', 3, [p[0]])
        cache.set('d', 4, [p[2]])
        cache.set('e', 5, [p[1]])
        cache.set('g', 6, [p[3]])
        cache.set('h', 7, [])
        cache.set('h', 8)
        cache.set('i', 9, [p[0], p[4]])
        cache.set('j', 10, [p[0], p[2]])

        time.sleep(2)
        touch(p[1])
        cache.set('f', 11, [p[1]])

        check(a=1, b=2, c=3, d=4, e=5,
              f=11, g=6, h=8, i=9, j=10)

        data = cache.save()
        time.sleep(2)
        touch(p[2])
        os.remove(p[3])
        cache = BuildCache()
        cache.load(data)

        check(a=None, b=2, c=3, d=None, e=None,
              f=11, g=None, h=None, i=9, j=None)

if __name__ == '__main__':
    _test()
