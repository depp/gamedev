# Copyright 2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
import os

class AtomicFile(object):
    """Context object for atomically writing to a file."""
    __slots__ = ['_tmppath', '_destpath', '_fp']

    def __init__(self, path, mode):
        if 'w' not in mode:
            raise ValueError('Mode must be writable')
        dirname, basename = os.path.split(path)
        if not basename:
            raise ValueError('Must specify file, not directory')
        self._tmppath = path + '.tmp'
        self._destpath = path
        self._fp = open(self._tmppath, mode)

    def __enter__(self):
        return self._fp

    def __exit__(self, type, value, traceback):
        self._fp.close()
        if type is None:
            os.replace(self._tmppath, self._destpath)
        else:
            os.remove(self._tmppath)
