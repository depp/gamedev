# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
from .error import ConfigError
from .file import AtomicFile
import sys
import os
import pickle

class PersistentStore(object):
    """Persistent storage."""
    __slots__ = ['_path', '_data']

    def __init__(self, path):
        self._path = path
        self._data = {}

    def load(self):
        """Load the persistent data."""
        try:
            fp = open(self._path, 'rb')
        except FileNotFoundError:
            self._data = {}
        else:
            with fp:
                self._data = pickle.load(fp)
            if not isinstance(self._data, dict):
                self._data = {}

    def save(self):
        """Save the persistent data."""
        with AtomicFile(self._path, 'wb') as fp:
            pickle.dump(self._data, fp,
                        protocol=pickle.HIGHEST_PROTOCOL)

    def get(self, key):
        """Get a persistent value."""
        if not isinstance(key, str):
            raise TypeError('key must be string')
        value = self._data.get(key)
        if value is None:
            return None
        return pickle.loads(value)

    def set(self, key, value):
        """Set a persistent value."""
        if not isinstance(key, str):
            raise TypeError('key must be string')
        self._data[key] = pickle.dumps(
            value, protocol=pickle.HIGHEST_PROTOCOL)

    def remove_prefix(self, prefix):
        """Remove all persistent values with the given prefix."""
        self._data = {k: v for k, v in self._data.items()
                             if not k.startswith(prefix)}

    def clear(self):
        """Remove all persistent data."""
        self._data = {}

class BuildScript(object):
    """A build system script.

    This is the top-level object which drives the whole build system.
    """
    __slots__ = [
        # Persistent data storage
        'storage'
    ]

    def __init__(self):
        self.storage = PersistentStore('config.dat')

    def run(self, argv=None):
        """Run the build script."""
        self.storage.load()
        if argv is None:
            argv = sys.argv
        script = argv[0]
        action = argv[1] if len(argv) >= 2 else None

        if action == '--action-regenerate':
            self._action_regenerate(argv[2:])
            return
        elif action == '--action-reconfigure':
            args = self.storage.get('args')
            if args is None:
                print('Error: cannot read configuration cache.',
                      file=sys.stderr)
                raise SystemExit(1)
        elif action == '--action-reconfigure-full':
            args = self.storage.get('args')
            self.storage.clear()
        else:
            args = argv[1:]
            self.storage.clear()

        from .action import run_action
        run_action(self, script, args)

    def _action_regenerate(self, args):
        """Regenerate a generated source file."""
        if len(args) != 1:
            print('Invalid arguments', file=sys.stderr)
            raise SystemExit(1)
        source = self.storage.get('source/' + args[0])
        source.regen()

    @property
    def options(self):
        """Get the build options."""
        return []

    @property
    def package_search_path(self):
        """Get the path for searching for external packages."""
        return None

    @property
    def sources(self):
        """Get a list of all source files."""
        return []

    @property
    def name(self):
        """Get the project name.

        This must be implemented by the subclass.
        """
        raise NotImplementedError()

    def adjust_config(self, cfg):
        """Adjust the configuration options."""
        pass

    def build(self, build):
        """Create the build targets.

        This must be implemented by the subclass.
        """
        raise NotImplementedError()
