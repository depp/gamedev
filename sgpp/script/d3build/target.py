# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
from .error import ConfigError
from .source import join_path
import os
import re

_DEP_FILE = re.compile(r'[-_A-Za-z0-9](?:[-_.A-Za-z0-9]*[-_A-Za-z0-9])$')
def _dep_file(x):
    """Test whether a file should be included as a build dependency."""
    if not _DEP_FILE.match(x):
        return False
    ext = os.path.splitext(x)[1]
    if ext in ('.pyc', '.pyo'):
        return False
    return True

def _dep_dir(x):
    """Test whether a directory should be included as a build dependency."""
    if not _DEP_FILE.match(x):
        return False
    if x == '__pycache__':
        return False
    return True

class BaseTarget(object):
    """Base class for all target buildsystems."""
    __slots__ = [
        # List of all generated sources.
        'generated_sources',
        # List of errors that occurred while creating targets.
        'errors',
        # List of build system dependencies.
        'dependencies',
    ]

    def __init__(self, build, name):
        self.generated_sources = []
        self.errors = []
        self.dependencies = [build.script]
        self.add_build_dependency_dir(join_path(__file__, '.'))

    def run_path(self, paths):
        """Convert a source path to a runtime path."""
        raise NotImplementedError('must be implemented by subclass')

    def _add_module(self, module):
        """Add a module's dependencies and errors to the target.

        Returns True if the module is clean, False if the module has
        errors.
        """
        for source in module.generated_sources:
            self.add_generated_source(source)
        for error in module.errors:
            if not any(x is error for x in self.errors):
                self.errors.append(error)
        return not module.errors

    def add_generated_source(self, source):
        """Add a generated source to the build system."""
        if not any(x is source for x in self.generated_sources):
            self.generated_sources.append(source)

    def add_default(self, target):
        """Set a target to be a default target."""

    def add_executable(self, *, name, module, uuid=None, arguments=[]):
        """Create an executable target.

        Returns the path to the executable.
        """
        raise ConfigError('this target does not support executables')

    def add_application_bundle(self, *, name, module, info_plist,
                               arguments=[]):
        """Create an OS X application bundle target.

        Returns the path to the application bundle.
        """
        raise ConfigError(
            'this target does not support application bundles')

    def add_build_dependency_dir(self, path):
        """Add a build system dependency directory.

        On supported targets, if a file in the dependency directory
        changes, the build script will be rebuilt.
        """
        for dirpath, dirnames, filenames in os.walk(path):
            dirnames[:] = [x for x in dirnames if _dep_dir(x)]
            self.dependencies.extend(
                os.path.join(dirpath, x) for x in filenames if _dep_file(x))

    def finalize(self):
        """Write the build system files."""
        remaining = set(source.target for source in self.generated_sources
                        if not source.is_regenerated_only)
        while remaining:
            advancing = False
            for source in self.generated_sources:
                if (source.target not in remaining or
                    remaining.intersection(source.dependencies)):
                    continue
                print('Creating {}...'.format(source.target))
                source.regen()
                advancing = True
                remaining.discard(source.target)
            if not advancing:
                raise ConfigError('circular dependency in generated sources')
