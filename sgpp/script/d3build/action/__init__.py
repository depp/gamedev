# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
import argparse
import importlib

def run_action(bscript, script, args):
    """Parse the command line arguments and produce a callable action."""
    p = argparse.ArgumentParser()

    p.add_argument('variables', nargs='*', metavar='VAR=VALUE',
                   help='build variables')
    p.add_argument(
        '-q', dest='verbosity', default=1,
        action='store_const', const=0,
        help='suppress messages')
    p.add_argument(
        '-v', dest='verbosity', default=1,
        action='store_const', const=2,
        help='verbose messages')
    p.add_argument(
        '--target',
        help='select a build target')
    p.set_defaults(action='configure')
    p.add_argument(
        '--scan',
        action='store_const', const='scan', dest='action',
        help='scan for extra or missing source files')

    optgroup = p.add_argument_group('features')
    for option in bscript.options:
        option.add_argument(optgroup)

    parsed_args = p.parse_args(args)
    mod = importlib.import_module(
        '{}.{}'.format(__package__, parsed_args.action))
    return mod.run_action(bscript, script, parsed_args, args)
