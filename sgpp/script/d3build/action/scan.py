# Copyright 2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
import os
import re

_DEP_FILE = re.compile(r'[-_A-Za-z0-9](?:[-_.A-Za-z0-9]*[-_A-Za-z0-9])$')

def run_action(bscript, script, args, raw_args):
    dirs = {}
    sources = bscript.sources
    for src in sources:
        dirname, basename = os.path.split(src.path)
        try:
            dirfiles = dirs[dirname]
        except KeyError:
            dirs[dirname] = {basename}
        else:
            dirfiles.add(basename)
    mismatch = []
    for dirname, dirfiles in dirs.items():
        actual = set(x for x in os.listdir(dirname)
                     if _DEP_FILE.match(x)
                     and os.path.isfile(os.path.join(dirname, x)))
        mismatch.extend(
            (os.path.join(dirname, x), True)
            for x in actual.difference(dirfiles))
        mismatch.extend(
            (os.path.join(dirname, x), False)
            for x in dirfiles.difference(actual))
    if not mismatch:
        print('No discrepancies found.')
        return
    mismatch.sort()
    print('Discrepancies found:')
    for path, status in mismatch:
        print('-+'[status], path)
    raise SystemExit(1)
