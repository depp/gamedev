# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
import platform
import sys
import re
import os
from ..cache import BuildCache
from ..error import UserError, ConfigError
from ..shell import escape, get_output
from ..util import yesno
from ..log import Log, logfile

PLATFORMS = {
    'Darwin': ('osx', 'xcode'),
    'Linux': ('linux', 'gnumake'),
    'Windows': ('windows', 'msvc'),
}

def run_action(bscript, script, args, raw_args):
    """Create the action from the command-line arguments."""
    bscript.storage.set('args', raw_args)
    config = Configuration(bscript, args)
    with Log('config.log', config.verbosity):
        print(
            'Arguments:',
            ' '.join(escape(x) for x in raw_args),
            file=logfile(2))
        bscript.adjust_config(config)
        config.dump(file=logfile(1))
        build = Build(bscript, script, config)
        bscript.build(build)
        if build.target.errors:
            log = logfile(0)
            print('Configuration failed.', file=log)
            for error in build.target.errors:
                print(file=log)
                for line in error.splitlines():
                    print(line, file=log)
            sys.exit(1)
        if build.variable_unused:
            print('Warning: unused variables:',
                  ' '.join(sorted(build.variable_unused)),
                  file=logfile(0))
        bscript.storage.remove_prefix('source/')
        for s in build.target.generated_sources:
            bscript.storage.set('source/' + s.target, s)
        bscript.storage.set('cache', build.cache.save())
        bscript.storage.save()
        build.target.finalize()

class Configuration(object):
    """Project configuration."""
    __slots__ = [
        # Output verbosity: 0, 1, or 2
        'verbosity',
        # List of variables specified on the configuration command line
        'variables',
        # The name of the target platform
        'platform',
        # The name of the target toolchain
        'target',
        # Target toolchain parameters
        'targetparams',
        # Dictionary mapping flag names to values
        'flags',
    ]

    def __init__(self, bscript, args):
        self.verbosity = args.verbosity
        self.variables = args.variables

        plat = platform.system()
        try:
            self.platform, target = PLATFORMS[plat]
        except KeyError:
            raise ConfigError('unknown platform: {}'.format(plat))
        if args.target is not None:
            target = args.target
        parts = target.split(':')
        self.targetparams = {}
        for part in parts[1:]:
            i = part.find('=')
            if i < 0:
                raise UserError(
                    'invalid target parameter: {!r}'.format(part))
            self.targetparams[part[:i]] = part[i+1:]
        self.target = parts[0]

        self.flags = {}
        for option in bscript.options:
            self.flags[option.name] = option.get_value(args)

    def dump(self, *, file=None):
        """Dump information about the configuration."""
        if file is None:
            file = sys.stdout
        print('Platform:', self.platform, file=file)
        print('Target:', ':'.join(
            [self.target] +
            sorted('{}={}'.format(param, value)
                   for param, value in self.targetparams.items())),
            file=file)
        print('Build variables:', file=file)
        for variable in self.variables:
            print('  {}'.format(variable), file=file)
        print('Flags:', file=file)
        for k, v in sorted(self.flags.items()):
            if isinstance(v, bool):
                v = yesno(v)
            print('  {}: {}'.format(k, v), file=file)

class Build(object):
    """An in-progress build script invocation."""
    __slots__ = [
        # The path to the configuration script.
        'script',
        # List of specifications for optional flags.
        'options',
        # The configuration options from the command line.
        'config',
        # Cached build data.
        'cache',
        # Path to bundled packages.
        'package_search_path',
        # Dictionary mapping {varname: value}.
        'variables',
        # List of all variables as (varname, value).
        'variable_list',
        # Set of unused variables.
        'variable_unused',
        # The build system target.
        'target',
    ]

    def __init__(self, bscript, script, config):
        self.script = script
        self.options = bscript.options
        self.config = config
        self.cache = BuildCache(bscript.storage.get('cache'))
        self.package_search_path = bscript.package_search_path
        self.variables = {}
        self.variable_list = []
        self.variable_unused = set()
        for vardef in config.variables:
            i = vardef.find('=')
            if i < 0:
                raise UserError(
                    'invalid variable syntax: {!r}'.format(vardef))
            varname = vardef[:i]
            value = vardef[i+1:]
            self.variables[varname] = value
            self.variable_list.append((varname, value))
            self.variable_unused.add(varname)

        if config.target == 'gnumake':
            from ..gnumake import target
            target = target.GnuMakeTarget
        elif config.target == 'xcode':
            from ..xcode import target
            target = target.XcodeTarget
        elif config.target == 'msvc':
            from ..msvc import target
            target = target.VisualStudioTarget
        else:
            raise ConfigError(
                'unknown target: {}'.format(config.target))
        self.target = target(self, bscript.name)
        self._read_extra_flags()

    def _read_extra_flags(self):
        """Parse extra flags specified on the command line."""
        base = self.target.base
        for key, value in self.variable_list:
            i = key.find(':')
            if i < 0:
                configs = None
                flagname = key
            else:
                configs = key[:i].split(',')
                flagname = key[i+1:]
            varnames = base.schema.flags.get(flagname)
            if varnames is None:
                continue
            variables = {}
            for varname in varnames:
                vardef = base.schema.get_variable(varname)
                varvalue = vardef.parse(value)
                variables[varname] = varvalue
            base.add_variables(variables, configs=configs)
            self.variable_unused.discard(key)

    def get_variable(self, name, default):
        """Get one of build variables."""
        value = self.variables.get(name, default)
        self.variable_unused.discard(name)
        return value

    def get_variable_bool(self, name, default):
        """Get one of the build variables as a boolean."""
        value = self.variables.get(name, None)
        if value is None:
            return default
        self.variable_unused.discard(name)
        uvalue = value.upper()
        if uvalue in ('YES', 'TRUE', 'ON', 1):
            return True
        if uvalue in ('NO', 'FALSE', 'OFF', 0):
            return False
        raise ConfigError('invalid value for {}: expecting boolean'
                          .format(name))

    def check_platform(self, platforms):
        """Throw an exception if the platform is not in the given set."""
        if isinstance(platforms, str):
            if self.config.platform == platforms:
                return
        else:
            if self.config.platform in platforms:
                return
            platforms = ', '.join(platforms)
        raise ConfigError('only valid on platforms: {}'.format(platforms))

    def find_package(self, pattern, *, varname=None):
        """Find a package matching the given pattern (a regular expression).

        Packages are directories stored in the package search path.  A
        target can use dependencies stored in the package search path
        without requiring the package to be installed on the
        developer's system or requiring the package to be integrated
        into the project repository.

        Returns the path to a directory in the package search path
        matching the given pattern.  If varname is not None, then it
        names a variable which can be used to specify the path to the
        given package, pre-empting the search process.
        """
        if varname is not None:
            value = self.get_variable(varname, None)
            if value is not None:
                return value
        if self.package_search_path is None:
            raise ConfigError('package_search_path is not set')
        try:
            filenames = os.listdir(self.package_search_path)
        except FileNotFoundError:
            raise ConfigError('package search path does not exist: {}'
                              .format(self.package_search_path))
        regex = re.compile(pattern)
        results = [fname for fname in filenames if regex.match(fname)]
        if not results:
            raise ConfigError('could not find package matching /{}/'
                              .format(pattern))
        if len(results) > 1:
            raise ConfigError('found multiple libraries matching /{}/: {}'
                              .format(pattern, ', '.join(results)))
        return os.path.join(self.package_search_path, results[0], '')

    def find_framework(self, name):
        """Find an OS X framework.

        Returns the framework's path.
        """
        if self.config.platform != 'osx':
            raise ConfigError('frameworks not available on this platform')
        try:
            home = os.environ['HOME']
        except KeyError:
            raise ConfigError('missing HOME environment variable')
        dir_paths = [
            os.path.join(home, 'Library/Frameworks'),
            '/Library/Frameworks',
            '/System/Library/Frameworks']
        framework_name = name + '.framework'
        for dir_path in dir_paths:
            try:
                fnames = os.listdir(dir_path)
            except FileNotFoundError:
                continue
            if framework_name in fnames:
                return os.path.join(dir_path, framework_name)
        raise ConfigError('could not find framework {}'
                          .format(framework_name))

    def external_builddir(self, name):
        """Get the build directory for an external package."""
        return os.path.join(self.package_search_path, 'build', name)

    def external_destdir(self):
        """Get the installation directory for external packages."""
        return os.path.join(self.package_search_path, 'build', 'products')

    def pkg_config(self, spec):
        """Return flags from the pkg-config tool."""
        cmdname = 'pkg-config'
        flags = {}
        for varname, arg in (('LIBS', '--libs'), ('CFLAGS', '--cflags')):
            stdout, stderr, retcode = get_output(
                [cmdname, '--silence-errors', arg, spec])
            if retcode:
                stdout, retcode = get_output(
                    [cmdname, '--print-errors', '--exists', spec],
                    combine_output=True)
                raise ConfigError(
                    '{} failed (spec: {})'.format(cmdname, spec),
                    details=stdout)
            flags[varname] = stdout.split()
        flags['CXXFLAGS'] = flags['CFLAGS']
        return flags

    def sdl_config(self, version):
        """Return flags from the sdl-config tool."""
        if version == 1:
            cmdname = 'sdl-config'
        elif version == 2:
            cmdname = 'sdl2-config'
        else:
            raise ValueError('unknown SDL version: {!r}'.format(version))
        flags = {}
        for varname, arg in (('LIBS', '--libs'), ('CFLAGS', '--cflags')):
            stdout, stderr, retcode = get_output([cmdname, arg])
            if retcode:
                raise ConfigError('{} failed'.format(cmdname), details=stderr)
            flags[varname] = stdout.split()
        flags['CXXFLAGS'] = flags['CFLAGS']
        return flags
