# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
from d3build.source import SourceList
from .path import sglib_path

######################################################################
# SGLib
######################################################################

src = SourceList(base=sglib_path())

src.add(path='include/sg', sources='''
atomic.h
audio_buffer.h
audio_file.h
binary.h
clock.h
cpu.h
cvar.h
defs.h
entry.h
error.h
event.h
file.h
hash.h
hashtable.h
key.h
keycode.h
log.h
mixer.h
net.h
opengl.h
pack.h
pixbuf.h
rand.h
record.h
shader.h
strbuf.h
thread.h
util.h
version.h
''')

src.add('include/config.h')

src.add(path='src/audio', sources='''
buffer.c
convert.c
file.c
ogg.c ogg
ogg.h ogg
opus.c ogg opus
resample.c
vorbis.c ogg vorbis
wav.c
writer.c
''')

src.add(path='src/core', sources='''
clock.c maybe_sdl
clock_impl.h
cvar.c
cvar_load.c
cvar_private.h
cvar_save.c
cvar_table.c
error.c
file_impl.h
file_load.c
file_posix.c posix
file_textwriter.c
file_win.c windows
keyid.c
keytable_evdev.c linux
keytable_mac.c osx
keytable_win.c windows
log.c
log_console.c
log_impl.h
log_network.c
log_test.c exclude
net.c
opengl.c
opengl_data.c exclude
opengl_debug.c
opengl_load.c
opengl_private.h
pack.c
path.c
path_filelist.c
path_fts.c posix
path_norm.c
path_posix.c posix
path_util.c
path_win.c windows
private.h
rand.c
shader.c
sys.c
timer.c
version.c
version_const.c
winutf8.c windows
''')

src.add(path='src/mixer', sources='''
channel.c
exacttime.c exclude
mixdown.c
mixer.c
mixer.h
queue.c
sound.c
sound.h
time.c
time.h
timeexact.c
system_alsa.c audio_alsa
system_coreaudio.c audio_coreaudio
system_directsound8.c audio_directsound
system_sdl.c audio_sdl
''')

src.add(path='src/pixbuf', sources='''
coregraphics.c image_coregraphics
image.c
libjpeg.c image_libjpeg
libpng.c image_libpng
pixbuf.c
premultiply_alpha.c
private.h
texture.c
wincodec.c image_wincodec
''')

src.add(path='src/record', tags=['video_recording'], sources='''
cmdargs.c
cmdargs.h
record.c !video_recording
record.h !video_recording
screenshot.c !video_recording
screenshot.h !video_recording
videoio.c
videoio.h
videoproc.c
videoproc.h
''')

src.add(path='src/util', sources='''
cpu.c
hash.c
hashtable.c
strbuf.c
thread_pthread.c posix
thread_windows.c windows
''')

src.add(path='src/core/osx', tags=['frontend_cocoa'], sources='''
GApplication.h
GApplication.m
GController.h
GController.m
GDisplay.h
GDisplay.m
GMain.m
GPrefix.h
GView.h
GView.m
GWindow.h
GWindow.m
MainMenu.xib exclude
''')

src.add(path='src/core/gtk', tags=['frontend_gtk'], sources='''
gtk.c
''')

src.add(path='src/core/sdl', tags=['frontend_sdl'], sources='''
event.c
main.c
platform.c
private.h
''')

src.add(path='src/core/windows', tags=['frontend_windows'], sources='''
windows.c
''')

src_sg = src.sources

######################################################################
# SGLib++
######################################################################

src = SourceList(base=sglib_path())

src.add(path='src/sgpp', sources='''
chunk.cpp
color.cpp
color_db16.cpp
color_db32.cpp
color_palette.cpp
file.cpp
image.cpp
log.cpp
mat.cpp
orientation.cpp
pack.cpp
quat.cpp
random.cpp
shader.cpp
''')

src.add(path='src/text', sources='''
batch.cpp
core.cpp
font.cpp
freetype.cpp freetype harfbuzz
layout.cpp
layout_builder.cpp harfbuzz
line_break.cpp
line_break_data.hpp
metrics.cpp
private.hpp
system.cpp
utf8.cpp
''')

src.add(path='include/sgpp', sources='''
array.hpp
box.hpp
chunk.hpp
color.hpp
error.hpp
file.hpp
image.hpp
log.hpp
mat.hpp
opengl.hpp
orientation.hpp
pack.hpp
quat.hpp
random.hpp
range.hpp
ref.hpp
shader.hpp
text.hpp
utf8.hpp
vec.hpp
''')

src_sgpp = src.sources

######################################################################

del src
