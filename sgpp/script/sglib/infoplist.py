# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
from d3build.generatedsource import GeneratedSource
from d3build.plist.xml import dump

class InfoPropertyList(GeneratedSource):
    __slots__ = ['target', 'value']

    def __init__(self, target, value):
        self.target = target
        self.value = value

    @property
    def is_binary(self):
        return True

    def write(self, fp):
        fp.write(dump(self.value))
