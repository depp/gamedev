# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
from d3build.source import SourceList, join_path
# Note: SourceList is for export
from d3build.build import BuildScript
from d3build.log import logfile
from . import options
from . import source
from .icon import Icon
from .path import sglib_path
# Icon is for export
import sys
import os

class App(BuildScript):
    __slots__ = [
        # Name of the project.
        'name',
        # SourceList of the project source code.
        '_sources',
        # Path to the project data files.
        'datapath',
        # Contact email address.
        'email',
        # Project home page URI.
        'uri',
        # Project copyright notice.
        # Should be "Copyright © YEAR Author".
        'copyright',
        # Project identifier reverse domain name notation, for OS X.
        'identifier',
        # Project UUID, for emitting Visual Studio projects (optional).
        'uuid',
        # Default flag values.
        'defaults',
        # Category in the Apple application store.
        'apple_category',
        # The application icon, an Icon instance.
        'icon',
        # OpenGL version
        'opengl_version',
        # OpenGL extensions
        'opengl_extensions',
    ]

    def __init__(self, *, name, sources, datapath,
                 email=None, uri=None, copyright=None,
                 identifier=None, uuid=None, defaults=None,
                 apple_category='public.app-category.games',
                 icon=None, opengl_version='2.0', opengl_extensions=[]):
        super(App, self).__init__()
        if isinstance(sources, SourceList):
            sources = sources.sources
        self.name = name
        self._sources = sources
        self.datapath = datapath
        self.email = email
        self.uri = uri
        self.copyright = copyright
        self.identifier = identifier
        self.uuid = uuid
        self.defaults = defaults
        self.apple_category = apple_category
        self.icon = icon
        self.opengl_version = opengl_version
        self.opengl_extensions = opengl_extensions

    @property
    def options(self):
        return options.flags

    @property
    def package_search_path(self):
        return sglib_path('lib')

    @property
    def sources(self):
        return self._sources + source.src_sg + source.src_sgpp

    def adjust_config(self, cfg):
        """Adjust the configuration."""
        options.adjust_config(cfg, self.defaults)

    def target_name(self, build):
        """Get the target name."""
        name = self.name
        if build.config.platform == 'linux':
            name = name.replace(' ', '_')
        return name

    def update_base(self, build):
        """Update the base build settings."""
        from . import base
        base.update_base(build)

    def default_args(self, build):
        """Get the default arguments for running the program."""
        run_path = build.target.run_path
        user_path = join_path('.', 'user/')
        cache_path = join_path(user_path, 'cache/')
        args = [
            ('path.data',   run_path(self.datapath)),
            ('path.user',   run_path(user_path)),
            ('path.config', run_path(user_path)),
            ('path.cache',  run_path(cache_path)),
            ('developer',   'yes'),
        ]
        if build.config.platform == 'windows':
            args.append(('log.winconsole', 'yes'))
        return ['{}={}'.format(*arg) for arg in args]

    def module(self, build):
        """Create the main game module."""
        from . import module
        mod = build.target.module()
        deps = [module.module_sg(build)]
        if any(src.sourcetype == 'c++' for src in self._sources):
            deps.append(module.module_sgpp(build))
        mod.add_sources(self._sources, {'public': deps})
        return mod

    def _main_nib(self, build):
        """Get the path to the main nib file.

        Returns None if no main nib file is included.
        """
        if build.config.flags['frontend'] == 'cocoa':
            return sglib_path('resources/MainMenu.xib')
        return None

    def info_plist(self, build):
        from . import version
        ver = version.get_info('git', join_path(build.script))

        if self.copyright is not None:
            getinfo = '{}, {}'.format(ver.desc, self.copyright)
        else:
            getinfo = ver.desc

        plist = {
            'CFBundleDevelopmentRegion': 'English',
            'CFBundleExecutable': '$(EXECUTABLE_NAME)',
            'CFBundleGetInfoString': getinfo,
            # CFBundleName
            'CFBundleIconFile': self.icon.name(build),
            'CFBundleIdentifier': self.identifier,
            'CFBundleInfoDictionaryVersion': '6.0',
            'CFBundlePackageType': 'APPL',
            'CFBundleShortVersionString': ver.desc,
            'CFBundleSignature': '????',
            'CFBundleVersion': '{}.{}.{}'.format(*ver.number),
            'LSApplicationCategoryType': self.apple_category,
            # LSArchicecturePriority
            # LSFileQuarantineEnabled
            'LSMinimumSystemVersion': '10.7.0',
            'NSMainNibFile': sglib_path('resources/MainMenu.xib'),
            'NSPrincipalClass': 'GApplication',
            # NSSupportsAutomaticTermination
            # NSSupportsSuddenTermination
        }
        return {k: v for k, v in plist.items() if v is not None}

    def build(self, build):
        """Create the project targets."""
        build.target.add_build_dependency_dir(join_path(__file__, '.'))
        from .version import VersionInfo
        from d3build.generatedsource.configheader import ConfigHeader
        from d3build.module import Module
        from d3build.error import UserError

        self.update_base(build)
        name = self.target_name(build)
        args = self.default_args(build)
        mod = self.module(build)

        icon = self.icon
        if icon is not None:
            icon = icon.module(build)
            if icon is not None:
                mod.add_module(icon)
        del icon

        mod.add_generated_source(
            ConfigHeader(sglib_path('include/config.h'), build))
        mod.add_generated_source(
            VersionInfo(
                sglib_path('src/core/version_const.c'),
                join_path(build.script),
                sglib_path(),
                'git'))

        self._add_opengl(build, mod)

        if build.config.platform == 'osx':
            from d3build.generatedsource.template import TemplateFile
            if build.config.flags['frontend'] == 'cocoa':
                main_nib = sglib_path('resources/MainMenu.xib')
                mod.add_generated_source(
                    TemplateFile(
                        main_nib,
                        sglib_path('src/core/osx/MainMenu.xib'),
                        {'EXE_NAME': self.name}))
            else:
                main_nib = None
            from .infoplist import InfoPropertyList
            info_plist = sglib_path('resources/Info.plist')
            mod.add_generated_source(
                InfoPropertyList(
                    info_plist,
                    self.info_plist(build)))
            build.target.add_application_bundle(
                name=name,
                module=mod,
                info_plist=info_plist,
                arguments=args)
        else:
            target = build.target.add_executable(
                name=name,
                module=mod,
                uuid=self.uuid,
                arguments=args)

        if build.config.platform == 'linux':
            from .runscript import RunScript
            default = build.get_variable('DEFAULT', 'Release')
            build.target.add_default(default)
            for variant, path in target.items():
                scriptname = '{}_{}'.format(name, variant)
                build.target.add_generated_source(
                    RunScript(scriptname, scriptname, path, args))
                build.target.add_alias(variant, [scriptname])

    def _add_opengl(self, build, mod):
        value = build.cache.get('sglib.opengl')
        spec = self.opengl_version, sorted(self.opengl_extensions)
        if value is not None and value[0] == spec:
            deps, sources = value[1:]
        else:
            deps, sources = self._add_opengl2(build, mod)
            build.cache.set('sglib.opengl', (spec, deps, sources),
                            deps + [__file__])
        build.target.dependencies.extend(deps)
        mod.add_sources(sources, {'base': [build.target.base]})

    def _add_opengl2(self, build, mod):
        from d3build.source import TagSourceFile
        import glgen
        glreg_file = os.path.join(os.path.dirname(__file__), 'gl.xml')
        glreg = glgen.Registry.load(None, 'gl', 'core')
        deps, files = glreg.get_data(
            max_version=glgen.parse_version(self.opengl_version),
            extensions=self.opengl_extensions,
            platform=build.config.platform)
        sources = []
        files_h = {}
        path_h = sglib_path('include/sggl')
        files_c = {}
        path_c = sglib_path('src/core')
        tags = 'base',
        for name, value in files.items():
            if name.endswith('.h'):
                files_h[name] = value
                fpath = os.path.join(path_h, name)
            else:
                files_c[name] = value
                fpath = os.path.join(path_c, name)
            sources.append(TagSourceFile(fpath, tags))
        write_files(path_h, files_h, delete_missing=True)
        write_files(path_c, files_c)
        return deps, sources

def write_files(base, files, *, delete_missing=False):
    """Write a set of files to disk.

    Base is the base path.  The files are a dictionary mapping
    filenames to bytes.  If delete_missing is true, any file not
    specified in the given directory will be deleted.
    """
    log = logfile(1)
    if not os.path.isdir(base):
        os.makedirs(base)
    existing = set(os.listdir(base))
    for name, value in files.items():
        path = os.path.join(base, name)
        if name in existing:
            with open(path, 'rb') as fp:
                if fp.read() == value:
                    continue
        print('Creating {}...'.format(path), file=log)
        with open(path, 'wb') as fp:
            fp.write(value)
    if delete_missing:
        for name in existing.difference(files):
            path = os.path.join(base, name)
            if not os.path.isfile(path):
                continue
            print('Deleting {}...'.format(path), file=log)
            os.unlink(path)
