/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

#include "sg/text.hpp"

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

using namespace SG::Text;

void compare_valid(const char *name, bool expected, bool output) {
    if (expected != output) {
        std::cerr << "Test " << name << ": FAIL\n";
        std::cerr << "    Expected valid: " << expected << '\n';
        std::cerr << "    Valid: " << output << '\n';
        std::exit(1);
    }
}

template <class C1, class C2>
void compare_seq(const char *name, const C1 &expected, const C2 &output) {
    auto p1 = std::begin(expected), e1 = std::end(expected);
    auto p2 = std::begin(output), e2 = std::end(output);
    int tline = 1;
    for (std::size_t off = 0; p1 != e1 && p2 != e2; p1++, p2++, off++) {
        unsigned x = *p1, y = *p2;
        if (x != y) {
            std::cerr << "Test " << name << ": FAIL\n";
            std::cerr << "    Offset: " << off << '\n';
            std::cerr << "    Data line: " << tline << '\n';
            std::cerr << "    Expected: " << x << '\n';
            std::cerr << "    Output: " << y << '\n';
            std::exit(1);
        }
        if (x == '\n') {
            tline++;
        }
    }
    if (p1 != e1 || p2 != e2) {
        std::cerr << "Test " << name << ": FAIL\n";
        std::cerr << "    Length mismatch\n";
        std::exit(1);
    }
}

void test_utf8(const char *name, const std::string &input,
               const std::u32string &expected_output, bool expected_valid) {
    std::vector<char32_t> output;
    bool valid = utf8_decode(output, &*std::begin(input),
                             std::end(input) - std::begin(input));
    compare_valid(name, expected_valid, valid);
    compare_seq(name, expected_output, output);
}

void test_from_wstring(const char *name, const std::u16string &input,
                       const std::string &expected_output,
                       bool expected_valid) {
    std::wstring winput;
    for (auto c : input) {
        winput.push_back(c);
    }
    std::string output;
    bool valid = from_wstring(output, winput);
    compare_valid(name, expected_valid, valid);
    compare_seq(name, expected_output, output);
}

void test_to_wstring(const char *name, const std::string &input,
                     const std::u16string &expected_output,
                     bool expected_valid) {
    std::wstring output;
    bool valid = to_wstring(output, &*std::begin(input),
                            std::end(input) - std::begin(input));
    compare_valid(name, expected_valid, valid);
    compare_seq(name, expected_output, output);
}

const char INVALID_UTF8_IN[] =
    // Unexpected continuation characters
    "8081828384858687\n"
    "88898A8B8C8D8E8F\n"
    "9091929394959697\n"
    "98999A9B9C9D9E9F\n"
    "A0A1A2A3A4A5A6A7\n"
    "A8A9AAABACADAEAF\n"
    "B0B1B2B3B4B5B6B7\n"
    "B8B9BABBBCBDBEBF\n"
    // Lonely start characters for two-char sequences
    "C0 C1 C2 C3 C4 C5 C6 C7\n"
    "C8 C9 CA CB CC CD CE CF\n"
    "D0 D1 D2 D3 D4 D5 D6 D7\n"
    "D8 D9 DA DB DC DD DE DF\n"
    // Lonely start characters for three-char sequences
    "E0 E1 E2 E3 E4 E5 E6 E7\n"
    "E8 E9 EA EB EC ED EE EF\n"
    // Lonely start characters for four-char sequences
    "F0 F1 F2 F3 F4 F5 F6 F7\n"
    // Bytes that are always invalid
    "F8 F9 FA FB FC FD FE FF\n"
    "FEFF FFFE FFFF FEFE\n"
    // Incomplete sequences
    "C2 DF E0A0 ED9F EE80 EFBF F09080 F48FBF\n"
    "C2DFE0A0ED9FEE80EFBFF09080F48FBF\n"
    // Overlong sequences
    "C080 C1BF\n"
    "E08080 E09FBF\n"
    "F0808080 F08FBFBF\n"
    // Single surrogates
    "EDA080 EDAFBF EDB080 EDBFBF\n"
    // Surrogate pairs
    "EDA080EDB080 EDA0B4EDB487\n";

const char INVALID_UTF8_OUT[] =
    // Unexpected continuation characters
    "xxxxxxxx\n"
    "xxxxxxxx\n"
    "xxxxxxxx\n"
    "xxxxxxxx\n"
    "xxxxxxxx\n"
    "xxxxxxxx\n"
    "xxxxxxxx\n"
    "xxxxxxxx\n"
    // Lonely start characters for two-char sequences
    "x x x x x x x x\n"
    "x x x x x x x x\n"
    "x x x x x x x x\n"
    "x x x x x x x x\n"
    // Lonely start characters for three-char sequences
    "x x x x x x x x\n"
    "x x x x x x x x\n"
    // Lonely start characters for four-char sequences
    "x x x x x x x x\n"
    // Bytes that are always invalid
    "x x x x x x x x\n"
    "xx xx xx xx\n"
    // Incomplete sequences
    "x x x x x x x x\n"
    "xxxxxxxx\n"
    // Overlong sequences
    "x x\n"
    "x x\n"
    "x x\n"
    // Single surrogates
    "x x x x\n"
    // Surrogate pairs
    "xx xx\n";

const char16_t INVALID_UTF16_IN[] = {
    0xd800, 32, 0xdfff, 32, 0xd800
};

const unsigned char INVALID_UTF16_OUT[] = {
    239, 191, 189, 32, 239, 191, 189, 32, 239, 191, 189
};

unsigned read_hex(const char *p, int n) {
    unsigned x = 0;
    for (int i = 0; i < n; i++) {
        unsigned d = static_cast<unsigned char>(p[i]);
        if (d >= 'A' && d <= 'F') {
            d = (d - 'A') + 10;
        } else if (d >= 'a' && d <= 'f') {
            d = (d - 'a') + 10;
        } else if (d >= '0' && d <= '9') {
            d = d - '0';
        } else {
            std::fputs("Invalid hex\n", stderr);
            std::exit(1);
        }
        x = (x << 4) | d;
    }
    return x;
}

int main() {
    {
        static const unsigned char TEST_U8[] = {
            0, 32, 127, 10,
            194, 128, 32, 223, 191, 10,
            224, 160, 128, 32, 237, 159, 191, 32,
            238, 128, 128, 32, 239, 191, 191, 10,
            240, 144, 128, 128, 32, 244, 143, 191, 191, 10
        };
        std::string u8 = "Unicode text\n";
        u8.append(reinterpret_cast<const char *>(TEST_U8), sizeof(TEST_U8));

        static const char16_t TEST_U16[] = {
            0, 32, 127, 10,
            128, 32, 2047, 10,
            2048, 32, 55295, 32, 57344, 32, 65535, 10,
            55296, 56320, 32, 56319, 57343, 10
        };
        std::u16string u16 = u"Unicode text\n";
        u16.append(std::begin(TEST_U16), std::end(TEST_U16));

        static const char32_t TEST_U32[] = {
            0x00, ' ', 0x7F, '\n',
            0x80, ' ', 0x7FF, '\n',
            0x800, ' ', 0xD7FF, ' ', 0xE000, ' ', 0xFFFF, '\n',
            0x10000, ' ', 0x10FFFF, '\n'
        };
        std::u32string u32 = U"Unicode text\n";
        u32.append(std::begin(TEST_U32), std::end(TEST_U32));

        test_utf8("valid 8-32", u8, u32, true);
        test_from_wstring("valid 16-8", u16, u8, true);
        test_to_wstring("valid 8-16", u8, u16, true);
    }

    {
        std::string u8;
        for (const char *p = std::begin(INVALID_UTF8_IN),
                        *e = std::end(INVALID_UTF8_IN);
             p != e;) {
            if (*p <= 0x32) {
                u8.push_back(*p);
                p++;
            } else {
                u8.push_back(read_hex(p, 2));
                p += 2;
            }
        }

        std::u16string u16;
        std::u32string u32;
        for (const char *p = std::begin(INVALID_UTF8_OUT),
                 *e = std::end(INVALID_UTF8_OUT);
             p != e; p++) {
            unsigned cp;
            if (*p == 'x') {
                cp = 0xFFFD;
            } else {
                cp = *p;
            }
            u16.push_back(cp);
            u32.push_back(cp);
        }

        test_utf8("invalid A 8-32", u8, u32, false);
        test_to_wstring("invalid A 8-16", u8, u16, false);
    }

    {
        std::u16string u16(std::begin(INVALID_UTF16_IN),
                           std::end(INVALID_UTF16_IN));
        std::string u8(reinterpret_cast<const char *>(INVALID_UTF16_OUT),
                       sizeof(INVALID_UTF16_OUT));

        test_from_wstring("invalid B 16-8", u16, u8, false);
    }

    return 0;
}
