/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

#include "sg/range.hpp"

#include <cassert>

using namespace SG;

void die(const char *why) {
    std::fprintf(stderr, "Error: %s\n", why);
    std::exit(1);
}

void check(const ArrayRange<int> &r) {
    int i;
    if (r.size() != 4) {
        die("Incorrect size");
    }
    for (i = 0; i < 4; i++) {
        if (r[i] != i) {
            die("Incorrect value");
        }
    }
    i = 0;
    for (int j : r) {
        if (j != i) {
            die("Incorrect value");
        }
        i++;
    }
    if (i != 4) {
        die("Incorrect size");
    }
}

int main() {
    int arr1[4] = {0, 1, 2, 3};
    std::vector<int> vec(std::begin(arr1), std::end(arr1));
    std::array<int, 4> arr2 = {{0, 1, 2, 3}};

    check(arr1);
    check(vec);
    check(arr2);

    return 0;
}
