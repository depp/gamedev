/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sg/file.h"
#include "src/core/file_impl.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct sg_path g_path[SG_PATH_MAXCOUNT];
int g_pathcount;

SG_ATTR_NORETURN
static void
usage(void)
{
    fputs(
        "Usage:\n"
        "    filelist list EXTENSIONS PATH SEARCHPATH...\n",
        stderr);
    exit(1);
}

SG_ATTR_NORETURN
static void
die(const char *msg)
{
    fprintf(stderr, "Error: %s\n", msg);
    exit(1);
}

int
sg_path_get(
    struct sg_path *search,
    unsigned flags,
    struct sg_error **err)
{
    (void) flags;
    (void) err;
    memcpy(search, g_path, sizeof(*search) * g_pathcount);
    return g_pathcount;
}

struct sg_path
sg_path_get_writable(
    unsigned flags,
    struct sg_error **err)
{
    struct sg_path path;
    (void) flags;
    (void) err;
    abort();
    path.path = NULL;
    path.len = 0;
    return path;
}

void
sg_path_initabort(
    const char *msg)
{
    (void) msg;
    abort();
}

void
sg_path_add(
    const pchar *path,
    size_t pathsize,
    unsigned read_flags,
    unsigned write_flags)
{
    (void) path;
    (void) pathsize;
    (void) read_flags;
    (void) write_flags;
    abort();
}

static void
set_paths(int argc, char **argv)
{
    int i;
    char *path;
    size_t len;
    if (argc > SG_PATH_MAXCOUNT)
        die("too many search paths.");
    if (!argc)
        die("no search paths.");
    for (i = 0; i < argc; i++) {
        path = argv[i];
        len = strlen(path);
        if (!len)
            die("empty search path.");
        if (path[len-1] != '/') {
            char *npath = malloc(len + 2);
            if (!npath)
                die("out of memory.");
            memcpy(npath, path, len);
            npath[len] = '/';
            npath[len+1] = '\0';
            path = npath;
            len++;
        }
        g_path[i].path = path;
        g_path[i].len = len;
    }
    g_pathcount = argc;
}

static void
cmd_list(int argc, char **argv)
{
    const char *extensions;
    struct sg_filelist list;
    int r, i;

    if (argc < 2)
        usage();
    extensions = argv[0];
    if (!strcmp(extensions, "-"))
        extensions = NULL;
    set_paths(argc - 2, argv + 2);
    r = sg_filelist_scan(
        &list,
        argv[1],
        strlen(argv[1]),
        SG_FILELIST_RECURSIVE,
        extensions,
        NULL);
    if (r)
        exit(1);
    for (i = 0; i < list.count; i++)
        puts(list.files[i].path);
}

int
main(int argc, char **argv)
{
    if (argc < 2)
        usage();

    if (!strcmp(argv[1], "list")) {
        cmd_list(argc - 2, argv + 2);
        return 0;
    }

    usage();
    return 0;
}
