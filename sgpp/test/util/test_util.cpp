/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sg/util.hpp"

#include <limits>
#include <cassert>
#include <cstdio>
#include <cstdlib>

using namespace SG;

void assert_eq_16(int lineno, int x, int y) {
    if (x != y) {
        std::fprintf(stderr, "Line %d: expected 0x%04x, got 0x%04x\n", lineno,
                     x, y);
        std::exit(1);
    }
}

void assert_eq_32(int lineno, unsigned x, unsigned y) {
    if (x != y) {
        std::fprintf(stderr, "Line %d: expected 0x%08x, got 0x%08x\n", lineno,
                     x, y);
        std::exit(1);
    }
}

void assert_eq_64(int lineno, unsigned long long x, unsigned long long y) {
    if (x != y) {
        std::fprintf(stderr, "Line %d: expected 0x%016llx, got 0x%016llx\n",
                     lineno, x, y);
        std::exit(1);
    }
}

#define eq16(x, y) assert_eq_16(__LINE__, x, y)
#define eq32(x, y) assert_eq_32(__LINE__, x, y)
#define eq64(x, y) assert_eq_64(__LINE__, x, y)

void test_swap16() {
    eq16(swap16(0x1234), 0x3412);
    eq16(swap16(0xffff), 0xffff);
    eq16(swap16(0x0000), 0x0000);

    union {
        unsigned char uc[2];
        unsigned short us;
    } x;
    x.uc[0] = 0xFE;
    x.uc[1] = 0x12;
    eq16(swapbe16(x.us), 0xFE12);
    eq16(swaple16(x.us), 0x12FE);
}

void test_swap32() {
    eq32(swap32(0x12345678), 0x78563412);
    eq32(swap32(0xFFFF0000), 0x0000FFFF);
    eq32(swap32(0x0000FFFF), 0xFFFF0000);

    union {
        unsigned char uc[4];
        unsigned ui;
    } x;
    x.uc[0] = 0xFE;
    x.uc[1] = 0xDC;
    x.uc[2] = 0x12;
    x.uc[3] = 0x34;
    eq32(swapbe32(x.ui), 0xFEDC1234);
    eq32(swaple32(x.ui), 0x3412DCFE);
}

void test_swap64() {
    eq64(swap64(0x0123456789ABCDEF), 0xEFCDAB8967452301ull);
    eq64(swap64(0xFFFFFFFF00000000), 0x00000000FFFFFFFFull);
    eq64(swap64(0x00000000FFFFFFFF), 0xFFFFFFFF00000000ull);

    union {
        unsigned char uc[8];
        unsigned long long ull;
    } x;
    x.uc[0] = 0xFE;
    x.uc[1] = 0xDC;
    x.uc[2] = 0xBA;
    x.uc[3] = 0x98;
    x.uc[4] = 0x76;
    x.uc[5] = 0x54;
    x.uc[6] = 0x32;
    x.uc[7] = 0x10;
    eq64(swapbe64(x.ull), 0xFEDCBA9876543210ull);
    eq64(swaple64(x.ull), 0x1032547698BADCFEull);
}

void test_rot32() {
    unsigned x = 0xadf85459, y = x;
    for (int i = 0; i < 32; i++) {
        eq32(rotr32(x, i), y);
        eq32(rotl32(y, i), x);
        eq32(rotr32(~x, i), ~y);
        eq32(rotl32(~y, i), ~x);
        y = (y << 31) | (y >> 1);
    }
}

template<class T>
void test_round_up_pow2() {
    assert(round_up_pow2(static_cast<T>(0)) == 0);
    for (T i = 1; i < 258; i++) {
        T r = round_up_pow2(i);
        assert((r & (r - 1)) == 0);
        assert(r >= i);
        assert((r >> 1) < i);
    }
    for (T i = 256; i; i <<= 1) {
        assert(round_up_pow2(i - 1) == i);
        assert(round_up_pow2(i) == i);
        if ((i << 1) != 0) {
            assert(round_up_pow2(i + 1) == (i << 1));
        }
    }
    assert(round_up_pow2(std::numeric_limits<T>::max()) == 0);
}

int main() {
    test_swap16();
    test_swap32();
    test_swap64();
    test_rot32();
    test_round_up_pow2<std::size_t>();
    test_round_up_pow2<unsigned>();
    return 0;
}
