/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

/* Dump platform definitions to stdout.  */

#include "sg/defs.hpp"

#include <cstdio>
#include <cstring>

#define S(x) #x
#define M(x) { #x, S(x) }
#define F(x) { "", x }

const char *const MACROS[][2] = {
    F("byteorder"),
    M(SG_BIG_ENDIAN),
    M(SG_LITTLE_ENDIAN),
    M(SG_BYTE_ORDER),

    F("attribute"),
    M(SG_ATTR_ARTIFICIAL),
    M(SG_ATTR_CONST),
    M(SG_ATTR_DEPRECATED),
    M(SG_ATTR_FORMAT(a,b,c)),
    M(SG_ATTR_MALLOC),
    M(SG_ATTR_NOINLINE),
    M(SG_ATTR_NONNULL((x))),
    M(SG_ATTR_NORETURN),
    M(SG_ATTR_NOTHROW),
    M(SG_ATTR_PURE),
    M(SG_ATTR_SENTINEL),
    M(SG_ATTR_WARN_UNUSED_RESULT),

    F("cpu"),
    M(SG_CPU_X64),
    M(SG_CPU_X86),
    M(SG_CPU_PPC64),
    M(SG_CPU_PPC),
    M(SG_CPU_ARM)
};

int main() {
    for (const auto &m : MACROS) {
        if (!*m[0]) {
            std::printf("\n// %s\n", m[1]);
        } else if (!std::strcmp(m[0], m[1])) {
            std::printf("#undef  %s\n", m[0]);
        } else if (*m[1]) {
            std::printf("#define %s %s\n", m[0], m[1]);
        } else {
            std::printf("#define %s\n", m[0]);
        }
    }
    return 0;
}
