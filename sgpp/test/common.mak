LIBS :=
LDFLAGS :=
CFLAGS := -O0 -g
CXXFLAGS := -O0 -g
CWARN := -Werror -Wall -Wextra -Wpointer-arith -Wwrite-strings -Wmissing-prototypes
CXXWARN := -Werror -Wall -Wextra -Wpointer-arith

UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Linux)
ASAN := 1
endif

ifeq ($(UNAME_S),Darwin)
override LIBS += -framework ApplicationServices
endif

ifdef ASAN
override CFLAGS += -fsanitize=address
override CXXFLAGS += -fsanitize=address
override LDFLAGS += -fsanitize=address
endif

%.o: %.c
	$(CC) -MF $*.d -MMD -MP -I../.. -I../../include $(CWARN) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<
%.o: %.cpp
	$(CXX) -MF $*.d -MMD -MP -I../.. -I../../include -std=c++11 $(CXXWARN) $(CXXFLAGS) $(CPPFLAGS) -c -o $@ $<

-include *.d

link = $(CC) $(LDFLAGS) -o $@ $^ $(LIBS)
link_cxx = $(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)
