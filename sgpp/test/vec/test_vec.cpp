/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

#include "sg/box.hpp"
#include "sg/color.hpp"
#include "sg/mat.hpp"
#include "sg/quat.hpp"
#include "sg/vec.hpp"

#include <iostream>
#include <type_traits>

using namespace SG;

template <typename T, int N>
void print_vec(const GVec<T, N> &v) {
    std::cerr << '<';
    for (int i = 0; i < N; i++) {
        if (i) {
            std::cerr << ' ';
        }
        std::cerr << v[i];
    }
    std::cerr << '>';
}

template <typename T>
bool auto_eq(T x, T y) {
    if (std::is_floating_point<T>::value) {
        T epsilon = static_cast<T>(2e-6);
        return x >= y - epsilon && x <= y + epsilon;
    } else {
        return x == y;
    }
}

template <typename T>
void scalar_eq(int lineno, T x, T y) {
    if (!auto_eq<T>(x, y)) {
        std::cerr << "Line " << lineno << ": FAIL\n";
        std::cerr << "Expected: " << x << '\n';
        std::cerr << "Output: " << y << '\n';
        std::exit(1);
    }
}

struct TestType {
    int lineno;
    TestType(int lineno) : lineno(lineno){};

    void eq(float x, float y) { scalar_eq(lineno, x, y); }

    void eq(int x, int y) { scalar_eq(lineno, x, y); }

    template <typename T, int N>
    void eq(const GVec<T, N> &x, const GVec<T, N> &y) {
        bool is_eq = true;
        for (int i = 0; i < N; i++) {
            if (!auto_eq<T>(x[i], y[i])) {
                is_eq = false;
            }
        }
        if (!is_eq) {
            std::cerr << "Line " << lineno << ": FAIL\n";
            std::cerr << "Expected: ";
            print_vec(x);
            std::cerr << '\n';
            std::cerr << "Output: ";
            print_vec(y);
            std::cerr << '\n';
            std::exit(1);
        }
    }

    template <typename T, int N>
    void eq(const GBox<T, N> &x, const GBox<T, N> &y) {
        eq(x.mins, y.mins);
        eq(x.maxs, y.maxs);
    }

    void operator()(bool x) {
        if (!x) {
            std::cerr << "Line " << lineno << ": FAIL\n";
            std::exit(1);
        }
    }
};

#define test TestType(__LINE__)

template<typename T, int N>
void test_gvec() {
    typedef GVec<T, N> gvec;
    gvec p1, p10, p11, n1;
    T scale;
    if (std::is_floating_point<T>::value) {
        scale = static_cast<T>(0.01);
    } else {
        scale = 1;
    }
    for (int i = 0; i < N; i++) {
        T x = static_cast<T>(i+1)*scale;
        p1[i] = x;
        p10[i] = x*10;
        p11[i] = x*11;
        n1[i] = x*-1;
    }
    test(p1 == p1);
    test(!(p1 != p1));
    for (int i = 0; i < N; i++) {
        gvec y = p1;
        y[i] = -1;
        test(p1 != y);
        test(!(p1 == y));
    }
    test.eq(p1, +p1);
    test.eq(n1, -p1);
    test.eq(p11, p10 + p1);
    test.eq(p10, p11 - p1);
    test.eq(p10, p1 * static_cast<T>(10));
    test.eq(p10, static_cast<T>(10) * p1);
    {
        gvec y = p1;
        y *= static_cast<T>(2);
        y -= p1;
        y += p10;
        test.eq(p11, y);
    }
}

int main() {
    test_gvec<float, 2>();
    test_gvec<float, 3>();
    test_gvec<float, 4>();
    test_gvec<int, 2>();
    test_gvec<int, 3>();
    test_gvec<int, 4>();
    test_gvec<short, 2>();
    test_gvec<short, 3>();
    test_gvec<short, 4>();

    // casting
    test.eq(ivec4{{1, 2, 3, 4}}, static_cast<ivec4>(ivec4{{1, 2, 3, 4}}));
    test.eq(ivec4{{1, 2, 3, 4}}, static_cast<ivec4>(svec4{{1, 2, 3, 4}}));
    test.eq(ivec4{{1, 2, 3, 4}}, static_cast<ivec4>(vec4{{1, 2, 3, 4}}));

    test.eq(svec4{{1, 2, 3, 4}}, static_cast<svec4>(ivec4{{1, 2, 3, 4}}));
    test.eq(svec4{{1, 2, 3, 4}}, static_cast<svec4>(svec4{{1, 2, 3, 4}}));
    test.eq(svec4{{1, 2, 3, 4}}, static_cast<svec4>(vec4{{1, 2, 3, 4}}));

    test.eq(vec4{{1, 2, 3, 4}}, static_cast<vec4>(ivec4{{1, 2, 3, 4}}));
    test.eq(vec4{{1, 2, 3, 4}}, static_cast<vec4>(svec4{{1, 2, 3, 4}}));
    test.eq(vec4{{1, 2, 3, 4}}, static_cast<vec4>(vec4{{1, 2, 3, 4}}));

    // length
    test.eq(30, length2(ivec4{{1, 2, 3, 4}}));
    test.eq(100.0f, length2(vec3{{0.0f, 0.0f, 10.0f}}));
    test.eq(37.0f, length(vec3{{36.0f, 8.0f, 3.0f}}));

    // distance
    test.eq(25, distance2(ivec2{{1, 2}}, ivec2{{4, 6}}));
    test.eq(25.0f, distance2(vec2{{1.0f, 2.0f}}, vec2{{4.0f, 6.0f}}));
    test.eq(0.5f, distance(vec2{{0.1f, 0.2f}}, vec2{{0.4f, 0.6f}}));

    // dot
    test.eq(0, dot(ivec4{{1, -1, 1, -1}}, ivec4{{1, 1, -1, -1}}));
    test.eq(10.0f, dot(vec2{{3, 7}}, vec2{{1, 1}}));

    // mix
    test.eq(vec3{{2.0f, 1.5f, 3.5f}},
            mix(vec3{{1, 2, 3}}, vec3{{5, 0, 5}}, 0.25f));

    // normalize
    test.eq(vec2{{0.6f, -0.8f}}, normalize(vec2{{48.0f, -64.0f}}));

    // cross
    test.eq(vec3{{0.0f, 0.0f, 1.0f}},
            cross(vec3{{1.0f, 0.0f, 0.0f}}, vec3{{0.0f, 1.0f, 0.0f}}));

    // data access
    test.eq(2.0f, data(vec3{{0.0f, 1.0f, 2.0f}})[2]);

    // quaternions
    const float pi_2 = std::atan(1.0f) * 2.0f;
    test.eq(vec3{{2.0f, 3.0f, 4.0f}},
            quat::one().transform(vec3{{2.0f, 3.0f, 4.0f}}));
    test.eq(vec3{{-4.0f, 2.0f, -3.0f}},
            quat::euler_angles(vec3{{pi_2, pi_2 * 2.0f, pi_2 * 3.0f}})
                .transform(vec3{{2.0f, 3.0f, 4.0f}}));
    {
        quat q = quat::axis_angle(vec3{{12.2f, 13.9f, -1.7f}}, 1.8f);
        vec3 v{{0.5f, 0.8f, 4.2f}};
        test.eq(length(v), length(q.transform(v)));
        test.eq(length(v), length(q.conjugate().transform(v)));
        test.eq(v, q.conjugate().transform(q.transform(v)));
        test.eq(v, q.transform(q.conjugate().transform(v)));
        test.eq(q.transform(v), q.normalized().transform(v));
    }

    // 3x3 matrixes
    test.eq(vec3{{9.0f, 8.0f, 7.0f}},
            mat3::identity().transform(vec3{{9.0f, 8.0f, 7.0f}}));
    test.eq(vec3{{2.0f, 4.0f, 6.0f}},
            mat3::scale(2.0f).transform(vec3{{1.0f, 2.0f, 3.0f}}));
    {
        mat3 m_sc = mat3::scale(vec3{{7.0f, 11.0f, 13.0f}});
        test.eq(vec3{{14.0f, 33.0f, 65.0f}},
                m_sc.transform(vec3{{2.0f, 3.0f, 5.0f}}));
        quat q_rot =
            quat::axis_angle(vec3{{1.0f, 1.0f, 1.0f}}, pi_2 * (4.0f / 3.0));
        mat3 m_rot = mat3::rotation(q_rot);
        test.eq(vec3{{3.0f, 1.0f, 2.0f}},
                q_rot.transform(vec3{{1.0f, 2.0f, 3.0f}}));
        test.eq(vec3{{3.0f, 1.0f, 2.0f}},
                m_rot.transform(vec3{{1.0f, 2.0f, 3.0f}}));
        test.eq(vec3{{65.0f, 14.0f, 33.0f}},
                (m_rot * m_sc).transform(vec3{{2.0f, 3.0f, 5.0f}}));
    }

    // 4x4 matrixes
    test.eq(vec3{{9.0f, 8.0f, 7.0f}},
            mat4::identity().transform(vec3{{9.0f, 8.0f, 7.0f}}));
    test.eq(vec3{{2.0f, 4.0f, 6.0f}},
            mat4::scale(2.0f).transform(vec3{{1.0f, 2.0f, 3.0f}}));
    test.eq(vec3{{11.0f, 22.0f, 33.0f}},
            mat4::translation(vec3{{10.0f, 20.0f, 30.0f}})
                .transform(vec3{{1.0f, 2.0f, 3.0f}}));
    {
        mat4 m_sc = mat4::scale(vec3{{7.0f, 11.0f, 13.0f}});
        test.eq(vec3{{14.0f, 33.0f, 65.0f}},
                m_sc.transform(vec3{{2.0f, 3.0f, 5.0f}}));
        quat q_rot =
            quat::axis_angle(vec3{{1.0f, 1.0f, 1.0f}}, pi_2 * (4.0f / 3.0));
        mat4 m_rot = mat4::rotation(q_rot);
        test.eq(vec3{{3.0f, 1.0f, 2.0f}},
                q_rot.transform(vec3{{1.0f, 2.0f, 3.0f}}));
        test.eq(vec3{{3.0f, 1.0f, 2.0f}},
                m_rot.transform(vec3{{1.0f, 2.0f, 3.0f}}));
        mat4 m_tr1 = mat4::translation(vec3{{100.0f, 200.0f, 300.0f}});
        mat4 m_tr2 = mat4::translation(vec3{{0.5f, 1.0f, 1.5f}});
        test.eq(
            vec3{{165.0f, 214.0f, 333.0f}},
            (m_tr1 * m_rot * m_sc * m_tr2).transform(vec3{{1.5f, 2.0f, 3.5f}}));
    }

    // boxes
    test.eq(ibox2{{{11, 22}}, {{13, 24}}},
            ibox2{{{1, 2}}, {{3, 4}}} + ivec2{{10, 20}});
    test.eq(ibox2{{{1, 2}}, {{3, 4}}},
            ibox2{{{11, 22}}, {{13, 24}}} - ivec2{{10, 20}});
    test.eq(ibox3{{{-5, 10, 20}}, {{10, 20, 40}}},
            ibox3::union_(ibox3{{{0, 10, 20}}, {{10, 20, 30}}},
                          ibox3{{{-5, 12, 35}}, {{2, 18, 40}}}));
    test.eq(ibox3{{{0, 12, 29}}, {{2, 18, 30}}},
            ibox3::intersection(ibox3{{{0, 10, 20}}, {{10, 20, 30}}},
                                ibox3{{{-5, 12, 29}}, {{2, 18, 40}}}));
    test(ibox3::zero().empty());
    test(ibox3{{{0, 0, 0}}, {{0, 10, 20}}}.empty());
    test(!ibox3{{{5, 5, 5}}, {{6, 6, 6}}}.empty());
    test(ibox3{{{0, 10, 20}}, {{10, 20, 30}}}.contains(ivec3{{5, 15, 25}}));
    test(!ibox3{{{0, 10, 20}}, {{10, 20, 30}}}.contains(ivec3{{-5, 15, 25}}));
    test(ibox3{{{20, 10, 0}},
               {{30, 20, 10}}}.contains(ibox3{{{22, 12, 2}}, {{28, 18, 8}}}));
    test(!ibox3{{{20, 10, 0}},
                {{30, 20, 10}}}.contains(ibox3{{{19, 12, 2}}, {{28, 18, 8}}}));
    test.eq(30, ibox3{{{0, 0, 0}}, {{2, 3, 5}}}.volume());

    // colors
    {
        auto pal = palette_db32();
        for (int i = 0; i < pal.size(); i++) {
            vec4 lc = pal.linear(i);
            vec4 sc = pal.srgb(i);
            test.eq(lc, from_srgb(sc));
            test.eq(sc, to_srgb(lc));
        }
    }

    return 0;
}
