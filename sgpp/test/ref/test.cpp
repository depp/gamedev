/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sg/ref.hpp"

#include <cstdio>
#include <cstdlib>

using namespace SG;

void die(const char *why) {
    std::fprintf(stderr, "Error: %s\n", why);
    std::exit(1);
}

class Obj {
private:
    friend class Ref<Obj>;

    int m_refcount;
    int m_op;

    static void incref(Obj *p){
        p->check();
        p->m_refcount++;
    }

    static void decref(Obj *p) {
        p->check();
        p->m_refcount--;
    }

public:
    bool m_extref;

    Obj() : m_refcount(0), m_op(0), m_extref(false) {}
    Obj(const Obj &) = delete;
    Obj &operator=(const Obj &) = delete;

    void check() {
        if (m_refcount <= 0 && !m_extref) {
            die("dead object, should be alive");
        }
    }

    void check_zero() {
        if (m_refcount) {
            die("alive object, should be dead");
        }
    }

    void op() {
        check();
        m_op++;
    }

    void check_op(int op) {
        check();
        if (m_op != op) {
            die("invalid counter");
        }
    }
};

int main() {
    Obj a, b;
    a.check_zero();
    b.check_zero();
    {
        Ref<Obj> x;
        if (x) {
            die("true");
        }
        a.m_extref = true;
        x.reset(&a);
        a.m_extref = false;
        if (!x) {
            die("false");
        }
        Ref<Obj> y(std::move(x));
        b.m_extref = true;
        x.reset(&b);
        b.m_extref = false;
        a.check_op(0);
        b.check_op(0);
        x->op();
        a.check_op(0);
        b.check_op(1);
        y->op();
        a.check_op(1);
        b.check_op(1);
        a.check();
        b.check();
        x = y;
        a.check();
        b.check_zero();
        x.reset();
        a.check();
        b.check_zero();
        if (x) {
            die("false");
        }
    }
    a.check_zero();
    b.check_zero();
}
