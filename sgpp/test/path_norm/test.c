/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sg/defs.h"
#include "sg/error.h"
#include "sg/file.h"
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char *const DATA[][2] = {
    /* Ordinary file paths */
    { "abc", "abc" },
    { "abc/def", "abc/def" },
    { "fx/stereo", "fx/stereo" },
    { "/abc", "abc" },
    { "./abc", "abc" },
    { "abc/../def", "def" },
    { "/abc/def/ghi/../../jkl", "abc/jkl" },
    { "///abc/.///def/./ghi/./.././//../jkl", "abc/jkl" },

    /* Directory paths */
    { "abc/", "abc/" },
    { ".", "" },
    { "abc/def/.", "abc/def/" },
    { "/abc/", "abc/" },
    { "abc/..", "" },
    { "abc/../def/.", "def/" },

    /* Backwards slashes */
    { "\\\\\\abcdef\\.\\..\\.\\ghijkl\\.\\.\\\\.\\\\\\\\", "ghijkl/" },
    { "\\abc\\def\\ghi\\..\\..\\jkl", "abc/jkl" },
    { "\\\\\\abc\\.\\\\\\def\\.\\ghi\\.\\..\\.\\\\\\..\\jkl", "abc/jkl" },
    { "abc\\..\\def\\.", "def/" },
    { "\\\\\\abcdef\\.\\..\\.\\ghijkl\\.\\.\\\\.\\\\\\\\", "ghijkl/" },

    /* Illegal characters */
    { "!", NULL },
    { "abc~", NULL },
    { "@", NULL },
    { "/abc/`/def/", NULL },

    /* Reserved names */
    { "con", NULL },
    { "PRN", NULL },
    { "aUx", NULL },
    { "nul.txt", NULL },
    { "com1.test", NULL },
    { "cOM2.DAT", NULL },
    { "com3/abc", NULL },
    { "abc/com4/def", NULL },
    { "com5.txt/a", NULL },
    { "com6", NULL },
    { "com7", NULL },
    { "com8", NULL },
    { "com9", NULL },
    { "lpt1.txt", NULL },
    { "lpt2.E", NULL },
    { "lpt3", NULL },
    { "lpt4", NULL },
    { "lpt5", NULL },
    { "lpt6", NULL },
    { "lpt7", NULL },
    { "lpt8", NULL },
    { "lpt9", NULL },

    /* Similar to reserved names */
    { "com", "com" },
    { "prnn", "prnn" },
    { "lptZ", "lptZ" },

#define LONGPATH \
    "abc/def/ghi/jkl/abc/def/ghi/jkl/abc/def/ghi/jkl/abc/def/ghi/jkl/"  \
    "abc/def/ghi/jkl/abc/def/ghi/jkl/abc/def/ghi/jkl/abc/def/ghi/jk"

    /* Almost too long */
    { LONGPATH "l", LONGPATH "l" },
    { LONGPATH "/", LONGPATH "/" },
    /* Too long */
    { LONGPATH "lm", NULL },
    { LONGPATH "l/", NULL },
    /* Input too long */

#define VERYLONG \
    "abc/def/../abc/def/../abc/def/../abc/def/../abc/def/../abc/def/../" \
    "abc/def/../abc/def/../abc/def/../abc/def/../abc/def/../abc/def/../"
    { VERYLONG "123",
      "abc/abc/abc/abc/abc/abc/abc/abc/abc/abc/abc/abc/123" },
    { VERYLONG,
      "abc/abc/abc/abc/abc/abc/abc/abc/abc/abc/abc/abc/" },
};

static void
die(const char *reason)
{
    fprintf(stderr, "error: %s\n", reason);
    exit(1);
}

static void *
xmalloc(size_t sz)
{
    void *p;
    if (!sz)
        return NULL;
    p = malloc(sz);
    if (!p)
        die("out of memory");
    return p;
}

SG_ATTR_FORMAT(printf, 2, 3)
SG_ATTR_NORETURN
static void
fail_func(int i, const char *msg, ...)
{
    va_list ap;
    fprintf(stderr, "Test %d failed.\n    Expected %s -> %s\n    ",
            i, DATA[i][0], DATA[i][1] ? DATA[i][1] : "<error>");
    va_start(ap, msg);
    vfprintf(stderr, msg, ap);
    va_end(ap);
    fputc('\n', stderr);
    exit(1);
}

#define fail(...) fail_func(i, __VA_ARGS__)

int
main(int argc, char **argv)
{
    int i, n, r;
    const char *ix, *iy;
    char *mx, *my;
    size_t sz;
    struct sg_error *err;

    (void) argc;
    (void) argv;

    assert(strlen(LONGPATH) == SG_MAX_PATH - 2);

    n = sizeof(DATA) / sizeof(*DATA);
    for (i = 0; i < n; i++) {
        ix = DATA[i][0];
        iy = DATA[i][1];
        sz = strlen(ix);
        mx = xmalloc(sz);
        memcpy(mx, ix, sz);
        my = xmalloc(SG_MAX_PATH);
        err = NULL;
        r = sg_path_norm(my, mx, sz, &err);
        if (r < 0) {
            if (!err || err->domain != &SG_ERROR_INVALPATH)
                die("bad error");
            if (iy)
                fail("Got error: %s", err->msg);
            sg_error_clear(&err);
        } else {
            if (!iy)
                fail("Got %s", my);
            sz = strlen(iy);
            if (r != (int) sz || memcmp(iy, my, sz + 1))
                fail("Got %s", my);
        }
        free(mx);
        free(my);
    }

    return 0;
}
