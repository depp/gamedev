/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

#include "sg/text.hpp"

#include "sg/defs.hpp"
#include "utf8.hpp"

namespace SG {
namespace Text {

const unsigned char UTF8_NBYTE[256] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0
};

bool utf8_decode(std::vector<char32_t> &out, const char *ptr,
                 std::size_t size) {
    const unsigned char *uptr = reinterpret_cast<const unsigned char *>(ptr),
                        *uend = uptr + size;
    bool no_errors = true;

    while (true) {
        unsigned uc;
        if (SG_LIKELY(uend - uptr >= 4)) {
            unsigned c0 = uptr[0];
            if (c0 < 0x80) {
                // 0xxxxxxx: 1 byte
                uc = c0;
                uptr += 1;
            } else {
                unsigned c1 = uptr[1], c2 = uptr[2], c3 = uptr[3];
                if (c0 < 0xe0) {
                    if (SG_UNLIKELY(c0 < 0xc0)) {
                        // 10xxxxxx: continuation
                        no_errors = false;
                        uc = 0xfffd;
                        uptr += 1;
                    } else {
                        // 110xxxxx: 2 byte
                        uc = ((c0 & 0x1f) << 6) | (c1 & 0x3f);
                        if (SG_LIKELY((c1 & 0xc0) == 0x80 && uc >= 0x80)) {
                            uptr += 2;
                        } else {
                            goto slow;
                        }
                    }
                } else {
                    if (c0 < 0xf0) {
                        // 1110xxxx: 3 byte
                        uc = ((c0 & 0x0f) << 12) | ((c1 & 0x3f) << 6) |
                             (c2 & 0x3f);
                        unsigned cm = c1 | (c2 << 8);
                        if (SG_LIKELY(((cm & 0xc0c0) == 0x8080 && uc >= 0x800 &&
                                    (uc & 0xf800) != 0xd800))) {
                            uptr += 3;
                        } else {
                            goto slow;
                        }
                    } else {
                        // 11110xxx: 4 byte
                        uc = ((c0 & 0x07) << 18) | ((c1 & 0x3f) << 12) |
                             ((c2 & 0x3f) << 6) | (c3 & 0x3f);
                        unsigned cm = c1 | (c2 << 8) | (c3 << 16) | (c0 << 24);
                        if (SG_LIKELY((cm & 0xf8c0c0c0) == 0xf0808080 &&
                                   uc >= 0x10000 && uc < 0x110000)) {
                            uptr += 4;
                        } else {
                            goto slow;
                        }
                    }
                }
            }
        } else {
        slow:
            if (uptr == uend) {
                break;
            }
            unsigned c0 = uptr[0];
            int n = UTF8_NBYTE[c0], i = 1;
            uc = c0;
            for (; i < n && uptr + i != uend; i++) {
                unsigned cc = uptr[i];
                if ((cc & 0xc0) != 0x80) {
                    break;
                }
                uc = (uc << 6) | (cc & 0x3f);
            }
            uptr += i;
            if (i != n) {
                no_errors = false;
                uc = 0xfffd;
            } else {
                switch (n) {
                default:
                case 1:
                    break;

                case 2:
                    uc &= 0x7ff;
                    if (uc < 0x80) {
                        no_errors = false;
                        uc = 0xfffd;
                    }
                    break;

                case 3:
                    uc &= 0xffff;
                    if (uc < 0x800 || ((uc & 0xf800) == 0xd800)) {
                        no_errors = false;
                        uc = 0xfffd;
                    }
                    break;

                case 4:
                    if (uc < 0x10000 || uc >= 0x110000) {
                        no_errors = false;
                        uc = 0xfffd;
                    }
                    break;
                }
            }
        }
        out.push_back(uc);
    }

    return no_errors;
}

}
}
