/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

#include "sg/text.hpp"

#include "sg/defs.hpp"
#include "utf8.hpp"

#include <cstring>
#include <cwchar>

namespace SG {
namespace Text {

bool from_wstring(std::string &out, const std::wstring &str) {
    return from_wstring(out, str.data(), str.size());
}

bool from_wstring(std::string &out, const wchar_t *ptr) {
    return from_wstring(out, ptr, std::wcslen(ptr));
}

bool from_wstring(std::string &out, const wchar_t *ptr, std::size_t size) {
    const wchar_t *uptr = ptr, *uend = ptr + size;
    bool no_errors = true;

    while (uend != uptr) {
        unsigned uc = *uptr;
        if (uc < 0x800) {
            if (uc < 0x80) {
                out.push_back(uc);
            } else {
                char c[2];
                c[0] = static_cast<char>(0xc0 | (uc >> 6));
                c[1] = static_cast<char>(0x80 | (uc & 0x3f));
                out.append(c, 2);
            }
            uptr++;
        } else {
            if (uc < 0xd800 || uc >= 0xe000) {
                char c[3];
                c[0] = static_cast<char>(0xe0 | (uc >> 12));
                c[1] = static_cast<char>(0x80 | ((uc >> 6) & 0x3f));
                c[2] = static_cast<char>(0x80 | (uc & 0x3f));
                out.append(c, 3);
                uptr++;
            } else {
                if (SG_UNLIKELY(uc >= 0xdc00 || uend - uptr < 2 ||
                                uptr[1] < 0xdc00 || uptr[1] >= 0xe000)) {
                    out.append(u8"\ufffd", 3);
                    uptr++;
                    no_errors = false;
                } else {
                    uc = 0x10000 + (((uc & 0x3ff) << 10) | (uptr[1] & 0x3ff));
                    char c[4];
                    c[0] = static_cast<char>(0xf0 | (uc >> 18));
                    c[1] = static_cast<char>(0x80 | ((uc >> 12) & 0x3f));
                    c[2] = static_cast<char>(0x80 | ((uc >> 6) & 0x3f));
                    c[3] = static_cast<char>(0x80 | (uc & 0x3f));
                    out.append(c, 4);
                    uptr += 2;
                }
            }
        }
    }

    return no_errors;
}

bool to_wstring(std::wstring &out, const std::string &str) {
    return to_wstring(out, str.data(), str.size());
}

bool to_wstring(std::wstring &out, const char *ptr) {
    return to_wstring(out, ptr, std::strlen(ptr));
}

bool to_wstring(std::wstring &out, const char *ptr, std::size_t size) {
    const unsigned char *uptr = reinterpret_cast<const unsigned char *>(ptr),
                        *uend = uptr + size;
    bool no_errors = true;

    while (true) {
        unsigned uc;
        if (SG_LIKELY(uend - uptr >= 4)) {
            unsigned c0 = uptr[0];
            if (c0 < 0x80) {
                // 0xxxxxxx: 1 byte
                uc = c0;
                uptr += 1;
                out.push_back(uc);
            } else {
                unsigned c1 = uptr[1], c2 = uptr[2], c3 = uptr[3];
                if (c0 < 0xe0) {
                    if (SG_UNLIKELY(c0 < 0xc0)) {
                        // 10xxxxxx: continuation
                        no_errors = false;
                        uc = 0xfffd;
                        uptr += 1;
                    } else {
                        // 110xxxxx: 2 byte
                        uc = ((c0 & 0x1f) << 6) | (c1 & 0x3f);
                        if (SG_LIKELY((c1 & 0xc0) == 0x80 && uc >= 0x80)) {
                            uptr += 2;
                        } else {
                            goto slow;
                        }
                    }
                    out.push_back(uc);
                } else {
                    if (c0 < 0xf0) {
                        // 1110xxxx: 3 byte
                        uc = ((c0 & 0x0f) << 12) | ((c1 & 0x3f) << 6) |
                             (c2 & 0x3f);
                        unsigned cm = c1 | (c2 << 8);
                        if (SG_LIKELY(((cm & 0xc0c0) == 0x8080 && uc >= 0x800 &&
                                       (uc & 0xf800) != 0xd800))) {
                            uptr += 3;
                        } else {
                            goto slow;
                        }
                        out.push_back(uc);
                    } else {
                        // 11110xxx: 4 byte
                        uc = ((c0 & 0x07) << 18) | ((c1 & 0x3f) << 12) |
                             ((c2 & 0x3f) << 6) | (c3 & 0x3f);
                        unsigned cm = c1 | (c2 << 8) | (c3 << 16) | (c0 << 24);
                        if (SG_LIKELY((cm & 0xf8c0c0c0) == 0xf0808080 &&
                                      uc >= 0x10000 && uc < 0x110000)) {
                            uptr += 4;
                        } else {
                            goto slow;
                        }
                        wchar_t sg[2];
                        uc -= 0x10000;
                        sg[0] = static_cast<wchar_t>(0xd800 | (uc >> 10));
                        sg[1] = static_cast<wchar_t>(0xdc00 | (uc & 0x3ff));
                        out.append(sg, 2);
                    }
                }
            }
        } else {
        slow:
            if (uptr == uend) {
                break;
            }
            unsigned c0 = uptr[0];
            int n = UTF8_NBYTE[c0], i = 1;
            uc = c0;
            for (; i < n && uptr + i != uend; i++) {
                unsigned cc = uptr[i];
                if ((cc & 0xc0) != 0x80) {
                    break;
                }
                uc = (uc << 6) | (cc & 0x3f);
            }
            uptr += i;
            if (i != n) {
                no_errors = false;
                out.push_back(0xfffd);
            } else {
                switch (n) {
                default:
                case 1:
                    out.push_back(uc);
                    break;

                case 2:
                    uc &= 0x7ff;
                    if (uc < 0x80) {
                        no_errors = false;
                        uc = 0xfffd;
                    }
                    out.push_back(uc);
                    break;

                case 3:
                    uc &= 0xffff;
                    if (uc < 0x800 || ((uc & 0xf800) == 0xd800)) {
                        no_errors = false;
                        uc = 0xfffd;
                    }
                    out.push_back(uc);
                    break;

                case 4:
                    if (uc < 0x10000 || uc >= 0x110000) {
                        no_errors = false;
                        uc = 0xfffd;
                        out.push_back(0xfffd);
                    } else {
                        wchar_t sg[2];
                        uc -= 0x10000;
                        sg[0] = static_cast<wchar_t>(0xd800 | (uc >> 10));
                        sg[1] = static_cast<wchar_t>(0xdc00 | (uc & 0x3ff));
                        out.append(sg, 2);
                    }
                    break;
                }
            }
        }
    }

    return no_errors;
}

}
}
