/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

namespace SG {
namespace Text {

//! Number of bytes in UTF-8 sequences starting with the given octet.
extern const unsigned char UTF8_NBYTE[256];

}
}
