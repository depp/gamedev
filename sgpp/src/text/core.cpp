/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "private.hpp"
#include "sg/pack.h"
#include "sgpp/image.hpp"
#include "sgpp/log.hpp"

#include "sgpp/utf8.hpp"
#include "sggl/3_2.h"

#include <algorithm>
#include <cassert>

using namespace gl_3_2;
namespace SG {
namespace Text {

Core::Core()
    : m_refcount(1),
      m_dirty(true),
      m_initialized(false),
      m_fontdata{0, IVec2::zero(), Vec2::zero()},
      m_arraydata{0, 0} {
    m_bitmaps.push_back(Bitmap{{{0, 0}}, {{0, 0}}, {{0, 0}}});
}

Core::~Core() {
    glDeleteTextures(1, &m_fontdata.texture);
    glDeleteBuffers(1, &m_arraydata.buffer);
}

int Core::get_font(FontInstance *font) {
    if (!font) {
        return -1;
    }
    std::size_t n = m_font.size();
    for (std::size_t i = 0; i < n; i++) {
        if (m_font[i].font.get() == font) {
            return static_cast<int>(i);
        }
    }
    m_font.push_back(FontEntry{Ref<FontInstance>(font), -1, -1});
    return static_cast<int>(n);
}

void Core::glyph_bitmaps(FontInstance *font, const Range *runs,
                         std::size_t run_count,
                         unsigned short *bitmap, const int *glyph) {
    int font_index = get_font(font);
    if (font_index < 0) {
        return;
    }
    FontEntry &fe = m_font[font_index];

    int bfirst = fe.first, bcount = font->info().glyph_count;
    if (bfirst < 0) {
        bfirst = static_cast<int>(m_glyph_bitmap.size());
        if (bcount > std::numeric_limits<int>::max() - bfirst) {
            std::abort();
        }
        m_glyph_bitmap.resize(bfirst + bcount, BITMAP_MISSING);
        m_font[font_index].first = bfirst;
        m_font[font_index].count = bcount;
    }

    std::vector<int> new_glyphs;
    for (std::size_t i = 0; i < run_count; i++) {
        int gfirst = runs[i].first, glast = gfirst + runs[i].count;
        for (int i = gfirst; i < glast; i++) {
            int gi = glyph[i];
            if (gi < 0 || gi >= bcount) {
                continue;
            }
            unsigned short bi = m_glyph_bitmap[gi + bfirst];
            if (bi == BITMAP_MISSING) {
                new_glyphs.push_back(gi);
                m_glyph_bitmap[gi + bfirst] = 0;
            }
        }
    }

    if (!new_glyphs.empty()) {
        std::size_t new_count = new_glyphs.size();
        std::size_t old_count = m_bitmaps.size();
        if (new_count > BITMAP_MISSING - old_count) {
            std::abort();
        }
        m_bitmaps.resize(old_count + new_count);
        m_missing.reserve(m_missing.size() + new_count);
        font->glyph_info(m_bitmaps.data() + old_count, new_glyphs.data(),
                         new_count);
        std::size_t j = 0;
        for (std::size_t i = 0; i < new_count; i++) {
            I16Vec2 size = m_bitmaps[old_count + i].size;
            I16Vec2 offset = m_bitmaps[old_count + i].offset;
            if (size[0] <= 0 || size[1] <= 0) {
                continue;
            }
            unsigned short bi = old_count + j;
            unsigned short fi = font_index;
            int gi = new_glyphs[i];
            m_bitmaps[bi].size = size;
            m_bitmaps[bi].offset = offset;
            m_glyph_bitmap[bfirst + gi] = bi;
            m_missing.push_back(MissingGlyph{bi, fi, gi});
            j++;
        }
        m_bitmaps.resize(old_count + j);
    }

    for (std::size_t i = 0; i < run_count; i++) {
        int gfirst = runs[i].first, glast = gfirst + runs[i].count;
        for (int i = gfirst; i < glast; i++) {
            int gi = glyph[i];
            unsigned short bi;
            if (gi < 0 || gi >= bcount) {
                bi = 0;
            } else {
                bi = m_glyph_bitmap[gi + bfirst];
            }
            bitmap[i] = bi;
        }
    }
}

namespace {

bool compare_member(const Membership &x, const Membership &y) {
    return x.range.first < y.range.first;
}

struct Vertex {
    I16Vec2 vpos, tpos, size, style;
};

bool compare_missing(const MissingGlyph &x, const MissingGlyph &y) {
    if (x.font != y.font) {
        return x.font < y.font;
    } else {
        return x.glyph < y.glyph;
    }
}

}

void Core::sync() {
    if (!m_dirty) {
        return;
    }
    m_dirty = false;
    if (!m_initialized) {
        m_initialized = true;
        glGenBuffers(1, &m_arraydata.buffer);
        glGenTextures(1, &m_fontdata.texture);
    }
    sync_glyphs();
    sync_batches();
}

int Core::new_batch() {
    std::size_t n = m_batches.size();
    for (std::size_t i = 0; i < n; i++) {
        if (!m_batches[i].active) {
            return i;
        }
    }
    m_batches.resize(n + 1);
    m_batches[n].active = false;
    return static_cast<int>(n);
}

int Core::new_member() {
    std::size_t n = m_members.size();
    for (std::size_t i = 0; i < n; i++) {
        if (m_members[i].batch == MEMBER_NONE) {
            return static_cast<int>(i);
        }
    }
    m_members.resize(n + 1);
    m_members[n].batch = MEMBER_NONE;
    return static_cast<int>(n);
}

namespace {

bool compare_range_first(const Range &x, const Range &y) {
    return x.first < y.first;
}

}

bool Core::gc_layout() {
    std::size_t mcount = 0;
    {
        auto p = std::begin(m_members), q = p, e = std::end(m_members);
        for (; p != e; p++) {
            if (p->batch != MEMBER_NONE) {
                mcount++;
                q = p + 1;
            }
        }
        m_members.erase(q, e);
    }
    if (!mcount) {
        bool result = !m_quads.empty();
        std::vector<Membership>().swap(m_members);
        std::vector<Quad>().swap(m_quads);
        return result;
    }
    // list of ranges which are in use
    std::vector<Range> ranges(mcount);
    // repurpose range.count as range.last
    {
        std::size_t pos = 0;
        for (const auto &m : m_members) {
            if (m.batch != MEMBER_NONE) {
                const auto &r = m.range;
                ranges[pos++] = Range{r.first, r.first + r.count};
            }
        }
        assert(pos == mcount);
    }
    // merge ranges
    std::sort(std::begin(ranges), std::end(ranges), compare_range_first);
    {
        auto p = std::begin(ranges), q = p + 1, e = std::end(ranges);
        for (; q != e; q++) {
            if (p->count >= q->first) {
                p->count = std::max(p->count, q->count);
            } else {
                p++;
                *p = *q;
            }
        }
        p++;
        ranges.erase(p, e);
    }
    // move layout data
    int qcount = 0;
    for (auto &r : ranges) {
        qcount += r.count - r.first;
    }
    if (static_cast<std::size_t>(qcount) == m_quads.size()) {
        return false;
    }
    std::vector<Quad> quads(qcount);
    // repurpose range.count as range.target
    {
        int pos = 0;
        for (auto &r : ranges) {
            int rcount = r.count - r.first;
            std::copy(
                std::begin(m_quads) + r.first,
                std::begin(m_quads) + r.count,
                std::begin(quads) + pos);
            r.count = pos;
            pos += rcount;
        }
    }
    for (auto &m : m_members) {
        int first = m.range.first;
        bool success = false;
        for (const auto &r : ranges) {
            if (r.first < first) {
                m.range.first = r.count;
                success = true;
                break;
            }
        }
        assert(success);
    }
    return true;
}

bool Core::gc_bitmap() {
    std::size_t bcount = m_bitmaps.size();
    assert(bcount >= 1);
    std::vector<unsigned char> used(bcount, 0);
    used[0] = 1;
    for (const auto &g : m_quads) {
        used[g.bitmap] = 1;
    }
    std::size_t new_bcount = 0;
    for (auto flag : used) {
        new_bcount += flag;
    }
    if (new_bcount == bcount) {
        return false;
    }
    std::vector<Bitmap> bitmaps(new_bcount);
    std::vector<unsigned short> index_map(bcount);
    for (std::size_t i = 0, j = 0; i < bcount; i++) {
        if (used[i]) {
            bitmaps[j] = m_bitmaps[i];
            index_map[i] = j;
            j++;
        } else {
            index_map[i] = BITMAP_MISSING;
        }
    }
    assert(index_map[0] == 0);
    for (auto &bi : m_glyph_bitmap) {
        bi = index_map[bi];
    }
    for (auto &quad : m_quads) {
        quad.bitmap = index_map[quad.bitmap];
    }
    m_bitmaps = std::move(bitmaps);
    return true;
}

bool Core::gc_font() {
    std::size_t fcount = m_font.size();
    std::size_t new_fcount = 0, new_icount = 0;
    std::vector<unsigned char> used(fcount, 0);
    for (std::size_t i = 0; i < fcount; i++) {
        const auto &font = m_font[i];
        int first = font.first, count = font.count, j;
        if (first < 0) {
            continue;
        }
        for (j = 0; j < count; j++) {
            if (m_glyph_bitmap[j + first] != BITMAP_MISSING) {
                break;
            }
        }
        if (j < count) {
            used[i] = 1;
            new_fcount++;
            new_icount += count;
        }
    }
    if (new_fcount == fcount) {
        return false;
    }
    std::vector<unsigned short> glyph_index(new_icount);
    std::vector<FontEntry> fonts(new_fcount);
    std::size_t fpos = 0, ipos = 0;
    for (std::size_t i = 0; i < fcount; i++) {
        if (!used[i]) {
            continue;
        }
        auto &font = m_font[i];
        int first = font.first, count = font.count;
        std::copy(
            std::begin(m_glyph_bitmap) + first,
            std::begin(m_glyph_bitmap) + first + count,
            std::begin(glyph_index) + ipos);
        fonts[fpos] = FontEntry{
            std::move(font.font),
            static_cast<int>(ipos),
            count
        };
        fpos++;
        ipos += count;
    }
    assert(fpos == new_fcount);
    assert(ipos == new_icount);
    m_glyph_bitmap = std::move(glyph_index);
    m_font = std::move(fonts);
    return true;
}

void Core::sync_glyphs() {
    if (m_missing.empty()) {
        return;
    }

    std::size_t mcount = m_missing.size();
    std::size_t moffset = m_bitmaps.size() - mcount;
    std::vector<Packing::Rect> rects(mcount);
    for (std::size_t i = 0; i < mcount; i++) {
        rects[i].size = m_bitmaps[moffset + i].size;
    }
    bool incremental = m_packing.size()[0] > 0 &&
        m_packing.pack(rects.data(), rects.size());
    IVec2 size;
    int miny, maxy;
    if (incremental) {
        size = m_packing.size();
        auto p = std::begin(m_missing), e = std::end(m_missing);
        std::sort(p, e, compare_missing);
        std::size_t n = m_font.size();
        std::vector<GlyphDraw> glyph_draw;
        miny = std::numeric_limits<int>::max();
        maxy = std::numeric_limits<int>::min();
        for (std::size_t i = 0; i < n; i++) {
            for (; p != e && p->font == i; p++) {
                unsigned short bi = p->bitmap;
                if (!bi) {
                    continue;
                }
                const auto r = rects[bi - moffset];
                miny = std::min<int>(miny, r.loc[1]);
                maxy = std::max(maxy, r.loc[1] + r.size[1]);
                glyph_draw.push_back(GlyphDraw{p->glyph, r.loc});
            }
            if (!glyph_draw.empty()) {
                m_font[i].font->draw_glyphs(
                    m_pixbuf, glyph_draw.data(), glyph_draw.size());
            }
        }
        glBindTexture(GL_TEXTURE_2D, m_fontdata.texture);
    } else {
        if (moffset > 0) {
            gc_layout() && gc_bitmap() && gc_font();
        }
        moffset = 0;
        mcount = m_bitmaps.size();
        rects.resize(mcount);
        for (std::size_t i = 0; i < mcount; i++) {
            rects[i].size = m_bitmaps[i].size;
        }
        bool success = m_packing.auto_pack(
            rects.data(), rects.size(), IVec2{{16, 16}}, IVec2{{16384, 16384}});
        if (!success) {
            std::abort();
        }
        size = m_packing.size();
        m_pixbuf.calloc(SG_R, size[0], size[1]);
        std::vector<GlyphDraw> glyph_draw;
        miny = 0;
        maxy = std::numeric_limits<int>::min();
        for (const auto &font : m_font) {
            glyph_draw.clear();
            int first = font.first, count = font.count;
            for (int i = 0; i < count; i++) {
                unsigned short bi = m_glyph_bitmap[first + i];
                if (bi == BITMAP_MISSING || !bi) {
                    continue;
                }
                const auto r = rects[bi];
                maxy = std::max(maxy, r.loc[1] + r.size[1]);
                glyph_draw.push_back(GlyphDraw{i, r.loc});
            }
            if (!glyph_draw.empty()) {
                font.font->draw_glyphs(
                    m_pixbuf, glyph_draw.data(), glyph_draw.size());
            }
        }
        glBindTexture(GL_TEXTURE_2D, m_fontdata.texture);
        m_fontdata.texsize = size;
        m_fontdata.texscale = Vec2{
            1.0f / static_cast<float>(size[0]),
            1.0f / static_cast<float>(size[1])
        };
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, size[0], size[1], 0,
                     GL_RED, GL_UNSIGNED_BYTE, nullptr);
        Log::debug("Packed %d glyphs into %dx%d texture",
                   (int) m_bitmaps.size(), size[0], size[1]);
    }
    for (std::size_t i = 0; i < mcount; i++) {
        m_bitmaps[moffset + i].loc = rects[i].loc;
    }
    m_missing.clear();

    if (maxy > miny) {
        auto &p = m_pixbuf.pixbuf();
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, miny, size[0], maxy - miny,
                        GL_RED, GL_UNSIGNED_BYTE,
                        static_cast<const char *>(p.data) +
                        p.rowbytes * miny);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
}

void Core::sync_batches() {
    const bool SHOW_TEXTURE = false;
    std::vector<Membership> members;
    members.reserve(m_members.size());
    int nbatch = m_batches.size();
    for (const auto &m : m_members) {
        if (m.batch >= 0) {
            assert(m.batch < nbatch);
            members.push_back(m);
        }
    }
    std::sort(std::begin(members), std::end(members), compare_member);
    std::vector<std::size_t> batches(nbatch, 0);
    for (const auto &m : members) {
        batches[m.batch] += m.range.count;
    }
    int count = 0;
    for (int i = 0; i < nbatch; i++) {
        int bcount = batches[i];
        batches[i] = count;
        m_batches[i].range = Range{count, bcount};
        count += bcount;
    }

    if (!count) {
        return;
    }
    glBindBuffer(GL_ARRAY_BUFFER, m_arraydata.buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * count, nullptr,
                 GL_STATIC_DRAW);
    Vertex *ptr = static_cast<Vertex *>(
        glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
    if (!ptr) {
        std::abort();
    }
    for (const auto m : members) {
        int pos = batches[m.batch];
        batches[m.batch] = pos + m.range.count;
        for (int i = 0; i < m.range.count; i++) {
            Quad quad = m_quads[m.range.first + i];
            auto bd = m_bitmaps[quad.bitmap];
            ptr[pos + i] = Vertex{
                quad.loc + m.offset,
                bd.loc,
                bd.size,
                quad.style
            };
        }
    }
    if (SHOW_TEXTURE) {
        ptr[0] = Vertex{
            {{0, 0}},
            {{0, 0}},
            static_cast<I16Vec2>(m_fontdata.texsize),
            {{0, 0}}
        };
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

}
}
