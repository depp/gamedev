/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "private.hpp"
#include "sgpp/log.hpp"
#include "sg/pixbuf.h"

#include <cassert>
#include <memory>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_TRUETYPE_TABLES_H
#include <hb.h>
#include <hb-ft.h>

namespace SG {
namespace Text {
class FontInstanceFT;

FT_Library g_library_ft;

class FontFileFT : public FontFile {
private:
    struct Instance {
        int size;
        FontInstanceFT *obj;
    };

    // The file data.
    Data m_data;

    // The FreeType face.
    FT_Face m_face;

    // The HarfBuzz font.
    hb_font_t *m_hb_font;

    // All instances of this font.
    std::vector<Instance> m_instances;

public:
    FontFileFT();
    virtual ~FontFileFT();

    bool load(Data data);

    virtual FontDesc desc() const override;

    virtual Ref<FontInstance> instantiate(float size) override;

    void remove(FontInstanceFT *instance);

    hb_font_t *hb_font();

    FT_Face face() const { return m_face; }
};

class FontInstanceFT : public FontInstance {
private:
    struct Glyph {
        // Whether the data has been filled in.
        unsigned short initted;
        // The horizontal pen advance.
        short advance;
        // Location of the bitmap, relative to the pen.
        I16Vec2 bitmap;
    };

    // The parent font file.
    FontFileFT *m_file;

    // The FreeType font face.
    FT_Face m_face;

    // The font size, in FreeType's 26.2 units.
    int m_size;

    // Information about each glyph, filled when glyph_data() is
    // called.
    std::vector<Glyph> m_glyph;

public:
    FontInstanceFT(FontFileFT *file, int size);
    virtual ~FontInstanceFT();

    virtual void map_glyphs(int *glyph_index, const int *code_point,
                            std::size_t count) override;

    virtual void glyph_info(Bitmap *bitmap, const int *glyph_index,
                            std::size_t count) override;

    virtual void draw_glyphs(Pixbuf &pixbuf, const GlyphDraw *glyph_draw,
                             std::size_t count) override;

    virtual int layout_run(GlyphPos *glyph_pos, const int *glyph_index,
                           std::size_t count, int pos) override;

    virtual void *hb_font() override;

private:
    void set_size();
};

////////////////////////////////////////////////////////////////////////

FontFileFT::FontFileFT() : m_face(nullptr), m_hb_font(nullptr) {}

FontFileFT::~FontFileFT() {
    if (m_face) {
        FT_Done_Face(m_face);
    }
}

bool FontFileFT::load(Data data) {
    FT_Error ferr;
    ferr = FT_New_Memory_Face(
        g_library_ft,
        static_cast<const unsigned char *>(data.data()),
        data.size(),
        0,
        &m_face);
    if (ferr) {
        return false;
    }

    m_data = std::move(data);
    return true;
}

FontDesc FontFileFT::desc() const {
    FontDesc s;
    s.family = m_face->family_name;
    s.size = 0.0f;

    {
        auto flags = m_face->style_flags;
        s.style = (flags & FT_STYLE_FLAG_ITALIC) != 0 ?
            FontStyle::ITALIC : FontStyle::NORMAL;
        s.weight = (flags & FT_STYLE_FLAG_BOLD) != 0 ? 700 : 400;
    }

    {
        const TT_OS2 *os2 = static_cast<TT_OS2 *>(
            FT_Get_Sfnt_Table(m_face, ft_sfnt_os2));
        if (os2) {
            s.weight = os2->usWeightClass;
        }
    }

    return s;
}

Ref<FontInstance> FontFileFT::instantiate(float size) {
    int isize = static_cast<int>(size * 64.0f + 0.5f);
    if (isize < 64 * 4) {
        isize = 64 * 4;
    } else if (isize > 64 * 1024) {
        isize = 64 * 1024;
    }
    for (auto &i : m_instances) {
        if (i.size == isize) {
            return Ref<FontInstance>(i.obj);
        }
    }
    auto fi = new FontInstanceFT(this, isize);
    Ref<FontInstance> result(new FontInstanceFT(this, isize));
    m_instances.push_back(Instance{isize, fi});
    return std::move(result);
}

void FontFileFT::remove(FontInstanceFT *instance) {
    for (auto p = std::begin(m_instances), e = std::end(m_instances);
         p != e; p++) {
        if (p->obj == instance) {
            m_instances.erase(p);
            return;
        }
    }
}

hb_font_t *FontFileFT::hb_font() {
    if (!m_hb_font) {
        hb_font_t *hf = hb_ft_font_create(m_face, nullptr);
        if (!hf) {
            std::abort();
        }
        m_hb_font = hf;
    }
    return m_hb_font;
}

////////////////////////////////////////////////////////////////////////

FontInstanceFT::FontInstanceFT(FontFileFT *file, int size)
    : m_file(file), m_face(file->face()), m_size(size) {
    set_size();
    const auto &m = m_face->size->metrics;
    m_info = FontInfo{
        static_cast<int>(m_face->num_glyphs),
        static_cast<int>(m.ascender >> 6),
        static_cast<int>(m.descender >> 6),
        static_cast<int>(m.height >> 6)
    };
    FontFile::incref(file);
}

FontInstanceFT::~FontInstanceFT() {
    m_file->remove(this);
    FontFile::decref(m_file);
}

void FontInstanceFT::map_glyphs(int *glyph_index, const int *code_point,
                                std::size_t count) {
    for (std::size_t i = 0; i < count; i++) {
        glyph_index[i] = FT_Get_Char_Index(m_face, code_point[i]);
    }
}

void FontInstanceFT::glyph_info(Bitmap *bitmaps, const int *glyph_index,
                                std::size_t count) {
    set_size();
    for (std::size_t i = 0; i < count; i++) {
        int glyph = glyph_index[i];
        FT_Error ferr;
        ferr = FT_Load_Glyph(m_face, glyph, FT_LOAD_TARGET_NORMAL);
        if (ferr) {
            std::abort();
        }
        auto slot = m_face->glyph;
        ferr = FT_Render_Glyph(slot, FT_RENDER_MODE_NORMAL);
        if (ferr) {
            std::abort();
        }
        bitmaps[i].size = I16Vec2{{static_cast<short>(slot->bitmap.width),
                                   static_cast<short>(slot->bitmap.rows)}};
        bitmaps[i].offset =
            I16Vec2{{static_cast<short>(slot->bitmap_left),
                     static_cast<short>(slot->bitmap_top - slot->bitmap.rows)}};
        // bitmaps[i].offset[1] = static_cast<short>(i);
    }
}

void FontInstanceFT::draw_glyphs(Pixbuf &pixbuf, const GlyphDraw *glyph_draw,
                                 std::size_t count)  {
    auto &pb = pixbuf.pixbuf();
    if (pb.format != SG_R) {
        std::abort();
    }
    set_size();
    std::size_t orb = pb.rowbytes;
    unsigned char *op = static_cast<unsigned char *>(pb.data);
    int px = pb.width, py = pb.height;
    for (std::size_t i = 0; i < count; i++) {
        const auto gd = glyph_draw[i];
        FT_Error ferr;
        ferr = FT_Load_Glyph(m_face, gd.glyph, FT_LOAD_TARGET_NORMAL);
        auto slot = m_face->glyph;
        if (ferr) {
            std::abort();
        }
        ferr = FT_Render_Glyph(slot, FT_RENDER_MODE_NORMAL);
        if (ferr) {
            std::abort();
        }
        int sx = slot->bitmap.width, sy = slot->bitmap.rows;
        assert(gd.loc[0] >= 0 && gd.loc[0] <= px &&
               sx >= 0 && sx <= px - gd.loc[0] &&
               gd.loc[1] >= 0 && gd.loc[1] <= py &&
               sy >= 0 && sy <= py - gd.loc[1]);
        const unsigned char *ip = slot->bitmap.buffer;
        std::size_t irb = slot->bitmap.pitch;
        std::size_t off = gd.loc[1] * orb + gd.loc[0];
        for (int y = 0; y < sy; y++) {
            for (int x = 0; x < sx; x++) {
                op[y * orb + x + off] = ip[y * irb + x];
            }
        }
    }
}

int FontInstanceFT::layout_run(GlyphPos *glyph_pos, const int *glyph_index,
                               std::size_t count, int pos) {
    int n = m_face->num_glyphs;
    if (m_glyph.empty()) {
        m_glyph.resize(
            std::max(n, 1),
            Glyph{0, 0, {{0, 0}}});
    }
    set_size();

    int cur_pos = pos;
    for (std::size_t i = 0; i < count; i++) {
        int glyph = glyph_index[i];
        if (glyph < 0 || glyph >= n) {
            std::abort();
        }
        auto &gd = m_glyph[glyph];
        if (!gd.initted) {
            FT_Error ferr;
            ferr = FT_Load_Glyph(m_face, glyph, FT_LOAD_TARGET_NORMAL);
            if (ferr) {
                std::abort();
            }
            auto slot = m_face->glyph;
            ferr = FT_Render_Glyph(slot, FT_RENDER_MODE_NORMAL);
            if (ferr) {
                std::abort();
            }
            gd.initted = 1;
            gd.advance = slot->advance.x >> 6;
            gd.bitmap = I16Vec2{{
                static_cast<short>(slot->bitmap_left),
                static_cast<short>(slot->bitmap_top -
                                   (slot->metrics.height >> 6))
            }};
        }
        glyph_pos[i].pos = cur_pos;
        glyph_pos[i].offset = gd.bitmap;
        cur_pos += gd.advance;
    }
    return cur_pos;
}

void FontInstanceFT::set_size() {
    FT_Error ferr = FT_Set_Char_Size(m_face, 0, m_size, 72, 72);
    if (ferr) {
        std::abort();
    }
}

void *FontInstanceFT::hb_font() {
    return m_file->hb_font();
}

////////////////////////////////////////////////////////////////////////

std::unique_ptr<FontFile> FontFile::load_freetype(SG::Data data) {
    if (!g_library_ft) {
        FT_Error ferr = FT_Init_FreeType(&g_library_ft);
        if (ferr) {
            Log::abort("Could not initialize FreeType.");
        }
    }

    std::unique_ptr<FontFileFT> font(new FontFileFT);
    if (!font->load(std::move(data))) {
        return nullptr;
    }
    return std::move(font);
}

}
}
