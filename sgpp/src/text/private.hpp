/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sgpp/text.hpp"
#include "sgpp/file.hpp"
#include "sgpp/image.hpp"
#include "sgpp/pack.hpp"
#include "sgpp/ref.hpp"
#include "sgpp/vec.hpp"
#include "sggl/common.h"

#include <memory>
#include <vector>

struct sg_pixbuf;
namespace SG {
namespace Text {
class FontInstance;

////////////////////////////////////////////////////////////////////////

/// A font file.
class FontFile {
private:
    int m_refcount;

public:
    FontFile();
    FontFile(const FontFile &) = delete;
    FontFile &operator=(const FontFile &) = delete;
    virtual ~FontFile();

    static void incref(FontFile *p);
    static void decref(FontFile *p);

    /// Get the font's description.
    virtual FontDesc desc() const = 0;

    /// Get a font instance for the font at the given size.
    virtual Ref<FontInstance> instantiate(float size) = 0;

    /// Get the font which is closest to the requested description,
    /// ignoring font size.
    ///
    /// This uses the font matching algorithm from CSS Fonts Module
    /// Level 3 (CR-3).
    ///
    /// http://www.w3.org/TR/css3-fonts/#font-matching-algorithm
    static Ref<FontFile> get_font(const FontDesc &desc);

    /// Load a font.  This will dispatch to one of the other loading
    /// functions.
    static std::unique_ptr<FontFile> load(SG::Data data);

    /// Load a font using the FreeType library.
    static std::unique_ptr<FontFile> load_freetype(SG::Data data);
};

inline FontFile::FontFile() : m_refcount(0) {}

inline void FontFile::incref(FontFile *p) {
    p->m_refcount++;
}

inline void FontFile::decref(FontFile *p) {
    p->m_refcount--;
    if (!p->m_refcount) {
        delete p;
    }
}

////////////////////////////////////////////////////////////////////////

/// Information about a glyph bitmap.
struct Bitmap {
    /// The size of the bitmap.
    I16Vec2 size;

    /// The location of the bitmap's upper left corner in the bitmap
    /// texture.
    I16Vec2 loc;

    /// The offset from the pen where the lower left corner of the
    /// bitmap should be drawn.
    I16Vec2 offset;
};

struct GlyphPos {
    // Pen position where the glyph is drawn.
    int pos;
    // Position of the glyph's bottom left corner, relative to the pen.
    I16Vec2 offset;
};

// A request to draw an individual glyph.
struct GlyphDraw {
    // The index of the glyph to draw.
    int glyph;
    // The location where the upper left corner of the glyph's bitmap
    // should be drawn.
    I16Vec2 loc;
};

// Information about an instance of a font, mostly metrics.
struct FontInfo {
    // The number of glyphs in the font.
    int glyph_count;

    // The ascender of the font, a Y coordinate, normally positive.
    int ascender;

    // The descender of the font, a Y coordinate, normally negative.
    int descender;

    // The vertical distance between two lines of text.
    int height;
};

////////////////////////////////////////////////////////////////////////

/// An instance of a font at a particular size.
class FontInstance {
private:
    int m_refcount;

protected:
    FontInfo m_info;

public:
    FontInstance();
    FontInstance(const FontInstance &) = delete;
    FontInstance &operator=(const FontInstance &) = delete;

    static void incref(FontInstance *p);
    static void decref(FontInstance *p);

    /// Get the font instance which is closest to the requested
    /// description.
    static Ref<FontInstance> get_font(const FontDesc &desc);

    /// Get the font info.
    const FontInfo &info() const { return m_info; }

    /// Map array of code points to array of glyph indexes.
    virtual void map_glyphs(int *glyph_index, const int *code_point,
                            std::size_t count) = 0;

    /// Get bitmap information for each glyph in the array.
    virtual void glyph_info(Bitmap *bitmap, const int *glyph_index,
                            std::size_t count) = 0;

    /// Draw glyphs into a pixbuf.
    virtual void draw_glyphs(Pixbuf &pixbuf, const GlyphDraw *glyph_draw,
                             std::size_t count) = 0;

    // Compute the glyph position for a run of text.  The glyph_pos
    // array will get one element for each glyph.  The final position
    // is returned.
    virtual int layout_run(GlyphPos *glyph_pos, const int *glyph_index,
                           std::size_t count, int pos) = 0;

    /// Get the HarfBuzz hb_font_t pointer for this font.
    virtual void *hb_font() = 0;

protected:
    virtual ~FontInstance();
};

inline FontInstance::FontInstance() : m_refcount(0) {
}

inline FontInstance::~FontInstance() {
}

inline void FontInstance::incref(FontInstance *p) {
    p->m_refcount++;
}

inline void FontInstance::decref(FontInstance *p) {
    p->m_refcount--;
    if (!p->m_refcount) {
        delete p;
    }
}

////////////////////////////////////////////////////////////////////////

/// Record for a glyph used in a layout but without a bitmap.
struct MissingGlyph {
    unsigned short bitmap;
    unsigned short font;
    int glyph;
};

struct FontEntry {
    Ref<FontInstance> font;

    // The index in the bitmap table of the first glyph in this font.
    int first;

    // The number of glyphs in this font.
    int count;
};

const int MEMBER_NONE = -1;
const int MEMBER_LAYOUT = -2;

/// The data for a batch.
struct BatchEntry {
    /// The range of vertexes in this batch.
    Range range;

    /// Whether the batch has active references.
    bool active;
};

/// A bitmap element in a layout.
struct Quad {
    /// The bitmap index.
    unsigned short bitmap;

    /// The lower-left corner where the bitmap will be drawn.
    I16Vec2 loc;

    /// The style for drawing this quad.  The meaning of this is
    /// determined by the shader program.
    I16Vec2 style;
};

/// Membership of layout data in batches and layouts.
struct Membership {
    /// If non-negative, then this is the index of the batch to which
    /// the glyphs belong.  If MEMBER_NONE, then this record is free.
    /// If MEMBER_LAYOUT, then this member is being used by a Layout
    /// object.
    int batch;

    /// The range of glyphs in the glyph array.
    Range range;

    /// The offset to add to the glyph locations.
    I16Vec2 offset;
};

/// A run of glyphs which use the same font.
struct FontRun {
    /// The index of the font in the Core object.
    int font_index;

    /// The array index of the first glyph in the run.
    int first;

    /// The number of glyphs in the run.
    int count;
};

////////////////////////////////////////////////////////////////////////

/// Instance of the typesetting system.
class Core {
private:
    int m_refcount;

    static const int BITMAP_MISSING = 0xffff;

public:
    // Whether the batch data has changed.
    bool m_dirty;

    // Whether the resources have been initialized.
    bool m_initialized;

    // All fonts being used in the system.
    std::vector<FontEntry> m_font;

    // Map from glyph indexes to bitmap indexes.
    std::vector<unsigned short> m_glyph_bitmap;

    // Map from bitmap indexes to texture locations.
    std::vector<Bitmap> m_bitmaps;

    // List of glyphs used in layouts but without bitmaps.
    std::vector<MissingGlyph> m_missing;

    // Free space in the texture for more bitmaps.
    Packing m_packing;

    // Pixbuf containing bitmap data.
    Pixbuf m_pixbuf;

    FontData m_fontdata;
    ArrayData m_arraydata;

    std::vector<BatchEntry> m_batches;
    std::vector<Quad> m_quads;
    std::vector<Membership> m_members;

    Core();
    Core(const Core &) = delete;
    Core &operator=(const Core &) = delete;
    ~Core();

    // Get the index for a font, adding it to the system if necessary.
    // The index is invalidated by sync().
    int get_font(FontInstance *font);

    /// Get the bitmaps for each of the specified glyphs.  Glyphs
    /// which are missing from the glyph texture will get allocated
    /// new bitmaps, but the texture won't contain these bitmaps until
    /// sync() is called.
    void glyph_bitmaps(FontInstance *font, const Range *runs,
                       std::size_t run_count,
                       unsigned short *bitmap, const int *glyph);
    /*
    // Create a new layout.
    void create_layout(Membership &membership, Metrics &metrics,
                       const std::string &text, int font_index,
                       int style_index);
    */
    // Synchronize with the renderer.
    void sync();

    // Create a new batch object and return its index.
    int new_batch();

    // Create a new membership record and return its index.
    int new_member();

    static void incref(Core *p);
    static void decref(Core *p);

private:
    // Garbage collect layout data.  Returns true if some data was
    // removed.
    bool gc_layout();

    /// Garbage collect bitmap.  Returns true if some data was
    /// removed.
    bool gc_bitmap();

    /// Garbage collect fonts.  Returns true if some data was removed.
    bool gc_font();

    /// Synchronize the glyph texture with the renderer.
    void sync_glyphs();

    /// Synchronize the batch data with the renderer.
    void sync_batches();
};

inline void Core::incref(Core *p) {
    p->m_refcount++;
}

inline void Core::decref(Core *p) {
    p->m_refcount--;
    if (!p->m_refcount) {
        delete p;
    }
}

////////////////////////////////////////////////////////////////////////

}
}
