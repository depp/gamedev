/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "private.hpp"

namespace SG {
namespace Text {

Layout::Layout(const Layout &other)
    : m_core(other.m_core) {
    if (m_core) {
        m_index = m_core->new_member();
        m_metrics = other.m_metrics;
        m_core->m_members[m_index] = m_core->m_members[other.m_index];
    }
}

Layout::~Layout() {
    if (m_core) {
        m_core->m_members[m_index].batch = MEMBER_NONE;
        m_core->m_dirty = true;
    }
}

Layout &Layout::operator=(const Layout &other) {
    return *this = Layout(other);
}

}
}
