/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "private.hpp"

namespace SG {
namespace Text {

Batch::Batch(const System &sys)
    : m_core(sys.m_core),
      m_index(m_core->new_batch()) {
    BatchEntry &be = m_core->m_batches[m_index];
    be.active = true;
    Core::incref(m_core);
}

Batch::~Batch() {
    if (m_core) {
        m_core->m_batches[m_index].active = false;
        Core::decref(m_core);
    }
}

Range Batch::range() const {
    if (!m_core) {
        return Range{0, 0};
    }
    return m_core->m_batches[m_index].range;
}

int Batch::add(const Layout &layout) {
    return add(layout, IVec2::zero());
}

int Batch::add(const Layout &layout, IVec2 offset) {
    if (!m_core || layout.m_core != m_core) {
        return -1;
    }
    int index = m_core->new_member();
    const auto &m = m_core->m_members[layout.m_index];
    m_core->m_members[index] = Membership{
        m_index,
        m.range,
        static_cast<I16Vec2>(offset)
    };
    m_core->m_dirty = true;
    return index;
}

void Batch::set(int layout, IVec2 offset) {
    std::size_t i = static_cast<std::size_t>(layout);
    if (!m_core || layout < 0 || i >= m_core->m_members.size()) {
        return;
    }
    auto &m = m_core->m_members[i];
    if (m.batch != m_index) {
        return;
    }
    I16Vec2 off16 = static_cast<I16Vec2>(offset);
    if (m.offset != off16) {
        m.offset = off16;
        m_core->m_dirty = true;
    }
}

void Batch::remove(int layout) {
    std::size_t i = static_cast<std::size_t>(layout);
    if (!m_core || layout < 0 || i >= m_core->m_members.size()) {
        return;
    }
    auto &m = m_core->m_members[i];
    if (m.batch != m_index) {
        return;
    }
    m.batch = MEMBER_NONE;
    m_core->m_dirty = true;
}

}
}
