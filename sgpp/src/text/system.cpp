/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "private.hpp"
namespace SG {
namespace Text {

System::System()
    : m_core(new Core) {
}

System::~System() {
    Core::decref(m_core);
}

const FontData &System::font_data() const {
    return m_core->m_fontdata;
}

const ArrayData &System::array_data() const {
    return m_core->m_arraydata;
}

void System::sync() {
    m_core->sync();
}

}
}
