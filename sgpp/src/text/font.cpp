/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "private.hpp"
#include "sgpp/file.hpp"
#include "sgpp/log.hpp"

#include <vector>

namespace SG {
namespace Text {

namespace {

const char FONT_DIR[] = "font/";
const char FONT_EXTENSIONS[] = "otf:ttf:woff";
const size_t FONT_MAXSZ = 16 * 1024 * 1024;

const char STYLE_NAME[3][8] = {
    "normal",
    "italic",
    "oblique"
};

// Record of information about a font file.
struct FontFileRecord {
    // Path to the file.
    std::string path;
    // Information about the font.
    FontDesc desc;
    // The loaded font, if it is successfully loaded.
    FontFile *data;
    // Whether the font is loaded, even if loading failed.
    bool loaded;
};

bool g_scanned;
std::vector<FontFileRecord> g_fontfiles;

void scan_library() {
    std::string base(FONT_DIR);
    FileList list;
    if (!list.scan(base, SG_PATH_DATA, FONT_EXTENSIONS)) {
        return;
    }
    for (const sg_fileentry &e : list) {
        std::string rpath(e.path);
        Data data;
        if (!data.read(base + rpath, FONT_MAXSZ, FONT_EXTENSIONS)) {
            continue;
        }
        auto font = FontFile::load(std::move(data));
        if (!font) {
            continue;
        }
        g_fontfiles.push_back(
            FontFileRecord{std::move(rpath), font->desc(), nullptr, false});
    }
    for (const auto &e : g_fontfiles) {
        Log::debug("%s: family: %s, weight: %d, style: %s",
                   e.path.c_str(), e.desc.family.c_str(), e.desc.weight,
                   STYLE_NAME[static_cast<int>(e.desc.style)]);
    }
    g_scanned = true;
}

static const int LIST_SIZE = 32;

struct FontList {
    FontFileRecord *rec[LIST_SIZE];
    int count;
};

void get_family(FontList &s, const std::string &family) {
    int n = 0;
    for (auto &e : g_fontfiles) {
        if (e.desc.family != family) {
            continue;
        }
        s.rec[n] = &e;
        n++;
        if (n == LIST_SIZE) {
            break;
        }
    }
    if (n == 0) {
        for (auto &e : g_fontfiles) {
            s.rec[n] = &e;
            n++;
            if (n == LIST_SIZE) {
                break;
            }
        }
    }
    s.count = n;
}

void filter_style(FontList &s, FontStyle style) {
    int n = s.count;
    FontStyle styles[3];
    styles[0] = style;
    switch (style) {
    default:
    case FontStyle::NORMAL:
        styles[1] = FontStyle::OBLIQUE;
        styles[1] = FontStyle::ITALIC;
        break;
    case FontStyle::ITALIC:
        styles[1] = FontStyle::OBLIQUE;
        styles[1] = FontStyle::NORMAL;
        break;
    case FontStyle::OBLIQUE:
        styles[1] = FontStyle::ITALIC;
        styles[1] = FontStyle::NORMAL;
        break;
    }
    for (const auto sstyle : styles) {
        int m = 0;
        for (int i = 0; i < n; i++) {
            if (s.rec[i]->desc.style == sstyle) {
                s.rec[m] = s.rec[i];
                m++;
            }
        }
        if (m > 0) {
            s.count = m;
            return;
        }
    }
}

FontFileRecord *match_weight(FontList &s, int weight) {
    int n = s.count;
    for (int i = 0; i < n; i++) {
        int w = s.rec[i]->desc.weight;
        if (w == weight) {
            return s.rec[i];
        }
    }
    if (weight >= 400 && weight <= 500) {
        for (int i = 0; i < n; i++) {
            int w = s.rec[i]->desc.weight;
            if (w >= 400 && w <= 500) {
                return s.rec[i];
            }
        }
    }
    int best = 0;
    FontFileRecord *rec = nullptr;
    if (weight <= 500) {
        for (int i = 0; i < n; i++) {
            int w = s.rec[i]->desc.weight;
            if (w > 500) {
                continue;
            }
            if (!rec || w > best) {
                best = w;
                rec = s.rec[i];
            }
        }
        if (rec) {
            return rec;
        }
        for (int i = 0; i < n; i++) {
            int w = s.rec[i]->desc.weight;
            if (!rec || w < best) {
                best = w;
                rec = s.rec[i];
            }
        }
    } else {
        for (int i = 0; i < n; i++) {
            int w = s.rec[i]->desc.weight;
            if (w <= 500) {
                continue;
            }
            if (!rec || w < best) {
                best = w;
                rec = s.rec[i];
            }
        }
        if (rec) {
            return rec;
        }
        for (int i = 0; i < n; i++) {
            int w = s.rec[i]->desc.weight;
            if (!rec || w > best) {
                best = w;
                rec = s.rec[i];
            }
        }
    }
    return rec;
}

}

FontFile::~FontFile() {
    for (auto &e : g_fontfiles) {
        if (e.data == this) {
            e.data = nullptr;
            e.loaded = false;
            break;
        }
    }
}

Ref<FontFile> FontFile::get_font(const FontDesc &desc) {
    if (!g_scanned) {
        scan_library();
    }

    FontFileRecord *rec;
    {
        FontList search;
        get_family(search, desc.family);
        filter_style(search, desc.style);
        rec = match_weight(search, desc.weight);
    }

    if (!rec) {
        return Ref<FontFile>();
    }

    if (rec->loaded) {
        return Ref<FontFile>(rec->data);
    }

    rec->loaded = true;
    std::string path(FONT_DIR);
    path += rec->path;
    Data data;
    if (!data.read(path, FONT_MAXSZ, FONT_EXTENSIONS)) {
        return Ref<FontFile>();
    }
    auto font = load(std::move(data));
    if (!font) {
        return Ref<FontFile>();
    }
    rec->data = font.get();
    return Ref<FontFile>(font.release());
}

std::unique_ptr<FontFile> FontFile::load(SG::Data data) {
    return FontFile::load_freetype(std::move(data));
}

////////////////////////////////////////////////////////////////////////

Ref<FontInstance> FontInstance::get_font(const FontDesc &desc) {
    auto font = FontFile::get_font(desc);
    if (!font) {
        return Ref<FontInstance>();
    }
    return font->instantiate(desc.size);
}

////////////////////////////////////////////////////////////////////////

FontDesc::FontDesc()
    : family(),
      size(16.0f),
      weight(400),
      style(FontStyle::NORMAL) {
}

bool operator==(const FontDesc &x, const FontDesc &y) {
    return (x.family == y.family &&
            x.size   == y.size   &&
            x.weight == y.weight &&
            x.style  == y.style);
}

bool operator!=(const FontDesc &x, const FontDesc &y) {
    return !(x == y);
}

}
}
