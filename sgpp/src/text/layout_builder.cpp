/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "private.hpp"
#include "sgpp/log.hpp"
#include "sgpp/utf8.hpp"

#include <hb.h>

namespace SG {
namespace Text {

namespace {

// Penalty for each line break.  Each line break incurs this penalty.
const float PENALTY_BREAK = 1.0f;

// Penalty for ragged lines not matching the target width.  Let A be
// the difference between the line width and target width, measured in
// line heights.  That difference is squared and multiplied by this
// penalty.
const float PENALTY_RAGGED = 20.0f;

// Penalty for overwide lines.  Each line break wider than the target
// width incurs this penalty.
const float PENALTY_OVERWIDE = 2000.0f;

// Both options have drawbacks.
IVec2 hb_pixels(int x, int y) {
#if 1
    return IVec2{{(x + 32) >> 6, (y + 32) >> 6}};
#else
    return IVec2{{x >> 6, y >> 6}};
#endif
}

// Get the advance from a glyph position.
IVec2 hb_advance(const hb_glyph_position_t &hpos) {
    return hb_pixels(hpos.x_advance, hpos.y_advance);
}

// Get the offset from a glyph position.
IVec2 hb_offset(const hb_glyph_position_t &hpos) {
    return hb_pixels(hpos.x_offset, hpos.y_advance);
}

}

LayoutBuilder::LayoutBuilder() : m_width(0), m_style(0), m_hb_buffer(nullptr) {}

LayoutBuilder::LayoutBuilder(LayoutBuilder &&other)
    : m_text(std::move(other.m_text)),
      m_font_desc(std::move(other.m_font_desc)),
      m_width(other.m_width),
      m_style(other.m_style),
      m_hb_buffer(other.m_hb_buffer) {
    other.m_hb_buffer = nullptr;
}

LayoutBuilder::~LayoutBuilder() {
    if (m_hb_buffer) {
        hb_buffer_destroy(static_cast<hb_buffer_t *>(m_hb_buffer));
    }
}

LayoutBuilder &LayoutBuilder::operator=(LayoutBuilder &&other) {
    m_text = std::move(other.m_text);
    m_font_desc = std::move(other.m_font_desc);
    m_width = other.m_width;
    m_style = other.m_style;
    std::swap(m_hb_buffer, other.m_hb_buffer);
    return *this;
}

void LayoutBuilder::clear() {
    m_text.clear();
    m_font_desc = FontDesc();
    m_width = 0;
    m_style = 0;
}

void LayoutBuilder::add_text(const std::string &text) {
    bool success = utf8_decode(m_text, text.data(), text.size());
    if (!success) {
        Log::warn("Invalid UTF-8 text added to layout.");
    }
}

void LayoutBuilder::end_line() {
    // U+2028 LINE SEPARATOR
    m_text.push_back(0x2028);
}

void LayoutBuilder::end_paragraph() {
    // U+2029 PARAGRAPH SEPARATOR
    m_text.push_back(0x2029);
}

void LayoutBuilder::set_font(const FontDesc &font_desc) {
    m_font_desc = font_desc;
}

void LayoutBuilder::set_width(float width) {
    if (width > 0.0f) {
        m_width = static_cast<int>(width + 0.5f);
    } else {
        m_width = 0;
    }
}

void LayoutBuilder::set_style(int style_index) {
    m_style = style_index;
}

enum class BreakType {
    // The line break is optional, and starts a new paragraph if chosen.
    OPTIONAL,

    // The line break is mandatory, but does not start a new paragraph.
    LINE,

    // The line break is mandatory, and starts a new paragraph.
    PARAGRAPH
};

struct LineBreak {
    // The type of the break opportunity.
    BreakType type;

    // Glyph index of the first glyph after the break.
    int offset;

    // Advance, in pixels, of the break.
    int advance;

    // Break index of the previous line break that should be chosen,
    // if this break is chosen.
    int previous;

    // Total penalty for choosing this line break, including penalties
    // for previous line breaks.
    float penalty;

    // Ascender for the preceding line, normally positive.
    short ascender;

    // Descender for the preceding line, normally negative.
    short descender;

    // Advance for trailing whitespace before the break.
    short trailing_whitespace;

    // Location of the previous line.
    I16Vec2 pos;
};

BreakType break_type(char32_t codepoint) {
    switch (codepoint) {
    case '\n': // U+000A Line Feed
        return BreakType::PARAGRAPH;
    case '\v': // U+000B Line Tabulation
        return BreakType::LINE;
    case '\f': // U+000C Form Feed
        return BreakType::PARAGRAPH;
    case '\r': // U+000D Carriage Return
        return BreakType::PARAGRAPH;
    case 0x0085: // U+0085 Ne
        return BreakType::PARAGRAPH;
    case 0x2028: // U+2028 Line Separator
        return BreakType::LINE;
    case 0x2029: // U+2029 Paragraph Separator
        return BreakType::PARAGRAPH;
    default:
        return BreakType::OPTIONAL;
    }
}

Layout LayoutBuilder::typeset(System &system) {
    hb_buffer_t *hbuf = static_cast<hb_buffer_t *>(m_hb_buffer);
    if (!hbuf) {
        hbuf = hb_buffer_create();
        m_hb_buffer = hbuf;
    }
    Core *core = system.m_core;
    auto font = FontInstance::get_font(m_font_desc);

    // Shape text, converting UTF-8 bytes to glyphs
    hb_font_t *hfont = static_cast<hb_font_t *>(font->hb_font());
    hb_buffer_clear_contents(hbuf);
    hb_buffer_set_direction(hbuf, HB_DIRECTION_LTR);
    hb_buffer_set_script(hbuf, HB_SCRIPT_LATIN);
    hb_buffer_set_language(hbuf, hb_language_from_string("en", 2));
    hb_buffer_add_codepoints(hbuf,
                             reinterpret_cast<const unsigned *>(m_text.data()),
                             m_text.size(), 0, m_text.size());
    hb_shape(hfont, hbuf, nullptr, 0);
    unsigned nglyph;
    const hb_glyph_info_t *glyph_info =
        hb_buffer_get_glyph_infos(hbuf, &nglyph);
    const hb_glyph_position_t *glyph_pos =
        hb_buffer_get_glyph_positions(hbuf, &nglyph);
    std::vector<int> glyph_index(nglyph);
    for (std::size_t i = 0; i < nglyph; i++) {
        glyph_index[i] = glyph_info[i].codepoint;
    }

    // Calculate line breaks
    std::vector<LineBreak> line_breaks;
    std::vector<unsigned char> attr(m_text.size());
    analyze_line_break(attr.data(), m_text.data(), m_text.size());

    {
        LineBreak br{
            BreakType::PARAGRAPH, 0, 0, 0, 0.0f, 0, 0, 0, {{0, 0}}};
        line_breaks.push_back(br);
        IVec2 pos{{0, 0}}, pos2{{0, 0}};
        unsigned bflag = Attr::LB_MUST_BREAK;
        if (m_width > 0) {
            bflag |= Attr::LB_CAN_BREAK;
        }
        for (std::size_t i = 0; i < nglyph; i++) {
            unsigned a = attr[glyph_info[i].cluster];
            if ((a & bflag) != 0) {
                br.type = break_type(m_text[glyph_info[i].cluster]);
                br.offset = i;
                br.advance = pos[0];
                br.trailing_whitespace = pos[0] - pos2[0];
                line_breaks.push_back(br);
            }
            pos += hb_advance(glyph_pos[i]);
            if ((a & Attr::LB_WHITE) == 0) {
                pos2 = pos;
            }
        }
        br.type = BreakType::PARAGRAPH;
        br.offset = nglyph;
        br.advance = pos[0];
        br.trailing_whitespace = pos[0] - pos2[0];
        line_breaks.push_back(br);
    }

    {
        int ascender, descender;
        {
            const auto &info = font->info();
            ascender = info.ascender;
            descender = info.descender;
            ascender += (info.height - (ascender - descender)) >> 1;
            descender = ascender - info.height;
        }
        int target_width = m_width;

        // Choose line breaks.  Mark chosen line breaks by changing
        // their types from BreakType::OPTIONAL to BreakType::LINE.
        // Uses the algorithm from Knuth and Plass (1981).
        for (std::size_t i = 1, n = line_breaks.size(); i < n; i++) {
            LineBreak br = line_breaks[i];
            int advance = br.advance - br.trailing_whitespace;
            br.penalty = std::numeric_limits<float>::infinity();
            for (std::size_t j = i; j > 0; j--) {
                const LineBreak &pbr = line_breaks[j - 1];
                int width = advance - pbr.advance;
                int delta = width > 0 ? target_width - width : 0;
                float pdelta = static_cast<float>(delta) /
                               static_cast<float>(ascender - descender);
                float penalty = pbr.penalty + PENALTY_BREAK;
                if (br.type == BreakType::PARAGRAPH) {
                    if (width > target_width) {
                        penalty += pdelta * pdelta * PENALTY_RAGGED;
                        penalty += PENALTY_OVERWIDE;
                    }
                } else {
                    penalty += pdelta * pdelta * PENALTY_RAGGED;
                    if (width > target_width) {
                        penalty += PENALTY_OVERWIDE;
                    }
                }
                if (penalty < br.penalty) {
                    br.previous = j - 1;
                    br.penalty = penalty;
                    br.ascender = ascender;
                    br.descender = descender;
                    short ydelta = static_cast<short>(pbr.descender - ascender);
                    br.pos = pbr.pos + I16Vec2{{0, ydelta}};
                }
                if (pbr.type != BreakType::OPTIONAL) {
                    break;
                }
            }
            line_breaks[i] = br;
            if (br.type != BreakType::OPTIONAL) {
                for (int j = br.previous;
                     line_breaks[j].type == BreakType::OPTIONAL;
                     j = line_breaks[j].previous) {
                    line_breaks[j].type = BreakType::LINE;
                }
                line_breaks[i].penalty = 0.0f;
            }
        }
        line_breaks.erase(
            std::remove_if(std::begin(line_breaks), std::end(line_breaks),
                           [](const LineBreak &b) {
                               return b.type == BreakType::OPTIONAL;
                           }),
            std::end(line_breaks));
    }

    // Get the bitmap for each glyph.
    std::vector<unsigned short> bitmap(nglyph);
    Range run{0, static_cast<int>(nglyph)};
    core->glyph_bitmaps(font.get(), &run, 1, bitmap.data(), glyph_index.data());

    Layout layout;

    // Calculate metrics, write glyph location data.
    std::size_t quad_first = core->m_quads.size();
    {
        const auto *bitmaps = core->m_bitmaps.data();
        const int MAX = std::numeric_limits<int>::max(),
                  MIN = std::numeric_limits<int>::min();
        int px0 = MAX, py0 = MAX, px1 = MIN, py1 = MIN;
        int lx0 = MAX, ly0 = MAX, lx1 = MIN, ly1 = MIN;
        I16Vec2 style{{static_cast<short>(m_style), 0}};
        int nbreaks = static_cast<int>(line_breaks.size());
        for (int lineno = 1, glyphno = 0; lineno < nbreaks; lineno++) {
            const LineBreak &br = line_breaks[lineno];
            IVec2 pos = static_cast<IVec2>(br.pos);
            int line_end = br.offset;
            {
                int width = br.advance - br.trailing_whitespace -
                            line_breaks[lineno - 1].advance;
                lx0 = std::min(lx0, pos[0]);
                lx1 = std::max(lx1, pos[0] + width);
                ly0 = std::min(ly0, pos[1] + br.descender);
                ly1 = std::max(ly1, pos[1] + br.ascender);
            }
            for (; glyphno < line_end; glyphno++) {
                unsigned short bi = bitmap[glyphno];
                const auto &hpos = glyph_pos[glyphno];
                IVec2 adv = hb_advance(hpos), off = hb_offset(hpos);
                if (bi != 0) {
                    IVec2 gpos =
                        pos + off + static_cast<IVec2>(bitmaps[bi].offset);
                    I16Vec2 size = bitmaps[bi].size;
                    px0 = std::min(px0, gpos[0]);
                    px1 = std::max(px1, gpos[0] + size[0]);
                    py0 = std::min(py0, gpos[1]);
                    py1 = std::max(py1, gpos[1] + size[1]);
                    core->m_quads.push_back(
                        Quad{bi, static_cast<I16Vec2>(gpos), style});
                }
                pos += adv;
            }
        }
        std::size_t quad_count = core->m_quads.size() - quad_first;
        if (!quad_count) {
            return layout;
        }
        int member_index = core->new_member();
        core->m_members[member_index] = Membership{
            MEMBER_LAYOUT,
            {static_cast<int>(quad_first), static_cast<int>(quad_count)},
            I16Vec2::zero()};
        layout.m_core = core;
        layout.m_index = member_index;
        layout.m_metrics = Metrics{{{{lx0, ly0}}, {{lx1, ly1}}},
                                   {{{px0, py0}}, {{px1, py1}}},
                                   line_breaks[1].pos[1]};
    }

    return layout;
}

}
}
