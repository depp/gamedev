/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sgpp/text.hpp"

namespace SG {
namespace Text {

IVec2 Metrics::point(HAlign halign, VAlign valign) const {
    IVec2 pt;

    switch (halign) {
    case HAlign::LEFT:
        pt[0] = logical_bounds.mins[0];
        break;

    case HAlign::CENTER:
        pt[0] = logical_bounds.mins[0] +
            ((logical_bounds.maxs[0] - logical_bounds.mins[0]) >> 1);
        break;

    case HAlign::RIGHT:
        pt[0] = logical_bounds.maxs[0];
        break;
    }

    switch (valign) {
    case VAlign::BASELINE:
        pt[1] = baseline;
        break;

    case VAlign::TOP:
        pt[1] = logical_bounds.maxs[1];
        break;

    case VAlign::CENTER:
        pt[1] = logical_bounds.mins[1] +
            ((logical_bounds.maxs[1] - logical_bounds.mins[1]) >> 1);
        break;

    case VAlign::BOTTOM:
        pt[1] = logical_bounds.mins[1];
        break;
    }

    return pt;
}

}
}
