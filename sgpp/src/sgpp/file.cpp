/* Copyright 2013-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sgpp/file.hpp"

#include <cstdlib>

#include "sg/log.h"
#include "sgpp/error.hpp"

namespace SG {

bool Data::read(const std::string &path, size_t maxsz) {
    return read(path, maxsz, nullptr);
}

bool Data::read(const std::string &path, size_t maxsz,
                const char *extensions) {
    Data data;
    Error err;
    int r = sg_file_load(&data.m_data, path.data(), path.size(), 0,
                         extensions, maxsz, nullptr, err.ptr());
    if (r) {
        sg_logerrf(SG_LOG_ERROR, err.get(),
                   "%s: could not read file", path.c_str());
        return false;
    }
    std::swap(m_data, data.m_data);
    return true;
}

bool FileList::scan(const std::string &path, unsigned flags) {
    return scan(path, flags, nullptr);
}

bool FileList::scan(const std::string &path, unsigned flags,
                    const char *extensions) {
    FileList list;
    Error err;
    int r = sg_filelist_scan(&list.m_list, path.data(), path.size(), flags,
                             extensions, err.ptr());
    if (r) {
        sg_logerrf(SG_LOG_ERROR, err.get(),
                   "%s: could not list files", path.c_str());
        return false;
    }
    std::swap(m_list, list.m_list);
    return true;
}

}
