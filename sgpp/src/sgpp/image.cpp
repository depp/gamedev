/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sgpp/image.hpp"

#include <cstdlib>
#include <cstring>

#include "sg/entry.h"
#include "sg/error.h"
#include "sg/log.h"
#include "sg/util.h"
#include "sggl/3_0.h"
#include "sgpp/file.hpp"
#include "sgpp/opengl.hpp"

using namespace gl_3_0;
namespace SG {

////////////////////////////////////////////////////////////////////////

Image::Image() : m_image(nullptr) {}

Image::Image(Image &&other) : m_image(nullptr) {
    std::swap(m_image, other.m_image);
}

Image::~Image() {
    if (m_image) {
        m_image->free(m_image);
    }
}

Image &Image::operator=(Image &&other) {
    std::swap(m_image, other.m_image);
    return *this;
}

bool Image::load(const std::string &path) {
    struct sg_error *err = nullptr;
    sg_image *img = sg_image_file(path.data(), path.size(), &err);
    if (!img) {
        sg_logerrf(SG_LOG_ERROR, err,
                   "%s: could not load image", path.c_str());
        sg_error_clear(&err);
        return false;
    }
    if (m_image) {
        m_image->free(m_image);
    }
    m_image = img;
    return true;
}

void Image::draw(Pixbuf &buf, int x, int y) const {
    if (!m_image) {
        return;
    }
    m_image->draw(m_image, &buf.m_pixbuf, x, y, nullptr);
}

void Image::draw(sg_pixbuf &buf, int x, int y) const {
    if (!m_image) {
        return;
    }
    m_image->draw(m_image, &buf, x, y, nullptr);
}

////////////////////////////////////////////////////////////////////////

Pixbuf::Pixbuf() {
    m_pixbuf.data = nullptr;
    m_pixbuf.format = SG_RGBA;
    m_pixbuf.width = 0;
    m_pixbuf.height = 0;
    m_pixbuf.rowbytes = 0;
}

Pixbuf::Pixbuf(Pixbuf &&other) {
    auto &p = other.m_pixbuf;
    m_pixbuf = p;
    p.data = nullptr;
    p.format = SG_RGBA;
    p.width = 0;
    p.height = 0;
    p.rowbytes = 0;
}

Pixbuf::~Pixbuf() {
    std::free(m_pixbuf.data);
}

Pixbuf &Pixbuf::operator=(Pixbuf &&other) {
    std::swap(m_pixbuf, other.m_pixbuf);
    return *this;
}

void Pixbuf::alloc(sg_pixbuf_format_t format, int width, int height) {
    std::free(m_pixbuf.data);
    m_pixbuf.data = nullptr;
    int r = sg_pixbuf_alloc(&m_pixbuf, format, width, height, nullptr);
    if (r) {
        sg_sys_abort("Pixbuf::alloc");
    }
}

void Pixbuf::calloc(sg_pixbuf_format_t format, int width, int height) {
    std::free(m_pixbuf.data);
    m_pixbuf.data = nullptr;
    int r = sg_pixbuf_calloc(&m_pixbuf, format, width, height, nullptr);
    if (r) {
        sg_sys_abort("Pixbuf::calloc");
    }
}

bool Pixbuf::write_png(const std::string &path) const {
    if (!m_pixbuf.data) {
        sg_sys_abort("Pixbuf::write_png");
    }
    sg_error *err = nullptr;
    int r = sg_pixbuf_writepng(
        const_cast<sg_pixbuf *>(&m_pixbuf), path.data(), path.size(),
        SG_PATH_DATA, &err);
    if (r) {
        sg_logerrf(SG_LOG_ERROR, err,
                   "%s: could not write image", path.c_str());
        sg_error_clear(&err);
        return false;
    }
    return true;
}

////////////////////////////////////////////////////////////////////////

Texture::Texture()
    : tex(0),
      texsize(IVec2::zero()),
      imgsize(IVec2::zero()),
      texscale(Vec2::zero()) {}

Texture::Texture(Texture &&other)
    : tex(other.tex),
      texsize(other.texsize),
      imgsize(other.imgsize),
      texscale(other.texscale) {
    other.tex = 0;
    other.texsize = IVec2::zero();
    other.imgsize = IVec2::zero();
    other.texscale = Vec2::zero();
}

Texture::~Texture() {
    if (tex) {
        glDeleteTextures(1, &tex);
    }
}

Texture &Texture::operator=(Texture &&other) {
    if (this != &other) {
        std::swap(tex, other.tex);
        std::swap(texsize, other.texsize);
        std::swap(imgsize, other.imgsize);
        std::swap(texscale, other.texscale);
    }
    return *this;
}

namespace {

struct TextureFormat {
    GLenum internal_format;
    GLenum format;
    GLenum type;
};

TextureFormat texture_format(sg_pixbuf_format_t pfmt, unsigned flags) {
    static const GLenum INTERNAL_FORMATS[4][SG_PIXBUF_NFORMAT] = {
        { GL_R8, GL_RG8, GL_RGB8, GL_RGBA8 },
        { GL_R8, GL_RG8, GL_RGB8, GL_RGBA8 },
        { GL_R8, GL_RG8, GL_RGB8, GL_RGBA8 },
        { GL_R8, GL_RG8, GL_RGB8, GL_RGBA8 }
    };

    static const GLenum NORMALIZED_FORMATS[SG_PIXBUF_NFORMAT] = {
        GL_RED, GL_RG, GL_RGBA, GL_RGBA
    };

    static const GLenum INTEGER_FORMATS[SG_PIXBUF_NFORMAT] = {
        GL_RED_INTEGER, GL_RG_INTEGER, GL_RGBA_INTEGER, GL_RGBA_INTEGER
    };

    TextureFormat fmt;
    unsigned mtype = flags & Texture::MASK_TYPE;
    if (mtype == Texture::TYPE_SRGB) {
        if (pfmt == SG_RGBA) {
            fmt.internal_format = GL_SRGB8_ALPHA8;
        } else {
            fmt.internal_format = GL_SRGB8;
        }
        fmt.format = NORMALIZED_FORMATS[pfmt];
        fmt.type = GL_UNSIGNED_BYTE;
    } else {
        fmt.internal_format = INTERNAL_FORMATS[mtype - 1][pfmt];
        switch (mtype) {
        default:
        case Texture::TYPE_UNORM:
            fmt.format = NORMALIZED_FORMATS[pfmt];
            fmt.type = GL_UNSIGNED_BYTE;
            break;
        case Texture::TYPE_UINT:
            fmt.format = INTEGER_FORMATS[pfmt];
            fmt.type = GL_UNSIGNED_BYTE;
            break;
        case Texture::TYPE_SNORM:
            fmt.format = NORMALIZED_FORMATS[pfmt];
            fmt.type = GL_BYTE;
            break;
        case Texture::TYPE_SINT:
            fmt.format = INTEGER_FORMATS[pfmt];
            fmt.type = GL_BYTE;
            break;
        }
    }
    return fmt;
}

void setup_texture(GLenum target, unsigned flags) {
    GLenum min, mag;
    if (flags & Texture::FILTER_NEAREST) {
        if (flags & Texture::NO_MIPMAP) {
            min = GL_NEAREST;
        } else {
            min = GL_NEAREST_MIPMAP_NEAREST;
        }
        mag = GL_NEAREST;
    } else {
        if (flags & Texture::NO_MIPMAP) {
            min = GL_LINEAR;
        } else {
            min = GL_LINEAR_MIPMAP_LINEAR;
        }
        mag = GL_LINEAR;
    }
    if (!(flags & Texture::NO_MIPMAP)) {
        glGenerateMipmap(target);
    }
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, min);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, mag);
    if (flags & Texture::CLAMP) {
        switch (target) {
        case GL_TEXTURE_3D:
            glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            // fall through
        case GL_TEXTURE_2D:
        case GL_TEXTURE_2D_ARRAY:
            glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            // fall through
        case GL_TEXTURE_1D:
        case GL_TEXTURE_1D_ARRAY:
            glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            // fall through
        default:
            break;
        }
    }
}

}

// FIXME: use GL_ARB_texture_storage

bool Texture::load_1d(const std::string &path, unsigned flags) {
    Image image;
    if (!image.load(path)) {
        return false;
    }
    return load_1d(path, flags);
}

bool Texture::load_1d(const Image &image, unsigned flags) {
    IVec2 isize = image.size();
    IVec2 tsize{{(int) sg_round_up_pow2_32(isize[0]), 1}};
    sg_pixbuf_format_t pfmt = image.has_alpha() ? SG_RGBA : SG_RGBX;
    Pixbuf pixbuf;
    if (isize != tsize) {
        pixbuf.calloc(pfmt, tsize[0], 1);
    } else {
        pixbuf.alloc(pfmt, tsize[0], 1);
    }
    image.draw(pixbuf, 0, 0);
    bool success = load_1d(pixbuf, flags);
    if (!success) {
        return false;
    }
    imgsize = isize;
    return true;
}

bool Texture::load_1d(const Pixbuf &pixbuf, unsigned flags) {
    DebugGroup dbg("Texture::load_1d");

    const auto &p = pixbuf.m_pixbuf;
    if (!tex) {
        glGenTextures(1, &tex);
    }
    glBindTexture(GL_TEXTURE_1D, tex);
    auto fmt = texture_format(p.format, flags);
    glTexImage1D(GL_TEXTURE_1D, 0, fmt.internal_format,
                 p.width, 0, fmt.format, fmt.type, p.data);
    setup_texture(GL_TEXTURE_1D, flags);
    glBindTexture(GL_TEXTURE_1D, 0);
    imgsize = texsize = IVec2{{p.width, 1}};
    texscale = Vec2{{1.0f / (float) texsize[0], 1.0f}};
    return true;
}

bool Texture::load_2d(const std::string &path, unsigned flags) {
    Image image;
    if (!image.load(path)) {
        return false;
    }
    return load_2d(path, flags);
}

bool Texture::load_2d(const Image &image, unsigned flags) {
    IVec2 isize = image.size();
    IVec2 tsize{{(int) sg_round_up_pow2_32(isize[0]),
                 (int) sg_round_up_pow2_32(isize[1])}};
    sg_pixbuf_format_t pfmt = image.has_alpha() ? SG_RGBA : SG_RGBX;
    Pixbuf pixbuf;
    if (isize != tsize) {
        pixbuf.calloc(pfmt, tsize[0], tsize[1]);
    } else {
        pixbuf.alloc(pfmt, tsize[0], tsize[1]);
    }
    image.draw(pixbuf, 0, 0);
    bool success = load_2d(pixbuf, flags);
    if (!success) {
        return false;
    }
    imgsize = isize;
    return true;
}

bool Texture::load_2d(const Pixbuf &pixbuf, unsigned flags) {
    DebugGroup dbg("Texture::load_2d");

    const auto &p = pixbuf.m_pixbuf;
    if (!tex) {
        glGenTextures(1, &tex);
    }
    glBindTexture(GL_TEXTURE_2D, tex);
    auto fmt = texture_format(p.format, flags);
    glTexImage2D(GL_TEXTURE_2D, 0, fmt.internal_format,
                 p.width, p.height, 0, fmt.format, fmt.type, p.data);
    setup_texture(GL_TEXTURE_2D, flags);
    glBindTexture(GL_TEXTURE_2D, 0);
    imgsize = texsize = IVec2{{p.width, p.height}};
    texscale = Vec2{{1.0f / (float) texsize[0], 1.0f / (float) texsize[1]}};
    return true;
}

}
