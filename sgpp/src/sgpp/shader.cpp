/* Copyright 2013-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sgpp/shader.hpp"

#include <cstring>

#include "sgpp/file.hpp"
#include "sgpp/log.hpp"

using namespace gl_2_0;
namespace SG {

std::string shader_path("shader/");

namespace {

const std::size_t MAX_SHADER_SIZE = 64 * 1024;

GLint &get_field(void *object, std::size_t offset) {
    return *reinterpret_cast<GLint *>(
        static_cast<char *>(object) + offset);
}

struct ShaderTypeInfo {
    char ext[5];
    GLenum type;
};

const ShaderTypeInfo SHADER_TYPE[] = {
    { "comp", 0x91B9 /* GL_COMPUTE_SHADER */ },
    { "vert", 0x8B31 /* GL_VERTEX_SHADER */ },
    { "tesc", 0x8E88 /* GL_TESS_CONTROL_SHADER */ },
    { "tese", 0x8E87 /* GL_TESS_EVALUATION_SHADER */ },
    { "geom", 0x8DD9 /* GL_GEOMETRY_SHADER */ },
    { "frag", 0x8B30 /* GL_FRAGMENT_SHADER */ }
};

const ShaderTypeInfo *shader_type_info(GLenum type) {
    for (const auto &i : SHADER_TYPE) {
        if (i.type == type) {
            return &i;
        }
    }
    return nullptr;
}

/// OpenGL shader object wrapper.
class Shader {
private:
    GLint m_shader;
    std::string m_name;

public:
    Shader();
    Shader(const Shader &) = delete;
    ~Shader();
    Shader &operator=(const Shader &) = delete;

    /// Get the shader object.
    GLint shader() const { return m_shader; }

    /// Get the name of the shader.
    const std::string &name() const { return m_name; }

    /// Load a shader from a file, and return true if successful.
    bool load(GLenum type, const std::string &name);
};

Shader::Shader()
    : m_shader(0) {
}

Shader::~Shader() {
    if (m_shader) {
        glDeleteShader(m_shader);
    }
}

bool Shader::load(GLenum type, const std::string &name) {
    SG::Data data;
    {
        auto ifo = shader_type_info(type);
        if (!ifo) {
            Log::error("Invalid shader type.");
            return false;
        }
        std::string p;
        p.reserve(shader_path.size() + name.size() + 1);
        p.append(shader_path);
        p.push_back('/');
        p.append(name);
        if (!data.read(p, MAX_SHADER_SIZE, ifo->ext)) {
            return false;
        }
    }

    Shader s;
    s.m_shader = glCreateShader(type);
    if (!s.m_shader) {
        std::abort();
    }
    const char *src[1] = {static_cast<const char *>(data.data())};
    GLint srclen[1] = {static_cast<int>(data.size())};
    glShaderSource(s.m_shader, 1, src, srclen);
    glCompileShader(s.m_shader);
    GLint flag = 0;
    glGetShaderiv(s.m_shader, GL_COMPILE_STATUS, &flag);
    if (!flag) {
        Log::error("%s: Compilation failed.", data.path());
    }
    {
        GLint loglen = 0;
        glGetShaderiv(s.m_shader, GL_INFO_LOG_LENGTH, &loglen);
        if (loglen > 1) {
            std::vector<char> log(loglen + 1);
            log[loglen] = '\0';
            glGetShaderInfoLog(s.m_shader, loglen, nullptr, log.data());
            Log::warn("%s", log.data());
        }
    }
    if (!flag) {
        return false;
    }
    s.m_name = data.path();
    std::swap(m_shader, s.m_shader);
    std::swap(m_name, s.m_name);
    return true;
}

}

bool ProgramObj::load(const ProgramSpec &spec) {
    ProgramObj p;
    p.m_prog = glCreateProgram();
    if (!p.m_prog) {
        std::abort();
    }

    for (int i = 0, n = spec.attribs.size(); i < n; i++) {
        const char *attrib = spec.attribs[i];
        if (attrib) {
            glBindAttribLocation(p.m_prog, i, attrib);
        }
    }

    {
        bool success = true;
        for (const auto &ss : spec.shaders) {
            Shader sh;
            if (!sh.load(std::get<0>(ss), std::get<1>(ss))) {
                success = false;
                continue;
            }
            if (success) {
                glAttachShader(p.m_prog, sh.shader());
                if (!p.m_name.empty()) {
                    p.m_name.push_back(' ');
                }
                p.m_name.append(sh.name());
            }
        }
        if (!success) {
            return false;
        }
    }

    glLinkProgram(p.m_prog);

    GLint flag = 0;
    glGetProgramiv(p.m_prog, GL_LINK_STATUS, &flag);
    if (!flag) {
        Log::error("%s: Linking failed.", p.m_name.c_str());
    }
    {
        GLint loglen = 0;
        glGetProgramiv(p.m_prog, GL_INFO_LOG_LENGTH, &loglen);
        if (loglen > 1) {
            std::vector<char> log(loglen + 1);
            log[loglen] = '\0';
            glGetProgramInfoLog(p.m_prog, loglen, nullptr, log.data());
            Log::warn("%s", log.data());
        }
    }
    if (!flag) {
        return false;
    }
    std::swap(m_prog, p.m_prog);
    std::swap(m_name, p.m_name);
    return true;
}

void ProgramObj::get_uniforms(void *object,
                              const ShaderField *uniforms) const {
    if (!m_prog) {
        for (int i = 0; uniforms[i].name; i++) {
            get_field(object, uniforms[i].offset) = -1;
        }
        return;
    }
    for (int i = 0; uniforms[i].name; i++) {
        GLint value = glGetUniformLocation(m_prog, uniforms[i].name);
        if (value < 0) {
            Log::warn("%s: Uniform does not exist: %s",
                      m_name.c_str(), uniforms[i].name);
        }
        get_field(object, uniforms[i].offset) = value;
    }
    GLint ucount = 0;
    glGetProgramiv(m_prog, GL_ACTIVE_UNIFORMS, &ucount);
    for (int j = 0; j < ucount; j++) {
        char name[32];
        name[0] = '\0';
        GLint size;
        GLenum type;
        glGetActiveUniform(m_prog, j, sizeof(name), nullptr,
                           &size, &type, name);
        for (int i = 0; ; i++) {
            if (!uniforms[i].name) {
                Log::warn("%s: Unbound uniform: %s",
                          m_name.c_str(), name);
                break;
            }
            if (!std::strcmp(name, uniforms[i].name)) {
                break;
            }
        }
    }
}

}
