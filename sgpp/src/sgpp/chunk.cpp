/* Copyright 2014 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sgpp/chunk.hpp"

#include <cstring>

#include "sg/defs.h"
#include "sgpp/log.hpp"

namespace SG {

namespace {

const char NATIVE_BYTE_ORDER[2] = {
#if SG_BYTE_ORDER == SG_LITTLE_ENDIAN
    'L', 'E'
#else
    'B', 'E'
#endif
};

}

struct ChunkReader::Entry {
    char name[4];
    unsigned offset;
    unsigned length;
};

ChunkReader::ChunkReader()
    : m_first(nullptr), m_last(nullptr) {}

ChunkReader::~ChunkReader() {}

bool ChunkReader::read(const Data &data) {
    const char *ptr = reinterpret_cast<const char *>(data.data());
    size_t size = data.size();
    if (size < 24) {
        Log::warn("%s: corrupted header.", data.path());
        return false;
    }
    if (std::memcmp(ptr + 18, NATIVE_BYTE_ORDER, 2)) {
        Log::warn("%s: not native byte order.", data.path());
        return false;
    }
    unsigned count = *reinterpret_cast<const unsigned *>(ptr + 20);
    if (count > 0x10000 || count * 12 > size - 24) {
        Log::warn("%s: corrupted header.", data.path());
        return false;
    }
    const Entry *first = reinterpret_cast<const Entry *>(ptr + 24),
        *last = first + count;
    for (auto i = first; i != last; i++) {
        if (i->offset > size || i->length > size - i->offset) {
            Log::warn("%s: corrupted header.", data.path());
            return false;
        }
    }

    m_data = data;
    m_first = first;
    m_last = last;
    return true;
}

const char *ChunkReader::magic() const {
    if (!m_first) {
        Log::abort("ChunkReader::magic");
    }
    return reinterpret_cast<const char *>(m_data.data());
}

ChunkReader::Version ChunkReader::version() const {
    if (!m_first) {
        Log::abort("ChunkReader::magic");
    }
    const unsigned char *ptr =
        reinterpret_cast<const unsigned char *>(m_data.data());
    return Version(ptr[16], ptr[17]);
}

ChunkReader::ChunkData ChunkReader::get_data(const char *name) const {
    for (auto i = m_first, e = m_last; i != e; i++) {
        if (!std::memcmp(i->name, name, 4)) {
            return ChunkData(
                reinterpret_cast<const char *>(m_data.data()) + i->offset,
                i->length);
        }
    }
    return ChunkData(nullptr, 0);
}

}
