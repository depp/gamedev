/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sg/error.h"
#include "sg/log.h"
#include <stddef.h>
#include <stdio.h>

/* This is an alternate implementation of the logging interface for
   use in unit tests.  */

static const char SG_LOGLEVEL[4][6] = {
    "DEBUG", "INFO", "WARN", "ERROR"
};

void
sg_logs(
    sg_log_level_t level,
    const char *msg)
{
    sg_logf(level, "%s", msg);
}

void
sg_logf(
    sg_log_level_t level,
    const char *msg,
    ...)
{
    va_list ap;
    va_start(ap, msg);
    sg_logerrv(level, NULL, msg, ap);
    va_end(ap);
}

void
sg_logv(
    sg_log_level_t level,
    const char *msg,
    va_list ap)
{
    sg_logerrv(level, NULL, msg, ap);
}

void
sg_logerrs(
    sg_log_level_t level,
    struct sg_error *err,
    const char *msg)
{
    sg_logerrf(level, err, "%s", msg);
}

void
sg_logerrf(
    sg_log_level_t level,
    struct sg_error *err,
    const char *msg, ...)
{
    va_list ap;
    va_start(ap, msg);
    sg_logerrv(level, err, msg, ap);
}

void
sg_logerrv(
    sg_log_level_t level,
    struct sg_error *err,
    const char *msg,
    va_list ap)
{
    fprintf(stderr, "%s: ", SG_LOGLEVEL[level]);
    vfprintf(stderr, msg, ap);
    if (err != NULL) {
        fprintf(
            stderr, " (domain: %s, msg: %s, code: %ld)",
            err->domain->name, err->msg, err->code);
    }
    fputc('\n', stderr);
}
