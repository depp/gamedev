/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
/* Internal file / path subsystem interface.  */
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "sg/defs.h"
struct sg_error;
struct sg_filedata;
struct sg_fileinfo;
struct sg_writer;

/* Additional return codes for file functions.  These additional
   return codes are only used internally in the path system.  */
enum {
    /* SG_FILE_OK */
    /* SG_FILE_ERROR */
    /* SG_FILE_NOTCHANGED */

    /* The file was not found.  */
    SG_FILE_ENOTFOUND = -3,
    /* File names may not begin with a period.  */
    SG_FILE_EPERIOD = -4,
    /* The file name is reserved (on Windows).  */
    SG_FILE_ERESERVED = -5,
    /* The file name contains an illegal character.  */
    SG_FILE_EBADCHAR = -6
};

enum {
    /* Maximum number of extensions for file_load() et al.  */
    SG_PATH_MAXEXTS = 8,
    /* Maximum number of search paths.  */
    SG_PATH_MAXCOUNT = 8
};

#if defined _WIN32

#include <wchar.h>
typedef wchar_t pchar;
#define SG_PATH_PATHSEP ';'
#define SG_PATH_DIRSEP '\\'
#define pmemcpy wmemcpy
#define pmemchr wmemchr
#define pmemcmp wmemcmp

#else

typedef char pchar;
#define SG_PATH_PATHSEP ':'
#define SG_PATH_DIRSEP '/'
#define pmemcpy memcpy
#define pmemchr memchr
#define pmemcmp memcmp

#endif

/* A search path.  */
struct sg_path {
    const pchar *path;
    int len;
};

/* The path where game data is stored alongside the application, if it
   is stored alongside the application.  */
#define SG_DEFAULTPATH_DATA "Data"
/* The path where user data is stored alongside the application, if it
   is stored alongside the application.  */
#define SG_DEFAULTPATH_USER "User"
/* The path where cache data is stored inside the user path.  */
#define SG_DEFAULTPATH_CACHE "cache"

/* Error messages for sg_path_initabort().  */
#define SG_PATHERR_EXEPATH "Could not get path to executable."
#define SG_PATHERR_EXEPATHLONG "Path to executable is too long."
#define SG_PATHERR_NOMEM "Out of memory."

#if defined _WIN32
struct sg_reader {
    void *handle;
};
#else
struct sg_reader {
    int fdes;
};
#endif

/* An extension to search.  */
struct sg_path_ext {
    const char *p;
    int len;
};

struct sg_filelist_builder;

#if defined _WIN32

static __inline uint64_t
sg_32to64(unsigned hi, unsigned lo)
{
    return ((uint64_t) hi << 32) | lo;
}

#endif

/* Returns 0 on success, -1 if the file was not found, and -2 for
   other errors.  */
int
sg_reader_open(
    struct sg_reader *fp,
    const pchar *path,
    struct sg_error **err);

/* Returns 0 for success, -1 for error.  */
int
sg_reader_getinfo(
    struct sg_reader *fp,
    struct sg_fileinfo *info,
    struct sg_error **err);

/* Returns number of bytes read, or -1 for error.  */
int
sg_reader_read(
    struct sg_reader *fp,
    void *buf,
    size_t bufsz,
    struct sg_error **err);

void
sg_reader_close(
    struct sg_reader *fp);

/* Returns NULL on error.  */
struct sg_filedata *
sg_reader_load(
    struct sg_reader *fp,
    size_t size,
    const char *path,
    size_t pathlen,
    struct sg_error **err);

/* Check that a file name (just a single component, not a whole path)
   is valid.  Returns 0 if the file name is valid, or returns one of
   the following error codes: SG_FILE_EPERIOD, SG_FILE_ERESERVED, or
   SG_FILE_EBADCHAR. */
int
sg_path_checkpart(
    const char *p,
    size_t len);

/* Copy the normalized path 'src' to the destination 'dst', converting
   normalized directory separators to platform directory separators
   (e.g., turn slashes backwards on Windows).  */
void
sg_path_copy(
    pchar *dest,
    const char *src,
    size_t len);

/* Get the search paths for the given set of flags.  Returns the
   number of search paths on success, or -1 to indicate an error. */
int
sg_path_get(
    struct sg_path *search,
    unsigned flags,
    struct sg_error **err);

/* Get a writable path.  Returns a NULL path if there is an error.  */
struct sg_path
sg_path_get_writable(
    unsigned flags,
    struct sg_error **err);

/* Get the maximum length of a search path in the given list.  */
int
sg_path_maxlen(
    struct sg_path *search,
    int count);

/* Parse a list of file extensions.  Returns the number of extensions
   on success, or -1 if the list is invalid.  */
int
sg_path_extlist(
    struct sg_path_ext *ext,
    const char *extensions);

/* Get the maximum length of a file extension in the given list.  */
int
sg_path_extmaxlen(
    struct sg_path_ext *ext,
    int count);

/* Strip an extension from a normalized file path, if it is in the
   list.  Returns the new length of the path.  If index is not NULL,
   stores the index of the extension removed in index, or -1 if no
   extension was removed.  This function never fails.  */
int
sg_path_extstrip(
    const char *path,
    int len,
    int *index,
    const struct sg_path_ext *ext,
    int extcount);

/* Create the parent directory containing a file, recursively if
   necessary.  The path data will be modified, but returned to its
   original value on success (with normalized path separators).
   Returns SG_FILE_OK if the directory was created or if it already
   exists.  Returns SG_FILE_ERROR if an error occurs.  Returns
   SG_FILE_NOTFOUND if the directory does not exist and could not be
   created.  */
int
sg_path_mkpardir(
    pchar *path,
    struct sg_error **err);

/* Abort during path initialization.  */
SG_ATTR_NORETURN
void
sg_path_initabort(
    const char *msg);

/* Add a default search path to the list of search paths.  The
   read_flags and write_flags flags are combinations of SG_PATH_DATA,
   SG_PATH_CONFIG, and SG_PATH_CACHE, which specify that the given
   path should be used as a read-only or writable part of that search
   path.  This should only be called from sg_path_getdefaults().  */
void
sg_path_add(
    const pchar *path,
    size_t pathsize,
    unsigned read_flags,
    unsigned write_flags);

/* Add a collection of search paths to the list of search paths,
   separated by the path separator.  This is equivalent to a series of
   calls to sg_path_add().  */
void
sg_path_addn(
    const pchar *paths,
    size_t pathssize,
    unsigned read_flags);

/* Get the default paths for this platform.  This should make calls to
   sg_path_add().  A writable path of each type should be specified
   for each group.  Paths specified in the given flags are already
   set, and do not need to be initialized to their defaults.  */
void
sg_path_getdefaults(
    unsigned read_flags,
    unsigned write_flags);

/* Get the file ID for the file at the given path.  */
int
sg_file_getinfo(
    const pchar *path,
    struct sg_fileinfo *info,
    struct sg_error **err);

/* Get the path where a new file would be created.  Create the
   directory containing the file, if necessary.  The resulting buffer
   must be freed with free() afterwards.  Returns NULL on error.

   This is used to interact with APIs that create files, but where
   it's more convenient to pass a filename rather than callbacks.  */
pchar *
sg_file_createpath(
    const char *path,
    size_t pathlen,
    unsigned flags,
    struct sg_error **err);

/* Add an entry to a file list.  Returns zero if the file was not
   added, a positive number if the file was not added, and a negative
   number if an error occurred.  If the result is positive, then info
   will be set to point to the location where the file info should be
   stored.  */
int
sg_filelist_add(
    struct sg_filelist_builder *bp,
    const char *path,
    int pathlen,
    struct sg_fileinfo **info);

/* Scan a directory on the filesystem, and add files to the given file
   list using sg_filelist_add().  The path is NUL-terminated, but the
   NUL byte is not included in the length.  */
int
sg_filelist_scanpath(
    struct sg_filelist_builder *bp,
    const pchar *path,
    int pathlen,
    unsigned flags,
    struct sg_error **err);
