/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sg/color.hpp"

namespace SG {

// DawnBringer's 16-color palette.
const unsigned char PALETTE_DB16[16][4] = {
    {  20,  12,  28, 255 },
    {  68,  36,  52, 255 },
    {  48,  52, 109, 255 },
    {  78,  74,  78, 255 },
    { 133,  76,  48, 255 },
    {  52, 101,  36, 255 },
    { 208,  70,  72, 255 },
    { 117, 113,  97, 255 },
    {  89, 125, 206, 255 },
    { 210, 125,  44, 255 },
    { 133, 149, 161, 255 },
    { 109, 170,  44, 255 },
    { 210, 170, 153, 255 },
    { 109, 194, 202, 255 },
    { 218, 212,  94, 255 },
    { 222, 238, 214, 255 }
};

}
