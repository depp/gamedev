/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

#include "sg/quat.hpp"

#include <cmath>

namespace SG {

quat quat::axis_angle(vec3 axis, float angle) {
    float a = 1.0f / length(axis);
    float b = std::cos(0.5f * angle);
    float c = a * std::sin(0.5f * angle);
    return quat{{b, c * axis[0], c * axis[1], c * axis[2]}};
}

quat quat::euler_angles(vec3 angles) {
    quat rx{{std::cos(0.5f * angles[0]), std::sin(0.5f * angles[0]), 0, 0}};
    quat ry{{std::cos(0.5f * angles[1]), 0, std::sin(0.5f * angles[1]), 0}};
    quat rz{{std::cos(0.5f * angles[2]), 0, 0, std::sin(0.5f * angles[2])}};
    return rz * ry * rx;
}

vec3 quat::transform(vec3 p) const {
    float w = v[0], x = v[1], y = v[2], z = v[3];
    return vec3{{p[0] * (1.0f - 2.0f * y * y - 2.0f * z * z) +
                     p[1] * (2.0f * x * y - 2.0f * w * z) +
                     p[2] * (2.0f * z * x + 2.0f * w * y),
                 p[0] * (2.0f * x * y + 2.0f * w * z) +
                     p[1] * (1.0f - 2.0f * z * z - 2.0f * x * x) +
                     p[2] * (2.0f * y * z - 2.0f * w * x),
                 p[0] * (2.0f * z * x - 2.0f * w * y) +
                     p[1] * (2.0f * y * z + 2.0f * w * x) +
                     p[2] * (1.0f - 2.0f * x * x - 2.0f * y * y)}};
}

quat quat::normalized() const {
    float mag2 = v[0] * v[0] + v[1] * v[1] + v[2] * v[2] + v[3] * v[3];
    float a = 1.0f / std::sqrt(mag2);
    return quat{{v[0] * a, v[1] * a, v[2] * a, v[3] * a}};
}

quat operator*(quat x, quat y) {
    return quat{{x[0] * y[0] - x[1] * y[1] - x[2] * y[2] - x[3] * y[3],
                 x[0] * y[1] + x[1] * y[0] + x[2] * y[3] - x[3] * y[2],
                 x[0] * y[2] + x[2] * y[0] + x[3] * y[1] - x[1] * y[3],
                 x[0] * y[3] + x[3] * y[0] + x[1] * y[2] - x[2] * y[1]}};
}

quat &operator*=(quat &x, quat y) {
    x = x * y;
    return x;
}

}
