/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "file_impl.h"
#include "sg/log.h"

#if defined _WIN32

void
sg_path_copy(wchar_t *dest, const char *src, size_t len)
{
    size_t i;
    for (i = 0; i < len; ++i)
        dest[i] = src[i] == '/' ? SG_PATH_DIRSEP : src[i];
}

#else

void
sg_path_copy(char *dest, const char *src, size_t len)
{
    memcpy(dest, src, len);
}

#endif

int
sg_path_extlist(struct sg_path_ext *ext, const char *extensions)
{
    const char *epos = extensions, *estart, *esep;
    int count = 0, elen;

    while (epos) {
        estart = epos;
        esep = strchr(epos, ':');
        if (!esep) {
            epos = NULL;
            elen = (int) strlen(estart);
        } else {
            epos = esep + 1;
            elen = (int) (esep - estart);
        }
        if (!elen)
            continue;
        if (count >= SG_PATH_MAXEXTS) {
            sg_logf(SG_LOG_ERROR,
                    "List of extensions is too long: %s", extensions);
            break;
        }
        ext[count].p = estart;
        ext[count].len = elen;
        count++;
    }
    return count;
}

int
sg_path_maxlen(struct sg_path *search, int count)
{
    int i, max = 0;
    for (i = 0; i < count; i++)
        if (search[i].len > max)
            max = search[i].len;
    return max;
}

int
sg_path_extstrip(const char *path, int len, int *index,
                 const struct sg_path_ext *ext, int extcount)
{
    int i, extlen, dummy;
    if (!index)
        index = &dummy;
    for (i = 0; i < extcount; i++) {
        extlen = ext[i].len;
        if (len > extlen &&
            path[len - extlen - 1] == '.' &&
            !memcmp(path + len - extlen, ext[i].p, extlen)) {
            *index = i;
            return len - extlen - 1;
        }
    }
    *index = -1;
    return len;
}

int
sg_path_extmaxlen(struct sg_path_ext *ext, int count)
{
    int i, max = 0;
    for (i = 0; i < count; i++)
        if (ext[i].len > max)
            max = ext[i].len;
    return max;
}
