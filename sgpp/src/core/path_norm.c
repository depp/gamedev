/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "file_impl.h"
#include "sg/file.h"
#include "sg/error.h"
#include <stdlib.h>

static void
sg_path_error(struct sg_error **err, const char *msg)
{
    sg_error_sets(err, &SG_ERROR_INVALPATH, 0, msg);
}

/* Table for which characters are valid in pathnames.  The list of
   characters is very conservative: only alphanumeric characters, "-",
   ".", and "_" are allowed.  */
/*
Python:
import string
a = [0] * 8
for c in string.ascii_letters + string.digits + "-._":
    n = ord(c)
    a[n >> 5] |= 1 << (n & 31)
*/
static const unsigned SG_PATH_ALLOWED[8] = {
    0, 67067904, 2281701374, 134217726, 0, 0, 0, 0
};

/*
  References:
  http://msdn.microsoft.com/en-us/library/windows/desktop/aa365247(v=vs.85).aspx
*/
int
sg_path_checkpart(const char *p, size_t len)
{
    unsigned allowed;
    size_t i, blen;
    char b[4];

    allowed = 1;
    for (i = 0; i < len; i++) {
        unsigned c = (unsigned char) p[i];
        allowed &= SG_PATH_ALLOWED[c >> 5] >> (c & 31);
    }
    if (!allowed)
        return SG_FILE_EBADCHAR;
    if (len > 0 && *p == '.')
        return SG_FILE_EPERIOD;

    if (len > 3 && p[3] == '.')
        blen = 3;
    else if (len > 4 && p[4] == '.')
        blen = 4;
    else if (len < 3 || len > 4)
        return 0;
    else
        blen = len;

    for (i = 0; i < blen; i++) {
        if (p[i] >= 'A' && p[i] <= 'Z')
            b[i] = p[i] + 'a' - 'A';
        else
            b[i] = p[i];
    }

    switch (b[0]) {
    case 'a':
        if (blen == 3 && b[1] == 'u' && b[2] == 'x')
            return SG_FILE_ERESERVED;
        break;

    case 'c':
        if (blen == 4 && b[1] == 'o' && b[2] == 'm' &&
            (b[3] >= '0' && b[3] <= '9'))
            return SG_FILE_ERESERVED;
        if (blen == 3 && b[1] == 'o' && b[2] == 'n')
            return SG_FILE_ERESERVED;
        break;

    case 'l':
        if (blen == 4 && b[1] == 'p' && b[2] == 't' &&
            (b[3] >= '0' && b[3] <= '9'))
            return SG_FILE_ERESERVED;
        break;

    case 'n':
        if (blen == 3 && b[1] == 'u' && b[2] == 'l')
            return SG_FILE_ERESERVED;
        break;

    case 'p':
        if (blen == 3 && b[1] == 'r' && b[2] == 'n')
            return SG_FILE_ERESERVED;
        break;

    default:
        break;
    }
    return 0;
}

static int
sg_path_norm2(char *buf, char *pos,
              const char *path, size_t pathlen,
              struct sg_error **err)
{
    char const *ip = path, *ie = path + pathlen;
    char *op = pos, *os, *oe = buf + SG_MAX_PATH;
    unsigned c;
    int r;

    do {
        if (op != buf && op[-1] != '/')
            *op++ = '/';
        if (op == oe)
            goto toolong;
        os = op;
        while (1) {
            if (ip == ie) {
                c = '\0';
                break;
            }
            c = (unsigned char) *ip++;
            if (c == '/' || c == '\\') {
                c = '/';
                break;
            }
            *op++ = c;
            if (op == oe)
                goto toolong;
        }
        if (os == op) {
        } else if (os + 1 == op && os[0] == '.') {
            op = os;
        } else if (os + 2 == op && os[0] == '.' && os[1] == '.') {
            op = os;
            if (op != buf) {
                do op--;
                while (op != buf && op[-1] != '/');
            }
        } else {
            r = sg_path_checkpart(os, op - os);
            switch (r) {
            case 0:
                break;
            case SG_FILE_EPERIOD:
                sg_path_error(err, "file names may not start with '.'");
                return -1;
            case SG_FILE_ERESERVED:
                sg_error_setf(
                    err, &SG_ERROR_INVALPATH, 0,
                    "'%.*s' is a reserved filename", (int) (op - os), os);
                return -1;
            case SG_FILE_EBADCHAR:
                sg_path_error(err, "path contains illegal character");
                return -1;
            default:
                abort();
            }
        }
    } while (c);

    *op = '\0';
    return (int) (op - buf);

toolong:
    sg_path_error(err, "path too long");
    return -1;
}

int
sg_path_norm(char *buf, const char *path, size_t pathlen,
             struct sg_error **err)
{
    return sg_path_norm2(buf, buf, path, pathlen, err);
}

int
sg_path_join(char *buf, const char *path, size_t pathlen,
             struct sg_error **err)
{
    char *slash;
    slash = strrchr(buf, '/');
    return sg_path_norm2(buf, slash != NULL ? slash + 1 : buf,
                         path, pathlen, err);
}
