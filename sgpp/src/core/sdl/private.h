/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sg/defs.h"

#include "SDL.h"

struct sg_sdl {
    SDL_Window *window;
    SDL_GLContext context;

    /* The current window status (using SGLib flags).  */
    unsigned window_status;
    /* Whether the user has requested mouse capture.  */
    int want_capture;
    /* Whether we have mouse capture.  */
    int have_capture;
};

extern struct sg_sdl sg_sdl;

/* Handle a fatal error from LibSDL, calls SDL_GetError().  */
void
sg_sdl_error(const char *what);

/* Update the mouse capture state (this is called after the window
   status changes).  */
void
sg_sdl_update_capture(void);

/* Set VSync according to the current cvar value.  */
void
sg_sdl_update_vsync(void);

/* Process all pending events.  Returns 1 to continue, 0 to quit.  */
int
sg_sdl_handle_events(void);
