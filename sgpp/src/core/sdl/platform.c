/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "private.h"
#include "../private.h"
#include "sg/entry.h"
#include "sg/event.h"
#include "sg/log.h"
#include "sg/version.h"

SG_ATTR_NORETURN
static void
sg_sdl_quit(int status)
{
    SDL_Quit();
    exit(status);
}

void
sg_sys_quit(void)
{
    sg_sdl_quit(0);
}

void
sg_sdl_error(const char *what)
{
    fprintf(stderr, "error: %s: %s\n", what, SDL_GetError());
    sg_sdl_quit(1);
}

void
sg_sys_abort(const char *msg)
{
    fprintf(stderr, "error: %s\n", msg);
    sg_sdl_quit(1);
}

void
sg_sdl_update_capture(void)
{
    int enabled = sg_sdl.want_capture &&
        (sg_sdl.window_status & SG_WINDOW_FOCUSED) != 0;
    int r = SDL_SetRelativeMouseMode(enabled ? SDL_TRUE : SDL_FALSE);
    if (r == 0) {
        sg_sdl.have_capture = enabled;
    } else {
        if (enabled)
            sg_logs(SG_LOG_WARN, "failed to set relative mouse mode");
        return;
    }
}

void
sg_sys_capturemouse(int enabled)
{
    sg_sdl.want_capture = enabled;
    sg_sdl_update_capture();
}

void
sg_version_platform(void)
{
    char v1[16], v2[16];
    SDL_version v;
    SDL_GetVersion(&v);
#if !defined _WIN32
    snprintf(v1, sizeof(v1), "%d.%d.%d",
             SDL_MAJOR_VERSION, SDL_MINOR_VERSION, SDL_PATCHLEVEL);
    snprintf(v2, sizeof(v2), "%d.%d.%d",
             v.major, v.minor, v.patch);
#else
    _snprintf_s(v1, sizeof(v1), _TRUNCATE, "%d.%d.%d",
        SDL_MAJOR_VERSION, SDL_MINOR_VERSION, SDL_PATCHLEVEL);
    _snprintf_s(v2, sizeof(v2), _TRUNCATE, "%d.%d.%d",
        v.major, v.minor, v.patch);
#endif
    sg_version_lib("LibSDL", v1, v2);
}

void
sg_sdl_update_vsync(void)
{
    int vsync = sg_sys.vsync.value, r;
    sg_sys.vsync.flags &= ~SG_CVAR_MODIFIED;
    switch (vsync) {
    case 0:
        r = SDL_GL_SetSwapInterval(0);
        break;
    case 1:
        r = SDL_GL_SetSwapInterval(1);
        break;
    case 2:
        r = SDL_GL_SetSwapInterval(-1);
        if (!r)
            return;
        sg_logf(SG_LOG_WARN, "Could not set vsync 2: %s", SDL_GetError());
        SDL_ClearError();
        vsync = 1;
        r = SDL_GL_SetSwapInterval(1);
        break;
    default:
        return;
    }
    if (r) {
        sg_logf(SG_LOG_WARN, "Could not set vsync %d: %s",
                vsync, SDL_GetError());
    }
}
