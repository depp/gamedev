/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "log_impl.h"
# include "sg/cvar.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if !defined _WIN32
# include <unistd.h>
#endif

static struct {
    int color;
#if defined _WIN32
    struct sg_cvar_bool winconsole_cvar;
    int winconsole_open;
#endif
} sg_log_console;

static void
sg_log_console_msg(struct sg_log_msg *m)
{
    const char *color;
    fwrite(m->time, 1, m->timelen, stderr);
    putc(' ', stderr);
    color = NULL;
    if (sg_log_console.color) {
        switch (m->levelval) {
        case SG_LOG_DEBUG: color = "\x1b[36m";   /* Cyan */     break;
        case SG_LOG_INFO:                        /* White */    break;
        case SG_LOG_WARN:  color = "\x1b[33m";   /* Yellow */   break;
        case SG_LOG_ERROR: color = "\x1b[1;31m"; /* Red+Bold */ break;
        }
    }
    if (color)
        fputs(color, stderr);
    fwrite(m->level, 1, m->levellen, stderr);
    if (color)
        fputs("\x1b[0m", stderr);
    fputs(": ", stderr);
    fwrite(m->msg, 1, m->msglen, stderr);
    putc('\n', stderr);
}

#if defined(_WIN32)
# include <Windows.h>
# include <io.h>
# include <fcntl.h>

static void
sg_log_console_reopen(FILE *fp, DWORD which, const char *mode)
{
    FILE *f;
    char *buf = NULL;
    int fdes;
    HANDLE h;
    h = GetStdHandle(which);
    fdes = _open_osfhandle((intptr_t) h, _O_TEXT);
    f = _fdopen(fdes, mode);
    setvbuf(f, buf, _IONBF, 1);
    *fp = *f;
}

void
sg_log_console_init(void)
{
    sg_cvar_defbool(
        "log", "winconsole", "Create a console window for logging",
        &sg_log_console.winconsole_cvar, 0, SG_CVAR_PERSISTENT);
}

void
sg_log_console_update(void)
{
    BOOL br;
    if (!sg_log_console.winconsole_cvar.value ||
        sg_log_console.winconsole_open)
        return;
    br = AllocConsole();
    if (!br)
        return;
    sg_log_console_reopen(stdin, STD_INPUT_HANDLE, "r");
    sg_log_console_reopen(stdout, STD_OUTPUT_HANDLE, "w");
    sg_log_console_reopen(stderr, STD_ERROR_HANDLE, "w");
    sg_log_console.winconsole_open = 1;
    sg_log_listen(sg_log_console_msg);
}

#else

void
sg_log_console_init(void)
{
    sg_log_console.color = isatty(STDERR_FILENO) == 1;
    sg_log_listen(sg_log_console_msg);
}

void
sg_log_console_update(void)
{ }

#endif
