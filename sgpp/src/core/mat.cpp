/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

#include "sg/mat.hpp"

namespace SG {

/* ======================================================================
   3x3 matrix
   ====================================================================== */

vec3 mat3::transform(vec3 v) const {
    return vec3{v[0] * m[0][0] + v[1] * m[1][0] + v[2] * m[2][0],
                v[0] * m[0][1] + v[1] * m[1][1] + v[2] * m[2][1],
                v[0] * m[0][2] + v[1] * m[1][2] + v[2] * m[2][2]};
}

mat3 mat3::identity() {
    return mat3{{
        {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f},
    }};
}

mat3 mat3::scale(float a) {
    return mat3{{
        {a, 0.0f, 0.0f}, {0.0f, a, 0.0f}, {0.0f, 0.0f, a},
    }};
}

mat3 mat3::scale(vec3 v) {
    return mat3{{
        {v[0], 0.0f, 0.0f}, {0.0f, v[1], 0.0f}, {0.0f, 0.0f, v[2]},
    }};
}

mat3 mat3::rotation(quat q) {
    float w = q[0], x = q[1], y = q[2], z = q[3];
    return mat3{{
        {1.0f - 2.0f * y * y - 2.0f * z * z, 2.0f * x * y + 2.0f * w * z,
         2.0f * z * x - 2.0f * w * y},
        {2.0f * x * y - 2.0f * w * z, 1.0f - 2.0f * z * z - 2.0f * x * x,
         2.0f * y * z + 2.0f * w * x},
        {2.0f * z * x + 2.0f * w * y, 2.0f * y * z - 2.0f * w * x,
         1.0f - 2.0f * x * x - 2.0f * y * y},
    }};
}

mat3 operator*(const mat3 &x, const mat3 &y) {
    mat3 z;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            z.m[j][i] = x.m[0][i] * y.m[j][0] + x.m[1][i] * y.m[j][1] +
                        x.m[2][i] * y.m[j][2];
        }
    }
    return z;
}

mat3 &operator*=(mat3 &x, const mat3 &y) {
    x = x * y;
    return x;
}

/* ======================================================================
   4x4 matrix
   ====================================================================== */

vec3 mat4::transform(vec3 v) const {
    return vec3{v[0] * m[0][0] + v[1] * m[1][0] + v[2] * m[2][0] + m[3][0],
                v[0] * m[0][1] + v[1] * m[1][1] + v[2] * m[2][1] + m[3][1],
                v[0] * m[0][2] + v[1] * m[1][2] + v[2] * m[2][2] + m[3][2]};
}

mat4 mat4::identity() {
    return mat4{{
        {1.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 1.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 1.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 1.0f},
    }};
}

mat4 mat4::translation(vec3 v) {
    return mat4{{{1.0f, 0.0f, 0.0f, 0.0f},
                 {0.0f, 1.0f, 0.0f, 0.0f},
                 {0.0f, 0.0f, 1.0f, 0.0f},
                 {v[0], v[1], v[2], 1.0f}}};
}

mat4 mat4::scale(float a) {
    return mat4{{
        {a, 0.0f, 0.0f, 0.0f},
        {0.0f, a, 0.0f, 0.0f},
        {0.0f, 0.0f, a, 0.0f},
        {0.0f, 0.0f, 0.0f, 1.0f},
    }};
}

mat4 mat4::scale(vec3 v) {
    return mat4{{
        {v[0], 0.0f, 0.0f, 0.0f},
        {0.0f, v[1], 0.0f, 0.0f},
        {0.0f, 0.0f, v[2], 0.0f},
        {0.0f, 0.0f, 0.0f, 1.0f},
    }};
}

mat4 mat4::rotation(quat q) {
    float w = q[0], x = q[1], y = q[2], z = q[3];
    return mat4{
        {{1.0f - 2.0f * y * y - 2.0f * z * z, 2.0f * x * y + 2.0f * w * z,
          2.0f * z * x - 2.0f * w * y, 0.0f},
         {2.0f * x * y - 2.0f * w * z, 1.0f - 2.0f * z * z - 2.0f * x * x,
          2.0f * y * z + 2.0f * w * x, 0.0f},
         {2.0f * z * x + 2.0f * w * y, 2.0f * y * z - 2.0f * w * x,
          1.0f - 2.0f * x * x - 2.0f * y * y, 0.0f},
         {0.0f, 0.0f, 0.0f, 1.0f}}};
}

mat4 mat4::perspective(float mx, float my, float near, float far) {
    return mat4{{{1.0f / mx, 0.0f, 0.0f, 0.0f},
                 {0.0f, 1.0f / my, 0.0f, 0.0f},
                 {0.0f, 0.0f, -(far + near) / (far - near), -1.0f},
                 {0.0f, 0.0f, -2.0f * far * near / (far - near), 0.0f}}};
}

mat4 operator*(const mat4 &x, const mat4 &y) {
    mat4 z;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            z.m[j][i] = x.m[0][i] * y.m[j][0] + x.m[1][i] * y.m[j][1] +
                        x.m[2][i] * y.m[j][2] + x.m[3][i] * y.m[j][3];
        }
    }
    return z;
}

mat4 &operator*=(mat4 &x, const mat4 &y) {
    x = x * y;
    return x;
}

}
