/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */

/* This function is in its own file because <fts.h> does not work with
   large file support on Linux.  So we use FTS_NOSTAT, and call stat()
   ourselves from a separate file with large file support enabled.  */
#include "file_impl.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/log.h"

#undef _FILE_OFFSET_BITS

#include <errno.h>
#include <fts.h>

int
sg_filelist_scanpath(
    struct sg_filelist_builder *bp,
    const char *path,
    int pathlen,
    unsigned flags,
    struct sg_error **err)
{
    struct sg_error *err2 = NULL;
    char *path_argv[2];
    FTS *ftsp;
    int r, ecode;

    path_argv[0] = (char *) path;
    path_argv[1] = NULL;
    ftsp = fts_open(path_argv, FTS_LOGICAL | FTS_NOSTAT | FTS_NOCHDIR, NULL);
    if (!ftsp)
        goto fts_error;

    while (1) {
        FTSENT *ent;
        struct sg_fileinfo *info;
        const char *epath;
        int elen;
        ent = fts_read(ftsp);
        if (!ent)
            break;

        switch (ent->fts_info) {
        case FTS_D:
            if (ent->fts_pathlen == pathlen)
                continue;
            if ((flags & SG_FILELIST_RECURSIVE) == 0 ||
                ent->fts_pathlen < pathlen ||
                ent->fts_pathlen >= pathlen + SG_MAX_PATH ||
                sg_path_checkpart(ent->fts_name, ent->fts_namelen)) {
                r = fts_set(ftsp, ent, FTS_SKIP);
                if (r)
                    goto fts_error;
            }
            break;

        case FTS_F:
        case FTS_NSOK:
            epath = ent->fts_path;
            elen = ent->fts_pathlen;
            if (elen < pathlen)
                break;
            epath += pathlen;
            elen -= pathlen;
            while (elen && *epath == '/') {
                epath++;
                elen--;
            }
            if (elen >= SG_MAX_PATH ||
                sg_path_checkpart(ent->fts_name, ent->fts_namelen))
                break;
            info = NULL;
            r = sg_filelist_add(bp, epath, elen, &info);
            if (r < 0)
                goto error;
            if (r > 0 && info) {
                err2 = NULL;
                r = sg_file_getinfo(ent->fts_path, info, &err2);
                if (r) {
                    sg_logerrf(SG_LOG_ERROR, err2,
                               "%s: could not get info", ent->fts_path);
                    sg_error_clear(&err2);
                }
            }
            break;

        case FTS_DNR:
        case FTS_ERR:
        case FTS_NS: {
            ecode = ent->fts_errno;
            if (ecode == ENOENT)
                break;
            err2 = NULL;
            sg_error_errno(&err2, ecode);
            sg_logerrf(SG_LOG_ERROR, err2,
                       "%s: could not scan", ent->fts_path);
            sg_error_clear(&err2);
            break;
        }

        default:
            break;
        }
    }

    r = fts_close(ftsp);
    ftsp = NULL;
    if (r)
        goto fts_error;

    (void) bp;
    (void) flags;
    return 0;

fts_error:
    sg_error_errno(err, errno);
    goto error;

error:
    if (ftsp)
        fts_close(ftsp);
    return -1;
}
