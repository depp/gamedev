/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "file_impl.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/log.h"
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

/* Maximum length of search paths, if a limit exists.  */
#define SG_PATH_BUFSZ 4096

int
sg_path_mkpardir(char *path, struct sg_error **err)
{
    char *p, *end;
    int r, ecode;

    p = strrchr(path, '/');
    if (!p || p == path)
        return 0;
    end = p;
    *p = '\0';
    while (1) {
        r = mkdir(path, 0777);
        if (!r)
            break;
        ecode = errno;
        if (ecode != ENOENT) {
            if (ecode == EEXIST)
                break;
            sg_error_errno(err, ecode);
            return SG_FILE_ERROR;
        }
        p = strrchr(path, '/');
        if (!p || p == path)
            return SG_FILE_ENOTFOUND;
        *p = '\0';
    }
    while (1) {
        *p = '/';
        if (p == end)
            return SG_FILE_OK;
        p += strlen(p);
        r = mkdir(path, 0777);
        if (r) {
            ecode = errno;
            if (ecode != EEXIST) {
                sg_error_errno(err, ecode);
                return SG_FILE_ERROR;
            }
        }
    }
}

static const struct {
    unsigned char read_flags, write_flags;
    char path[14];
} sg_path_default[3] = {
    { SG_PATH_DATA, 0, SG_DEFAULTPATH_DATA },
    { 0, SG_PATH_DATA | SG_PATH_CONFIG, SG_DEFAULTPATH_USER },
    { 0, SG_PATH_CACHE, SG_DEFAULTPATH_USER "/" SG_DEFAULTPATH_CACHE }
};

#if defined __APPLE__
#include <CoreFoundation/CFBundle.h>

void
sg_path_getdefaults(
    unsigned read_flags,
    unsigned write_flags)
{
    CFBundleRef bundle;
    CFURLRef url1, url2;
    CFStringRef relpath;
    Boolean r;
    const char *crelpath;
    int i;
    unsigned rflags, wflags;
    char *buf = NULL;

    bundle = CFBundleGetMainBundle();
    if (!bundle)
        goto error;
    url1 = CFBundleCopyBundleURL(bundle);
    if (!url1)
        goto error;
    url2 = CFURLCreateCopyDeletingLastPathComponent(NULL, url1);
    CFRelease(url1);
    if (!url2)
        goto error;
    for (i = 0; i < 3; i++) {
        rflags = sg_path_default[i].read_flags & ~read_flags;
        wflags = sg_path_default[i].write_flags & ~write_flags;
        if (!(rflags | wflags))
            continue;
        crelpath = sg_path_default[i].path;
        relpath = CFStringCreateWithBytes(
            kCFAllocatorDefault, (const UInt8 *) crelpath, strlen(crelpath),
            kCFStringEncodingASCII, false);
        if (!relpath)
            goto error;
        url1 = CFURLCreateCopyAppendingPathComponent(
            NULL, url2, relpath, true);
        CFRelease(relpath);
        if (!url1)
            goto error;
        if (!buf) {
            buf = malloc(SG_PATH_BUFSZ);
            if (!buf) {
                sg_path_initabort(SG_PATHERR_NOMEM);
                return;
            }
        }
        r = CFURLGetFileSystemRepresentation(
            url1, false, (UInt8 *) buf, SG_PATH_BUFSZ);
        CFRelease(url1);
        if (!r) {
            sg_path_initabort(SG_PATHERR_EXEPATHLONG);
            return;
        }
        sg_path_add(buf, strlen(buf), rflags, wflags);
    }
    CFRelease(url2);
    return;

error:
    sg_path_initabort(SG_PATHERR_EXEPATH);
}

#if 0

size_t
sg_path_getdatapath(char *buf, size_t buflen)
{
    CFBundleRef bundle;
    CFURLRef url;
    Boolean r;
    bundle = CFBundleGetMainBundle();
    if (!bundle)
        return 0;
    url = CFBundleCopyBundleURL(bundle);
    if (!url)
        return 0;
    r = CFURLGetFileSystemRepresentation(url, false, (UInt8 *) buf, buflen);
    CFRelease(url);
    if (!r)
        return 0;
    return strlen(buf);
}

#endif

#elif defined __linux__
#include <unistd.h>

void
sg_path_getdefaults(
    unsigned read_flags,
    unsigned write_flags)
{
    char *buf, *p;
    size_t buflen, slen;
    ssize_t r;
    struct sg_error *err = NULL;
    int i;
    unsigned rflags, wflags;

    buf = malloc(SG_PATH_BUFSZ);
    if (!buf) {
        sg_path_initabort(SG_PATHERR_NOMEM);
        return;
    }
    r = readlink("/proc/self/exe", buf, SG_PATH_BUFSZ);
    if (r < 0) {
        sg_error_errno(&err, errno);
        sg_logerrs(SG_LOG_ERROR, err, SG_PATHERR_EXEPATH);
        sg_error_clear(&err);
        sg_path_initabort(SG_PATHERR_EXEPATH);
        return;
    }
    if (r >= SG_PATH_BUFSZ) {
        sg_path_initabort(SG_PATHERR_EXEPATHLONG);
        return;
    }
    buflen = r;
    buf[buflen] = '\0';
    p = strrchr(buf, '/');
    if (!p) {
        sg_path_initabort(SG_PATHERR_EXEPATH);
        return;
    }
    buflen = p + 1 - buf;

    for (i = 0; i < 3; i++) {
        rflags = sg_path_default[i].read_flags & ~read_flags;
        wflags = sg_path_default[i].write_flags & ~write_flags;
        if (!(rflags | wflags))
            continue;
        slen = strlen(sg_path_default[i].path);
        if (buflen + slen > SG_PATH_BUFSZ) {
            sg_path_initabort(SG_PATHERR_EXEPATHLONG);
            return;
        }
        memcpy(buf + buflen, sg_path_default[i].path, slen);
        sg_path_add(buf, buflen + slen, rflags, wflags);
    }
}

#else

#error "No path implementation for this platform"

#endif
