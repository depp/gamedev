/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "file_impl.h"
#include "sg/cvar.h"
#include "sg/defs.h"
#include "sg/entry.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/log.h"
#include "sg/util.h"
#include "private.h"

#include <assert.h>
#include <limits.h>
#include <stdlib.h>

/* Information about cvars which specify search paths.  */

enum {
    SG_PATHCVAR_DATA,
    SG_PATHCVAR_USERDATA,
    SG_PATHCVAR_CONFIG,
    SG_PATHCVAR_CACHE,
    SG_PATHCVAR_COUNT
};

struct sg_pathcvar {
    char name[7];
    unsigned char path;
    const char *help;
};

static const struct sg_pathcvar SG_PATHCVAR[SG_PATHCVAR_COUNT] = {
    { "data",   0,
      "The paths where data files are found." },
    { "user",   SG_PATH_DATA,
      "The path where user data is stored." },
    { "config", SG_PATH_CONFIG,
      "The path where configuration files are stored." },
    { "cache",  SG_PATH_CACHE,
      "The path where cache data is stored." }
};

/* Global list of search paths.  */

struct sg_pathentry {
    unsigned short offset, length;
    unsigned char write_flags;
    unsigned char read_flags;
};

struct sg_paths {
    struct sg_pathentry entry[SG_PATH_MAXCOUNT];
    pchar *ptr;
    int count;
    unsigned plen;
    unsigned palloc;

    struct sg_cvar_string cvar[SG_PATHCVAR_COUNT];
};

static struct sg_paths sg_paths;

SG_ATTR_NORETURN
void
sg_path_initabort(
    const char *msg)
{
    sg_sys_abortf("Path initialization failed: %s", msg);
}

/* Get the search flags from the general path flags, or return 0 if
   the search flags are invalid.  */
static unsigned
sg_path_sflags(
    unsigned flags)
{
    unsigned sflags = flags & (SG_PATH_DATA | SG_PATH_CONFIG | SG_PATH_CACHE);
    switch (sflags) {
    case 0:
        return SG_PATH_DATA;
        break;
    case SG_PATH_DATA:
    case SG_PATH_CONFIG:
    case SG_PATH_CACHE:
        return sflags;
    default:
        return 0;
    }
}

int
sg_path_get(
    struct sg_path *search,
    unsigned flags,
    struct sg_error **err)
{
    int i, j = 0, n = sg_paths.count;
    unsigned sflags;
    const pchar *pdata = sg_paths.ptr;

    sflags = sg_path_sflags(flags);
    if (!sflags) {
        sg_error_sets(err, &SG_ERROR_INVALID, 0, "Invalid path flags.");
        return -1;
    }

    for (i = 0; i < n; i++) {
        if ((sflags & sg_paths.entry[i].write_flags) == 0)
            continue;
        assert(j <= SG_PATH_MAXCOUNT);
        search[j].path = pdata + sg_paths.entry[i].offset;
        search[j].len = sg_paths.entry[i].length;
        j++;
    }
    for (i = 0; i < n; i++) {
        if ((sflags & sg_paths.entry[i].read_flags) == 0)
            continue;
        assert(j <= SG_PATH_MAXCOUNT);
        search[j].path = pdata + sg_paths.entry[i].offset;
        search[j].len = sg_paths.entry[i].length;
        j++;
    }

    return j;
}

struct sg_path
sg_path_get_writable(
    unsigned flags,
    struct sg_error **err)
{
    int i, n = sg_paths.count;
    unsigned sflags;
    struct sg_path path;

    sflags = sg_path_sflags(flags);
    if (!sflags) {
        sg_error_sets(err, &SG_ERROR_INVALID, 0, "Invalid path flags.");
    } else {
        for (i = 0; i < n; i++) {
            if ((sflags & sg_paths.entry[i].write_flags) == 0)
                continue;
            path.path = sg_paths.ptr + sg_paths.entry[i].offset;
            path.len = sg_paths.entry[i].length;
            return path;
        }
        sg_error_sets(err, &SG_ERROR_GENERIC, 0,
                      "No writable path available.");
    }
    path.path = NULL;
    path.len = 0;
    return path;
}

pchar *
sg_file_createpath(
    const char *path,
    size_t pathlen,
    unsigned flags,
    struct sg_error **err)
{
    pchar *result;
    struct sg_path root;
    char nbuf[SG_MAX_PATH];
    int r, nlen;

    nlen = sg_path_norm(nbuf, path, pathlen, err);
    if (nlen < 0)
        return NULL;
    root = sg_path_get_writable(flags, err);
    if (!root.path)
        return NULL;
    result = malloc(sizeof(pchar) * (root.len + nlen + 1));
    if (!result) {
        sg_error_nomem(err);
        return NULL;
    }
    pmemcpy(result, root.path, root.len);
    sg_path_copy(result + root.len, nbuf, nlen);
    result[root.len + nlen] = 0;

    r = sg_path_mkpardir(result, err);
    if (r != SG_FILE_OK) {
        free(result);
        if (r != SG_FILE_ERROR)
            sg_error_notfound(err, nbuf);
        return NULL;
    }

    return result;
}

void
sg_path_add(
    const pchar *path,
    size_t pathsize,
    unsigned read_flags,
    unsigned write_flags)
{
    int i, n;
    unsigned adj_write_flags = write_flags, offset;
    pchar *pdata;

    if (!pathsize)
        return;
    while (pathsize > 0 && path[pathsize - 1] == SG_PATH_DIRSEP)
        pathsize--;
    n = sg_paths.count;
    for (i = 0; i < n; i++)
        adj_write_flags &= ~sg_paths.entry[i].write_flags;
    if (!adj_write_flags && !read_flags)
        return;
    pdata = sg_paths.ptr;
    for (i = 0; i < n; i++) {
        if (sg_paths.entry[i].length != pathsize + 1 ||
            pmemcmp(path, pdata + sg_paths.entry[i].offset, pathsize))
            continue;
        sg_paths.entry[i].write_flags |= adj_write_flags;
        sg_paths.entry[i].read_flags |= read_flags;
        return;
    }
    if (sg_paths.count >= SG_PATH_MAXCOUNT)
        sg_path_initabort("Too many paths.");
    offset = sg_paths.plen;
    if (offset > USHRT_MAX || pathsize + 1 > USHRT_MAX)
        sg_path_initabort("Paths too long.");
    if (pathsize + 2 > sg_paths.palloc - offset) {
        unsigned nalloc = sg_round_up_pow2_32(
            offset + (unsigned) pathsize + 2);
        if (pathsize + 2 > nalloc - offset)
            sg_path_initabort(SG_PATHERR_NOMEM);
        pdata = realloc(pdata, sizeof(pchar) * nalloc);
        if (!pdata)
            sg_path_initabort(SG_PATHERR_NOMEM);
        sg_paths.ptr = pdata;
        sg_paths.palloc = nalloc;
    }
    sg_paths.entry[n].offset = (unsigned short) offset;
    sg_paths.entry[n].length = (unsigned short) pathsize + 1;
    sg_paths.entry[n].write_flags = adj_write_flags;
    sg_paths.entry[n].read_flags = read_flags;
    sg_paths.plen = offset + (unsigned) pathsize + 2;
    pmemcpy(pdata + offset, path, pathsize);
    pdata[offset + pathsize + 0] = SG_PATH_DIRSEP;
    pdata[offset + pathsize + 1] = '\0';
    sg_paths.count = n + 1;
}

void
sg_path_addn(
    const pchar *path,
    size_t pathsize,
    unsigned read_flags)
{
    const pchar *p = path, *e = path + pathsize, *q, *s;
    while (p != e) {
        s = p;
        q = pmemchr(p, SG_PATH_PATHSEP, e - p);
        if (q)
            p = q + 1;
        else
            p = q = e;
        if (s != q)
            sg_path_add(s, q - s, read_flags, 0);
    }
}

#if defined _WIN32

static void
sg_path_add_utf8(
    const char *path,
    size_t pathsize,
    unsigned read_flags,
    unsigned write_flags)
{
    int r, wpathsize;
    wchar_t *wpath;
    r = sg_wchar_from_utf8(&wpath, &wpathsize, path, pathsize);
    if (r)
        sg_path_initabort(SG_PATHERR_NOMEM);
    sg_path_add(wpath, wpathsize, read_flags, write_flags);
    free(wpath);
}

static void
sg_path_addn_utf8(
    const char *path,
    size_t pathsize,
    unsigned read_flags)
{
    int r, wpathsize;
    wchar_t *wpath;
    r = sg_wchar_from_utf8(&wpath, &wpathsize, path, pathsize);
    if (r)
        sg_path_initabort(SG_PATHERR_NOMEM);
    sg_path_addn(wpath, wpathsize, read_flags);
    free(wpath);
}

#else

#define sg_path_add_utf8 sg_path_add
#define sg_path_addn_utf8 sg_path_addn

#endif

void
sg_path_init(void)
{
    int i, n;
    unsigned read_flags = 0, write_flags = 0;
    const char *path;

    for (i = 0; i < SG_PATHCVAR_COUNT; i++) {
        sg_cvar_defstring(
            "path", SG_PATHCVAR[i].name, SG_PATHCVAR[i].help,
            &sg_paths.cvar[i], NULL, SG_CVAR_INITONLY);
        path = sg_paths.cvar[i].value;
        if (!*path)
            continue;
        if (i == 0) {
            read_flags |= SG_PATH_DATA;
            sg_path_addn_utf8(path, strlen(path), SG_PATH_DATA);
        } else {
            write_flags |= SG_PATHCVAR[i].path;
            sg_path_add_utf8(path, strlen(path), 0, SG_PATHCVAR[i].path);
        }
    }

    sg_path_getdefaults(read_flags, write_flags);

    n = sg_paths.count;
    for (i = 0; i < n; i++)
        sg_paths.entry[i].read_flags &= ~sg_paths.entry[i].write_flags;
    if (sg_paths.palloc < sg_paths.plen)
        sg_paths.ptr = realloc(sg_paths.ptr, sizeof(pchar) * sg_paths.plen);
}
