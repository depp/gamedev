/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sg/color.hpp"

namespace SG {

// DawnBringer's 32-color palette.
const unsigned char PALETTE_DB32[32][4] = {
    {   0,   0,   0, 255 }, // Black
    {  34,  32,  52, 255 }, // Valhalla
    {  69,  40,  60, 255 }, // Loulou
    { 102,  57,  49, 255 }, // Oiled cedar
    { 143,  86,  59, 255 }, // Rope
    { 223, 113,  38, 255 }, // Tahiti gold
    { 217, 160, 102, 255 }, // Twine
    { 238, 195, 154, 255 }, // Pancho
    { 251, 242,  54, 255 }, // Golden fizz
    { 153, 229,  80, 255 }, // Atlantis
    { 106, 190,  48, 255 }, // Christi
    {  55, 148, 110, 255 }, // Elf green
    {  75, 105,  47, 255 }, // Dell
    {  82,  75,  36, 255 }, // Verdigris
    {  50,  60,  57, 255 }, // Opal
    {  63,  63, 116, 255 }, // Deep koamaru
    {  48,  96, 130, 255 }, // Venice blue
    {  91, 110, 225, 255 }, // Royal blue
    {  99, 155, 255, 255 }, // Cornflower
    {  95, 205, 228, 255 }, // Viking
    { 203, 219, 252, 255 }, // Light steel blue
    { 255, 255, 255, 255 }, // White
    { 155, 173, 183, 255 }, // Heather
    { 132, 126, 135, 255 }, // Topaz
    { 105, 106, 106, 255 }, // Dim gray
    {  89,  86,  82, 255 }, // Smokey ash
    { 118,  66, 138, 255 }, // Clairvoyant
    { 172,  50,  50, 255 }, // Brown
    { 217,  87,  99, 255 }, // Mandy
    { 215, 123, 186, 255 }, // Plum
    { 143, 151,  74, 255 }, // Rain forest
    { 138, 111,  48, 255 }  // Stinger
};

}
