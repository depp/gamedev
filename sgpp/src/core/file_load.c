/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "file_impl.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/log.h"
#include <stdlib.h>
#include <string.h>

const char SG_FILEDATA_EMPTY[1] = { 0 };

struct sg_filedata sg_filedata_empty = {
    { 1 },
    SG_FILEDATA_EMPTY,
    0,
    SG_FILEDATA_EMPTY,
    0
};

static void
sg_filedata_free_(struct sg_filedata *data)
{
    if (data == &sg_filedata_empty)
        return;
    if (data->data)
        free((void *) data->data);
    free(data);
}

void
sg_filedata_incref(struct sg_filedata *data)
{
    sg_atomic_inc(&data->refcount_);
}

void
sg_filedata_decref(struct sg_filedata *data)
{
    int c = sg_atomic_fetch_add(&data->refcount_, -1);
    if (c == 1)
        sg_filedata_free_(data);
}

struct sg_filedata *
sg_reader_load(
    struct sg_reader *fp,
    size_t size,
    const char *path,
    size_t pathlen,
    struct sg_error **err)
{
    struct sg_filedata *dp;
    unsigned char *buf = NULL;
    char *pp;
    size_t pos;
    int r;

    buf = malloc(size + 1);
    if (!buf)
        goto nomem;
    pos = 0;
    while (pos < size) {
        r = sg_reader_read(fp, buf + pos, size - pos, err);
        if (r < 0)
            goto nomem;
        if (r == 0)
            break;
        pos += r;
    }
    buf[pos] = '\0';
    if (pos < size)
        buf = realloc(buf, pos + 1);
    dp = malloc(sizeof(*dp) + pathlen + 1);
    if (!dp)
        goto nomem;
    pp = (char *) (dp + 1);
    sg_atomic_set(&dp->refcount_, 1);
    dp->data = buf;
    dp->length = pos;
    dp->path = pp;
    dp->pathlen = pathlen;
    memcpy(pp, path, pathlen);
    pp[pathlen] = '\0';
    return dp;

nomem:
    free(buf);
    sg_error_nomem(err);
    return NULL;
}

int
sg_file_load(
    struct sg_filedata **data,
    const char *path,
    size_t pathlen,
    unsigned flags,
    const char *extensions,
    size_t maxsize,
    struct sg_fileinfo *fileinfo,
    struct sg_error **err)
{
    struct sg_reader fp;
    char nbuf[SG_MAX_PATH];
    pchar *pbuf = NULL;
    struct sg_path search[SG_PATH_MAXCOUNT];
    int nlen, r, searchcount, maxslen;

    r = sg_path_get(search, flags, err);
    if (r < 0)
        return -1;
    searchcount = r;
    maxslen = sg_path_maxlen(search, searchcount);

    nlen = sg_path_norm(nbuf, path, pathlen, err);
    if (nlen < 0)
        return SG_FILE_ERROR;

    if (extensions) {
        struct sg_path_ext ext[SG_PATH_MAXEXTS];
        const char *extp;
        pchar *pptr;
        int r, extcount, maxextlen, extlen, i, j, k;

        r = sg_path_extlist(ext, extensions);
        if (r < 0) {
            sg_error_invalid(err, __FUNCTION__, "extensions");
            goto error;
        }
        /* If extensions is not NULL but lists no extensions, it could
           be caused by support for all file formats being compiled
           out, in which case "file not found" is the most sensible
           result, because there is no satisfactory file which could
           be found.  */
        if (r == 0)
            goto notfound;
        extcount = r;
        maxextlen = sg_path_extmaxlen(ext, extcount);

        r = sg_path_extstrip(nbuf, nlen, NULL, ext, extcount);
        if (r < nlen) {
            sg_logf(SG_LOG_WARN,
                    "Removing extension %.*s from path: %.*s",
                    nlen - r, nbuf + r, nlen, nbuf);
        }
        nlen = r;

        if (nlen + maxextlen + 1 >= SG_MAX_PATH) {
            sg_error_sets(err, &SG_ERROR_INVALPATH, 0,
                          "Path too long for given extension list");
            return SG_FILE_ERROR;
        }

        if (!searchcount)
            goto notfound;

        pbuf = malloc((maxslen + nlen + maxextlen + 2) * sizeof(pchar));
        if (!pbuf)
            goto nomem;
        sg_path_copy(pbuf + maxslen, nbuf, nlen);
        pbuf[maxslen + nlen] = '.';
        for (i = 0; i < searchcount; i++) {
            pptr = pbuf + maxslen - search[i].len;
            pmemcpy(pptr, search[i].path, search[i].len);
            for (j = 0; j < extcount; j++) {
                extp = ext[j].p;
                extlen = ext[j].len;
                for (k = 0; k < extlen; k++)
                    pbuf[maxslen + nlen + 1 + k] = extp[k];
                pbuf[maxslen + nlen + 1 + extlen] = '\0';
                r = sg_reader_open(&fp, pptr, err);
                if (r == SG_FILE_OK) {
                    nbuf[nlen++] = '.';
                    memcpy(nbuf + nlen, extp, extlen);
                    nlen += extlen;
                    nbuf[nlen] = '\0';
                    goto success;
                } else if (r == SG_FILE_ERROR) {
                    goto error;
                }
            }
        }
    } else {
        pchar *pptr;
        int i, r;

        if (!searchcount)
            goto notfound;

        pbuf = malloc((maxslen + nlen + 1) * sizeof(pchar));
        if (!pbuf)
            goto nomem;
        sg_path_copy(pbuf + maxslen, nbuf, nlen);
        pbuf[maxslen + nlen] = '\0';
        for (i = 0; i < searchcount; i++) {
            pptr = pbuf + maxslen - search[i].len;
            pmemcpy(pptr, search[i].path, search[i].len);
            r = sg_reader_open(&fp, pptr, err);
            if (r == SG_FILE_OK) {
                goto success;
            } else if (r == SG_FILE_ERROR) {
                goto error;
            }
        }
    }

    goto notfound;

success:
    free(pbuf);
    pbuf = NULL;
    {
        int64_t flen;
        struct sg_fileinfo fi;
        struct sg_filedata *dp;
        int r;

        r = sg_reader_getinfo(&fp, &fi, err);
        if (r) {
            sg_reader_close(&fp);
            return SG_FILE_ERROR;
        }
        flen = fi.size;
        if ((uint64_t) flen > maxsize) {
            sg_error_sets(err, &SG_ERROR_DATA, 0, "file is too large");
            sg_reader_close(&fp);
            return SG_FILE_ERROR;
        }
        if (flags & SG_FILE_IFCHANGED) {
            if (fi.size == fileinfo->size && fi.mtime == fileinfo->mtime) {
                sg_reader_close(&fp);
                return SG_FILE_NOTCHANGED;
            }
        }
        dp = sg_reader_load(&fp, (size_t) flen, nbuf, nlen, err);
        sg_reader_close(&fp);
        if (!dp)
            return SG_FILE_ERROR;
        *data = dp;
        if (fileinfo)
            *fileinfo = fi;
    }
    return SG_FILE_OK;

notfound:
    sg_error_notfound(err, nbuf);
    goto error;

nomem:
    sg_error_nomem(err);
    goto error;

error:
    free(pbuf);
    return SG_FILE_ERROR;
}
