/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "file_impl.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/log.h"
#include "sg/util.h"
#include <Windows.h>

/* Returns the first part of the path not counting the drive specification.
   For "C:\dir\file.txt", returns "\dir\file.txt".
   For "\\server\mount\dir\file.txt", returns "\dir\file.txt".  */
static wchar_t *
sg_path_skipdrive(wchar_t *path)
{
    wchar_t *p, *q;
    if (path[0] == '\\' && path[1] == '\\') {
        p = wcschr(path + 2, '\\');
        if (!p)
            return path;
        q = wcschr(p + 1, '\\');
        if (q == p + 1)
            return path;
        if (!q)
            return path + wcslen(path);
        return q;
    }
    if (path[0] && path[1] == ':')
        return path + 2;
    return path;
}

int
sg_path_mkpardir(wchar_t *path, struct sg_error **err)
{
    wchar_t *start, *p, *end;
    DWORD ecode;
    BOOL r;

    start = sg_path_skipdrive(path);
    p = wcsrchr(start, '\\');
    if (!p || p == start || !*p)
        return SG_FILE_ENOTFOUND;
    end = p;
    *p = '\0';
    while (1) {
        r = CreateDirectory(path, NULL);
        if (r)
            break;
        ecode = GetLastError();
        if (ecode != ERROR_PATH_NOT_FOUND) {
            if (ecode == ERROR_ALREADY_EXISTS)
                break;
            sg_error_win32(err, ecode);
            return SG_FILE_ERROR;
        }
        p = wcsrchr(start, '\\');
        if (!p || p == start)
            return SG_FILE_ENOTFOUND;
        *p = '\0';
    }
    while (1) {
        *p = '\\';
        if (p == end)
            return SG_FILE_OK;
        p += wcslen(p);
        r = CreateDirectory(path, NULL);
        if (!r) {
            ecode = errno;
            if (ecode != ERROR_ALREADY_EXISTS) {
                sg_error_win32(err, ecode);
                return SG_FILE_ERROR;
            }
        }
    }
}

static const struct {
    unsigned char read_flags, write_flags;
    char path[14];
} sg_path_default[3] = {
    { SG_PATH_DATA, 0, SG_DEFAULTPATH_DATA },
    { 0, SG_PATH_DATA | SG_PATH_CONFIG, SG_DEFAULTPATH_USER },
    { 0, SG_PATH_CACHE, SG_DEFAULTPATH_USER "\\" SG_DEFAULTPATH_CACHE }
};

static size_t
sg_path_getexerelpath(wchar_t *buf, size_t buflen, const char *relpath)
{
    size_t len;
    const char *p;
    len = GetModuleFileName(NULL, buf, buflen);
    if (!len || len > buflen)
        return 0;
    while (len > 0 && buf[len - 1] != L'\\')
        len--;
    if (len == 0)
        return 0;
    p = relpath;
    while (len < buflen && *p)
        buf[len++] = *p++;
    if (*p)
        return 0;
    return len;
}

void
sg_path_getdefaults(
    unsigned read_flags,
    unsigned write_flags)
{
    int i;
    unsigned rflags, wflags;
    wchar_t *buf, *p;
    DWORD len;
    size_t slen, j;

    buf = malloc(MAX_PATH * sizeof(*buf));
    if (!buf) {
        sg_path_initabort(SG_PATHERR_NOMEM);
        return;
    }
    len = GetModuleFileName(NULL, buf, MAX_PATH);
    if (!len) {
        sg_path_initabort(SG_PATHERR_EXEPATH);
        return;
    }
    if (len >= MAX_PATH) {
        sg_path_initabort(SG_PATHERR_EXEPATHLONG);
        return;
    }
    buf[len] = '\0';
    p = wcsrchr(buf, '\\');
    if (!p) {
        sg_path_initabort(SG_PATHERR_EXEPATH);
        return;
    }
    len = (DWORD) (p + 1 - buf);

    for (i = 0; i < 3; i++) {
        rflags = sg_path_default[i].read_flags & ~read_flags;
        wflags = sg_path_default[i].write_flags & ~write_flags;
        if (!(rflags | wflags))
            continue;
        slen = strlen(sg_path_default[i].path);
        if (len + slen > MAX_PATH) {
            sg_path_initabort(SG_PATHERR_EXEPATHLONG);
            return;
        }
        for (j = 0; j < slen; j++)
            buf[len + j] = sg_path_default[i].path[j];
        sg_path_add(buf, len + slen, rflags, wflags);
    }
}

int
sg_filelist_scanpath(
struct sg_filelist_builder *bp,
    const wchar_t *path,
    int pathlen,
    unsigned flags,
    struct sg_error **err)
{
    wchar_t *buf = NULL;
    char rel[SG_MAX_PATH];
    char *data = NULL;
    int baselen = pathlen, dirlen, nlen, rellen, i, bits, r;
    unsigned dataalloc = 0, datasize = 0;
    unsigned stackalloc = 0, stacksize = 0, *stack = NULL;
    WIN32_FIND_DATA fd;
    HANDLE h = INVALID_HANDLE_VALUE;
    struct sg_fileinfo *info;

    buf = malloc(sizeof(wchar_t) * (pathlen + SG_MAX_PATH + 2));
    if (!buf) {
        sg_error_nomem(err);
        return -1;
    }
    wmemcpy(buf, path, baselen);
    if (!baselen || path[baselen - 1] != '\\')
        buf[baselen++] = '\\';
    buf[baselen] = '\0';
    dirlen = 0;

    while (1) {
        h = FindFirstFile(buf, &fd);
        if (h == INVALID_HANDLE_VALUE) {
            DWORD ecode;
            ecode = GetLastError();
            if (ecode == ERROR_FILE_NOT_FOUND)
                goto next_directory;
            sg_error_win32(err, ecode);
            goto error;
        }
        do {
            nlen = wcslen(fd.cFileName);
            if (nlen > SG_MAX_PATH - 1 - dirlen)
                continue;
            rellen = dirlen + nlen;
            bits = 0;
            for (i = 0; i < nlen; i++) {
                bits |= fd.cFileName[i];
                rel[dirlen + i] = (char) fd.cFileName[i];
            }
            rel[rellen] = '\0';
            if (bits >= 0x80 || sg_path_checkpart(rel + dirlen, nlen))
                continue;
            if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) {
                if (rellen > SG_MAX_PATH - 3)
                    continue;
                if ((unsigned) rellen > dataalloc - datasize) {
                    unsigned nalloc;
                    char *ndata;
                    nalloc = sg_round_up_pow2_32(datasize + rellen);
                    if (!nalloc)
                        goto error_nomem;
                    ndata = realloc(data, nalloc);
                    if (!ndata)
                        goto error_nomem;
                    data = ndata;
                    dataalloc = nalloc;
                }
                memcpy(data + datasize, rel, rellen);
                if (stacksize > stackalloc) {
                    unsigned nalloc;
                    unsigned *nstack;
                    nalloc = stackalloc ? stackalloc * 2 : 8;
                    if (!nalloc)
                        goto error_nomem;
                    nstack = realloc(stack, nalloc * sizeof(*stack));
                    if (!nstack)
                        goto error_nomem;
                    stack = nstack;
                    stackalloc = nalloc;
                }
                stack[stacksize] = datasize;
                datasize += rellen;
                stacksize++;
            } else {
                r = sg_filelist_add(bp, rel, rellen, &info);
                if (r < 0)
                    goto error;
                if (r > 0 && info) {
                    info->size = sg_32to64(
                        fd.nFileSizeHigh, fd.nFileSizeLow);
                    info->mtime = sg_32to64(
                        fd.ftLastWriteTime.dwHighDateTime,
                        fd.ftLastWriteTime.dwLowDateTime);
                }
            }
        } while (FindNextFile(h, &fd));
        FindClose(h);
    next_directory:
        if (!stacksize)
            break;
        dirlen = datasize - stack[stacksize - 1];
        datasize = stack[stacksize - 1];
        stacksize--;
        memcpy(rel, data + datasize, dirlen);
        rel[dirlen] = '/';
        dirlen++;
        for (i = 0; i < dirlen; i++) {
            buf[baselen + i] =
                data[datasize + i] == '/' ? '\\' : data[datasize + 1];
        }
        buf[baselen + dirlen] = '\0';
    }

    free(buf);
    free(stack);
    free(data);
    return 0;

error_nomem:
    sg_error_nomem(err);
    goto error;

error:
    if (h != INVALID_HANDLE_VALUE)
        FindClose(h);
    free(buf);
    free(stack);
    free(data);
    return -1;
}
