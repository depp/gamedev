/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "file_impl.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/hash.h"
#include "sg/util.h"
#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

struct sg_filelist_bentry {
    int name;
    unsigned short namelen;
    unsigned short priority;
    struct sg_fileinfo info;
};

struct sg_filelist_builder {
    struct sg_error **err;

    struct sg_path_ext ext[SG_PATH_MAXEXTS];
    int extcount;

    struct sg_filelist_bentry *entry;
    unsigned entrycount;
    unsigned entryalloc;

    char *name;
    size_t namelen;
    size_t namealloc;

    int priority_base;
    int get_info;
};

static void
sg_filelist_builder_init(
    struct sg_filelist_builder *bp,
    unsigned flags,
    struct sg_error **err);

static void
sg_filelist_builder_destroy(
    struct sg_filelist_builder *bp);

static int
sg_filelist_builder_copyout(
    struct sg_filelist *list,
    struct sg_filelist_builder *bp,
    struct sg_error **err);

int
sg_filelist_scan(
    struct sg_filelist *list,
    const char *path,
    size_t pathlen,
    unsigned flags,
    const char *extensions,
    struct sg_error **err)
{
    struct sg_filelist_builder builder;
    struct sg_path search[SG_PATH_MAXCOUNT];
    char nbuf[SG_MAX_PATH];
    int r, nlen, searchcount, maxslen, i, plen;
    pchar *pbuf, *pptr;

    sg_filelist_builder_init(&builder, flags, err);

    r = sg_path_get(search, flags, err);
    if (r < 0)
        return -1;
    searchcount = r;

    nlen = sg_path_norm(nbuf, path, pathlen, err);
    if (nlen < 0)
        return -1;

    if (extensions) {
        r = sg_path_extlist(builder.ext, extensions);
        if (r < 0) {
            sg_error_invalid(err, __FUNCTION__, "extensions");
            return -1;
        }
        builder.extcount = r;
    } else {
        builder.extcount = -1;
    }

    if (searchcount > 0) {
        maxslen = sg_path_maxlen(search, searchcount);
        pbuf = malloc((maxslen + nlen + 1) * sizeof(pchar));
        if (!pbuf) {
            sg_error_nomem(err);
            return -1;
        }
        sg_path_copy(pbuf + maxslen, nbuf, nlen);
        pbuf[maxslen + nlen] = 0;
        for (i = 0; i < searchcount; i++) {
            plen = search[i].len;
            pptr = pbuf + maxslen - plen;
            pmemcpy(pptr, search[i].path, plen);
            builder.priority_base = i * SG_PATH_MAXEXTS;
            r = sg_filelist_scanpath(
                &builder, pptr, plen + nlen, flags, err);
            if (r) {
                sg_filelist_builder_destroy(&builder);
                return -1;
            }
        }
    }

    return sg_filelist_builder_copyout(list, &builder, err);
}

void
sg_filelist_destroy(
    struct sg_filelist *list)
{
    free(list->files);
    free(list->data_);
}

int
sg_filelist_add(
    struct sg_filelist_builder *bp,
    const char *path,
    int pathlen,
    struct sg_fileinfo **info)
{
    char *np = bp->name;
    struct sg_filelist_bentry *es = bp->entry;
    unsigned i, pos, n = bp->entryalloc, nn, hash;
    int npathlen, priority;

    priority = bp->priority_base;
    if (bp->extcount >= 0) {
        int extindex;
        npathlen = sg_path_extstrip(
            path, pathlen, &extindex, bp->ext, bp->extcount);
        if (npathlen == pathlen)
            return 0;
        priority += extindex;
    } else {
        npathlen = pathlen;
    }

    hash = sg_hash(path, npathlen);
    for (i = 0; i < n; i++) {
        pos = (i + hash) & (n - 1);
        if (es[pos].name < 0)
            break;
        if (es[pos].namelen == npathlen &&
            !memcmp(path, np + es[pos].name, npathlen)) {
            if (priority >= es[pos].priority) {
                return 0;
            } else {
                es[pos].priority = priority;
                *info = &es[pos].info;
                return 1;
            }
        }
    }

    nn = bp->entrycount + 1;
    nn = sg_round_up_pow2_32(nn + (nn >> 1));
    if (nn < n)
        goto nomem;
    if (nn > n) {
        struct sg_filelist_bentry *nes;
        nes = malloc(sizeof(*nes) * nn);
        if (!nes)
            goto nomem;
        for (i = 0; i < nn; i++)
            nes[i].name = -1;
        for (i = 0; i < n; i++) {
            unsigned j, pos2;
            if (es[i].name < 0)
                continue;
            hash = sg_hash(np + es[i].name, es[i].namelen);
            for (j = 0; ; j++) {
                assert(j < nn);
                pos2 = (hash + j) & (nn - 1);
                if (nes[pos2].name < 0) {
                    nes[pos2] = es[i];
                    break;
                }
            }
        }
        free(es);
        es = nes;
        n = nn;
        bp->entry = es;
        bp->entryalloc = n;
        for (i = 0; ; i++) {
            assert(i < n);
            pos = (hash + i) & (n - 1);
            if (es[pos].name < 0)
                break;
        }
    }

    {
        size_t a = bp->namealloc, len = bp->namelen;
        if (len > INT_MAX)
            goto nomem;
        if ((size_t) npathlen + 1 > a - len) {
            char *nnp;
            size_t na = sg_round_up_pow2(len + npathlen + 1);
            if (!na)
                goto nomem;
            nnp = realloc(np, na);
            if (!nnp)
                goto nomem;
            bp->name = nnp;
            bp->namealloc = na;
            np = nnp;
        }

        es[pos].name = (int) len;
        es[pos].namelen = npathlen;
        es[pos].priority = priority;
        memcpy(np + len, path, npathlen);
        np[len + npathlen] = '\0';
        bp->namelen = len + npathlen + 1;
        bp->entrycount++;

        es[pos].info.size = 0;
        es[pos].info.mtime = 0;
        if (bp->get_info) {
            *info = &es[pos].info;
            return 1;
        } else {
            return 0;
        }
    }

nomem:
    sg_error_nomem(bp->err);
    return -1;
}

static void
sg_filelist_builder_init(
    struct sg_filelist_builder *bp,
    unsigned flags,
    struct sg_error **err)
{
    bp->err = err;
    bp->extcount = -1;
    bp->entry = NULL;
    bp->entrycount = 0;
    bp->entryalloc = 0;
    bp->name = NULL;
    bp->namelen = 0;
    bp->namealloc = 0;
    bp->priority_base = 0;
    bp->get_info = (flags & SG_FILELIST_GETINFO) != 0;
}

static void
sg_filelist_builder_destroy(
    struct sg_filelist_builder *bp)
{
    free(bp->entry);
    free(bp->name);
}

static int
sg_fileentry_compare(const void *x, const void *y)
{
    const struct sg_fileentry *ex = x, *ey = y;
    return strcmp(ex->path, ey->path);
}

static int
sg_filelist_builder_copyout(
    struct sg_filelist *list,
    struct sg_filelist_builder *bp,
    struct sg_error **err)
{
    char *name = bp->name;
    struct sg_filelist_bentry *be = bp->entry;
    struct sg_fileentry *ee;
    int i, j, n = bp->entrycount, nn = bp->entryalloc;

    if (!n) {
        list->files = NULL;
        list->count = 0;
        list->data_ = NULL;
        free(be);
        free(name);
        return 0;
    }

    ee = malloc(sizeof(*ee) * n);
    if (!ee) {
        sg_error_nomem(err);
        return -1;
    }

    j = 0;
    for (i = 0; i < nn; i++) {
        if (be[i].name < 0)
            continue;
        assert(j < n);
        ee[j].path = name + be[i].name;
        ee[j].pathlen = be[i].namelen;
        ee[j].info = be[i].info;
        j++;
    }
    assert(j == n);

    qsort(ee, n, sizeof(*ee), sg_fileentry_compare);

    list->files = ee;
    list->count = n;
    list->data_ = name;
    free(be);
    return 0;
}
