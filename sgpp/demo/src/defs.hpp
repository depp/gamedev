/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sgpp/log.hpp"
#include "sgpp/mat.hpp"
#include "sgpp/quat.hpp"
#include "sgpp/shader.hpp"
#include "sgpp/vec.hpp"

union sg_event;
namespace SGTest {
using SG::Log;
using SG::IVec2;
using SG::IVec3;
using SG::Vec2;
using SG::Vec3;
using SG::Mat3;
using SG::Mat4;
using SG::Quat;

/// A screen in the test program.
class Screen {
public:
    Screen() = default;
    Screen(const Screen &) = delete;
    virtual ~Screen();
    Screen &operator=(const Screen &) = delete;

    /// Load the screen's resources.
    virtual bool load() = 0;
    /// Draw the screen, given the current window size and timestamp.
    virtual void draw(IVec2 wsize, double time) = 0;
    // Handle an event.
    virtual void handle_event(const sg_event &evt) = 0;
    // Get the previous screen, or null if this is the main menu.
    virtual void go_previous();
};

/// Set the active screen.
void set_screen(Screen *screen);

/// Create a main screen instance.
Screen *main_screen();

/// Create an image screen instance.
Screen *image_screen();

/// Create a type screen instance.
Screen *type_screen();

/// Create an audio screen instance.
Screen *audio_screen();

namespace Shader {

struct Plain {
    static const struct SG::ShaderField FIELDS[];
    // Attr: Loc
    GLint VertOff, VertScale, Color;
};

struct Background {
    static const struct SG::ShaderField FIELDS[];
    // Attr: Loc
    GLint TexMat, Texture, TexSel;
};

struct Textured {
    static const struct SG::ShaderField FIELDS[];
    // Attr: Loc, TexCoord
    GLint View, Texture;
};

struct Text {
    static const struct SG::ShaderField FIELDS[];
    // Attr: Vertex
    GLint VertOff, VertScale, TexScale, Texture, FgColor, BgColor;
};

struct Text2 {
    static const struct SG::ShaderField FIELDS[];
    // Attr: VertPos, TexPos, Size
    GLint VertOff, VertScale, TexScale, Colors, Texture;
};

}

}
