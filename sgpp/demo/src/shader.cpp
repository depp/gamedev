/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include <cstddef>
namespace SGTest {
namespace Shader {

#define F(name) {  "" #name,       offsetof(TYPE, name) }
#define A(name) {  "" #name "[0]", offsetof(TYPE, name) }

#define TYPE Plain
const struct SG::ShaderField TYPE::FIELDS[] = {
    F(VertOff),
    F(VertScale),
    F(Color),
    {0, 0}
};
#undef TYPE

#define TYPE Background
const struct SG::ShaderField TYPE::FIELDS[] = {
    F(TexMat),
    F(Texture),
    F(TexSel),
    {0, 0}
};
#undef TYPE

#define TYPE Textured
const struct SG::ShaderField TYPE::FIELDS[] = {
    F(View),
    F(Texture),
    {0, 0}
};
#undef TYPE

#define TYPE Text
const struct SG::ShaderField TYPE::FIELDS[] = {
    F(VertOff),
    F(VertScale),
    F(TexScale),
    F(Texture),
    F(FgColor),
    F(BgColor),
    {0, 0}
};
#undef TYPE

#define TYPE Text2
const struct SG::ShaderField TYPE::FIELDS[] = {
    F(VertOff),
    F(VertScale),
    F(TexScale),
    A(Colors),
    F(Texture),
    {0, 0}
};
#undef TYPE

}
}
