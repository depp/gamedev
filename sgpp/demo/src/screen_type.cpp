/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "sggl/3_2.h"
#include "sgpp/color.hpp"
#include "sgpp/opengl.hpp"
#include "sgpp/text.hpp"
#include <cstring>
using namespace gl_3_2;
using namespace SG;

namespace SGTest {

namespace {

const int TEXT_COUNT = 4;
const int LAYOUT_COUNT = 7;

const char *const TEXT[TEXT_COUNT] = {
"Text Demo",

// Greek text with diacritical marks in decomposed form.  Without
// HarfBuzz, this will display incorrectly, with rectangles in place
// of some of the glyphs.
"\xce\xa4\xce\xbf \xce\xa7\xce\xbf\xcc\x81\xce\xb2\xce\xb5\xcf\x81"
"\xce\xba\xcf\x81\xce\xb1\xcc\x81\xcf\x86\xcf\x84 \xce\xbc\xce\xbf"
"\xcf\x85 \xce\xb5\xce\xb9\xcc\x81\xce\xbd\xce\xb1\xce\xb9 \xce\xb3"
"\xce\xb5\xce\xbc\xce\xb1\xcc\x81\xcf\x84\xce\xbf \xcf\x87\xce\xb5"
"\xcc\x81\xce\xbb\xce\xb9\xce\xb1",

// Long text for testing word wrapping.
"Solemnly he came forward and mounted the round gunrest. He faced about and "
"blessed gravely thrice the tower, the surrounding land and the awaking "
"mountains. Then, catching sight of Stephen Dedalus, he bent towards him "
"and made rapid crosses in the air, gurgling in his throat and shaking his "
"head. Stephen Dedalus, displeased and sleepy, leaned his arms on the top "
"of the staircase and looked coldly at the shaking gurgling face that "
"blessed him, equine in its length, and at the light untonsured hair, "
"grained and hued like pale oak.",

// Instructions.
"1: invert, 2: boxes"
};

// A fullscreen triangle in clip space.
/*
const short BGDATA[] = {
    -1, -1, 3, -1, -1, 3
};
*/

}

class TypeScreen : public Screen {
private:
    struct Text {
        int index;
        IVec2 point;
    };

    SG::Program<Shader::Text2> m_prog_text;
    Text m_text[LAYOUT_COUNT];
    GLuint m_arr;
    bool m_dark, m_boxes;

    SG::Text::System m_textsys;
    SG::Text::Batch m_textbatch;

public:
    TypeScreen();
    virtual ~TypeScreen();

    virtual bool load();
    virtual void draw(IVec2 wsize, double time);
    virtual void handle_event(const sg_event &evt);

private:
    bool load_text();
    bool load_arrays();

    void set_layout_pos(int index, IVec2 pos);
};

TypeScreen::TypeScreen()
    : m_textsys(),
      m_textbatch(m_textsys) {
    m_arr = 0;
    m_dark = false;
    m_boxes = false;
}

TypeScreen::~TypeScreen() {}

bool TypeScreen::load() {
    SG::DebugGroup dbg("TypeScreen::load");
    bool success = true;

    {
        SG::ProgramSpec s{
            {"VertPos", "TexPos", "Size", "Style"},
            {{GL_VERTEX_SHADER, "text2"},
             {GL_GEOMETRY_SHADER, "text2"},
             {GL_FRAGMENT_SHADER, "text2"}}
        };
        if (!m_prog_text.load(s)) {
            success = false;
        }
    }

    m_textsys.sync();
    if (!load_text()) {
        success = false;
    }
    if (!load_arrays()) {
        success = false;
    }

    return success;
}

bool TypeScreen::load_text() {
    using SG::Text::HAlign;
    using SG::Text::VAlign;
    SG::Text::FontDesc font;

    SG::Text::LayoutBuilder lb;
    {
        lb.clear();
        font.family = "Roboto";
        font.size = 32.0f;
        lb.set_font(font);
        lb.set_style(0);
        lb.add_text(TEXT[0]);
        SG::Text::Layout layout = lb.typeset(m_textsys);
        const auto &m = layout.metrics();
        m_text[0] = Text{
            m_textbatch.add(layout),
            m.point(HAlign::LEFT, VAlign::BOTTOM)
        };
        m_text[1] = Text{
            m_textbatch.add(layout),
            m.point(HAlign::RIGHT, VAlign::BOTTOM)
        };
        m_text[2] = Text{
            m_textbatch.add(layout),
            m.point(HAlign::LEFT, VAlign::TOP)
        };
        m_text[3] = Text{
            m_textbatch.add(layout),
            m.point(HAlign::RIGHT, VAlign::TOP)
        };
    }
    {
        lb.clear();
        font.family = "Noto Serif";
        font.size = 32.0f;
        lb.set_font(font);
        lb.set_style(1);
        lb.add_text(TEXT[1]);
        SG::Text::Layout layout = lb.typeset(m_textsys);
        const auto &m = layout.metrics();
        m_text[4] = Text{
            m_textbatch.add(layout),
            m.point(HAlign::LEFT, VAlign::TOP)
        };
    }
    {
        lb.clear();
        font.family = "Roboto";
        font.size = 16.0f;
        lb.set_font(font);
        lb.set_style(2);
        lb.set_width(300.0f);
        lb.add_text(TEXT[2]);
        SG::Text::Layout layout = lb.typeset(m_textsys);
        const auto &m = layout.metrics();
        m_text[5] = Text{
            m_textbatch.add(layout),
            m.point(HAlign::LEFT, VAlign::TOP)
        };
    }
    {
        lb.clear();
        font.family = "Roboto";
        font.size = 16.0f;
        lb.set_font(font);
        lb.set_style(3);
        lb.add_text(TEXT[3]);
        SG::Text::Layout layout = lb.typeset(m_textsys);
        const auto &m = layout.metrics();
        m_text[6] = Text{
            m_textbatch.add(layout),
            m.point(HAlign::CENTER, VAlign::BOTTOM)
        };
    }

    return true;
}

bool TypeScreen::load_arrays() {
    glGenVertexArrays(1, &m_arr);
    const auto &data = m_textsys.array_data();

    glBindVertexArray(m_arr);
    glBindBuffer(GL_ARRAY_BUFFER, data.buffer);
    const GLsizei STRIDE = 16;
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_SHORT, GL_FALSE, STRIDE,
        reinterpret_cast<const void *>(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
        1, 2, GL_SHORT, GL_FALSE, STRIDE,
        reinterpret_cast<const void *>(4));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
        2, 2, GL_SHORT, GL_FALSE, STRIDE,
        reinterpret_cast<const void *>(8));
    glEnableVertexAttribArray(3);
    glVertexAttribIPointer(
        3, 2, GL_SHORT, STRIDE,
        reinterpret_cast<const void *>(12));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return true;
}

void TypeScreen::draw(IVec2 wsize, double time) {
    SG::DebugGroup dbg("TypeScreen::draw");
    (void) time;
    glViewport(0, 0, wsize[0], wsize[1]);
    glClearColor(Color::db32(m_dark ? 21 : 1));
    glClear(GL_COLOR_BUFFER_BIT);

    if (m_prog_text.is_loaded()) {
        const auto &fdata = m_textsys.font_data();
        const auto &prog = m_prog_text;
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        glUseProgram(prog.prog());

        glUniform2f(
            prog->VertScale,
            (float) (2.0 / wsize[0]),
            (float) (2.0 / wsize[1]));
        glUniform1i(prog->Texture, 0);
        {
            const int N = 4;
            Color c[N];
            for (int i = 0; i < N; i++) {
                c[i] = Color::db32(21 - i);
            }
            glUniform4fv(prog->Colors, N, c[0].v);
        }
        glUniform2fv(prog->VertOff, 1,
                     (static_cast<Vec2>(wsize) * -0.5f).v);
        glUniform2fv(prog->TexScale, 1, fdata.texscale.v);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, fdata.texture);

        set_layout_pos(0, IVec2{{10, 10}});
        set_layout_pos(1, IVec2{{wsize[0] - 10, 10}});
        set_layout_pos(2, IVec2{{10, wsize[1] - 10}});
        set_layout_pos(3, IVec2{{wsize[0] - 10, wsize[1] - 10}});
        set_layout_pos(4, IVec2{{10, wsize[1] - 50}});
        set_layout_pos(5, IVec2{{10, wsize[1] - 90}});
        set_layout_pos(6, IVec2{{wsize[0]/2, 10}});
        m_textsys.sync();

        const auto range = m_textbatch.range();
        glBindVertexArray(m_arr);
        glDrawArrays(GL_POINTS, range.first, range.count);
        glBindVertexArray(0);
    }
}

void TypeScreen::set_layout_pos(int index, IVec2 pos) {
    auto &t = m_text[index];
    m_textbatch.set(t.index, pos - t.point);
}

void TypeScreen::handle_event(const sg_event &evt) {
    (void) &evt;
}

Screen *type_screen() {
    return new TypeScreen();
}

}
