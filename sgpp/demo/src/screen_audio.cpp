/* Copyright 2013-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "sg/event.h"
#include "sg/keycode.h"
#include "sg/mixer.h"
#include "sggl/3_2.h"
#include "sgpp/color.hpp"
#include "sgpp/opengl.hpp"
using namespace gl_3_2;
using namespace SG;

namespace SGTest {

namespace {

const int NUM_CHANNEL = 10;

enum Audio {
    CLANK, DONK, TINK, STEREO, LEFT, RIGHT, MUSIC, ALIEN
};
const int NUM_AUDIO = static_cast<int>(Audio::ALIEN) + 1;

const char AUDIO_NAME[NUM_AUDIO][10] = {
    "clank1",
    "donk1",
    "tink1",
    "stereo",
    "left",
    "right",
    "looptrack",
    "alien"
};

static const float DATA[] = {
    -0.9f, -0.9f,
    -0.9f, +0.9f,
    +0.9f, +0.9f,
    +0.9f, -0.9f
};

}

class AudioScreen : public Screen {
private:
    struct Chan {
        int state;
        int mod;
        sg_mixer_channel *chan;
    };

    SG::Program<Shader::Plain> m_prog_plain;
    GLuint m_buffer;
    GLuint m_array;
    sg_mixer_sound *m_audio[NUM_AUDIO];
    unsigned m_cmd;
    Chan m_ch[4];

public:
    AudioScreen();
    virtual ~AudioScreen();

    virtual bool load();
    virtual void draw(IVec2 wsize, double time);
    virtual void handle_event(const sg_event &evt);

private:
    sg_mixer_sound *audio(Audio a);
    bool get_state(int idx);
    void draw_1(double time);
    void draw_2(double time);
    void draw_3(double time);
    void draw_4(double time);
    void draw_key(int key, int state);
};

AudioScreen::AudioScreen()
    : m_buffer(0),
      m_array(0),
      m_cmd(0) {
    for (int i = 0; i < 4; i++) {
        m_ch[i].state = 0;
        m_ch[i].mod = 0;
        m_ch[i].chan = nullptr;
    }
}

AudioScreen::~AudioScreen() {}

bool AudioScreen::load() {
    SG::DebugGroup dbg("AudioScreen::load");
    bool success = true;

    std::string base("fx/");
    for (int i = 0; i < NUM_AUDIO; i++) {
        std::string path(base + AUDIO_NAME[i]);
        m_audio[i] = sg_mixer_sound_file(path.data(), path.size(), nullptr);
    }

    {
        ProgramSpec s{
            {"loc"},
            {{GL_FRAGMENT_SHADER, "plain"},
             {GL_VERTEX_SHADER, "plain"}}
        };
        if (!m_prog_plain.load(s)) {
            success = false;
        }
    }

    glGenBuffers(1, &m_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(DATA), DATA, GL_STATIC_DRAW);

    glGenVertexArrays(1, &m_array);
    glBindVertexArray(m_array);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return success;
}

sg_mixer_sound *AudioScreen::audio(Audio a) {
    return m_audio[static_cast<int>(a)];
}

bool AudioScreen::get_state(int idx) {
    return (m_cmd & (1u << idx)) != 0;
}

void AudioScreen::draw(IVec2 wsize, double time) {
    SG::DebugGroup dbg("AudioScreen::draw");
    glClearColor(Color::db32(14));
    glViewport(0, 0, wsize[0], wsize[1]);
    glClear(GL_COLOR_BUFFER_BIT);

    {
        const auto &prog = m_prog_plain;
        glUseProgram(prog.prog());
        glBindVertexArray(m_array);
        float sc = (float) (1.0 / NUM_CHANNEL);
        glUniform2f(prog->VertScale, sc, sc);
        draw_1(time);
        draw_2(time);
        draw_3(time);
        draw_4(time);
    }
}

void AudioScreen::draw_1(double time) {
    auto &ch = m_ch[0];
    int state = get_state(0);

    if (state && !ch.state) {
        sg_mixer_sound *snd = nullptr;
        switch (ch.mod) {
        case 0: snd = audio(Audio::CLANK); break;
        case 1: snd = audio(Audio::DONK);  break;
        case 2: snd = audio(Audio::TINK);  break;
        }
        ch.mod = (ch.mod + 1) % 3;
        sg_mixer_channel_play(snd, time, SG_MIXER_FLAG_DETACHED);
    }

    ch.state = state;
    draw_key(0, state);
}

void AudioScreen::draw_2(double time) {
    auto &ch = m_ch[1];
    int state = get_state(1);
    float pan = 0.0f;

    if (state && !ch.state) {
        sg_mixer_sound *snd = nullptr;
        switch (ch.mod / 3) {
        case 0: snd = audio(Audio::CLANK); break;
        case 1: snd = audio(Audio::DONK);  break;
        case 2: snd = audio(Audio::TINK);  break;
        }
        switch (ch.mod % 3) {
        case 0: pan = 0.0f; break;
        case 1: pan = -1.0f; break;
        case 2: pan = 1.0f; break;
        }
        ch.mod = (ch.mod + 1) % 9;
        ch.chan = sg_mixer_channel_play(snd, time, SG_MIXER_FLAG_DETACHED);
        sg_mixer_channel_setparam(ch.chan, SG_MIXER_PARAM_PAN, pan);
    }

    ch.state = state;
    draw_key(1, state);
}

void AudioScreen::draw_3(double time) {
    auto &ch = m_ch[2];
    int state = get_state(2);

    if (state && !ch.state) {
        sg_mixer_sound *snd = nullptr;
        float pan = 0.0f;
        switch (ch.mod) {
        case 0: snd = audio(Audio::STEREO); pan =  0.00f; break;
        case 1: snd = audio(Audio::LEFT);   pan = -0.75f; break;
        case 2: snd = audio(Audio::RIGHT);  pan = +0.75f; break;
        }
        ch.mod = (ch.mod + 1) % 3;
        ch.chan = sg_mixer_channel_play(snd, time, SG_MIXER_FLAG_DETACHED);
        sg_mixer_channel_setparam(ch.chan, SG_MIXER_PARAM_PAN, pan);
    }

    ch.state = state;
    draw_key(2, state);
}

void AudioScreen::draw_4(double time) {
    auto &ch = m_ch[3];
    int state = get_state(3);

    if (ch.chan && sg_mixer_channel_isdone(ch.chan)) {
        sg_mixer_channel_stop(ch.chan);
        ch.chan = nullptr;
    }

    switch (ch.state & 3) {
    case 0:
        if (state) {
            auto snd = audio((ch.state & 4) ? Audio::ALIEN : Audio::MUSIC);
            ch.chan = sg_mixer_channel_play(snd, time, SG_MIXER_FLAG_LOOP);
            ch.state++;
        }
        break;

    case 2:
        if (state) {
            sg_mixer_channel_stop(ch.chan);
            ch.chan = nullptr;
            ch.state++;
        }
        break;

    default:
        if (!state) {
            ch.state++;
        }
        break;
    }

    ch.state &= 7;
    draw_key(3, state);
}

void AudioScreen::draw_key(int key, int state) {
    int n = NUM_CHANNEL;
    const auto &prog = m_prog_plain;
    glUniform2f(prog->VertOff, 2*key+1-n, 1-n);
    glUniform4fv(prog->Color, 1, Color::db32(state ? 8 : 3).v);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glUniform4fv(prog->Color, 1, Color::db32(5).v);
    glDrawArrays(GL_LINE_LOOP, 0, 4);
}

void AudioScreen::handle_event(const sg_event &evt) {
    int cmd;
    switch (evt.common.type) {
    case SG_EVENT_KDOWN:
    case SG_EVENT_KUP:
        cmd = -1;
        switch (evt.key.key) {
        case KEY_1: cmd = 0; break;
        case KEY_2: cmd = 1; break;
        case KEY_3: cmd = 2; break;
        case KEY_4: cmd = 3; break;
        case KEY_5: cmd = 4; break;
        case KEY_6: cmd = 5; break;
        case KEY_7: cmd = 6; break;
        case KEY_8: cmd = 7; break;
        case KEY_9: cmd = 8; break;
        case KEY_0: cmd = 9; break;
        default: break;
        }
        if (cmd >= 0) {
            if (evt.key.type == SG_EVENT_KDOWN)
                m_cmd |= 1u << cmd;
            else
                m_cmd &= ~(1u << cmd);
        }
        break;

    default:
        break;
    }

}

Screen *audio_screen() {
    return new AudioScreen();
}

}
