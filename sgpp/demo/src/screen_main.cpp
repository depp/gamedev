/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "sg/event.h"
#include "sg/keycode.h"
#include "sggl/3_2.h"
#include "sgpp/color.hpp"
#include "sgpp/opengl.hpp"
#include "sgpp/text.hpp"
#include <cstring>
#include <vector>
using namespace gl_3_2;
using namespace SG;

namespace SGTest {

namespace {

struct Test {
    char name[16];
    Screen *(*func)();
};

const Test TEST[] = {
    { "Image", image_screen },
    { "Typesetting", type_screen },
    { "Audio", audio_screen }
};
const int NUM_TEST = sizeof(TEST) / sizeof(*TEST);

const int BOX_MARGIN =   4;
const int BOX_WIDTH  = 100;
const int BOX_HEIGHT =  18;

const short BOX_VERTEX[] = {
    -BOX_MARGIN, -BOX_MARGIN,
    BOX_MARGIN + BOX_WIDTH, -BOX_MARGIN,
    BOX_MARGIN + BOX_WIDTH, BOX_MARGIN + BOX_HEIGHT,
    -BOX_MARGIN, BOX_MARGIN + BOX_HEIGHT
};

}

class MainScreen : public Screen {
private:
    int m_selection;

    SG::Program<Shader::Plain> m_prog_plain;
    SG::Program<Shader::Text2> m_prog_text;

    GLuint m_buffer;
    GLuint m_array[2];
    SG::Text::System m_textsys;
    SG::Text::Batch m_textbatch;

public:
    MainScreen();
    virtual ~MainScreen();

    virtual bool load();
    virtual void draw(IVec2 wsize, double time);
    virtual void handle_event(const sg_event &evt);
    virtual void go_previous();

private:
    bool load_text();
    void set_selection(int selection);
};

MainScreen::MainScreen()
    : m_selection(0),
      m_buffer(0),
      m_array{0, 0},
      m_textbatch(m_textsys) {
}

MainScreen::~MainScreen() {}

bool MainScreen::load() {
    SG::DebugGroup dbg("MainScreen::load");
    bool success = true;

    {
        SG::ProgramSpec s{
            {"Loc"},
            {{GL_VERTEX_SHADER, "plain"},
             {GL_FRAGMENT_SHADER, "plain"}}
        };
        if (!m_prog_plain.load(s)) {
            success = false;
        }
    }
    {
        SG::ProgramSpec s{
            {"VertPos", "TexPos", "Size", "Style"},
            {{GL_VERTEX_SHADER, "text2"},
             {GL_GEOMETRY_SHADER, "text2"},
             {GL_FRAGMENT_SHADER, "text2"}}
        };
        if (!m_prog_text.load(s)) {
            success = false;
        }
    }

    if (!load_text()) {
        return false;
    }

    return success;
}

bool MainScreen::load_text() {
    SG::Text::FontDesc font;
    font.family = "Roboto";

    SG::Text::LayoutBuilder lb;
    for (int i = 0; i < NUM_TEST; i++) {
        lb.clear();
        lb.set_font(font);
        lb.set_style(i);
        lb.add_text(TEST[i].name);
        SG::Text::Layout layout = lb.typeset(m_textsys);
        m_textbatch.add(
            layout,
            IVec2{{0, -32 * i}} -
            layout.metrics().point(
                SG::Text::HAlign::LEFT, SG::Text::VAlign::TOP));
    }

    m_textsys.sync();

    glGenVertexArrays(2, m_array);
    glGenBuffers(1, &m_buffer);

    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(BOX_VERTEX), BOX_VERTEX,
                 GL_STATIC_DRAW);
    glBindVertexArray(m_array[0]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_SHORT, GL_FALSE, 0, 0);

    const GLsizei STRIDE = 16;
    const auto &data = m_textsys.array_data();
    glBindVertexArray(m_array[1]);
    glBindBuffer(GL_ARRAY_BUFFER, data.buffer);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_SHORT, GL_FALSE, STRIDE,
        reinterpret_cast<const void *>(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
        1, 2, GL_SHORT, GL_FALSE, STRIDE,
        reinterpret_cast<const void *>(4));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
        2, 2, GL_SHORT, GL_FALSE, STRIDE,
        reinterpret_cast<const void *>(8));
    glEnableVertexAttribArray(3);
    glVertexAttribIPointer(
        3, 2, GL_SHORT, STRIDE,
        reinterpret_cast<const void *>(12));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return true;
}

void MainScreen::draw(IVec2 wsize, double time) {
    SG::DebugGroup dbg("MainScreen::draw");

    (void) time;

    glViewport(0, 0, wsize[0], wsize[1]);
    glClearColor(Color::db32(1));
    glClear(GL_COLOR_BUFFER_BIT);

    float vertscale[2] = { 2.0f / (float) wsize[0], 2.0f / (float) wsize[1] };

    if (m_prog_plain.is_loaded()) {
        int i = m_selection;
        const auto &prog = m_prog_plain;
        glUseProgram(prog.prog());
        glBindVertexArray(m_array[0]);

        glUniform2f(
            prog->VertOff,
            32.0f - 0.5f * (float) wsize[0],
            0.5f * (float) wsize[1] - 32.0f * (float) (i + 1) - BOX_HEIGHT);
        glUniform2fv(prog->VertScale, 1, vertscale);
        glUniform4fv(prog->Color, 1, Color::db32(20).v);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    }

    if (m_prog_text.is_loaded()) {
        const auto &fdata = m_textsys.font_data();
        const auto &prog = m_prog_text;
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
        glUseProgram(prog.prog());
        glBindVertexArray(m_array[1]);

        glUniform2f(
            prog->VertScale,
            (float) (2.0 / wsize[0]),
            (float) (2.0 / wsize[1]));
        glUniform1i(prog->Texture, 0);
        {
            Color c[NUM_TEST];
            std::fill(std::begin(c), std::end(c), Color::db32(20));
            c[m_selection] = Color::db32(0);
            glUniform4fv(prog->Colors, NUM_TEST, c[0].v);
        }
        {
            Vec2 voff{{
                static_cast<float>(-wsize[0] + 64) * 0.5f,
                static_cast<float>(+wsize[1] - 64) * 0.5f
            }};
            glUniform2fv(prog->VertOff, 1, voff.v);
        }
        glUniform2fv(prog->TexScale, 1, fdata.texscale.v);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, fdata.texture);
        const auto range = m_textbatch.range();
        glBindVertexArray(m_array[1]);
        glDrawArrays(GL_POINTS, range.first, range.count);

        glBindVertexArray(0);
        glDisable(GL_BLEND);
    }
}

void MainScreen::handle_event(const sg_event &evt) {
    switch (evt.common.type) {
    case SG_EVENT_KDOWN:
    case SG_EVENT_KREPEAT:
        switch (evt.key.key) {
        case KEY_Down:
            set_selection(m_selection + 1);
            break;

        case KEY_Up:
            set_selection(m_selection - 1);
            break;

        case KEY_Enter:
            if (evt.common.type == SG_EVENT_KDOWN) {
                set_screen(TEST[m_selection].func());
            }
            break;

        default:
            break;
        }
        break;

    default:
        break;
    }
}

void MainScreen::set_selection(int selection) {
    if (selection < 0)
        selection += NUM_TEST;
    else if (selection >= NUM_TEST)
        selection -= NUM_TEST;
    m_selection = selection;
}

void MainScreen::go_previous() {}

Screen *main_screen() {
    return new MainScreen();
}

}
