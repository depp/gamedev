/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "sgpp/image.hpp"
#include "sgpp/opengl.hpp"
#include "sggl/3_2.h"
#include "sg/event.h"
#include "sg/keycode.h"
#include <cstring>
#include <vector>
using namespace gl_3_2;

namespace SGTest {

namespace {

struct Vertex {
    short x, y;
    unsigned char tex[4];
};

int bufalign(int x) {
    return static_cast<int>((static_cast<unsigned>(x) + 63u) & ~63u);
}

struct ImageInfo {
    char name[12];
    unsigned char x, y;
};

const int NUM_IMG = 15;
const int IMG_X = 6, IMG_Y = 5, IMG_SPACING = 128, IMG_SIZE = 96;
const ImageInfo IMAGE_INFO[NUM_IMG] = {
    { "png_i4",     0, 2 },
    { "png_i8",     0, 3 },
    { "png_ia4",    1, 2 },
    { "png_ia8",    1, 3 },
    { "png_rgb8",   2, 3 },
    { "png_rgb16",  2, 4 },
    { "png_rgba8",  3, 3 },
    { "png_rgba16", 3, 4 },
    { "png_y1",     4, 0 },
    { "png_y2",     4, 1 },
    { "png_y4",     4, 2 },
    { "png_y8",     4, 3 },
    { "png_y16",    4, 4 },
    { "png_ya8",    5, 3 },
    { "png_ya16",   5, 4 }
};

const int NUM_TEX = 3;
const char TEX_NAMES[NUM_TEX][12] = { "brick", "ivy", "roughstone" };

// A fullscreen triangle in clip space.
static const short BGDATA[] = {
    -1, -1, 3, -1, -1, 3
};

// Load a 2D array texture.
GLuint load_2darray(const std::vector<std::string> paths) {
    SG::DebugGroup dbg("load_2darray");
    GLuint buffer = 0;
    char *ptr = nullptr;
    int n = paths.size();
    bool has_image = false;
    IVec2 size = IVec2::zero();
    int planesize = 0;
    for (int i = 0; i < n; i++) {
        SG::Image img;
        if (!img.load(paths[i])) {
            continue;
        }
        IVec2 isize = img.size();
        if (!has_image) {
            size = isize;
            has_image = true;
            planesize = isize[0] * isize[1] * 4;
            glGenBuffers(1, &buffer);
            glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer);
            glBufferData(GL_PIXEL_UNPACK_BUFFER, planesize * n, nullptr,
                         GL_STREAM_DRAW);
            ptr = static_cast<char *>(
                glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY));
            if (!ptr)
                return 0;
        } else {
            if (isize != size) {
                Log::error(
                    "%s: Mismatched size, expected %dx%d but got %dx%d.",
                    paths[i].c_str(),
                    size[0], size[1], isize[0], isize[1]);
                continue;
            }
        }
        sg_pixbuf pb;
        pb.data = ptr + planesize * i;
        pb.format = SG_RGBA;
        pb.width = size[0];
        pb.height = size[1];
        pb.rowbytes = size[0] * 4;
        img.draw(pb, 0, 0);
    }
    if (!has_image) {
        return 0;
    }
    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D_ARRAY, tex);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_SRGB8_ALPHA8, size[0], size[1], n,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,
                    GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,
                    GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glDeleteBuffers(1, &buffer);
    return tex;
}

}

class ImageScreen : public Screen {
private:
    SG::Program<Shader::Background> m_prog_background;
    SG::Program<Shader::Textured> m_prog_textured;

    // Textures (both 2D arrays)
    GLuint m_fgtex, m_bgtex;
    // Buffer
    GLuint m_buffer;
    // Vertex arrays: background, foreground.
    GLuint m_arr[2];

public:
    ImageScreen();
    virtual ~ImageScreen();

    virtual bool load();
    virtual void draw(IVec2 wsize, double time);
    virtual void handle_event(const sg_event &evt);

private:
    bool load_fgtex();
    bool load_bgtex();
    bool load_buffer();
    bool load_arrays();
};

ImageScreen::ImageScreen()
    : m_fgtex(0),
      m_bgtex(0),
      m_buffer(0) {
    for (int i = 0; i < 2; i++)
        m_arr[i] = 0;
}

ImageScreen::~ImageScreen() {}

bool ImageScreen::load() {
    SG::DebugGroup dbg("ImageScreen::load");
    bool success = true;

    {
        SG::ProgramSpec s{
            {"Loc"},
            {{GL_VERTEX_SHADER, "background"},
             {GL_FRAGMENT_SHADER, "background"}}
        };
        if (!m_prog_background.load(s)) {
            success = false;
        }
    }
    {
        SG::ProgramSpec s{
            {"Loc", "TexCoord"},
            {{GL_VERTEX_SHADER, "textured"},
             {GL_FRAGMENT_SHADER, "textured"}}
        };
        if (!m_prog_textured.load(s)) {
            success = false;
        }
    }

    if (!load_fgtex()) {
        success = false;
    }
    if (!load_bgtex()) {
        success = false;
    }

    if (!load_buffer()) {
        success = false;
    }
    if (!load_arrays()) {
        success = false;
    }

    return success;
}

bool ImageScreen::load_fgtex() {
    std::string base("imgtest/");
    std::vector<std::string> paths;
    paths.reserve(NUM_IMG);
    for (int i = 0; i < NUM_IMG; i++) {
        paths.push_back(base + IMAGE_INFO[i].name);
    }
    GLuint tex = load_2darray(paths);
    if (tex == 0) {
        return false;
    }
    m_fgtex = tex;
    return true;
}

bool ImageScreen::load_bgtex() {
    std::string base("tex/");
    std::vector<std::string> paths;
    paths.reserve(NUM_TEX);
    for (int i = 0; i < NUM_TEX; i++) {
        paths.push_back(base + TEX_NAMES[i]);
    }
    GLuint tex = load_2darray(paths);
    if (tex == 0) {
        return false;
    }
    m_bgtex = tex;
    return true;
}

bool ImageScreen::load_buffer() {
    if (m_buffer) {
        return true;
    }

    int bgpos = 0;
    int bgsize = sizeof(BGDATA);
    int fgpos = bgpos + bufalign(bgsize);
    int fgsize = sizeof(Vertex) * NUM_IMG * 6;
    int size = fgpos + bufalign(fgsize);

    glGenBuffers(1, &m_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
    glBufferData(GL_ARRAY_BUFFER, size, nullptr, GL_STATIC_DRAW);
    char *ptr = static_cast<char *>(
        glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));

    memcpy(ptr, BGDATA, sizeof(BGDATA));

    Vertex *vert = reinterpret_cast<Vertex *>(ptr + fgpos);
    for (int i = 0; i < NUM_IMG; i++) {
        Vertex *v = vert + i * 6;
        const auto &ifo = IMAGE_INFO[i];
        int x = (ifo.x * 2 + 1 - IMG_X) * IMG_SPACING / 2;
        int y = (ifo.y * 2 + 1 - IMG_Y) * IMG_SPACING / 2;
        short x0 = x - IMG_SIZE / 2, x1 = x0 + IMG_SIZE;
        short y0 = y - IMG_SIZE / 2, y1 = y0 + IMG_SIZE;
        unsigned char tz = i;
        v[0] = Vertex{ x0, y0, { 0, 1, tz, 0 } };
        v[1] = Vertex{ x1, y0, { 1, 1, tz, 0 } };
        v[2] = Vertex{ x0, y1, { 0, 0, tz, 0 } };
        v[3] = Vertex{ x0, y1, { 0, 0, tz, 0 } };
        v[4] = Vertex{ x1, y0, { 1, 1, tz, 0 } };
        v[5] = Vertex{ x1, y1, { 1, 0, tz, 0 } };
    }

    glUnmapBuffer(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return true;
}

bool ImageScreen::load_arrays() {
    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
    glGenVertexArrays(2, m_arr);

    int bgpos = 0;
    int bgsize = sizeof(BGDATA);
    int fgpos = bgpos + bufalign(bgsize);
    // int fgsize = sizeof(Vertex) * NUM_IMG * 6;

    // background
    glBindVertexArray(m_arr[0]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_SHORT, GL_FALSE, 4,
        reinterpret_cast<void *>(bgpos));

    // textured
    glBindVertexArray(m_arr[1]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0, 2, GL_SHORT, GL_FALSE, 8,
        reinterpret_cast<void *>(fgpos));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
        1, 3, GL_UNSIGNED_BYTE, GL_FALSE, 8,
        reinterpret_cast<void *>(fgpos + 4));

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return true;
}

void ImageScreen::draw(IVec2 wsize, double time) {
    SG::DebugGroup dbg("ImageScreen::draw");
    glViewport(0, 0, wsize[0], wsize[1]);
    glClearColor(0.05, 0.1, 0.2, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    // csi = clip space from image space
    // ics = image space from clip space
    float sci[2], isc[2];
    {
        double ssz[2] = { (double) wsize[0], (double) wsize[1] };
        double isz[2] = { IMG_SPACING * IMG_X, IMG_SPACING * IMG_Y };
        double ssi[2] = { 1.0 / ssz[0], 1.0 / ssz[1] };
        double isi[2] = { 1.0 / isz[0], 1.0 / isz[1] };

        // i = 1 if screen is wider, 0 if screen is taller
        int i = ssz[0] * isz[1] > ssz[1] * isz[0], j = !i;
        sci[i] = (float) (2.0 * isi[i]);
        sci[j] = (float) (2.0 * isi[i] * ssz[i] * ssi[j]);
        isc[i] = (float) (0.5 * isz[i]);
        isc[j] = (float) (0.5 * isz[i] * ssi[i] * ssz[j]);
    }

    {
        SG::DebugGroup dbg("background");
        const auto &prog = m_prog_background;

        glDisable(GL_BLEND);
        glUseProgram(prog.prog());
        glBindVertexArray(m_arr[0]);

        double t = std::fmod(time / 8.0, NUM_TEX);
        if (t < 0.0) {
            t += NUM_TEX;
        }
        int texidx = static_cast<int>(t);
        if (texidx >= NUM_TEX || texidx < 0) {
            texidx = 0;
        }
        float t2 = (float) std::fmod(t, 1.0);

        float ts1 = 1.0f / (IMG_SPACING * IMG_X) *
            std::exp(std::log(4.0f) *
                     (0.75f + std::cos((8.0f * std::atan(1.0f)) * t2)));
        float ts[2] = { ts1 * isc[0], ts1 * isc[1] };
        float texmat[3][2] = {
            { ts[0], 0.0f }, { 0.0f, ts[1] }, { 0.0f, 0.0f }
        };
        glUniformMatrix3x2fv(prog->TexMat, 1, GL_FALSE, &texmat[0][0]);
        glUniform1i(prog->Texture, 0);
        float texsel[3] = {
            (float) texidx,
            (float) ((texidx + 1) % NUM_TEX),
            (float) std::fmod(t, 1.0)
        };
        glUniform3fv(prog->TexSel, 1, texsel);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D_ARRAY, m_bgtex);

        glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    {
        SG::DebugGroup dbg("foreground");
        const auto &prog = m_prog_textured;

        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        glUseProgram(prog.prog());
        glBindVertexArray(m_arr[1]);

        float view[3][2] = {
            { sci[0], 0.0f }, { 0.0f, sci[1] }, { 0.0f, 0.0f }
        };
        glUniformMatrix3x2fv(prog->View, 1, GL_FALSE, &view[0][0]);
        glUniform1i(prog->Texture, 1);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D_ARRAY, m_fgtex);

        glDrawArrays(GL_TRIANGLES, 0, NUM_IMG * 6);
    }
}

void ImageScreen::handle_event(const sg_event &evt) {
    (void) &evt;
}

Screen *image_screen() {
    return new ImageScreen();
}

}
