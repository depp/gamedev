/* Copyright 2014-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of the
   2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "sg/entry.h"
#include "sg/event.h"
#include "sg/keycode.h"
#include "sg/mixer.h"
#include "sg/record.h"
#include "sggl/3_2.h"
#include "sgpp/text.hpp"

using namespace gl_3_2;
namespace SGTest {

Screen::~Screen() {}

void Screen::go_previous() {
    set_screen(main_screen());
}

namespace {

Screen *g_screen, *g_nextscreen;
bool g_screen_loaded;

void update_screen() {
    if (!g_nextscreen) {
        return;
    }
    if (g_screen) {
        delete g_screen;
    }
    g_screen = g_nextscreen;
    g_nextscreen = nullptr;
    g_screen_loaded = false;
}

}

void set_screen(Screen *screen) {
    if (g_nextscreen) {
        delete g_nextscreen;
    }
    g_nextscreen = screen;
}

}

using namespace SGTest;

void sg_game_init(void) {
    sg_mixer_start();
    g_screen = main_screen();
}

void sg_game_destroy(void) {}

void sg_game_getinfo(struct sg_game_info *info) {
    info->name = "SGLib Tests";
}

void sg_game_event(union sg_event *evt) {
    switch (evt->common.type) {
    case SG_EVENT_VIDEO_INIT:
        glEnable(GL_FRAMEBUFFER_SRGB);
        break;

    case SG_EVENT_KDOWN:
        switch (evt->key.key) {
        case KEY_Backslash:
            sg_record_screenshot();
            return;

        case KEY_F10:
            sg_record_start(evt->common.time);
            return;

        case KEY_F11:
            sg_record_stop();
            return;

        case KEY_Escape:
            g_screen->go_previous();
            update_screen();
            return;
        }
        break;

    default:
        break;
    }

    g_screen->handle_event(*evt);
    update_screen();
}

void sg_game_draw(int width, int height, double time) {
    sg_mixer_settime(time);
    if (!g_screen_loaded) {
        if (!g_screen->load()) {
            Log::error("Could not load screen resources.");
        }
        g_screen_loaded = true;
    }
    g_screen->draw(IVec2{{width, height}}, time);
    update_screen();
    sg_mixer_commit();
}
