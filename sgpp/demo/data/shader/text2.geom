#version 150

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in Geom {
    vec2 TexPos;
    vec2 TexSize;
    vec4 VertX;
    vec4 VertY;
    vec4 Color;
} In[];

out Frag {
    vec2 TexPos;
    flat vec4 Color;
} Out;

void main() {
    vec2 tpos = In[0].TexPos;
    vec2 tsize = In[0].TexSize;
    vec4 vpos = gl_in[0].gl_Position;
    vec4 vx = In[0].VertX;
    vec4 vy = In[0].VertY;
    vec4 color = In[0].Color;

    Out.TexPos = tpos + vec2(0.0, tsize.y);
    Out.Color = color;
    gl_Position = vpos;
    EmitVertex();

    Out.TexPos = tpos + tsize;
    Out.Color = color;
    gl_Position = vpos + vx;
    EmitVertex();

    Out.TexPos = tpos;
    Out.Color = color;
    gl_Position = vpos + vy;
    EmitVertex();

    Out.TexPos = tpos + vec2(tsize.x, 0.0);
    Out.Color = color;
    gl_Position = vpos + vx + vy;
    EmitVertex();

    EndPrimitive();
}
