#version 150

in Frag {
    vec3 TexCoord;
} In;
out vec4 Out;

uniform sampler2DArray Texture;

void main() {
    Out = texture(Texture, In.TexCoord);
}
