#version 150

in vec4 Vertex;
out Frag {
    vec2 TexCoord;
} Out;

uniform vec2 VertOff;
uniform vec2 VertScale;
uniform vec2 TexScale;

void main() {
    Out.TexCoord = Vertex.zw * TexScale;
    gl_Position = vec4((Vertex.xy + VertOff) * VertScale, 0.0, 1.0);
}
