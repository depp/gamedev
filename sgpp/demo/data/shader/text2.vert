#version 150

in vec2 VertPos;
in vec2 TexPos;
in vec2 Size;
in ivec2 Style;

out Geom {
    vec2 TexPos;
    vec2 TexSize;
    vec4 VertX;
    vec4 VertY;
    vec4 Color;
} Out;

uniform vec2 VertOff;
uniform vec2 VertScale;
uniform vec2 TexScale;
uniform vec4 Colors[32];

void main() {
    Out.TexPos = TexPos * TexScale;
    Out.TexSize = Size * TexScale;
    Out.VertX = vec4(VertScale.x * Size.x, 0.0, 0.0, 0.0);
    Out.VertY = vec4(0.0, VertScale.y * Size.y, 0.0, 0.0);
    Out.Color = Colors[Style.x];
    gl_Position = vec4((VertPos + VertOff) * VertScale, 0.0, 1.0);
}
