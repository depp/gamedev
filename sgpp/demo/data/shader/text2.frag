#version 150

in Frag {
    vec2 TexPos;
    flat vec4 Color;
} In;

out vec4 OutColor;

uniform sampler2D Texture;

void main() {
    OutColor = In.Color * texture(Texture, In.TexPos).r;
}
