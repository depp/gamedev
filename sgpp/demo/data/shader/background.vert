#version 150

in vec2 Loc;
out Frag {
    vec2 TexCoord;
} Out;

uniform mat3x2 TexMat;

void main() {
    Out.TexCoord = TexMat * vec3(Loc, 1.0);
    gl_Position = vec4(Loc, 0.0, 1.0);
}
