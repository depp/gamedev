#version 150

in Frag {
    vec2 TexCoord;
} In;
out vec4 Out;

uniform sampler2D Texture;
uniform vec4 FgColor, BgColor;

void main() {
    Out = mix(BgColor, FgColor, texture(Texture, In.TexCoord).r);
}
