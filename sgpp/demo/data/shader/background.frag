#version 150

in Frag {
    vec2 TexCoord;
} In;
out vec4 Out;

uniform sampler2DArray Texture;
uniform vec3 TexSel;

void main() {
    Out = mix(
        texture(Texture, vec3(In.TexCoord, TexSel.x)),
        texture(Texture, vec3(In.TexCoord, TexSel.y)),
        TexSel.z);
}
