#version 150

in vec2 Loc;
in vec3 TexCoord;
out Frag {
    vec3 TexCoord;
} Out;

uniform mat3x2 View;

void main() {
    Out.TexCoord = TexCoord;
    gl_Position = vec4(View * vec3(Loc, 1.0), 0.0, 1.0);
}
