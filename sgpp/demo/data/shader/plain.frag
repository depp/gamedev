#version 150

out vec4 Out;

uniform vec4 Color;

void main() {
    Out = Color;
}
