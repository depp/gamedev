#!/usr/bin/env python3
# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of the
# 2-clause BSD license.  For more information, see LICENSE.txt.
import sys
import os.path
sys.path.append(
    os.path.normpath(
        os.path.join(os.path.dirname(__file__), '..', 'script')))
import sglib

_BASE = sglib.join_path(os.path.normpath(__file__))
def project_path(*paths):
    return sglib.join_path(_BASE, *paths)

src = sglib.SourceList(base=project_path('src/'), sources='''
defs.hpp
main.cpp
screen_audio.cpp
screen_image.cpp
screen_main.cpp
screen_type.cpp
shader.cpp
''')

icon = sglib.Icon(base=project_path('icon/'), sources='''
ico icon.ico
16 icon16.png
32 icon32.png
48 icon48.png
64 icon64.png
128 icon128.png
256 icon256.png
512 icon512.png
1024 icon1024.png
''')

def configure(build):
    return build.target.module().add_sources(
        src.sources,
        {'private': [sglib.module_sg(build), sglib.module_sgpp(build)]})

app = sglib.App(
    name='SGLib Demo',
    datapath=project_path('data/'),
    email='depp@zdome.net',
    uri='http://www.moria.us/',
    copyright='Copyright © 2011-2014 Dietrich Epp',
    identifier='us.moria.sglib-test',
    uuid='8d7f256e-ed27-443c-b350-c375d25a9e54',
    sources=src,
    icon=icon,
    opengl_version='3.3',
    opengl_extensions='''
        KHR_debug
    '''.split()
)

if __name__ == '__main__':
    app.run()
