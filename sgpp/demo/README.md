# SGLib Demo

This is a demo of some of the features of SGLib.

You should run the configuration script from the root of the SGLib directory, not from the demo directory.

    $ cd ..
    $ ./demo/config.py
