# SGPP

SGPP (or SG++) is a framework, written in C++11, for making games.
SGPP manages the rendering context, image loading, audio decoding, text rendering, and other common tasks.
A lot of the functionality in this framework is redundant with other frameworks like LibSDL, especially if you include related libraries like SDL\_image and SDL\_mixer.

Also refer to the SGC project, which is very similar, but written in C.  Both projects share common ancestry.
