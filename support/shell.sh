# Gamedev shell profile: source this file to make gamedev utilities available.
#
# This script is idempotent, you can run it multiple times and it will only
# make changes once.
if test "${ZSH_VERSION}" ; then
  gamedev_script="${(%):-%N}"
elif test "${BASH_VERSION}" ; then
  gamedev_script="${BASH_SOURCE[0]}"
else
  echo 2>&1 "Error: Unknown shell."
  return
fi
eval "$(cd $(dirname ${gamedev_script}); python3 env.py)"
