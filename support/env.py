"""Environment setup script.

Prints environment variable settings to standard output.  Used by shell.sh, but
you can also call this script directly to see what variables are set.
"""

from sys import exit, version_info, stderr, stdout, platform
if version_info.major != 3 or version_info.minor < 5:
    stderr.write('Error: Python 3.5 or greater required.\n')
    exit(255)

from os.path import abspath, dirname, join
from os import environ
from shlex import quote

def add_var(varname, value):
    """Write an environment variable setting to stdandard output."""
    if environ.get(varname) == value:
        return
    stdout.write('export {}={}\n'.format(varname, value))

def add_path(varname, values):
    """Add a path to a colon-separated search path environment variable.
    
    For example, add_path('PATH', '/usr/bin').
    """
    oldvalue = environ.get(varname, '')
    oldvalues = oldvalue.split(':')
    newvalues = []
    for value in values:
        if value not in oldvalues:
            newvalues.append(value)
    if not newvalues:
        return
    if oldvalue:
        stdout.write(
            'export {0}={1}:"${{{0}}}"\n'.format(varname, quote(value)))
    else:
        stdout.write(
            'export {0}={1}\n'.format(varname, quote(value)))

support = dirname(abspath(__file__))
root = dirname(support)
if 'GAMEDEV_ROOT' not in environ:
    add_var('PS1', '"(gamedev) ${PS1}"')
add_var('GAMEDEV_ROOT', quote(root))
add_path('PYTHONPATH', [join(support, 'python')])
paths = [join(support, 'javascript/node_modules/.bin')]
if platform == 'darwin':
    # https://github.com/numpy/numpy/issues/7801
    paths.append('/usr/sbin')
add_path('PATH', paths)
