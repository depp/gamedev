"""Top-level gamedev package.

This lets us import all packages in the gamedev repository by underneath the
gamedev package, so relative imports are not necessary.
"""
from sys import version_info, stderr, exit
if version_info.major != 3 or version_info.minor < 5:
    stderr.write('Error: Python 3.5 or greater required.\n')
    exit(255)
from os.path import abspath, dirname, join
__path__.insert(0, dirname(dirname(dirname(dirname(abspath(__file__))))))
