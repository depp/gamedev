from . import font
from . import bitmap
import codecs

class BDFFile(object):
    __slots__ = ['_fp', 'lineno']
    def __init__(self, fp):
        self._fp = fp
        self.lineno = 0
    def read(self):
        while True:
            line = self._fp.readline()
            if not line:
                raise EOFError()
            if not line.endswith('\n'):
                line += '\n'
            pos = 0
            fields = []
            while True:
                if line[pos] == '"':
                    i = pos + 1
                    while True:
                        i = line.find('"', i)
                        if i < 0:
                            raise ValueError('invalid quoted string')
                        if line[i+1] != '"':
                            break
                        i += 2
                    field = line[pos+1:i].replace('""', '"')
                    pos = i + 1
                elif 33 <= ord(line[pos]) <= 126:
                    i = line.find(' ', pos)
                    if i < 0:
                        i = line.find('\n')
                    if i < 0:
                        i = len(line)
                    field = line[pos:i]
                    pos = i
                else:
                    raise ValueError('invalid character')
                fields.append(field)
                while line[pos] == ' ':
                    pos += 1
                if line[pos] == '\n':
                    break
            if fields:
                return fields[0], fields[1:]

def get_encoding_latin(encoding):
    codec = codecs.lookup('ISO8859-{}'.format(int(encoding)))
    def decode(codepoint):
        c, n = codec.decode(chr(codepoint))
        if n != 1:
            raise ValueError('invalid codepoint')
        return c
    return decode

def get_encoding_unicode(encoding):
    def decode(codepoint):
        return unichr(codepoint)
    return decode

REGISTRY = {
    'ISO8859': get_encoding_latin,
    'ISO10646': get_encoding_unicode,
}

def get_encoding(registry, encoding):
    try:
        func = REGISTRY[registry.upper()]
    except KeyError:
        raise ValueError('unknown encoding registry: {!r}'.format(registry))
    return func(encoding)

def load_font(path):
    fnt = font.Font()
    with open(path, 'r') as fp:
        bdf = BDFFile(fp)
        while True:
            cmd, args = bdf.read()
            if cmd == 'STARTFONT':
                break

        props = None
        while True:
            cmd, args = bdf.read()
            if cmd == 'STARTCHAR':
                if props is None:
                    raise ValueError('no properties')
                advance, encoding, w, h, x, y = [None] * 6
                rows = []
                while True:
                    cmd, args = bdf.read()
                    if cmd == 'ENCODING':
                        encoding = font_encoding(int(args[0]))
                    elif cmd == 'SWIDTH':
                        pass
                    elif cmd == 'DWIDTH':
                        advance = int(args[0])
                    elif cmd == 'BBX':
                        w, h, x, y = [int(a) for a in args]
                    elif cmd == 'BITMAP':
                        rows = []
                        while True:
                            cmd, args = bdf.read()
                            if cmd == 'ENDCHAR':
                                break
                            rows.append(cmd)
                        break
                    else:
                        raise ValueError('unexpected cmd in glyph')
                if any(a is None for a in [advance, encoding, w, h, x, y]):
                    raise ValueError('missing data')
                glyph = fnt.new_glyph()
                glyph.data = bitmap.from_text(' '.join(rows), w, h)
                glyph.xoffset = x
                glyph.yoffset = y
                glyph.advance = advance
                fnt.add_encoding(glyph, encoding)
            elif cmd == 'STARTPROPERTIES':
                propdict = {prop.bdfname: prop for prop in font.PROPERTIES}
                if props is not None:
                    raise ValueError('multiple property blocks')
                props = {}
                while True:
                    cmd, args = bdf.read()
                    if cmd == 'ENDPROPERTIES':
                        break
                    if len(args) != 1:
                        raise ValueError('too many fields')
                    if cmd in props:
                        raise ValueError('duplicate property')
                    props[cmd] = args[0]
                    try:
                        prop = propdict[cmd]
                    except KeyError:
                        pass
                    else:
                        data = prop.type(args[0])
                        fnt.properties[prop.name] = data
                font_encoding = get_encoding(
                    props['CHARSET_REGISTRY'], props['CHARSET_ENCODING'])
            elif cmd == 'ENDFONT':
                break

    return fnt
