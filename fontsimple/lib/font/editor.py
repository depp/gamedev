#!/usr/bin/env python
from __future__ import absolute_import
import gtk
import cairo
from gtk import gdk
import gobject
import pygtk
import numpy
import sys
import collections
import math
import PIL.Image
pygtk.require('2.0')

from . import font
from . import gtk as fontgtk

Rect = collections.namedtuple('Rect', 'x y w h')
def merge_rects(rects):
    """Return the smallest rect containing all rects in the given rects."""
    rs = iter(rects)
    for r in rs:
        break
    else:
        raise ValueError('empty list')
    x0, y0, w, h = r
    x1 = x0 + w
    y1 = y0 + h
    for r in rs:
        x, y, w, h = r
        x0 = min(x0, x)
        y0 = min(y0, y)
        x1 = max(x1, x+w)
        y1 = max(y1, y+h)
    return Rect(x0, y0, x1-x0, y1-y0)

class Glyphs(object):
    __slots__ = ['img', 'arr', 'rects', 'rectid', 'unused', 'baselines',
                 'sel_rect', 'sel_ids', 'sel_baseline',
                 '_listener', '_order', '_rect_band', '_glyph_add_callback']
    def __init__(self, path, glyph_add_callback):
        self._listener = []
        self._glyph_add_callback = glyph_add_callback

        img = PIL.Image.open(path)
        img = img.convert("RGBA")
        w, h = img.size
        iarr = numpy.array(img.getdata()).reshape(h, w, 4)
        arr = numpy.less(iarr[...,...,0], 128)
        iarr2 = numpy.empty((h, w), numpy.uint32)
        iarr2 |= iarr[...,...,3]
        iarr2 <<= 8
        iarr2 |= iarr[...,...,0]
        iarr2 <<= 8
        iarr2 |= iarr[...,...,1]
        iarr2 <<= 8
        iarr2 |= iarr[...,...,2]
        del iarr
        img = cairo.ImageSurface.create_for_data(
            bytearray(iarr2.data), cairo.FORMAT_ARGB32, w, h, w * 4)
        self.img = img
        self.arr = arr

        h, w = arr.shape
        rectid = numpy.zeros(arr.shape, dtype=numpy.uint16)
        rects = {}
        counter = 1
        dirs = ((1, 0), (-1, 0), (0, 1), (0, -1))
        for idx in numpy.ndindex(*arr.shape):
            if not arr[idx] or rectid[idx]:
                continue
            y0, x0 = idx
            y1, x1 = y0+1, x0+1
            expanded = True
            while expanded:
                ny0 = max(y0-1, 0)
                ny1 = min(y1+1, h-1)
                nx0 = max(x0-1, 0)
                nx1 = min(x1+1, w-1)
                expanded = False
                if y0 > 0 and numpy.any(arr[y0-1,nx0:nx1]):
                    y0 -= 1
                    expanded = True
                if x0 > 0 and numpy.any(arr[ny0:ny1,x0-1]):
                    x0 -= 1
                    expanded = True
                if y1 < w-1 and numpy.any(arr[y1,nx0:nx1]):
                    y1 += 1
                    expanded = True
                if x1 < h-1 and numpy.any(arr[ny0:ny1,x1]):
                    x1 += 1
                    expanded = True
            merge = set()
            r = Rect(x0,y0,x1-x0,y1-y0)
            while True:
                rectid_slice = rectid[r.y:r.y+r.h,r.x:r.x+r.w]
                new_merge = set(rectid_slice.flat)
                new_merge.discard(0)
                if new_merge == merge:
                    break
                merge = new_merge
                r = merge_rects([r] + [rects[n] for n in merge])
            rectid_slice.fill(counter)
            rects[counter] = r
            counter += 1
            for n in merge:
                del rects[n]

        band_arr = numpy.equal(numpy.max(rectid, axis=1), 0)
        band_min = -1
        band_max = -1
        band_list = []
        for y, v in enumerate(band_arr):
            if v:
                if band_min >= 0:
                    band_list.append((band_min, band_max))
                    band_min = -1
            else:
                if band_min < 0:
                    band_min = y
                band_max = y
        if band_min >= 0:
            band_list.append((band_min, band_max))

        assert set(rectid.flat) == set(rects).union([0])

        band_baseline = []
        rect_band = {}
        order = {}
        counter = 0
        for bn, band in enumerate(band_list):
            band_rects = set(rectid[band[0]:band[1]+1].flat)
            band_rects.discard(0)
            band_rects = [(rects[n], n) for n in band_rects]
            band_rects.sort()
            for r, n in band_rects:
                order[n] = counter
                rect_band[n] = bn
                counter += 1
            bases = numpy.array([r.y + r.h for r, n in band_rects],
                                dtype=numpy.uint16)
            bases = numpy.bincount(bases)
            band_baseline.append(numpy.argmax(bases))

        assert set(order) == set(rects)

        self.rects = rects
        self.rectid = rectid
        self.unused = set(rects)
        self._order = order
        self.baselines = band_baseline
        self._rect_band = rect_band

        self.sel_rect = None
        self.sel_ids = None
        self.select_next()

    @property
    def _key(self):
        order = self._order
        return lambda x: order[x]

    @property
    def size(self):
        h, w = self.arr.shape
        return w, h

    def select_next(self):
        order = self._order
        if self.sel_ids:
            minr = min(self.sel_ids, key=self._key)
            rects = [r for r in self.unused if order[r] > order[minr]]
            if rects:
                self.select([min(rects, key=self._key)])
                return
        if self.unused:
            self.select([min(self.unused, key=self._key)])
            return
        self.select(())

    def select_prev(self):
        order = self._order
        if self.sel_ids:
            minr = min(self.sel_ids, key=self._key)
            rects = [r for r in self.unused if order[r] < order[minr]]
            if rects:
                self.select([max(rects, key=self._key)])
                return
        if self.unused:
            self.select([max(self.unused, key=self._key)])
            return
        self.select(())

    def select(self, rects):
        self.sel_ids = set(rects)
        if self.sel_ids:
            self.sel_rect = merge_rects(self.rects[r] for r in self.sel_ids)
            rect = min(self.sel_ids, key=self._key)
            self.sel_baseline = self.baselines[self._rect_band[rect]]
        else:
            self.sel_rect = None
            self.sel_baseline = None
        self._changed()

    def select_rect(self, r):
        self.sel_rect = r
        self.sel_ids = set(self.rectid[r.y:r.y+r.h,r.x:r.x+r.h].flat)
        self.sel_ids.discard(0)
        if self.sel_ids:
            rect = min(self.sel_ids, key=self._key)
            self.sel_baseline = self.baselines[self._rect_band[rect]]
        else:
            self.sel_baseline = None
        self._changed()

    def _changed(self):
        for func in self._listener:
            func()

    def add_listener(self, func):
        self._listener.append(func)

    def make_glyph(self):
        r = self.sel_rect
        if r is None:
            return
        arr = numpy.copy(self.arr[r.y:r.y+r.h,r.x:r.x+r.w])
        baseline = self.sel_baseline
        if baseline is None:
            baseline = r.h
        else:
            baseline -= r.y
        glyph = font.Glyph.glyph(arr, baseline)
        if self._glyph_add_callback(glyph):
            self.unused.difference_update(self.sel_ids)
            self.select_next()

    def adjust_baseline(self, amt):
        if self.sel_baseline is None:
            return
        rect = min(self.sel_ids, key=self._key)
        self.baselines[self._rect_band[rect]] += amt
        self.sel_baseline += amt
        self._changed()

class BaseView(object):
    __slots__ = ['glyphs', 'widget']
    def __init__(self, glyphs, size):
        self.glyphs = glyphs
        self.widget = gtk.DrawingArea()
        self.widget.set_size_request(*size)
        self.widget.connect("expose-event", self.expose)
        self.glyphs.add_listener(self.widget.queue_draw)
        if hasattr(self, 'key_press'):
            self.widget.set_can_focus(True)
            self.widget.connect('key-press-event', self.key_press)
        if hasattr(self, 'button_press'):
            self.widget.add_events(gdk.BUTTON_PRESS_MASK)
            self.widget.connect('button-press-event', self.button_press)
    def expose(self, area, event):
        cr = self.widget.window.cairo_create()
        cr.rectangle(event.area.x, event.area.y,
                     event.area.width, event.area.height)
        cr.clip()
        self.redraw(cr)

ARROWS = [gtk.keysyms.Left,
          gtk.keysyms.Up,
          gtk.keysyms.Right,
          gtk.keysyms.Down]

class GlyphsView(BaseView):
    __slots__ = []
    def key_press(self, widget, event):
        if event.keyval == gtk.keysyms.Tab:
            self.glyphs.select_next()
            return True
        if event.keyval == gtk.keysyms.ISO_Left_Tab:
            self.glyphs.select_prev()
            return True
        if self.glyphs.sel_rect is not None:
            try:
                d = ARROWS.index(event.keyval)
            except ValueError:
                pass
            else:
                if event.state & gdk.CONTROL_MASK:
                    if d == 1:
                        amt = -1
                    elif d == 3:
                        amt = +1
                    else:
                        return False
                    self.glyphs.adjust_baseline(amt)
                    return True
                r = self.glyphs.sel_rect
                c = [r.x, r.y, r.x + r.w, r.y + r.h]
                shrink = bool(event.state & gdk.SHIFT_MASK)
                delta = +1 if bool(d >= 2) else -1
                if shrink:
                    d ^= 2
                c[d] += delta
                nr = Rect(c[0], c[1], c[2] - c[0], c[3] - c[1])
                w, h = self.glyphs.size
                if (nr.w > 0 and nr.h > 0 and
                    nr.x >= 0 and nr.y >= 0 and
                    nr.x + nr.w <= w and nr.y + nr.h <= h):
                    self.glyphs.select_rect(nr)
                return True
    def button_press(self, widget, event):
        self.widget.grab_focus()
        return False

class WideView(GlyphsView):
    """Big picture view of the image."""
    __slots__ = ['zoom']
    def __init__(self, glyphs, zoom):
        w, h = glyphs.size
        super(WideView, self).__init__(glyphs, (w * zoom, h * zoom))
        self.zoom = zoom

    def redraw(self, cr):
        zoom = self.zoom
        w, h = self.glyphs.size

        cr.save()
        cr.scale(self.zoom, self.zoom)
        cr.set_source_surface(self.glyphs.img)
        cr.get_source().set_filter(cairo.FILTER_NEAREST)
        cr.paint()
        cr.restore()

        def rect(r):
            cr.rectangle(r.x * zoom + 0.5, r.y * zoom + 0.5,
                         r.w * zoom - 1, r.h * zoom - 1)

        cr.save()
        cr.set_source_rgba(0.0, 0.0, 1.0, 0.5)
        cr.set_line_width(1.0)
        for y in self.glyphs.baselines:
            cr.move_to(0, y * zoom - 0.5)
            cr.rel_line_to(w * zoom, 0);
        cr.stroke()
        cr.restore()

        cr.save()
        cr.set_source_rgba(0.5, 0.5, 0.5, 0.5)
        cr.set_line_width(1.0)
        for r in self.glyphs.unused:
            rect(self.glyphs.rects[r])
        cr.stroke()
        cr.restore()

        if self.glyphs.sel_rect is not None:
            cr.save()
            cr.set_line_width(3.0)
            cr.set_source_rgba(1.0, 0.0, 0.0, 0.5)
            rect(self.glyphs.sel_rect)
            cr.stroke()
            cr.restore()

    def button_press(self, widget, event):
        if super(WideView, self).button_press(widget, event):
            return True
        x = math.floor(event.x / self.zoom)
        y = math.floor(event.y / self.zoom)
        w, h = self.glyphs.size
        if 0 <= x < w and 0 <= y < h:
            r = self.glyphs.rectid[y,x]
            rs = (r,) if r else ()
        else:
            rs = ()
        if event.state & gdk.SHIFT_MASK:
            rs = set(self.glyphs.sel_ids).symmetric_difference(rs)
        self.glyphs.select(rs)
        return True

class CharView(GlyphsView):
    """Picture of the current glyph."""
    __slots__ = ['zoom', 'size', 'zoom', 'pat']
    def __init__(self, glyphs, size, zoom):
        w, h = size
        super(CharView, self).__init__(glyphs, (w * zoom, h * zoom))
        self.zoom = zoom
        self.size = size

    def redraw(self, cr):
        zoom = self.zoom
        w, h = self.size
        r = self.glyphs.sel_rect
        if r is None:
            return

        cr.save()
        cr.set_source_rgb(0.9, 0.9, 0.9)
        cr.paint()
        cr.restore()

        x = (w - r.w) // 2
        y = (h - r.h) // 2

        cr.save()
        cr.scale(zoom, zoom)
        cr.translate(x, y)
        cr.rectangle(0, 0, r.w, r.h)
        cr.clip()
        cr.set_source_surface(self.glyphs.img, -r.x, -r.y)
        cr.get_source().set_filter(cairo.FILTER_NEAREST)
        cr.paint()
        cr.restore()

        b = self.glyphs.sel_baseline
        if b is not None:
            b += y - r.y
            cr.save()
            cr.set_source_rgba(0.0, 0.0, 1.0, 0.5)
            cr.set_line_width(4.0)
            cr.move_to(0, b * zoom)
            cr.rel_line_to(w * zoom, 0)
            cr.stroke()
            cr.restore()

class Main(object):
    __slots__ = ['glyphs', 'font', 'window', 'wideview', 'charview',
                 'fileview', 'entry_codepoint', 'entry_char',
                 'cur_codepoint']

    def __init__(self, image, fontfile):
        self.glyphs = Glyphs(image, self.add_glyph)
        self.font = font.GlyphsetFile(fontfile)

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("destroy", self.destroy)

        self.wideview = WideView(self.glyphs, 4)
        self.charview = CharView(self.glyphs, (32, 32), 16)
        self.fileview = fontgtk.FontFileView(self.font)

        box = gtk.HBox()
        box.pack_start(self.wideview.widget)
        box2 = gtk.VBox()
        box2.pack_start(self.charview.widget)
        box3 = gtk.HBox()
        box3.pack_start(gtk.Label('Char:'))
        self.entry_char = gtk.Entry()
        self.entry_char.connect('insert-text', self.entry_char_insert)
        self.entry_char.connect('delete-text', self.entry_char_delete)
        box3.pack_start(self.entry_char)
        box3.pack_start(gtk.Label('Codepoint:'))
        self.entry_codepoint = gtk.Entry()
        box3.pack_start(self.entry_codepoint)
        butn = gtk.Button(stock=gtk.STOCK_ADD)
        butn.connect('clicked', lambda butn: self.glyphs.make_glyph())
        box3.pack_start(butn)
        box2.pack_start(box3)
        box.pack_start(box2)

        self.set_char('A')

        box2 = gtk.VBox()
        box2.pack_start(box, expand=False)
        box2.pack_start(self.fileview.widget)

        self.window.add(box2)
        self.window.set_focus(self.wideview.widget)
        self.window.show_all()

    def set_char(self, char):
        if not isinstance(char, unicode):
            char = char.decode('UTF-8')
        codepoint = ord(char)
        char = unichr(codepoint)
        self.entry_char.get_buffer().set_text(char, len(char))
        self.entry_codepoint.set_text('U+%04X' % codepoint)
        self.cur_codepoint = codepoint

    def set_char_next(self):
        codepoint = self.cur_codepoint + 1
        if codepoint >= 127 or codepoint < 33:
            codepoint = 33
        self.set_char(unichr(codepoint))

    def entry_char_insert(self, entry, string, stringlen, pos):
        entry.stop_emission('insert-text')
        self.set_char(string)

    def entry_char_delete(self, entry, start, end):
        entry.stop_emission('delete-text')

    def add_glyph(self, glyph):
        glyphset = self.fileview.get_selected_glyphset()
        if glyphset is None:
            return False
        glyphset.add_glyph(self.cur_codepoint, glyph)
        self.set_char_next()
        return True

    def destroy(self, widget, data=None):
        gtk.main_quit()

def run(image, font):
    obj = Main(image, font)
    try:
        gtk.main()
    finally:
        try:
            obj.font.save()
        except:
            import traceback
            traceback.print_exc()
