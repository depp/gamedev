import PIL.Image
import numpy
from . import kern

def glyph_place(glyphs, kerning_table, y):
    """Calculate locations for the given glyphs."""
    x = 0
    locs = []
    last = -1
    for glyph in glyphs:
        x += kerning_table.get((last, glyph.index), 0)
        locs.append((x, y))
        x += glyph.advance
        last = glyph.index
    return locs

def glyph_bounds(glyphs, locs):
    """Get the bounds of the given glyphs.

    Returns (x0,y0,x1,y1).
    """
    first = True
    for glyph, loc in zip(glyphs, locs):
        gx0 = loc[0] + glyph.xoffset
        gy0 = loc[1] + glyph.yoffset
        gx1 = gx0 + glyph.width
        gy1 = gy0 + glyph.height
        if first:
            x0, y0, x1, y1 = gx0, gy0, gx1, gy1
            first = False
        else:
            x0 = min(x0, gx0)
            y0 = min(y0, gy0)
            x1 = max(x1, gx1)
            y1 = max(y1, gy1)
    return x0, y0, x1, y1

def glyph_draw(glyphs, locs, arr, x, y):
    h = arr.shape[0]
    for glyph, loc in zip(glyphs, locs):
        gh, gw = glyph.data.shape
        gx = glyph.xoffset + loc[0] + x
        gy = glyph.yoffset + loc[1] + y
        arr[h - gy - gh : h - gy, gx : gx + gw] |= glyph.data

def text_draw(fnt, text, margin, kerning=True, leading=None):
    if kerning:
        kerning_table = kern.kerning_table(fnt)
    else:
        kerning_table = {}
    lines = text.splitlines()
    lglyphs = [fnt.shape_text(line) for line in lines]
    ascent, descent = fnt.font_metrics()
    height = ascent + descent
    if leading is None:
        leading = height
    llocs = [glyph_place(glyphs, kerning_table, -leading * n)
             for n, glyphs in enumerate(lglyphs)]
    glyphs = [glyph for glyphs in lglyphs for glyph in glyphs]
    locs = [loc for locs in llocs for loc in locs]
    if not glyphs:
        raise ValueError('empty')
    x0, y0, x1, y1 = glyph_bounds(glyphs, locs)
    arr = numpy.zeros((y1-y0+2*margin, x1-x0+2*margin), dtype=numpy.bool)
    glyph_draw(glyphs, locs, arr, margin - x0, margin - y0)
    image = PIL.Image.fromarray(arr.astype(numpy.uint8) * 0xff, mode='L')
    return image.convert('1')
