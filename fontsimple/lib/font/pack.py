from __future__ import absolute_import
import numpy
import pack.sprite
from . import font

def pack_fonts(fonts, **kw):
    """Pack glyphs from the given fonts into a sprite sheet."""
    glyphs = []
    for fnt in fonts:
        glyphs.extend(glyph for glyph in fnt.glyphs()
                      if glyph.width and glyph.height)
    img, sprites = pack.sprite.pack([glyph.data for glyph in glyphs], **kw)
    for glyph, sprite in zip(glyphs, sprites):
        glyph.sprite = font.Sprite(sprite.x, sprite.y, sprite.orientation)
    img = img.convert('1')
    return img
