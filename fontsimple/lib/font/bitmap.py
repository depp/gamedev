"""Tools for working with 2D bitmaps.

2D bitmaps are represented using numpy boolean arrays.
"""
import binascii
import numpy

def print_ascii(arr, fp):
    """Write the binary array to the given file as ASCII art."""
    h, w = arr.shape
    for y in range(h):
        fp.write(''.join('#' if arr[y,x] else '.'
                         for x in range(w)) + '\n')

# ASCII bitmap format

def to_text(arr):
    """Pack a 2D bitmap as an ASCII string."""
    packed = numpy.packbits(arr.astype(numpy.uint8), axis=1)
    return ' '.join(binascii.b2a_hex(row) for row in packed).upper()

def from_text(data, width, height):
    """Unpack a 2D bitmap from an ASCII string."""
    rowbytes = (width + 7) // 8
    rows = data.split()
    if len(rows) != height or any(len(row) != 2*rowbytes for row in rows):
        raise ValueError('invalid bitmap data')
    data = ''.join(binascii.a2b_hex(row) for row in rows)
    arr = numpy.fromstring(data, dtype=numpy.uint8).reshape((height, rowbytes))
    return numpy.unpackbits(arr, axis=1)[...,0:width].astype(numpy.bool)

# Binary bitmap format

def to_binary(arr):
    """Pack a 2D bitmap as binary data."""
    return numpy.packbits(arr.astype(numpy.uint8), axis=1).tostring()

def from_binary(data, width, height):
    """Unpack a 2D bitmap from binary data."""
    rowbytes = (width + 7) // 8
    if len(data) != height * rowbytes:
        raise ValueError('invalid bitmap data')
    arr = numpy.fromstring(data, dtype=numpy.uint8).reshape((height, rowbytes))
    return numpy.unpackbits(arr, axis=1)[...,0:width].astype(numpy.bool)

# Cairo image format

def to_cairo(arr):
    """Convert a 2D bitmap to a Cairo surface."""
    import cairo
    height, width = arr.shape
    rowbytes = cairo.ImageSurface.format_stride_for_width(
        cairo.FORMAT_ARGB32, width)
    assert (rowbytes & 3) == 0
    narr = numpy.zeros((h, rowbytes//4), numpy.uint32)
    narr[0:height,0:width] = arr
    narr -= 1
    narr |= 0xff000000
    img = cairo.ImageSurface.create_for_data(
        bytearray(narr.data), cairo.FORMAT_ARGB32,
        width, height, rowbytes)
    return img

# PIL image format

def to_pil(arr):
    """Convert a 2D bitmap to a PIL image."""
    import PIL.Image
    return PIL.Image.fromstring('L', (arr.shape[1], arr.shape[0]),
                                arr.astype(numpy.uint8) * 255)

def from_pil(img):
    """Convert a PIL image to a 2D bitmap."""
    arr = numpy.array(img.convert("L").getdata(), dtype=numpy.uint8)
    arr = arr.reshape(img.shape[1], img.shape[1])
    return numpy.less(arr, 128)
