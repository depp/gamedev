import numpy

def kerning_table(fnt):
    """Calculate the kerning table for a font."""
    kerning = {}
    if fnt.auto_kern_kernel is None:
        return kerning
    kernel = fnt.auto_kern_kernel
    klen = len(kernel) - 1
    kernel = numpy.concatenate([kernel, kernel[::-1][1:]])
    kernel = kernel.astype(numpy.uint16)

    min_kern = -2
    max_kern = 2

    metrics = fnt.glyph_metrics()
    height = metrics.y1 - metrics.y0
    glyphs = []
    for glyph in fnt.glyphs():
        if not glyph.width or not glyph.height:
            continue
        gy = glyph.yoffset - metrics.y0
        gdata = numpy.zeros((height, glyph.width), dtype=numpy.int16)
        gdata[height - glyph.height - gy : height - gy] = glyph.data
        idx = numpy.arange(1, glyph.width+1, dtype=numpy.int16)
        left = numpy.amax(gdata * idx, axis=1)
        right = numpy.amax(gdata * idx[::-1], axis=1)
        left = numpy.concatenate([
            numpy.zeros(klen, dtype=numpy.int16),
            left,
            numpy.zeros(klen, dtype=numpy.int16),
            ])
        rarr = numpy.zeros((1 + klen * 2, height + klen * 2),
                           dtype=numpy.int16)
        for i in xrange(1 + klen * 2):
            rarr[i,i:i+height] = right + kernel[i]
        right = numpy.amax(rarr, axis=0)
        glyphs.append((glyph, left, right))

    for glyph1, left1, right1 in glyphs:
        for glyph2, left2, right2 in glyphs:
            space = (glyph1.advance - glyph1.xoffset +
                     glyph2.xoffset + glyph2.width)
            kspace = numpy.amax(left1 + right2)
            kern = kspace - space
            kern = min(max(kern, min_kern), max_kern)
            kern = max(kern, 1 - glyph1.advance)
            if kern:
                kerning[(glyph1.index, glyph2.index)] = kern

    return kerning
