from __future__ import absolute_import
import gtk
import cairo
import pango
import pangocairo

class FontListView(object):
    __slots__ = ['font', 'store', 'widget', 'order']
    def __init__(self, font):
        self.font = font
        self.store = gtk.ListStore(str, int)
        self.widget = gtk.TreeView(self.store)

        cell = gtk.CellRendererText()

        font.add_listener(self.glyphset_changed)
        self.widget.insert_column_with_attributes(
            0, 'Name', cell, text=0)
        self.widget.insert_column_with_attributes(
            1, 'Size', cell, text=1)

        self.glyphset_changed(None)

    def glyphset_changed(self, glyphset):
        if glyphset is None:
            order = {}
            self.store.clear()
            for n, name in enumerate(sorted(self.font.glyphsets)):
                gs = self.font.glyphsets[name]
                self.store.insert(n, [gs.name, len(gs.glyphs)])
                order[name] = n
            self.order = order
        else:
            pos = self.order[glyphset]
            gs = self.font.glyphsets[glyphset]
            pos = self.store.get_iter(str(pos))
            self.store.set_value(pos, 1, len(gs.glyphs))

def error(parent, message):
    dialog = gtk.MessageDialog(
        parent,
        gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
        gtk.MESSAGE_ERROR,
        gtk.BUTTONS_OK,
        message
    )
    dialog.show()
    dialog.run()
    dialog.destroy()

def verify(parent, message):
    dialog = gtk.MessageDialog(
        parent,
        gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
        gtk.MESSAGE_QUESTION,
        gtk.BUTTONS_YES_NO,
        message
    )
    dialog.show()
    response = dialog.run()
    dialog.destroy()
    return response == gtk.RESPONSE_YES

import string
CHAR_SETS = [
    ('Glyphs', None),
    ('Upper', string.ascii_uppercase),
    ('Lower', string.ascii_lowercase),
    ('Letter', string.ascii_letters),
    ('Digit', string.digits),
    ('Alphanumeric', string.ascii_letters + string.digits),
    ('Punctuation', 
     ''.join(sorted(set(chr(c) for c in range(33, 127))
                    .difference(string.ascii_letters + string.digits)))),
    ('ASCII', ''.join(chr(c) for c in range(32, 127))),
]
CHAR_SET_ORDER = [x[0] for x in CHAR_SETS]
CHAR_SETS = dict(CHAR_SETS)
del x

class CharSetSelector(object):
    __slots__ = ['widget', 'store', 'cset', '_listener']
    def __init__(self):
        self.store = gtk.ListStore(str)
        for name in CHAR_SET_ORDER:
            self.store.append((name,))
        self.widget = gtk.ComboBox(self.store)
        renderer = gtk.CellRendererText()
        self.widget.pack_start(renderer, True)
        self.widget.add_attribute(renderer, 'text', 0)
        self.widget.set_active_iter(self.store.get_iter_from_string('0'))
        self.widget.connect(
            'changed', self.selection_changed)
        self.cset = None
        self._listener = []

    def selection_changed(self, selection):
        idx = self.widget.get_active()
        if idx < 0:
            self.cset = None
        else:
            value = CHAR_SET_ORDER[idx]
            self.cset = CHAR_SETS[value]
        self._changed()

    def _changed(self):
        for func in self._listener:
            func()

    def add_listener(self, func):
        self._listener.append(func)

class GlyphSetView(object):
    __slots__ = ['font', 'glyphset', 'drawarea',
                 'widget', 'gsize', 'csetsel']

    def __init__(self, font, gsize):
        self.font = font
        self.glyphset = None
        self.drawarea = gtk.DrawingArea()
        self.drawarea.set_size_request(512, 48*3)
        self.drawarea.connect('expose-event', self.expose)
        font.add_listener(self.glyphset_changed)
        self.gsize = gsize
        self.csetsel = CharSetSelector()
        self.widget = gtk.VBox()
        self.widget.pack_start(self.csetsel.widget, False)
        self.widget.pack_start(self.drawarea)
        self.csetsel.add_listener(self.update)

    def set_selection(self, glyphset):
        self.glyphset = glyphset
        self.update()

    def glyphset_changed(self, glyphset):
        if glyphset is not None and glyphset != self.glyphset:
            return
        if self.glyphset not in self.font.glyphsets:
            self.glyphset = None
        self.update()

    def update(self):
        self.drawarea.queue_draw()

    def expose(self, area, event):
        cr = self.drawarea.window.cairo_create()
        cr.rectangle(event.area.x, event.area.y,
                     event.area.width, event.area.height)
        cr.clip()
        self.redraw(cr)

    def redraw(self, cr):
        cr.save()
        cr.set_source_rgb(0.7, 0.7, 0.7)
        cr.paint()
        cr.restore()

        if self.glyphset is None:
            return
        gw, gh = self.gsize
        nw = 512 // gw
        gs = self.font.glyphsets[self.glyphset]

        ascent = 0
        descent = 0
        for g in gs.glyphs.values():
            ascent = max(ascent, g.baseline)
            descent = max(descent, g.size[1] - g.baseline)
        # print 'ascent: %d, descent: %d' % (ascent, descent)
        labelheight = 12
        baseline = ((gh - ascent - descent - labelheight) // 2 +
                    ascent + labelheight)

        pcxt = pangocairo.CairoContext(cr)
        pl = pcxt.create_layout()
        pl.set_font_description(pango.FontDescription('Sans 8'))

        cset = self.csetsel.cset
        if cset is not None:
            cset = set(ord(c) for c in self.csetsel.cset)
            allchars = list(cset.union(gs.glyphs))
        else:
            cset = ()
            allchars = list(gs.glyphs)
        allchars.sort()

        for n, codepoint in enumerate(allchars):
            g = gs.glyphs.get(codepoint, None)
            ny, nx = divmod(n, nw)
            missing = g is None and codepoint != 32

            cr.save()
            cr.translate(nx * gw, ny * gh)

            cr.save()
            cr.rectangle(1, 1, gw-2, gh-2)
            if missing:
                cr.set_source_rgb(1.0, 0.5, 0.5)
            else:
                cr.set_source_rgb(1.0, 1.0, 1.0)
            cr.fill()
            cr.restore()

            cr.save()
            if missing:
                cr.set_source_rgb(0.3, 0.3, 0.3)
            else:
                cr.set_source_rgb(0.6, 0.6, 0.6)
            cr.translate(2, 2)
            pl.set_text(unichr(codepoint))
            pcxt.update_layout(pl)
            pcxt.show_layout(pl)
            cr.restore()

            if g is not None:
                w, h = g.size
                x = (gw - w) // 2
                y = baseline - g.baseline

                cr.save()
                cr.translate(x, y)
                cr.set_source_surface(g.image)
                cr.rectangle(0, 0, w, h)
                cr.clip()
                cr.paint()
                cr.restore()

                cr.save()
                cr.set_source_rgba(0.0, 0.0, 1.0, 0.125)
                cr.set_line_width(1.0)
                cr.move_to(0, baseline - 0.5)
                cr.rel_line_to(gw, 0)
                cr.stroke()
                cr.restore()
            else:
                pass

            cr.restore()

class FontFileView(object):
    __slots__ = ['font', 'widget', 'listview', 'glyphsetview']
    def error(self, msg):
        error(self.widget.get_toplevel(), msg)

    def verify(self, msg):
        verify(self.widget.get_toplevel(), msg)

    def __init__(self, font):
        self.font = font

        box1 = gtk.HBox()

        box = gtk.VBox()
        box1.pack_start(box)
        self.listview = FontListView(font)
        box.pack_start(self.listview.widget)

        box2 = gtk.HBox()
        box.pack_start(box2, expand=False)
        butn = gtk.Button(stock=gtk.STOCK_ADD)
        butn.connect('clicked', self.add_glyphset)
        box2.pack_start(butn)
        butn = gtk.Button(stock=gtk.STOCK_DELETE)
        butn.connect('clicked', self.delete_glyphset)
        box2.pack_start(butn)

        self.listview.widget.get_selection().connect(
            'changed', self.selected_glyphset_changed)

        self.glyphsetview = GlyphSetView(font, (32, 48))
        box1.pack_start(self.glyphsetview.widget)

        self.widget = box1

    def add_glyphset(self, widget):
        dialog = gtk.Dialog(
            'Add Glyphset', self.widget.get_toplevel(),
            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
            (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
             gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        box = gtk.HBox()
        box.pack_start(gtk.Label('Add glyphset:'))
        entry = gtk.Entry()
        entry.connect("activate", 
                      lambda ent: dialog.response(gtk.RESPONSE_ACCEPT))
        box.pack_start(entry)
        dialog.vbox.pack_start(box)
        box.show_all()
        response = dialog.run()
        text = entry.get_text()
        dialog.destroy()
        if response == gtk.RESPONSE_ACCEPT:
            if not text:
                self.error("Name cannot be empty.")
            elif text in self.font.glyphsets:
                self.error("Glyphset already exists.")
            else:
                self.font.add_glyphset(text)

    def selected_glyphset_changed(self, selection):
        gs = self.get_selected_glyphset()
        self.glyphsetview.set_selection(gs.name if gs is not None else None)

    def get_selected_glyphset(self):
        smodel, siter = self.listview.widget.get_selection().get_selected()
        if siter is None:
            return None
        value, = self.listview.store.get(siter, 0)
        return self.font.glyphsets[value]

    def delete_glyphset(self, widget):
        if self.verify('Delete glyphset?'):
            print 'DELETE'
