from cStringIO import StringIO
import struct
import os
from . import bitmap
from . import kern
from . import font

MAGIC = 'SGLib Font 1.0\0\0'
assert len(MAGIC) == 16

class Tags(object):
    __slots__ = ['fp']
    def __init__(self):
        self.fp = StringIO()
    def write(self, name, data):
        assert len(name) == 4
        self.fp.write(name)
        self.fp.write(struct.pack('>I', len(data)))
        self.fp.write(data)
    def getvalue(self):
        return self.fp.getvalue() + '\0\0\0\0'

_PROPERTIES = 'foundry', 'family', 'weight', 'slant', 'width', 'style'
def _dump_props(fnt, glyphs, tags, path):
    if not any(fnt.properties.get(prop) for prop in _PROPERTIES):
        return
    fp = StringIO()
    for prop in _PROPERTIES:
        s = fnt.properties.get(prop, '')
        if '\0' in s:
            raise ValueError('NUL byte in property')
        fp.write(s + '\0')
    tags.write('PROP', fp.getvalue())

def _dump_fontmetrics(fnt, glyphs, tags, path):
    ascent, descent = fnt.font_metrics()
    tags.write('FMET', struct.pack('>hh', ascent, descent))

def _dump_encoding(fnt, glyphs, tags, path):
    fp = StringIO()
    for glyph in glyphs:
        for e in sorted(glyph.encoding):
            fp.write(e.encode('UTF-8') + '\xff')
        fp.write('\xff')
    tags.write('CHA1', fp.getvalue())

def _dump_metrics(fnt, glyphs, tags, path):
    fp = StringIO()
    for glyph in glyphs:
        fp.write(struct.pack(
            'BBBbb', glyph.advance, glyph.width, glyph.height,
            glyph.xoffset, glyph.yoffset))
    tags.write('MET1', fp.getvalue())

def _dump_bitmap(fnt, glyphs, tags, path):
    fp = StringIO()
    for glyph in glyphs:
        fp.write(bitmap.to_binary(glyph.data))
    tags.write('BIT1', fp.getvalue())

def _dump_sprites(fnt, glyphs, tags, path):
    if all(glyph.sprite is None for glyph in glyphs):
        return
    fp = StringIO()
    for glyph in glyphs:
        sprite = glyph.sprite or font.Sprite(0, 0, 0)
        fp.write(struct.pack(
            '>HHB', sprite.x, sprite.y, sprite.orientation))
    tags.write('SPR1', fp.getvalue())

def _dump_sprite_file(fnt, glyphs, tags, path):
    if fnt.sprite_sheet_file is None:
        return
    rpath = os.path.relpath(
        os.path.abspath(fnt.sprite_sheet_file),
        os.path.dirname(os.path.abspath(path)))
    tags.write('SPRF', rpath)

def _dump_kerning(fnt, glyphs, tags, path):
    kerning = kern.kerning_table(fnt)
    if not kerning:
        return
    index_map = {glyph.index: n for n, glyph in enumerate(glyphs)}
    kerning2 = {}
    for (left, right), value in kerning.iteritems():
        left = index_map[left]
        right = index_map[right]
        try:
            subtable = kerning2[left]
        except KeyError:
            subtable = {}
            kerning2[left] = subtable
        subtable[right] = value
    fp = StringIO()
    for left, table in sorted(kerning2.iteritems()):
        table = list(table.items())
        table.sort()
        fp.write(struct.pack('>HH', left, len(table)))
        for right, value in table:
            fp.write(struct.pack('>Hb', right, value))
    tags.write('KER1', fp.getvalue())

def dump_font(path, fnt, no_bitmaps=False):
    glyphs = fnt.sorted_glyphs()
    tags = Tags()

    funcs = [
        _dump_props,
        _dump_fontmetrics,
        _dump_encoding,
        _dump_metrics,
        _dump_sprites,
        _dump_sprite_file,
        _dump_kerning,
    ]
    if not no_bitmaps:
        funcs.append(_dump_bitmap)
    for func in funcs:
        func(fnt, glyphs, tags, path)

    data = tags.getvalue()
    with open(path, 'wb') as fp:
        fp.write(MAGIC)
        fp.write(data)
