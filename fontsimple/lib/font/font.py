import numpy
import collections
import os
import importlib

Sprite = collections.namedtuple('Sprite', 'x y orientation')
Metrics = collections.namedtuple('Metrics', 'x0 y0 x1 y1 advance')
Property = collections.namedtuple('Property', 'name type bdfname')

PROPERTIES = [
    Property('foundry', str, 'FOUNDRY'),
    Property('family',  str, 'FAMILY_NAME'),
    Property('weight',  str, 'WEIGHT_NAME'),
    Property('slant',   str, 'SLANT'),
    Property('width',   str, 'SETWIDTH_NAME'),
    Property('style',   str, 'ADD_STYLE_NAME'),

    Property('ascent',  int, 'FONT_ASCENT'),
    Property('descent', int, 'FONT_DESCENT'),
]

class Glyph(object):
    __slots__ = ['data', 'xoffset', 'yoffset', 'advance',
                 '_index', '_encoding', 'sprite']

    def __init__(self):
        self.data = numpy.zeros((0, 0), dtype=bool)
        self.xoffset = 0
        self.yoffset = 0
        self.advance = 0
        self._index = None
        self._encoding = set()
        self.sprite = None

    @property
    def width(self):
        """The glyph width."""
        return self.data.shape[1]

    @property
    def height(self):
        """The glyph height."""
        return self.data.shape[0]

    @property
    def encoding(self):
        """Set of encodings for this glyph."""
        return frozenset(self._encoding)

    @property
    def index(self):
        """Glyph index for this glyph."""
        return self._index

    def copy(self):
        g = Glyph()
        g.data = numpy.copy(self.data)
        g.xoffset = self.xoffset
        g.yoffset = self.yoffset
        g.advance = self.advance
        g._encoding = set(self._encoding)
        return g

    def clear(self):
        self.data = numpy.zeros((0, 0), dtype=bool)
        self.xoffset = 0
        self.yoffset = 0

    def trim(self):
        data = self.data
        if not numpy.any(data):
            self.clear()
            return
        w = numpy.argwhere(data)
        y0, x0 = numpy.min(w, 0)
        y1, x1 = numpy.max(w, 0) + numpy.array([1, 1])
        self.data = data[y0:y1,x0:x1]
        self.xoffset += x0
        self.yoffset += data.shape[0] - y1

    def __repr__(self):
        return '<Glyph {}>'.format(self._index)

class Font(object):
    __slots__ = [
        'properties',
        '_glyph', '_glyph_counter', '_encoding',
        'auto_kern_kernel',
        'sprite_sheet_file',
        '_listener',
    ]

    def __init__(self):
        self.properties = {}

        # Map from glyph index to glyph
        self._glyph = {}
        # Next glyph index
        self._glyph_counter = 0
        # Map from encoding to glyph
        self._encoding = {}

        self.auto_kern_kernel = None
        self.sprite_sheet_file = None
        self._listener = []

    def _add_glyph(self, glyph):
        """Add a glyph without copying it."""
        glyph._index = self._glyph_counter
        self._glyph_counter += 1
        self._glyph[glyph._index] = glyph

    def glyphs(self):
        return self._glyph.itervalues()

    def sorted_glyphs(self):
        """Get a list of glyphs, sorted canonically."""
        glyphs = list(self._glyph.itervalues())
        glyphs.sort(key=lambda glyph:
                    min(glyph.encoding) if glyph.encoding else '')
        return glyphs

    def new_glyph(self):
        """Create a new glyph."""
        glyph = Glyph()
        self._add_glyph(glyph)
        return glyph

    def add_glyph(self, glyph):
        """Add a glyph, copying it and returning the new object.

        The glyph's encoding will be cleared.
        """
        glyph = glyph.copy()
        glyph._encoding = set()
        self._add_glyph(glyph)
        return glyph

    def remove_glyph(self, glyph):
        """Remove a glyph."""
        self._check_glyph(glyph)
        del self._glyph[glyph._index]
        for encoding in glyph._encoding:
            del self._encoding[encoding]

    def add_encoding(self, glyph, encoding):
        """Add an encoding to a glyph."""
        self._check_glyph(glyph)
        try:
            oglyph = self._encoding[encoding]
        except KeyError:
            pass
        else:
            oglyph._encoding.discard(encoding)
        self._encoding[encoding] = glyph
        glyph._encoding.add(encoding)

    def remove_encoding(self, glyph, encoding):
        """Remove an encoding from a glyph by glyph index."""
        self._check_glyph(glyph)
        if encoding not in glyph._encoding:
            raise ValueError('invalid encoding')
        glyph._encoding.discard(encoding)
        del self._encoding[encoding]

    def shape_text(self, text):
        """Convert a string to a list of glyphs."""
        maxlen = max(len(e) for e in self._encoding)
        pos = 0
        glyphs = []
        fallback = self._encoding.get(u'\ufffd')
        while pos < len(text):
            glyph = fallback
            glen = 1
            for clen in xrange(1, min(maxlen+1, len(text) + 1 - pos)):
                try:
                    glyph = self._encoding[text[pos:pos+clen]]
                    glen = clen
                except KeyError:
                    continue
            pos += glen
            if glyph is not None:
                glyphs.append(glyph)
            else:
                print 'No glyph for: {!r}'.format(text[pos-1])
        return glyphs

    def _check_glyph(self, glyph):
        if self._glyph.get(glyph._index, None) is not glyph:
            raise ValueError('invalid glyph')

    def add_listener(self, func):
        self._listener.append(func)

    def remove_listener(self, func):
        del self._listener[self._listener.index(func)]

    def _changed(self):
        for func in self._listener:
            func(self)

    def __repr__(self):
        try:
            name = repr(self.properties['family'])
        except KeyError:
            name = hex(id(self))
        return '<Font {}>'.format(name)

    def glyph_metrics(self):
        """Get the computed metrics for this font.

        The rectangle (x0,y0,x1,y1) is the smallest bounding box
        containing all glyphs, and the advance is the largest advance
        of any glyph.
        """
        if not self._glyph:
            return Metrics(0, 0, 0, 0, 0)
        advance = max(glyph.advance for glyph in self._glyph.itervalues())
        bounds = numpy.empty((len(self._glyph), 4), dtype=numpy.int)
        n = 0
        for glyph in self._glyph.itervalues():
            if not (glyph.width and glyph.height):
                continue
            bounds[n] = (
                glyph.xoffset,
                glyph.yoffset,
                glyph.xoffset + glyph.width,
                glyph.yoffset + glyph.height,
            )
            n += 1
        if n:
            x0, y0 = numpy.amin(bounds[:n,:2], 0)
            x1, y1 = numpy.amax(bounds[:n,2:], 0)
        else:
            x0, y0, x1, y1 = [0] * 4
        return Metrics(x0, y0, x1, y1, advance)

    def font_metrics(self):
        """Get the ascent and descent for this font."""
        metrics = self.glyph_metrics()
        ascent = self.properties.get('ascent', metrics.y1)
        descent = self.properties.get('descent', -metrics.y0)
        return ascent, descent

    def trim(self):
        for glyph in self._glyph.itervalues():
            glyph.trim()

_EXTS = {'.xml': 'xml', '.bdf': 'bdf', '.pcf': 'pcf', '.sgfont': 'sgfont'}
def load_font(path):
    ext = os.path.splitext(path)[1]
    try:
        module = _EXTS[ext]
    except KeyError:
        raise ValueError('unknown extension')
    module = importlib.import_module('{}.{}'.format(__package__, module))
    return module.load_font(path)
