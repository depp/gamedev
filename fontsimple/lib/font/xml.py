from __future__ import absolute_import
try:
    import lxml.etree as etree
except ImportError:
    import xml.etree.ElementTree as etree
import os
import numpy
from . import font, bitmap
__all__ = ['load_font']

_FONT_ELEM = {}

def location(elem):
    return getattr(elem, 'sourceline', None)

class XmlFontError(ValueError):
    def __init__(self, msg, loc=None, path=None):
        self.msg = msg
        self.path = path
        self.loc = loc
    def __str__(self):
        loc = [str(x) for x in [self.path, self.loc] if x]
        if loc:
            return '{}: {}'.format(':'.join(loc), self.msg)
        return self.msg

def check_attrib(info, elem):
    for a in elem.attrib:
        if a not in info:
            raise XmlFontError('unknown attribute: {!r}'.format(a),
                               location(elem))
    for a, required in info.iteritems():
        if required and not a in elem.attrib:
            raise XmlFontError('missing attribute: {!r}'.format(a),
                               location(elem))

def parse_encoding(elem):
    try:
        for e in elem.attrib['encoding'].split():
            yield u''.join(unichr(int(c)) for c in e.split(','))
    except ValueError:
        raise XmlFontError('invalid encoding')

_GLYPH_ATTRIB = {'metrics': True, 'data': True, 'encoding': False,
                 'sprite': False}
def parse_glyph(fnt, elem, cache, path):
    glyph = fnt.new_glyph()
    check_attrib(_GLYPH_ATTRIB, elem)
    try:
        metrics = [int(x) for x in elem.attrib['metrics'].split()]
        glyph.advance, width, height, glyph.xoffset, glyph.yoffset = metrics
        if width < 0 or height < 0 or glyph.advance < 0:
            raise ValueError()
    except ValueError:
        raise XmlFontError('invalid metrics')
    if 'sprite' in elem.attrib:
        try:
            x, y, orientation = [int(x) for x in
                                 elem.attrib['sprite'].split()]
            if x < 0 or y < 0 or not (0 <= orientation <= 7):
                raise ValueError()
            glyph.sprite = font.Sprite(x, y, orientation)
        except ValueError:
            raise XmlFontError('invalid sprite data')
    try:
        glyph.data = bitmap.from_text(elem.attrib['data'], width, height)
    except ValueError:
        raise XmlFontError('invalid bitmap data')
    for e in parse_encoding(elem):
        fnt.add_encoding(glyph, e)

_SPACE_ATTRIB = {'advance': True, 'encoding': False}
def parse_space(fnt, elem, cache, path):
    glyph = fnt.new_glyph()
    check_attrib(_SPACE_ATTRIB, elem)
    try:
        glyph.advance = int(elem.attrib['advance'])
        if glyph.advance < 0:
            raise ValueError()
    except ValueError:
        raise XmlFontError('invalid advance')
    for e in parse_encoding(elem):
        fnt.add_encoding(glyph, e)

_AUTO_KERNING_ATTRIB = {'kernel': True}
def parse_auto_kerning(fnt, elem, cache, path):
    check_attrib(_AUTO_KERNING_ATTRIB, elem)
    kernel = numpy.array([int(x) for x in elem.attrib['kernel'].split()],
                         dtype=numpy.int)
    fnt.auto_kern_kernel = kernel

_SPRITE_SHEET_FILE_ATTRIB = {'ref': True}
def parse_sprite_sheet_file(fnt, elem, cache, path):
    check_attrib(_SPRITE_SHEET_FILE_ATTRIB, elem)
    if fnt.sprite_sheet_file is not None:
        raise XmlFontError('multiple sprite sheet files')
    fnt.sprite_sheet_file = os.path.abspath(os.path.join(
        os.path.dirname(os.path.abspath(path)),
        elem.attrib['ref']))

_INCLUDE_ATTRIB = {'ref': True}
def parse_include(fnt, elem, cache, path):
    check_attrib(_INCLUDE_ATTRIB, elem)
    ipath = os.path.join(os.path.dirname(path), elem.attrib['ref'])
    ipath = os.path.normpath(ipath)
    try:
        ifnt = cache[ipath]
    except KeyError:
        if ipath.endswith('.xml'):
            ifnt = load_font(ipath, cache)
        else:
            ifnt = font.load_font(ipath)
            cache[ipath] = ifnt
    else:
        if ifnt is None:
            raise XmlFontError('font recursively includes itself')
    for iglyph in ifnt.glyphs():
        glyph = fnt.add_glyph(iglyph)
        for encoding in iglyph.encoding:
            fnt.add_encoding(glyph, encoding)

def parse_properties(fnt, elem, cache, path):
    check_attrib({}, elem)
    propdict = {prop.name: prop for prop in font.PROPERTIES}
    for pelem in elem:
        if pelem.tag == etree.Comment:
            continue
        try:
            prop = propdict[pelem.tag]
        except KeyError:
            raise XmlFontError('unknown property: {!r}'.format(pelem.tag),
                               location(pelem))
        if prop.name in fnt.properties:
            raise XmlFontError('duplicate property',
                               location(pelem))
        text = pelem.text or ''
        text = text.strip()
        try:
            data = prop.type(text)
        except ValueError:
            raise XmlFontError('invalid property value',
                               location(pelem))
        fnt.properties[prop.name] = data

_FONT_ELEM = {
    'glyph': parse_glyph,
    'space': parse_space,
    'auto-kerning': parse_auto_kerning,
    'sprite-sheet-file': parse_sprite_sheet_file,
    'include': parse_include,
    'properties': parse_properties,
}

def load_font(path, cache=None):
    if cache is None:
        cache = {}
    cache[path] = None
    try:
        with open(path, 'rb') as fp:
            try:
                doc = etree.parse(fp)
            except SyntaxError as ex:
                raise XmlFontError(ex.msg)

        fnt = font.Font()
        root = doc.getroot()

        if root.tag != 'font':
            raise XmlFontError('expected <font> tag at root',
                               location(root))
        for elem in root:
            if elem.tag == etree.Comment:
                continue
            try:
                func = _FONT_ELEM[elem.tag]
            except KeyError:
                raise XmlFontError('unexpected tag: {!r}'.format(elem.tag),
                                   location(elem))
            func(fnt, elem, cache, path)
    except XmlFontError as ex:
        if ex.path is None:
            ex.path = path
        raise

    cache[path] = fnt
    return fnt

def make_pretty(root, indent=0):
    if not len(root):
        return
    last = None
    istr = '\n' + '  ' * (indent + 1)
    for node in root:
        if last is None:
            root.text = istr
        else:
            last.tail = istr
        make_pretty(node, indent + 1)
        last = node
    if last is not None:
        last.tail = '\n' + '  ' * indent

def dump_font(path, fnt):
    root = etree.Element('font')

    if fnt.properties:
        elem = etree.SubElement(root, 'properties')
        proporder = {prop.name: n for n, prop in enumerate(font.PROPERTIES)}
        props = list(fnt.properties.iteritems())
        props.sort(key=lambda x: proporder[x[0]])
        for tag, data in props:
            etree.SubElement(elem, tag).text = str(data)

    if fnt.auto_kern_kernel is not None:
        etree.SubElement(
            root, 'auto-kerning',
            {'kernel': ' '.join(str(x) for x in fnt.auto_kern_kernel)})

    if fnt.sprite_sheet_file is not None:
        img_path = os.path.relpath(os.path.abspath(fnt.sprite_sheet_file),
                                   os.path.dirname(os.path.abspath(path)))
        etree.SubElement(root, 'sprite-sheet-file', {'ref': img_path})

    for glyph in fnt.sorted_glyphs():
        attrib = {}
        if glyph.encoding:
            attrib['encoding'] = ' '.join(
                ','.join(str(ord(c)) for c in s)
                for s in sorted(glyph.encoding))
        if glyph.width and glyph.height:
            attrib['metrics'] = '{} {} {} {} {}'.format(
                glyph.advance, glyph.width, glyph.height,
                glyph.xoffset, glyph.yoffset)
            attrib['data'] = bitmap.to_text(glyph.data)
            if glyph.sprite is not None:
                attrib['sprite'] = '{} {} {}'.format(*glyph.sprite)
            etree.SubElement(root, 'glyph', attrib=attrib)
        else:
            attrib['advance'] = str(glyph.advance)
            etree.SubElement(root, 'space', attrib=attrib)

    make_pretty(root)
    root.tail = '\n'
    tree = etree.ElementTree(root)
    with open(path, 'wb') as fp:
        tree.write(fp, encoding='UTF-8')
