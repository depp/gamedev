import argparse
import os

def nonneg(x):
    x = int(x)
    if x < 0:
        raise ValueError()
    return x

_COMMANDS = []

def command(x):
    _COMMANDS.append(x)
    return x

@command
class pack(object):
    help = 'pack fonts into a sprite sheet'

    @staticmethod
    def make_parser(parser):
        add_argument = parser.add_argument
        add_argument('font', nargs='+',
                     help='fonts (these will be updated)')
        add_argument('-o', '--output', '--out', dest='output', required=True,
                     help='output image files')
        add_argument('--margin', dest='margin', type=nonneg,
                     default=1,
                     help='margin between adjacent glyphs')
        add_argument('--no-pngcrush', dest='pngcrush',
                     action='store_false', default=True,
                     help='do not run pngcrush')
        add_argument('--no-reorient', dest='allow_reorient',
                     action='store_false', default=True,
                     help='do not reorient glyphs in sprite sheet')

    @staticmethod
    def run(args):
        import subprocess
        from . import pack
        from . import xml
        fonts = [(fpath, xml.load_font(fpath)) for fpath in args.font]
        image = pack.pack_fonts(
            [fnt[1] for fnt in fonts],
            margin=args.margin,
            allow_reorient=args.allow_reorient)
        tmppath = args.output + '.tmp'
        if args.pngcrush:
            image.save(tmppath, 'png')
            subprocess.check_call(
                ['pngcrush', '--', tmppath, args.output])
            os.unlink(tmppath)
        else:
            image.save(args.output, 'png', bits=1)
        imgpath = os.path.abspath(args.output)
        for fpath, fnt in fonts:
            fnt.sprite_sheet_file = imgpath
            xml.dump_font(fpath, fnt)

@command
class edit(object):
    help = 'edit a font'

    @staticmethod
    def make_parser(parser):
        add_argument = parser.add_argument
        add_argument('font', help='font to add glyphs to')
        add_argument('image', help='image to extract glyphs from')

    @staticmethod
    def run(args):
        from . import editor
        editor.run(args.image, args.font)

@command
class render(object):
    help = 'render text using a font'

    @staticmethod
    def make_parser(parser):
        add_argument = parser.add_argument
        add_argument('font')
        add_argument('text')
        add_argument('-o', '--output', '--out', dest='output', required=True)
        add_argument('--margin', dest='margin', type=nonneg,
                     default=10,
                     help='margin around text')
        add_argument('--no-kerning', dest='kerning', default=True,
                     action='store_false',
                     help='do not use kerning tables')
        add_argument('--leading', dest='leading', type=nonneg,
                     default=None, help='type leading')

    @staticmethod
    def run(args):
        from . import font
        from . import render
        fnt = font.load_font(args.font)
        image = render.text_draw(fnt, args.text.decode('UTF-8'),
                                 args.margin, args.kerning, args.leading)
        image.save(args.output, 'png', bits=1)

@command
class convert(object):
    help = 'convert a font file'

    @staticmethod
    def make_parser(parser):
        add_argument = parser.add_argument
        add_argument('font')
        add_argument('output')
        add_argument('--no-bitmaps', dest='no_bitmaps',
                     action='store_true', default=False,
                     help='do not copy font bitmaps')
        add_argument('-f', '--format', dest='fontformat',
                     choices={'xml', 'sgfont'},
                     default='xml', help='output font format')
        add_argument('--trim', dest='trim', action='store_true',
                     default=False, help='trim glyphs to minimum size')

    @staticmethod
    def run(args):
        from . import font
        import importlib
        fnt = font.load_font(args.font)
        if args.trim:
            fnt.trim()
        if args.fontformat == 'xml':
            from . import xml
            xml.dump_font(args.output, fnt)
        elif args.fontformat == 'sgfont':
            from . import sgfont
            sgfont.dump_font(args.output, fnt, args.no_bitmaps)
        else:
            assert False

@command
class info(object):
    help = 'get information about a font'

    @staticmethod
    def make_parser(parser):
        add_argument = parser.add_argument
        add_argument('font', nargs='+')

    @classmethod
    def print_metrics(class_, fnt):
        metrics = fnt.glyph_metrics()
        print 'Height: {}'.format(metrics.y1 - metrics.y0)
        print 'Ascent: {}'.format(metrics.y1)
        print 'Descent: {}'.format(-metrics.y0)
        print 'Glyph BBOX: ({}, {}) to ({}, {})'.format(
            metrics.x0, metrics.y0, metrics.x1, metrics.y1)
        print 'Maximum advance: {}'.format(metrics.advance)

    @classmethod
    def print_encoding_stats(class_, fnt):
        encodings = [e for glyph in fnt.glyphs() for e in glyph.encoding]
        internal_nodes = set()
        for encoding in encodings:
            encoding = encoding.encode('UTF-8')
            for i in xrange(1, len(encoding)):
                internal_nodes.add(encoding[:i])
        print 'ENC: Mappings: {}'.format(len(encodings))
        print 'ENC: Internal nodes: {}'.format(len(internal_nodes))
        print 'ENC: Compressed size: {}'.format(
            len(encodings) * 4 + len(internal_nodes) * 8)
        print 'ENC: Expanded size: {}'.format(
            (len(internal_nodes) + 1) * (256 * 2 + 2))

    @classmethod
    def run(class_, args):
        from . import font
        for fpath in args.font:
            fnt = font.load_font(fpath)
            print 'Font: {}'.format(fpath)
            class_.print_metrics(fnt)
            class_.print_encoding_stats(fnt)
            print

def run():
    top = argparse.ArgumentParser(prog='font.sh')
    sub = top.add_subparsers(help='sub-command help')

    for command in _COMMANDS:
        parser = sub.add_parser(command.__name__, help=command.help)
        command.make_parser(parser)
        parser.set_defaults(func=command.run)

    args = top.parse_args()
    args.func(args)

if __name__ == '__main__':
    run()
