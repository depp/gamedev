import numpy
import collections

# Thanks to
# http://gamedev.stackexchange.com/questions/2829/texture-packing-algorithm

def _try_pack(rects, width, height):
    frontier = numpy.zeros((height,), numpy.uint16)
    ypos = 0
    sprites = []
    for rect in rects:
        sw, sh = rect.width, rect.height
        if ypos + sh > height:
            ypos = 0
        while ypos + sh <= height and frontier[ypos] + sw > width:
            ypos += 1
        if ypos + sh > height:
            return None
        xpos = int(frontier[ypos])
        frontier[:ypos+sh] = numpy.maximum(frontier[:ypos+sh], xpos + sw)
        assert 0 <= ypos <= height - sh
        assert 0 <= xpos <= width - sw
        sprites.append((xpos, ypos))
        ypos += sh
    return sprites

def _metrics(width, height):
    """Metrics for comparing packings."""
    return width * height, abs(width - height)

Placement = collections.namedtuple('Placement', 'x y flipped')
_Rect = collections.namedtuple('Rect', 'index width height flipped')

def pack(rects, allow_reorient=True):
    """Pack rectangles in a larger rectangle.

    The larger rectangle is chosen to have sides which are a power of
    two between 16 and 2048.  Returns (width,height,placement), where
    width and height are the size of the enclosing rectangle, and
    placement is a list of placements corresponding to the input
    rectangles.  If allow_reorient is true, then some of the resulting
    rectangles may be flipped (stored with exchanged width and
    height), otherwise no rectangles will be flipped.
    """
    nrects = []
    for n, (width, height) in enumerate(rects):
        if allow_reorient and width > height:
            nrects.append(_Rect(n, height, width, True))
        else:
            nrects.append(_Rect(n, width, height, False))
    nrects.sort(key=lambda x: (-x.width, -x.height))
    sizes = [2**n for n in xrange(4, 11)]
    npixels = sum(rect.width * rect.height for rect in nrects)
    best = None
    for height in sizes:
        packing = None
        for width in sizes:
            if (width * height < npixels or
                (best is not None and
                 _metrics(width, height) >= best[3])):
                continue
            # print 'Trying %dx%d...' % (width, height)
            packing = _try_pack(nrects, width, height)
            if packing is not None:
                best = width, height, packing, _metrics(width, height)
                break
    if not best:
        raise PackError('failed to pack sprites')
    result = [None] * len(nrects)
    for rect, pos in zip(nrects, best[2]):
        assert 0 <= pos[0] <= best[0] - rect.width
        assert 0 <= pos[1] <= best[1] - rect.height
        result[rect.index] = Placement(pos[0], pos[1], rect.flipped)
    return best[0], best[1], result
