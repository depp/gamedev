import numpy
import PIL.Image
import collections
import struct
from . import rect

def _convert_image(image):
    """Convert a PIL or numpy image to 8-bit RGBA numpy."""
    if isinstance(image, PIL.Image.Image):
        arr = numpy.array(image.convert('RGBA').getdata(), dtype=numpy.uint8)
        width, height = image.size
        return arr.reshape((height, width, 4))
    if isinstance(image, numpy.ndarray):
        if image.dtype == numpy.bool:
            image = image.astype(numpy.uint8) * 0xff
        elif image.dtype == numpy.uint8:
            pass
        else:
            raise TypeError('invalid numpy array element type')
        if len(image.shape) == 3:
            height, width, channels = image.shape
            if channels == 4:
                return image
            arr = numpy.empty((height, width, 4), numpy.uint8)
            if channels == 3:
                arr[:,:,0:3] = image
                arr[:,:,3] = 0xff
            elif channels == 2:
                for i in range(3):
                    arr[:,:,i] = image[:,:,0]
                arr[:,:,3] = image[:,:,1]
            elif channels == 1:
                for i in range(3):
                    arr[:,:,i] = image[:,:,0]
                arr[:,:,3] = 0xff
            else:
                raise TypeError('invalid number of components')
            return arr
        elif len(image.shape) == 2:
            height, width = image.shape
            arr = numpy.empty((height, width, 4), numpy.uint8)
            for i in range(3):
                arr[:,:,i] = image
            arr[:,:,3] = 0xff
            return arr
        else:
            raise TypeError('invalid number of dimensions: {}'
                            .format(len(image.shape)))
    raise TypeError('invalid image type')

def orient_compose(x, y):
    """Compose two orientations."""
    xm = x >> 2
    xr = x & 3
    ym = y >> 2
    yr = y & 3
    if ym:
        xr *= 3
    return ((xm ^ ym) << 2) | ((xr + yr) & 3)

def orient_inverse(x):
    """Get the inverse of an orientation."""
    if x & 4 or not (x & 1):
        return x
    return 4 - x

if __name__ == '__main__':
    for x in range(8):
        assert orient_compose(x, 0) == x
        y = orient_inverse(x)
        assert orient_compose(x, y) == 0
        for y in range(8):
            for z in range(8):
                a = orient_compose(x, orient_compose(y, z))
                b = orient_compose(orient_compose(x, y), z)
                assert 0 <= a <= 7
                assert a == b

def orient_apply(image, orientation):
    """Apply an orientation to an image."""
    if orientation & 1:
        image = numpy.transpose(image, (1, 0, 2))
    if 1 <= (orientation & 3) <= 2:
        image = image[::-1]
    if 2 <= (orientation & 7) <= 5:
        image = image[:,::-1]
    return image

def _image_flatten(image):
    """Flatten an image to a string.  Used for finding duplicates."""
    h = struct.pack('II', image.shape[1], image.shape[0])
    return h + image.tostring()

def _deduplicate_images_reorient(images):
    """Deduplicate images, reorienting them if necessary.

    Returns (map,images), where map is a list mapping old image
    indexes to (index,orientation), and images is a list of images
    with duplicates removed.
    """
    data_map = {}
    dup_map = []
    oimages = []
    for iindex, image in enumerate(images):
        for orientation in range(8):
            flat = _image_flatten(orient_apply(image, orientation))
            try:
                oindex = data_map[flat]
            except KeyError:
                pass
            else:
                dup_map.append((oindex, orientation))
                break
        else:
            data_map[_image_flatten(image)] = len(oimages)
            dup_map.append((len(oimages), 0))
            oimages.append(image)
    return dup_map, images

def _deduplicate_images(images):
    """Deduplicate images without reorienting them.

    Otherwise, the same as _deduplicate_images_reorient.  Note that
    the orientation will always be zero.
    """
    data_map = {}
    dup_map = []
    oimages = []
    for iindex, image in enumerate(images):
        flat = _image_flatten(image)
        try:
            oindex = data_map[flat]
        except KeyError:
            data_map[flat] = len(oimages)
            dup_map.append((len(oimages), 0))
            oimages.append(image)
        else:
            dup_map.append((oindex, 0))
    return dup_map, images

Sprite = collections.namedtuple('Sprite', 'x y orientation')

def pack(sprites, allow_reorient=True, margin=0, deduplicate=True):
    """Pack a collection of rectangular sprites into a texture.

    The sprites must be PIL images or suitable numpy arrays.  If
    allow_reorient is True, then the sprites may be reoriented to pack
    them.  The margin parameter specifies the minimum vertical and
    horizontal space between sprites.  If deduplicate is True, then
    duplicate sprites will be coalesced.

    Returns (img,sprites), where img is an RGBA PIL image, and sprites
    is a list of sprite objects corresponding to the input images.
    """
    if margin < 0:
        raise ValueError('negative margin')

    images = [_convert_image(sprite) for sprite in sprites]

    if deduplicate:
        if allow_reorient:
            dup_map, images = _deduplicate_images_reorient(images)
        else:
            dup_map, images = _deduplicate_images(images)

    width, height, placements = rect.pack(
        ((image.shape[1] + margin, image.shape[0] + margin)
         for image in images),
        allow_reorient=allow_reorient)

    sheet = numpy.zeros((height, width, 4), dtype=numpy.uint8)
    result = []
    for image, pos in zip(images, placements):
        orientation = 1 if pos.flipped else 0
        result.append(Sprite(pos.x, pos.y, orientation))
        image = orient_apply(image, orientation)
        height, width = image.shape[:2]
        sheet[pos.y : pos.y + height, pos.x : pos.x + width, :] = image
    sheet = PIL.Image.fromarray(sheet, mode='RGBA')

    if deduplicate:
        nresult = []
        for index, orientation in dup_map:
            sprite = result[index]
            nresult.append(sprite._replace(
                orientation=orient_compose(
                    sprite.orientation, orientation)))
        result = nresult

    return sheet, result
