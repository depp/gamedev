#!/usr/bin/env python
import argparse
import os
import pack
import subprocess
import PIL.Image
import json

MAGIC = 'SGLib Font 1.0\0\0'
assert len(MAGIC) == 16

def run():
    p = argparse.ArgumentParser()
    p.add_argument('image', nargs='+')
    p.add_argument('-o', dest='output', required=True)
    p.add_argument('--margin', dest='margin', type=int,
                   default=0,
                   help='margin between adjacent sprites')
    p.add_argument('--no-pngcrush', dest='pngcrush',
                   action='store_false', default=True,
                   help='do not run pngcrush')
    p.add_argument('--full-path', action='store_true', default=False,
                   help='name with full path')
    args = p.parse_args()

    sprites = {}
    for path in args.image:
        img = PIL.Image.open(path)
        if args.full_path:
            name = path
        else:
            name = os.path.basename(path)
        name = os.path.splitext(name)[0]
        sprites[name] = img

    img, sprites = pack.sprite_pack(sprites, allow_flip=False,
                                    margin=args.margin)

    jdata = {}
    for name, sp in sprites.items():
        jdata[name] = [sp.x, sp.y, sp.w, sp.h]

    with open(args.output + '.json', 'wb') as fp:
        json.dump(jdata, fp, separators=(',',':'));

    tmppath = args.output + '.tmp.png'
    pngpath = args.output + '.png'

    if args.pngcrush:
        img.save(tmppath, 'png')
        subprocess.check_call(
            ['pngcrush', '--', tmppath, pngpath])
        os.unlink(tmppath)
    else:
        img.save(pngpath, 'png')

run()
