Font Simple
===========

This is a total hack.  It is a tool for turning bitmap images into
some custom font format I can use for games.

The workflow is designed to be very simple.

1. Create a bitmap font in GIMP.  For now, just use a white background
with black characters.  Save the image in PNG format.

2. The tool will scan for groups of black pixels and put them in
candidate rectangles.  A candidate rectangle is a rectangle that
contains black pixels, does not contain any candidate rectangles, and
would not contain any additional black pixels if it were expanded by
one pixel in every direction.  This might be a bit tough to compute,
so the candidates might be a bit sloppy.

3. You go through the candidates and assign them to character codes
and set the baseline.  It will go through candidates left to right,
then top to bottom.  It assumes that the baseline is the same as the
baseline for the previous rect.

4. You then group characters into sets, and assign the sets names.
