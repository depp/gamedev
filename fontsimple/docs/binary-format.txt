Binary Font Format
==================

The binary font format starts with a 16-byte magic number, and
consists of a series of chunks.

File:
    16-byte magic: 'SGLib Font 1.0\0\0'
    followed by font descriptions, concatenated

A chunk has the following layout:

Chunk:
    4-byte chunk ID
    4-byte chunk length in bytes, big endian
    <data>

Terminated by a chunk ID of '\0\0\0\0'

-----

Chunk 'NAME'

Font name, a string.

-----

Chunk 'PROP': font properties

Contains textual properties, each terminated by a NUL byte.  A missing
property is represented by an empty string or by the end of the chunk.
The properties appear in the following sequence:

  * Foundry name
  * Family name
  * Weight name
  * Slant
  * Width name
  * Style

These properties have the same meaning as in the XML format.  Extra
properties after these properties are reserved for future expansion,
they should be omitted on output and ignored on input.

-----

Chunk 'FMET': font metrics

Contains two fields:
    sint16 - ascent
    sint16 - descent

Extra fields after these are reserved for future expansion, they
should be omitted on output and ignored on input.

-----

Chunk 'CHA1': map characters to glyphs.

Consists of the encoding data for each glyph, in order, concatenated.
Encoding data for a glyph consists of a sequence of all possible UTF-8
sequences which can produce the glyph, each terminated with a <FF>
byte and concatenated, followed by an additional <FF> byte.

For example, if a glyph is produced for both the character 'a' and the
character 'A', then the encoding data is:

    'a' <FF> 'A' <FF> <FF>

-----

Chunk 'MET1': glyph metrics

For each glyph, consists of the following fields:
    uint8 - glyph advance (logical width)
    uint8 - bitmap width
    uint8 - bitmap height
    sint8 - bitmap x offset
    sint8 - bitmap y offset

The coordinate system has the positive X-axis going to the right, and
the positive Y-axis going up.  The origin is the cursor position
before the glyph is drawn, which is on the baseline to the left of a
glyph.  After drawing the glyph, the cursor is adjusted by moving it
to the right by an amount equal to the glyph advance.

-----

Chunk 'BIT1': bitmap data

For each glyph, contains the bitmap for that glyph.  Each bitmap is
stored as a sequence of rows, with the top row first.  Each row
contains one bit for each pixel in that row, packed into bytes with
the most significant bit first, and padded with zeroes so that the row
is an integral number of bytes.

For example, suppose a glyph has the following 4x5 bitmap:

    XXXX
    X
    XXX
    X
    X

The bitmap data would be stored as the hexadecimal bytes:

    F0 80 E0 80 80

-----

Chunk 'KER1': kerning pairs

Consists of a sequence of kerning tables.

Each table starts with a uint16 glyph index, indicating the left glyph
in the pair, followed by a uint16 count of the number of pairs in the
table.  The table has an entry for each right glyph, consisting of a
uint16 glyph index followed by an sint8 kerning adjustment.  Positive
adjustments move the pair farther apart, negative adjustments move the
pair closer together.  For example, given the table:

    [0005 0002] [0006 FF] [0007 FF]

When glyph #5 is followed by glyph #6 or #7, the glyphs are moved
closer together by one pixel.

-----

Chunk 'SPR1': sprite sheet location

For each glyph, contains the following fields:
    uint16 x location
    uint16 y location
    uint8 orientation

The x location is the offset of the sprite's left edge from the left
of the sprite sheet, and the y location is the offset of the sprite's
top edge from the top of the sprite sheet.  The width and height of
the sprite are determined from the glyph metrics.

Orientations 0-3 are rotations by that many quarter turns
counter-clockwise.

Orientations 4-7 are the same, except followed by horizontal
mirroring.

Therefore, orientation #7 swaps the X and Y coordinates.

-----

Chunk 'SPRF': path to sprite sheet image file

This contains the path to the sprite sheet image, relative to the
directory containing this font.
