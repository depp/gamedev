#!/bin/sh
set -e
rm -rf out
mkdir out
for font in *.xml ; do
    ../font.sh convert $font out/$font
done
cd out
../../font.sh pack --margin 0 --out glyphs.png *.xml
for font in *.xml ; do
    ../../font.sh convert --format sgfont --no-bitmap $font `basename $font .xml`.sgfont
done
rm *.xml
