#!/bin/sh
set -e
in="$1"
out="$2"
if test -z "$in" || test -z "$out" ; then
    echo 'Usage: bdfpack IN OUT'
    exit 1
fi
dir=`dirname "$0"`
tool="$dir/../font.sh"

set -x
"$tool" convert "$in" "$out.xml"
"$tool" pack --out "$out.png" --margin 0 "$out.xml"
"$tool" convert --format sgfont --no-bitmap "$out.xml" "$out.sgfont"
