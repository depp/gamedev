#!/usr/bin/env python3
import sys
import xml.etree.ElementTree as etree
import os

def read_fields(filename):
    path = os.path.join(os.path.dirname(__file__), filename)
    with open(path) as fp:
        for n, line in enumerate(fp, 1):
            line = line.strip()
            if line.startswith('#') or not line:
                continue
            yield filename, n, line.split(';')

def read_names():
    for filename, n, fields in read_fields('glyphlist.txt'):
        yield filename, n, fields[1], fields[0]
    for filename, n, fields in read_fields('aglfn.txt'):
        yield filename, n, fields[0], fields[1]
    for filename, n, fields in read_fields('zapfdingbats.txt'):
        yield filename, n, fields[1], fields[0]

def ordername(name1, name2):
    d1 = any(c.isnumeric() for c in name1)
    d2 = any(c.isnumeric() for c in name2)
    if d1 and not d2:
        return True
    if d2 and not d1:
        return False
    return len(name2) > len(name1)

def read_encoding(encoding):
    return ''.join(chr(int(x, 16)) for x in encoding.split())

def default_names():
    names = {'\ufffd': ['.notdef']}
    for filename, n, encoding, name in read_names():
        s = read_encoding(encoding)
        try:
            x = names[s]
        except KeyError:
            names[s] = [name]
        else:
            if name not in x:
                x.append(name)
    for x in names.values():
        x.sort(key=lambda y: (any(c.isnumeric() for c in y), len(y), y))
    return names

def read_glyphs(fp):
    glyphs = []
    tree = etree.parse(fp)
    for child in tree.getroot():
        glyphs.append((
            chr(int(child.attrib['encoding'])),
            tuple(int(x) for x in child.attrib['metrics'].split()),
            ' '.join(child.attrib['data'].split()),
        ))
    return glyphs

def dump_glyphs(glyphs, fp):
    names = default_names()
    for encoding, metrics, data in glyphs:
        adv, w, h, x0, y0 = metrics
        print('{} b {} {} {} {} {} {}'.format(
            names[encoding][0],
            h, w, x0, y0, adv,
            data), file=fp)

def main():
    import sys
    if len(sys.argv) == 2:
        inpath = sys.argv[1]
        outpath = '-'
    elif len(sys.argv) == 3:
        inpath, outpath = sys.argv[1:]
    else:
        print('Invalid usage', file=sys.stderr)
        sys.exit(1)
    with open(inpath) as fp:
        x = read_glyphs(fp)
    if outpath == '-':
        dump_glyphs(x, sys.stdout)
    else:
        with open(outpath, 'w') as fp:
            dump_glyphs(x, fp)

if __name__ == '__main__':
    main()
