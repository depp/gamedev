extern crate serialize;
use font::{Font,CharMapEntry,Glyph,Metrics,Mode};
use font;
use std::str::from_utf8;
use self::serialize::hex::FromHex;

enum Line {
    Empty,
    Error(&'static str),
    Data(String, Vec<String>)
}

fn parse_line(line: &str) -> Line {
    let mut s = line;
    let mut parts: Vec<String> = Vec::new();
    loop {
        let olds = s;
        match s.slice_shift_char() {
            (None, _) => break,
            (Some(' '), rest) => {
                s = rest;
                parts.push(String::new());
            },
            (Some('#'), _) => break,
            (Some('"'), rest) => {
                let mut part = String::new();
                s = rest;
                loop {
                    match s.slice_shift_char() {
                        (None, _) => return Error("Unexpected end of line"),
                        (Some('"'), rest) => match rest.slice_shift_char() {
                            (None, _) => {
                                s = rest;
                                break;
                            },
                            (Some('"'), rest) => {
                                part.push('"');
                                s = rest;
                            },
                            (Some(' '), rest) => {
                                s = rest;
                                break;
                            },
                            _ => return Error("Unexpected character")
                        },
                        (Some(c), rest) => {
                            if c < '\x20' || c > '\x7e' {
                                return Error("Unexpected character");
                            }
                            part.push(c);
                            s = rest;
                        }
                    }
                }
                parts.push(part);
            },
            (Some(c), _) => {
                if c < '\x21' || c > '\x7e' {
                    return Error("Unexpected character");
                }
                parts.push(match olds.find(' ') {
                    None => {
                        s = "";
                        olds.into_string()
                    },
                    Some(i) => {
                        s = olds.slice_from(i + 1);
                        olds.slice_to(i).into_string()
                    }
                });
            }
        }
    }
    match parts.remove(0) {
        None => Empty,
        Some(x) => Data(x, parts)
    }
}

fn filter_line(line: (uint, &str)) ->
        Option<Result<(uint, String, Vec<String>), String>> {
    let (lineno, data) = line;
    match parse_line(data) {
        Empty => None,
        Error(e) => Some(Err(format!("{}: {}", lineno + 1, e))),
        Data(x, f) => Some(Ok((lineno + 1, x, f)))
    }
}

fn parse_version(ver: &str) -> Option<(uint, uint)> {
    let i = match ver.find('.') {
        None => return None,
        Some(x) => x
    };
    let maj = match from_str(ver.slice_to(i)) {
        None => return None,
        Some(x) => x
    };
    let min = match from_str(ver.slice_from(i+1)) {
        None => return None,
        Some(x) => x
    };
    Some((maj, min))
}

fn parse_mode(fields: Vec<String>) -> Option<Mode> {
    if fields.len() != 1 {
        return None;
    }
    Some(match fields[0].as_slice() {
        "monochrome" => font::Mono,
        "gray_8" => font::Gray,
        "rgb_8" => font::RGB,
        _ => return None
    })
}

fn parse_space(fields: Vec<String>) -> Option<(Metrics, Vec<u8>)> {
    if fields.len() != 1 {
        return None;
    }
    let advance = match from_str(fields[0].as_slice()) {
        Some(x) => x,
        None => return None
    };
    Some((Metrics {
        width: 0,
        height: 0,
        bearing_x: 0,
        bearing_y: 0,
        advance: advance
    }, Vec::new()))
}

fn parse_bitmap(fields: Vec<String>, mode: Mode) -> Option<(Metrics, Vec<u8>)> {
    if fields.len() != 6 {
        return None;
    }
    let w: u8 = match from_str(fields[0].as_slice()) {
        Some(x) => x, None => return None
    };
    let h: u8 = match from_str(fields[1].as_slice()) {
        Some(x) => x, None => return None
    };
    let x: i8 = match from_str(fields[2].as_slice()) {
        Some(x) => x, None => return None
    };
    let y: i8 = match from_str(fields[3].as_slice()) {
        Some(x) => x, None => return None
    };
    let advance: u8 = match from_str(fields[4].as_slice()) {
        Some(x) => x, None => return None
    };
    let mut data: Vec<u8> = match fields[5].as_slice().from_hex() {
        Ok(d) => d,
        Err(_) => return None
    };
    match mode {
        font::Mono => {
            let p = (w as uint + 7) / 8;
            if data.len() != p * (h as uint) {
                return None;
            }
            let mask: u8 = (0 - (1u << ((0 - (w as uint)) & 7))) as u8;
            for i in range(0, h as uint) {
                *data.get_mut((i + 1) * (p as uint) - 1) &= mask;
            }
        },
        font::Gray => if data.len() != (w as uint) * (h as uint) {
            return None;
        },
        font::RGB => if data.len() != (w as uint) * (h as uint) * 3 {
            return None
        }
    }
    Some((Metrics {
        width: w,
        height: h,
        bearing_x: x,
        bearing_y: y,
        advance: advance
    }, data))
}

fn parse_glyph(fields: Vec<String>, mode: Mode) -> Option<Glyph> {
    let mut fields = fields;
    let name = match fields.remove(0) {
        None => return None,
        Some(x) => x
    };
    let x = match fields.remove(0) {
        None => return None,
        Some(s) => match s.as_slice() {
            "s" => parse_space(fields),
            "b" => parse_bitmap(fields, mode),
            _ => return None
        }
    };
    match x {
        Some((metrics, data)) =>
        Some(Glyph { name: name, metrics: metrics, data: data }),
        None => None
    }
}

fn parse_charmap(fields: Vec<String>) -> Option<CharMapEntry> {
    let mut fields = fields;
    if fields.len() != 2 {
        return None;
    }
    match from_str(fields[0].as_slice()) {
        Some(codepoint) => Some(CharMapEntry {
            char_code: codepoint,
            glyph_code: fields.pop().unwrap()
        }),
        None => None
    }
}

pub fn read(data: &[u8]) -> Result<Font, String> {
    let sdata = match from_utf8(data) {
        Some(x) => x,
        None => return Err("Invalid UTF-8".to_string())
    };
    let mut iter = sdata.lines_any().enumerate().filter_map(filter_line);

    match iter.next() {
        None => return Err("Not a fontsimple font".to_string()),
        Some(line) => {
            let (lineno, cmd, fields) = try!(line);
            if cmd.as_slice() != "fontsimple" {
                return Err(format!("{}: Not a fontsimple font", lineno));
            }
            if fields.len() < 1 {
                return Err(format!("{}: Missing version number", lineno));
            }
            let (maj, _) = match parse_version(fields[0].as_slice()) {
                None => return Err(format!(
                    "{}: Invalid version number", lineno)),
                Some(x) => x
            };
            if maj > 1 {
                return Err(format!(
                    "{}: Version number too high", lineno));
            }
        }
    }

    let mut glyphs = Vec::new();
    let mut charmap = Vec::new();
    let mut mode = font::Mono;
    loop {
        let (lineno, cmd, fields) = match iter.next() {
            Some(x) => try!(x),
            None => break
        };
        match cmd.as_slice() {
            "mode" => {
                if glyphs.len() > 0 {
                    return Err(format!(
                        "{}: Cannot change mode after glyphs", lineno));
                }
                mode = match parse_mode(fields) {
                    Some(x) => x,
                    None => return Err(format!(
                        "{}: Invalid mode", lineno))
                }
            },
            "start_glyphs" => {
                loop {
                    let (lineno, cmd, fields) = match iter.next() {
                        Some(x) => try!(x),
                        None => return Err(
                            "Unexpected end of file".to_string())
                    };
                    match cmd.as_slice() {
                        "end_glyphs" => break,
                        "" => (),
                        _ => return Err(format!(
                            "{}: Unexpected command", lineno))
                    }
                    glyphs.push(match parse_glyph(fields, mode) {
                        Some(x) => x,
                        None => return Err(format!(
                            "{}: Invalid glyph", lineno))
                    });
                }
            },
            "start_charmap" => {
                loop {
                    let (lineno, cmd, fields) = match iter.next() {
                        Some(x) => try!(x),
                        None => return Err(
                            "Unexpected end of file".to_string())
                    };
                    match cmd.as_slice() {
                        "end_charmap" => break,
                        "" => (),
                        _ => return Err(format!(
                            "{}: Unexpected command", lineno))
                    };
                    charmap.push(match parse_charmap(fields) {
                        Some(x) => x,
                        None => return Err(format!(
                            "{}: Invalid charmap entry", lineno))
                    });
                }
            },
            _ => return Err(format!(
                "{}: Unknown command", lineno))
        }
    }
    Ok(Font {
        mode: mode,
        glyphs: glyphs,
        cmap: charmap
    })
}
