#include "defs.hpp"

int main(int argc, char **argv) {
    Options opts;
    opts.parse(argc, argv);

    FT_Library library;
    auto err = FT_Init_FreeType(&library);
    if (err) die_ft(err, "could not initialize freetype");

    Font font(library, opts.inpath);
    font.scale(opts);
    font.load_names(opts);
    font.load_charmap(opts);
    font.dump(opts, opts.outpath);

    return 0;
}
