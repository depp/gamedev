#include "defs.hpp"
namespace Flags {

struct Flag {
    unsigned flag;
    const char *name;
};

void print(const Flag *flags, unsigned value) {
    for (int i = 0; flags[i].flag != 0; i++) {
        if ((value & flags[i].flag) == 0)
            continue;
        std::printf("  %s\n", flags[i].name);
    }
}

#define FLAG(x) { x, #x + 13 }
const Flag FACE_FLAGS[] = {
    FLAG(FT_FACE_FLAG_SCALABLE),
    FLAG(FT_FACE_FLAG_FIXED_SIZES),
    FLAG(FT_FACE_FLAG_FIXED_WIDTH),
    FLAG(FT_FACE_FLAG_SFNT),
    FLAG(FT_FACE_FLAG_HORIZONTAL),
    FLAG(FT_FACE_FLAG_VERTICAL),
    FLAG(FT_FACE_FLAG_KERNING),
    FLAG(FT_FACE_FLAG_FAST_GLYPHS),
    FLAG(FT_FACE_FLAG_MULTIPLE_MASTERS),
    FLAG(FT_FACE_FLAG_GLYPH_NAMES),
    FLAG(FT_FACE_FLAG_EXTERNAL_STREAM),
    FLAG(FT_FACE_FLAG_HINTER),
    FLAG(FT_FACE_FLAG_CID_KEYED),
    FLAG(FT_FACE_FLAG_TRICKY),
    FLAG(FT_FACE_FLAG_COLOR),
    { 0, nullptr }
};
#undef FLAG

}
