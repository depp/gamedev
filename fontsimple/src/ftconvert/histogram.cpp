#include "defs.hpp"
#include <cmath>

void Histogram::add(int value) {
    auto i = std::begin(m_points), e = std::end(m_points);
    for (; i != e; i++) {
        if (i->value > value)
            break;
        if (i->value == value) {
            i->count++;
            return;
        }
    }
    Point pt;
    pt.value = value;
    pt.count = 1;
    m_points.insert(i, pt);
}

void Histogram::dump() const {
    for (auto i = std::begin(m_points), e = std::end(m_points);
         i != e; i++) {
        std::printf(" (%d, %d)", i->value, i->count);
    }
    std::putchar('\n');
}

double Histogram::autoscale() const {
    int count = 0;
    for (auto i = std::begin(m_points), e = std::end(m_points);
         i != e; i++)
        count += i->count;
    if (!count)
        die("No glyphs");
    int max = m_points.back().value;
    int limit = std::min(max, 48);
    int n;
    for (n = 3; n <= limit; n++) {
        double scale = static_cast<double>(n) / static_cast<double>(max);
        double accvar = 0.0;
        for (auto i = std::begin(m_points), e = std::end(m_points);
             i != e; i++) {
            double ipart;
            double fpart = std::modf(scale * i->value, &ipart);
            if (fpart < -0.5)
                fpart += 1.0;
            else if (fpart > 0.5)
                fpart -= 1.0;
            accvar += fpart * fpart * i->count;
        }
        double var = accvar * scale / count;
        if (var < 1e-4) {
            return scale;
        }
    }
    die("Could not guess pixel size (must specify size explicitly)");
    return 1.0;
}
