#include "defs.hpp"
#include <cstdarg>
#include <cstdio>
#include <cstdlib>

#define FT_ERRORDEF(e, v, s) { v, s },
#define FT_ERROR_START_LIST {
#define FT_ERROR_END_LIST }

struct fterror { int code; const char *msg; };

static const struct fterror FTERROR[] =
#undef __FTERRORS_H__
#include FT_ERRORS_H
    ;

__attribute__((noreturn))
void die(const char *reason, ...)
{
    std::fputs("Error: ", stderr);
    va_list ap;
    va_start(ap, reason);
    std::vfprintf(stderr, reason, ap);
    va_end(ap);
    std::fputc('\n', stderr);
    std::exit(1);
}

__attribute__((noreturn))
void die_ft(FT_Error err, const char *reason, ...)
{
    std::fputs("Error: ", stderr);
    va_list ap;
    va_start(ap, reason);
    std::vfprintf(stderr, reason, ap);
    va_end(ap);
    std::fputs(": ", stderr);
    for (unsigned i = 0; i < sizeof(FTERROR) / sizeof(*FTERROR); i++) {
        if (err == FTERROR[i].code) {
            std::fputs(FTERROR[i].msg, stderr);
            goto known;
        }
    }
    std::fprintf(stderr, "unknown error %d", err);
known:
    std::fputc('\n', stderr);
    std::exit(1);
}
