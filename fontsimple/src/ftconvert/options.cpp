#include "defs.hpp"
#include <stdexcept>

void Options::parse(int argc, char **argv) {
    int k = 0;
    for (int i = 1; i < argc; i++)  {
        std::string arg(argv[i]);
        if (arg.size() >= 2 && arg[0] == '-') {
            if (arg == "-s" || arg == "--size") {
                i++;
                if (i == argc)
                    die("Missing parameter for -s/--size");
                arg = argv[i];
                if (arg == "-" || arg == "auto") {
                    size = -1;
                } else {
                    try {
                        size = std::stoi(arg);
                    } catch (std::logic_error &) {
                        die("Invalid size: %s", arg.c_str());
                    }
                }
            } else if (arg == "-m" || arg == "--mode") {
                i++;
                if (i == argc)
                    die("Missing parameter for -m/--mode");
                arg = argv[i];
                if (arg == "mono") {
                    mode = Mode::MONOCHROME;
                } else if (arg == "gray") {
                    mode = Mode::GRAY;
                } else if (arg == "rgb") {
                    mode = Mode::RGB;
                } else {
                    die("Invalid mode, must be mono, gray, or rgb");
                }
            } else {
                die("Unknown argument: %s", arg.c_str());
            }
        } else {
            switch (k++) {
            case 0: inpath = std::move(arg); break;
            case 1: outpath = std::move(arg); break;
            default:
                die("Unexpected argument: %s", arg.c_str());
                break;
            }
        }
    }
    if (k != 2)
        die("usage: ftconvert [-s SIZE] [-m MODE] INPUT OUTPUT");
}
