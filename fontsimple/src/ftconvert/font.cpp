#include "defs.hpp"
#include <cmath>

Font::Font(FT_Library library, const std::string &path)
    : m_face(nullptr) {
    FT_Error err;
    err = FT_New_Face(library, path.c_str(), 0, &m_face);
    if (err)
        die_ft(err, "%s", path.c_str());

    std::printf("Number of glyphs: %ld\n", m_face->num_glyphs);
    std::puts("Flags:");
    Flags::print(Flags::FACE_FLAGS, m_face->face_flags);
    std::printf("Units per em: %d\n", m_face->units_per_EM);
    std::printf("Fixed sizes: %d\n", m_face->num_fixed_sizes);
}

void Font::scale(const Options &opts) {
    FT_Error err;
    int pixel_size;
    if (opts.size > 0) {
        pixel_size = opts.size * 64;
    } else if (m_face->num_fixed_sizes > 0) {
        pixel_size = m_face->available_sizes[0].y_ppem;
    } else {
        Histogram histo;
        for (int i = 0, n = m_face->num_glyphs; i < n; i++) {
            err = FT_Load_Glyph(m_face, i, FT_LOAD_NO_SCALE);
            if (err) die_ft(err, "Could not load glyph %d", i);
            auto ol = m_face->glyph->outline;
            for (auto p = ol.points, e = p + ol.n_points; p != e; p++) {
                histo.add(p->x);
                histo.add(p->y);
            }
        }
        double scale = histo.autoscale();
        pixel_size = static_cast<int>(
            std::floor(m_face->units_per_EM * scale * 64 + 0.5));
    }
    std::printf("Choosing font size: %f\n",
                static_cast<double>(pixel_size) * (1.0 / 64.0));
    err = FT_Set_Char_Size(
        m_face, 0, pixel_size, 72, 72);
    if (err) die_ft(err, "Could not set font size");
}

void Font::load_names(const Options &opts) {
    (void) &opts;
    FT_Error err;
    m_names.clear();
    if (FT_HAS_GLYPH_NAMES(m_face)) {
        for (int i = 0, n = m_face->num_glyphs; i < n; i++) {
            char gname[64];
            err = FT_Get_Glyph_Name(m_face, i, gname, sizeof(gname));
            if (err) die_ft(err, "Could not get glyph name for glyph %d", i);
            m_names.push_back(gname);
        }
    } else {
        for (int i = 0, n = m_face->num_glyphs; i < n; i++) {
            char gname[16];
            std::snprintf(gname, sizeof(gname), "g%d", i);
            m_names.push_back(gname);
        }
    }
}

void Font::load_charmap(const Options &opts) {
    (void) &opts;
    FT_Error err;
    for (int i = 0, n = m_face->num_charmaps; i < n; i++) {
        auto cmap = m_face->charmaps[i];
        if (cmap->encoding == FT_ENCODING_UNICODE) {
            err = FT_Set_Charmap(m_face, cmap);
            if (err) die_ft(err, "Could not set charmap %d", i);
            return;
        }
    }
    die("No Unicode charmap available");
}

void Font::dump_glyphs(const Options &opts, FILE *out) const {
    FT_Error err;

    FT_UInt32 lflags;
    FT_Render_Mode rflags;
    const char *modename;
    switch (opts.mode) {
    case Mode::MONOCHROME:
        modename = "monochrome";
        lflags = FT_LOAD_TARGET_MONO;
        rflags = FT_RENDER_MODE_MONO;
        break;
    case Mode::GRAY:
        modename = "gray_8";
        lflags = FT_LOAD_TARGET_NORMAL;
        rflags = FT_RENDER_MODE_NORMAL;
        break;
    case Mode::RGB:
        modename = "rgb_8";
        lflags = FT_LOAD_TARGET_LCD;
        rflags = FT_RENDER_MODE_LCD;
        break;
    default:
        die("Invalid mode");
        return;
    }
    std::fprintf(out, "mode %s\n", modename);

    std::fputs("start_glyphs\n", out);
    for (int i = 0, n = m_face->num_glyphs; i < n; i++) {
        err = FT_Load_Glyph(m_face, i, lflags);
        if (err) die_ft(err, "Could not load glyph %d", i);
        err = FT_Render_Glyph(m_face->glyph, rflags);
        if (err) die_ft(err, "Could not load glyph %d", i);
        auto slot = m_face->glyph;
        std::fputc(' ', out);
        std::fputs(m_names.at(i).c_str(), out);
        auto &bitmap = slot->bitmap;
        if (bitmap.width == 0 || bitmap.rows == 0) {
            std::fprintf(out, " s %d\n", (int) (slot->advance.x >> 6));
        } else {
            std::fprintf(
                out, " b %d %d %d %d %d ",
                bitmap.width, bitmap.rows,
                slot->bitmap_left, slot->bitmap_top,
                (int) (slot->advance.x >> 6));

            int w = bitmap.width, h = bitmap.rows, p = bitmap.pitch;
            const unsigned char *buf = bitmap.buffer;
            switch (opts.mode) {
            case Mode::MONOCHROME:
                if (bitmap.pixel_mode != FT_PIXEL_MODE_MONO)
                    die("Bitmap not in mono");
                {
                    int mask = 0x100 - (1 << ((-w) & 7));
                    for (int y = 0; y < h; y++) {
                        for (int x = 0; x < (w + 7) / 8; x++) {
                            int d = buf[y * p + x];
                            if (x == (w + 7) / 8)
                                d &= mask;
                            std::fprintf(out, "%02X", d);
                        }
                    }
                }
                break;

            case Mode::GRAY:
                if (bitmap.pixel_mode != FT_PIXEL_MODE_GRAY)
                    die("Bitmap not in grayscale");
                for (int y = 0; y < h; y++) {
                    for (int x = 0; x < w; x++) {
                        std::fprintf(out, "%02X", buf[y * p + x]);
                    }
                }
                break;

            case Mode::RGB:
                if (bitmap.pixel_mode != FT_PIXEL_MODE_LCD)
                    die("Bitmap not in RGB");
                die("Not supported anyway");
                break;
            }
            std::fputc('\n', out);
        }
    }
    std::fputs("end_glyphs\n", out);

}

void Font::dump_charmap(const Options &opts, FILE *out) const {
    (void) &opts;
    std::fputs("start_charmap\n", out);
    FT_UInt gindex;
    FT_ULong charcode = FT_Get_First_Char(m_face, &gindex);
    while (gindex != 0) {
        std::fprintf(out, " %lu %s\n",
                     charcode, m_names.at(gindex).c_str());
        charcode = FT_Get_Next_Char(m_face, charcode, &gindex);
    }
    std::fputs("end_charmap\n", out);
}

void Font::dump(const Options &opts, const std::string &path) const {
    FILE *out = std::fopen(path.c_str(), "w");
    std::fputs("fontsimple 1.0\n", out);
    dump_glyphs(opts, out);
    dump_charmap(opts, out);
    fclose(out);
}
