#include <ft2build.h>
#include FT_FREETYPE_H
#include <vector>
#include <string>

__attribute__((noreturn, format(printf, 1, 2)))
void die(const char *reason, ...);

__attribute__((noreturn, format(printf, 2, 3)))
void die_ft(FT_Error err, const char *reason, ...);

enum class Mode {
    MONOCHROME,
    GRAY,
    RGB
};

struct Options {
    int size;
    Mode mode;
    std::string inpath;
    std::string outpath;

    Options() : size(-1), mode(Mode::MONOCHROME) { }

    void parse(int argc, char **argv);
};

namespace Flags {

struct Flag;

void print(const Flag *flags, unsigned value);

extern const Flag FACE_FLAGS[];

}

class Histogram {
private:
    struct Point {
        int value;
        int count;
    };

    std::vector<Point> m_points;

public:
    void add(int value);
    void dump() const;
    double autoscale() const;
};

class Font {
private:
    FT_Face m_face;
    std::vector<std::string> m_names;

    void dump_glyphs(const Options &opts, FILE *out) const;
    void dump_charmap(const Options &opts, FILE *out) const;

public:
    Font(FT_Library library, const std::string &path);
    void scale(const Options &opts);
    void load_names(const Options &opts);
    void load_charmap(const Options &opts);
    void dump(const Options &opts, const std::string &path) const;
};
