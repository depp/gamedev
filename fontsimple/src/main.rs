#![crate_name = "fonttool"]
#![crate_type = "bin"]
extern crate image;
use std::io::{IoResult,File,BufferedWriter};
use image::{Grey};
use image::png::{PNGEncoder};

mod font;
mod show;
mod read;
mod write;

fn main_io() -> IoResult<()> {
    let args = std::os::args();
    let inpath = Path::new(args[1].as_slice());
    let outpath = Path::new(args[2].as_slice());
    let imgpath = Path::new(args[3].as_slice());

    let data = try!(File::open(&inpath).read_to_end());
    let font = match read::read(data.as_slice()) {
        Ok(x) => x,
        Err(e) => {
            println!("Error: {}", e);
            return Ok(());
        }
    };

    try!(write::write_font(
        &font,
        &mut BufferedWriter::new(try!(File::create(&outpath)))));

    {
        let (w, h, data) = show::show(&font);
        let mut enc = PNGEncoder::new(File::create(&imgpath));
        try!(enc.encode(data.as_slice(), w, h, Grey(8)));
    }

    Ok(())
}

fn main() {
    match main_io() {
        Ok(_) => (),
        Err(e) => println!("Error: {}", e)
    }
}
