use std::io::{IoResult};
use font::{Font,Glyph,CharMapEntry};

fn write_glyph<W: Writer>(glyph: &Glyph, w: &mut W) -> IoResult<()> {
    try!(write!(w, " {} ", glyph.name));
    let m = glyph.metrics;
    if m.width == 0 || m.height == 0 {
        try!(write!(w, "s {}", m.advance));
    } else {
        try!(write!(
            w, "b {} {} {} {} {} ",
            m.width, m.height, m.bearing_x, m.bearing_y, m.advance));
        for byte in glyph.data.iter() {
            try!(write!(w, "{:02X}", *byte));
        }
    }
    w.write_char('\n')
}

fn write_charmap<W: Writer>(cmap: &[CharMapEntry], w: &mut W) -> IoResult<()> {
    try!(write!(w, "start_charmap\n"));
    for e in cmap.iter() {
        try!(write!(w, " {} {}\n", e.char_code, e.glyph_code));
    }
    w.write_str("end_charmap\n")
}

pub fn write_font<W: Writer>(font: &Font, w: &mut W) -> IoResult<()> {
    try!(w.write_str("fontsimple 1.0\n"));
    try!(w.write_str("start_glyphs\n"));
    for glyph in font.glyphs.iter() {
        try!(write_glyph(glyph, w));
    }
    try!(w.write_str("end_glyphs\n"));
    write_charmap(font.cmap.as_slice(), w)
}
