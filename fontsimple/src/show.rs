use font::Font;
use font;
use std::cmp::{max,min};

pub fn show(font: &Font) -> (u32, u32, Vec<u8>) {
    let glyphs = &font.glyphs;
    let cells_w: uint = 32;
    let cells_h: uint = (glyphs.len() as uint + cells_w - 1) / cells_w;
    let cells_h = max(cells_h, 1);
    let (glyph_w, glyph_h) = glyphs.iter().fold(
        (0, 0),
        |x, y| (max(x.val0(), y.metrics.width as uint),
                max(x.val1(), y.metrics.height as uint)));
    let margin = 2;
    let box_w = glyph_w + margin * 2 + 1;
    let box_h = glyph_h + margin * 2 + 1;
    let img_w = cells_w * box_w - 1;
    let img_h = cells_h * box_h - 1;

    let mut data = Vec::from_elem(img_w * img_h, 0u8);

    let grid_color = 64u8;
    for y in range(0, img_h) {
        for xi in range(1, cells_w) {
            let x = xi * box_w - 1;
            *data.get_mut(y * img_w + x) = grid_color;
        }
    }
    for yi in range(1, cells_h) {
        let y = yi * box_h - 1;
        for x in range(0, img_w) {
            *data.get_mut(y * img_w + x) = grid_color;
        }
    }

    for (i, glyph) in glyphs.iter().enumerate() {
        let gx = (i % cells_w) * box_w + margin;
        let gy = (i / cells_w) * box_h + margin;
        let gw = glyph.metrics.width as uint;
        let gh = glyph.metrics.height as uint;
        let gdata = glyph.data.as_slice();
        match font.mode {
            font::Mono => {
                let gr = (gw + 7) / 8;
                for y in range(0, gh) {
                    for r in range(0, gr) {
                        let pix = gdata[y * gr + r];
                        for x in range(0, min(8, gw - r * 8)) {
                            *data.get_mut((gy + y) * img_w + gx + r * 8 + x) =
                                ((pix >> (7 - x)) & 1) * 255;
                        }
                    }
                }
            },
            font::Gray => {
                for y in range(0, gh) {
                    for x in range(0, gw) {
                        *data.get_mut((gy + y) * img_w + gx + x) =
                            gdata[y * gw + x]
                    }
                }
            },
            font::RGB => fail!("RGB not supported")
        }
    }

    (img_w as u32, img_h as u32, data)
}
