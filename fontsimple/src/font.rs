#[deriving(Clone)]
pub enum Mode {
    Mono,
    Gray,
    RGB
}

#[deriving(Clone)]
pub struct Metrics {
    pub width: u8,
    pub height: u8,
    pub bearing_x: i8,
    pub bearing_y: i8,
    pub advance: u8
}

#[deriving(Clone)]
pub struct Glyph {
    pub name: String,
    pub metrics: Metrics,
    pub data: Vec<u8>,
}

#[deriving(Clone)]
pub struct CharMapEntry {
    pub char_code: uint,
    pub glyph_code: String
}

#[deriving(Clone)]
pub struct Font {
    pub mode: Mode,
    pub glyphs: Vec<Glyph>,
    pub cmap: Vec<CharMapEntry>
}
