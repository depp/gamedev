# Moria Gamedev Repository

This is a collection of games, as well as libraries and tools used to build and develop those games.  New components added to this repository as they are created.

Many of these games were created for game jams, especially Ludum Dare.

## Licensing

The license varies somewhat from project to project.  Check LICENSE.txt in project folders.  The most common licenses are the MIT and 2-clause BSD licenses.

## Monorepository / Monolithic Version Control

From 2011 to 2016 these games and tools were organized in separate repositories.  Larger components were organized as submodules in Git, smaller components were copied to repositories as needed.  This made it difficult or cumbersome to share code between different projects, especially since not all projects were written in the same language.

Inspired by companies like Google, Facebook, and Twitter, these separate repositories (over 30 of them) were slowly merged into one monolithic repository.  During the merge, binary assets were removed from history to cut down on repository size.
