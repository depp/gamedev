#!/usr/bin/env python3

FACE = 'upheavtt.ttf'
SIZE = 20
OUT_PNG = 'Font.png'
OUT_JSON = 'Font.json'

CHARSET = """ABCDEFGHIJKLMNOPQRSTUVWXYZ.,/() '!?-*:0123456789"""
CHARSET = """ !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ"""
print("Character count", len(CHARSET))

CSIZE = 16, 16
ISIZE = 8, 8

import collections
import numpy
import freetype
import PIL.Image
import json

Glyph = collections.namedtuple('Glyph', 'chr advance bx by arr')

def load_font():
    face = freetype.Face(FACE)
    face.set_char_size(SIZE * 64)
    glyphs = []
    for c in CHARSET:
        face.load_char(c)
        bitmap = face.glyph.bitmap
        arr = (numpy.array(bitmap.buffer, dtype=numpy.uint8)
               .reshape(bitmap.rows, bitmap.width))
        glyphs.append(Glyph(
            c,
            face.glyph.advance.x >> 6,
            face.glyph.bitmap_left,
            face.glyph.bitmap_top,
            arr))
    return glyphs

def get_metrics(glyphs):
    print('SX', max(g.arr.shape[1] for g in glyphs))
    print('SY', max(g.arr.shape[0] for g in glyphs))
    print(
        'X range:',
        min(g.bx for g in glyphs),
        max(g.bx + g.arr.shape[1] for g in glyphs))
    print(
        'Y range:',
        min(g.by for g in glyphs),
        max(g.by + g.arr.shape[0] for g in glyphs))


def pack_font(glyphs):
    info = []
    get_metrics(glyphs)
    a = numpy.zeros((CSIZE[1] * ISIZE[1], CSIZE[0] * ISIZE[0]),
                    dtype=numpy.uint8)
    for n, glyph in enumerate(glyphs):
        x = n % ISIZE[0]
        y = n // ISIZE[0]
        xo = x * CSIZE[0]
        yo = y * CSIZE[1]
        b = glyph.arr
        a[yo:yo+b.shape[0],xo:xo+b.shape[1]] = b
        info.extend([
            glyph.advance,
            glyph.bx,
            glyph.by,
            b.shape[1],
            b.shape[0],
        ])
    img = PIL.Image.fromarray(a)
    img.save(OUT_PNG)
    with open(OUT_JSON, 'w') as fp:
        json.dump({
            'Chars': CHARSET,
            'Data': info
        }, fp, separators=(',',':'))
    print('DONE')

pack_font(load_font())
