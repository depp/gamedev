precision mediump float;

varying vec3 vShade;
varying vec4 vHighlight;
varying vec2 vTexCoord;

uniform sampler2D Texture;

void main() {
    vec3 c = mix(
        texture2D(Texture, vTexCoord).rgb * vShade,
        vHighlight.rgb,
        vHighlight.a);
    gl_FragColor = vec4(c, 1.0);
}
