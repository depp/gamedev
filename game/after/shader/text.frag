precision mediump float;

varying vec2 vTexCoord;
varying vec4 vColor;

uniform sampler2D Texture;

void main() {
    gl_FragColor = texture2D(Texture, vTexCoord).r * vColor;
}
