attribute vec4 Pos;
attribute vec2 TexCoord;

varying vec2 vTexCoord;

uniform mat4 MVP;
uniform vec2 TexScale;

void main() {
    vTexCoord = TexCoord * TexScale;
    gl_Position = MVP * vec4(Pos.xyz, 1.0);
}
