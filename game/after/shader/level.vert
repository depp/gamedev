attribute vec4 Pos;
attribute vec2 TexCoord;
attribute float Highlight;

varying vec3 vShade;
varying vec4 vHighlight;
varying vec2 vTexCoord;

uniform mat4 MVP;
uniform vec4 Shades[6];
uniform vec4 Highlights[16];
uniform float Blink;

void main() {
    int shade = int(Pos.w);
    vShade = Shades[shade].rgb;
    int highlight = int(Highlight);
    vHighlight = mix(
        Highlights[highlight * 2],
        Highlights[highlight * 2 + 1],
        Blink);
    vTexCoord = TexCoord * vec2(0.0078125, 0.015625);
    gl_Position = MVP * vec4(Pos.xyz, 1.0);
}
