attribute vec3 Vert;
varying vec4 vColor;
uniform vec4 Colors[4];
void main() {
    vColor = Colors[int(Vert.z)];
    gl_Position = vec4(Vert.xy, 0.0, 1.0);
}
