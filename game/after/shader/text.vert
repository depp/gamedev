attribute vec4 Glyph;
attribute float Style;

varying vec2 vTexCoord;
varying vec4 vColor;

uniform vec2 VertScale;
uniform vec2 VertOff;
uniform vec2 TexScale;
uniform vec4 Colors[32];

void main() {
    vTexCoord = Glyph.zw * TexScale;
    vec4 c = Colors[int(Style)];
    vColor = vec4(c.rgb * c.a, c.a);
    gl_Position = vec4(Glyph.xy * VertScale + VertOff, 0.0, 1.0);
}
