#!/bin/sh
set -e
gulp clean
gulp package
cd build
tar zc . | ssh gloin 'set -e; cd /home/web; rm -r after; mkdir after; cd after; tar xzv'
