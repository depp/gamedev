'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var transform = require('vinyl-transform');
var del = require('del');
var webserver = require('gulp-webserver');
var fc2json = require('gulp-file-contents-to-json');
var jshint = require('gulp-jshint');
var pkgJson = require('./package.json');
var uglify = require('gulp-uglify');

function handleError(err) {
	gutil.log('' + err);
	this.emit('end');
}

gulp.task('clean', function(cb) {
	return del(['build', 'shader/shaders.json'], cb);
});

gulp.task('shaders', function() {
	return gulp.src(['shader/*.vert', 'shader/*.frag'])
		.pipe(fc2json('shaders.json'))
		.pipe(gulp.dest('shader'));
});

gulp.task('lint', function() {
	return gulp.src(['./js/**/*.js', '!**/.*'])
		.pipe(jshint(pkgJson.jshintConfig))
		.pipe(jshint.reporter('default'));
});

function js(debugEnabled) {
	var browserified = transform(function(filename) {
		var b = browserify(filename, {debug:debugEnabled});
		return b.bundle();
	});

	var x = gulp.src('./js/app.js')
		.pipe(browserified);

	if (!debugEnabled) {
		x = x.pipe(uglify());
	} else {
		x = x.on('error', handleError);
	}
	return x.pipe(gulp.dest('./build/'));
};

gulp.task('browserify', ['shaders'], function() {
	return js(true);
});

gulp.task('copy', function() {
	return gulp.src([
		'js/index.html',
		'tools/sprites/sprites.png',
		'tools/fonts/glyphs.png',
		'img/*.png'
	]).pipe(gulp.dest('build'));
});

gulp.task('watch', ['lint', 'browserify', 'copy'], function() {
	gulp.watch([
		'js/**/*.js',
		'shader/*.vert',
		'shader/*.frag',
		'tools/sprites/sprites.json',
		'tools/fonts/font.json'
	], [
		'lint', 'browserify'
	]);
	gulp.watch([
		'js/index.html',
		'tools/sprites/sprites.png',
		'tools/fonts/glyphs.png',
		'img/*.png'
	], [
		'copy'
	]);
});

gulp.task('dev', ['watch'], function() {
	gulp.src('build')
		.pipe(webserver({}));
});

gulp.task('dev-pub', ['watch'], function() {
	gulp.src('build')
		.pipe(webserver({host:'0.0.0.0'}));
});

gulp.task('package', ['lint', 'shaders', 'copy'], function() {
	js(false);
	gulp.src(['js/services.json']).pipe(gulp.dest('build'));
	gulp.src(['levels/*.json']).pipe(gulp.dest('build/level'));
});
