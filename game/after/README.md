# After We Saved the World

*After We Saved the World* (or "After" for short) is a game developed for Mini Ludum Dare #57.

## Running locally

You can play the game through a local web server by running the following command:

    gulp dev
