(function(){
	'use strict';
	var err_webgl = 'Your browser does not seem to support WebGL.';
	var err_script = 'Could not load game script.';
	function fail(reason) {
		console.log('FAIL', reason);
		document.body.removeChild(canvas);
		var err = document.createElement('p');
		var text = document.createTextNode('Error: ' + reason);
		err.className = 'error';
		err.appendChild(text);
		document.body.appendChild(err);
	}
	var canvas = document.createElement('canvas');
	document.body.appendChild(canvas);
	var gl = canvas.getContext('webgl') ||
		canvas.getContext('experimental-webgl');
	if (!gl) {
		return fail(err_webgl);
	}
	var script = document.createElement('script');
	script.onload = function() {
		try{
			var game = require('game');
			window.game = game;
			game.init(canvas, gl);
		} catch(e) {
			console.error(e);
			fail(err_script);
		}
	};
	script.onerr = function() {
		fail(err_script);
	};
	script.type = 'text/javascript';
	script.src = '{{ref:game.js}}';
	document.body.appendChild(script);
})();
