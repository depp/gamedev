# Development tools

Run `python3 -m after.service` to start the web API.

Run `python3 -m after.font` to create the font sheet.

Run `./gensprite.sh` to create the sprite sheet.
