#include "defs.hpp"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>

int main(int argc, char *argv[]) {
    (void) argc;
    (void) argv;

    std::vector<Packing::Rect> rects;
    std::string line;
    std::istringstream is;
    while (std::cin) {
        std::getline(std::cin, line, '\n');
        if (line.empty()) {
            continue;
        }
        is.str(line);
        is.clear();
        Packing::Rect r;
        is >> r.size[0] >> r.size[1];
        if (is.fail()) {
            die("Invalid size");
        }
        rects.push_back(r);
    }

    Packing pack;
    IVec2 min_size{{8, 8}}, max_size{{2048, 2048}};
    if (!pack.auto_pack(rects.data(), rects.size(), min_size, max_size)) {
        die("Packing failed");
    }

    std::cout << pack.size()[0] << ' ' << pack.size()[1] << '\n';
    for (const auto r : rects) {
        std::cout << r.loc[0] << ' ' << r.loc[1] << '\n';
    }

    return 0;
}
