#include "defs.hpp"

#include <iostream>
#include <ft2build.h>
#include FT_FREETYPE_H

#define FT_ERRORDEF(e, v, s) { v, s },
#define FT_ERROR_START_LIST {
#define FT_ERROR_END_LIST }

namespace {

struct FTError { int code; const char *msg; };

static const struct FTError FTERROR[] =
#undef __FTERRORS_H__
#include FT_ERRORS_H
    ;

}

void die_ft(const char *file, int line, int err) {
    std::cerr << file << ':' << line << ": freetype error: ";
    for (const auto &e : FTERROR) {
        if (e.code == err) {
            std::cerr << e.msg;
            break;
        }
    }
    std::cerr << '\n';
    std::exit(1);
}
