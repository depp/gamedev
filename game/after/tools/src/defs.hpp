#include <string>
#include <vector>

#include "vec.hpp"

__attribute__((noreturn))
void die_func(const char *file, int line, const char *msg);

__attribute__((noreturn))
void die_ft(const char *file, int line, int err);

#define die(msg) die_func(__FILE__, __LINE__, msg)

/// A packing of rectangles in a larger bin.
///
/// The object tracks free space within the bin, so rectangles can be
/// added to the bin dynamically.
class Packing {
public:
    struct RectRef;

    /// A rectangle to be packed in a bin.
    struct Rect {
        /// The rectangle size.  This is a packing algorithm input.
        I16Vec2 size;

        /// The location of the rectangle's bottom left corner.  This is a
        /// packing algorithm output.
        I16Vec2 loc;
    };

private:
    std::vector<I16Vec2> m_nodes;
    IVec2 m_size;

public:
    Packing();

    /// Get the size of the bin.
    IVec2 size() const { return m_size; }

    /// Add rectangles to the packing.  Returns true if successful,
    /// false if the packing failed.
    bool pack(Rect *rects, std::size_t count);

    /// Reset the packing, and add the given rectangles to the
    /// packing, resizing the packing as necessary.  Only powers of
    /// two will be used for dimensions.  Returns true if successful,
    /// false if the packing failed.
    bool auto_pack(Rect *rects, std::size_t count,
                   IVec2 min_size, IVec2 max_size);

    /// Reset the packing to an empty bin with the given size.
    void reset(IVec2 size);

private:
    bool pack_impl(Rect *rects, const RectRef *refs, std::size_t count);
};

/// An 8-bit grayscale image.
class GrayImage {
private:
    std::vector<unsigned char> m_data;
    IVec2 m_size;

public:
    GrayImage();
    explicit GrayImage(IVec2 size);
    GrayImage(IVec2 size, const void *data, int rowbytes);

    /// Get the size of this image.
    IVec2 size() const { return m_size; }

    /// Save this image to a PNM file at the given path.
    void save(const std::string &path) const;

    /// Paste another image into this one.
    void paste(const GrayImage &other, IVec2 pos);
};
