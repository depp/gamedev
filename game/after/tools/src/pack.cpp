/* Copyright 2013-2015 Dietrich Epp.  */
#include "defs.hpp"

#include <algorithm>
#include <cassert>
#include <vector>

/*
  Rectangle packing

  From Jukka Jylänki's "A Thousand Ways to Pack the Bin", we use the
  "skyline" algorithm, with bottom-left heuristic for placing the
  rects, and sorting the rects by width and then by height, with
  biggest rects placed first.  No waste management is performed.
*/

struct Packing::RectRef {
    unsigned index;
    I16Vec2 size;
};

namespace {

/// Sort so that widest rectangles are first, then tallest.
bool ref_compare(const Packing::RectRef &x, const Packing::RectRef &y) {
    if (x.size[0] != y.size[0]) {
        return x.size[0] > y.size[0];
    } else if (x.size[1] != y.size[1]) {
        return x.size[1] > y.size[1];
    } else {
        return false;
    }
}

class AreaSequence {
private:
    int m_min[2], m_max[2];
    int m_area;
    int m_index;
    int m_count;
    I16Vec2 m_rects[32];

public:
    AreaSequence(IVec2 min_size, IVec2 max_size, std::size_t min_area);
    IVec2 next();
};

/// Get the ceiling of the base 2 logarithm of a number.
int ilog2(unsigned x) {
    int i = 0;
    while (i < 31 && x > (1u << i)) {
        i++;
    }
    return i;
}

AreaSequence::AreaSequence(IVec2 min_size, IVec2 max_size,
                           std::size_t min_area)
    : m_min{ilog2(min_size[0]), ilog2(min_size[1])},
      m_max{ilog2(max_size[0] + 1) - 1, ilog2(max_size[1] + 1) - 1},
      m_area(std::max(m_min[0] + m_min[1], ilog2(min_area))),
      m_index(0),
      m_count(0) {
}

IVec2 AreaSequence::next() {
    if (m_index < m_count) {
        return static_cast<IVec2>(m_rects[m_index++]);
    }
    int min = std::max(m_min[0], m_area - m_max[1]);
    int max = std::min(m_max[0], m_area - m_min[1]);
    int n = 0;
    if ((m_area & 1) == 0) {
        int i = m_area >> 1;
        if (i >= min && i <= max) {
            short d = 1 << i;
            m_rects[n++] = I16Vec2{{d, d}};
        }
    }
    for (int i = (m_area - 1) >> 1, j = m_area - i;
         i >= min || j <= max; i--, j++) {
        short d1 = 1 << i, d2 = 1 << j;
        if (j >= min && j <= max) {
            m_rects[n++] = I16Vec2{{d2, d1}};
        }
        if (i >= min && i <= max) {
            m_rects[n++] = I16Vec2{{d1, d2}};
        }
    }
    if (!n) {
        return IVec2::zero();
    }
    m_area++;
    m_index = 1;
    m_count = n;
    return static_cast<IVec2>(m_rects[0]);
}

}

Packing::Packing()
    : m_nodes(),
      m_size(IVec2::zero()) {
}

bool Packing::pack(Rect *rects, std::size_t count) {
    if (!count) {
        return true;
    }
    std::vector<RectRef> ref;
    ref.reserve(count);
    for (std::size_t i = 0; i < count; i++) {
        auto sz = rects[i].size;
        if (sz[0] <= 0 || sz[1] <= 0) {
            rects[i].loc = I16Vec2::zero();
            continue;
        }
        ref.push_back(RectRef{static_cast<unsigned>(i), sz});
    }
    if (ref.empty()) {
        return true;
    }
    std::sort(std::begin(ref), std::end(ref), ref_compare);
    return pack_impl(rects, ref.data(), ref.size());
}

bool Packing::auto_pack(Rect *rects, std::size_t count,
                        IVec2 min_size, IVec2 max_size) {
    if (!count) {
        reset(min_size);
        return true;
    }
    std::vector<RectRef> ref;
    ref.reserve(count);
    std::size_t rect_area = 0;
    for (std::size_t i = 0; i < count; i++) {
        auto sz = rects[i].size;
        if (sz[0] <= 0 || sz[1] <= 0) {
            rects[i].loc = I16Vec2::zero();
            continue;
        }
        rect_area += (static_cast<std::size_t>(sz[0]) *
                      static_cast<std::size_t>(sz[1]));
        min_size[0] = std::max<int>(min_size[0], sz[0]);
        min_size[1] = std::max<int>(min_size[1], sz[1]);
        ref.push_back(RectRef{static_cast<unsigned>(i), sz});
    }
    if (ref.empty()) {
        reset(min_size);
        return true;
    }
    std::sort(std::begin(ref), std::end(ref), ref_compare);

    AreaSequence seq(min_size, max_size, rect_area);
    while (true) {
        IVec2 size = seq.next();
        if (!size[0]) {
            reset(IVec2::zero());
            return false;
        }
        reset(size);
        if (pack_impl(rects, ref.data(), ref.size())) {
            return true;
        }
    }
}

void Packing::reset(IVec2 size) {
    m_nodes.clear();
    if (size[0] > 0) {
        m_nodes.push_back({{static_cast<short>(size[0]), 0}});
    }
    m_size = size;
}

bool Packing::pack_impl(Rect *rects, const RectRef *refs, std::size_t count) {
    for (std::size_t i = 0; i < count; i++) {
        int size_x = refs[i].size[0], size_y = refs[i].size[1];
        int index = refs[i].index;
        std::vector<I16Vec2>::iterator
            first = std::begin(m_nodes), last = std::end(m_nodes),
            best_first = last, best_last = last;
        int best_x = -1, best_y = std::numeric_limits<int>::max();
        int x = 0, next_x, max_x = m_size[0] - size_x;
        for (auto p = first; p != last && x <= max_x; p++, x = next_x) {
            next_x = (*p)[0];
            int y = (*p)[1];
            if (y >= best_y) {
                continue;
            }
            auto q = p;
            if (next_x < x + size_x) {
                bool success = false;
                for (q++; q != last; q++) {
                    int qx = (*q)[0];
                    int qy = (*q)[1];
                    if (qy > y) {
                        if (qy >= best_y) {
                            break;
                        }
                        y = qy;
                    }
                    if (qx >= x + size_x) {
                        success = true;
                        break;
                    }
                }
                if (!success) {
                    continue;
                }
            }
            best_first = p;
            best_last = q;
            best_x = x;
            best_y = y;
        }
        if (best_x < 0 || best_y + size_y > m_size[1]) {
            return false;
        }
        if (best_first != first) {
            auto prev = best_first - 1;
            if ((*prev)[1] == best_y + size_y) {
                best_first = prev;
            }
        }
        if ((*best_last)[0] == best_x + size_x) {
            best_last++;
        }
        if (best_last != last && (*best_last)[1] == best_y + size_y) {
            m_nodes.erase(best_first, best_last);
        } else {
            I16Vec2 node{{static_cast<short>(best_x + size_x),
                          static_cast<short>(best_y + size_y)}};
            if (best_first == best_last) {
                m_nodes.insert(best_first, node);
            } else {
                *best_first = node;
                best_first++;
                m_nodes.erase(best_first, best_last);
            }
        }
        rects[index].loc = I16Vec2{{static_cast<short>(best_x),
                                    static_cast<short>(best_y)}};
    }
    return true;
}
