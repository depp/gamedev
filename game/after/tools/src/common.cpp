#include "defs.hpp"

#include <cstdlib>
#include <iostream>

void die_func(const char *file, int line, const char *msg) {
    std::cerr << file << ':' << line << ": error: " << msg << '\n';
    std::exit(1);
}
