#include "defs.hpp"

#include <cstring>
#include <fstream>

GrayImage::GrayImage()
    : m_size{{0, 0}} {
}

GrayImage::GrayImage(IVec2 size)
    : m_data(size[0] * size[1], 0),
      m_size(size) {
}

GrayImage::GrayImage(IVec2 size, const void *data, int rowbytes)
    : m_data(size[0] * size[1], 0),
      m_size(size) {
    for (int y = 0; y < m_size[1]; y++) {
        std::memcpy(
            m_data.data() + m_size[0] * y,
            reinterpret_cast<const char *>(data) + rowbytes * y,
            m_size[0]);
    }
}

void GrayImage::save(const std::string &path) const {
    std::ofstream fp(path);
    fp << "P2\n";
    fp << m_size[0] << ' ' << m_size[1] << '\n';
    fp << "255\n";
    int pos = 0;
    for (unsigned char c : m_data) {
        if (pos == 15) {
            fp << '\n';
            pos = 0;
        } else if (pos > 0) {
            fp << ' ';
        }
        fp << static_cast<int>(c);
        pos++;
    }
    fp << '\n';
}

void GrayImage::paste(const GrayImage &other, IVec2 pos) {
    for (int i = 0; i < 2; i++) {
        if (pos[i] < 0 || pos[i] > m_size[i] ||
            pos[i] + other.m_size[i] > m_size[i]) {
            die("Invalid paste location");
        }
    }
    for (int y = 0; y < other.m_size[1]; y++) {
        std::memcpy(
            m_data.data() + m_size[0] * (y + pos[1]) + pos[0],
            other.m_data.data() + other.m_size[0] * y,
            other.m_size[0]);
    }
};
