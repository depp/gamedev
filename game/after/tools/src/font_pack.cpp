#include "defs.hpp"

#include <iostream>

#include <ft2build.h>
#include FT_FREETYPE_H

#define FT(x)                                   \
    do {                                        \
        FT_Error err = x;                       \
        if (err) {                              \
            die_ft(__FILE__, __LINE__, err);    \
        }                                       \
    } while (0)

struct Glyph {
    int cx, cy, advance;
    GrayImage image;
};

struct FontMetrics {
    int ascender;
    int descender;
    int height;
};

int main(int argc, char **argv) {
    if (argc != 3) {
        die("Usage: MARGIN OUT.pnm");
    }

    FT_Library lib;
    FT(FT_Init_FreeType(&lib));

    std::vector<FontMetrics> metrics;
    FT_Face face = nullptr;
    std::vector<Glyph> glyphs;
    std::string line;
    int margin = std::stoi(argv[1]);
    std::vector<Packing::Rect> rects;
    while (std::cin) {
        std::getline(std::cin, line, '\n');
        if (line.empty()) {
            continue;
        }
        switch (line[0]) {
        case '@':
            if (face) {
                FT(FT_Done_Face(face));
                face = nullptr;
            }
            FT(FT_New_Face(lib, line.c_str() + 1, 0, &face));
            break;

        case '$': {
            if (!face) {
                die("No font.");
            }
            line.erase(std::begin(line), std::begin(line) + 1);
            double size = std::stod(line);
            int isize = static_cast<int>(size * 64.0 + 0.5);
            FT(FT_Set_Char_Size(face, 0, isize, 72, 72));
            const auto &m = face->size->metrics;
            metrics.push_back(FontMetrics{
                static_cast<int>((m.ascender + 32) >> 6),
                static_cast<int>((m.descender + 32) >> 6),
                static_cast<int>((m.height + 32) >> 6),
            });
        }
            break;

        default: {
            if (!face) {
                die("No font.");
            }
            int codepoint = std::stoi(line);
            FT(FT_Load_Char(face, codepoint, FT_LOAD_RENDER));
            auto slot = face->glyph;
            glyphs.push_back(Glyph{
                static_cast<int>(slot->bitmap_left),
                static_cast<int>(slot->bitmap_top - slot->bitmap.rows),
                static_cast<int>((slot->advance.x + 32) >> 6),
                GrayImage(
                    IVec2{{slot->bitmap.width, slot->bitmap.rows}},
                    slot->bitmap.buffer,
                    slot->bitmap.pitch)
            });
            rects.push_back(Packing::Rect{
                {{static_cast<short>(slot->bitmap.width + margin),
                  static_cast<short>(slot->bitmap.rows + margin)}},
                {{0, 0}}
            });
        }
            break;
        }
    }

    Packing pack;
    IVec2 min_size{{8, 8}}, max_size{{2048, 2048}};
    if (!pack.auto_pack(rects.data(), rects.size(), min_size, max_size)) {
        die("Packing failed");
    }

    GrayImage pimg(pack.size());
    std::cout << pack.size()[0] << ' ' << pack.size()[1] << '\n';
    for (const auto &m : metrics) {
        std::cout << m.ascender << ' ' << m.descender << ' '
                  << m.height << '\n';
    }
    for (std::size_t i = 0, n = glyphs.size(); i < n; i++) {
        const auto &g = glyphs[i];
        const auto &r = rects[i];
        auto size = g.image.size();
        std::cout << r.loc[0] << ' ' << r.loc[1] << ' '
                  << size[0] << ' ' << size[1] << ' '
                  << g.cx << ' ' << g.cy << ' ' << g.advance << '\n';
        pimg.paste(g.image, static_cast<IVec2>(r.loc));
    }
    pimg.save(argv[2]);

    return 0;
}
