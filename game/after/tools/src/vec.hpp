/* Copyright 2014-2015 Dietrich Epp.  */
#include <cmath>
#include <type_traits>

/// Integer vector.
template<typename T, int N>
struct GVec {
    static_assert(std::is_integral<T>::value ||
                  std::is_floating_point<T>::value,
                  "Vector must be integer or float.");
    static_assert(N >= 1 && N <= 4,
                  "Vector size must be between 1 and 4.");

    T v[N];

    T operator[](int i) const { return v[i]; }
    T &operator[](int i) { return v[i]; }

    template<typename U>
    explicit operator GVec<U, N>() const;

    /// The zero vector.
    static GVec zero();
};

template<typename T, int N>
T length2(GVec<T, N> v) {
    T a = 0;
    for (int i = 0; i < N; i++)
        a += v[i] * v[i];
    return a;
}

template<typename T, int N>
T length(GVec<T, N> v) {
    static_assert(std::is_floating_point<T>::value,
                  "Must be floating point.");
    return std::sqrt(length2(v));
}

template<typename T, int N>
T distance2(GVec<T, N> u, GVec<T, N> v) {
    return length2(u - v);
}

template<typename T, int N>
T distance(GVec<T, N> u, GVec<T, N> v) {
    return length(u - v);
}

template<typename T, int N>
T dot(GVec<T, N> u, GVec<T, N> v) {
    T a = 0;
    for (int i = 0; i < N; i++)
        a += u.v[i] * v.v[i];
    return a;
}

template<typename T, int N>
T mix(GVec<T, N> u, GVec<T, N> v, T a) {
    static_assert(std::is_floating_point<T>::value,
                  "Must be floating point.");
    return u + (v - u) * a;
}

template<typename T, int N>
T normalize(GVec<T, N> v) {
    return v * (1 / length(v));
}

template<typename T, int N>
template<typename U>
GVec<T, N>::operator GVec<U, N>() const {
    GVec<U, N> r;
    for (int i = 0; i < N; i++)
        r.v[i] = static_cast<U>(v[i]);
    return r;
}

template<typename T, int N>
bool operator==(GVec<T, N> u, GVec<T, N> v) {
    for (int i = 0; i < N; i++)
        if (u[i] != v[i])
            return false;
    return true;
}

template<typename T, int N>
bool operator!=(GVec<T, N> u, GVec<T, N> v) {
    return !(u == v);
}

template<typename T, int N>
GVec<T, N> operator+(GVec<T, N> u) {
    return u;
}

template<typename T, int N>
GVec<T, N> operator+(GVec<T, N> u, GVec<T, N> v) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++)
        r[i] = u[i] + v[i];
    return r;
}

template<typename T, int N>
GVec<T, N> &operator+=(GVec<T, N> &u, GVec<T, N> v) {
    for (int i = 0; i < N; i++)
        u[i] += v[i];
    return u;
}

template<typename T, int N>
GVec<T, N> operator-(GVec<T, N> v) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++)
        r[i] = -v[i];
    return r;
}

template<typename T, int N>
GVec<T, N> operator-(GVec<T, N> u, GVec<T, N> v) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++)
        r[i] = u[i] - v[i];
    return r;
}

template<typename T, int N>
GVec<T, N> &operator-=(GVec<T, N> &u, GVec<T, N> v) {
    for (int i = 0; i < N; i++)
        u[i] -= v[i];
    return u;
}

template<typename T, int N>
GVec<T, N> operator*(T a, GVec<T, N> v) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++)
        r[i] = a * v[i];
    return r;
}

template<typename T, int N>
GVec<T, N> operator*(GVec<T, N> v, T a) {
    GVec<T, N> r;
    for (int i = 0; i < N; i++)
        r[i] = v[i] * a;
    return r;
}

template<typename T, int N>
GVec<T, N> &operator*=(GVec<T, N> &u, T a) {
    for (int i = 0; i < N; i++)
        u[i] *= a;
    return u;
}

template<typename T, int N>
GVec<T, N> GVec<T, N>::zero() {
    GVec<T, N> r;
    for (int i = 0; i < N; i++)
        r[i] = static_cast<T>(0);
    return r;
}

typedef GVec<float, 2> Vec2;
typedef GVec<float, 3> Vec3;
typedef GVec<int, 2> IVec2;
typedef GVec<int, 3> IVec3;
typedef GVec<short, 2> I16Vec2;
typedef GVec<short, 3> I16Vec3;
