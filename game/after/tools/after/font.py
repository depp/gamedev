import subprocess
import os
import json
import yaml
import io
import sys
from . import path

def parse_charset(charset):
    chars = set()
    for crange in charset:
        if isinstance(crange, str):
            chars.update(crange)
        else:
            lo, hi = crange
            chars.update(chr(n) for n in range(lo, hi+1))
    return chars

def run():
    fpath = path.project_path('tools/fonts');
    with open(os.path.join(fpath, 'spec.yaml')) as fp:
        data = yaml.safe_load(fp)
    chars = sorted(parse_charset(data['characters']))
    size = data['size']
    margin = data['margin']

    cmd = io.StringIO()
    fonts = sorted(data['fonts'].keys())
    for name in fonts:
        print('@' + os.path.join(fpath, data['fonts'][name]), file=cmd)
        print('${:d}'.format(size), file=cmd)
        for char in chars:
            print(ord(char), file=cmd)

    # print(cmd.getvalue())
    proc = subprocess.Popen(
        [path.project_path('tools/src/font_pack'),
         str(margin),
         os.path.join(fpath, 'glyphs.pnm')],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE);
    stdout, stderr = proc.communicate(cmd.getvalue().encode('ASCII'))
    # print(stdout)
    if proc.returncode != 0:
        print('Font packing failed', file=sys.stderr)
        raise SystemExit(1)

    lines = stdout.splitlines()
    assert len(lines) == (len(chars) + 1) * len(fonts) + 1

    size = tuple(int(x) for x in lines[0].split())
    assert len(size) == 2
    metrics = [[int(x) for x in line.split()]
               for line in lines[1:len(fonts)+1]]
    assert all(len(x) == 3 for x in metrics)
    lines = lines[len(fonts)+1:]

    dfonts = {}
    data = {'sx': size[0], 'sy': size[1], 'fonts': dfonts}
    for n, name in enumerate(fonts):
        ascender, descender, height = metrics[n]
        fglyphs = {}
        fdata = {
            'ascender': ascender,
            'descender': descender,
            'height': height,
            'glyphs': fglyphs
        }
        dfonts[name] = fdata
        for char, line in zip(chars, lines[n*len(chars):]):
            gdata = [int(x) for x in line.split()]
            assert len(gdata) == 7
            fglyphs[char] = gdata
    with open(os.path.join(fpath, 'font.json'), 'w') as fp:
        json.dump(data, fp, separators=(',', ':'))

    for fname in ['glyphs.png', 'temp.png']:
        try:
            os.unlink(os.path.join(fpath, fname))
        except FileNotFoundError:
            pass
    subprocess.check_call(
        ['gm', 'convert', 'glyphs.pnm', 'glyphs.png'],
        cwd=fpath)
    subprocess.check_call(
        ['pngcrush', '-rem', 'alla', 'glyphs.png', 'temp.png'],
        cwd=fpath)
    os.replace(os.path.join(fpath, 'temp.png'),
               os.path.join(fpath, 'glyphs.png'));

if __name__ == '__main__':
    run()
