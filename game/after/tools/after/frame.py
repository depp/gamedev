import collections
import numpy
from PIL import Image

FrameID = collections.namedtuple('FrameID', 'name x y')
FrameData = collections.namedtuple('FrameData', 'cx cy image')

EMPTY_FRAME = FrameData(0, 0, Image.new('RGBA', (0, 0)))

def combine(frames):
    """Combine a list of FrameData objects into one."""
    if len(frames) == 1:
        return frames[0]
    if not frames:
        return EMPTY_FRAME
    x0 = min(-f.cx for f in frames)
    y0 = min(-f.cy for f in frames)
    x1 = max(f.image.size[0] - f.cx for f in frames)
    y1 = max(f.image.size[1] - f.cy for f in frames)
    img = Image.new('RGBA', (x1 - x0, y1 - y0))
    for f in frames:
        img.paste(f.image, (-x0 - f.cx, -y0 - f.cy), f.image)
    return FrameData(-x0, -y0, img)

def auto_crop(frame):
    """Auto-crop a frame, returning a new frame."""
    pixels = numpy.array(frame.image)[:,:,3] > 0
    xs = numpy.nonzero(numpy.max(pixels, axis=0))[0]
    ys = numpy.nonzero(numpy.max(pixels, axis=1))[0]
    if not len(xs) or not len(ys):
        return EMPTY_FRAME
    # We have to convert back to Python integers, lest we get some
    # message like '12' is not JSON serializable.  We absolutely LOVE
    # dynamic typing.
    x0 = int(xs[0])
    y0 = int(ys[0])
    x1 = int(xs[-1] + 1)
    y1 = int(ys[-1] + 1)
    return FrameData(
        int(frame.cx - x0),
        int(frame.cy - y0),
        frame.image.crop((x0, y0, x1, y1)))

def grid(name, image, *, offset=(0, 0), frame_size, grid_size):
    """Split a grid into frames."""
    nx, ny = grid_size
    sx, sy = frame_size
    x0, y0 = offset
    for x in range(nx):
        for y in range(ny):
            rect = x * sx, y * sy, (x + 1) * sx, (y + 1) * sy
            yield (
                FrameID(name, x + x0, y + y0),
                FrameData(sx // 2, sy // 2, image.crop(rect)))
