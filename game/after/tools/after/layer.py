import collections
from PIL import Image
import re
from . import error
from . import util
from . import walk
from . import frame
from . import imageutil

ORDER = [
    'body',
    'body/ears body/eyes body/nose',
    'feet',
    'legs',
    'hands',
    'torso',
    'belt',
    'formal',
    'torso/back',
    'accessories',
    'facial',
    'hair',
    'head',
    'weapons',
]
ORDER_INDEX = {
    part: n for n, parts in enumerate(ORDER)
    for part in parts.split()
}

def layer_order(name):
    """Get the layering order for a layer with the given name."""
    order = None
    for prefix in util.prefixes(name):
        try:
            order = ORDER_INDEX[prefix]
        except KeyError:
            pass
    return order

Anim = collections.namedtuple('Anim', 'ox oy sx sy')

FULL_ANIMS = {
    'spell':   Anim(0,  0,  7,  4),
    'thrust':  Anim(0,  4,  8,  4),
    'stand':   Anim(0,  8,  1,  4),
    'walk':    Anim(1,  8,  8,  4),
    'slash':   Anim(0, 12,  6,  4),
    'shoot':   Anim(0, 16, 13,  4),
    'hurt':    Anim(0, 20,  6,  1),
}

OVERSIZE_ANIMS = {
    'slash':   Anim(0,  0,  6,  4)
}

# Sizes of each animation
ALL_ANIMS = {
    'spell':   ( 7,  4),
    'thrust':  ( 8,  4),
    'stand':   ( 1,  4),
    'walk':    ( 8,  4),
    'slash':   ( 6,  4),
    'shoot':   (13,  4),
    'hurt':    ( 6,  1),
}

LayerType = collections.namedtuple(
    'LayerType', 'animations frame_size image_size')

LAYER_TYPES = {
    'full': LayerType(FULL_ANIMS, (64, 64), None),
    'oversize': LayerType(OVERSIZE_ANIMS, (192, 192), None),
}

def layer_size(ltype):
    """Get the image size for the given layer type."""
    size = ltype.image_size
    if size is None:
        fsize = ltype.frame_size
        anims = ltype.animations.values()
        size = (max(a.ox + a.sx for a in anims) * fsize[0],
                max(a.oy + a.sy for a in anims) * fsize[1])
    return size

class SpriteLayer(object):
    """A sprite layer."""
    __slots__ = ['path', 'layer_type']

    def __init__(self, path, layer_type):
        self.path = path
        self.layer_type = layer_type

    def get_frames(self, *, animations=None):
        """Iterate over all frames in the layer.

        Yields (frameID, frameData) pairs.
        """
        if animations is not None:
            animations = set(animations)
            extra = animations.difference(ALL_ANIMS)
            if extra:
                raise ValueError(
                    'Unknown animations: {0}'
                    .format(', '.join(extra)))
        ltype = LAYER_TYPES[self.layer_type]
        size = layer_size(ltype)
        img = Image.open(self.path)
        if img.size != size:
            raise error.DataError(
                '{0}: Image should be {1[0]}x{1[1]}, but is {2[0]}x{2[1]}'
                .format(self.path, size, img.size))
        img = imageutil.cleanup(img)
        sx, sy = ltype.frame_size
        for name, anim in ltype.animations.items():
            if animations is not None and anim not in animations:
                continue
            aimage = img.crop((
                anim.ox * sx, anim.oy * sy,
                (anim.ox + anim.sx) * sx, (anim.oy + anim.sy) * sy))
            if imageutil.image_empty(aimage):
                # print('No data for animation: {}'.format(name))
                continue
            yield from frame.grid(
                name, aimage,
                frame_size=(sx, sy),
                grid_size=(anim.sx, anim.sy))

WORD = re.compile(r'\w+')
NON_WORD = re.compile(r'[^A-Za-z0-9]+')

LayerName = collections.namedtuple('LayerName', 'name genders ltype')

def parse_name(name):
    """Parse a layer name."""
    parts = []
    gender = None
    ltype = None
    genders = util.GENDERS
    for part in name.lower().split('/'):
        if part in LAYER_TYPES:
            assert ltype is None
            ltype = part
            continue
        if part in genders:
            assert not gender or part == gender
            gender = part
            continue
        parts2 = []
        for part2 in NON_WORD.split(part):
            if not part2:
                continue
            if part2 in genders:
                assert not gender or part2 == gender
                gender = part2
                continue
            parts2.append(part2)
        if 'no' in parts2:
            parts2 = parts2[:parts2.index('no')]
        parts.append('_'.join(parts2))
    genders = (gender,) if gender is not None else genders
    return LayerName('/'.join(parts), genders, ltype or 'full')

class LayerSet(object):
    """A set of sprite layers."""
    __slots__ = ['genders']

    def __init__(self, path):
        all_files = {g: [] for g in util.GENDERS}
        for fpath, name in walk.walk_files(path, exts=['png']):
            name = parse_name(name)
            obj = SpriteLayer(fpath, name.ltype)
            for gender in name.genders:
                all_files[gender].append((name.name, obj))
        self.genders = {}
        for g in all_files:
            front = {}
            back = {}
            names = set()
            for name, obj in all_files[g]:
                if name in names:
                    print('Duplicate:')
                    print('  Gender:', gender)
                    print('  Name:', name.name)
                    raise SystemExit(1)
                names.add(name)
                if name.startswith('behind_body/'):
                    name = name[name.find('/')+1:]
                    back[name] = obj
                else:
                    front[name] = obj
            layers = {}
            used_bnames = set()
            for name, obj in front.items():
                obj2 = None
                for bname in util.suffixes(name):
                    try:
                        obj2 = back[bname]
                    except KeyError:
                        pass
                    else:
                        used_bnames.add(bname)
                        break
                layers[name] = obj, obj2
            for bname in back:
                if bname not in used_bnames:
                    print('Unused back layer:', bname)
            self.genders[g] = layers
        names = set()
        for layers in self.genders.values():
            names.update(layers)
        for name in names:
            if layer_order(name) is None:
                print('Unknown layer order:', name)

    def list(self, gender):
        """List all layers compatible with the given gender."""
        if gender is None:
            table = set()
            for layers in self.genders.values():
                table.update(layers)
            table = list(table)
        else:
            table = list(self.genders[gender])
        table.sort()
        print('Layers for gender {}: {}'.format(gender, len(table)))
        for item in table:
            print('  ' + item)

    def get(self, name, gender):
        """Get the layer with the given name for the given gender.

        Returns (front, back), or None.  Either front or back may be
        None.
        """
        if gender not in util.GENDERS:
            raise ValueError('Invalid gender')
        return self.genders[gender].get(name)

if __name__ == '__main__':
    assert layer_order('bogus') is None
    assert layer_order('body/light') == 0
    assert layer_order('body/ears/spock') == 1
