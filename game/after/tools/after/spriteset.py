import yaml
import os
from PIL import Image
from . import error
from . import frame
from . import layer
from . import util
from . import sheet
from . import path
from . import imageutil

# This should be relative to YAML file but I'm on a deadline
SPRITES_PATH = path.project_path('tools/sprites')

def _parse_dict(obj, pfx, data, *arg, **kw):
    if not isinstance(data, dict):
        raise error.DataError('Expected dictionary')
    for k, v in data.items():
        if not isinstance(k, str):
            raise error.DataError('Bad key')
        try:
            func = getattr(obj, pfx + k)
        except AttributeError:
            raise error.DataError('Unknown key: {!r}'.format(k))
        func(v, *arg, **kw)

def check_parts(data):
    if not isinstance(data, list):
        raise error.DataError('Parts must be a list')
    if not all(isinstance(x, str) for x in data):
        raise error.DataError('Invalid part, must be a string')
    last_order = None
    for part in data:
        order = layer.layer_order(part)
        if order is None:
            raise error.DataError('Unknown part: {!r}'.format(part))
        if last_order is not None and last_order > order:
            print('Warning: Part is out of order: {}'.format(part))
        last_order = order

class Animation(object):
    """A character animation."""
    __slots__ = ['anim', 'parts']

    def __init__(self, anim, parts):
        self.anim = anim
        self.parts = list(parts)

    def read(self, data):
        if data:
            _parse_dict(self, '_top_', data)
        if self.anim not in layer.ALL_ANIMS:
            raise error.DataError('Unknown animation: {!r}'.format(self.anim))

    def _top_anim(self, data):
        self.anim = anim

    def _top_parts(self, data):
        check_parts(data)
        parts = list(enumerate(self.parts))
        parts.extend(enumerate(data, len(parts)))
        parts.sort(key=lambda x: (layer.layer_order(x[1]), x[0]))
        self.parts = [part[1] for part in parts]

class Character(object):
    __slots__ = ['gender', 'parts', 'animations']

    def __init__(self):
        self.gender = None
        self.parts = []
        self.animations = {}

    def read(self, data):
        _parse_dict(self, '_top_', data)
        animations = self.animations
        self.animations = {}
        for k, v in animations.items():
            anim = Animation(k, self.parts)
            anim.read(v)
            self.animations[k] = anim

    def _top_gender(self, data):
        assert data in util.GENDERS
        self.gender = data

    def _top_parts(self, data):
        check_parts(data)
        self.parts.extend(data)

    def _top_animations(self, data):
        self.animations = data

    def write_sheet(self, ssheet, layers, cname):
        """Add all animations to a sprite sheet."""
        all_parts = set()
        for anim in self.animations.values():
            all_parts.update(anim.parts)
        part_data = {}
        for part in all_parts:
            lobjs = layers.get(part, self.gender)
            if lobjs is None:
                raise error.DataError(
                    'Missing layer: {} ({})'.format(part, self.gender))
            data = []
            for n, lobj in enumerate(lobjs):
                if lobj is None:
                    data.append(None)
                    continue
                data.append(dict(lobj.get_frames()))
            part_data[part] = tuple(data)
        for aname_out, animation in self.animations.items():
            aname_out = '{}.{}'.format(cname, aname_out)
            layers = []
            for part in animation.parts:
                front, back = part_data[part]
                if back is not None:
                    layers.insert(0, back)
                if front is not None:
                    layers.append(front)
            aname = animation.anim
            aw, ah = layer.ALL_ANIMS[aname]
            asheet = sheet.Animation(aw, ah)
            for x in range(aw):
                for y in range(ah):
                    fid = frame.FrameID(aname, x, y)
                    frames = []
                    for obj in layers:
                        f = obj.get(fid)
                        if f is not None:
                            frames.append(f)
                    if not frames:
                        raise error.DataError('Frame is empty')
                    asheet[x, y] = frame.combine(frames)
            ssheet.add(aname_out, asheet)

class Grid(object):
    __slots__ = ['framesize', 'gridsize', 'image']

    def __init__(self):
        self.framesize = None
        self.gridsize = None
        self.image = None

    def read(self, data):
        _parse_dict(self, '_top_', data)
        assert self.framesize and self.gridsize and self.image

    def _top_framesize(self, data):
        assert len(data) == 2
        self.framesize = tuple(data)

    def _top_gridsize(self, data):
        assert len(data) == 2
        self.gridsize = tuple(data)

    def _top_image(self, data):
        assert isinstance(data, str)
        self.image = os.path.join(SPRITES_PATH, data)

    def write_sheet(self, ssheet, layers, cname):
        """Add all frames to a sprite sheet."""
        image = imageutil.cleanup(Image.open(self.image))
        asheet = sheet.Animation(*self.gridsize)
        grid = frame.grid(
            None, image,
            frame_size=self.framesize,
            grid_size=self.gridsize)
        for fid, fr in grid:
            asheet[fid.x, fid.y] = fr
        ssheet.add(cname, asheet)

TYPES = {
    'character': Character,
    'grid': Grid
}

class SpriteSet(object):
    """A set of sprites."""
    __slots__ = ['characters', 'margin']

    def __init__(self):
        self.characters = {}
        self.margin = 0

    def _top_sprites(self, data):
        for k, v in data.items():
            if not isinstance(v, dict):
                raise error.DataError('sprite must be be a dictionary')
            stype = v['type']
            class_ = TYPES[stype]
            del v['type']
            obj = class_()
            obj.read(v)
            self.characters[k] = obj

    def _top_margin(self, data):
        if not isinstance(data, int) and data >= 0:
            raise error.DataError('Invalid margin')
        self.margin = data

    def read(self, path):

        with open(path) as fp:
            data = yaml.safe_load(fp)
        _parse_dict(self, '_top_', data)

    def write_sheet(self, ssheet, layers):
        """Add all animations to a sprite sheet."""
        for name, obj in self.characters.items():
            obj.write_sheet(ssheet, layers, name)
