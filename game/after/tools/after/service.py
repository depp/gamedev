"""Game service implementation.

This contains the web API for level editing.
"""
import cherrypy
import os
import re
import json
from . import path

NAME = re.compile('[-_a-zA-Z0-9]+$')

def level_name(name):
    """Get the level name from the name parameter."""
    base, ext = os.path.splitext(name)
    if ext != '.json' or not NAME.match(base):
        raise cherrypy.HTTPError(404)
    return base

class ServiceInfo(object):
    __slots__ = []
    exposed = True

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def GET(self):
        return {'levels':{'uri':'/level','writable':True}}

class Levels(object):
    __slots__ = ['path']

    def __init__(self):
        p = path.project_path('levels')
        if not os.path.isdir(p):
            os.mkdir(p)
        print('Levels in {}'.format(p))
        self.path = p

    exposed = True

    @cherrypy.expose
    def GET(self, name=None):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        if name is None:
            levels = []
            for fname in os.listdir(self.path):
                base, ext = os.path.splitext(fname)
                if ext != '.json' or not NAME.match(base):
                    continue
                levels.append(base)
            return json.dumps(levels).encode('ASCII')
        name = level_name(name)
        try:
            with open(os.path.join(self.path, name + '.json'), 'rb') as fp:
                data = fp.read()
            return data
        except FileNotFoundError:
            raise cherrypy.HTTPError(404)

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def PUT(self, name):
        print('put', repr(name))
        name = level_name(name)
        data = cherrypy.request.json
        tmppath = os.path.join(self.path, '{}.json.tmp'.format(name))
        outpath = os.path.join(self.path, '{}.json'.format(name))
        with open(tmppath, 'w') as fp:
            json.dump(data, fp, sort_keys=True, indent=2)
        os.replace(tmppath, outpath)
        cherrypy.response.status = 204

def app():
    cfg = {'/': {'request.dispatch': cherrypy.dispatch.MethodDispatcher()}}
    cherrypy.tree.mount(ServiceInfo(), '/services.json', cfg)
    cherrypy.tree.mount(Levels(), '/level', cfg)
    return cherrypy.tree
