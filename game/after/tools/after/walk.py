import os

def walk_files(path, *, exts=None):
    """Walk over files below a given directory."""
    if exts is not None:
        exts = frozenset(exts)
    for dirpath, dirnames, filenames in os.walk(path):
        dirnames[:] = [
            x for x in dirnames
            if not x.startswith('.') and not x.startswith('_')]
        for filename in filenames:
            if filename.startswith('.'):
                continue
            if exts is not None:
                ext = os.path.splitext(filename)[1]
                if not ext or ext[1:].lower() not in exts:
                    continue
            abspath = os.path.join(dirpath, filename)
            relpath = os.path.relpath(abspath, path).split(os.path.sep)
            relpath[-1] = os.path.splitext(relpath[-1])[0]
            yield abspath, '/'.join(relpath)
