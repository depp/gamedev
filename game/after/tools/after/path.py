import os

def project_path(path):
    """Get a path relative to the project root."""
    r = os.path.abspath(__file__)
    for i in range(3):
        r = os.path.dirname(r)
    return os.path.join(r, path)
