import numpy
from PIL import Image

def image_empty(img):
    """Test whether an image is empty."""
    return not numpy.any(numpy.array(img)[:,:,3] > 250)

def count_bad_alpha(img):
    arr = numpy.array(img)
    carr = numpy.logical_and(arr[:,:,3] != 0, arr[:,:,3] != 255)
    return numpy.sum(carr)

def cleanup(img):
    """Clean up an image's alpha channel."""
    arr = numpy.array(img.convert('RGBA'))
    pixels = arr[:,:,3] < 127
    arr[pixels] = 0
    arr[numpy.logical_not(pixels),3] = 255
    return Image.fromarray(arr)
