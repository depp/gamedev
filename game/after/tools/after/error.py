class DataError(Exception):
    """An error in the data files."""
    pass
