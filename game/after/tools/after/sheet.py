import io
import subprocess
import os
from PIL import Image
import json
from . import error
from . import frame

class Animation(object):
    """An animation."""
    __slots__ = ['sx', 'sy', 'frames']

    def __init__(self, sx, sy):
        self.sx = sx
        self.sy = sy
        self.frames = [None] * (sx * sy)

    def index(self, index):
        """Get the index for the given frame, (x, y)."""
        x, y = index
        if not (0 <= x < self.sx) or not (0 <= y < self.sy):
            raise KeyError('Invalid frame')
        return x * self.sy + y

    def __getitem__(self, index):
        return self.frames[self.index(index)]

    def __setitem__(self, index, value):
        self.frames[self.index(index)] = value

class SpriteSheet(object):
    """A sprite sheet."""
    __slots__ = ['animations']

    def __init__(self):
        self.animations = {}

    def add(self, name, animation):
        """Add an animation to the sprite sheet."""
        if name in self.animations:
            raise error.DataError('Duplicate animation: {}'.format(name))
        self.animations[name] = animation

    def write(self, path, *, margin=0):
        """Pack a sprite sheet and write it to disk.

        Writes a JSON file and a PNG file.
        """
        frames = []
        anims = {}
        for name, anim in self.animations.items():
            aframes = [-1] * len(anim.frames)
            anims[name] = aframes
            for n, fr in enumerate(anim.frames):
                if fr is None:
                    continue
                aframes[n] = len(frames)
                frames.append(frame.auto_crop(fr))
        fp = io.StringIO()
        for fr in frames:
            sx, sy = fr.image.size
            print(sx + margin, sy + margin, file=fp)
        pardir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        proc = subprocess.Popen(
            [os.path.join(pardir, 'src/rect_pack')],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE)
        stdout, stderr = proc.communicate(fp.getvalue().encode('ASCII'))
        del fp
        if proc.returncode != 0:
            print('ERROR: Packing failed')
            raise SystemExit(1)
        lines = stdout.splitlines()
        if len(lines) != len(frames) + 1:
            print('ERROR: Incorrect number of lines in output')
            raise SystemExit(1)
        locs = []
        for line in lines:
            loc = [int(x) for x in line.split()]
            assert len(loc) == 2
            locs.append(tuple(loc))
        del lines
        img_size = locs[0]
        locs = locs[1:]
        img = Image.new('RGBA', img_size)
        for fr, loc in zip(frames, locs):
            img.paste(fr.image, loc)
        img.save(path + '.png')

        adatas = {}
        for name, anim in self.animations.items():
            fdata = []
            adata = {'x': anim.sx, 'y': anim.sy, 'frame': fdata}
            adatas[name] = adata
            for idx in anims[name]:
                if idx < 0:
                    fdata.extend((0, 0, 0, 0, 0, 0))
                else:
                    fr = frames[idx]
                    fdata.extend(locs[idx])
                    fdata.extend(fr.image.size)
                    fdata.extend((fr.cx, fr.cy))

        info = {
            'x': img_size[0],
            'y': img_size[1],
            'anim': adatas
        }
        with open(path + '.json', 'w') as fp:
            json.dump(info, fp, separators=(',', ':'))
