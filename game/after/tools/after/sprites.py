import argparse
from . import layer
from . import spriteset
from . import sheet

def main():
    p = argparse.ArgumentParser()
    p.add_argument('--lpc-dir', help='LPC sprite directory', required=True)
    p.add_argument('--out', help='output path')
    p.add_argument('--list-female', action='store_true',
                   help='List all female layers.')
    p.add_argument('--list-male', action='store_true',
                   help='List all male layers.')
    p.add_argument('--list-all', action='store_true',
                   help='List all layers.')
    p.add_argument('specs', nargs='*',
                   help='Sprite specifications')

    args = p.parse_args();

    layers = layer.LayerSet(args.lpc_dir)
    if args.list_female:
        layers.list('female')
    if args.list_male:
        layers.list('male')
    if args.list_all:
        layers.list(None)

    for specs in args.specs:
        print('Reading:', specs)
        sprites = spriteset.SpriteSet()
        sprites.read(specs)
        print('Reading images...')
        ssheet = sheet.SpriteSheet()
        sprites.write_sheet(ssheet, layers)
        if args.out is not None:
            print('Writing:', args.out)
            ssheet.write(args.out, margin=sprites.margin)

if __name__ == '__main__':
    main()
