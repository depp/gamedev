"""GLSL shader processor."""
import argparse
import io
import json
import os
import re
import sys
from . import path

DECL = re.compile(
    r'\s*(attribute|uniform)\s+'
    r'(?:.*\s+)?'
    r'(\w+)'
    r'\s*(?:\[[^]]*\]\s*)?;'
);

EXTS = {'.vert', '.frag'}

def run():
    p = argparse.ArgumentParser()
    p.add_argument('--release', action='store_true',
                   help='Minimize output for release')
    args = p.parse_args()

    root = path.project_path('shader')
    shaders = {}
    for fname in os.listdir(root):
        if fname.startswith('.'):
            continue
        base, ext = os.path.splitext(fname)
        if ext not in EXTS:
            continue
        with open(os.path.join(root, fname)) as fp:
            shaders.setdefault(base, {})[ext[1:]] = fp.read()

    sys.stdout.write(HEADER)
    for name in sorted(shaders.keys()):
        data = shaders[name]
        uniforms = []
        attributes = []
        parts = {}
        for stype in data:
            lines = []
            for line in data[stype].splitlines():
                line = line.strip()
                if not line:
                    continue
                lines.append(line)
                m = DECL.match(line)
                if not m:
                    continue
                if m.group(1) == 'attribute':
                    attributes.append(m.group(2))
                elif m.group(1) == 'uniform':
                    uniforms.append(m.group(2))
            parts[stype] = '\n'.join(lines) if args.release else data[stype]
        uniforms.sort()
        sys.stdout.write(SHADER.format(
            name=name.title(),
            uniform_defs=''.join(
                '    {}: WebGLUniformLocation;\n'.format(u)
                for u in uniforms),
            parts={k: json.dumps(v) for k, v in parts.items()},
            attribute=json.dumps(' '.join(attributes)),
            uniform=json.dumps(' '.join(uniforms))))

HEADER = '''\
'use strict';
var shader = require('./shader-load');
'''

SHADER = '''\
module.exports.{name} = function(gl) {{
    return shader.loadProgram(gl, {{
        name: "{name}",
        vert: {parts[vert]},
        frag: {parts[frag]},
        attribute: {attribute},
        uniform: {uniform}
    }});
}};
'''

if __name__ == '__main__':
    run()
