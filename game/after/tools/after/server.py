"""Game development server.

Running this module will allow you to play the game and edit levels.
"""
import argparse
import calendar
import io
import math
import os
import random
import re
import threading
import time
import wsgiref
import wsgiref.simple_server
import wsgiref.util
import yaml
from . import path
from . import service

PLAIN_TEXT = ('Content-Type', 'text/plain;charset=UTF-8')

def error_not_found(environ, start_response):
    start_response('404 Not Found', [PLAIN_TEXT])
    body = '404 Not Found: {!r}'.format(environ['PATH_INFO'])
    return [body.encode('UTF-8')]

def error_method_not_allowed(environ, start_response):
    allow = ('Allow', 'GET, HEAD')
    start_response('405 Method Not Allowed', [PLAIN_TEXT, allow])
    body = '405 Method Not Allowed: {!r}'.format(environ['REQUEST_METHOD'])
    return [body.encode('UTF-8')]

def uniform_chunks(chunks, size):
    """Break a stream of chunks into chunks of uniform size."""
    data = io.BytesIO()
    for chunk in chunks:
        data.write(chunk)
        if data.tell() < size:
            continue
        data.seek(0)
        chunk = data.read(size)
        while len(chunk) >= size:
            yield chunk
            chunk = data.read(size)
        data.truncate(0)
        data.seek(0)
        data.write(chunk)
    data.seek(0)
    chunk = data.read(size)
    while chunk:
        yield chunk
        chunk = data.read(size)

class SlowWrapper(object):
    """WSGI wrapper which simulates a slower network connection."""
    __slots__ = ['app', 'lock', 'delay0', 'delay1', 'size1']

    def __init__(self, app, rate=56e3, delay=0.2):
        self.app = app
        self.lock = threading.Lock()
        self.delay0 = delay
        self.delay1 = 0.1
        self.size1 = max(1, round(rate / (8 * self.delay1)))
        if rate > 1e6:
            rate = '{:.1f} Mbit/s'.format(rate * 1e-6)
        else:
            rate = '{:.1f} Kbit/s'.format(rate * 1e-3)
        print('Throttling to {}'.format(rate))

    def __call__(self, environ, start_response):
        time.sleep((0.5 + random.random()) * self.delay0)
        chunk_iter = self.app(environ, start_response)
        for chunk in uniform_chunks(chunk_iter, self.size1):
            with self.lock:
                time.sleep(self.delay1)
                yield chunk

SAFE_COMPONENT = re.compile(r'^[-_A-Za-z0-9]+(?:\.[-_A-Za-z0-9]+)*$')
TIME_FORMAT = '%a, %d %b %Y %H:%M:%S GMT'

class FileApp(object):
    """An application which serves files."""
    __slots__ = ['path', 'mime']

    def __init__(self, path):
        self.path = path
        self.mime = {}
        mpath = os.path.join(os.path.dirname(__file__), 'mime.yaml')
        with open(mpath) as fp:
            mime = yaml.safe_load(fp)
        for k, v in mime.items():
            for kk in k.split():
                self.mime[kk] = v

    def __call__(self, environ, start_response):
        path = environ['PATH_INFO']
        if path.endswith('/'):
            path += 'index.html'
        if not path.startswith('/'):
            return error_not_found(environ, start_response)
        parts = path[1:].split('/')
        if not all(SAFE_COMPONENT.match(x) for x in parts):
            return error_not_found(environ, start_response)
        mime = self.mime_type(parts[-1])
        if mime is None:
            return error_not_found(environ, start_response)
        method = environ['REQUEST_METHOD']
        fullpath = os.path.join(self.path, path[1:])
        if method == 'HEAD':
            try:
                st = os.stat(fullpath)
            except OSError:
                return error_not_found(environ, start_response)
            start_response('200 Ok', self.headers(st, mime))
            return iter(())
        elif method == 'GET':
            try:
                fp = open(fullpath, 'rb')
                st = os.fstat(fp.fileno())
            except OSError:
                return error_not_found(environ, start_response)
            tsince = environ.get('HTTP_IF_MODIFIED_SINCE')
            if tsince:
                try:
                    tsince = calendar.timegm(
                        time.strptime(tsince, TIME_FORMAT))
                except ValueError:
                    pass
                else:
                    if tsince >= math.floor(st.st_mtime):
                        start_response('304 Not Modified', [])
                        return []
            start_response('200 Ok', self.headers(st, mime))
            return wsgiref.util.FileWrapper(fp)
        else:
            return error_method_not_allowed(environ, start_response)

    def headers(self, st, mime):
        """Get the HTTP headers for a file from its stat and mime type."""
        return [
            ('Content-Type', mime),
            ('Last-Modified',
             time.strftime(TIME_FORMAT, time.gmtime(st.st_mtime))),
            ('Content-Length', str(st.st_size)),
        ]

    def mime_type(self, fname):
        """Get the mime type for a filename."""
        i = fname.rfind('.')
        return self.mime.get(fname[i+1:]) if i >= 0 else None

def ancestors(path):
    """Iterate over all ancestors to a path, starting with itself."""
    pos = -1
    yield path
    while True:
        i = path.rfind('/', 0, pos)
        if i <= 0:
            break
        yield path[:i]
        pos = i

class Router(object):
    """An application which routes to CherryPy with fallback."""
    __slots__ = ['service', 'static']

    def __init__(self, service, static):
        self.service = service
        self.static = static

    def __call__(self, environ, start_response):
        for path in ancestors(environ['PATH_INFO']):
            if path in self.service.apps:
                app = self.service
                break
        else:
            app = self.static
        return app(environ, start_response)

SUFFIX = {'K': 1e3, 'M': 1e6}
def parse_rate(rate):
    factor = 1
    if rate:
        try:
            factor = SUFFIX[rate[-1]]
        except KeyError:
            raise ValueError()
        rate = rate[:-1]
    return factor * float(rate)

def run():
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument(
        '--host', '-H', default='',
        help='Host to listen on')
    p.add_argument(
        '--port', '-p', default=8000, type=int,
        help='Port to listen on')
    p.add_argument(
        '--slow', '-s', action='store_true',
        help='Emulate slow network connection')
    p.add_argument(
        '--rate', '-r', type=parse_rate,
        help='Set slow network speed (may use K or M suffix)')
    args = p.parse_args()

    app = Router(service.app(), FileApp(path.project_path('build/out')))
    if args.rate is not None:
        app = SlowWrapper(app, rate=args.rate)
    elif args.slow:
        app = SlowWrapper(app)

    auth = args.host or 'localhost'
    if args.port != 80:
        auth = '{}:{}'.format(auth, args.port)
    print('Serving on http://{}/'.format(auth))

    httpd = wsgiref.simple_server.make_server('', 8000, app)
    httpd.serve_forever()

if __name__ == '__main__':
    run()
