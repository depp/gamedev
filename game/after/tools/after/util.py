GENDERS = ('male', 'female')

def prefixes(name):
    """Iterate over all path prefixes for a hierarchical name."""
    pos = 0
    while True:
        i = name.find('/', pos)
        if i < 0:
            yield name
            break
        yield name[:i]
        pos = i + 1

def suffixes(name):
    """Iterate over all path suffixes for a hierarchical name."""
    yield name
    pos = 0
    while True:
        i = name.find('/', pos)
        if i < 0:
            break
        yield name[i+1:]
        pos = i + 1

if __name__ == '__main__':
    assert list(prefixes('abc')) == ['abc']
    assert list(prefixes('abc/def')) == ['abc', 'abc/def']
    assert list(prefixes('abc/def/ghi')) == ['abc', 'abc/def', 'abc/def/ghi']
    assert list(suffixes('abc')) == ['abc']
    assert list(suffixes('abc/def')) == ['abc/def', 'def']
    assert list(suffixes('abc/def/ghi')) == ['abc/def/ghi', 'def/ghi', 'ghi']
    print('Ok')
