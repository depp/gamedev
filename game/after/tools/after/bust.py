"""Cache buster."""
import argparse
import cgi
import hashlib
import json
import os
import re
import sys

def die(*arg):
    print('Error:', *arg, file=sys.stderr)
    raise SystemExit(1)

def cmd_update(args):
    try:
        with open(args.file_map) as fp:
            mapdata = json.load(fp)
    except FileNotFoundError:
        mapdata = {}

    for path in args.files:
        fname = os.path.basename(path)
        h = hashlib.new('sha256')
        with open(path, 'rb') as fp:
            while True:
                data = fp.read(64 * 1024)
                if not data:
                    break
                h.update(data)
        file_hash = h.hexdigest()
        new_name = '{0[0]}-{1}{0[1]}'.format(
            os.path.splitext(fname),
            h.hexdigest()[0:20])
        old_path = mapdata.get(fname)
        new_path = os.path.join(args.out_dir, new_name)
        dirname = os.path.dirname(new_path)
        if dirname:
            os.makedirs(dirname, exist_ok=True)
        try:
            os.remove(new_path)
        except FileNotFoundError:
            pass
        os.link(path, new_path)
        mapdata[fname] = new_name

    dirname = os.path.dirname(args.file_map)
    if dirname:
        os.makedirs(dirname, exist_ok=True)
    with open(args.file_map + '.tmp', 'w') as fp:
        json.dump(mapdata, fp, indent=2)
    os.replace(args.file_map + '.tmp', args.file_map)

def cmd_emit(args):
    with open(args.file_map) as fp:
        mapdata = json.load(fp)

    def emit_file(path):
        ext = os.path.splitext(path)[1]
        if ext == '.html':
            escape = cgi.escape
        elif ext == '.js':
            def escape(text):
                return text
        else:
            def escape(text):
                die('cannot escape for file type:', ext)

        def repl(m):
            cmd, arg = m.groups()
            if cmd == 'include':
                path = os.path.join(os.path.dirname(args.file_map), arg)
                text = emit_file(path)
            elif cmd == 'ref':
                try:
                    text = mapdata[arg]
                except KeyError:
                    die('unknown file:', repr(arg))
            else:
                die('unknown template:', repr(cmd))
            return escape(text)

        try:
            with open(path) as fp:
                data = fp.read()
        except FileNotFoundError:
            die('file not found:', repr(path))
        return re.sub(r'\{\{(\w+):([-./\w]*)\}\}', repl, data)

    sys.stdout.write(emit_file(args.template))

def run():
    p = argparse.ArgumentParser()

    ss = p.add_subparsers()
    ss.required = True
    ss.dest = 'cmd'

    pp = ss.add_parser('update', help='Update cache-busting files')
    pp.set_defaults(func=cmd_update)
    pp.add_argument('--file-map', required=True, help='The file map')
    pp.add_argument('--out-dir', required=True, help='Output directory')
    pp.add_argument('files', nargs='*', help='Files to update')

    pp = ss.add_parser('emit', help='Emit expanded file')
    pp.set_defaults(func=cmd_emit)
    pp.add_argument('file_map', help='The file map')
    pp.add_argument('template', help='The template file')

    args = p.parse_args()
    args.func(args)

if __name__ == '__main__':
    run()
