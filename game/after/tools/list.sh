#!/bin/sh
# usage: list.sh [female|male]
set -e
if test -z "${LPCDIR}" ; then
    LPCDIR="${HOME}/Downloads/Software/Source/Universal-LPC-spritesheet"
fi
exec python3 -m after.sprites --lpc-dir "${LPCDIR}" --list-"$1"
