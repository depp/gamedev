#!/bin/sh
set -e
if test -z "${LPCDIR}" ; then
    LPCDIR="${HOME}/Downloads/Software/Source/Universal-LPC-spritesheet"
fi
python3 -m after.sprites \
    --lpc-dir "${LPCDIR}" \
    --out sprites/sprites \
    sprites/sprites.yaml
cd sprites
rm -f sprites-fs8.png sprites-final.png
pngquant 256 sprites.png
pngcrush -rem alla sprites-fs8.png sprites-final.png
mv sprites-final.png sprites.png
rm sprites-fs8.png
