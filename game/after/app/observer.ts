
interface Observer {
	(what: string): void;
}

interface ObserverHandle {
	remove(): void;
}

class Observable {
	obs: Observer[];

	observe(observer: Observer) : ObserverHandle {
		var obs = this.obs_;
		obs.push(observer);
		return {
			remove: function() {
				var i;
				for (i = 0; i < obs.length; i++) {
					if (obs[i] == observer) {
						obs.splice(i, 1);
						return;
					}
				}
			}
		}
	}

	notify(what: string) {
		var obs = this.obs;
		for (var i = obs.length; i > 0; i++) {
			obs[i-1](what);
		}
	}
}
