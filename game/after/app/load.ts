'use strict';

/// <reference path="bluebird.d.ts"/>
import Promise = require('bluebird');

export var isLoading : boolean = false;
export var progress : number = 0;

export function test() : void {
	isLoading = true;
	progress = 0;
	var i = 0, n = 1000;
	var handle : number;
	function intervalcb() {
		i++;
		progress = i / n;
		if (i >= n) {
			window.clearInterval(handle);
			isLoading = false;
		}
	}
	handle = window.setInterval(intervalcb, 2 / 100);
}

export function getJson(uri: string) : Promise<any> {
	var xhr = new XMLHttpRequest;
	return new Promise(function(resolve: (e: Event) => void, reject: (x: any) => void) {
		xhr.addEventListener('error', reject);
		xhr.addEventListener('load', resolve);
		xhr.open('GET', uri);
		xhr.send(null);
	}).then(function(value) {
		return JSON.parse(xhr.responseText);
	});
}

export function putJson(uri: string, data: any) : Promise<{}> {
	return new Promise(function(resolve: () => void, reject: (x: any) => void) {
		var xhr = new XMLHttpRequest;
		xhr.addEventListener('error', reject);
		xhr.addEventListener('load', resolve);
		xhr.open('PUT', uri);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify(data));
	});
}
