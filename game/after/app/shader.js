'use strict';
var shader = require('./shader-load');
function getLevel(gl) {
    return shader.loadProgram(gl, {
        name: "Level",
        vert: "attribute vec4 Pos;\nattribute vec2 TexCoord;\nattribute float Highlight;\n\nvarying vec3 vShade;\nvarying vec4 vHighlight;\nvarying vec2 vTexCoord;\n\nuniform mat4 MVP;\nuniform vec4 Shades[6];\nuniform vec4 Highlights[16];\nuniform float Blink;\n\nvoid main() {\n    int shade = int(Pos.w);\n    vShade = Shades[shade].rgb;\n    int highlight = int(Highlight);\n    vHighlight = mix(\n        Highlights[highlight * 2],\n        Highlights[highlight * 2 + 1],\n        Blink);\n    vTexCoord = TexCoord * vec2(0.0078125, 0.015625);\n    gl_Position = MVP * vec4(Pos.xyz, 1.0);\n}\n",
        frag: "precision mediump float;\n\nvarying vec3 vShade;\nvarying vec4 vHighlight;\nvarying vec2 vTexCoord;\n\nuniform sampler2D Texture;\n\nvoid main() {\n    vec3 c = mix(\n        texture2D(Texture, vTexCoord).rgb * vShade,\n        vHighlight.rgb,\n        vHighlight.a);\n    gl_FragColor = vec4(c, 1.0);\n}\n",
        attribute: "Pos TexCoord Highlight",
        uniform: "Blink Highlights MVP Shades Texture"
    });
}
exports.getLevel = getLevel;
function getLoad(gl) {
    return shader.loadProgram(gl, {
        name: "Load",
        vert: "attribute vec3 Vert;\nvarying vec4 vColor;\nuniform vec4 Colors[4];\nvoid main() {\n    vColor = Colors[int(Vert.z)];\n    gl_Position = vec4(Vert.xy, 0.0, 1.0);\n}\n",
        frag: "precision lowp float;\nvarying vec4 vColor;\nvoid main() {\n    gl_FragColor = vColor;\n}\n",
        attribute: "Vert",
        uniform: "Colors"
    });
}
exports.getLoad = getLoad;
function getSprite(gl) {
    return shader.loadProgram(gl, {
        name: "Sprite",
        vert: "attribute vec4 Pos;\nattribute vec2 TexCoord;\n\nvarying vec2 vTexCoord;\n\nuniform mat4 MVP;\nuniform vec2 TexScale;\n\nvoid main() {\n    vTexCoord = TexCoord * TexScale;\n    gl_Position = MVP * vec4(Pos.xyz, 1.0);\n}\n",
        frag: "precision mediump float;\n\nvarying vec2 vTexCoord;\n\nuniform sampler2D Texture;\n\nvoid main() {\n    gl_FragColor = texture2D(Texture, vTexCoord);\n}\n",
        attribute: "Pos TexCoord",
        uniform: "MVP TexScale Texture"
    });
}
exports.getSprite = getSprite;
function getText(gl) {
    return shader.loadProgram(gl, {
        name: "Text",
        vert: "attribute vec4 Glyph;\nattribute float Style;\n\nvarying vec2 vTexCoord;\nvarying vec4 vColor;\n\nuniform vec2 VertScale;\nuniform vec2 VertOff;\nuniform vec2 TexScale;\nuniform vec4 Colors[32];\n\nvoid main() {\n    vTexCoord = Glyph.zw * TexScale;\n    vec4 c = Colors[int(Style)];\n    vColor = vec4(c.rgb * c.a, c.a);\n    gl_Position = vec4(Glyph.xy * VertScale + VertOff, 0.0, 1.0);\n}\n",
        frag: "precision mediump float;\n\nvarying vec2 vTexCoord;\nvarying vec4 vColor;\n\nuniform sampler2D Texture;\n\nvoid main() {\n    gl_FragColor = texture2D(Texture, vTexCoord).r * vColor;\n}\n",
        attribute: "Glyph Style",
        uniform: "Colors TexScale Texture VertOff VertScale"
    });
}
exports.getText = getText;
