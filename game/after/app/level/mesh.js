'use strict';
var BLOCK_WIDTH = 64;
// Generate a random integer in a half-open range.
function randRange(min, max) {
    var x = min + Math.floor(Math.random() * (max - min));
    return Math.min(max - 1, Math.max(min, x));
}
var Mesh = (function () {
    function Mesh(sx, sy) {
        this.sx = sx;
        this.sy = sy;
        this.ttype = new Uint8Array(sx * sy);
        this.ttop = new Uint8Array(sx * sy);
        this.tside = new Uint8Array(sx * sy);
        this.trough = new Uint8Array(sx * sy);
        this.theight = new Int16Array(sx * sy);
        this.vheight = new Int16Array((sx + 2) * (sy + 2) * 4);
    }
    ////////////////////////////////////////////////////////////////////
    // Mesh generation
    ////////////////////////////////////////////////////////////////////
    // Regenerate vertex heights from tile information.
    Mesh.prototype.regenerate = function () {
        var sx = this.sx, sy = this.sy;
        var x0 = sx, x1 = 0, y0 = sy, y1 = 0;
        for (var y = 0; y < sy; y++) {
            for (var x = 0; x < sx; x++) {
                var idx = y * sx + x;
                var type = this.ttype[idx];
                var z0, z1, z2, z3;
                if (type === 0) {
                    z0 = z1 = z2 = z3 = 0;
                }
                else {
                    x0 = Math.min(x0, x);
                    y0 = Math.min(y0, y);
                    x1 = Math.max(x1, x + 1);
                    y1 = Math.max(y1, y + 1);
                    var rough = this.trough[idx];
                    var height = this.theight[idx];
                    z0 = z1 = z2 = z3 = 16 * (height + 1);
                    switch (type) {
                        case 2:
                            z0 -= 16;
                            z1 -= 16;
                            z3 += 16;
                            z2 += 16;
                            break;
                        case 3:
                            z0 -= 16;
                            z1 += 16;
                            z3 += 16;
                            z2 -= 16;
                            break;
                        case 4:
                            z0 += 16;
                            z1 += 16;
                            z3 -= 16;
                            z2 -= 16;
                            break;
                        case 5:
                            z0 += 16;
                            z1 -= 16;
                            z3 -= 16;
                            z2 += 16;
                            break;
                    }
                    if (rough) {
                        z0 += randRange(-8, 9);
                        z1 += randRange(-8, 9);
                        z2 += randRange(-8, 9);
                        z3 += randRange(-8, 9);
                    }
                }
                var hoff = (y + 1) * (sx + 2) * 4 + (x + 1) * 4;
                this.vheight[hoff] = z0;
                this.vheight[hoff + 1] = z1;
                this.vheight[hoff + 2] = z2;
                this.vheight[hoff + 3] = z3;
            }
        }
        /*
        if (x0 < x1) {
            this.center.set([
                (x0 + x1 - sx) * (BLOCK_WIDTH * 0.5),
                (y0 + y1 - sy) * (BLOCK_WIDTH * 0.5),
                BLOCK_WIDTH,
            ]);
            this.diameter = Math.hypot(x1 - x0, y1 - y0) * BLOCK_WIDTH;
        } else {
            this.center.set([0, 0, BLOCK_WIDTH]);
            this.diameter = BLOCK_WIDTH * 10;
        }
        this.notify_();
        */
    };
    ////////////////////////////////////////////////////////////////////
    // Saving and loading
    ////////////////////////////////////////////////////////////////////
    // Load mesh data from its JSON representation.
    Mesh.prototype.load = function (data) {
        var sx = this.sx, sy = this.sy;
        var rows = data.tiles || [];
        for (var y = 0; y < sy; y++) {
            var i = rows.length - 1 - y;
            if (i < 0) {
                continue;
            }
            var row = rows[i].split(' ');
            for (var x = 0; x < row.length; x++) {
                if (x < 0 || x >= sx) {
                    continue;
                }
                var idx = y * sx + x;
                if (row[x] == '.') {
                    continue;
                }
                var tile = row[x];
                this.ttype[idx] = (tile) & 7;
                this.trough[idx] = (tile >> 3) & 1;
                this.ttop[idx] = (tile >> 4) & 15;
                this.tside[idx] = (tile >> 8) & 15;
                this.theight[idx] = (tile >> 12) & 15;
            }
        }
        this.regenerate();
    };
    // Dump mesh data to its JSON representation.
    Mesh.prototype.dump = function (data, rect) {
        var sx = this.sx, sy = this.sy;
        var x0 = 0, y0 = 0, x1 = sx, y1 = sy;
        if (rect) {
            x0 = Math.max(0, rect.x0);
            y0 = Math.max(0, rect.y0);
            x1 = Math.min(sx, rect.x1);
            y1 = Math.min(sy, rect.y1);
        }
        if (x0 >= x1 || y0 >= y1) {
            return;
        }
        var rows = [], row = [];
        for (var y = y0; y < y1; y++) {
            if (y < 0 || y >= sy) {
                for (x = x0; x < x1; x++) {
                    row[x - x0] = '.';
                }
            }
            else {
                var x = x0;
                for (; x < 0 && x < x1; x++) {
                    row[x - x0] = '.';
                }
                for (; x < sx && x < x1; x++) {
                    var idx, tile;
                    idx = y * sx + x;
                    tile = (((this.ttype[idx] & 7)) | ((this.trough[idx] & 1) << 3) | ((this.ttop[idx] & 15) << 4) | ((this.tside[idx] & 15) << 8) | ((this.theight[idx] & 15) << 12));
                    row[x - x0] = tile.toString(16);
                }
                for (; x < x1; x++) {
                    row[x - x0] = '.';
                }
            }
            rows.push(row.join(' '));
        }
        rows.reverse();
        data.tiles = rows;
    };
    return Mesh;
})();
exports.Mesh = Mesh;
