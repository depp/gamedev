'use strict';
var load = require('../load');
var mesh = require('./mesh');
var Level = (function () {
    function Level(sx, sy) {
        this.sx = sx;
        this.sy = sy;
        this.tileSet = null;
        this.mesh = new mesh.Mesh(sx, sy);
    }
    ////////////////////////////////////////////////////////////////////
    // Saving, loading, and creating
    ////////////////////////////////////////////////////////////////////
    // Load the level from its JSON representation.
    Level.prototype.load = function (data) {
        if (typeof data.tileSet == 'string') {
            this.tileSet = data.tileSet;
        }
        this.mesh.load(data);
    };
    // Serialize a level to its JSON representation.
    Level.prototype.dump = function (data, rect) {
        if (rect) {
            data.sx = rect.x1 - rect.x0;
            data.sy = rect.y1 - rect.y0;
        }
        else {
            data.sx = this.sx;
            data.sy = this.sy;
        }
        data.tileSet = this.tileSet;
        this.mesh.dump(data, rect);
    };
    // Open a level from
    Level.open = function (name) {
        return load.getJson('level/' + name + '.json').then(function (data) {
            var valid = typeof data == 'object' && typeof data.sx == 'number' && data.sx > 0 && typeof data.sy == 'number' && data.sy > 0;
            if (!valid) {
                throw Error('Invalid level data.');
            }
            var level = new Level(data.sx, data.sy);
            level.load(data);
            return level;
        });
    };
    // Save the level to the level database.
    Level.prototype.save = function (name) {
        var data = {};
        this.dump(data);
        return load.putJson('level/' + name + '.json', data);
    };
    return Level;
})();
exports.Level = Level;
