'use strict';

/// <reference path="bluebird.d.ts"/>
import Promise = require('bluebird');

import load = require('../load');
import util = require('../util');
import mesh = require('./mesh');

export class Level {
	tileSet: string;
	mesh: mesh.Mesh;

	constructor(public sx: number, public sy: number) {
		this.tileSet = null;
		this.mesh = new mesh.Mesh(sx, sy);
	}

	////////////////////////////////////////////////////////////////////
	// Saving, loading, and creating
	////////////////////////////////////////////////////////////////////

	// Load the level from its JSON representation.
	load(data: any) {
		if (typeof data.tileSet == 'string') {
			this.tileSet = <string>data.tileSet;
		}
		this.mesh.load(data);
	}

	// Serialize a level to its JSON representation.
	dump(data: any, rect?: util.Rect) {
		if (rect) {
			data.sx = rect.x1 - rect.x0;
			data.sy = rect.y1 - rect.y0;
		} else {
			data.sx = this.sx;
			data.sy = this.sy;
		}
		data.tileSet = this.tileSet;
		this.mesh.dump(data, rect);
	}

	// Open a level from
	static open(name: string) : Promise<Level> {
		return load.getJson('level/' + name + '.json')
			.then(function(data) {
				var valid = typeof data == 'object' &&
					typeof data.sx == 'number' && data.sx > 0 &&
					typeof data.sy == 'number' && data.sy > 0;
				if (!valid) {
					throw Error('Invalid level data.');
				}
				var level = new Level(<number>data.sx, <number>data.sy);
				level.load(data);
				return level;
			});
	}

	// Save the level to the level database.
	save(name: string) : Promise<{}> {
		var data : any = {};
		this.dump(data);
		return load.putJson('level/' + name + '.json', data);
	}
}
