'use strict';
var load = require('./load');
var loadscreen = require('./load-screen');
load.test();
(function (EventType) {
    EventType[EventType["MouseDown"] = 0] = "MouseDown";
    EventType[EventType["MouseUp"] = 1] = "MouseUp";
    EventType[EventType["MouseMove"] = 2] = "MouseMove";
    EventType[EventType["MouseEnter"] = 3] = "MouseEnter";
    EventType[EventType["MouseLeave"] = 4] = "MouseLeave";
    EventType[EventType["KeyDown"] = 5] = "KeyDown";
    EventType[EventType["KeyUp"] = 6] = "KeyUp";
    EventType[EventType["KeyRepeat"] = 7] = "KeyRepeat";
})(exports.EventType || (exports.EventType = {}));
var EventType = exports.EventType;
var ComponentRecord = (function () {
    function ComponentRecord() {
    }
    return ComponentRecord;
})();
var components = [];
var loadScreen = new loadscreen.LoadScreen;
// Handle an event.
function event(evt) {
    if (load.isLoading) {
        return;
    }
    var cs = components;
    for (var i = cs.length; i > 0; i--) {
        if (cs[i - 1].obj.event(evt)) {
            break;
        }
    }
}
exports.event = event;
// Render the contents of the screen.
function render(time, gl, aspect) {
    if (load.isLoading) {
        gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
        gl.clearColor(0.2, 0.2, 0.2, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        loadScreen.setProgress(load.progress);
        loadScreen.render(gl, aspect);
    }
    else {
        var cs = components;
        for (var j = cs.length; j > 0; j--) {
            cs[j - 1].obj.update(time);
        }
        gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
        gl.clearColor(0.2, 0.2, 0.2, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    }
}
exports.render = render;
// Remove everything from the screen, and replace it with the result
// of a callback function.
function set(cb) {
    components = [];
    var cs = cb();
    if (cs) {
        for (var i = 0; i < cs.length; i++) {
            components.push({ obj: cs[i], parent: null });
        }
    }
}
exports.set = set;
// Add a component to the screen.  The component will get updated and
// receive events before existing components.
function add(c, parent) {
    if (!c) {
        return;
    }
    components.push({ obj: c, parent: parent });
}
exports.add = add;
// Remove a component and all its children from the screen.
function remove(c) {
    var removed = false;
    var cs = components;
    if (!c) {
        console.error('Cannot remove nothing');
        return;
    }
    for (var i = 0; i < cs.length; i++) {
        if (cs[i].obj == c) {
            cs.splice(i, 1);
            removed = true;
            break;
        }
    }
    if (!removed) {
        console.error('Could not remove screen component');
    }
    else {
        c.removed();
        var children = cs.filter(function (cc) {
            return cc.parent == c;
        });
        for (i = 0; i < children.length; i++) {
            remove(children[i].obj);
        }
    }
}
exports.remove = remove;
