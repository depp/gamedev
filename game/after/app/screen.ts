'use strict';

import load = require('./load');
import loadscreen = require('./load-screen');

load.test();

export enum EventType {
	MouseDown,
	MouseUp,
	MouseMove,
	MouseEnter,
	MouseLeave,
	KeyDown,
	KeyUp,
	KeyRepeat
}

export interface GameEvent {
	type: EventType;
}

export interface MouseEvent extends GameEvent {
	// Whether the shift key is down.
	shift: boolean;

	// Location in UI space.
	px: number;
	py: number;

	// Location in clip space.
	cx: number;
	cy: number;
}

export interface KeyEvent extends Event {
	// Whether the shift key is down.
	shift: boolean;

	// The key name.
	key: string;
}

export interface Component {
	// Update the component before a screen update.
	update(time: number) : void;

	// Handle an event.  Return true to prevent the event from passing
	// to other components.
	event(evt: GameEvent) : boolean;

	// Called when the component has been removed from the screen.
	removed() : void;
}

class ComponentRecord {
	obj: Component;
	parent: Component;
}

var components : ComponentRecord[] = [];
var loadScreen = new loadscreen.LoadScreen;

// Handle an event.
export function event(evt: GameEvent) : void {
	if (load.isLoading) {
		return;
	}
	var cs = components;
	for (var i = cs.length; i > 0; i--) {
		if (cs[i-1].obj.event(evt)) {
			break;
		}
	}
}

// Render the contents of the screen.
export function render(time: number, gl: WebGLRenderingContext, aspect: number) : void {
	if (load.isLoading) {
		gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
		gl.clearColor(0.2, 0.2, 0.2, 1.0);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		loadScreen.setProgress(load.progress);
		loadScreen.render(gl, aspect);
	} else {
		var cs = components;
		for (var j = cs.length; j > 0; j--) {
			cs[j-1].obj.update(time);
		}
		gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
		gl.clearColor(0.2, 0.2, 0.2, 1.0);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	}
}

// Remove everything from the screen, and replace it with the result
// of a callback function.
export function set(cb: () => Component[]) {
	components = [];
	var cs = cb();
	if (cs) {
		for (var i = 0; i < cs.length; i++) {
			components.push({obj: cs[i], parent: null});
		}
	}
}

// Add a component to the screen.  The component will get updated and
// receive events before existing components.
export function add(c: Component, parent?: Component) {
	if (!c) {
		return;
	}
	components.push({obj: c, parent: parent});
}

// Remove a component and all its children from the screen.
export function remove(c: Component) {
	var removed = false;
	var cs = components;
	if (!c) {
		console.error('Cannot remove nothing');
		return;
	}
	for (var i = 0; i < cs.length; i++) {
		if (cs[i].obj == c) {
			cs.splice(i, 1);
			removed = true;
			break;
		}
	}
	if (!removed) {
		console.error('Could not remove screen component')
	} else {
		c.removed();
		var children = cs.filter(function (cc) {
			return cc.parent == c;
		});
		for (i = 0; i < children.length; i++) {
			remove(children[i].obj)
		}
	}
}
