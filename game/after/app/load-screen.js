'use strict';
var shader = require('./shader');
var BAR_WIDTH = 0.9;
var BAR_HEIGHT = 0.1;
var LINE_THICKNESS = 0.01;
var SHADOW_X = 0.008, SHADOW_Y = -0.012;
var BAR_YPOS = 0.15;
var COLORS = new Float32Array([
    0.8,
    0.8,
    0.8,
    1.0,
    0.1,
    0.2,
    0.8,
    1.0,
    0.1,
    0.1,
    0.1,
    1.0,
    0.1,
    0.1,
    0.1,
    1.0
]);
// Emit a large X to indicate that an error occurred.
function emitError() {
    var x0 = -1.0, x1 = -0.9, x2 = +0.9, x3 = +1.0;
    var y0 = -1.0, y1 = +1.0;
    var va = new Float32Array([
        x0,
        y0,
        0,
        x1,
        y0,
        0,
        x2,
        y1,
        0,
        x3,
        y1,
        0,
        x0,
        y1,
        0,
        x1,
        y1,
        0,
        x2,
        y0,
        0,
        x3,
        y0,
        0
    ]);
    var ia = new Uint16Array([
        0,
        1,
        2,
        3,
        3,
        4,
        4,
        5,
        6,
        7
    ]);
    return { va: va, ia: ia };
}
// Emit the progress bar geometry.
function emitBar(progress, aspect) {
    var dy = BAR_YPOS * 2 - 1;
    var x0 = -BAR_WIDTH, x1 = +BAR_WIDTH;
    var y0 = -BAR_HEIGHT + dy, y1 = +BAR_HEIGHT + dy;
    var w = LINE_THICKNESS, h = LINE_THICKNESS * aspect;
    var sx = SHADOW_X, sy = SHADOW_Y * aspect;
    var x2 = x0 + w, x3 = x1 - w;
    var y2 = y0 + h, y3 = y1 - h;
    var x4 = x0 + progress * (x1 - x0);
    var va = new Float32Array([
        x0 + sx,
        y0 + sy,
        2,
        x1 + sx,
        y0 + sy,
        2,
        x0 + sx,
        y1 + sy,
        2,
        x1 + sx,
        y1 + sy,
        2,
        x0,
        y0,
        0,
        x2,
        y2,
        0,
        x1,
        y0,
        0,
        x3,
        y2,
        0,
        x1,
        y1,
        0,
        x3,
        y3,
        0,
        x0,
        y1,
        0,
        x2,
        y3,
        0,
        x2,
        y2,
        1,
        x2,
        y3,
        1,
        x4,
        y2,
        1,
        x4,
        y3,
        1,
        x4,
        y2,
        3,
        x4,
        y3,
        3,
        x3,
        y2,
        3,
        x3,
        y3,
        3,
    ]);
    var ia = new Int16Array([
        0,
        1,
        2,
        3,
        3,
        4,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        4,
        5,
        5,
        12,
        12,
        13,
        14,
        15,
        15,
        16,
        16,
        17,
        18,
        19
    ]);
    return { va: va, ia: ia };
}
var LoadScreen = (function () {
    function LoadScreen() {
        this.loaded = false;
        this.program = null;
        this.progress = 0;
        this.valid = false;
        this.vbuffer = null;
        this.ibuffer = null;
        this.icount = null;
    }
    LoadScreen.prototype.setProgress = function (progress) {
        var p = Math.round(progress * 1e3);
        if (p != this.progress) {
            this.progress = p;
            this.valid = false;
        }
    };
    LoadScreen.prototype.emit = function (gl, aspect) {
        if (!this.loaded) {
            this.loaded = true;
            this.program = shader.getLoad(gl);
        }
        if (!this.program) {
            return;
        }
        var geom = this.progress >= 0 ? emitBar(this.progress * 1e-3, aspect) : emitError();
        if (!this.vbuffer) {
            this.vbuffer = gl.createBuffer();
        }
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
        gl.bufferData(gl.ARRAY_BUFFER, geom.va, gl.STATIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        if (!this.ibuffer) {
            this.ibuffer = gl.createBuffer();
        }
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, geom.ia, gl.STATIC_DRAW);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
        this.icount = geom.ia.length;
    };
    LoadScreen.prototype.render = function (gl, aspect) {
        gl.clearColor(0.2, 0.2, 0.2, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT);
        if (!this.valid) {
            this.valid = true;
            this.emit(gl, aspect);
        }
        if (this.icount <= 0) {
            return;
        }
        var p = this.program;
        gl.useProgram(p.program);
        gl.uniform4fv(p.Colors, COLORS);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);
        gl.drawElements(gl.TRIANGLE_STRIP, this.icount, gl.UNSIGNED_SHORT, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
        gl.useProgram(null);
    };
    return LoadScreen;
})();
exports.LoadScreen = LoadScreen;
