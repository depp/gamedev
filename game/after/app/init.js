'use strict';
var screen = require('./screen');
var EventType = screen.EventType;
// import load = require('./load');
var level = require('./level/level');
var canvas = null;
var gl = null;
var handle = null;
var lastResize = -1;
var lastWidth = 0;
var lastHeight = 0;
var RESIZE_DELAY = 500;
var canvasHeight = 0;
var MOUSE_EVENTS = {
    mousedown: 0 /* MouseDown */,
    mouseup: 1 /* MouseUp */,
    mousemove: 2 /* MouseMove */,
    mouseenter: 3 /* MouseEnter */,
    mouseleave: 4 /* MouseLeave */
};
function init(c, g) {
    if (canvas && gl) {
        return;
    }
    if (!c || !g) {
        return;
    }
    canvas = c;
    gl = g;
    window.addEventListener('resize', resize, false);
    resize();
    start();
    level.Level.open('z1_bridge').then(function (lvl) {
        console.log('Level: ' + lvl.sx + 'x' + lvl.sy);
    }, function (e) {
        console.error('Level failed to load', e);
    });
}
exports.init = init;
function start() {
    if (handle !== null) {
        return;
    }
    window.addEventListener('keydown', keyDown);
    window.addEventListener('keyup', keyUp);
    for (var type in MOUSE_EVENTS) {
        canvas.addEventListener(type, mouseEvent);
    }
    handle = window.requestAnimationFrame(render);
}
exports.start = start;
function stop() {
    if (handle === null) {
        return;
    }
    window.cancelAnimationFrame(handle);
    handle = null;
    window.removeEventListener('keydown', keyDown);
    window.removeEventListener('keyup', keyUp);
    for (var type in MOUSE_EVENTS) {
        canvas.removeEventListener(type, mouseEvent);
    }
}
exports.stop = stop;
function resize() {
    var w = canvas.clientWidth;
    var h = Math.max(1, Math.round(w * 9 / 16));
    if (h != canvasHeight) {
        canvas.style.height = h + 'px';
        canvasHeight = h;
    }
}
// Main rendering loop.
function render(time) {
    var w = canvas.clientWidth, h = canvas.clientHeight;
    var needsResize = lastResize < 0 || (time > lastResize + RESIZE_DELAY && (w != lastWidth || h != lastHeight));
    if (needsResize) {
        canvas.width = lastWidth = w;
        canvas.height = lastHeight = h;
        lastResize = time;
    }
    handle = window.requestAnimationFrame(render);
    screen.render(time, gl, w / h);
}
function mouseEvent(e) {
    e.preventDefault();
    var type;
    var offx = 0, offy = 0;
    var elt = canvas;
    do {
        offx += elt.offsetLeft - elt.scrollLeft;
        offy += elt.offsetTop - elt.scrollTop;
    } while ((elt = elt.offsetParent));
    var sx = canvas.offsetWidth;
    var sy = canvas.offsetHeight;
    var px = e.pageX - offx;
    var py = sy - (e.pageY - offy) - 1;
    var cx = Math.min(sx, Math.max(0, px + 0.5)) * 2 / sx - 1;
    var cy = Math.min(sy, Math.max(0, py + 0.5)) * 2 / sy - 1;
    screen.event({
        type: MOUSE_EVENTS[e.type],
        shift: e.shiftKey,
        px: px,
        py: py,
        cx: cx,
        cy: cy
    });
    return false;
}
var KEYS = {
    8: 'delete',
    9: 'tab',
    13: 'enter',
    27: 'escape',
    32: 'space',
    33: 'pageup',
    34: 'pagedown',
    35: 'end',
    36: 'home',
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down',
    45: 'insert',
    46: 'forwarddelete',
    186: ';',
    187: '=',
    188: ',',
    189: '-',
    190: '.',
    191: '/',
    192: '`',
    219: '[',
    220: '\\',
    221: ']',
    222: '\''
};
// Map a key code to a key name.
function mapKey(code) {
    if (code >= 48 && code <= 57 || code >= 65 && code <= 90) {
        return String.fromCharCode(code);
    }
    return KEYS[code] || null;
}
var keys_down = {};
// Handle a key down event.
function keyDown(e) {
    var key = mapKey(e.keyCode);
    if (!key) {
        return;
    }
    e.preventDefault();
    var type = keys_down[key] ? 7 /* KeyRepeat */ : 5 /* KeyDown */;
    keys_down[key] = true;
    screen.event({
        type: type,
        shift: e.shiftKey,
        key: key
    });
    return false;
}
function keyUp(e) {
    var key = mapKey(e.keyCode);
    if (!key) {
        return;
    }
    e.preventDefault();
    if (!keys_down[key]) {
        return;
    }
    delete keys_down[key];
    screen.event({
        type: 6 /* KeyUp */,
        shift: e.shiftKey,
        key: key
    });
    return false;
}
