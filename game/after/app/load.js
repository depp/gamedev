'use strict';
/// <reference path="bluebird.d.ts"/>
var Promise = require('bluebird');
exports.isLoading = false;
exports.progress = 0;
function test() {
    exports.isLoading = true;
    exports.progress = 0;
    var i = 0, n = 1000;
    var handle;
    function intervalcb() {
        i++;
        exports.progress = i / n;
        if (i >= n) {
            window.clearInterval(handle);
            exports.isLoading = false;
        }
    }
    handle = window.setInterval(intervalcb, 2 / 100);
}
exports.test = test;
function getJson(uri) {
    var xhr = new XMLHttpRequest;
    return new Promise(function (resolve, reject) {
        xhr.addEventListener('error', reject);
        xhr.addEventListener('load', resolve);
        xhr.open('GET', uri);
        xhr.send(null);
    }).then(function (value) {
        return JSON.parse(xhr.responseText);
    });
}
exports.getJson = getJson;
function putJson(uri, data) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest;
        xhr.addEventListener('error', reject);
        xhr.addEventListener('load', resolve);
        xhr.open('PUT', uri);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify(data));
    });
}
exports.putJson = putJson;
