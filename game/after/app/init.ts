'use strict';

import screen = require('./screen');
import EventType = screen.EventType;

// import load = require('./load');
import level = require('./level/level');

var canvas : HTMLCanvasElement = null;
var gl : WebGLRenderingContext = null;
var handle : number = null;
var lastResize : number = -1;
var lastWidth : number = 0;
var lastHeight : number = 0;
var RESIZE_DELAY : number = 500;
var canvasHeight : number = 0;

var MOUSE_EVENTS : {[name: string]: EventType} = {
	mousedown: EventType.MouseDown,
	mouseup: EventType.MouseUp,
	mousemove: EventType.MouseMove,
	mouseenter: EventType.MouseEnter,
	mouseleave: EventType.MouseLeave
};

export function init(c: HTMLCanvasElement, g: WebGLRenderingContext) {
	if (canvas && gl) {
		return;
	}
	if (!c || !g) {
		return;
	}
	canvas = c;
	gl = g;
	window.addEventListener('resize', resize, false);
	resize();
	start();

	level.Level.open('z1_bridge')
		.then(function(lvl) {
			console.log('Level: ' + lvl.sx + 'x' + lvl.sy);
		}, function(e) {
			console.error('Level failed to load', e);
		});
}

export function start() {
	if (handle !== null) {
		return;
	}
	window.addEventListener('keydown', keyDown);
	window.addEventListener('keyup', keyUp);
	for (var type in MOUSE_EVENTS) {
		canvas.addEventListener(type, mouseEvent);
	}
	handle = window.requestAnimationFrame(render);
}

export function stop() {
	if (handle === null) {
		return;
	}
	window.cancelAnimationFrame(handle);
	handle = null;
	window.removeEventListener('keydown', keyDown);
	window.removeEventListener('keyup', keyUp);
	for (var type in MOUSE_EVENTS) {
		canvas.removeEventListener(type, mouseEvent);
	}
}

function resize() {
	var w = canvas.clientWidth;
	var h = Math.max(1, Math.round(w * 9 / 16));
	if (h != canvasHeight) {
		canvas.style.height = h + 'px';
		canvasHeight = h;
	}
}

// Main rendering loop.
function render(time : number) {
	var w = canvas.clientWidth, h = canvas.clientHeight;
	var needsResize = lastResize < 0 ||
		(time > lastResize + RESIZE_DELAY &&
		 (w != lastWidth || h != lastHeight));
	if (needsResize) {
		canvas.width = lastWidth = w;
		canvas.height = lastHeight = h;
		lastResize = time;
	}
	handle = window.requestAnimationFrame(render);
	screen.render(time, gl, w / h);
}

function mouseEvent(e: MouseEvent) {
	e.preventDefault();
	var type : EventType;
	var offx = 0, offy = 0;
	var elt : HTMLElement = canvas;
	do {
		offx += elt.offsetLeft - elt.scrollLeft;
		offy += elt.offsetTop - elt.scrollTop;
	} while ((elt = <HTMLElement>elt.offsetParent));
	var sx = canvas.offsetWidth;
	var sy = canvas.offsetHeight;
	var px = e.pageX - offx;
	var py = sy - (e.pageY - offy) - 1;
	var cx = Math.min(sx, Math.max(0, px + 0.5))*2/sx-1;
	var cy = Math.min(sy, Math.max(0, py + 0.5))*2/sy-1;
	screen.event({
		type: MOUSE_EVENTS[e.type],
		shift: e.shiftKey,
		px: px,
		py: py,
		cx: cx,
		cy: cy
	});
	return false;
}

var KEYS : {[k: string]: string} = {
	8: 'delete',
	9: 'tab',
	13: 'enter',
	27: 'escape',
	32: 'space',
	33: 'pageup',
	34: 'pagedown',
	35: 'end',
	36: 'home',
	37: 'left',
	38: 'up',
	39: 'right',
	40: 'down',
	45: 'insert',
	46: 'forwarddelete',
	186: ';',
	187: '=',
	188: ',',
	189: '-',
	190: '.',
	191: '/',
	192: '`',
	219: '[',
	220: '\\',
	221: ']',
	222: '\''
};

// Map a key code to a key name.
function mapKey(code: number) : string {
	if (code >= 48 && code <= 57 || code >= 65 && code <= 90) {
		return String.fromCharCode(code);
	}
	return KEYS[code] || null;
}

var keys_down : {[name: string]: boolean} = {};

// Handle a key down event.
function keyDown(e: KeyboardEvent) {
	var key = mapKey(e.keyCode);
	if (!key) {
		return;
	}
	e.preventDefault();
	var type = keys_down[key] ? EventType.KeyRepeat : EventType.KeyDown;
	keys_down[key] = true;
	screen.event({
		type: type,
		shift: e.shiftKey,
		key: key
	});
	return false;
}

function keyUp(e: KeyboardEvent) {
	var key = mapKey(e.keyCode);
	if (!key) {
		return;
	}
	e.preventDefault();
	if (!keys_down[key]) {
		return;
	}
	delete keys_down[key];
	screen.event({
		type: EventType.KeyUp,
		shift: e.shiftKey,
		key: key
	});
	return false;
}
