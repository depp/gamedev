'use strict';

interface ShaderSpec {
	name: string;
	type: number;
	source: string;
}

function loadShader(gl: WebGLRenderingContext, s: ShaderSpec) : WebGLShader {
	var shader = gl.createShader(s.type);
	gl.shaderSource(shader, s.source);
	gl.compileShader(shader);
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		console.log('Errors for shader: ' + s.name);
		console.log(gl.getShaderInfoLog(shader));
		gl.deleteShader(shader);
		return null;
	}
	return shader;
}

interface ProgramSpec {
	name: string;
	vert: string;
	frag: string;
	attribute: string;
	uniform: string;
}

export function loadProgram(gl: WebGLRenderingContext, p: ProgramSpec): any {
	var specs: ShaderSpec[] = [{
		name: p.name + '.vert',
		type: gl.VERTEX_SHADER,
		source: p.vert
	}, {
		name: p.name + '.frag',
		type: gl.FRAGMENT_SHADER,
		source: p.frag
	}];
	var i: number;
	var program = gl.createProgram();
	for (i = 0; i < specs.length; i++) {
		var shader = loadShader(gl, specs[i]);
		if (!shader) {
			gl.deleteProgram(program);
			return null;
		}
		gl.attachShader(program, shader);
		gl.deleteShader(shader);
	}
	var attrib = p.attribute.split(' ');
	for (i = 0; i < attrib.length; i++) {
		gl.bindAttribLocation(program, i, attrib[i]);
	}
	gl.linkProgram(program);
	if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
		console.log('Errors for program: ', p.name);
		console.log(gl.getProgramInfoLog(program));
		gl.deleteProgram(program);
		return null;
	}
	var obj: any = {program: program};
	var uniform = p.uniform.split(' ');
	for (i = 0; i < uniform.length; i++) {
		var uname = uniform[i];
		var loc = gl.getUniformLocation(program, uname);
		if (!loc) {
			console.log('Missing uniform: ' + uname + ' (' + name + ')');
		}
		obj[uname] = loc;
	}
	return obj;
}
