'use strict';

module.exports = {
	camera: require('./sys/camera.js'),
	CameraControl: require('./sys/camera-control.js'),
	level: require('./level/level.js'),
	load: require('./sys/load.js'),
	screen: require('./screen.js'),
	services: require('./sys/services.js'),
	sprites: require('./sys/sprites.js'),
	text: require('./sys/text.js')
};
