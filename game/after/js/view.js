'use strict';

var LevelRenderer = require('./level/renderer.js');
var sprites = require('./sys/sprites.js');
var text = require('./sys/text.js');
var camera = require('./sys/camera.js');
var load = require('./sys/load.js');
var LoadScreen = require('./sys/load-screen.js');

var rLevel = new LevelRenderer();

var parts = [];
var loading = null;

function renderLoading(time, gl, width, height, aspect) {
	gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
	gl.clearColor(0.1, 0.1, 0.1, 1.0);
	gl.clear(gl.COLOR_BUFFER_BIT);
	loading.draw(gl, aspect);
	return true;
}

function renderScreen(time, gl, width, height, aspect) {
	var i, cur_parts = parts;
	for (i = cur_parts.length; i > 0; i--) {
		cur_parts[i-1].obj.update(time, aspect);
	}
	if (parts !== cur_parts) {
		return false;
	}

	gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
	gl.clearColor(0.2, 0.2, 0.2, 1.0);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	camera.update(aspect);
	rLevel.render(time, gl, aspect);
	sprites.draw(time, gl, aspect);
	text.draw(time, gl, aspect);
	return true;
}

function render(time, gl, width, height, aspect) {
	var i, f;
	for (i = 0; i < 5; i++) {
		f = loading ? renderLoading : renderScreen;
		if (f(time, gl, width, height, aspect)) {
			return;
		}
	}
	console.error('Update loop!');
}

function handleEvent(evt) {
	if (loading) {
		return;
	}
	var cur_parts = parts, i;
	for (i = cur_parts.length; i > 0; i--) {
		if (cur_parts[i-1].obj.handleEvent(evt)) {
			break;
		}
	}
}

function onLoaded() {
	loading = null;
}

// Replace the current screen with the results of a function.
function setScreen(screen, arg) {
	var new_parts, i;
	parts = [];
	rLevel.clear();
	sprites.clear();
	text.clear();
	camera.clear();
	new_parts = screen(arg);
	if (new_parts) {
		for (i = 0; i < new_parts.length; i++) {
			add(new_parts[i], null);
		}
	}
	if (!load.loaded && !loading) {
		loading = new LoadScreen(onLoaded);
	}
}

// Add a view to the screen.
function add(part, parent) {
	parts.push({obj: part, parent: parent || null});
}

// Remove a view from the screen.
function remove(part) {
	var removed = false, i, children;
	if (!part) {
		console.error('Cannot remove nothing');
	}
	for (i = 0; i < parts.length; i++) {
		if (parts[i].obj === part) {
			parts.splice(i, 1);
			removed = true;
			break;
		}
	}
	if (!removed) {
		console.error('Failed to remove view');
	} else {
		children = parts.filter(function(p) {
			return p.parent === part;
		});
		for (i = 0; i < children.length; i++) {
			remove(children[i].obj);
		}
	}
}

function setLevel(level) {
	rLevel.setLevel(level);
}

function setHighlight(highlight) {
	rLevel.setHighlight(highlight);
}

module.exports = {
	render: render,
	handleEvent: handleEvent,
	setScreen: setScreen,
	add: add,
	remove: remove,

	setLevel: setLevel,
	setHighlight: setHighlight
};
