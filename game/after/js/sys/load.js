'use strict';

var completed = 0;
var total = 0;
var error = null;
var callbacks = [];
var current = null;

function getState() {
	return  {
		completed: completed,
		total: total,
		error: error,
		current: current
	};
}

function runCallbacks(state) {
	var i;
	for (i = 0; i < callbacks.length; i++) {
		callbacks[i](state);
	}
}

function rsrcLoad(name) {
	if (error) {
		return;
	}
	if (false) {
		console.log('loaded', name);
	}
	current = name;
	completed++;
	var state = getState();
	if (completed == total) {
		module.exports.loaded = true;
		completed = 0;
		total = 0;
		current = null;
	}
	runCallbacks(state);
}

function rsrcFail(name, msg) {
	if (msg !== null) {
		console.error('' + name + ': ' + msg);
	}
	current = name;
	console.error('Failed to load resource: ' + name);
	fail('Failed to load resource: ' + name);
}

// Register a resource to be loaded.
function add(obj, name) {
	total++;
	module.exports.loaded = false;
	obj.onload = function(e) { rsrcLoad(name); };
	obj.onerror = function(e) { rsrcFail(name); };
}

// Get a JSON document.
function json(uri, name, success, failure) {
	success = success || function() {};
	failure = failure || function() {};
	function handleFailure(param) {
		var ignore = failure(param);
		if (ignore) {
			rsrcLoad(name);
		} else {
			rsrcFail(name, param.msg);
		}
	}
	var xhr = new XMLHttpRequest();
	xhr.open('GET', uri, true);
	xhr.onload = function(e) {
		var ignoreError;
		if (xhr.readyState !== 4) {
			return;
		}
		if (xhr.status !== 200) {
			handleFailure({
				msg: 'Request failed.',
				status: xhr.status
			});
			return;
		}
		var obj;
		try {
			obj = JSON.parse(xhr.responseText);
		} catch(ex) {
			handleFailure({
				msg: 'Server returned invalid JSON.'
			});
			return;
		}
		success(obj);
		rsrcLoad(name);
	};
	xhr.onerror = function(e) {
		handleFailure({
			msg: 'Request failed.'
		});
	};
	xhr.send(null);
}

// Load an image.
function image(src) {
	var img = new Image();
	add(img, src);
	img.src = src;
	return img;
}

// Register a callback for when the state of resources changes.  This
// will be given a single parameter with the following attributes:
// total (number of resources), completed (number of successfully
// loaded resources), errors (list of resources which failed to load),
// and current (the name of most recent resource to successfully load,
// or null).
function addListener(cb) {
	cb(getState());
	callbacks.push(cb);
}

function removeListener(cb) {
	var idx = callbacks.indexOf(cb);
	if (idx === -1) {
		console.log('Could not remove load listener');
	}
	callbacks.splice(idx, 1);
}

// Signal failure.
function fail(msg) {
	if (error) {
		return;
	}
	error = msg;
	runCallbacks(getState());
}

// Test the loading screen by injecting dummy resources.
function test() {
	var i = 0, n = 10, t = 500 / n;
	total += 10;
	module.exports.loaded = false;
	function callback() {
		i++;
		rsrcLoad('Test ' + i);
		if (i < n) {
			schedule();
		}
	}
	function schedule() {
		window.setTimeout(callback, (0.5 + Math.random()) * t);
	}
	schedule();
}

module.exports = {
	add: add,
	image: image,
	json: json,
	addListener: addListener,
	removeListener: removeListener,
	fail: fail,
	test: test,
	loaded: true
};
