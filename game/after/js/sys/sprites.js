'use strict';

/* EXAMPLE:

sprites.frames.push({
	loc: <vec3>,
	face: null || degrees,
	name: 'animation_name',
	frame: 2
});
*/

var glm = require('gl-matrix');
var vec2 = glm.vec2;

var util = require('../util.js');

var camera = require('./camera.js');
var load = require('./load.js');
var shader = require('./shader.js');

var sheet = require('../../tools/sprites/sprites.json');

// frames must have attributes:
//   loc: a vec3
//   name: a string, name of the animation
//   face: facing angle, in degrees
//   frame: frame number

function Sprites() {
	this.image = load.image('sprites.png');
	this.frames = [];
	this.vdata = null;
	this.asize = 0;

	// OpenGL data
	this.program = null;
	this.vbuffer = null;
	this.ibuffer = null;
	this.icount = -1;
	this.texture = null;
	this.texscale = vec2.create();
	vec2.set(this.texscale, 1.0 / sheet.x, 1.0 / sheet.y);
}

// Clear all sprites.
Sprites.prototype.clear = function() {
	this.frames = [];
	this.icount = -1;
};

// Emit the vertex data for the sprites.
Sprites.prototype.compile = function(gl) {
	var n = this.frames.length;
	if (!n) {
		this.icount = 0;
		return;
	}
	var i, j;
	if (this.asize < n) {
		this.asize = util.nextPow2(n);
		this.vdata = new ArrayBuffer(this.asize * 16 * 4);
		var idata = new Int16Array(this.asize * 6);
		// Always quads, always the same pattern.
		for (i = 0; i < this.asize; i++) {
			idata[i*6+0] = i*4+0;
			idata[i*6+1] = i*4+1;
			idata[i*6+2] = i*4+2;
			idata[i*6+3] = i*4+2;
			idata[i*6+4] = i*4+1;
			idata[i*6+5] = i*4+3;
		}
		if (!this.ibuffer) {
			this.ibuffer = gl.createBuffer();
		}
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, idata, gl.STATIC_DRAW);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
	}
	var vf32 = new Float32Array(this.vdata);
	var vi16 = new Int16Array(this.vdata);
	var vi = 0;
	var right = camera.right, up = camera.up;
	var errors =0;
	for (i = 0; i < this.frames.length; i++) {
		var fr = this.frames[i];
		var loc = fr.loc;
		var anim = sheet.anim[fr.name];
		if (!anim) {
			errors++;
			console.log('No such animation: ' + fr.name);
			continue;
		}
		var face;
		if (typeof fr.face == 'number' && anim.y >= 4) {
			face = Math.floor((camera.azimuth - fr.face + 225) * (1 / 90));
			face &= 3;
		} else {
			face = 0;
		}
		var frame = fr.frame;
		var idx = 6 * (face * anim.x + frame);
		var s = anim.frame;
		if (idx < 0 || idx >= s.length) {
			errors++;
			console.log('Invalid index: ' + idx);
			continue;
		}
		var lx = s[idx+0], ly = s[idx+1];
		var sx = s[idx+2], sy = s[idx+3];
		var ox = s[idx+4], oy = s[idx+5];

		for (j = 0; j < 3; j++) {
			vf32[vi*4+j] = loc[j] - ox*right[j] + (oy-sy)*up[j];
		}
		vi16[vi*8+6] = lx;
		vi16[vi*8+7] = ly+sy;
		vi++;

		for (j = 0; j < 3; j++) {
			vf32[vi*4+j] = loc[j] + (sx-ox)*right[j] + (oy-sy)*up[j];
		}
		vi16[vi*8+6] = lx+sx;
		vi16[vi*8+7] = ly+sy;
		vi++;

		for (j = 0; j < 3; j++) {
			vf32[vi*4+j] = loc[j] - ox*right[j] + oy*up[j];
		}
		vi16[vi*8+6] = lx;
		vi16[vi*8+7] = ly;
		vi++;

		for (j = 0; j < 3; j++) {
			vf32[vi*4+j] = loc[j] + (sx-ox)*right[j] + oy*up[j];
		}
		vi16[vi*8+6] = lx+sx;
		vi16[vi*8+7] = ly;
		vi++;
	}

	if (errors > 0) {
		console.log('Sprite errors: ' + errors);
	}
	if (!vi) {
		this.icount = 0;
		return;
	}

	if (!this.vbuffer) {
		this.vbuffer = gl.createBuffer();
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
	gl.bufferData(gl.ARRAY_BUFFER, this.vdata, gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	this.icount = vi + (vi >> 1);

	if (!this.program) {
		this.program = shader.getProgram(
			gl,
			['sprite.vert', 'sprite.frag'],
			['Pos', 'TexCoord'],
			['MVP', 'TexScale', 'Texture']);
		if (!this.program) {
			this.icount = 0;
			return;
		}
	}

	if (!this.texture) {
		this.texture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, this.texture);
		gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
		gl.texImage2D(
			gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(
			gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
};

Sprites.prototype.draw = function(time, gl, aspect) {
	if (this.icount < 0) {
		this.compile(gl);
	}
	if (!this.icount) {
		return;
	}

	gl.enable(gl.BLEND);
	gl.enable(gl.DEPTH_TEST);
	gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
	gl.useProgram(this.program.program);
	gl.uniformMatrix4fv(this.program.MVP, false, camera.mvp);
	gl.uniform2fv(this.program.TexScale, this.texscale);
	gl.uniform1i(this.program.Texture, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
	gl.enableVertexAttribArray(0);
	gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 16, 0);
	gl.enableVertexAttribArray(1);
	gl.vertexAttribPointer(1, 2, gl.SHORT, false, 16, 12);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, this.texture);

	gl.drawElements(gl.TRIANGLES, this.icount, gl.UNSIGNED_SHORT, 0);

	gl.disableVertexAttribArray(0);
	gl.disableVertexAttribArray(1);
	gl.disable(gl.BLEND);
	gl.disable(gl.DEPTH_TEST);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
	gl.useProgram(null);
};

module.exports = new Sprites();
