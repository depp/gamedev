'use strict';

var camera = require('./camera.js');
var bezierEasing = require('bezier-easing');

var swiftOut = bezierEasing(0.55, 0, 0.1, 1);

var DELTA_TIME = 250;

function CameraControl(azimuth) {
	if (typeof azimuth != 'number') {
		this.pos = 2;
		this.offset = -10;
	} else {
		this.pos = (azimuth / 90 + 0.5) & 3;
		this.offset = azimuth - (this.pos * 90);
	}
	this.state = 0;
	this.pos0 = this.pos;
	this.pos1 = this.pos;
	this.time0 = 0;
	this.time1 = 0;
}

CameraControl.prototype.update = function(time, aspect) {
	var fraction, value;

	switch (this.state) {
	case 0:
		break;

	case 1:
		this.time0 = time;
		this.time1 = time + DELTA_TIME;
		this.pos0 = this.pos;
		this.state = 2;
		break;

	case 2:
		if (time >= this.time1) {
			value = this.pos1 & 3;
			this.pos0 = value;
			this.pos1 = value;
			this.state = 0;
		} else {
			fraction = (time - this.time0) / (this.time1 - this.time0);
			fraction = swiftOut(fraction);
			value = this.pos0 + fraction * (this.pos1 - this.pos0);
		}
		this.pos = value;
		camera.azimuth = value * 90 + this.offset;
		break;
	}
};

CameraControl.prototype.handleEvent = function(evt) {
	if (evt.type != 'keydown') {
		return;
	}
	switch (evt.key) {
	case '[':
		this.pos1++;
		this.state = 1;
		return true;
	case ']':
		this.pos1--;
		this.state = 1;
		return true;
	}
};

module.exports = CameraControl;
