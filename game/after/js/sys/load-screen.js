'use strict';

var load = require('./load.js');
var shader = require('./shader.js');

var BAR_WIDTH = 0.9;
var BAR_HEIGHT = 0.1;
var LINE_THICKNESS = 0.01;
var BAR_YPOS = 0.3;

var COLORS = new Float32Array([
	0.4, 0.4, 0.4, 1.0,
	0.2, 0.2, 0.8, 1.0
]);

function LoadScreen(callback) {
	this.progress = 0;
	this.valid = true;
	this.program = null;
	this.vbuffer = null;
	this.ibuffer = null;
	this.icount = 0;
	this.gl = null;
	this.loaded = false;
	this.onloaded = null;
	this.callback = callback;
	this._listener = loadCallback.bind(this);
	load.addListener(this._listener);
}

function loadCallback(state) {
	/*jshint validthis:true*/
	if (this.progress < 0) {
		return;
	}
	if (state.error) {
		this.progress = -1;
		this.valid = false;
		alert('Error: ' + state.error);
	} else if (state.completed == state.total) {
		this.progress = -2;
		if (this.gl) {
			if (this.program) {
				this.gl.deleteProgram(this.program.program);
			}
			if (this.vbuffer) {
				this.gl.deleteBuffer(this.vbuffer);
			}
			if (this.ibuffer) {
				this.gl.deleteBuffer(this.vbuffer);
			}
		}
		load.removeListener(this._listener);
		this.callback();
	} else {
		var progress = 0;
		if (state.total > 0) {
			progress = Math.max(0, Math.min(state.total, state.completed));
			progress /= state.total;
		}
		this.progress = progress;
		this.valid = false;
	}
}

LoadScreen.prototype.handleMouse = function(action, x, y) {};

LoadScreen.prototype.handleKey = function(action, key) {};

function drawError() {
	var va, ia;
	var x0 = -1.0, x1 = -0.9, x2 = +0.9, x3 = +1.0;
	var y0 = -1.0, y1 = +1.0;

	va = new Float32Array([
		x0, y0, 0, x1, y0, 0,
		x2, y1, 0, x3, y1, 0,
		x0, y1, 0, x1, y1, 0,
		x2, y0, 0, x3, y0, 0
	]);

	ia = new Uint16Array([
		0, 1, 2, 3,
		3, 4,
		4, 5, 6, 7
	]);

	return {va:va, ia:ia};
}

function drawProgress(progress, aspect) {
	var va, ia;
	var dy = BAR_YPOS * 2 - 1;
	var x0 = -BAR_WIDTH, x1 = +BAR_WIDTH;
	var y0 = -BAR_HEIGHT + dy, y1 = +BAR_HEIGHT + dy;
	var w = LINE_THICKNESS, h = LINE_THICKNESS * aspect;
	var x2 = x0 + w, x3 = x1 - w;
	var y2 = y0 + h, y3 = y1 - h;
	var x4 = x0 + progress * (x1 - x0);

	va = new Float32Array([
		x0, y0, 0, x2, y2, 0,
		x1, y0, 0, x3, y2, 0,
		x1, y1, 0, x3, y3, 0,
		x0, y1, 0, x2, y3, 0,

		x2, y2, 1, x2, y3, 1,
		x4, y2, 1, x4, y3, 1
	]);

	ia = new Int16Array([
		0, 1, 2, 3, 4, 5, 6, 7, 0, 1,
		1, 8,
		8, 9, 10, 11
	]);

	return {va:va, ia:ia};
}

LoadScreen.prototype.emit = function(gl, aspect) {
	this.gl = gl;
	var arr;
	if (this.progress < 0) {
		arr = drawError();
	} else {
		arr = drawProgress(this.progress, aspect);
	}

	if (!this.vbuffer) {
		this.vbuffer = gl.createBuffer();
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
	gl.bufferData(gl.ARRAY_BUFFER, arr.va, gl.STATIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	if (!this.ibuffer) {
		this.ibuffer = gl.createBuffer();
	}
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, arr.ia, gl.STATIC_DRAW);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

	if (!this.program) {
		this.program = shader.getProgram(
			gl,
			['load.vert', 'load.frag'],
			['Vert'],
			['Colors']);
	}
	this.valid = true;
	this.icount = arr.ia.length;
};

LoadScreen.prototype.draw = function(gl, aspect) {
	if (this.progress == -2) {
		return;
	}
	if (!this.valid) {
		this.emit(gl, aspect);
	}

	gl.useProgram(this.program.program);
	gl.uniform4fv(this.program.Colors, COLORS);
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
	gl.enableVertexAttribArray(0);
	gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);

	gl.drawElements(gl.TRIANGLE_STRIP, this.icount, gl.UNSIGNED_SHORT, 0);

	gl.disableVertexAttribArray(0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
	gl.useProgram(null);
};

module.exports = LoadScreen;
