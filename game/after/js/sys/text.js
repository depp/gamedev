'use strict';

/* EXAMPLE
	text.layout({
		text: 'Hello, world!',
		visible: true,
		x: 0,
		y: 360,
		valign: 'top',
		halign: 'left'
	});
*/

var glm = require('gl-matrix');
var vec2 = glm.vec2;

var load = require('./load.js');
var camera = require('./camera.js');
var shader = require('./shader.js');

var font = require('../../tools/fonts/font.json');

// Size of the color uniform.
var COLOR_COUNT = 32;
var SHADOW_X = 1.5, SHADOW_Y = -2;

// Scaling factors / vertex coordinate adjustments.  SCALE is chosen
// so that CENTER_X and CENTER_Y don't overflow a 16-bit integer.
var SCREEN_SCALE = 64, CENTER_X = 640 / 2, CENTER_Y = 360 / 2;

// Scaling factor applied to all layouts.
var TEXT_SCALE = 0.5;

// Attributes:
//   font        font name
//   width       wrap width
//   text        string to typeset
//   visible     whether layout is initially visible
//   leading     space between consecutive lines
//   valign      'top', 'center', 'bottom'
//   halign      'left', 'center', 'right'
//   textalign   'left', 'center', 'right'
//   scale       relative font scale, 1.0 by default

// Set text into lines.
function typeset(sys, obj) {
	if (isNaN(obj.x) || isNaN(obj.y)) {
		console.error('Missing text coordinates');
		return;
	}
	var i;
	var glyphcount = 0;
	var width = obj.width || 0;
	var lines = []; // array of line objects
	var line = []; // array of hbox objects
	var hbox = []; // array of glyphs
	var text = obj.text;
	var fname = obj.font || 'normal';
	var fdata = font.fonts[fname];
	if (!fdata) {
		console.error('Unknown font:', fname);
		return;
	}

	var total_width = width;
	var fglyphs = fdata.glyphs;
	var vert_pos = 0, vert_min = 0;
	var line_pos = 0, hbox_pos = 0, line_width = 0;
	var ascender = fdata.ascender, descender = fdata.descender;
	var height = fdata.height;
	var leading = obj.leading || height;

	function end_line(forced) {
		lines.push({
			hboxes: line,
			x: 0,
			y: vert_pos - ascender,
			width: line_width,
			forced: forced
		});
		if (!width) {
			total_width = Math.max(line_width, width);
		}
		line = [];
		line_pos = 0;
		line_width = 0;
		vert_min = vert_pos - (ascender - descender);
		vert_pos -= leading;
	}

	function end_hbox() {
		var j, g;
		if (!hbox) {
			return;
		}
		if (width && line_pos > 0 && line_pos + hbox_pos > width) {
			end_line(false);
		}
		line.push({
			glyphs: hbox,
			x: line_pos,
			width: hbox_pos
		});
		line_pos += hbox_pos;
		hbox = [];
		hbox_pos = 0;
	}

	for (i = 0; i < text.length; i++) {
		// glyph structure: loc[2], size[2], offset[2], advance
		var c = text[i];
		var gd = fglyphs[c];
		switch (c) {
		case ' ':
			end_hbox();
			line_pos += gd ? gd[6] || 0 : 0;
			break;

		case '\n':
			end_hbox();
			end_line(true);
			break;

		default:
			if (!gd) {
				console.error('No glyph for character:', c);
			} else {
				glyphcount++;
				hbox.push({gd: gd, x: hbox_pos});
				hbox_pos += gd[6] || 0;
			}
			break;
		}
	}

	end_hbox();
	end_line(true);

	var valign = obj.valign || 'top';
	var halign = obj.halign || 'left';
	var text_align = obj.text_align || 'left';
	var x0 = 0, y0 = 0;

	switch (halign) {
	case 'left':
		break;
	case 'center':
		x0 = Math.floor(total_width * 0.5);
		break;
	case 'right':
		x0 = -total_width;
		break;
	default:
		console.error('Invalid halign', halign);
		break;
	}

	switch (valign) {
	case 'top':
		break;
	case 'center':
		y0 = Math.floor(-vert_min * 0.5);
		break;
	case 'bottom':
		y0 = -vert_min;
		break;
	default:
		console.error('Invalid valign', valign);
		break;
	}

	switch (text_align) {
	case 'left':
		break;
	case 'center':
		for (i = 0; i < lines.length; i++) {
			line.x = Math.floor((total_width - line.width) * 0.5);
		}
		break;
	case 'right':
		for (i = 0; i < lines.length; i++) {
			line.x = total_width - line.width;
		}
		break;
	default:
		console.error('Invalid text align', text_align);
		break;
	}

	if (!glyphcount) {
		return null;
	}
	var scale = (obj.scale || 1) * TEXT_SCALE;
	return {
		lines: lines,
		glyphcount: glyphcount,
		sy: -vert_min,
		sx: total_width,
		x0: x0 + (obj.x - CENTER_X) / scale,
		y0: y0 + (obj.y - CENTER_Y) / scale,
		scale: scale * SCREEN_SCALE
	};
}

// Convert lines to a layout buffer.
function glyphBuffer(gdata) {
	var va = new Int16Array(36 * gdata.glyphcount);
	var li, hi, gi, vi = 0;
	var scale = gdata.scale;
	var x0 = gdata.x0, y0 = gdata.y0;
	for (li = 0; li < gdata.lines.length; li++) {
		var line = gdata.lines[li];
		var lx0 = x0 + line.x, ly0 = y0 + line.y;
		for (hi = 0; hi < line.hboxes.length; hi++) {
			var hbox = line.hboxes[hi];
			var hx0 = hbox.x + lx0;
			for (gi = 0; gi < hbox.glyphs.length; gi++) {
				// glyph structure: loc[2], size[2], offset[2], advance
				var g = hbox.glyphs[gi];
				var gd = g.gd;
				var vx0 = hx0 + g.x + gd[4], vx1 = vx0 + gd[2];
				var vy0 = ly0 + gd[5], vy1 = vy0 + gd[3];
				var tx0 = gd[0], tx1 = tx0 + gd[2];
				var ty1 = gd[1], ty0 = ty1 + gd[3];
				vx0 = (vx0 * scale) | 0; vx1 = (vx1 * scale) | 0;
				vy0 = (vy0 * scale) | 0; vy1 = (vy1 * scale) | 0;
				va.set([
					vx0, vy0, tx0, ty0, 0, 0,
					vx1, vy0, tx1, ty0, 0, 0,
					vx0, vy1, tx0, ty1, 0, 0,
					vx0, vy1, tx0, ty1, 0, 0,
					vx1, vy0, tx1, ty0, 0, 0,
					vx1, vy1, tx1, ty1, 0, 0,
				], vi*36);
				vi++;
			}
		}
	}
	if (vi != gdata.glyphcount) {
		console.error('ASSERT FAIL');
	}
	return va;
}

// A text layout.
function Layout(sys, obj) {
	this.sys = sys;
	this.vdata = null;
	this.visible = false;
	this.vcount = 0;

	var gdata = typeset(sys, obj);
	if (gdata) {
		this.vdata = glyphBuffer(gdata);
		this.vcount = gdata.glyphcount * 6;
		sys.layouts.push(this);
		this.setVisible(obj.visible);
	}
}

// Set the visibility of this text layout.
Layout.prototype.setVisible = function(flag) {
	if (!this.vdata) {
		return;
	}
	if (flag) {
		if (!this.visible) {
			this.visible = true;
			this.sys.vcount = -1;
		}
	} else {
		if (this.visible) {
			this.visible = false;
			this.sys.vcount = -1;
		}
	}
};

// Remove this layout from the text system.
Layout.prototype.remove = function() {
	var i, layouts = this.sys.layouts;
	for (i = 0; i < layouts.length; i++) {
		if (layouts[i] === this) {
			layouts.splice(i, 1);
			if (this.visible) {
				this.sys.vcount = -1;
			}
		}
	}
};

// The text rendering system.
function Text() {
	var i;
	this.image = load.image('glyphs.png');
	this.layouts = [];
	this.colorAlloc = [true];
	for (i = 1; i < COLOR_COUNT; i++) {
		this.colorAlloc.push(false);
	}
	// First color is always white.
	this.colorAlloc[0] = true;
	this.colors = new Float32Array(COLOR_COUNT * 4);
	this.colors.set([1, 1, 1, 1]);
	// Always black
	this.colors2 = new Float32Array(COLOR_COUNT * 4);
	for (i = 0; i < COLOR_COUNT; i++) {
		this.colors2[i*4+3] = 1.0;
	}

	// OpenGL data.
	this.program = null;
	this.vbuffer = null;
	this.vcount = 0;
	this.texture = null;
	this.texscale = vec2.create();
	vec2.set(this.texscale, 1 / font.sx, 1 / font.sy);
}

Text.prototype.layout = function(obj) {
	return new Layout(this, obj);
};

// Emit text graphics data.
Text.prototype.emit = function(gl) {
	var vcount, size, offset, i, layout;

	vcount = 0;
	size = 0;
	for (i = 0; i < this.layouts.length; i++) {
		layout = this.layouts[i];
		if (layout.visible) {
			vcount += layout.vcount;
			size += layout.vdata.byteLength;
		}
	}

	if (!this.vbuffer) {
		this.vbuffer = gl.createBuffer();
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
	gl.bufferData(gl.ARRAY_BUFFER, size, gl.DYNAMIC_DRAW);
	offset = 0;
	for (i = 0; i < this.layouts.length; i++) {
		layout = this.layouts[i];
		if (layout.visible) {
			gl.bufferSubData(gl.ARRAY_BUFFER, offset, layout.vdata);
			offset += layout.vdata.byteLength;
		}
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	this.vcount = vcount;

	if (!this.program) {
		this.program = shader.getProgram(
			gl,
			['text.vert', 'text.frag'],
			['Glyph', 'Style'],
			['VertScale', 'VertOff', 'TexScale', 'Colors']);
		if (!this.program) {
			return;
		}
	}

	if (!this.texture) {
		this.texture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, this.texture);
		gl.texImage2D(
			gl.TEXTURE_2D, 0, gl.LUMINANCE, gl.LUMINANCE,
			gl.UNSIGNED_BYTE, this.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(
			gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
};

// Remove all text layouts.
Text.prototype.clear = function() {
	var i;
	this.vcount = 0;
	this.layouts.length = 0;
	for (i = 1; i < COLOR_COUNT; i++) {
		this.colorAlloc[i] = false;
	}
};

// Draw all text.
Text.prototype.draw = function(time, gl, aspect) {
	if (this.vcount < 0) {
		this.emit(gl);
	}

	if (!this.vcount) {
		return;
	}

	gl.enable(gl.BLEND);
	gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
	gl.useProgram(this.program.program);
	gl.uniform2f(
		this.program.VertScale,
		camera.uiScale[0] / SCREEN_SCALE,
		camera.uiScale[1] / SCREEN_SCALE);
	gl.uniform2fv(this.program.TexScale, this.texscale);
	gl.uniform1i(this.program.Texture, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
	gl.enableVertexAttribArray(0);
	gl.vertexAttribPointer(0, 4, gl.SHORT, false, 12, 0);
	gl.enableVertexAttribArray(1);
	gl.vertexAttribPointer(1, 1, gl.SHORT, false, 12, 8);
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, this.texture);

	gl.uniform2f(
		this.program.VertOff,
		camera.uiScale[0] * SHADOW_X,
		camera.uiScale[1] * SHADOW_Y);
	gl.uniform4fv(this.program.Colors, this.colors2);
	gl.drawArrays(gl.TRIANGLES, 0, this.vcount);
	gl.uniform2f(this.program.VertOff, 0, 0);
	gl.uniform4fv(this.program.Colors, this.colors);
	gl.drawArrays(gl.TRIANGLES, 0, this.vcount);

	gl.disableVertexAttribArray(0);
	gl.disableVertexAttribArray(1);
	gl.disable(gl.BLEND);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.useProgram(null);
};

module.exports = new Text();
