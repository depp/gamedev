'use strict';

var load = require('./load.js');

var serviceInfo;
var serviceCallbacks = [];

load.json('services.json', 'Service information', function(data) {
	var i, callbacks;
	serviceInfo = data;
	callbacks = serviceCallbacks;
	serviceCallbacks = null;
	for (i = 0; i < callbacks.length; i++) {
		callbacks[i]();
	}
});

// Get the URI for the given level.
function levelUri(levelName) {
	var uri = serviceInfo.levels.uri;
	if (!uri) {
		return null;
	}
	if (uri[uri.length-1] != '/') {
		uri += '/';
	}
	return uri + levelName + '.json';
}

function serviceCallback(callback) {
	if (serviceCallbacks) {
		serviceCallbacks.push(callback);
	} else {
		callback();
	}
}

// Load the given level.
function loadLevel(levelName, success, failure) {
	serviceCallback(function() {
		var uri = levelUri(levelName);
		if (uri) {
			load.json(uri, 'Level: ' + levelName, success, failure);
		} else {
			failure({
				msg: 'Level database is configured incorrectly.'
			});
		}
	});
}

// Save the given level to the level database, this will fail in
// production but work in development.
function saveLevel(levelName, data, success, failure) {
	success = success || function() {};
	failure = failure || function() {};
	serviceCallback(function() {
		if (!serviceInfo.levels.writable) {
			failure({
				msg: 'The level database is read-only.'
			});
			return;
		}
		var xhr = new XMLHttpRequest();
		xhr.open('PUT', levelUri(levelName), true);
		xhr.onload = function(e) {
			if (xhr.readyState !== 4) {
				return;
			}
			if (xhr.status < 200 || xhr.status > 299) {
				failure({
					msg: 'Failed to save level data.',
					status: xhr.status
				});
			} else {
				success();
			}
			return;
		};
		xhr.onerror = function(e) {
			failure({
				msg: 'Failed to save level data.',
			});
		};
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify(data));
	});
}

module.exports = {
	loadLevel: loadLevel,
	saveLevel: saveLevel
};
