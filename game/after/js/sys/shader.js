'use strict';

var load = require('./load.js');

var shaders = require('../../shader/shaders.json');

function getShader(gl, name) {
	var src = shaders[name];
	if (!src) {
		throw 'No such shader: ' + name;
	}
	var ext = name.slice(name.lastIndexOf('.') + 1);
	var type;
	switch (ext) {
	case 'vert': type = gl.VERTEX_SHADER; break;
	case 'frag': type = gl.FRAGMENT_SHADER; break;
	default:
		throw 'Unknown shader type.';
	}
	var shader = gl.createShader(type);
	gl.shaderSource(shader, src);
	gl.compileShader(shader);
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		console.log('Errors for shader: ' + name);
		console.log(gl.getShaderInfoLog(shader));
		gl.deleteShader(shader);
		load.fail('Could not load shader: ' + name);
		return null;
	}
	return shader;
}

function getProgram(gl, shaders, attribs, uniforms) {
	var i;
	var name = shaders.join(', ');
	var program = gl.createProgram();
	for (i = 0; i < shaders.length; i++) {
		var shader = getShader(gl, shaders[i]);
		if (!shader) {
			gl.deleteProgram(program);
			return null;
		}
		gl.attachShader(program, shader);
		gl.deleteShader(shader);
	}
	for (i = 0; i < attribs.length; i++) {
		gl.bindAttribLocation(program, i, attribs[i]);
	}
	gl.linkProgram(program);
	if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
		console.log('Errors for shader program: ' + name);
		console.log(gl.getProgramInfoLog(program));
		gl.deleteProgram(program);
		load.fail('Could load shader program: ' + name);
		return null;
	}
	var obj = {program: program};
	for (i = 0; i < uniforms.length; i++) {
		var uname = uniforms[i];
		var loc = gl.getUniformLocation(program, uniforms[i]);
		if (!loc) {
			console.log('Missing uniform: ' + uname+ ' (' + name + ')');
		}
		obj[uname] = loc;
	}
	return obj;
}

module.exports = {
	getProgram: getProgram
};
