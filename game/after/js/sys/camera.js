'use strict';

var glm = require('gl-matrix');
var vec2 = glm.vec2;
var vec3 = glm.vec3;
var vec4 = glm.vec4;
var mat4 = glm.mat4;
var quat = glm.quat;

var vecX = vec3.fromValues(1, 0, 0);
var vecY = vec3.fromValues(0, 1, 0);
var vecZ = vec3.fromValues(0, 0, 1);
var degrees = Math.PI / 180;

var DIRMAP = {right:0, up:1, left:2, down:3};
var DIRS = [{x:-1,y:0},{x:0,y:-1},{x:1,y:0},{x:0,y:1}];

function Camera() {
	this.clear();

	this._quat1 = quat.create();
	this._quat2 = quat.create();
	this._quat3 = quat.create();
	this._loc = vec3.create();

	this._mat1 = mat4.create();
	this._mat2 = mat4.create();

	this.mvp = null;
	this._mvpInv = null;

	this._tmpv = vec4.create();

	this.right = vec3.create();
	this.up = vec3.create();

	this.uiOff = vec2.create();
	this.uiScale = vec2.create();
	this.dirs = {};
	this.updateDirs();
}

// Reset the camera to its default values.
Camera.prototype.clear = function() {
	this.focal = 55;
	this.subjectSize = 1;
	this.subjectPos = vec3.create();
	vec3.set(this.subjectPos, 0, 0, 0);
	this.cameraAspect = 16 / 9;
	this.viewportAspect = 16 / 9;
	this.azimuth = 170;
	this.elevation = 40;
	this.roll = 0;
	this.uiHeight = 360;
};

// Update the camera projection matrix.
Camera.prototype.update3D = function(aspect) {
	var distance;
	{
		var xratio = 18.0 / this.focal;
		var yratio = xratio / this.cameraAspect;
		distance = 0.5 * this.subjectSize / xratio;
		if (aspect > this.cameraAspect) {
			xratio = yratio * aspect;
		} else {
			yratio = xratio / aspect;
		}

		var znear = Math.max(1, distance - 0.5 * this.subjectSize);
		var zfar = distance + 0.5 * this.subjectSize;
		var px = znear * xratio, py = znear * yratio;
		mat4.frustum(this._mat1, -px, +px, -py, +py, znear, zfar);
	}

	quat.setAxisAngle(this._quat1, vecZ, degrees * this.roll);
	quat.setAxisAngle(this._quat2, vecX, degrees * (this.elevation - 90));
	quat.setAxisAngle(this._quat3, vecZ, degrees * (this.azimuth - 180));
	quat.multiply(this._quat1, this._quat1, this._quat2);
	quat.multiply(this._quat1, this._quat1, this._quat3);
	vec3.transformQuat(this._loc, this.subjectPos, this._quat1);
	vec3.negate(this._loc, this._loc);
	this._loc[2] -= distance;
	mat4.fromRotationTranslation(this._mat2, this._quat1, this._loc);

	mat4.multiply(this._mat1, this._mat1, this._mat2);
	this.mvp = this._mat1;
	this._mvpInv = null;

	quat.conjugate(this._quat2, this._quat1);
	vec3.set(this.right, 1, 0, 0);
	vec3.set(this.up, 0, 1, 0);
	vec3.transformQuat(this.right, this.right, this._quat2);
	vec3.transformQuat(this.up, this.up, this._quat2);
};

// Update the camera 2D attributes.
Camera.prototype.update2D = function(aspect) {
	var d;
	if (aspect > this.cameraAspect) {
		d = 2 / this.uiHeight;
		this.uiScale.set([d / aspect, d]);
		this.uiOff.set([-this.cameraAspect / aspect, -1]);
	} else {
		d = 2 / (this.uiHeight * this.cameraAspect);
		this.uiScale.set([d, d * aspect]);
		this.uiOff.set([-1, -aspect / this.cameraAspect]);
	}
};

// Update the 2D directions.
Camera.prototype.updateDirs = function() {
	var off = Math.floor(-this.azimuth / 90 + 0.5) & 3, d;
	for (d in DIRMAP) {
		this.dirs[d] = DIRS[(DIRMAP[d] + off) & 3];
	}
};

// Update the camera attributes.
Camera.prototype.update = function(aspect) {
	this.update3D(aspect);
	this.update2D(aspect);
	this.updateDirs();
};

// Compute the inverse projection of a 2D point in clip space.
// Returns an array of two vec3, defining the line segment in world
// space which projects to the input location.
Camera.prototype.projectInverse = function(x, y) {
	if (this._mvpInv === null) {
		if (this.mvp === null) {
			console.log('Cannot reverse project uninitialized camera.');
			return [vec3.create(), vec3.create()];
		}
		this._mvpInv = this._mat2;
		mat4.invert(this._mvpInv, this.mvp);
	}
	var t = this._tmpv, p = [], i;
	for (i = 0; i < 2; i++) {
		vec4.set(t, x, y, i*2-1, 1);
		vec4.transformMat4(t, t, this._mvpInv);
		var pp = vec3.create();
		var a = 1 / t[3];
		vec3.set(pp, a*t[0], a*t[1], a*t[2]);
		p.push(pp);
	}
	return p;
};

module.exports = new Camera();
