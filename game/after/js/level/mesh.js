'use strict';

var glm = require('gl-matrix');
var vec3 = glm.vec3;
var Collider = require('./collider.js');
var util = require('../util.js');

var TILE_TYPES = [
	'f',
	'r11', 'r12', 'r13',
	'r21', 'r22', 'r23',
	'r31', 'r32', 'r33',
	'r41', 'r42', 'r43'
];

// Generate a random integer in a half-open range.
function randRange(min, max) {
	var x = min + Math.floor(Math.random() * (max - min));
	return Math.min(max - 1, Math.max(min, x));
}

// The X and Y size of each block, in units.
var BLOCK_WIDTH = 64;

// A level mesh.
function Mesh(sx, sy) {
	this.sx_ = sx;
	this.sy_ = sy;
	this.showFloor_ = false;

	// Information about each tile.
	this.type_    = new Uint8Array(sx * sy);
	this.top_     = new Uint8Array(sx * sy);
	this.side_    = new Uint8Array(sx * sy);
	this.rough_   = new Uint8Array(sx * sy);
	this.height_  = new Int16Array(sx * sy);

	// Height of each vertex.
	this.vheight_ = new Uint8Array((sx + 2) * (sy + 2));

	// List of observers.
	this.obs_ = [];

	this.center = vec3.create();
	this.diameter = BLOCK_WIDTH * 10;
}

Mesh.prototype.observe = function(cb) {
	var obs = this.obs_;
	obs.push(cb);
	return {
		remove: function() {
			var i;
			for (i = 0; i < obs.length; i++) {
				if (obs[i] == cb) {
					obs.splice(i, 1);
					return;
				}
			}
		}
	};
};

Mesh.prototype.notify_ = function() {
	var obs = this.obs_, i;
	for (i = 0; i < obs.length; i++) {
		obs('mesh');
	}
};

// Fill the level with random data.
Mesh.prototype.fillRandom = function() {
	var sx = this.sx_, sy = this.sy_;
	var x, y, idx, i, type, top, side, rough, height;
	for (y = 0; y < sy; y++) {
		for (x = 0; x < sx; x++) {
			idx = y*sx + y;
			type = randRange(0, 7);
			if (type === 0) {
				top = side = rough = height = 0;
			} else {
				top = randRange(0, 4);
				side = randRange(0, 2);
				rough = randRange(0, 1);
				height = randRange(0, 16);
			}
			this.type_  [idx] = type;
			this.top_   [idx] = top;
			this.side_  [idx] = side;
			this.rough_ [idx] = rough;
			this.height_[idx] = height;
		}
	}
};

////////////////////////////////////////////////////////////////////////
// Mesh generation
////////////////////////////////////////////////////////////////////////

// Regenerate the vertex heights from the tile information.
Mesh.prototype.regenerate_ = function() {
	var sx = this.sx_, sy = this.sy_;
	var x, y, idx, i, hoff, type, rough, height, z0, z1, z2, z3;
	var x0 = sx, x1 = 0, y0 = sy, y1 = 0;
	for (y = 0; y < sy; y++) {
		for (x = 0; x < sx; x++) {
			idx = y*sx + x;
			type = this.type_[idx];
			if (type === 0) {
				z0 = z1 = z2 = z3 = 0;
			} else {
				x0 = Math.min(x0, x);
				y0 = Math.min(y0, y);
				x1 = Math.max(x1, x+1);
				y1 = Math.max(y1, y+1);
				rough = this.rough_[idx];
				height = this.height_[idx];
				z0 = z1 = z2 = z3 = 16 * (height + 1);
				switch (type) {
				case 2: z0 -= 16; z1 -= 16; z3 += 16; z2 += 16; break;
				case 3: z0 -= 16; z1 += 16; z3 += 16; z2 -= 16; break;
				case 4: z0 += 16; z1 += 16; z3 -= 16; z2 -= 16; break;
				case 5: z0 += 16; z1 -= 16; z3 -= 16; z2 += 16; break;
				}
				if (rough) {
					z0 += randRange(-8, 9);
					z1 += randRange(-8, 9);
					z2 += randRange(-8, 9);
					z3 += randRange(-8, 9);
				}
			}
			hoff = (y+1)*(sx+2)*4 + (x+1)*4;
			this.vheight_[hoff  ] = z0;
			this.vheight_[hoff+1] = z1;
			this.vheight_[hoff+2] = z2;
			this.vheight_[hoff+3] = z3;
		}
	}
	if (x0 < x1) {
		this.center.set([
			(x0 + x1 - sx) * (BLOCK_WIDTH * 0.5),
			(y0 + y1 - sy) * (BLOCK_WIDTH * 0.5),
			BLOCK_WIDTH,
		]);
		this.diameter = Math.hypot(x1 - x0, y1 - y0) * BLOCK_WIDTH;
	} else {
		this.center.set([0, 0, BLOCK_WIDTH]);
		this.diameter = BLOCK_WIDTH * 10;
	}
	this.notify_();
};

// Get texture coordinates from a texture index.
function getTexture(tex, value, x, y, face) {
	var tx0, tx1, ty0, ty1, permute, t;
	if (value < 4) {
		tx0 = 32 * (value & 1);
		tx1 = tx0 + 32;
		ty1 = 32 * ((value >> 1) & 1);
		ty0 = ty1 + 32;
		if (face === 0) {
			permute = randRange(0, 8);
		} else {
			permute = 0;
		}
		if (permute & 1) {
			t = tx0; tx0 = tx1; tx1 = t;
		}
		if (permute & 2) {
			t = ty0; ty0 = ty1; ty1 = t;
		}
		if (permute & 4) {
			tex.set([
				tx0, ty0,
				tx0, ty1,
				tx1, ty0,
				tx1, ty1
			]);
		} else {
			tex.set([
				tx0, ty0,
				tx1, ty0,
				tx0, ty1,
				tx1, ty1
			]);
		}
	} else {
		tx0 = 64 + (value & 1) * 32;
		tx1 = tx0 + 32;
		if ((value >> 1) & 1) {
			switch (face) {
			case 0: ty0 = (x & 1) * 32; ty1 = ty0 + 32; break;
			case 1: ty0 = (x & 1) * 32; ty1 = ty0 + 32; break;
			case 2: ty0 = (y & 1) * 32; ty1 = ty0 + 32; break;
			case 3: ty1 = (x & 1) * 32; ty0 = ty1 + 32; break;
			case 4: ty1 = (y & 1) * 32; ty0 = ty1 + 32; break;
			}
			tex.set([
				tx1, ty1,
				tx1, ty0,
				tx0, ty1,
				tx0, ty0
			]);
		} else {
			if (face === 0) {
				ty0 = (y & 1) * 32;
				ty1 = ty0 + 32;
			} else {
				if (Math.random() > 0.5) {
					t = tx0; tx0 = tx1; tx1 = t;
				}
				ty0 = randRange(0, 64);
				ty1 = ty0 + 32;
			}
			tex.set([
				tx0, ty1,
				tx1, ty1,
				tx0, ty0,
				tx1, ty0
			]);
		}
	}
}

// Get the order in which to traverse neighboring tile heights,
// expressed as array offsets.
//
// Traverses heights in the following order (the 2x2 cells in the
// center are the vertexes of the current tile):
//
// 14-15  9-10
//  |     |  |
// 13-12  8 11
//
//  3  0  4--5
//  |  |     |
//  2--1  7--6
function getZOrder(sx, sy) {
	var d = (sx+2)*4, i, j;
	var zorder = [
		0, -d+2, -d-1, -3,
		1, 4, -d+6, -d+3,
		3, d+1, d+4, 6,
		2, -1, d-3, d,
	];
	for (i = 0; i < 4; i++) {
		j = (i*4+4) & 15;
		zorder.push(zorder[j], zorder[j+3], zorder[j+2], zorder[j+1]);
	}
	return zorder;
}

// Emit the raw mesh data.  The parameter will have the following
// properties, which can be reused for future calls:
//
// - vdata: vertex data
// - idata: index data
// - vcount: number of vertexes
// - icount: number of indexes
// - vindex: map from tiles to indexes
Mesh.prototype.emit = function(obj) {
	var sx = this.sx_, sy = this.sy_;
	var va, ia, ii, vi, vindex;

	/*
	  Maximum size of vertex / index array:
	  - each tile gets 4 / 6 for its top
	  - each corner can have four Z values
	  - each Z value beyond the first results in 2 extra triangles
	  This also works out with the "checker" pattern.
	  Total: 8 / 18 per vertex
	*/
	vi = sx*sy*4 + (sx+1)*(sy+1)*8;
	ii = sx*sy*6 + (sx+1)*(sy+1)*18;
	if (!obj.vdata || obj.vdata.length < vi * 6) {
		obj.vdata = new Uint16Array(util.nextPow2(vi));
	}
	va = obj.vdata;
	if (!obj.idata || obj.idata.length < ii) {
		obj.idata = new Int16Array(util.nextPow2(ii));
	}
	ia = obj.idata;
	if (!obj.vindex || obj.vindex.length < sx * sy) {
		obj.vindex = new Uint16Array(util.nextPow2(sx * sy));
	}
	vindex = obj.vindex;

	var x, y, i, j;
	var w = BLOCK_WIDTH;
	var edgez = new Int16Array(8);
	var tex = new Int16Array(8);
	var zorder = getZOrder(sx, sy);
	var vpos = new Int16Array([
		0, 0,
		BLOCK_WIDTH, 0,
		BLOCK_WIDTH, BLOCK_WIDTH,
		0, BLOCK_WIDTH,
		0, 0
	]);

	vi = 0;
	ii = 0;
	for (y = 0; y < sy; y++) {
		for (x = 0; x < sx; vindex[y*sx+x] = vi, x++) {
			var type, hoff, vx, vy, shade;
			type = this.type_[y*sx+x];
			if (!type && !this.showFloor_) {
				continue;
			}

			hoff = (x+1)*(sy+2)*4 + (y+1)*4;
			vx = (x - sx*0.5) * w;
			vy = (y - sx*0.5) * w;

			// Emit top
			shade = type ? 0 : 5;
			getTexture(tex, this.top_[y*sx+x], x, y, 0);
			this.idata_.set([
				vi+0, vi+1, vi+2,
				vi+2, vi+1, vi+3,
			], ii);
			ii += 6;
			this.vdata_.set([
				vx,   vy,   this.vheight_[hoff  ], shade, tex[0], tex[1],
				vx+w, vy,   this.vheight_[hoff+0], shade, tex[2], tex[3],
				vx,   vy+w, this.vheight_[hoff+1], shade, tex[4], tex[5],
				vx+w, vy+w, this.vheight_[hoff+2], shade, tex[6], tex[7]
			], vi*6);
			vi += 4;

			if (!type) {
				continue;
			}

			// Emit sides
			var sideTex = this.side_[y*sx+x];
			for (i = 0; i < 4; i++) {
				var n0, n1;
				// Get edge Z values
				for (j = 0; j < 2; j++) {
					var zoff = j*16 + i*4;
					var z0 = this.vheight_[hoff + zorder[zoff+0]];
					var z1 = this.vheight_[hoff + zorder[zoff+1]];
					var z2 = this.vheight_[hoff + zorder[zoff+2]];
					var z3 = this.vheight_[hoff + zorder[zoff+3]];
					var n = 4*j;
					edgez[n++] = z0;
					if (z1 < z0) {
						if (z2 < z0 && z2 > z1) {
							if (z3 < z0 && z3 > z2) {
								edgez[n++] = z3;
							}
							edgez[n++] = z2;
						}
						edgez[n++] = z1;
					}
					if (j === 0) {
						n0 = n;
					} else {
						n1 = n - 4;
					}
				}
				if (n0 <= 1 && n1 <= 1) {
					continue;
				}

				// Emit vertex data
				var v0 = vi, v1 = vi + n0;
				var ex, ey, ez, tx, ty, txz, tyz;
				getTexture(tex, sideTex, x, y, i + 1);
				shade = i + 1;

				txz = (tex[4] - tex[0]) * (1/64);
				tyz = (tex[5] - tex[1]) * (1/64);

				ex = vx + vpos[i*2  ];
				ey = vy + vpos[i*2+1];
				tx = tex[0];
				ty = tex[1];
				for (j = 0; j < n0; j++) {
					ez = edgez[j];
					this.vdata_.set([
						ex, ey, ez, shade,
						tx + Math.floor(txz * ez),
						ty + Math.floor(tyz * ez)
					], vi*6);
					vi++;
				}

				ex = vx + vpos[i*2+2];
				ey = vy + vpos[i*2+3];
				tx = tex[2];
				ty = tex[3];
				for (j = 0; j < n1; j++) {
					ez = edgez[j+4];
					this.vdata_.set([
						ex, ey, ez, shade,
						tx + Math.floor(txz * ez),
						ty + Math.floor(tyz * ez)
					], vi*6);
					vi++;
				}

				// Emit index data
				var i0 = 1, i1 = 1;
				while (true) {
					var left;
					if (i0 < n0) {
						if (i1 < n1) {
							left = edgez[i0] >= edgez[4+i1];
						} else {
							left = true;
						}
					} else {
						if (i1 < n1) {
							left = false;
						} else {
							break;
						}
					}
					if (left) {
						this.idata_.set([
							v1 + i1 - 1,
							v0 + i0 - 1,
							v0 + i0
						], ii);
						i0++;
					} else {
						this.idata_.set([
							v1 + i1 - 1,
							v0 + i0 - 1,
							v1 + i1
						], ii);
						i1++;
					}
					ii += 3;
				}
			}
		}
	}

	obj.vcount = vi;
	obj.icount = ii;
};

////////////////////////////////////////////////////////////////////////
// Queries
////////////////////////////////////////////////////////////////////////

// Get the loctaion of a tile's center.
Mesh.prototype.tileLoc = function(loc, x, y) {
	var sx = this.sx_, sy = this.sy_, z = 0;
	if (x >= 0 && x < sx && y >= 0 && y < sy) {
		var h = this.vheight_;
		var hoff = (y+1)*(sx+2)*4 + (x+1)*4;
		z = (h[hoff] + h[hoff+1] + h[hoff+2] + h[hoff+3]);
	}
	loc[0] = (x + 0.5 - this.sx_ * 0.5) * BLOCK_WIDTH;
	loc[1] = (y + 0.5 - this.sy_ * 0.5) * BLOCK_WIDTH;
	loc[2] = z * 0.25;
};

// Get the location of a tile's edge.
Mesh.prototype.edgeLoc = function(loc, x, y, x1, y1) {
	var sx = this.sx_, sy = this.sy_, z = 0;
	if (x >= 0 && x < sx && y >= 0 && y < sy) {
		var h = this.vheight_;
		var hoff = (y+1)*(sx+2)*4 + (x+1)*4;
		if (x1 != x) {
			if (x1 > x) {
				z = h[hoff+1] + h[hoff+3];
			} else {
				z = h[hoff+0] + h[hoff+2];
			}
		} else {
			if (y1 > y) {
				z = h[hoff+2] + h[hoff+3];
			} else {
				z = h[hoff+0] + h[hoff+1];
			}
		}
	}
	loc[0] = (x + x1 + 1 - this.sx_) * (0.5 * BLOCK_WIDTH);
	loc[1] = (y + y1 + 1 - this.sy_) * (0.5 * BLOCK_WIDTH);
	loc[2] = z * 0.5;
};

////////////////////////////////////////////////////////////////////////
// Modification
////////////////////////////////////////////////////////////////////////

// Set an array to the value, masked by a selection.  Does not call
// notifications.
Mesh.prototype.setArray_ = function(mask, array, value) {
	if (typeof value !== 'number') {
		return;
	}
	var n = this.sx_ * this.sy_, i, changed = false;
	if (mask.length != n) {
		console.error('Invalid mask size');
		return;
	}
	for (i = 0; i < n; i++) {
		if (!mask[i] || !this.type_[i]) {
			continue;
		}
		if (array[i] !== value) {
			array[i] = value;
			changed = true;
		}
	}
	return changed;
};

// Modify the selection's top texture.
Mesh.prototype.setTexture = function(mask, params) {
	var changed = false;
	if (this.setArray_(mask, this.top_, params.top)) {
		changed = true;
	}
	if (this.setArray_(mask, this.side_, params.side)) {
		changed = true;
	}
	if (changed) {
		this.regenerate_();
	}
	return changed;
};

// Change whether the given tiles are rough.
Mesh.prototype.setRough = function(mask, value) {
	var changed = this.setArray_(mask, this.tileRough, value ? 1 : 0);
	if (changed) {
		this.regenerate_();
	}
	return changed;
};

// Set the type of the given tiles.
Mesh.prototype.setType = function(mask, value) {
	var n = this.sx_ * this.sy_, i, changed = false, type;
	if (mask.length != n) {
		console.error('Invalid mask size');
		return;
	}
	if (value === null) {
		for (i = 0; i < n; i++) {
			if (!mask[i] || this.type_[i]) {
				continue;
			}
			this.type_[i]   = 0;
			this.top[i]     = 0;
			this.side_[i]   = 0;
			this.rough_[i]  = 0;
			this.height_[i] = 0;
			changed = true;
		}
	} else {
		type = TILE_TYPES.indexof(type);
		if (type < 0) {
			console.error('Invalid tile type');
			return;
		}
		for (i = 0; i < n; i++) {
			if (!mask[i] || this.type_[i] == type) {
				continue;
			}
			this.type_[i] = type;
			changed = true;
		}
	}
	if (changed) {
		this.regenerate_();
	}
	return changed;
};

// Modify the selection's height.
Mesh.prototype.addHeight = function(mask, amt) {
	var n = this.sx_ * this.sy_, i, changed = false, v1, v2;
	if (mask.length != n) {
		console.error('Invalid mask size');
		return;
	}
	for (i = 0; i < n; i++) {
		if (!mask[i] || !this.type_[i]) {
			continue;
		}
		v1 = this.height_[i];
		v2 = Math.max(0, Math.min(15, v1 + amt));
		if (v1 != v2) {
			this.height_[i] = v2;
			changed = true;
		}
	}
	if (changed) {
		this.regenerate_();
	}
	return changed;
};

////////////////////////////////////////////////////////////////////////
// Saving and loading
////////////////////////////////////////////////////////////////////////

// Load mesh data from its JSON representation.
Mesh.prototype.load = function(data) {
	var sx = this.sx_, sy = this.sy_;
	var rows = data.tiles || [];
	var x, y, i, row, idx, tile;
	for (y = 0; y < sy; y++) {
		i = rows.length - 1 - y;
		if (i < 0) {
			continue;
		}
		row = rows[i].split(' ');
		for (x = 0; x < row.length; x++) {
			if (x < 0 || x >= sx) {
				continue;
			}
			idx = y*sx + x;
			if (row[x] == '.') {
				continue;
			}
			this.type_[idx]   = (tile      ) &  7;
			this.rough_[idx]  = (tile >>  3) &  1;
			this.top_[idx]    = (tile >>  4) & 15;
			this.side_[idx]   = (tile >>  8) & 15;
			this.height_[idx] = (tile >> 12) & 15;
		}
	}
	this.regenerate_();
};

// Dump mesh data to its JSON representation.
Mesh.prototype.dump = function(data, rect) {
	var sx = this.sx, sy = this.sy;
	var x0 = 0, y0 = 0, x1 = sx, y1;
	if (rect) {
		x0 = rect.x0; y0 = rect.y0; x1 = rect.x1; y1 = rect.y1;
	}
	var x, y, idx, tile;
	var rows = [], row = [];
	for (y = y0; y < y1; y++) {
		if (y < 0 || y >= sy) {
			for (x = x0; x < x1; x++) {
				row[x - x0] = '.';
			}
		} else {
			x = x0;
			for (; x < 0 && x < x1; x++) {
				row[x - x0] = '.';
			}
			for (; x < sx && x < x1; x++) {
				idx = y*sx+x;
				tile = (((this.type_  [idx] &  7)      ) |
						((this.rough_ [idx] &  1) <<  3) |
						((this.top_   [idx] & 15) <<  4) |
						((this.side_  [idx] & 15) <<  8) |
						((this.height_[idx] & 15) << 12));
				row[x - x0] = tile.toString(16);
			}
			for (; x < x1; x++) {
				row[x - x0] = '.';
			}
		}
		rows.push(row.join(' '));
	}
	rows.reverse();
	data.tiles = rows;
};

// Create a collider for the level mesh.
Mesh.prototype.createCollider = function() {
	return new Collider(this.sx_, this.sy_, this.heights_);
};

module.exports = Mesh;
