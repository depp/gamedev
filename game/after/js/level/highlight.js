'use strict';

var util = require('../util.js');

// The number of highlight colors.
var NUM_COLORS = 8;

// A highlighted part of a level.
function Highlight(sx, sy) {
	this.sx_ = sx;
	this.sy_ = sy;
	this.speed_ = Math.PI * 2;
	this.highlightSides_ = false;
	this.colors_ = null;
	this.data = null;
	this.empty_ = true;
}

// Create a copy of the highlight set.
Highlight.prototype.copy = function() {
	var copy = new Highlight(this.sx_, this.sy_);
	copy.speed_ = this.speed_;
	copy.highlightSides = this.highlightSides_;
	copy.colors_ = new Float32Array(this.colors_);
	copy.data = new Uint8Array(this.data);
	return copy;
};

Highlight.prototype.observe = function(cb) {
	var obs = this.obs_;
	obs.push(cb);
	return {
		remove: function() {
			var i;
			for (i = 0; i < obs.length; i++) {
				if (obs[i] == cb) {
					obs.splice(i, 1);
					return;
				}
			}
		}
	};
};

Highlight.prototype.notify_ = function(param) {
	var obs = this.obs_, i;
	for (i = 0; i < obs.length; i++) {
		obs(param);
	}
};

// Emit the contents of the highlight buffer.  The parameter will have
// the folliwng properties, which can be reused for future calls:
//
// - vdata: highlight vertex data
// - colors: (write-only) color data
// - speed: (write-only) blink speed
// - vindex: (read-only) map from tiles to indexes
Highlight.prototype.emit = function(obj) {
	var n = obj.vindex.length;
	if (obj.vindex.length != this.sx_ * this.sy_) {
		console.error('Mismatched highlight buffer');
		return;
	}

	var ia = obj.vindex, da = this.data;
	var vn = obj.vindex[n - 1], va;
	if (!obj.vdata || obj.vdata.length < vn) {
		obj.vdata = new Int16Array(util.nextPow2(vn));
	}
	va = obj.vdata;

	var i, j, vi = 0, ve, c;
	if (this.highlightSides_) {
		for (i = 0; i < n; i++) {
			ve = ia[i];
			c = da[i];
			if (vi + 4 > ve) {
				vi = ve;
				continue;
			}
			for (j = 0; j < 4; j++) {
				va[vi+j] = c;
			}
			for (vi += 4; vi < ve; vi++) {
				va[vi] = 0;
			}
		}
	} else {
		for (i = 0; i < n; i++) {
			ve = ia[i];
			c = da[i];
			for (; vi < ve; vi++) {
				va[vi] = c;
			}
		}
	}
};

// Remove the highlight from all tiles.
Highlight.prototype.clear = function() {
	if (!this.data) {
		this.colors_ = new Float32Array(NUM_COLORS * 4);
		this.data = new Uint8Array(this.sx * this.sy);
		this.empty_ = true;
	} else if (!this.empty_) {
		this.data.fill(0);
		this.empty_ = true;
		this.notify_();
	}
};

// Set the speed at which the blinking tiles blink, in Hz.
Highlight.prototype.setSpeed = function(value) {
	var nvalue = value * (Math.PI * 2);
	if (this.speed_ != nvalue) {
		this.speed_ = nvalue;
		this.notify_();
	}
};

// Set whether the full tiles are highlighted, instead of just the
// top.
Highlight.prototype.setHighlightSides = function(value) {
	var nvalue = !!value;
	if (this.highlightSides_ != nvalue) {
		this.highlightSides_ = nvalue;
		this.notify_();
	}
};

// Set a highlight color.  If two colors are specified, the tiles will
// fade between the two colors.  A null color is taken to be
// transparent.
Highlight.prototype.setColor = function(which, color1, color2) {
	var c, r, g, b, a;
	if (which < 0 || which >= NUM_COLORS) {
		console.error('invalid highlight index');
		return;
	}

	if (color1) {
		c = color1.toRGB();
		a = c.a;
		r = c.r * a;
		g = c.g * a;
		b = c.b * a;
	} else {
		r = g = b = a = 0;
	}
	this.colors_[which*8+0] = r;
	this.colors_[which*8+1] = g;
	this.colors_[which*8+2] = b;
	this.colors_[which*8+3] = a;

	if (color2 !== undefined) {
		if (color2) {
			c = color2.toRGB();
			a = c.a;
			r = c.r * a;
			g = c.g * a;
			b = c.b * a;
		} else {
			r = g = b = a = 0;
		}
	}
	this.colors_[which*8+4] = r;
	this.colors_[which*8+5] = g;
	this.colors_[which*8+6] = b;
	this.colors_[which*8+7] = a;

	this.notify_();
};

// Get level highlight.
Highlight.prototype.getTile = function(x, y) {
	var sx = this.sx_, sy = this.sy_;
	if (x >= 0 && x < sx && y >= 0 && y < sy) {
		return this.data[y*sx+x];
	}
	return 0;
};

// Set level highlight for an individual tile.
Highlight.prototype.setTile = function(x, y, value) {
	var changed = false;
	var sx = this.sx_, sy = this.sy_;
	if (x >= 0 && x < sx && y >= 0 && y < sy) {
		if (this.data[y*sx+x] != value) {
			this.data[y*sx+x] = value;
			changed = true;
		}
	}
	if (changed) {
		this.notify_();
	}
	return changed;
};

// Set level highlight for a rectangle.
Highlight.prototype.setRect = function(x0, y0, x1, y1, value) {
	var changed = false;
	var sx = this.sx_, sy = this.sy_, x, y;
	x0 = Math.max(x0, 0);
	y0 = Math.max(y0, 0);
	x1 = Math.min(x1, sx);
	y1 = Math.min(y1, sy);
	for (y = y0; y < y1; y++) {
		for (x = x0; x < x1; x++) {
			if (this.tileSel[y*sx+x] != value) {
				this.tileSel[y*sx+x] = value;
				changed = true;
			}
		}
	}
	if (changed) {
		this.notify_();
	}
	return changed;
};

// Set level highlight from a flattened list of points.
Highlight.prototype.setPoints = function(points, value) {
	var changed = false;
	var sx = this.sx_, sy = this.sy_, i, x, y;
	for (i = 0; i < value.length - 1; i += 2) {
		x = points[i+0];
		y = points[i+1];
		if (x >= 0 && x < sx && y >= 0 && y < sy) {
			if (this.data[y*sx+x] != value) {
				this.data[y*sx+x] = value;
				changed = true;
			}
		}
	}
	if (changed) {
		this.notify_();
	}
	return changed;
};

module.exports = Highlight;
