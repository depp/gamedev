'use strict';

var shader = require('../sys/shader.js');
var camera = require('../sys/camera.js');
var load = require('../sys/load.js');

var SHADES = new Float32Array([
	1.0, 1.0, 1.0, 1.0,
	0.8, 0.8, 0.8, 1.0,
	1.0, 0.8, 0.8, 1.0,
	0.8, 1.0, 0.8, 1.0,
	0.8, 0.8, 1.0, 1.0,
	0.3, 0.3, 0.3, 1.0
]);

var HIGHLIGHTS = new Float32Array(8);

function LevelRenderer() {
	// Mesh data
	this.hobj_ = null;
	this.mobs_ = null;
	this.mdata_ = {
		vdata: null,
		idata: null,
		vcount: 0,
		icount: 0,
		vindex: null
	};
	this.vindex_ = null;

	// Highlight data
	this.hobj_ = null;
	this.hobs_ = null;
	this.hdata = {
		vdata: null,
		colors: null,
		speed: 0,
		vindex: 0
	};

	// Tileset data
	this.texImage_ = null;
	this.texName_ = null;
	this.texLoaded_ = false;

	// OpenGL resources
	this.program_ = null;
	this.vbuffer1_ = null;
	this.vbuffer2_ = null;
	this.ibuffer_ = null;
	this.vcount_ = 0;
	this.icount_ = 0;
	this.texture_ = null;

	// Data valid flags
	this.valid_ = false;
	this.validProgram_ = false;
	this.validHighlight_ = true;
	this.validMesh_ = true;
	this.validTexture_ = true;
}

LevelRenderer.prototype.emit_ = function(gl) {
	this.valid_ = true;
	if (!this.validProgram_) {
		this.validProgram_ = true;
		this.program_ = shader.getProgram(
			gl,
			['level.vert', 'level.frag'],
			['Pos', 'TexCoord', 'Highlight', 'Blink'],
			['MVP', 'Shades', 'Highlights', 'Texture']);
	}
	if (!this.program_ || !this.mesh_) {
		return;
	}

	if (!this.validMesh_) {
		this.validMesh_ = true;
		this.mesh_.emit(this.mdata_);

		if (!this.vbuffer_) {
			this.vbuffer_ = gl.createBuffer();
		}
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer_);
		gl.bufferData(
			gl.ARRAY_BUFFER,
			this.mdata_.vdata.subarray(this.mdata_.vcount * 6),
			gl.STATIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		if (!this.ibuffer) {
			this.ibuffer = gl.createBuffer();
		}
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);
		gl.bufferData(
			gl.ELEMENT_ARRAY_BUFFER,
			this.mdata_.idata.subarray(this.mdata_.icount),
			gl.STATIC_DRAW);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

		this.highlightValid_ = false;
	}

	if (!this.validHighlight_) {
		this.validHighlight_ = true;
		if (this.hobj_) {
			this.hobj_.emit(this.hdata_);

			if (!this.vbuffer2) {
				this.vbuffer2 = gl.createBuffer();
			}

			gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer2);
			gl.bufferData(
				gl.ARRAY_BUFFER,
				this.hdata_.vdata.subarray(0, this.mdata_.vcount),
				gl.DYNAMIC_DRAW);
			gl.bindBuffer(gl.ARRAY_BUFFER, null);
		}
	}

	if (!this.validTexture_) {
		this.validTexture_ = true;
		if (!this.texture) {
			this.texture = gl.createTexture();
		}

		if (this.textureImage_) {
			gl.bindTexture(gl.TEXTURE_2D, this.texture);
			gl.texImage2D(
				gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE,
				this.texImage_);
			gl.texParameteri(
				gl.TEXTURE_2D,
				gl.TEXTURE_MAG_FILTER,
				gl.LINEAR);
			gl.texParameteri(
				gl.TEXTURE_2D,
				gl.TEXTURE_MIN_FILTER,
				gl.LINEAR_MIPMAP_LINEAR);
			gl.generateMipmap(gl.TEXTURE_2D);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
	}
};

LevelRenderer.prototype.render = function(time, gl, aspect) {
	if (!this.valid_) {
		this.emit_(gl);
	}

	if (!this.program_ || !this.mesh_) {
		return;
	}

	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	gl.useProgram(this.program.program);

	gl.uniformMatrix4fv(this.program.MVP, false, camera.mvp);
	gl.uniform4fv(this.program.Shades, SHADES);
	gl.uniform1i(this.program.Texture, 0);
	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, this.texture_);
	gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer1);
	gl.enableVertexAttribArray(0);
	gl.vertexAttribPointer(0, 4, gl.SHORT, false, 12, 0);
	gl.enableVertexAttribArray(1);
	gl.vertexAttribPointer(1, 2, gl.SHORT, false, 12, 8);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibuffer);

	if (this.hobj_) {
		gl.uniform4fv(this.program.Highlights, this.hdata_.colors);
		gl.uniform1f(
			this.program.Blink,
			1 - 0.5 * Math.cos(this.hdata_.speed * 1e-3 * time));
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer2);
		gl.enableVertexAttribArray(2);
		gl.vertexAttribPointer(2, 1, gl.SHORT, false, 2, 0);
	} else {
		gl.uniform4fv(this.program.Highlights, HIGHLIGHTS);
		gl.uniform1f(this.program.Blink, 0);
		gl.vertexAttrib1f(2, 0);
	}

	gl.drawElements(gl.TRIANGLES, this.icount, gl.UNSIGNED_SHORT, 0);

	gl.disableVertexAttribArray(0);
	gl.disableVertexAttribArray(1);
	gl.disableVertexAttribArray(2);
	gl.disable(gl.DEPTH_TEST);
	gl.disable(gl.CULL_FACE);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
	gl.useProgram(null);
};

LevelRenderer.prototype.clear = function() {
	this.setLevel(null);
};

LevelRenderer.prototype.setHighlight = function(highlight) {
	if (this.hobj_) {
		this.hobs_.remove();
		this.hobj_ = null;
		this.hobs_ = null;
		this.validHighlight_ = false;
	}
	if (highlight) {
		this.hobj_ = highlight;
		this.hobs_ = this.hobj_.observe(function() {
			this.validHighlight_ = false;
			this.valid_ = false;
		}.bind(this));
		this.validHighlight_ = false;
	}
};

LevelRenderer.prototype.setLevel = function(level) {
	var mesh, texName;
	this.setHighlight(null);

	if (level) {
		mesh = level.mesh;
		texName = level.tileSet;
	} else {
		mesh = null;
		texName = null;
	}

	if (this.mobj_) {
		this.mobs_.remove();
		this.mobj_ = null;
		this.valid_ = this.validMesh_ = false;
	}
	if (mesh) {
		this.mobj_ = mesh;
		this.mobs_ = mesh.observe(function() {
			this.valid_ = this.validMesh_ = false;
		}.bind(this));
	}

	if (texName) {
		if (texName !== this.texName) {
			this.texImage_ = load.image(texName + '.png');
			this.texName_ = texName;
			this.valid_ = this.validTexture_ = false;
		}
	} else if (!this.validTexture_) {
		this.texImage_ = null;
		this.texName = null;
		this.validTexture_ = true;
	}
};

module.exports = LevelRenderer;
