'use strict';

/*
Hit testing.

The result is null if the trace hits nothing.  The result will be
non-null, however, if the trace hits an 'emtpy' tile or goes out of
bounds and hits the ground plane.

result object properties:
    x    - x tile pos
	y    - y tile pos
	t    - fraction of the trace that completed, 0 <= t <= 1
	loc  - vec3 impact location
    obj  - type of object impacted, currently 'side' or 'top'
*/

var glm = require('gl-matrix');
var vec3 = glm.vec3;

// The X and Y size of each block, in units.
var BLOCK_WIDTH = 64;

// Level mesh collider.
function Collider(sx, sy, blockWidth, heights) {
	this.sx_ = sx;
	this.sy_ = sy;
	this.x0_ = sx * (BLOCK_WIDTH / 2);
	this.y0_ = sy * (BLOCK_WIDTH / 2);
	this.heights_ = heights;
}

// Trace a line through the level.  The line is specified as an array
// of two points in world space.
Collider.prototype.traceLine = function(line) {
	var heights = this.heights_;
	var x0 = line[0][0], y0 = line[0][1], z0 = line[0][2];
	var x1 = line[1][0], y1 = line[1][1], z1 = line[1][2];
	x0 = (x0 + this.x0_) * (1 / BLOCK_WIDTH);
	x1 = (x1 + this.x0_) * (1 / BLOCK_WIDTH);
	y0 = (y0 + this.y0_) * (1 / BLOCK_WIDTH);
	y1 = (y1 + this.y0_) * (1 / BLOCK_WIDTH);
	var bx0 = Math.floor(x0), bx1 = Math.floor(x1);
	var by0 = Math.floor(y0), by1 = Math.floor(y1);
	var xdir = x1 > x0 ? 1 : 0, ydir = y1 > y0 ? 1 : 0;
	var tdx = 1 / Math.abs(x1 - x0), tdy = 1 / Math.abs(y1 - y0);
	var tnx = (bx0 + xdir - x0) / (x1 - x0);
	var tny = (by0 + ydir - y0) / (y1 - y0);
	var x = bx0, y = by0, i, n = Math.abs(bx1 - bx0) + Math.abs(by1 - by0);
	var sx = this.sx, sy = this.sy;
	var loc = vec3.create();
	var hoff;
	for (i = 0; i < n; i++) {
		var t, nx = x, ny = y, et, px, py, pz, h0, h1, h2, h3, h;
		if (tnx < tny) {
			t = tnx;
			tnx += tdx;
			px = x + xdir;
			py = y0 + t*(y1 - y0);
			py = Math.min(y+1, Math.max(y, py));
			et = py - y;
			if (xdir === 0) {
				h0 = 0;
				h2 = 1;
				nx--;
			} else {
				h0 = 1;
				h1 = 0;
				nx++;
			}
			h1 = h0 + 2;
			h3 = h2 + 2;
		} else {
			t = tny;
			tny += tdy;
			px = x0 + t*(x1 - x0);
			py = y + ydir;
			px = Math.min(x+1, Math.max(x, px));
			et = px - x;
			if (ydir === 0) {
				h0 = 0;
				h2 = 2;
				ny--;
			} else {
				h0 = 2;
				h2 = 0;
				ny++;
			}
			h1 = h0 + 1;
			h3 = h2 + 1;
		}
		pz = z0 + t*(z1 - z0);
		hoff = (x+1)*(sy+2)*4 + (y+1)*4;
		if (x >= 0 && x < sx && y >= 0 && y < sy ) {
			h0 = heights[hoff + h0];
			h1 = heights[hoff + h1];
			h = h0 + et*(h1 - h0);
			if (pz < h) {
				h0 = heights[hoff];
				h1 = heights[hoff+1];
				h2 = heights[hoff+2];
				h3 = heights[hoff+3];
				var v0 = vec3.create(), v1 = vec3.create(), v2 = vec3.create();
				var normal = vec3.create();
				if (nx > x || ny > y) {
					vec3.set(
						v0,
						(x + 1) * BLOCK_WIDTH - this.x0_,
						(y + 1) * BLOCK_WIDTH - this.y0_,
						h3);
					vec3.set(v1, -BLOCK_WIDTH, 0, h2 - h3);
					vec3.set(v2, 0, -BLOCK_WIDTH, h1 - h3);
				} else {
					vec3.set(
						v0,
						x * BLOCK_WIDTH - this.x0_,
						y * BLOCK_WIDTH - this.y0_,
						h0);
					vec3.set(v1, BLOCK_WIDTH, 0, h1 - h0);
					vec3.set(v2, 0, BLOCK_WIDTH, h2 - h0);
				}
				vec3.cross(normal, v1, v2);
				vec3.scale(normal, normal, 1 / (BLOCK_WIDTH * BLOCK_WIDTH));
				vec3.subtract(v1, v0, line[0]);
				vec3.subtract(v2, line[1], line[0]);
				t = vec3.dot(v1, normal) / vec3.dot(v2, normal);
				if (!(t >= 0 && t <= 1)) {
					t = 1;
				}
				vec3.scale(v2, v2, t);
				vec3.add(loc, line[0], v2);
				if (false) {
					var lx = (loc[0] + this.x0_) * (1 / BLOCK_WIDTH);
					var ly = (loc[1] + this.y0_) * (1 / BLOCK_WIDTH);
					console.log(x, y, lx, ly);
				}
				return {
					x: x,
					y: y,
					t: t,
					loc: loc,
					obj: 'top'
				};
			}
		} else {
			if (pz < 0) {
				t = -z0 / (z1 - z0);
				px = Math.min(x+1, Math.max(x, t*(x1 - x0)));
				py = Math.min(y+1, Math.max(y, t*(y1 - y0)));
				px = px * BLOCK_WIDTH - this.x0_;
				py = py * BLOCK_WIDTH - this.y0_;
				vec3.set(loc, px, py, 0);
				return {
					x: x,
					y: y,
					t: t,
					loc: loc,
					obj: 'top'
				};
			}
		}
		hoff = (nx+1)*(sy+2)*4 + (ny+1)*4;
		if (nx >= 0 && nx < sx && ny >= 0 && ny < sy) {
			h2 = heights[hoff + h2];
			h3 = heights[hoff + h3];
			h = h2 + et*(h3 - h2);
			if (pz < h) {
				px = px * BLOCK_WIDTH - this.x0_;
				px = py * BLOCK_WIDTH - this.y0_;
				vec3.set(loc, px, py, pz);
				return {
					x: nx,
					y: ny,
					t: t,
					loc: loc,
					obj: 'side'
				};
			}
		}
		x = nx;
		y = ny;
	}
	return null;
};

module.exports = Collider;
