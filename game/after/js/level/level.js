'use strict';

var load = require('../sys/load.js');
var services = require('../sys/services.js');

var Mesh = require('./mesh.js');
var Spawn = require('./spawn.js');
var Highlight = require('./highlight.js');

var TILE_SETS = [
	'tile0',
	'tile1'
];

////////////////////////////////////////////////////////////////////////

// Level data.
function Level(sx, sy) {
	this.sx_ = 0;
	this.sy_ = 0;
	this.mesh = new Mesh(sx, sy);
	this.collider = this.mesh.createCollider();
	this.spawn = new Spawn(sx, sy);
	this.tileSet = null;

	this.mesh.observe(this.notify.bind(this));
	this.spawn.observe(this.notify.bind(this));
}

Level.prototype.observe = function(cb) {
	var obs = this.obs_;
	obs.push(cb);
	return {
		remove: function() {
			var i;
			for (i = 0; i < obs.length; i++) {
				if (obs[i] == cb) {
					obs.splice(i, 1);
					return;
				}
			}
		}
	};
};

Level.prototype.notify_ = function(param) {
	var obs = this.obs_, i;
	for (i = 0; i < obs.length; i++) {
		obs(param);
	}
};

Level.prototype.setTileSet = function(tileSet) {
	if (this.tileSet !== tileSet) {
		this.tileSet = tileSet;
		this.notify_('tileSet');
	}
};

Level.prototype.createHighlight = function() {
	return new Highlight(this.sx_, this.sy_);
};

////////////////////////////////////////////////////////////////////////
// Saving, loading, and creating
////////////////////////////////////////////////////////////////////////

// Load a level from its JSON representation.
Level.prototype.load = function(data) {
	this.tileSet = data.tileSet;
	this.mesh.load(data);
	this.spawn.load(data);
};

// Serialize a level to its JSON representation.
Level.prototype.dump = function(data, rect) {
	if (rect) {
		data.sx = rect.x1 - rect.x0;
		data.sy = rect.y1 - rect.y0;
	}
	data.tileSet = this.tileSet;
	this.mesh.dump(data, rect);
	this.spawn.dump(data, rect);
};

Level.prototype.save = function(levelName, success, failure) {
	var data = {};
	this.dump(data, null);
	services.saveLevel(levelName, data, success, failure);
};

function create(sx, sy) {
	return new Level(sx, sy);
}

function load(levelName, success, failure) {
	services.loadLevel(levelName, function(data) {
		var level;
		var valid =
			typeof data.sx === 'number' && data.sx > 0 &&
			typeof data.sy === 'number' && data.sy > 0;
		if (!valid) {
			failure({msg: 'Invalid level data'});
		} else {
			level = new Level(data.sx, data.sy);
			level.load(data);
			success(level);
		}
	});
}

////////////////////////////////////////////////////////////////////////

module.exports = {
	TILE_SETS: TILE_SETS,
	create: create,
	load: load
};
