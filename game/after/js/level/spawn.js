'use strict';

// A set of spawn points for a level.
function Spawn(sx, sy) {
	this.sx_ = sx;
	this.sy_ = sy;
	this.points_ = {};
	this.obs_ = [];
}

Spawn.prototype.observe = function(cb) {
	var obs = this.obs_;
	obs.push(cb);
	return {
		remove: function() {
			var i;
			for (i = 0; i < obs.length; i++) {
				if (obs[i] == cb) {
					obs.splice(i, 1);
					return;
				}
			}
		}
	};
};

Spawn.prototype.notify_ = function() {
	var obs = this.obs_, i;
	for (i = 0; i < obs.length; i++) {
		obs('spawn');
	}
};

// Add the masked tiles to the given set of spawn points.
Spawn.prototype.add = function(mask, index) {
	var sx = this.sx_, sy = this.sy_, n = sx * sy, i, x, y;
	var points, npoints, arr;
	if (mask.length !== n) {
		console.error('Incorrect spawn mask');
		return;
	}
	arr = new Uint8Array(mask);
	points = this.points_[index];
	if (points) {
		for (i = 0; i < points.length - 1; i += 2) {
			x = points[i+0];
			y = points[i+1];
			arr[y*sx+x] = 1;
		}
	}
	npoints = [];
	for (y = 0; y < sy; y++) {
		for (x = 0; x < sx; x++) {
			if (arr[y*sx+x]) {
				npoints.push(x, y);
			}
		}
	}
	var changed = npoints.length == (points ? points.length : 0);
	if (changed) {
		this.points_[index] = npoints;
		this.notify_();
	}
	return changed;
};

// Remove the masked tiles from the given set of spawn points.  If the
// index is null, then all spawn points are removed from the masked
// location.
Spawn.prototype.remove = function(mask, index) {
	var changed = false;
	var sx = this.sx_, sy = this.sy_, n = sx * sy;
	var i, j, idxs, arr = null, points, npoints, x, y;
	if (mask.length !== n) {
		console.error('Incorrect spawn mask');
		return;
	}
	if (index === null) {
		idxs = Object.keys(this.points);
	} else {
		idxs = this.points_[index] ? [index] : [];
	}
	if (idxs.length === 0) {
		return false;
	}
	arr = new Uint8Array(n);
	for (i = 0; i < idxs.length; i++) {
		points = this.points_[idxs[i]];
		npoints = [];
		for (j = 0; j < points.length - 1; j += 2) {
			x = points[j+0];
			y = points[j+1];
			if (x < 0 || x >= sx || y < 0 || y >= sx) {
				changed = true;
				continue;
			}
			if (!mask[y*sy+x]) {
				changed = true;
				continue;
			}
			npoints.push(x, y);
		}
		if (npoints.length > 0) {
			this.points_[idxs[i]] = npoints;
		} else {
			delete this.points_[idxs[i]];
		}
	}
	if (changed) {
		this.notify_();
	}
	return changed;
};

// Get a list of spawn points with the given index.
Spawn.prototype.get = function(index) {
	return this.points_[index] || null;
};

////////////////////////////////////////////////////////////////////////
// Saving and loading
////////////////////////////////////////////////////////////////////////

// Load spawn point data from its JSON representation.
Spawn.prototype.load = function(data) {
	var sx = this.sx_, sy = this.sy_;
	var spawn = data.spawn;
	var idx, points, npoints, i, x, y;
	if (!spawn) {
		return;
	}
	for (idx in spawn) {
		points = spawn[idx].split(' ');
		npoints = [];
		for (i = 0; i < points.length - 1; i += 2) {
			x = parseInt(points[i+0]);
			y = parseInt(points[i+1]);
			if (x >= 0 && x < sx && y >= 0 && y < sy) {
				points.push(x, y);
			}
		}
		if (npoints.length > 0) {
			this.points_[idx] = npoints;
		}
	}
};

// Dump spawn poitn data to its JSON representation.
Spawn.prototype.dump = function(data, rect) {
	var sx = this.sx_, sy = this.sy_;
	var x0 = 0, y0 = 0, x1 = sx, y1;
	if (rect) {
		x0 = rect.x0; y0 = rect.y0; x1 = rect.x1; y1 = rect.y1;
	}
	var idx, points, npoints, spawn, i, x, y;
	var nonempty = false;
	spawn = {};
	for (idx in this.points_) {
		points = this.points_[idx];
		npoints = [];
		for (i = 0; i < points.length - 1; i += 2) {
			x = points[i+0];
			y = points[i+1];
			if (x >= x0 && x < x1 && y >= y0 && y < y1) {
				npoints.push(
					(x - x0).toString(16),
					(y - y0).toString(16));
			}
		}
		if (npoints.length > 0) {
			spawn[idx] = npoints.join(' ');
			nonempty = true;
		}
	}
	if (nonempty) {
		data.spawn = spawn;
	}
};

module.exports = Spawn;
