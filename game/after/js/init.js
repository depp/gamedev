'use strict';

var view = require('./view');

var canvas = null;
var gl = null;
var handle = null;
var lastResize = -1, lastWidth = 0, lastHeight = 0;
var RESIZE_DELAY = 500;
var canvasHeight = 0;

/*
var router = hashRouter();
router.addRoute('#/', function(hash, evt) {
	sys.screen.setScreen(game, {});
});
router.addRoute('#/play/:level', function(hash, evt) {
	sys.screen.setScreen(game, evt.params);
});
router.addRoute('#/edit/:level', function(hash, evt) {
	sys.screen.setScreen(editor, evt.params);
});
window.addEventListener('hashchange', router);
router();
*/
var MOUSE_EVENTS = [
	'mousedown',
	'mouseup',
	'mousemove',
	'mouseenter',
	'mouseleave'
];

// Initialize the game state.
function init(c, g) {
	if (canvas && gl) {
		return;
	}
	if (!c || !g) {
		console.error('Invalid init() call.');
		return;
	}
	canvas = c;
	gl = g;
	window.addEventListener('resize', resize, false);
	resize();
	start();
}

// Start the game loop.
function start() {
	var i;
	if (handle !== null) {
		return;
	}
	addEventListener('keydown', keyDown);
	addEventListener('keyup', keyUp);
	for (i = 0; i < MOUSE_EVENTS.length; i++) {
		canvas['on' + MOUSE_EVENTS[i]] = mouseEvent;
	}
	handle = window.requestAnimationFrame(render);
}

// Stop the game loop.
function stop() {
	var i;
	if (handle === null) {
		return;
	}
	window.cancelAnimationFrame(handle);
	handle = null;
	removeEventListener('keydown', keyDown);
	removeEventListener('keyup', keyUp);
	for (i = 0; i < MOUSE_EVENTS.length; i++) {
		canvas['on' + MOUSE_EVENTS[i]] = null;
	}
}

// Handle the canvas being resized.
function resize() {
	var w = canvas.clientWidth;
	var h = Math.max(1, Math.round(w * 9 / 16));
	if (h != canvasHeight) {
		canvas.style.height = h + 'px';
		canvasHeight = h;
	}
}

// Main rendering loop.
function render(time) {
	var w = canvas.clientWidth, h = canvas.clientHeight;
	var needsResize = lastResize < 0 ||
		(time > lastResize + RESIZE_DELAY &&
		 (w != lastWidth || h != lastHeight));
	if (needsResize) {
		canvas.width = lastWidth = w;
		canvas.height = lastHeight = h;
		lastResize = time;
	}
	view.render(
		time, gl, gl.drawingBufferWidth, gl.drawingBufferHeight, w / h);
	handle = window.requestAnimationFrame(render);
}

// Main rendering loop.
function render(time) {
	var w = canvas.clientWidth, h = canvas.clientHeight;
	var needsResize = lastResize < 0 ||
		(time > lastResize + RESIZE_DELAY &&
		 (w != lastWidth || h != lastHeight));
	if (needsResize) {
		canvas.width = lastWidth = w;
		canvas.height = lastHeight = h;
		lastResize = time;
	}
	handle = window.requestAnimationFrame(render);
	view.render(time, gl, w / h);
}

// Mouse events have a (px, py) for pixel coordinates, and a (cx, cy)
// for clip space coordinates.  Pixel coordinates are from the bottom
// left.
function mouseEvent(e) {
	e.preventDefault();
	var offx = 0, offy = 0, elt = canvas;
	do {
		offx += elt.offsetLeft - elt.scrollLeft;
		offy += elt.offsetTop - elt.scrollTop;
	} while ((elt = elt.offsetParent));
	var sx = canvas.offsetWidth;
	var sy = canvas.offsetHeight;
	var px = e.pageX - offx;
	var py = sy - (e.pageY - offy) - 1;
	view.event({
		type: e.type,
		shift: e.shiftKey,
		px: px,
		py: py,
		cx: Math.min(sx, Math.max(0, px + 0.5))*2/sx-1,
		cy: Math.min(sy, Math.max(0, py + 0.5))*2/sy-1
	});
	return false;
}

var KEYS = {
	8: 'delete',
	9: 'tab',
	13: 'enter',
	27: 'escape',
	32: 'space',
	33: 'pageup',
	34: 'pagedown',
	35: 'end',
	36: 'home',
	37: 'left',
	38: 'up',
	39: 'right',
	40: 'down',
	45: 'insert',
	46: 'forwarddelete',
	186: ';',
	187: '=',
	188: ',',
	189: '-',
	190: '.',
	191: '/',
	192: '`',
	219: '[',
	220: '\\',
	221: ']',
	222: '\''
};

// Map a key code to a key name.
function mapKey(code) {
	if (code >= 48 && code <= 57 || code >= 65 && code <= 90) {
		return String.fromCharCode(code);
	}
	return KEYS[code] || null;
}

var keys_down = {};

function keyDown(e) {
	var key = mapKey(e.keyCode);
	if (!key) {
		return;
	}
	e.preventDefault();
	var type = keys_down[key] ? 'keyrepeat' : 'keydown';
	keys_down[key] = true;
	view.event({
		type: type,
		shift: e.shiftKey,
		key: key
	});
	return false;
}

function keyUp(e) {
	var key = mapKey(e.keyCode);
	if (!key) {
		return;
	}
	e.preventDefault();
	if (!keys_down[key]) {
		return;
	}
	delete keys_down[key];
	view.event({
		type: 'keyup',
		shift: e.shiftKey,
		key: key
	});
	return false;
}

module.exports = {
	init: init,
	start: start,
	stop: stop
};
