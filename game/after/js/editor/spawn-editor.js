'use strict';

var sys = require('../sys.js');

var select = require('./select.js');

// Editing mode for the level spawn points.
function SpawnEditor(editor) {
	this.editor = editor;
	this.help = 'Spawn points';
}

SpawnEditor.prototype.update = function(time, aspect) {
};

SpawnEditor.prototype.handleEvent = function(evt) {
	var line, trace, n, success;
	switch (evt.type) {
	case 'keydown':
		n = parseInt(evt.key);
		if (!isNaN(n)) {
			success = sys.level.addSpawn(n);
			break;
		}
		if (evt.key == 'delete') {
			success = sys.level.deleteSpawn();
			break;
		}
		return;

	case 'mousedown':
		select.handleEvent(this, evt);
		return true;

	default:
		return;
	}

	if (success) {
		this.editor.markDirty('spawn');
	} else {
		console.log('editor operation failed');
	}
};

module.exports = SpawnEditor;
