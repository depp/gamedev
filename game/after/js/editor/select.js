'use strict';

var sys = require('../sys.js');

// Handle an event that will begin a selectin.
function selectionEvent(parent, evt) {
	var trace = sys.trace.traceScreen(evt.cx, evt.cy);
	if (trace) {
		sys.screen.add(new Selection(trace.x, trace.y, evt.shift), parent);
	} else {
		if (!evt.shift) {
			sys.level.hiClear();
		}
	}
}

function Selection(x, y, modify) {
	this.original = sys.level.hiSave();
	this.x0 = x;
	this.y0 = y;
	this.x1 = x;
	this.y1 = y;
	this.modify = modify;
	this.value = (modify && sys.level.hiGet(x, y)) ?
		null : {color: 0, full: true};

	sys.level.hiSet(x, y, this.value);
	this.update();
}

Selection.prototype.update = function() {
	if (this.modify) {
		sys.level.hiRestore(this.original);
	} else {
		sys.level.hiClear();
	}
	sys.level.hiSetRect(
		Math.min(this.x0, this.x1),
		Math.min(this.y0, this.y1),
		Math.max(this.x0, this.x1) + 1,
		Math.max(this.y0, this.y1) + 1,
		this.value);
};

Selection.prototype.exit = function(keep) {
	if (!keep) {
		sys.level.hiRestore(this.original);
	}
	sys.screen.remove(this);
};

Selection.prototype.handleEvent = function(evt) {
	var trace;
	switch (evt.type) {
	case 'mouseleave':
		this.exit(false);
		break;

	case 'mousedown':
	case 'mouseup':
	case 'mouseenter':
	case 'mousemove':
		trace = sys.trace.traceScreen(evt.cx, evt.cy);
		if (trace) {
			if (trace.x != this.x1 || trace.y != this.y1) {
				this.x1 = trace.x;
				this.y1 = trace.y;
				this.update();
			}
		}

		if (evt.type == 'mouseup') {
			this.exit(true);
		}
		break;

	case 'keydown':
		if (evt.key == 'escape') {
			this.exit(false);
		}
		break;
	}
	return true;
};

module.exports = {
	handleEvent: selectionEvent
};
