'use strict';

var bezierEasing = require('bezier-easing');

var sys = require('../sys.js');

var swiftOut = bezierEasing(0.55, 0, 0.1, 1);

var DELTA_TIME = 250;
var ZOOM_FACTOR = 0.2;
var MOVE_SPEED = 1024;

function CameraEditor() {
	this.state = 0;
	this.zoom = 0;
	this.zoom0 = 0;
	this.zoom1 = 0;
	this.time0 = 0;
	this.time1 = 0;

	this.buttons = {};
	this.lastTime = null;
}

CameraEditor.prototype.update = function(time, aspect) {
	var fraction, value, delta, factor;

	switch (this.state) {
	case 0:
		break;

	case 1:
		this.time0 = time;
		this.time1 = time + DELTA_TIME;
		this.zoom0 = this.zoom;
		this.state = 2;
		break;

	case 2:
		if (time >= this.time1) {
			this.zoom = this.zoom0 = this.zoom1;
			this.state = 0;
		} else {
			fraction = (time - this.time0) / (this.time1 - this.time0);
			fraction = swiftOut(fraction);
			this.zoom = this.zoom0 + fraction * (this.zoom1 - this.zoom0);
		}
		break;
	}

	factor = Math.exp(-this.zoom * ZOOM_FACTOR);
	sys.camera.subjectSize = (64*20) * factor;

	if (this.lastTime) {
		delta = time - this.lastTime;
		var sx = 0, sy = 0;
		sx = (this.buttons.left ? -1 : 0) + (this.buttons.right ? +1 : 0);
		sy = (this.buttons.down ? -1 : 0) + (this.buttons.up ? +1 : 0);
		if (sx || sy) {
			var amt = MOVE_SPEED * delta * factor * 0.001;
			sx *= amt;
			sy *= amt;
			var angle = (Math.PI / 180) * sys.camera.azimuth;
			var cos = Math.cos(angle), sin = Math.sin(angle);
			sys.camera.subjectPos[0] += -cos * sx - sin * sy;
			sys.camera.subjectPos[1] +=  sin * sx - cos * sy;
		}
	}

	this.lastTime = time;
};

CameraEditor.prototype.handleEvent = function(evt) {
	if (evt.type != 'keydown' && evt.type != 'keyup') {
		return;
	}
	switch (evt.key) {
	case '=':
		if (evt.type == 'keydown') {
			this.zoom1++;
			this.state = 1;
		}
		return true;
	case '-':
		if (evt.type == 'keydown') {
			this.zoom1--;
			this.state = 1;
		}
		return true;
	case 'left':
	case 'right':
	case 'up':
	case 'down':
		if (evt.type == 'keydown') {
			this.buttons[evt.key] = true;
		} else {
			delete this.buttons[evt.key];
		}
		return true;
	}
};

module.exports = CameraEditor;
