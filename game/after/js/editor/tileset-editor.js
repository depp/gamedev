'use strict';

var sys = require('../sys.js');

// Editing mode for changing the tileset
function TileSetEditor(editor) {
	this.editor = editor;
	this.help = 'Tile set';
}

TileSetEditor.prototype.update = function(time, aspect) {
};

TileSetEditor.prototype.handleEvent = function(evt) {
	var n;
	switch (evt.type) {
	case 'keydown':
		n = parseInt(evt.key);
		if (!isNaN(n)) {
			if (n !== sys.level.tileSet) {
				sys.level.tileSet = n;
				sys.level.invalidate();
				this.editor.markDirty();
			}
			return true;
		}
		break;
	}
};

module.exports = TileSetEditor;
