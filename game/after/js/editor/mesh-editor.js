'use strict';

var sys = require('../sys.js');

var select = require('./select.js');

// Editing mode for the level mesh.
function MeshEditor(editor) {
	this.editor = editor;
	this.help = 'Mesh';
}

MeshEditor.prototype.update = function(time, aspect) {
};

MeshEditor.prototype.handleEvent = function(evt) {
	var trace, success, obj;
	switch (evt.type) {
	case 'keydown':
		switch (evt.key) {
		case 'pageup':
			success = sys.level.addHeight(1);
			break;
		case 'pagedown':
			success = sys.level.addHeight(-1);
			break;
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
			obj = {};
			obj[evt.shift ? 'side' : 'top'] = parseInt(evt.key) - 1;
			success = sys.level.setTexture(obj);
			break;
		case 'R':
			success = sys.level.setRough(!evt.shift);
			break;
		case 'delete':
			success = sys.level.setActive(false);
			break;
		case 'space':
			success = sys.level.setActive(true);
			break;
		default:
			return;
		}
		break;

	case 'mousedown':
		select.handleEvent(this, evt);
		return true;

	default:
		return;
	}

	if (success) {
		this.editor.markDirty('mesh');
	} else {
		console.log('editor operation failed');
	}
	return true;
};

module.exports = MeshEditor;
