'use strict';

var glm = require('gl-matrix');
var vec3 = glm.vec3;

var sys = require('../sys.js');

var CameraEditor = require('./camera-editor.js');
var Select = require('./select.js');
var MeshEditor = require('./mesh-editor.js');
var SpawnEditor = require('./spawn-editor.js');
var TileSetEditor = require('./tileset-editor.js');

var SAVE_INTERVAL = 1000;

function Editor(levelName) {
	this.levelName = levelName;
	this.levelData = null;
	this.editorUri = null;
	this.saveTimout = null;
	this.saveState = null;

	this.modes = [
		new MeshEditor(this),
		new SpawnEditor(this),
		new TileSetEditor(this)
	];
	this.mode = null;
	this.setMode(0);
	this.spawnPoints = [];
	this.dirty = {};

	this.load();
}

// Load the level data from the level database.
Editor.prototype.load = function() {
	sys.services.loadLevel(this.levelName, function(data) {
		this.levelData = data;
		this.loadData();
	}.bind(this), function() {
		this.levelData = {sx:20, sy:20};
		this.loadData();
		return true;
	}.bind(this));
};

// Load the level data from the cached copy.
Editor.prototype.loadData = function() {
	sys.level.load(this.levelData);
	sys.level.showFloor = true;
	this.dirty = {spawn: true, mesh: true};
};

// Save the level data.
Editor.prototype.save = function() {
	if (this.saveTimout !== null) {
		window.clearTimeout(this.saveTimeout);
		this.saveTimeout = null;
	}
	this.saveState = 'saving';
	var data = {};
	sys.level.dump(data);
	sys.services.saveLevel(this.levelName, data, function() {
		console.log('saved');
		if (this.saveState == 'requested') {
			this.saveState = null;
			this.markDirty();
		} else {
			this.saveState = null;
		}
	}.bind(this), function(msg) {
		console.error(msg);
		alert(msg.msg);
		this.saveState = null;
	}.bind(this));
};

// Mark the level as dirty so it will be auto-saved.
Editor.prototype.markDirty = function() {
	var i;
	for (i = 0; i < arguments.length; i++) {
		this.dirty[arguments[i]] = true;
	}
	if (this.saveState) {
		this.saveState = 'requested';
		return;
	}
	this.saveTimeout = window.setTimeout((function() {
		this.saveTimeout = null;
		this.save();
	}).bind(this), SAVE_INTERVAL);
	this.saveState = 'requested';
};

// Set the current mode by number.
Editor.prototype.setMode = function(mode) {
	if (this.modeText) {
		this.modeText.remove();
		this.modeText = null;
	}
	if (this.mode !== null) {
		sys.screen.remove(this.modes[this.mode]);
	}
	var obj = this.modes[mode];
	this.mode = mode;
	this.modeText = sys.text.layout({
		text: 'Mode: ' + obj.help,
		x: 4,
		y: sys.camera.uiHeight - 4,
		valign: 'top',
		halign: 'left',
		visible: true
	});
	sys.screen.add(obj, this);
};

Editor.prototype.update = function(time, aspect) {
	var i, spawnIdx, spawnList, x, y, sp;
	var updateSpawnLoc = false;
	if (this.dirty.spawn) {
		updateSpawnLoc = true;
		this.spawnPoints = [];
		for (spawnIdx in sys.level.spawnPoints) {
			spawnList = sys.level.spawnPoints[spawnIdx];
			spawnIdx = parseInt(spawnIdx) - 1;
			for (i = 0; i < spawnList.length - 1; i += 2) {
				x = spawnList[i+0];
				y = spawnList[i+1];
				this.spawnPoints.push({
					x:x, y:y, loc:vec3.create(),
					n:spawnIdx
				});
			}
		}
		delete this.dirty.spawn;
	}
	if (this.dirty.mesh) {
		updateSpawnLoc = true;
		delete this.dirty.mesh;
	}

	sys.sprites.clear();
	for (i = 0; i < this.spawnPoints.length; i++) {
		sp = this.spawnPoints[i];
		sys.level.tilePos(sp.loc, sp.x, sp.y);
		sp.loc[2] += 16;
		sys.sprites.frames.push({
			loc: sp.loc,
			face: null,
			name: 'number',
			frame: sp.n
		});
	}
};

Editor.prototype.handleEvent = function(evt) {
	if (evt.type == 'keydown' && evt.key == 'tab') {
		var newMode = this.mode;
		if (evt.shift) {
			newMode--;
			if (newMode < 0) {
				newMode = this.modes.length - 1;
			}
		} else {
			newMode++;
			if (newMode >= this.modes.length) {
				newMode = 0;
			}
		}
		this.setMode(newMode);
		return true;
	}
};

function editor(params) {
	var levelName = params.level || 'default';
	return [
		new Editor(levelName),
		new sys.CameraControl(),
		new CameraEditor(),
	];
}

module.exports = editor;
