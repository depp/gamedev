'use strict';

// Round up to the next power of two.
function nextPow2(x) {
	x -= 1;
	x |= x >> 1;
	x |= x >> 2;
	x |= x >> 4;
	x |= x >> 8;
	x |= x >> 16;
	return x + 1;
}

module.exports = {
	nextPow2: nextPow2
};
