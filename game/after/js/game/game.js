'use strict';

var glm = require('gl-matrix');
var vec3 = glm.vec3;

var sys = require('../sys.js');
var level = require('../level/level.js');

var Character = require('./character.js');
var PlaceAction = require('./place-action.js');

function Game(level, characters) {
	var i;
	this.level = level;
	sys.screen.setLevel(level);
	this.characters = [];
	for (i = 0; i < characters.length; i++) {
		this.characters.push(new Character(characters[i]));
	}
	this.helpLayout = null;
}

// Start the action.
Game.prototype.start = function() {
	sys.camera.subjectPos.set(sys.level.center);
	sys.camera.subjectSize = sys.level.diameter;

	if (this.characters.length > 0) {
		this.runAction(new PlaceAction(this));
	}
};

// Drop all characters to the ground.
Game.prototype.dropAllToGround = function() {
	var i;
	for (i = 0; i < this.characters.length; i++) {
		this.characters[i].dropToGround();
	}
};

// Update the state of the game.
Game.prototype.update = function(time, aspect) {
	var i, c;
	sys.sprites.clear();
	for (i = 0; i < this.characters.length; i++) {
		c = this.characters[i];
		if (c.state) {
			this.characters[i].emit();
		}
	}
};

// Run an action.
Game.prototype.runAction = function(action) {
	if (this.helpLayout) {
		this.helpLayout.remove();
		this.helpLayout = null;
	}
	if (action.help) {
		this.helpLayout = sys.text.layout({
			text: action.help,
			x: 10,
			y: sys.camera.uiHeight - 10,
			valign: 'top',
			halign: 'left',
			visible: true,
			scale: 0.8
		});
	}
	sys.screen.add(action, this);
};

// Handle an event.
Game.prototype.handleEvent = function(evt) {
};

// Handle a key event.
Game.prototype.handleKey = function(evt) {
	if (evt.type != 'down') {
		return;
	}
};

function game(params) {
	var levelName = params.level || 'z1_bridge';
	level.load(levelName, function(ldata) {
		var chars = ['p1', 'p2', 'p3', 'p4'];
		screen.add(new Game(level, chars));
		screen.add(new sys.CameraControl());
	});

	var obj = new Game();
	sys.services.loadLevel(levelName, function(data) {
		sys.level.load(data);
		obj.start();
	});
	return [obj, new sys.CameraControl()];
}

module.exports = game;
