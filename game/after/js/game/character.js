'use strict';

var glm = require('gl-matrix');
var vec3 = glm.vec3;

var sys = require('../sys.js');

// Character height off the ground.
var HEIGHT = 32;

var characters = {
	'p1': {
		name: 'Alice'
	},
	'p2': {
		name: 'Bjorn'
	},
	'p3': {
		name: 'Placidus'
	},
	'p4': {
		name: 'Tauthe'
	}
};

function Character(ctype) {
	this.ctype = ctype;
	this.state = null;
	this.x = 0;
	this.y = 0;
	this.face = 0;
	this.loc = vec3.create();
}

// Emit the character's sprites.
Character.prototype.emit = function() {
	sys.sprites.frames.push({
		loc: this.loc,
		name: this.ctype + '.stand',
		face: this.face,
		frame: 0
	});
};

// Drop the character to the ground.
Character.prototype.dropToGround = function() {
	sys.level.tilePos(this.loc, this.x, this.y);
	this.loc[2] += HEIGHT;
};

// Place the character on the map.
Character.prototype.place = function(x, y) {
	this.state = 'active';
	this.x = x;
	this.y = y;
	this.dropToGround();
};

module.exports = Character;
