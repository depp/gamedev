'use strict';

var sys = require('../sys.js');

// Highlight colors
var SPAWN = 5;
var CURSOR_GOOD = 2;
var CURSOR_BAD = 7;
var CURSOR_PLACED = 1;

// Action to place characters on the level.
function PlaceAction(game) {
	this.game = game;
	this.help = 'Place characters';
	this.cx = -1;
	this.cy = -1;
	this.cval = null;
	this.index = 0;

	sys.level.hiClear();
	sys.level.hiBlink(SPAWN, 1);
	var count = sys.level.hiSpawn(1, {color: SPAWN});
	if (!count) {
		console.error('No spawn points');
	}
}

PlaceAction.prototype.update = function(time, aspect) {
};

PlaceAction.prototype.handleEvent = function(evt) {
	var trace, action;
	switch (evt.type) {
	case 'mousedown':
	case 'mousemove':
		trace = sys.trace.traceScreen(evt.cx, evt.cy);
		if (trace) {
			this.setCursor(trace.x, trace.y);
		} else {
			this.setCursor(-1, -1);
		}
		if (evt.type == 'mousedown') {
			switch (this.cval ? this.cval.color : 0) {
			case SPAWN:
				console.log('PLACE', this.x, this.y);
				this.game.characters[this.index++].place(this.x, this.y);
				this.cval = {color: CURSOR_PLACED};
				if (this.index == this.game.characters.length) {
					this.exit();
				}
				break;
			}
		}
		return true;
	}
};

PlaceAction.prototype.setCursor = function(x, y, action) {
	if (this.x === x && this.y === y) {
		return;
	}
	sys.level.hiSet(this.x, this.y, this.cval);
	this.cval = sys.level.hiGet(x, y);
	this.x = x;
	this.y = y;
	sys.level.hiSet(
		this.x, this.y,
		{color: this.cval ? CURSOR_GOOD : CURSOR_BAD});
};

PlaceAction.prototype.exit = function() {
	sys.level.hiClear();
	sys.screen.remove(this);
};

module.exports = PlaceAction;
