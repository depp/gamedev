'use strict';

function RGBColor(r, g, b, a) {
	this.r = r;
	this.g = g;
	this.b = b;
	this.a = a;
}

RGBColor.prototype.toRGB = function() {
	return this;
};

RGBColor.prototype.toHMM = function() {
	var m1 = Math.min(this.r, this.g, this.b);
	var m2 = Math.max(this.r, this.g, this.b);
	var h, x;
	if (m2 > m1) {
		x = 1 / (m2 - m1);
		if (this.r == m2) {
			h = x * (this.g - this.b);
		} else if (this.g == m2) {
			h = x * (this.b - this.r) + 2;
		} else {
			h = x * (this.r - this.g) + 4;
		}
	} else {
		h = 0;
	}
	return new HMMColor(h, m1, m2);
};

// Color which stores the hue with the minimum and maximum RGB values.
function HMMColor(h, m1, m2, a) {
	this.h = h;
	this.m1 = m1;
	this.m2 = m2;
	this.a = a;
}

HMMColor.prototype.toRGB = function() {
	var h = (this.h / 60) % 6;
	if (h < 0) {
		h += 6;
	}
	var c1 = this.m1, c3 = this.m2;
	var c2 = c1 + (c2 - c1) * (1 - Math.abs((h % 2) - 1));
	switch (Math.floor(h)) {
	default:
	case 0: return new RGBColor(c3, c2, c1);
	case 1: return new RGBColor(c2, c3, c1);
	case 2: return new RGBColor(c1, c3, c2);
	case 3: return new RGBColor(c1, c2, c3);
	case 4: return new RGBColor(c2, c1, c3);
	case 5: return new RGBColor(c3, c1, c2);
	}
};

function rgb(r, g, b) {
	return new RGBColor(r, g, b, 1);
}

function rgba(r, g, b, a) {
	return new RGBColor(r, g, b, a);
}

function hsl(h, s, l) {
	return hsla(h, s, l, 1);
}

function hsla(h, s, l, a) {
	var c = (1 - Math.abs(2 * l - 1)) * s;
	return new HMMColor(h, l - c * 0.5, l + c * 0.5, a);
}

function hsv(h, s, v) {
	return hsva(h, s, v, 1);
}

function hsva(h, s, v, a) {
	var c = v * s;
	var x = (1 - Math.abs((h * (1/60) % 2) - 1)) * c;
	return new HMMColor(h, v - c, v - c + x);
}

function hwb(h, w, b) {
	return hwba(h, w, b, 1);
}

function hwba(h, w, b, a) {
	var v = 1 - b;
	var c = v - w;
	var x = (1 - Math.abs((h * (1/60) % 2) - 1)) * c;
	return new HMMColor(h, v - c, v - c + x);
}

function gray(g) {
	return rgba(g, g, g, 1.0);
}

module.exports = {
	rgb: rgb,
	rgba: rgba,
	hsl: hsl,
	hsla: hsla,
	hsv: hsv,
	hsva: hsva,
	hwb: hwb,
	hwba: hwba,
	gray: gray
};
