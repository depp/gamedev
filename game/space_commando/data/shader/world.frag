#version 120

varying vec2 v_texcoord;

void main() {
    gl_FragColor = vec4(fract(v_texcoord), 1.0, 1.0);
}
