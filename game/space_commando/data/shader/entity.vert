#version 120

attribute vec3 vertex;
uniform mat4 modelview;
uniform mat4 projection;

void main() {
    gl_Position = projection * (modelview * vec4(vertex, 1.0));
}
