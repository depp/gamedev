#version 120

attribute vec4 a_vert;
uniform mat4 u_modelview;
uniform mat4 u_projection;
varying vec2 v_texcoord;

void main() {
    vec3 loc = a_vert.xyz * 0.0625;
    float face = a_vert.w;
    if (face <= 1.0) {
        v_texcoord = vec2(loc.y * (face * +2.0 - 1.0), loc.z);
    } else if (face <= 3.0) {
        v_texcoord = vec2(loc.x * (face * -2.0 + 5.0), loc.z);
    } else {
        v_texcoord = vec2(loc.x * (face * +2.0 - 9.0), loc.y);
    }
    gl_Position = u_projection * (u_modelview * vec4(loc, 1.0));
}
