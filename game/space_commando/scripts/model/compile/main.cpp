/* Copyright 2014 Dietrich Epp. */
#include <endian.h>

#include <algorithm>
#include <cstdarg>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <vector>

const int MAGIC_SIZE = 16;
const char MAGIC_RAW[MAGIC_SIZE] = "SGLib Model Raw";
const char MAGIC_COMPILED[MAGIC_SIZE] = "SGLib Model";
const char ZERO[4] = { 0, 0, 0, 0 };

void die(const char *msg, ...)
    __attribute__((format(printf, 1, 2), noreturn));

void die(const char *msg, ...) {
    va_list ap;
    va_start(ap, msg);
    std::fputs("Error: ", stderr);
    std::vfprintf(stderr, msg, ap);
    std::fputc('\n', stderr);
    va_end(ap);
    std::exit(1);
}

struct Vec3 {
    float v[3];
};

enum class Type {
    FLOAT = 0,
    DOUBLE = 1,
    INT = 2,
    UNSIGNED_INT = 3
};

/* ======================================================================
   Byte swapping
   ====================================================================== */

unsigned swap32(unsigned x) {
    return (((x & 0x000000ff) << 24) |
            ((x & 0x0000ff00) << 8) |
            ((x & 0x00ff0000) >> 8) |
            ((x & 0xff000000) >> 24));
}

template<typename T>
void byteswap(T &x);

template<>
void byteswap<unsigned>(unsigned &x) {
    x = swap32(x);
}

template<>
void byteswap<int>(int &x) {
    byteswap(reinterpret_cast<unsigned &>(x));
}

template<>
void byteswap<float>(float &x) {
    byteswap(reinterpret_cast<unsigned &>(x));
}

template<>
void byteswap<Vec3>(Vec3 &x) {
    for (int i = 0; i < 3; i++)
        byteswap(x.v[i]);
}

/* ======================================================================
   OpenGL constants
   ====================================================================== */

namespace GL {

const int BYTE                            = 0x1400;
const int UNSIGNED_BYTE                   = 0x1401;
const int SHORT                           = 0x1402;
const int UNSIGNED_SHORT                  = 0x1403;
const int INT                             = 0x1404;
const int UNSIGNED_INT                    = 0x1405;
const int HALF_FLOAT                      = 0x140b;
const int FLOAT                           = 0x1406;
const int DOUBLE                          = 0x140a;
const int FIXED                           = 0x140c;
const int INT_2_10_10_10_REV              = 0x8d9f;
const int UNSIGNED_INT_2_10_10_10_REV     = 0x8368;
const int UNSIGNED_INT_10F_11F_11F_REV    = 0x8c3b;

const int POINTS                          = 0x0000;
const int LINE_STRIP                      = 0x0003;
const int LINE_LOOP                       = 0x0002;
const int LINES                           = 0x0001;
const int LINE_STRIP_ADJACENCY            = 0x000b;
const int LINES_ADJACENCY                 = 0x000a;
const int TRIANGLE_STRIP                  = 0x0005;
const int TRIANGLE_FAN                    = 0x0006;
const int TRIANGLE_STRIP_ADJACENCY        = 0x000d;
const int TRIANGLES_ADJACENCY             = 0x000c;

}

/* ======================================================================
   Chunk reader
   ====================================================================== */

class ChunkReader {
private:
    struct Chunk {
        char name[4];
        unsigned offset;
        unsigned length;
    };

    char m_magic[MAGIC_SIZE];
    std::vector<Chunk> m_chunks;
    std::ifstream m_file;
    bool m_swapped;
    int m_version[2];

public:
    explicit ChunkReader(const std::string &path);

    template<typename T>
    std::vector<T> read_chunk(const char *name);

private:
    Chunk find_chunk(const char *name) const;
};

ChunkReader::ChunkReader(const std::string &path)
    : m_file(path, std::ios::in | std::ios::binary) {
    m_file.read(m_magic, MAGIC_SIZE);
    unsigned count;

    {
        char info[8];
        m_file.read(info, 8);
        bool big;
        if (info[2] == 'B' && info[3] == 'E') {
            big = true;
        } else if (info[2] == 'L' && info[3] == 'E') {
            big = false;
        } else {
            die("Invalid byte order.");
        }
        m_swapped = big == (BYTE_ORDER == LITTLE_ENDIAN);
        m_version[0] = info[0];
        m_version[1] = info[1];
        std::memcpy(&count, info + 4, 4);
        if (m_swapped)
            byteswap(count);
    }

    for (unsigned i = 0; i < count; i++) {
        char name[4];
        unsigned info[2];
        m_file.read(name, 4);
        m_file.read(reinterpret_cast<char *>(info), 8);
        if (m_swapped) {
            for (int i = 0; i < 2; i++)
                info[i] = swap32(info[i]);
        }
        Chunk chunk;
        std::memcpy(chunk.name, name, 4);
        chunk.offset = info[0];
        chunk.length = info[1];
        m_chunks.push_back(chunk);
    }
}

template<typename T>
std::vector<T> ChunkReader::read_chunk(const char *name) {
    auto chunk = find_chunk(name);
    std::vector<T> data;
    if (!chunk.length)
        return data;
    std::size_t count = chunk.length / sizeof(T);
    data.resize(count);
    m_file.seekg(chunk.offset);
    m_file.read(reinterpret_cast<char *>(data.data()), count * sizeof(T));
    if (m_swapped) {
        for (auto &x : data)
            byteswap(x);
    }
    return data;
}

ChunkReader::Chunk ChunkReader::find_chunk(const char *name) const {
    for (const auto &chunk : m_chunks) {
        if (!std::memcmp(name, chunk.name, 4))
            return chunk;
    }
    Chunk c;
    memcpy(c.name, name, 4);
    c.offset = 0;
    c.length = 0;
    return c;
}

/* ======================================================================
   Chunk writer
   ====================================================================== */

unsigned align(unsigned x) {
    return (x + 3) & ~3u;
}

class ChunkWriter {
private:
    struct ChunkData {
        char name[4];
        int order;
        std::vector<char> data;
    };
    char m_magic[MAGIC_SIZE];
    int m_version[2];
    std::vector<ChunkData> m_chunks;

public:
    ChunkWriter(const char *magic, int ver_major, int ver_minor);

    void add_data(const char *name, int order, std::vector<char> data);

    void add_data(const char *name, int order,
                  const void *data, std::size_t size);

    template<typename T>
    void add_array(const char *name, int order,
                   const std::vector<T> &data);

    void write(const std::string &path) const;
};

ChunkWriter::ChunkWriter(const char *magic,
                         int ver_major, int ver_minor) {
    std::memcpy(m_magic, magic, MAGIC_SIZE);
    m_version[0] = ver_major;
    m_version[1] = ver_minor;
}

void ChunkWriter::add_data(const char *name, int order,
                           std::vector<char> data) {
    ChunkData chunk;
    std::memcpy(chunk.name, name, 4);
    chunk.order = order;
    chunk.data = std::move(data);
    auto i = std::begin(m_chunks), e = std::end(m_chunks);
    for (; i != e; ++i) {
        if (i->order > order)
            break;
        if (i->order == order && std::memcmp(i->name, name, 4) > 0)
            break;
    }
    m_chunks.insert(i, std::move(chunk));
}

void ChunkWriter::add_data(const char *name, int order,
                           const void *data, std::size_t size) {
    std::vector<char> dvec;
    dvec.resize(size);
    std::memcpy(dvec.data(), data, size);
    add_data(name, order, std::move(dvec));
}

template<typename T>
void ChunkWriter::add_array(const char *name, int order,
                            const std::vector<T> &data) {
    add_data(name, order, data.data(), data.size() * sizeof(T));
}

void ChunkWriter::write(const std::string &path) const {
    std::ofstream file(path, std::ios::out | std::ios::binary);
    file.write(m_magic, 16);
    {
        char info[4];
        info[0] = m_version[0];
        info[1] = m_version[1];
        if (BYTE_ORDER == LITTLE_ENDIAN) {
            info[2] = 'L';
            info[3] = 'E';
        } else {
            info[2] = 'B';
            info[3] = 'E';
        }
        file.write(info, 4);
    }

    {
        unsigned count = m_chunks.size();
        file.write(reinterpret_cast<const char *>(&count), 4);
    }

    unsigned offset = 24 + 12 * m_chunks.size();
    for (const auto &chunk : m_chunks) {
        unsigned info[2];
        info[0] = offset;
        info[1] = chunk.data.size();
        offset = align(offset + chunk.data.size());
        file.write(chunk.name, 4);
        file.write(reinterpret_cast<const char *>(info), 8);
    }

    for (const auto &chunk : m_chunks) {
        file.write(chunk.data.data(), chunk.data.size());
        int rem = align(chunk.data.size()) - chunk.data.size();
        if (rem > 0)
            file.write(ZERO, rem);
    }
}

/* ======================================================================
   Attribute data
   ====================================================================== */

int attr_size(int type, int size) {
    if (size < 1 || size > 4)
        die("Invalid attribute size.");

    switch (type) {
    case GL::BYTE:
    case GL::UNSIGNED_BYTE:
        return size;

    case GL::SHORT:
    case GL::UNSIGNED_SHORT:
    case GL::HALF_FLOAT:
        return 2 * size;

    case GL::INT:
    case GL::UNSIGNED_INT:
    case GL::FLOAT:
    case GL::FIXED:
        return 4 * size;

    case GL::DOUBLE:
        return 8 * size;

    case GL::INT_2_10_10_10_REV:
    case GL::UNSIGNED_INT_2_10_10_10_REV:
        if (size != 4)
            die("Invalid size.");
        return 3;

    case GL::UNSIGNED_INT_10F_11F_11F_REV:
        if (size != 3)
            die("Invalid size.");
        return 4;

    default:
        die("Invalid attribute type.");
    }
}

class AttributeData {
private:
    struct Attribute {
        std::string name;
        std::vector<char> data;
        Type tclass;
        int type;
        int size;
        bool normalize;
    };

    int m_count;
    std::vector<Attribute> m_attribute;

public:
    explicit AttributeData(int vertex_count);

    void add(
        const std::string &name,
        std::vector<char> data,
        Type tclass,
        int type,
        int size,
        bool normalize);

    void write(ChunkWriter &writer) const;
};

AttributeData::AttributeData(int vertex_count)
    : m_count(vertex_count) { }

void AttributeData::add(
    const std::string &name,
    std::vector<char> data,
    Type tclass,
    int type,
    int size,
    bool normalize)
{
    int asize = attr_size(type, size);
    if (static_cast<std::size_t>(asize * m_count) != data.size()) {
        die("Incorrect attribute data size: %s", name.c_str());
    }

    m_attribute.emplace_back();
    auto &attr = m_attribute.back();
    attr.name = name;
    attr.data = std::move(data);
    attr.tclass = tclass;
    attr.type = type;
    attr.size = size;
    attr.normalize = normalize;
}

void AttributeData::write(ChunkWriter &writer) const {
    int count = m_count;
    int stride = 0;
    for (const auto &attr : m_attribute)
        stride += attr_size(attr.type, attr.size);
    int offset = 0;
    std::vector<char> anam, atyp, adat;
    adat.resize(stride * count);
    {
        unsigned short s = stride;
        atyp.insert(std::end(atyp),
                    reinterpret_cast<const char *>(&s),
                    reinterpret_cast<const char *>(&s + 1));
    }
    for (const auto &attr : m_attribute) {
        {
            anam.insert(std::end(anam),
                        std::begin(attr.name), std::end(attr.name));
            anam.push_back('\0');
        }
        {
            char typ[6];
            typ[0] = static_cast<unsigned char>(attr.tclass);
            typ[1] = attr.size;
            unsigned short x = attr.type;
            std::memcpy(&typ[2], &x, 2);
            typ[4] = attr.normalize ? 1 : 0;
            typ[5] = offset;
            atyp.insert(std::end(atyp),
                        std::begin(typ), std::end(typ));
        }
        {
            int size = attr_size(attr.type, attr.size);
            for (int i = 0 ; i < count; i++) {
                std::memcpy(&adat[i * stride + offset],
                            &attr.data[i * size],
                            size);
            }
            offset += size;
        }
    }
    writer.add_data("ANAM", 0x12, std::move(anam));
    writer.add_data("ATYP", 0x22, std::move(atyp));
    writer.add_data("ADAT", 0x32, std::move(adat));
}

/* ======================================================================
   Uniform data
   ====================================================================== */

class UniformData {
private:
    struct Uniform {
        std::string name;
        std::vector<char> data;
        Type tclass;
        int rows;
        int columns;
        int count;
    };

    std::vector<Uniform> m_uniform;

public:
    void add_scalar(
        const std::string &name,
        const float *data,
        int count);

    void add_vector(
        const std::string &name,
        const float *data,
        int rows,
        int count);

    void add_matrix(
        const std::string &name,
        const float *data,
        int rows,
        int columns,
        int count);

    void write(ChunkWriter &writer) const;

private:
    void add_data(
        const std::string &name,
        std::vector<char> data,
        Type tclass,
        int rows,
        int columns,
        int count);
};

void UniformData::add_scalar(
    const std::string &name,
    const float *data,
    int count)
{
    add_matrix(name, data, 1, 1, count);
}

void UniformData::add_vector(
    const std::string &name,
    const float *data,
    int rows,
    int count)
{
    add_matrix(name, data, rows, 1, count);
}

void UniformData::add_matrix(
    const std::string &name,
    const float *data,
    int rows,
    int columns,
    int count)
{
    std::vector<char> cdata;
    int size = 4 * rows * columns * count;
    cdata.resize(size);
    std::memcpy(cdata.data(), data, size);
    add_data(name, std::move(cdata), Type::FLOAT, rows, columns, count);
}

void UniformData::write(ChunkWriter &writer) const {
    std::vector<char> unam, utyp, udat;
    for (const auto &u : m_uniform) {
        {
            unam.insert(std::end(unam),
                        std::begin(u.name), std::end(u.name));
            unam.push_back('\0');
        }
        {
            unsigned short typ[2];
            typ[0] = ((static_cast<int>(u.tclass) << 8) |
                      (u.rows << 4) |
                      u.columns);
            typ[1] = u.count;
            utyp.insert(std::end(utyp),
                        reinterpret_cast<const char *>(&typ[0]),
                        reinterpret_cast<const char *>(&typ[2]));
        }
        {
            udat.insert(std::end(udat),
                        std::begin(u.data), std::end(u.data));
        }
    }
    writer.add_data("UNAM", 0x11, std::move(unam));
    writer.add_data("UTYP", 0x21, std::move(utyp));
    writer.add_data("UDAT", 0x31, std::move(udat));
}

void UniformData::add_data(
    const std::string &name,
    std::vector<char> data,
    Type tclass,
    int rows,
    int columns,
    int count)
{
    if (rows < 1 || rows > 4 || columns < 1 || columns > 4) {
        die("Invalid uniform dimensions.");
    }
    if (count < 1) {
        die("Invalid uniform size.");
    }
    m_uniform.emplace_back();
    auto &u = m_uniform.back();
    u.name = name;
    u.data = std::move(data);
    u.tclass = tclass;
    u.rows = rows;
    u.columns = columns;
    u.count = count;
}

/* ======================================================================
   Raw model
   ====================================================================== */

struct RawModel {
    std::vector<Vec3> vertex;
    std::vector<Vec3> normal;
    std::vector<int> index;

    static RawModel read(const std::string &path);
};

RawModel RawModel::read(const std::string &path) {
    ChunkReader infile(path);
    RawModel model;

    model.vertex = infile.read_chunk<Vec3>("VERT");
    model.normal = infile.read_chunk<Vec3>("NORM");
    model.index = infile.read_chunk<int>("POLY");

    auto size = model.vertex.size();

    if (!model.normal.empty() && model.normal.size() != size) {
        die("Array size mismatch: NORM");
    }

    for (auto idx : model.index) {
        if (idx != -1 && static_cast<std::size_t>(idx) > size) {
            die("Invalid index: %d", idx);
        }
    }

    return model;
}

/* ======================================================================
   Model compiler
   ====================================================================== */

struct BBox {
    Vec3 mins, maxs;
};

BBox get_bbox(const std::vector<Vec3> &vertex) {
    BBox bbox { vertex[0], vertex[0] };
    for (const auto &v : vertex) {
        for (int i = 0; i < 3; i++) {
            bbox.mins.v[i] = std::min(bbox.mins.v[i], v.v[i]);
            bbox.maxs.v[i] = std::max(bbox.maxs.v[i], v.v[i]);
        }
    }
    return bbox;
}

ChunkWriter compile(const RawModel &model) {
    ChunkWriter writer(MAGIC_COMPILED, 1, 0);

    {
        AttributeData attr(model.vertex.size());
        {
            const auto &vert = model.vertex;
            std::vector<char> vdata;
            vdata.insert(
                std::end(vdata),
                reinterpret_cast<const char *>(vert.data()),
                reinterpret_cast<const char *>(vert.data() + vert.size()));
            attr.add("vertex", std::move(vdata), Type::FLOAT,
                     GL::FLOAT, 3, false);
        }
        if (false && !model.normal.empty()) {
            std::vector<char> ndata;
            for (const auto &v : model.normal) {
                float mag = std::sqrt(
                    v.v[0] * v.v[0] + v.v[1] * v.v[1] + v.v[2] * v.v[2]);
                float data[3] = { v.v[0] / mag, v.v[1] / mag, v.v[2] / mag };
                ndata.insert(
                    std::end(ndata),
                    reinterpret_cast<const char *>(&data[0]),
                    reinterpret_cast<const char *>(&data[3]));
            }
            attr.add("normal", std::move(ndata), Type::FLOAT,
                     GL::FLOAT, 3, false);
        }
        attr.write(writer);
    }

    {
        unsigned short type = GL::UNSIGNED_SHORT;
        writer.add_data("ITYP", 0x23,
                        reinterpret_cast<const char *>(&type),
                        sizeof(type));
    }

    {
        std::vector<char> idata;
        int state = 0;
        unsigned short tri[3];
        for (auto idx : model.index) {
            if (idx == -1) {
                state = 0;
            } else if (state == 2) {
                tri[2] = idx;
                idata.insert(
                    std::end(idata),
                    reinterpret_cast<const char *>(&tri[0]),
                    reinterpret_cast<const char *>(&tri[3]));
                tri[1] = idx;
            } else if (state == 1) {
                tri[1] = idx;
                state = 2;
            } else {
                tri[0] = idx;
                state = 1;
            }
        }
        writer.add_data("IDAT", 0x33, std::move(idata));
    }

    return writer;
}

/* ======================================================================
   Main
   ====================================================================== */

int main(int argc, char **argv) {
    if (argc != 3) {
        die("Usage: compile INPUT OUTPUT");
    }

    std::string inpath(argv[1]), outpath(argv[2]);
    auto model = RawModel::read(inpath);
    if (model.vertex.empty()) {
        die("Model is empty");
    }
    auto writer = compile(model);
    writer.write(outpath);

    return 0;
}
