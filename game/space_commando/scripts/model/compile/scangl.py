#!/usr/bin/env python3
# Copyright 2014 Dietrich Epp.
import sys
import re
import os

def scan_header(path):
    symbols = {}
    regex = re.compile(r'^#\s*define\s+GL_(\w+)\s+(\S+)\s*$')
    with open(path, 'r') as fp:
        for line in fp:
            m = regex.match(line)
            if m is None:
                continue
            name = m.group(1)
            value = m.group(2)
            try:
                value = int(value, 0)
            except ValueError:
                continue
            symbols[name] = value
    return symbols

def replace_defs(path, symbols):
    tmppath = path + '.tmp'
    regex = re.compile(r'^const int (\S+)')
    with open(path, 'r') as fp:
        with open(tmppath, 'w') as ofp:
            for line in fp:
                ofp.write(line)
                if line.startswith('namespace GL'):
                    break
            for line in fp:
                m = regex.match(line)
                if m is not None:
                    name = m.group(1)
                    try:
                        value = symbols[name]
                    except KeyError:
                        raise Exception('unknown symbol: GL_{}'.format(name))
                    line = 'const int {:<31} = 0x{:04x};\n'.format(
                        name, value)
                ofp.write(line)
                if line.startswith('}'):
                    break
            for line in fp:
                ofp.write(line)
    os.replace(tmppath, path)

def main():
    import sys
    if len(sys.argv) == 1:
        header = '/usr/include/GL/gl.h'
    elif len(sys.argv) == 2:
        header = sys.argv[1]
    else:
        raise Exception('invalid usage')

    symbols = scan_header(header)
    replace_defs('main.cpp', symbols)

if __name__ == '__main__':
    main()
