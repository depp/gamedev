# Copyright 2014 Dietrich Epp.
import bpy
import array
import struct
import sys

def write(filepath, apply_modifiers=True):
    scene = bpy.context.scene

    arr_vert = array.array('f')
    arr_norm = array.array('f')
    arr_index = array.array('i')
    for obj in bpy.context.selected_objects:
        if apply_modifiers or obj.type != 'MESH':
            try:
                mesh = obj.to_mesh(scene, True, 'PREVIEW')
            except:
                continue
            is_tmp_mesh = True
        else:
            mesh = obj.data
            if not mesh.tessfaces and mesh.polygons:
                mesh.calc_tessfaces()
            is_tmp_mesh = False

        mat_vert = obj.matrix_world.copy()
        mat_norm = mat_vert.to_3x3()
        mat_norm.invert()
        mat_norm.transpose()
        base_index = len(arr_vert) // 3
        for face in mesh.tessfaces:
            for index in face.vertices:
                arr_index.append(index + base_index)
            arr_index.append(-1)
        for vertex in mesh.vertices:
            arr_vert.extend(mat_vert * vertex.co)
            arr_norm.extend(mat_norm * vertex.normal)
        if is_tmp_mesh:
            bpy.data.meshes.remove(mesh)

    chunks = [
        (b'VERT', arr_vert),
        (b'NORM', arr_norm),
        (b'POLY', arr_index),
    ]

    with open(filepath, 'wb') as fp:
        fp.write(b'SGLib Model Raw\0\x01\x02')
        fp.write(b'BE' if sys.byteorder == 'big' else b'LE')
        fp.write(struct.pack('I', len(chunks)))
        offset = 24 + 12 * len(chunks)
        for name, arr in chunks:
            arrlen = arr.itemsize * len(arr)
            fp.write(name)
            fp.write(struct.pack('II', offset, arrlen))
            offset += arrlen
        for name, arr in chunks:
            arr.tofile(fp)
