# Copyright 2014 Dietrich Epp.
"""
This script exports a mesh to an SGMod file.
"""
import bpy

bl_info = {
    'name': 'SGMod raw model format (.sgmodraw)',
    'author': 'Dietrich Epp',
    'version': (0, 1),
    'blender': (2, 72, 0),
    'location': 'File > Import-Export > SGMod (.sgmodraw)',
    'description': 'Import-Export SGMod',
    'warning': '',
    'category': 'Import-Export',
}

if 'bpy' in locals():
    import imp
    if 'export_sgmod' in locals():
        imp.reload(export_sgmod)
else:
    import bpy

from bpy.props import StringProperty, BoolProperty
from bpy_extras.io_utils import ExportHelper

class SGModExporter(bpy.types.Operator, ExportHelper):
    """Save SGMod file."""
    bl_idname = 'export_mesh.sgmod'
    bl_label = 'Export SGMod'

    filename_ext = '.sgmodraw'
    filter_glob = StringProperty(
        default='*.sgmodraw',
        options={'HIDDEN'})

    apply_modifiers = BoolProperty(
        name='Apply Modifiers',
        description='Used transformed mesh data from each object',
        default=True)

    def execute(self, context):
        from . import export_sgmod
        export_sgmod.write(
            self.filepath,
            self.apply_modifiers)
        return {'FINISHED'}

def menu_export(self, context):
    self.layout.operator(
        SGModExporter.bl_idname,
        text='SGMod Model (.sgmodraw)')

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_export)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_export)

if __name__ == '__main__':
    register()
