#!/usr/bin/env python3
# Copyright 2014 Dietrich Epp.
import struct
import collections
import os
import io

Lump = collections.namedtuple('Lump', 'name data')

class Wad(object):
    __slots__ = ['type', 'lumps']

    def __init__(self, type):
        self.type = type
        self.lumps = []

    @classmethod
    def load(class_, path):
        """Read a WAD file from disk."""
        with open(path, 'rb') as fp:
            data = fp.read()
        header = data[:12]
        type = header[:4]
        if header[:4] not in (b'IWAD', b'PWAD'):
            raise Exception('not a WAD file')
        wad = class_(type.decode('ASCII'))
        count, offset = struct.unpack('<II', header[4:])
        for n in range(count):
            entry = data[offset+n*16:offset+n*16+16]
            eoffset, esize = struct.unpack('<II', entry[:8])
            ename = entry[8:]
            i = ename.find(b'\0')
            if i >= 0:
                ename = ename[:i]
            ename = ename.decode('ASCII')
            wad.lumps.append(Lump(ename, data[eoffset:eoffset+esize]))
        return wad

    def save(self, path):
        """Save a WAD file to disk."""
        dlen = sum(len(lump.data) for lump in self.lumps)
        directory = io.BytesIO()
        with open(path, 'wb') as fp:
            fp.write(self.type.encode('ASCII'))
            fp.write(struct.pack('<II', len(self.lumps), dlen + 12))
            offset = 12
            for lump in self.lumps:
                name = lump.name.encode('ASCII')
                if len(name) > 8:
                    raise ValueError('invalid lump name')
                directory.write(struct.pack('<II', offset, len(lump.data)))
                directory.write(name.ljust(8, b'\0'))
                fp.write(lump.data)
                offset += len(lump.data)
            fp.write(directory.getvalue())

    def find(self, name):
        """Find a lump with the given name."""
        for lump in self.lumps:
            if name == lump.name:
                return lump
        return None

    def find_range(self, startname, endname):
        """Find a range of lumps in a range between markers."""
        result = []
        lumps = iter(self.lumps)
        for lump in lumps:
            if lump.name == startname:
                break
        else:
            raise Exception('could not find start: {}'.format(startname))
        for lump in lumps:
            if lump.name == endname:
                break
            result.append(lump)
        else:
            raise Exception('could not find end: {}'.format(endname))
        return result

def extract_sprites(wad):
    palette = wad.find('PLAYPAL')
    lumps = wad.find_range('S_START', 'S_END')
    sprites = {}
    for lump in lumps:
        sname = lump.name[:4]
        try:
            slist = sprites[sname]
        except KeyError:
            sprites[sname] = [lump]
        else:
            slist.append(lump)
    try:
        os.mkdir('sprites')
    except FileExistsError:
        pass
    palette1 = Lump('PLAYPAL', palette.data[:768])
    sstart = Lump('S_START', b'')
    send = Lump('S_END', b'')
    for name, slist in sprites.items():
        swad = Wad('IWAD')
        swad.lumps.extend([palette1, sstart])
        swad.lumps.extend(sorted(slist, key=lambda x: x.name))
        swad.lumps.append(send)
        swad.save(os.path.join('sprites', name.lower() + '.wad'))
    print('Sprites extracted:', len(sprites))

def main():
    import sys
    if len(sys.argv) != 2:
        print('Usage: wad.py FILE.wad', file=sys.stderr)
    wad = Wad.load(sys.argv[1])
    extract_sprites(wad)

main()
