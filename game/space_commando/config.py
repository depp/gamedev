#!/usr/bin/env python3
# Copyright 2014 Dietrich Epp.
import sys
from os.path import join, dirname
sys.path.append(join(dirname(__file__), 'sglib', 'script'))
import sglib

src = sglib.SourceList(base=__file__, path='src')

src.add(sources='''
main.cpp
''')

src.add(path='base', sources='''
array.hpp
chunk.cpp
chunk.hpp
file.cpp
file.hpp
image.cpp
image.hpp
log.cpp
log.hpp
mat.cpp
mat.hpp
quat.cpp
random.cpp
random.hpp
shader.cpp
shader.hpp
vec.hpp
''')

src.add(path='game', sources='''
audio.cpp
audio.hpp
audio_array.hpp
audio_enum.hpp
color.cpp
color.hpp
control.cpp
control.hpp
defs.hpp
game.cpp
game.hpp
move.cpp
move.hpp
''')

src.add(path='graphics', sources='''
defs.hpp
fov.cpp
fov.hpp
model.cpp
model.hpp
shader.cpp
shader.hpp
system.cpp
system.hpp
''')

src.add(path='world', sources='''
buffer.cpp
emit.cpp
model_cube.cpp
model.hpp
trace.cpp
world.cpp
world.hpp
''')

def configure(build, sgmod):
    mod = build.target.module()
    mod.add_sources(
        src.sources,
        {'private': [
            build.target.module()
            .add_header_path(sglib._base(__file__, 'src')),
            sgmod]})
    return mod

app = sglib.App(
    name='Space Commando',
    datapath=sglib._base(__file__, 'data'),
    email='depp@zdome.net',
    uri='http://www.moria.us/',
    copyright='Copyright © 2014 Dietrich Epp',
    identifier='us.moria.sglib-test',
    uuid='695f6334-7594-445a-92a2-8187e98e55df',
    sources=src,
    configure=configure,
    icon=None,
)

if __name__ == '__main__':
    app.run()
