/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "sg/entry.h"
#include "sg/event.h"
#include "sg/mixer.h"
#include "game/audio.hpp"
#include "game/game.hpp"
#include "graphics/system.hpp"

namespace {

const float MUSIC_VOLUME = -12.0;

Game::Game *game;
Graphics::System *graphics;

}

void sg_game_init(void) {
    Game::Audio::init();
    game = new Game::Game;
}

void sg_game_destroy(void) {}

void sg_game_getinfo(struct sg_game_info *info) {
    info->name = "Space Commando";
}

void sg_game_event(union sg_event *evt) {
    switch (evt->common.type) {
    case SG_EVENT_VIDEO_INIT:
        if (graphics) {
            delete graphics;
            graphics = nullptr;
        }
        graphics = new Graphics::System;
        sg_sys_capturemouse(1);
        break;

    default:
        break;
    }

    game->handle_event(*evt);
}

void sg_game_draw(int width, int height, double time) {
    sg_mixer_settime(time);
    game->update(time);
    sg_mixer_commit();
    graphics->draw(width, height, *game);
}
