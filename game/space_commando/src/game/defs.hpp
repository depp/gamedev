/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#ifndef LD_GAME_DEFS_HPP
#define LD_GAME_DEFS_HPP
#include "base/vec.hpp"
#include "base/quat.hpp"
#include "base/mat.hpp"
#include "base/ivec.hpp"
#include "base/ibox.hpp"
#include "base/log.hpp"
#include <cmath>
namespace Game {
using ::Base::Log;
using ::Base::Vec3;
using ::Base::Vec2;
using ::Base::Quat;
using ::Base::Mat4;
using ::Base::IVec3;
using ::Base::IBox3;

/// Results from tracing through the world.
struct Trace {
    /// Fraction of the trace completed before the hit (0..1).
    float fraction;
    /// Object position where the trace hit.
    Vec3 end;
    /// Contact position.
    Vec3 contact;
    /// Normal of surface at contact position.
    Vec3 normal;
};

}
#endif
