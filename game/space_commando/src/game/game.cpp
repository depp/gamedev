/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "game.hpp"
#include "control.hpp"
namespace Game {

namespace {
const double DEFAULT_DT = 1.0 / 16.0;
const double MAX_UPDATE = 1.0;
}

Game::Game()
    : m_frametime(0.0), m_curtime(0.0), m_dt(DEFAULT_DT), m_world(0) {
    World::Buffer buf(IVec3 { 10, 10, 10 }, 0);
    buf.fill(IBox3 {{  0,  0,  0 }, { 10, 10,  8 }}, 1);
    buf.fill(IBox3 {{  2,  2,  8 }, {  8,  8, 10 }}, 1);
    buf.fill(IBox3 {{  1,  1,  1 }, {  9,  9,  7 }}, 0);
    buf.fill(IBox3 {{  3,  3,  7 }, {  7,  7,  9 }}, 0);

    m_world.write(buf, IVec3 { 1, 1, 1 });
}

Game::~Game() {}

void Game::handle_event(const sg_event &evt) {
    m_bindings.handle_event(m_input, evt);
}

void Game::update(double time) {
    double delta = time - m_curtime;
    if (m_curtime <= 0.0) {
        m_curtime = time;
        m_frametime = time;
    } else if (delta > MAX_UPDATE) {
        Log::warn("lag");
        double skip = delta - MAX_UPDATE;
        m_frametime += skip;
        m_curtime += skip;
    }

    auto &states = m_input.states();
    if (m_frametime + m_dt <= time) {
        auto sts = std::begin(states), ste = std::end(states), stp = sts;
        do {
            double start = m_frametime;
            m_frametime += m_dt;
            auto stq = stp + 1;
            while (stq != ste && stq->time < m_frametime)
                stq++;
            m_move.update(start, m_frametime, &*stp, &*stq);
            m_move.commit();
            const auto &gun = m_move.gun_state();
            if (gun.trigger) {
                Log::debug("shoot %f %f %f",
                           gun.dir[0], gun.dir[1], gun.dir[2]);
                Trace tr;
                m_world.trace_box(&tr, gun.pos, gun.pos + (gun.dir * 20.0f),
                                  Vec3{0.1f, 0.1f, 0.1f});
                if (tr.fraction > 0.0f) {
                    Log::debug("    --> hit %f %f %f",
                               tr.end[0], tr.end[1], tr.end[2]);
                } else {
                    Log::debug("    --> miss");
                }
            }
            stp = stq - 1;
        } while (m_frametime + m_dt <= time);
        states.erase(sts, stp);
    }

    {
        auto sts = std::begin(states), ste = std::end(states), stp = sts + 1;
        while (stp != ste && stp->time < time)
            stp++;
        m_move.update(m_frametime, time, &*sts, &*stp);
    }

    m_curtime = time;
}

}
