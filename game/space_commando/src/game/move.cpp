/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "move.hpp"
#include "control.hpp"

using Game::Control::Button;

namespace Game {

namespace {
const float TURN_SPEED   = 3.0f;    // rad/s
const float LOOK_SPEED   = 0.0035f; // rad/pixel
const float MOVE_SPEED   = 10.0f;   // m/s
const float ACCELERATION = 10.0f;   // 1/s
const float STOP_SPEED   =  3.0f;   // m/s
const float FRICTION     =  9.0f;   // 1/s

const float PI = std::atan(1.0f) * 4.0f;
const float MAX_PITCH = PI * 0.5f * 0.9f;

Vec3 update_angles(Vec3 angles, Vec2 turn) {
    angles[0] -= turn[0];
    angles[1] -= turn[1];
    if (angles[0] < -PI)
        angles[0] += 2.0f * PI;
    else if (angles[0] > PI)
        angles[0] -= 2.0f * PI;
    if (angles[1] < -MAX_PITCH)
        angles[1] = -MAX_PITCH;
    else if (angles[1] > MAX_PITCH)
        angles[1] = MAX_PITCH;
    return angles;
}

}

Move::Move() {
    m_state[0] = State {
        { 6.0f, 6.0f, 3.5f }, Vec3::zero(), Vec3::zero()
    };
    m_state[1] = m_state[0];
}

void Move::update(double starttime, double endtime,
                  const Control::State *first, const Control::State *last) {
    float delta = (float) (endtime - starttime);
    Vec2 turn(Vec2::zero());
    Vec3 move(Vec3::zero());
    State state(m_state[0]);
    GunState gun;
    gun.trigger = false;

    // Read controls
    {
        auto p = first;
        double time = starttime;
        unsigned buttons = first->buttons;
        while (true) {
            Vec2 sturn = p->get_turn(TURN_SPEED);
            Vec3 smove = p->get_move();
            float mag2 = smove.mag2();
            if (mag2 > 1.0f)
                smove *= 1.0f / std::sqrt(mag2);
            p++;
            if (p == last) {
                float sdelta = (float) (endtime - time);
                move += smove * sdelta;
                turn += sturn * sdelta;
                break;
            } else {
                float sdelta = (float) (p->time - time);
                time = p->time;
                move += smove * sdelta;
                turn += sturn * sdelta + p->look * LOOK_SPEED;
                unsigned newbuttons = p->buttons & ~buttons;
                if (!gun.trigger &&
                    (newbuttons & button_mask(Button::SHOOT)) != 0) {
                    gun.trigger = true;
                    gun.time = time;
                    gun.dir = Quat::euler(update_angles(state.angles, turn))
                        .transform(Vec3 { 1.0f, 0.0f, 0.0f });
                }
            }
        }
        move *= 1.0f / delta;
    }

    // Update angle
    state.angles = update_angles(state.angles, turn);
    m_dir = Quat::euler(state.angles);

    if (delta < 1e-4)
        return;

    // Update velocity and position
    {
        Vec3 vel0 = state.vel;

        // Friction
        {
            float speed1 = state.vel.mag();
            float speed2 = speed1 -
                std::max(STOP_SPEED, speed1) * FRICTION * delta;
            if (speed2 <= 0.0f)
                state.vel = Vec3::zero();
            else
                state.vel = state.vel * (speed2 / speed1);
        }

        // Movement
        {
            Vec3 drive = (m_dir.transform(Vec3 { move[1], -move[0], 0.0f }) +
                          Vec3 { 0.0f, 0.0f, move[2] }) * MOVE_SPEED;
            float drivespeed = drive.mag();
            if (drivespeed > 0.0f) {
                Vec3 drivedir = drive * (1.0f / drivespeed);
                float curspeed = Vec3::dot(state.vel, drivedir);
                float deltaspeed = drivespeed - curspeed;
                if (deltaspeed > 0.0f) {
                    state.vel += drivedir * std::min(
                        deltaspeed,
                        ACCELERATION * delta * drivespeed);
                }
            }
        }

        // Position when the gun fired
        if (gun.trigger) {
            float gdelta = (float) (gun.time - starttime);
            float a = 0.5f * gdelta * gdelta / delta;
            gun.pos = state.pos + vel0 * (gdelta - a) + state.vel * a;
        }

        state.pos += (vel0 + state.vel) * (0.5f * delta);
    }

    m_state[1] = state;
    m_gun = gun;
}

void Move::commit() {
    m_state[0] = m_state[1];
}

}
