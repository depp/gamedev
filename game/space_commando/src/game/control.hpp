/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#ifndef LD_GAME_CONTROL_HPP
#define LD_GAME_CONTROL_HPP
#include <utility>
#include <vector>
#include "defs.hpp"
union sg_event;
namespace Game {
namespace Control {

/// On/off control buttons.
enum class Button {
    MOVE_LEFT, MOVE_RIGHT, MOVE_BACKWARD, MOVE_FORWARD,
    TURN_LEFT, TURN_RIGHT, TURN_DOWN, TURN_UP,
    JUMP, CROUCH, SHOOT, ACTION, SWAP
};

/// Number of on/off control buttons.
static const int BUTTON_COUNT = 14;

/// Get the bitmask for a button.
inline unsigned button_mask(Button button) {
    return 1u << static_cast<int>(button);
}

/// Snapshot of player input at a moment in time.
struct State {
    /// State timestamp.
    double time;
    /// The state of all buttons.
    unsigned buttons;
    /// Horizontal movement vector, a velocity.
    Vec2 move;
    /// Turning vector, an angular velocity.
    Vec2 turn;
    /// Looking vector, an angular delta.
    Vec2 look;

    /// Get the state of a button.
    bool get_button(Button button) const;
    /// Get the value of an axis controlled by buttons.
    float get_axis(Button negative, Button positive) const;
    /// Get the turning vector.
    Vec2 get_turn(float keyspeed) const;
    /// Get the movement vector.
    Vec3 get_move() const;
};

/// Record of a button being pressed.
struct ButtonPress {
    /// Device where the button was pressed, e.g., to distinguish
    /// keyboard from gamepad.
    int source;
    /// Identity of the button that was pressed, e.g., to distinguish
    /// different keys on a keyboard.
    int ident;
    /// The button which was pressed.
    Button target;
};

/// State of input, including recent history.
class Input {
private:
    std::vector<State> m_state;
    std::vector<ButtonPress> m_button;

public:
    Input();
    Input(const Input &) = delete;
    ~Input();
    Input &operator=(const Input &) = delete;

    /// Record a button press.
    void button_press(double time, int source, int ident, Button button);
    /// Record a button release.
    void button_release(double time, int source, int ident);
    /// Look with the mouse.
    void look(double time, Vec2 amt);
    /// Get a reference to the internal list of states.
    std::vector<State> &states() { return m_state; }

private:
    /// Set the button mask.
    void buttons_set(double time, unsigned buttons);
};

/// A binding for a button.
struct ButtonBinding {
    int source;
    Button target;
};

/// A map from device buttons to virtual buttons.
class ButtonBindings {
private:
    std::vector<signed char> m_map;

public:
    ButtonBindings();
    ButtonBindings(const ButtonBindings &) = delete;
    ~ButtonBindings();
    ButtonBindings &operator=(const ButtonBindings &) = delete;

    /// Clear all bindings.
    void clear();
    /// Bind a device button to a target.
    void bind(int source, Button target);
    /// Bind many device buttons to targets.
    template<class I>
    void bind(I first, I last);
    /// Get the target for a device button, or -1 if unbound.
    int get(int source) const;
};

template<class I>
void ButtonBindings::bind(I first, I last) {
    for (I p = first; p != last; ++p)
        bind(p->source, p->target);
}

/// Control bindings.
class Bindings {
private:
    ButtonBindings m_button_map[2];

public:
    Bindings();
    Bindings(const Bindings &) = delete;
    ~Bindings();
    Bindings &operator=(const Bindings &) = delete;

    /// Handle an event.  Returns true if the event was handled.
    bool handle_event(Input &input, const sg_event &evt);

private:
    bool handle_button(Input &input, double time, int device, int ident,
                       bool state);
};

}
}
#endif
