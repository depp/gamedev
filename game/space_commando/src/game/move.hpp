/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#ifndef LD_GAME_MOVE_HPP
#define LD_GAME_MOVE_HPP
#include "defs.hpp"
namespace Game {
namespace Control {
struct State;
}

class Move {
public:
    struct State {
        Vec3 pos;
        Vec3 vel;
        Vec3 angles;
    };

    struct GunState {
        bool trigger;
        double time;
        Vec3 dir;
        Vec3 pos;
    };

private:
    State m_state[2];
    GunState m_gun;
    Quat m_dir;

public:
    Move();
    void update(double starttime, double endtime,
                const Control::State *first, const Control::State *last);
    void commit();

    Vec3 position() const { return m_state[1].pos; }
    Quat direction() const { return m_dir; }
    const GunState &gun_state() const { return m_gun; }

};

}
#endif
