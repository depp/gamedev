/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "control.hpp"
#include "sg/entry.h"
#include "sg/event.h"
#include "sg/keycode.h"
#include <algorithm>
namespace Game {
namespace Control {

namespace {

const struct ButtonBinding STANDARD_KEY[] = {
    { KEY_W,      Button::MOVE_FORWARD },
    { KEY_A,      Button::MOVE_LEFT },
    { KEY_S,      Button::MOVE_BACKWARD },
    { KEY_D,      Button::MOVE_RIGHT },

    { KEY_Up,     Button::MOVE_FORWARD },
    { KEY_Left,   Button::TURN_LEFT },
    { KEY_Down,   Button::MOVE_BACKWARD },
    { KEY_Right,  Button::TURN_RIGHT },

    { KEY_Space,  Button::JUMP },
    { KEY_LeftControl, Button::CROUCH },
    { KEY_C,      Button::CROUCH },
    { KEY_E,      Button::ACTION },
    { KEY_Enter,  Button::ACTION },
    { KEY_Q,      Button::SWAP },
    { KEY_Tab,    Button::SWAP }
};

const struct ButtonBinding STANDARD_MOUSEBUTTON[] = {
    { 0, Button::SHOOT },
    { 1, Button::JUMP },
};

State clone_state(const State &st, double time) {
    return State {
        time, st.buttons, st.move, st.turn, Vec2::zero()
    };
}

}

bool State::get_button(Button button) const {
    return (buttons & button_mask(button)) != 0;
}

float State::get_axis(Button negative, Button positive) const {
    return (get_button(negative) ? -1.0f : 0.0f) +
        (get_button(positive) ? +1.0f : 0.0f);
}

Vec2 State::get_turn(float keyspeed) const {
    Vec2 keyturn { get_axis(Button::TURN_LEFT, Button::TURN_RIGHT),
                   get_axis(Button::TURN_DOWN, Button::TURN_UP) };
    return keyturn * keyspeed;
}

Vec3 State::get_move() const {
    Vec3 keymove { get_axis(Button::MOVE_LEFT, Button::MOVE_RIGHT),
                   get_axis(Button::MOVE_BACKWARD, Button::MOVE_FORWARD),
                   get_axis(Button::CROUCH, Button::JUMP) };
    return keymove;
}

/* ======================================================================
   Input
   ====================================================================== */

Input::Input() {
    m_state.push_back(State {
        0.0, 0, Vec2::zero(), Vec2::zero(), Vec2::zero()
    });
}

Input::~Input() { }

void Input::button_press(double time, int source, int ident, Button button) {
    for (auto &b : m_button) {
        if (source == b.source && ident == b.ident)
            return;
    }

    m_button.push_back(ButtonPress { source, ident, button });
    buttons_set(time, m_state.back().buttons | button_mask(button));
}

void Input::button_release(double time, int source, int ident) {
    Button button;

    {
        auto i = std::begin(m_button), e = std::end(m_button);
        for (; i != e; i++) {
            if (source == i->source && ident == i->ident)
                break;
        }
        if (i == e)
            return;
        button = i->target;
        m_button.erase(i);
    }

    {
        auto i = std::begin(m_button), e = std::end(m_button);
        for (; i != e; i++) {
            if (i->target == button)
                return;
        }
    }

    buttons_set(time, m_state.back().buttons & ~button_mask(button));
}

void Input::look(double time, Vec2 amt) {
    State &cur = m_state.back();
    if (cur.time >= time) {
        cur.look += amt;
    } else {
        State st = clone_state(cur, time);
        st.look = amt;
        m_state.push_back(st);
    }
}

void Input::buttons_set(double time, unsigned buttons) {
    State &cur = m_state.back();
    if (cur.buttons == buttons)
        return;
    if (cur.time >= time) {
        if (m_state.size() >= 2) {
            State &prev = m_state[m_state.size() - 2];
            unsigned diff = buttons ^ cur.buttons;
            if ((prev.buttons & diff) == (cur.buttons & diff)) {
                cur.buttons = buttons;
                return;
            }
        }
        time = cur.time;
    }
    State st = clone_state(cur, time);
    st.buttons = buttons;
    m_state.push_back(st);
}

/* ======================================================================
   ButtonBindings
   ====================================================================== */

ButtonBindings::ButtonBindings() { }

ButtonBindings::~ButtonBindings() { }

void ButtonBindings::clear() {
    m_map.clear();
}

void ButtonBindings::bind(int source, Button target) {
    int target_idx = static_cast<int>(target);
    if (source < 0 || source >= 256 ||
        target_idx < 0 || target_idx >= BUTTON_COUNT)
        sg_sys_abort("Control::ButtonBindings::bind");
    std::size_t s = source;
    if (s >= m_map.size())
        m_map.resize(s + 1, -1);
    m_map[s] = target_idx;
}

int ButtonBindings::get(int source) const {
    if (source < 0 || static_cast<std::size_t>(source) >= m_map.size())
        return -1;
    return m_map[source];
}

/* ======================================================================
   Bindings
   ====================================================================== */

Bindings::Bindings() {
    m_button_map[0].bind(
        std::begin(STANDARD_KEY),
        std::end(STANDARD_KEY));
    m_button_map[1].bind(
        std::begin(STANDARD_MOUSEBUTTON),
        std::end(STANDARD_MOUSEBUTTON));
}

Bindings::~Bindings() { }

bool Bindings::handle_event(Input &input, const sg_event &evt) {
    switch (evt.common.type) {
    case SG_EVENT_KDOWN:
    case SG_EVENT_KUP:
        return handle_button(input, evt.key.time, 0, evt.key.key,
                             evt.common.type == SG_EVENT_KDOWN);

    case SG_EVENT_MDOWN:
    case SG_EVENT_MUP:
        return handle_button(input, evt.mouse.time, 1, evt.mouse.button,
                             evt.common.type == SG_EVENT_MDOWN);

    case SG_EVENT_MMOVEREL:
        input.look(evt.mouse.time,
                   Vec2 { static_cast<float>(evt.mouse.x),
                          static_cast<float>(evt.mouse.y) });
        return true;

    default:
        return false;
    }
}

bool Bindings::handle_button(Input &input, double time, int device, int ident,
                             bool state) {
    if (state) {
        int button = m_button_map[device].get(ident);
        if (button >= 0) {
            input.button_press(time, device, ident,
                               static_cast<Button>(button));
        }
    } else {
        input.button_release(time, device, ident);
    }
    return true;
}

}
}
