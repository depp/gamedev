/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#ifndef LD_GRAPHICS_SHADER_HPP
#define LD_GRAPHICS_SHADER_HPP
#include "base/shader.hpp"
namespace Graphics {
namespace Shader {

/// Uniforms and attributes for the "text" shader.
struct Text {
    static const Base::ShaderField UNIFORMS[];
    static const Base::ShaderField ATTRIBUTES[];

    GLint a_vert;
    GLint u_vertxform;
    GLint u_vertoff;
    GLint u_texscale;
    GLint u_texture;
    GLint u_color;
};

/// Uniforms and attributes for the "world" shader.
struct World {
    static const Base::ShaderField UNIFORMS[];
    static const Base::ShaderField ATTRIBUTES[];

    GLint a_vert;

    GLint u_modelview;
    GLint u_projection;
};

/// Uniforms and attributes for the "entity" shader.
struct Entity {
    static const Base::ShaderField UNIFORMS[];
    static const Base::ShaderField ATTRIBUTES[];

    GLint vertex;

    GLint modelview;
    GLint projection;
};

}
}
#endif
