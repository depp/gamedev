/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "defs.hpp"
#include "fov.hpp"
#include "model.hpp"
#include "system.hpp"
#include "world/world.hpp"
#include "game/game.hpp"
namespace Graphics {

namespace {
const Quat LOOK = { 0.5f, -0.5f, 0.5f, 0.5f };
}

System::System()
    : m_prog_world("world", "world"),
      m_worlddirty(true) {
    m_models.load();
}

System::~System() { }

void System::draw(int width, int height, const Game::Game &game) {
    if (m_worlddirty)
        update_world(game);

    glViewport(0, 0, width, height);
    glClearColor(0.0f, 0.1f, 0.2f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Fov fov {
        Fov::Type::ANGLE,
        Fov::Mode::EQUAL_AREA,
        82.0f
    };
    Vec2 size { (float) width, (float) height };
    Mat4 p = fov.get_projection_matrix(size, 0.25f, 100.0f);
    Mat4 wv = Mat4::rotation(LOOK * game.m_move.direction().conjugate()) *
        Mat4::translation(-game.m_move.position());
    const auto &prog = m_prog_world;

    glUseProgram(prog.prog());
    glEnableVertexAttribArray(prog->a_vert);
    glBindBuffer(GL_ARRAY_BUFFER, m_worldbuf);
    glVertexAttribPointer(prog->a_vert, 4, GL_UNSIGNED_BYTE, GL_FALSE, 0, 0);
    glUniformMatrix4fv(prog->u_projection, 1, GL_FALSE, p.data());
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    for (World::SectionDrawData &sec : m_worldsection) {
        Mat4 mv = wv * Mat4::translation(sec.offset);
        glUniformMatrix4fv(prog->u_modelview, 1, GL_FALSE, mv.data());
        glDrawArrays(GL_TRIANGLES, sec.first, sec.count);
    }

    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    m_models.draw(1, wv, p);

    sg_opengl_checkerror("System::draw");
}

void System::update_world(const Game::Game &game) {
    World::WorldDrawData dd = game.m_world.emit();
    Base::Log::info("vertex count: %u", dd.vertex.size());
    glGenBuffers(1, &m_worldbuf);
    glBindBuffer(GL_ARRAY_BUFFER, m_worldbuf);
    dd.vertex.upload(GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    m_worldsection = std::move(dd.section);
    m_worlddirty = false;
}

}
