/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#ifndef LD_GRAPHICS_MODEL_HPP
#define LD_GRAPHICS_MODEL_HPP
#include "defs.hpp"
#include "shader.hpp"
#include <vector>
namespace Graphics {

class ModelData {
public:
    struct Model;

private:
    ::Base::Program<Shader::Entity> m_prog_entity;
    GLuint m_buffer;
    unsigned m_index_offset;
    std::vector<Model> m_models;

public:
    ModelData();
    ModelData(const ModelData &) = delete;
    ~ModelData();
    ModelData &operator=(const ModelData &) = delete;

    void load();
    void draw(int model_index,
              const Mat4 &modelview, const Mat4 &projection);
};

}
#endif
