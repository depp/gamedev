/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#ifndef LD_GRAPHICS_FOV_HPP
#define LD_GRAPHICS_FOV_HPP
#include "defs.hpp"
namespace Graphics {

struct Fov {
    enum class Type {
        ANGLE,
        FOCAL_LENGTH
    };

    enum class Mode {
        HORIZONTAL,
        VERTICAL,
        DIAGONAL,
        EQUAL_AREA
    };

    Type type;
    Mode mode;
    float value;

    /// Get the primary ratio used for calculating perspective.
    float get_primary_ratio() const;

    /// Get FOV ratios for the given screen size
    Vec2 get_ratios(Base::Vec2 size) const;

    /// Get the projection matrix for the given screen size.
    Mat4 get_projection_matrix(Base::Vec2 size, float near, float far) const;
};

}
#endif
