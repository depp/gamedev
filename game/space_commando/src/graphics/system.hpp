/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#ifndef LD_GRAPHICS_SYSTEM_HPP
#define LD_GRAPHICS_SYSTEM_HPP
#include "model.hpp"
#include "shader.hpp"
#include "world/world.hpp"
#include "sg/opengl.h"
namespace Game {
class Game;
}
namespace Graphics {

/// The graphics system, responsible for drawing everything.
class System {
private:
    ::Base::Program<Shader::World> m_prog_world;
    bool m_worlddirty;
    GLuint m_worldbuf;
    std::vector<World::SectionDrawData> m_worldsection;
    ModelData m_models;

public:
    System();
    System(const System &) = delete;
    ~System();
    System &operator=(const System &) = delete;

    /// Draw the game's graphics.
    void draw(int width, int height, const Game::Game &game);

private:
    void update_world(const Game::Game &game);
};

}
#endif
