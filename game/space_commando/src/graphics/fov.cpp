/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "fov.hpp"
#include <cmath>
namespace Graphics {

float Fov::get_primary_ratio() const {
    switch (type) {
    case Type::ANGLE:
        return std::tan(value * (std::atan(1.0f) / 90.0f));

    case Type::FOCAL_LENGTH:
        float length;
        switch (mode) {
        case Mode::HORIZONTAL:
            length = 18.0f;
            break;
        case Mode::VERTICAL:
            length = 12.0f;
            break;
        case Mode::DIAGONAL:
            length = std::sqrt(18.0f * 18.0f + 12.0f * 12.0f);
            break;
        case Mode::EQUAL_AREA:
            length = std::sqrt(12.0f * 18.0f);
            break;
        }
        return length / value;
    }

    return 0.0f;
}

Vec2 Fov::get_ratios(Vec2 size) const {
    float aspect = size[0] / size[1];
    float ratio = get_primary_ratio();
    switch (mode) {
    case Mode::HORIZONTAL:
        return Vec2 { ratio, ratio / aspect };
    case Mode::VERTICAL:
        return Vec2 { ratio * aspect, ratio };
    case Mode::DIAGONAL:
        return Vec2 { ratio / std::sqrt(1 + 1 / (aspect*aspect)),
                      ratio / std::sqrt(1 + aspect*aspect) };
    case Mode::EQUAL_AREA:
        float a = std::sqrt(aspect);
        return Vec2 { ratio * a, ratio / a };
    }

    return Vec2::zero();
}

Mat4 Fov::get_projection_matrix(Vec2 size, float near, float far) const {
    Vec2 ratios = get_ratios(size);
    return Mat4::perspective(ratios[0], ratios[1], near, far);
}

}
