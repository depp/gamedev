/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "model.hpp"
#include "base/chunk.hpp"
#include "base/file.hpp"
#include "sg/defs.h"

#include <cstring>

namespace Graphics {

namespace {

const std::size_t MAX_MODEL_SIZE = 1024 * 128;

const int MAGIC_SIZE = 16;
const char MAGIC[MAGIC_SIZE] = "SGLib Model";

const unsigned VERTEX_SIZE = 12;
const unsigned INDEX_SIZE = 2;

struct ModelFile {
    ::Base::Data data;
    unsigned vertex_count;
    unsigned index_count;
    const void *vertex_data;
    const void *index_data;

    static ModelFile load(const std::string &name);
};

ModelFile ModelFile::load(const std::string &name) {
    ModelFile file;
    file.vertex_count = 0;
    file.index_count = 0;
    file.vertex_data = nullptr;
    file.index_data = nullptr;

    {
        std::string rpath("model/");
        rpath += name;
        file.data.read(rpath, MAX_MODEL_SIZE, "sgmod");
    }
    const char *path = file.data.path();

    Base::ChunkReader chunks;
    if (!chunks.read(file.data)) {
        Log::abort("%s: Corrupted model.", path);
    }

    if (std::memcmp(chunks.magic(), MAGIC, MAGIC_SIZE)) {
        Log::abort("%s: Corrupted model.", path);
    }

    {
        auto version = chunks.version();
        if (version.first != 1) {
            Log::abort("%s: Unknown version: %d.%d.",
                       path, version.first, version.second);
        }
    }

    {
        auto chunk = chunks.get("ADAT");
        if (!chunk.first) {
            Log::abort("%s: Missing essential chunks.", path);
        }
        file.vertex_count = (unsigned) (chunk.second / VERTEX_SIZE);
        file.vertex_data = chunk.first;
    }

    {
        auto chunk = chunks.get("IDAT");
        if (!chunk.first) {
            Log::abort("%s: Missing essential chunks.", path);
        }
        file.index_count = (unsigned) (chunk.second / INDEX_SIZE);
        file.index_data = chunk.first;
    }

    return file;
}

};

struct ModelData::Model {
    unsigned count;
    unsigned base_index;
    unsigned base_vertex;
};

ModelData::ModelData()
    : m_prog_entity("entity", "entity"),
      m_buffer(0), m_index_offset(0), m_models() {}

ModelData::~ModelData() { }

void ModelData::load() {
    if (m_buffer) {
        glDeleteBuffers(1, &m_buffer);
        m_buffer = 0;
    }
    m_models.clear();

    std::vector<std::string> names { "cube", "column" };
    std::vector<ModelFile> files;
    for (const auto &name : names) {
        files.push_back(ModelFile::load(name));
    }

    unsigned vertex_count = 0, index_count = 0;
    for (const auto &file : files) {
        vertex_count += file.vertex_count;
        index_count += file.index_count;
    }
    Log::info("Loaded models: %u models, %u vertexes, %u indexes",
              (unsigned) files.size(), vertex_count, index_count);

    GLuint buffer = 0;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(
        GL_ARRAY_BUFFER,
        vertex_count * VERTEX_SIZE + index_count * INDEX_SIZE,
        nullptr,
        GL_STATIC_DRAW);
    std::vector<Model> models;
    unsigned vertex_pos = 0, index_pos = 0;
    for (const auto &file : files) {
        {
            Model model;
            model.count = file.index_count;
            model.base_index = index_pos;
            model.base_vertex = vertex_pos;
            models.push_back(model);
        }
        glBufferSubData(
            GL_ARRAY_BUFFER,
            vertex_pos * VERTEX_SIZE,
            file.vertex_count * VERTEX_SIZE,
            file.vertex_data);
        glBufferSubData(
            GL_ARRAY_BUFFER,
            vertex_count * VERTEX_SIZE + index_pos * INDEX_SIZE,
            file.index_count * INDEX_SIZE,
            file.index_data);
        vertex_pos += file.vertex_count;
        index_pos += file.index_count;
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    m_buffer = buffer;
    m_index_offset = vertex_count * VERTEX_SIZE;
    m_models = std::move(models);
}

void ModelData::draw(int model_index,
                     const Mat4 &modelview, const Mat4 &projection) {
    if (model_index < 0)
        return;
    const auto &prog = m_prog_entity;
    const auto &model = m_models[model_index];

    glUseProgram(prog.prog());
    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    glEnableVertexAttribArray(prog->vertex);
    glVertexAttribPointer(
        prog->vertex, 3, GL_FLOAT, GL_FALSE, 0,
        reinterpret_cast<void *>(model.base_vertex * VERTEX_SIZE));

    glUniformMatrix4fv(prog->modelview, 1, GL_FALSE, modelview.data());
    glUniformMatrix4fv(prog->projection, 1, GL_FALSE, projection.data());

    glDrawElements(
        GL_TRIANGLES, model.count, GL_UNSIGNED_SHORT,
        reinterpret_cast<void *>(
            m_index_offset + model.base_index * INDEX_SIZE));

    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
}

}
