/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "shader.hpp"
namespace Graphics {
namespace Shader {
using Base::ShaderField;

#define FIELD(n) { #n, offsetof(TYPE, n) }

#define TYPE Text
const ShaderField TYPE::UNIFORMS[] = {
    FIELD(u_vertxform),
    FIELD(u_vertoff),
    FIELD(u_texscale),
    FIELD(u_texture),
    FIELD(u_color),
    { nullptr, 0 }
};

const ShaderField TYPE::ATTRIBUTES[] = {
    FIELD(a_vert),
    { nullptr, 0 }
};
#undef TYPE

#define TYPE World
const ShaderField TYPE::UNIFORMS[] = {
    FIELD(u_modelview),
    FIELD(u_projection),
    { nullptr, 0 }
};

const ShaderField TYPE::ATTRIBUTES[] = {
    FIELD(a_vert),
    { nullptr, 0 }
};
#undef TYPE

#define TYPE Entity
const ShaderField TYPE::UNIFORMS[] = {
    FIELD(modelview),
    FIELD(projection),
    { nullptr, 0 }
};

const ShaderField TYPE::ATTRIBUTES[] = {
    FIELD(vertex),
    { nullptr, 0 }
};
#undef TYPE

}
}
