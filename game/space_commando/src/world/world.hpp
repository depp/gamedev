/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#ifndef LD_WORLD_WORLD_HPP
#define LD_WORLD_WORLD_HPP
#include "base/array.hpp"
#include "base/vec.hpp"
#include "base/ivec.hpp"
#include "base/ibox.hpp"
#include "base/log.hpp"
#include "game/defs.hpp"
#include <vector>
namespace World {

using ::Base::Log;
using ::Base::Array;
using ::Base::Vec3;
using ::Base::IVec3;
using ::Base::IBox3;
using ::Game::Trace;

/// Size of a block.
static const int BLOCK_SIZE = 16;

/// Logarithm of a section's size, in blocks.
static const int SECTION_BITS = 3;

/// Size of a section, in blocks.
static const int SECTION_SIZE = 1 << SECTION_BITS;

// ============================================================
// Conversions to and from section / block coordinates

inline int section_from_block(int x) {
    return x >> SECTION_BITS;
}

inline int section_from_block_ceil(int x) {
    return (x + SECTION_SIZE - 1) >> SECTION_BITS;
}

inline IVec3 section_from_block(IVec3 v) {
    IVec3 r;
    for (int i = 0; i < 3; i++)
        r[i] = section_from_block(v[i]);
    return r;
}

inline IVec3 section_from_block_ceil(IVec3 v) {
    IVec3 r;
    for (int i = 0; i < 3; i++)
        r[i] = section_from_block_ceil(v[i]);
    return r;
}

inline IBox3 section_from_block(const IBox3 &v) {
    return IBox3 { section_from_block(v.mins),
                   section_from_block_ceil(v.maxs) };
}

inline int block_from_section(int x) {
    return x << SECTION_BITS;
}

inline IVec3 block_from_section(IVec3 v) {
    IVec3 r;
    for (int i = 0; i < 3; i++)
        r[i] = block_from_section(v[i]);
    return r;
}

inline IBox3 block_from_section(const IBox3 &v) {
    return IBox3 { block_from_section(v.mins),
                   block_from_section(v.maxs) };
}

inline int index3(IVec3 size, IVec3 p) {
    return (p[2] * size[1] + p[1]) * size[0] + p[0];
}

inline int index3(const IBox3 &box, IVec3 p) {
    return index3(box.maxs - box.mins, p - box.mins);
}

/// World vertex data.
struct Vertex {
    /// Location, relative to the section origin.
    unsigned char x, y, z;
    /// Face attributes.
    unsigned char face;
};

/// Information about a section's vertexes.
struct SectionDrawData {
    Base::Vec3 offset;
    int first;
    int count;
};

/// Information about how to draw the world.
struct WorldDrawData {
    Base::Array<Vertex> vertex;
    std::vector<SectionDrawData> section;
};

/// Data for a section.
struct SectionData {
    unsigned char block[SECTION_SIZE][SECTION_SIZE][SECTION_SIZE];

    unsigned char ref(IVec3 p) const {
        return block[p[2]][p[1]][p[0]];
    }
    unsigned char &ref(IVec3 p) {
        return block[p[2]][p[1]][p[0]];
    }
};

/// A buffer for creating world data.
class Buffer {
private:
    IVec3 m_size;
    std::vector<unsigned char> m_data;

public:
    Buffer();
    Buffer(IVec3 size, unsigned char fill);

    IVec3 size() const { return m_size; }
    unsigned char *data() { return m_data.data(); }
    const unsigned char *data() const { return m_data.data(); }
    unsigned char &ref(IVec3 v) {
        return m_data[(v[2] * m_size[1] + v[1]) * m_size[0] + v[0]];
    }
    unsigned char ref(IVec3 v) const {
        return m_data[(v[2] * m_size[1] + v[1]) * m_size[0] + v[0]];
    }
    void fill(IBox3 b, unsigned char fill);
};

/// The world: a 3D grid of blocks.
class World {
    std::vector<SectionData> m_sectiondata;
    std::vector<int> m_sectionindex;
    IBox3 m_bounds;

public:
    explicit World(int fill);
    World(const World &) = delete;
    ~World();
    World &operator=(const World &) = delete;

    /// Write a 3D rectangle of data.
    void write(const unsigned char *block, const IBox3 &bounds);
    void write(const Buffer &block, const IVec3 &offset);

    /// Emit drawing data.
    WorldDrawData emit() const;

    /// Trace a box across the world.
    void trace_box(Trace *tr, Vec3 start, Vec3 end, Vec3 size) const;

private:
    /// Expand to contain the given box.
    void expand(const IBox3 &box);
    /// Get a section, creating it if necessary.
    SectionData &get_section_write(IVec3 vec);
    /// Get a section.
    const SectionData &get_section(IVec3 vec) const;
};

}
#endif
