/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "model.hpp"
namespace World {

namespace {

const int CUBE_VERTEXCOUNT = 36;

const unsigned char CUBE_VERTEX[CUBE_VERTEXCOUNT * 4] = {
    // -X
    0, 0, 0, 0,
    0, 0, 1, 0,
    0, 1, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 1, 1, 0,

    // +X
    1, 0, 0, 1,
    1, 1, 0, 1,
    1, 0, 1, 1,
    1, 0, 1, 1,
    1, 1, 0, 1,
    1, 1, 1, 1,

    // -Y
    0, 0, 0, 2,
    1, 0, 0, 2,
    0, 0, 1, 2,
    0, 0, 1, 2,
    1, 0, 0, 2,
    1, 0, 1, 2,

    // +Y
    0, 1, 0, 3,
    0, 1, 1, 3,
    1, 1, 0, 3,
    1, 1, 0, 3,
    0, 1, 1, 3,
    1, 1, 1, 3,

    // -Z
    0, 0, 0, 4,
    0, 1, 0, 4,
    1, 0, 0, 4,
    1, 0, 0, 4,
    0, 1, 0, 4,
    1, 1, 0, 4,

    // +Z
    0, 0, 1, 5,
    1, 0, 1, 5,
    0, 1, 1, 5,
    0, 1, 1, 5,
    1, 0, 1, 5,
    1, 1, 1, 5
};

}

const Model CUBE = {
    CUBE_VERTEX,
    CUBE_VERTEXCOUNT,
    16,
    { 1, 1, 1, 1, 1, 1 }
};

}
