/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "world.hpp"
#include "base/log.hpp"
#include <cstring>
namespace World {

World::World(int fill)
    : m_sectiondata(1), m_bounds(IBox3::zero()) {
    std::memset(m_sectiondata[0].block, fill,
                SECTION_SIZE * SECTION_SIZE * SECTION_SIZE);
}

World::~World() { }

namespace {

void write_section(SectionData &sec, const unsigned char *block,
                   const IBox3 bounds) {
    const int N = SECTION_SIZE;
    IBox3 b = IBox3::intersection(bounds, IBox3 {{ 0, 0, 0 }, { N, N, N }});
    IVec3 t;
    for (t[2] = b.mins[2]; t[2] < b.maxs[2]; t[2]++)
        for (t[1] = b.mins[1]; t[1] < b.maxs[1]; t[1]++)
            for (t[0] = b.mins[0]; t[0] < b.maxs[0]; t[0]++)
                sec.ref(t) = block[index3(bounds, t)];
}

}

void World::write(const unsigned char *block, const IBox3 &bounds) {
    IBox3 sb = section_from_block(bounds);
    expand(sb);
    IVec3 s;
    for (s[2] = sb.mins[2]; s[2] < sb.maxs[2]; s[2]++)
        for (s[1] = sb.mins[1]; s[1] < sb.maxs[1]; s[1]++)
            for (s[0] = sb.mins[0]; s[0] < sb.maxs[0]; s[0]++)
                write_section(get_section_write(s),
                              block, bounds - block_from_section(s));
}

void World::write(const Buffer &block, const IVec3 &offset) {
    write(block.data(), IBox3{ offset, offset + block.size() });
}

void World::expand(const IBox3 &box) {
    if (m_bounds.contains(box))
        return;
    // Base::Log::debug("expanding");
    IBox3 ob = m_bounds, nb = IBox3::union_(ob, box);
    std::vector<int> nindex(nb.volume(), 0);
    IVec3 s;
    for (s[2] = ob.mins[2]; s[2] < ob.maxs[2]; s[2]++)
        for (s[1] = ob.mins[1]; s[1] < ob.maxs[1]; s[1]++)
            for (s[0] = ob.mins[0]; s[0] < ob.maxs[0]; s[0]++)
                nindex[index3(nb, s)] = m_sectionindex[index3(ob, s)];
    std::swap(nindex, m_sectionindex);
    m_bounds = nb;
}

SectionData &World::get_section_write(IVec3 vec) {
    int &si = m_sectionindex[index3(m_bounds, vec)];
    if (si != 0)
        return m_sectiondata[si];
    // Base::Log::debug("new section");
    int nsi = m_sectiondata.size();
    si = nsi;
    m_sectiondata.resize(nsi + 1);
    std::memcpy(&m_sectiondata[nsi].block,
                &m_sectiondata[0].block,
                sizeof(m_sectiondata[0].block));
    return m_sectiondata[nsi];
}

const SectionData &World::get_section(IVec3 vec) const {
    return m_sectiondata[m_sectionindex[index3(m_bounds, vec)]];
}

}
