/* Copyright 2014 make.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "world.hpp"
#include "sg/entry.h"
namespace World {

Buffer::Buffer()
    : m_size(IVec3::zero()) { }

Buffer::Buffer(IVec3 size, unsigned char fill)
    : m_size(size), m_data(size[0] * size[1] * size[2], fill) { }

void Buffer::fill(IBox3 b, unsigned char fill) {
    IVec3 p, sz(m_size);
    IBox3 bounds { { 0, 0, 0 }, m_size };
    if (!bounds.contains(b))
        sg_sys_abort("Game::World::Buffer::fill");
    for (p[2] = b.mins[2]; p[2] < b.maxs[2]; p[2]++)
        for (p[1] = b.mins[1]; p[1] < b.maxs[1]; p[1]++)
            for (p[0] = b.mins[0]; p[0] < b.maxs[0]; p[0]++)
                m_data[index3(sz, p)] = fill;
}

}
