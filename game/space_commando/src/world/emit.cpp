/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "model.hpp"
#include "world.hpp"
#include <cstring>
namespace World {

namespace {

struct SectionFaces {
    unsigned char face[SECTION_SIZE+2][SECTION_SIZE+2][SECTION_SIZE+2];
};

void gen_faces(const SectionData &d, SectionFaces &f) {
    std::memset(f.face, 0, sizeof(f.face));
    const int N = SECTION_SIZE;
    for (int z = 0; z < N; z++) {
        for (int y = 0; y < N; y++) {
            for (int x = 0; x < N; x++) {
                int block = d.block[z][y][x];
                if (!block)
                    continue;
                const Model &model = CUBE;
                f.face[z+1][y+1][x+0] |= model.faces[0] << 1;
                f.face[z+1][y+1][x+2] |= model.faces[1] << 0;
                f.face[z+1][y+0][x+1] |= model.faces[2] << 3;
                f.face[z+1][y+2][x+1] |= model.faces[3] << 2;
                f.face[z+0][y+1][x+1] |= model.faces[4] << 5;
                f.face[z+2][y+1][x+1] |= model.faces[5] << 4;
            }
        }
    }
}

void glue_faces_x(SectionFaces &f1, SectionFaces &f2) {
    const int N = SECTION_SIZE;
    for (int z = 1; z <= N; z++) {
        for (int y = 1; y <= N; y++) {
            f1.face[z][y][  N] |= f2.face[z][y][  0];
            f2.face[z][y][  1] |= f1.face[z][y][N+1];
        }
    }
}

void glue_faces_y(SectionFaces &f1, SectionFaces &f2) {
    const int N = SECTION_SIZE;
    for (int z = 1; z <= N; z++) {
        for (int x = 1; x <= N; x++) {
            f1.face[z][  N][x] |= f2.face[z][  0][x];
            f2.face[z][  1][x] |= f1.face[z][N+1][x];
        }
    }
}

void glue_faces_z(SectionFaces &f1, SectionFaces &f2) {
    const int N = SECTION_SIZE;
    for (int y = 1; y <= N; y++) {
        for (int x = 1; x <= N; x++) {
            f1.face[  N][y][x] |= f2.face[  0][y][x];
            f2.face[  1][y][x] |= f1.face[N+1][y][x];
        }
    }
}

struct WorldFaces {
    int xs, ys, zs;
    SectionFaces fillfaces;
    std::vector<SectionFaces> sectfaces;

    WorldFaces(int xs, int ys, int zs)
        : xs(xs), ys(ys), zs(zs), sectfaces(xs * ys * zs) { }

    SectionFaces &get(int x, int y, int z) {
        if (x < 0 || x >= xs || y < 0 || y >= ys || z < 0 || z >= zs)
            return fillfaces;
        return sectfaces[(z * ys + y) * xs + x];
    }
};

void vertex_emit(const SectionData &d, const SectionFaces &f,
                 Base::Array<Vertex> &arr) {
    const int N = SECTION_SIZE;
    for (int z = 0; z < N; z++) {
        for (int y = 0; y < N; y++) {
            for (int x = 0; x < N; x++) {
                int block = d.block[z][y][x];
                unsigned bfaces = f.face[z+1][y+1][x+1];
                if (!block)
                    continue;
                const Model &model = CUBE;
                const unsigned char *mvert = model.vertex;
                int n = model.vertexcount;
                int a = model.scale;
                Vertex *vert = arr.insert(n);
                for (int i = 0; i < n; i++) {
                    unsigned face = mvert[i*4+3];
                    if ((bfaces & (1 << face)) != 0)
                        continue;
                    vert->x = mvert[i*4+0] * a + x * BLOCK_SIZE;
                    vert->y = mvert[i*4+1] * a + y * BLOCK_SIZE;
                    vert->z = mvert[i*4+2] * a + z * BLOCK_SIZE;
                    vert->face = face;
                    vert++;
                }
                arr.set_end(vert);
            }
        }
    }
}

}

WorldDrawData World::emit() const {
    IBox3 b(m_bounds);
    int xs = b.maxs[0] - b.mins[0],
        ys = b.maxs[1] - b.mins[1],
        zs = b.maxs[2] - b.mins[2],
        scount = xs * ys * zs;

    WorldFaces faces(xs, ys, zs);
    std::memset(&faces.fillfaces.face, 0xff, sizeof(faces.fillfaces.face));
    for (int i = 0; i < scount; i++)
        gen_faces(m_sectiondata[m_sectionindex[i]],
                  faces.sectfaces[i]);

    for (int z = 0; z <= zs; z++) {
        for (int y = 0; y <= ys; y++) {
            for (int x = 0; x <= xs; x++) {
                SectionFaces &f = faces.get(x, y, z);
                glue_faces_x(faces.get(x-1, y, z), f);
                glue_faces_y(faces.get(x, y-1, z), f);
                glue_faces_z(faces.get(x, y, z-1), f);
            }
        }
    }

    WorldDrawData data;
    for (int z = 0; z < zs; z++) {
        for (int y = 0; y < ys; y++) {
            for (int x = 0; x < xs; x++) {
                int idx = (z * ys + y) * xs + x;
                SectionDrawData sdata;
                sdata.first = data.vertex.size();
                vertex_emit(m_sectiondata[m_sectionindex[idx]],
                            faces.get(x, y, z),
                            data.vertex);
                sdata.count = data.vertex.size() - sdata.first;
                if (sdata.count == 0)
                    continue;
                sdata.offset = Base::Vec3 {
                    (float) ((b.mins[0] + x) * SECTION_SIZE),
                    (float) ((b.mins[1] + y) * SECTION_SIZE),
                    (float) ((b.mins[2] + z) * SECTION_SIZE)
                };
                data.section.push_back(sdata);
            }
        }
    }

    return data;
}

}
