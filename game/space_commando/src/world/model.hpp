/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#ifndef LD_WORLD_MODEL_HPP
#define LD_WORLD_MODEL_HPP
namespace World {

/* Faces are: -X, +X, -Y, +Y, -Z, +Z */

struct Model {
    const unsigned char *vertex;
    int vertexcount;
    int scale;
    unsigned char faces[6];
};

extern const Model CUBE;

}
#endif
