/* Copyright 2014 Dietrich Epp.
   This file is part of Space Commando.  Space Commando is licensed
   under the terms of the 2-clause BSD license.  For more information,
   see LICENSE.txt. */
#include "world.hpp"
#include <algorithm>
namespace World {

namespace {

bool sec_trace_box(
    const SectionData &d, Trace *tr,
    Vec3 start, Vec3 end, Vec3 size)
{
    const int N = SECTION_SIZE;
    const IBox3 scan_volume { { 0, 0, 0 }, { N, N, N } };
    float epsilon = 0.001f;
    int bpos[3], bdir[3], bend[3];
    float bstart[3], binv[3], btime[3];
    float mintime = 0.0f;
    int a;

    for (a = 0; a < 3; a++) {
        if (end[a] > start[a] + epsilon) {
            bpos[a] = std::max((int) std::ceil(start[a] + size[a]), 0);
            bdir[a] = +1;
            bend[a] = N - 1 + (int) std::ceil(size[a] * 2.0f);
            bstart[a] = start[a] + size[a];
            binv[a] = 1.0f / (end[a] - start[a]);
            btime[a] = (bpos[a] - start[a]) * binv[a];
            if (bpos[a] == 0 && btime[a] >= mintime)
                mintime = btime[a];
            else if (bpos[a] >= N)
                btime[a] = 1.0f;
        } else if (end[a] < start[a] - epsilon) {
            bpos[a] = std::min((int) std::floor(start[a] - size[a]), N) - 1;
            bdir[a] = -1;
            bend[a] = -(int) std::ceil(size[a] * 2.0f);
            bstart[a] = start[a] - size[a] - 1.0f;
            binv[a] = 1.0f / (end[a] - start[a]);
            btime[a] = (bpos[a] - start[a]) * binv[a];
            if (bpos[a] == N - 1 && btime[a] >= mintime)
                mintime = btime[a];
            else if (bpos[a] < 0)
                btime[a] = 1.0f;
        } else {
            bdir[a] = 0;
            btime[a] = 1.0f;
        }
    }

    if (mintime > 0.0f) {
        if (mintime >= tr->fraction)
            return false;
        for (a = 0; a < 3; a++) {
            float dt = mintime - btime[a];
            if (dt < epsilon)
                continue;
            float dx = dt * (end[a] - start[a]);
            if (bdir[a] > 0) {
                bpos[a] += (int) std::ceil(dx - epsilon);
                if (bpos[a] >= bend[a])
                    bpos[a] = bend[a];
            } else {
                bpos[a] += (int) std::floor(dx + epsilon);
                if (bpos[a] <= bend[a])
                    bpos[a] = bend[a];
            }
            if (bpos[a] == bend[a])
                btime[a] = 1.0f;
            else
                btime[a] = ((float) bpos[a] - bstart[a]) * binv[a];
        }
    }

    while (true) {
        a = -1;
        float time = tr->fraction;
        for (int i = 0; i < 3; i++) {
            if (btime[i] < time) {
                time = btime[i];
                a = i;
            }
        }
        if (a < 0)
            return false;

        Vec3 tpos = Vec3::lerp(start, end, time);
        IBox3 area;
        for (int i = 0; i < 3; i++) {
            area.mins[i] = (int) std::floor(tpos[i] - size[i]);
            area.maxs[i] = (int) std::ceil(tpos[i] - size[i]) + 1;
        }
        area.mins[a] = bpos[a];
        area.maxs[a] = bpos[a] + 1;
        area = IBox3::intersection(area, scan_volume);
        IVec3 p;
        for (p[2] = area.mins[2]; p[2] < area.maxs[2]; p[2]++) {
            for (p[1] = area.mins[1]; p[1] < area.maxs[1]; p[1]++) {
                for (p[0] = area.mins[0]; p[0] < area.maxs[0]; p[0]++) {
                    int block = d.ref(p);
                    if (!block)
                        continue;
                    tr->fraction = time;
                    tr->end = tpos;
                    return true;
                }
            }
        }

        bpos[a] += bdir[a];
        if (bpos[a] == bend[a])
            btime[a] = 1.0f;
        else
            btime[a] = ((float) bpos[a] - bstart[a]) * binv[a];
    }
}

}

void World::trace_box(Trace *tr, Vec3 start, Vec3 end, Vec3 size) const {
    tr->fraction = 1.0f;
    IBox3 area(m_bounds);
    IVec3 p;
    for (p[2] = area.mins[2]; p[2] < area.maxs[2]; p[2]++) {
        for (p[1] = area.mins[1]; p[1] < area.maxs[1]; p[1]++) {
            for (p[0] = area.mins[0]; p[0] < area.maxs[0]; p[0]++) {
                Vec3 pos;
                for (int i = 0; i < 3; i++)
                    pos[i] = (float) (p[i] * SECTION_SIZE);
                bool r = sec_trace_box(
                    m_sectiondata[m_sectionindex[index3(area, p)]],
                    tr, start - pos, end - pos, size);
                if (r)
                    tr->end += pos;
            }
        }
    }
}

}
