#include "actor.hpp"
#include "defs.hpp"

namespace Actor {
Location::Location()
    : pos_(0, 0), curtime_(0), speed_(0), moving_(false)
{
}

void Location::setPos(Vec2 pos)
{
    pos_ = pos;
    path_.clear();
    curtime_ = 0;
    moving_ = false;
}

void Location::setPath(const std::vector<Vec2> &path)
{
    path_ = path;
}

void Location::setPath(std::vector<Vec2> &&path)
{
    path_ = std::move(path);
}

void Location::update()
{
    curtime_ -= FRAMETICKS;
    if (curtime_ < 0) {
        if (!path_.empty()) {
            Vec2 newPos = path_.back();
            path_.pop_back();
            Vec2 delta = newPos - pos_;
            int time = (delta.x != 0 && delta.y != 0) ?
                speed_ + (speed_ >> 1) : speed_;
            prevpos_[1] = prevpos_[0];
            prevpos_[0] = pos_;
            pos_ = newPos;
            prevtime_[1] = prevtime_[0];
            prevtime_[0] = time;
            curtime_ += time;
        } else {
            curtime_ = 0;
            moving_ = false;
        }
    }


}

Vec2 Location::animPos(int delta) const
{
    if (curtime_ == 0)
        return pos_ * TILESIZE;
    int t = curtime_ - delta, denom;
    Vec2 a, b;
    if (t <= prevtime_[0]) {
        denom = prevtime_[0];
        a = pos_;
        b = prevpos_[0];
    } else {
        t -= prevtime_[0];
        denom = prevtime_[1];
        a = prevpos_[0];
        b = prevpos_[1];
    }
    a *= TILESIZE;
    b *= TILESIZE;
    if (t > denom)
        t = denom;
    if (denom == 0)
        return a;
    return Vec2::lerp(a, b, (double) t / denom);
}

}
