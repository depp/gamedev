/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "opengl.hpp"
#include "shader.hpp"

namespace Test {

void testShader(int width, int height)
{
    (void) width;
    (void) height;

    // Load triangle buffer
    static const short DATA[] = {
        -1, -1,
        +1, -1,
        0, +1
    };
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(DATA), DATA, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    Main::checkGLError(HERE);

    Shader::Program<Shader::Plain> progPlain("plain", "plain");
    float matrix[4] = { 0.5f, 0.0f, 0.0f, 0.5f };
    float offset[2] = { 0.125f, 0.0f };
    float color[4] = { 0.5f, 0.1f, 0.0f, 1.0f };

    glClearColor(0.5f, 0.6f, 0.7f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(progPlain.prog());
    glEnableVertexAttribArray(progPlain->a_vert);

    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glVertexAttribPointer(progPlain->a_vert, 2, GL_SHORT, GL_FALSE, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glUniformMatrix2fv(progPlain->u_vertmat, 1, GL_FALSE, matrix);
    glUniform2fv(progPlain->u_vertoff, 1, offset);
    glUniform4fv(progPlain->u_color, 1, color);

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(progPlain->a_vert);
    glUseProgram(0);

    Main::checkGLError(HERE);

    Main::swapWindow();
    Main::delay(1000);
}

}
