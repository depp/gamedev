/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef ONECRAFT_FILE_HPP
#define ONECRAFT_FILE_HPP

#include <cstddef>
#include <cstdlib>
#include <string>

class Data {
private:
    void *ptr_;
    std::size_t size_;

public:
    Data();
    Data(const Data &other) = delete;
    Data(Data &&other);
    ~Data();
    Data &operator=(const Data &other) = delete;
    Data &operator=(Data &&other);

    const void *ptr() const { return ptr_; }
    std::size_t size() const { return size_; }

    static Data read(const std::string &path);
};

inline Data::Data()
    : ptr_(nullptr), size_(0)
{
}

inline Data::Data(Data &&other)
    : ptr_(nullptr), size_(0)
{
    ptr_ = other.ptr_;
    size_ = other.size_;
    other.ptr_ = nullptr;
    other.size_ = 0;
}

inline Data::~Data()
{
    std::free(ptr_);
}

inline Data &Data::operator=(Data &&other)
{
    if (this == &other)
        return *this;
    std::free(ptr_);
    ptr_ = other.ptr_;
    size_ = other.size_;
    other.ptr_ = nullptr;
    other.size_ = 0;
    return *this;
}

#endif
