/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef ONECRAFT_SURFACE_HPP
#define ONECRAFT_SURFACE_HPP

#include <SDL.h>

struct Surface {
    SDL_Surface *surface;

    Surface();
    Surface(const Surface &other) = delete;
    Surface(Surface &&other);
    ~Surface();
    Surface &operator=(const Surface &other) = delete;
    Surface &operator=(Surface &&other);
};

Surface::Surface()
    : surface(nullptr)
{
}

Surface::Surface(Surface &&other)
    : surface(nullptr)
{
    surface = other.surface;
    other.surface = nullptr;
}

Surface::~Surface()
{
    if (surface)
        SDL_FreeSurface(surface);
}

Surface &Surface::operator=(Surface &&other)
{
    if (this == &other)
        return *this;
    if (surface)
        SDL_FreeSurface(surface);
    surface = other.surface;
    other.surface = nullptr;
    return *this;
}

#endif
