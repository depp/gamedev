/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef ONECRAFT_ARRAY_HPP
#define ONECRAFT_ARRAY_HPP

#include "opengl.hpp"
#include <limits>
#include <new>
#include <cstdlib>

namespace Array {

// OpenGL attribute array data types.
template<typename T>
struct ArrayType { };

template<>
struct ArrayType<short> {
    static const GLenum TYPE = GL_SHORT;
    static const int SIZE = 1;
};

template<>
struct ArrayType<short[2]> {
    static const GLenum TYPE = GL_SHORT;
    static const int SIZE = 2;
};

template<>
struct ArrayType<short[3]> {
    static const GLenum TYPE = GL_SHORT;
    static const int SIZE = 3;
};

template<>
struct ArrayType<short[4]> {
    static const GLenum TYPE = GL_SHORT;
    static const int SIZE = 4;
};

template<>
struct ArrayType<float> {
    static const GLenum TYPE = GL_FLOAT;
    static const int SIZE = 1;
};

template<>
struct ArrayType<float[2]> {
    static const GLenum TYPE = GL_FLOAT;
    static const int SIZE = 2;
};

template<>
struct ArrayType<float[3]> {
    static const GLenum TYPE = GL_FLOAT;
    static const int SIZE = 3;
};

template<>
struct ArrayType<float[4]> {
    static const GLenum TYPE = GL_FLOAT;
    static const int SIZE = 4;
};

// OpenGL attribute array class.
template<class T>
class Array {
private:
    T *data_;
    int count_;
    int alloc_;
    GLuint buffer_;

public:
    explicit Array();
    Array(const Array &other) = delete;
    Array(Array &&other);
    ~Array();
    Array &operator=(const Array &other) = delete;
    Array &operator=(Array &&other);

    // Gets the number of elements in the array.
    int size() const;
    // Set the number of elements in the array to zero.
    void clear();
    // Reserve space for the given total number of elements.
    void reserve(std::size_t total);
    // Insert the given number of elements and return a pointer to the first.
    T *insert(std::size_t count);
    // Upload the array to an OpenGL buffer.
    void upload(GLenum usage);
    // Set the array as a vertex attribute.
    void setAttrib(GLint attrib);
};

template<class T>
inline Array<T>::Array()
    : data_(nullptr), count_(0), alloc_(0), buffer_(0)
{
}

template<class T>
inline Array<T>::Array(Array<T> &&other)
    : data_(nullptr), count_(0), alloc_(0), buffer_(0)
{
    data_ = other.data_;
    count_ = other.count_;
    alloc_ = other.alloc_;
    buffer_ = other.buffer_;
    other.data_ = nullptr;
    other.count_ = 0;
    other.alloc_ = 0;
    other.buffer_ = 0;
}

template<class T>
inline Array<T>::~Array()
{
    std::free(data_);
    glDeleteBuffers(1, &buffer_);
}

template<class T>
Array<T> &Array<T>::operator=(Array<T> &&other)
{
    if (this == &other)
        return *this;
    std::free(data_);
    data_ = other.data_;
    count_ = other.count_;
    alloc_ = other.alloc_;
    buffer_ = other.buffer_;
    other.data_ = nullptr;
    other.count_ = 0;
    other.alloc_ = 0;
    other.buffer_ = 0;
    return *this;
}

template<class T>
int Array<T>::size() const
{
    return count_;
}

template<class T>
void Array<T>::clear()
{
    count_ = 0;
}

template<class T>
void Array<T>::reserve(std::size_t total)
{
    std::size_t rounded;
    if (total > std::numeric_limits<int>::max())
        throw std::bad_alloc();
    if (alloc_ >= total)
        return;
    rounded = total - 1;
    rounded |= rounded >> 1;
    rounded |= rounded >> 2;
    rounded |= rounded >> 4;
    rounded |= rounded >> 8;
    rounded |= rounded >> 16;
    rounded += 1;
    T *newdata = static_cast<T *>(std::realloc(data_, sizeof(T) * rounded));
    if (!newdata)
        throw std::bad_alloc();
    data_ = newdata;
    alloc_ = rounded;
}

template<class T>
T *Array<T>::insert(std::size_t count)
{
    if (count > alloc_ - count_) {
        if (count > std::numeric_limits<int>::max() - count_)
            throw std::bad_alloc();
        reserve(count + count_);
    }
    int pos = count_;
    count_ += count;
    return data_ + pos;
}

template<class T>
void Array<T>::upload(GLenum usage)
{
    if (buffer_ == 0)
        glGenBuffers(1, &buffer_);
    glBindBuffer(GL_ARRAY_BUFFER, buffer_);
    glBufferData(GL_ARRAY_BUFFER, count_ * sizeof(T), data_, usage);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

template<class T>
void Array<T>::setAttrib(GLint attrib)
{
    glBindBuffer(GL_ARRAY_BUFFER, buffer_);
    glVertexAttribPointer(
        attrib, ArrayType<T>::SIZE, ArrayType<T>::TYPE,
        GL_FALSE, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

}

#endif
