/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef ONECRAFT_PACK_HPP
#define ONECRAFT_PACK_HPP

#include <stddef.h>
#include <vector>
namespace Pack {

struct Size {
    int width;
    int height;
};

struct Location {
    int x;
    int y;
};

struct Packing {
    Size size;
    std::vector<Location> locations;
};

Packing pack(const std::vector<Size> &rects);

}

#endif
