/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef ONECRAFT_DEFS_HPP
#define ONECRAFT_DEFS_HPP

#define HERE __FILE__, __LINE__

namespace Main {

__attribute__((noreturn))
void fail(const char *reason);

void checkSDLError(const char *file, int line);

void failSDL(const char *file, int line, const char *msg);

void checkGLError(const char *file, int line);

void delay(int msec);

void swapWindow();

}

const int FRAMEBITS = 7;
const int FRAMETICKS = 1 << FRAMEBITS;
const int TILESIZE = 32;

#endif
