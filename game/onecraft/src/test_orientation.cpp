/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#include "sprite.hpp"
#include <cstdlib>
#include <cstdio>

using Sprite::Orientation;

static void test(Orientation x, Orientation y, Orientation expected)
{
    auto result = x * y;
    if (result != expected) {
        std::fprintf(
            stderr, "FAIL! %d * %d = %d (expected %d)\n",
            static_cast<int>(x),
            static_cast<int>(y),
            static_cast<int>(result),
            static_cast<int>(expected));
        std::exit(1);
    }
}

int main(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    for (int i = 0; i < 8; i++) {
        auto x = static_cast<Orientation>(i);
        test(Orientation::Normal, x, x);
        test(x, Orientation::Normal, x);

        auto y = static_cast<Orientation>(i ^ 4);
        test(Orientation::FlipVert, x, y);
    }

    test(Orientation::FlipHoriz,
         Orientation::FlipVert,
         Orientation::Rotate180);

    return 0;
}
