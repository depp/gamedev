/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "file.hpp"
#include "shader.hpp"
#include <cstdio>
#include <stdexcept>
#include <assert.h>

namespace Shader {

GLuint loadShader(const std::string &name, GLenum type)
{
    std::string path("shader/");
    path += name;
    switch (type) {
    case GL_VERTEX_SHADER: path += ".vert.glsl"; break;
    case GL_FRAGMENT_SHADER: path += ".frag.glsl"; break;
    default: assert(0);
    }

    Main::checkGLError(HERE);

    Data data = Data::read(path);

    const char *darr[1];
    GLint larr[1];

    GLuint shader = glCreateShader(type);
    darr[0] = static_cast<const char *>(data.ptr());
    larr[0] = data.size();
    glShaderSource(shader, 1, darr, larr);
    glCompileShader(shader);
    GLint flag;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &flag);
    if (flag) {
        Main::checkGLError(HERE);
        return shader;
    }

    GLint loglen;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &loglen);
    std::fprintf(stderr, "%s: compilation failed\n", path.c_str());
    if (loglen > 0) {
        char *log = new char[loglen];
        glGetShaderInfoLog(shader, loglen, NULL, log);
        std::fputs(log, stderr);
        std::fputc('\n', stderr);
        delete[] log;
    }
    glDeleteShader(shader);
    throw std::runtime_error("load_shader");
}

void linkProgram(GLuint prog, std::string &name)
{
    GLint flag, loglen;
    char *log;

    glLinkProgram(prog);
    glGetProgramiv(prog, GL_LINK_STATUS, &flag);
    if (flag)
        return;

    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &loglen);
    std::fprintf(stderr, "%s: linking failed", name.c_str());
    if (loglen > 0) {
        log = new char[loglen];
        glGetProgramInfoLog(prog, loglen, NULL, log);
        std::fputs(log, stderr);
        std::fputc('\n', stderr);
        delete[] log;
    }
    throw std::runtime_error("link_program");
}

void getUniforms(GLuint prog, void *object, const Field *uniforms)
{
    for (int i = 0; uniforms[i].name; i++) {
        GLint *ptr = reinterpret_cast<GLint *>(
            static_cast<char *>(object) + uniforms[i].offset);
        *ptr = glGetUniformLocation(prog, uniforms[i].name);
    }
}

void getAttributes(GLuint prog, void *object, const Field *attributes)
{
    for (int i = 0; attributes[i].name; i++) {
        GLint *ptr = reinterpret_cast<GLint *>(
            static_cast<char *>(object) + attributes[i].offset);
        *ptr = glGetAttribLocation(prog, attributes[i].name);
    }
}

GLuint loadProgram(const std::string &vertexshader,
                   const std::string &fragmentshader,
                   const Field *uniforms,
                   const Field *attributes,
                   void *object)
{
    std::string name = vertexshader + ", " + fragmentshader;
    GLuint vertex = loadShader(vertexshader, GL_VERTEX_SHADER);
    GLuint fragment = loadShader(fragmentshader, GL_FRAGMENT_SHADER);
    GLuint prog = glCreateProgram();
    glAttachShader(prog, vertex);
    glAttachShader(prog, fragment);
    glDeleteShader(vertex);
    glDeleteShader(fragment);
    linkProgram(prog, name);
    getUniforms(prog, object, uniforms);
    getAttributes(prog, object, attributes);
    return prog;
}

#define FIELD(n) { #n, offsetof(TYPE, n) }

#define TYPE Plain
const Field Plain::UNIFORMS[] = {
    FIELD(u_vertmat),
    FIELD(u_vertoff),
    FIELD(u_color),
    { nullptr, 0 }
};

const Field Plain::ATTRIBUTES[] = {
    FIELD(a_vert),
    { nullptr, 0 }
};
#undef TYPE

#define TYPE Sprite
const Field Sprite::UNIFORMS[] = {
    FIELD(u_vertoff),
    FIELD(u_vertscale),
    FIELD(u_texscale),
    FIELD(u_texture),
    { nullptr, 0 }
};

const Field Sprite::ATTRIBUTES[] = {
    FIELD(a_vert),
    { nullptr, 0 }
};
#undef TYPE

}
