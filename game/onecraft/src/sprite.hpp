/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef ONECRAFT_SPRITE_HPP
#define ONECRAFT_SPRITE_HPP

#include "array.hpp"
#include "opengl.hpp"
#include <array>
#include <cstdlib>
#include <vector>
#include <string>

namespace Sprite {

enum class Orientation {
    Normal, Rotate90, Rotate180, Rotate270,
    FlipVert, Transpose2, FlipHoriz, Transpose
};

// Compose two orientations.
inline Orientation operator*(Orientation x, Orientation y)
{
    int xi = static_cast<int>(x), yi = static_cast<int>(y);
    int xm = (xi >> 2) & 1, ym = (yi >> 2) & 1;
    int xr = (xi & 3), yr = (yi & 3);
    if (ym)
        xr *= 3;
    return static_cast<Orientation>(((xm ^ ym) << 2) | ((xr + yr) & 3));
}

struct Rect {
    short x, y, w, h;
};

struct Sprite {
    const char *name;
    short x, y, w, h;
};

// Sprite sheet.
class Sheet {
private:
    std::vector<Rect> sprites_;
    GLuint texture_;
    int width_, height_;
    float texscale_[2];

public:
    Sheet();
    Sheet(const std::string &dirname, const Sprite *sprites);
    Sheet(const Sheet &other) = delete;
    Sheet(Sheet &&other);
    ~Sheet();
    Sheet &operator=(const Sheet &other) = delete;
    Sheet &operator=(Sheet &&other);

    GLuint texture() const { return texture_; }
    int width() const { return width_; }
    int height() const { return height_; }
    const float *texscale() const { return texscale_; }
    Rect get(int index) const { return sprites_.at(index); }
};

// Array of sprite rectangles with texture coordinates.
// Draw with GL_TRIANGLES.
class Array {
private:
    ::Array::Array<short[4]> array_;

public:
    Array();
    Array(const Array &other) = delete;
    Array(Array &&other);
    ~Array();
    Array &operator=(const Array &other) = delete;
    Array &operator=(Array &&other) = delete;

    // Add a sprite (tex) at the given lower-left coordinate.
    void add(Rect tex, int x, int y);
    // Add a sprite (tex) at the given lower-left coordinate.
    void add(Rect tex, int x, int y, Orientation orientation);
    // Upload the array data.
    void upload(GLuint usage);
    // Bind the OpenGL attribute.
    void setAttrib(GLint attrib);
    // Get the number of vertexes.
    int size() const { return array_.size(); }
};

}

#endif
