#include "actor.hpp"
#include "defs.hpp"

namespace Actor {

void UnitLocation::update()
{
    curtime_ -= FRAMETICKS;
    if (curtime_ < 0) {
        if (!path_.empty()) {
            Vec2 newPos = path_.pop_back();
            
        }
    }
}

Vec2 UnitLocation::animPos(int delta) const
{
    if (curtime_ == 0)
        return pos_ * TILESIZE;
    int t = curtime_ - delta, denom;
    Vec2 a, b;
    if (t <= prevtime_[0]) {
        denom = prevtime_[0];
        a = pos_;
        b = prevpos_[0];
    } else {
        t -= prevtime_[0];
        denom = prevtime_[1];
        a = prevpos_[0];
        b = prevpos_[1];
    }
    a *= TILESIZE;
    b *= TILESIZE;
    if (t > denom)
        t = denom;
    if (denom == 0)
        return a;
    return Vec2::lerp(a, b, (double) t / denom);
}

void Unit::update()
{
    animtime_ += FRAMETICKS;
    if (pathtime_ > 0) {
        assert(!path_.empty());
        pathcur_ += FRAMETICKS;
        while (pathcur_ > pathtime_) {
            
        }
    }
    if (!path_.empty()) {
        
    }
}

Vec2 Unit::animpos(int delta)
{
    
}
