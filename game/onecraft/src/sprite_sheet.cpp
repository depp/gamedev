/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "sprite.hpp"
#include "opengl.hpp"
#include "pack.hpp"
#include "surface.hpp"
#include <unordered_map>
#include <cstdio>
#include <string>
#include <SDL.h>
#include <SDL_image.h>

namespace Sprite {

Sheet::Sheet()
    : sprites_(), texture_(0), width_(0), height_(0),
      texscale_{0.0f, 0.0f}
{
}

Sheet::Sheet(const std::string &dirname, const Sprite *sprites)
    : sprites_(), texture_(0), width_(0), height_(0),
      texscale_{0.0f, 0.0f}
{
    std::vector<Surface> images;
    std::vector<std::size_t> spriteImages;
    std::size_t count = 0;

    {
        std::string dirpath("data/");
        dirpath += dirname;
        dirpath += '/';
        std::unordered_map<std::string, std::size_t> imageNames;
        for (int i = 0; sprites[i].name; i++) {
            count++;
            std::string name(sprites[i].name);
            auto x = imageNames.insert(
                std::unordered_map<std::string, std::size_t>::value_type(
                    name, images.size()));
            if (x.second) {
                std::string path = dirpath;
                path += name;
                path += ".png";
                Surface image;
                image.surface = IMG_Load(path.c_str());
                if (!image.surface) {
                    Main::checkSDLError(HERE);
                    std::fprintf(stderr, "Error: failed to load image: %s\n",
                                 path.c_str());
                    Main::fail("Failed to load image");
                }
                if (0) {
                    std::fprintf(
                        stderr, "Loaded %s (%s)\n",
                        path.c_str(),
                        SDL_GetPixelFormatName(
                            image.surface->format->format));
                }
                images.push_back(std::move(image));
            }
            spriteImages.push_back(x.first->second);
        }
    }

    std::vector<Pack::Size> imageSizes;
    imageSizes.reserve(images.size());
    for (auto &image : images) {
        Pack::Size sz = { image.surface->w, image.surface->h };
        imageSizes.push_back(sz);
    }

    Pack::Packing packing = Pack::pack(imageSizes);

    std::fprintf(stderr, "Packing %zu sprites into a %dx%d sheet\n",
                 count, packing.size.width, packing.size.height);

    sprites_.reserve(count);
    for (std::size_t i = 0; i < count; i++) {
        auto loc = packing.locations[spriteImages[i]];
        Rect rect;
        rect.x = sprites[i].x + loc.x;
        rect.y = sprites[i].y + loc.y;
        rect.w = sprites[i].w;
        rect.h = sprites[i].h;
        sprites_.push_back(rect);
    }

    width_ = packing.size.width;
    height_ = packing.size.height;
    texscale_[0] = 1.0 / width_;
    texscale_[1] = 1.0 / height_;

    Surface surface;
    surface.surface = SDL_CreateRGBSurface(
        0, packing.size.width, packing.size.height, 32,
        0x00ff0000u, 0x0000ff00u, 0x000000ffu, 0xff000000u);
    if (!surface.surface) Main::failSDL(HERE, "Failed to pack sprites");

    int r;

    SDL_Surface *main = surface.surface;
    for (std::size_t i = 0; i < images.size(); i++) {
        SDL_Surface *surf = images[i].surface;
        r = SDL_SetSurfaceBlendMode(surf, SDL_BLENDMODE_BLEND);
        if (r) Main::failSDL(HERE, "Failed to pack sprites");

        SDL_Rect rect;
        rect.x = packing.locations[i].x;
        rect.y = packing.locations[i].y;
        rect.w = surf->w;
        rect.h = surf->h;

        r = SDL_BlitSurface(surf, nullptr, main, &rect);
        if (r) Main::failSDL(HERE, "Failed to pack sprites");
    }

    r = SDL_LockSurface(main);
    if (r) Main::failSDL(HERE, "Failed to pack sprites");

    glGenTextures(1, &texture_);
    glBindTexture(GL_TEXTURE_2D, texture_);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RGBA8,
        packing.size.width,
        packing.size.height,
        0,
        GL_BGRA,
        GL_UNSIGNED_INT_8_8_8_8_REV,
        main->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);
}

Sheet::Sheet(Sheet &&other)
    : sprites_(std::move(other.sprites_)), texture_(other.texture_),
      width_(other.width_), height_(other.height_)
{
    other.sprites_.clear();
    other.texture_ = 0;
    other.width_ = 0;
    other.height_ = 0;
    other.texscale_[0] = 0.0f;
    other.texscale_[1] = 0.0f;
}

Sheet &Sheet::operator=(Sheet &&other)
{
    if (this == &other)
        return *this;
    if (texture_ != 0)
        glDeleteTextures(1, &texture_);

    sprites_ = std::move(other.sprites_);
    texture_ = other.texture_;
    width_ = other.width_;
    height_ = other.height_;
    texscale_[0] = other.texscale_[0];
    texscale_[1] = other.texscale_[1];
    other.sprites_.clear();
    other.texture_ = 0;
    other.width_ = 0;
    other.height_ = 0;
    other.texscale_[0] = 0.0f;
    other.texscale_[1] = 0.0f;

    return *this;
}

Sheet::~Sheet()
{
    if (texture_ != 0)
        glDeleteTextures(1, &texture_);
}

}
