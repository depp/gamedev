/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "file.hpp"
#include <cstdlib>
#include <cstdio>
#include <new>

Data Data::read(const std::string &path)
{
    std::string fullpath("data/");
    fullpath += path;
    FILE *fp = std::fopen(fullpath.c_str(), "rb");
    if (!fp) {
        std::fprintf(stderr, "Could not open file: %s\n", path.c_str());
        Main::fail("Could not load file.");
    }
    try {
        static const std::size_t INITSZ = 4096;
        Data data;
        std::size_t alloc = INITSZ;
        data.ptr_ = std::malloc(alloc);
        if (!data.ptr_)
            throw std::bad_alloc();
        std::size_t pos = 0;
        while (1) {
            std::size_t amt = std::fread(
                static_cast<char *>(data.ptr_) + pos,
                1, alloc - pos, fp);
            if (amt == 0) {
                if (std::feof(fp)) {
                    data.size_ = pos;
                    std::fclose(fp);
                    return data;
                }
                std::fprintf(stderr, "Could not read file: %s\n",
                             path.c_str());
                Main::fail("Could not load file.");
            }
            pos += amt;
            if (pos >= alloc) {
                alloc *= 2;
                if (!alloc) {
                    std::fprintf(stderr, "File too large: %s\n",
                                 path.c_str());
                    Main::fail("Could not load file.");
                }
                void *nptr = realloc(data.ptr_, alloc);
                if (!nptr)
                    throw std::bad_alloc();
            }
        }
    } catch (...) {
        std::fclose(fp);
        throw;
    }
}
