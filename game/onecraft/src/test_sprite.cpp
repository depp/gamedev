/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#include "defs.hpp"
#include "shader.hpp"
#include "sprite.hpp"
#include "test.hpp"

namespace Test {

void testSprite(int width, int height)
{
    using Sprite::Orientation;
    Shader::Program<Shader::Sprite> prog("sprite", "spritedummy");
    Sprite::Rect rect = { 0, 0, 128, 64 };
    Sprite::Array arr;

    arr.add(rect, 16 + 144*0, 16 + 144*0);
    arr.add(rect, 16 + 144*0, 16 + 144*1, Orientation::Normal);
    arr.add(rect, 16 + 144*1, 16 + 144*1, Orientation::Rotate90);
    arr.add(rect, 16 + 144*2, 16 + 144*1, Orientation::Rotate180);
    arr.add(rect, 16 + 144*3, 16 + 144*1, Orientation::Rotate270);
    arr.add(rect, 16 + 144*0, 16 + 144*2, Orientation::FlipVert);
    arr.add(rect, 16 + 144*1, 16 + 144*2, Orientation::Transpose2);
    arr.add(rect, 16 + 144*2, 16 + 144*2, Orientation::FlipHoriz);
    arr.add(rect, 16 + 144*3, 16 + 144*2, Orientation::Transpose);

    arr.upload(GL_STATIC_DRAW);

    Main::checkGLError(HERE);

    glClearColor(0.5f, 0.6f, 0.7f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(prog.prog());
    glEnableVertexAttribArray(prog->a_vert);

    glUniform2f(prog->u_vertoff, -1.0f, -1.0f);
    glUniform2f(prog->u_vertscale, 2.0 / width, 2.0 / height);
    glUniform2f(prog->u_texscale, 1.0 / 128, 1.0 / 64);
    arr.setAttrib(prog->a_vert);

    glDrawArrays(GL_TRIANGLES, 0, arr.size());

    glDisableVertexAttribArray(prog->a_vert);
    glUseProgram(0);

    Main::checkGLError(HERE);

    Main::swapWindow();
    Main::delay(10000);
}

}
