#include <vector>
#include "vec2.hpp"
namespace Terrain {

std::size_t size2d(int x, int y)
{
    return static_cast<std::size_t>(x) * static_cast<std::size_t>(y);
}

enum class Tile : unsigned char {
    Water, Dirt, Grass, Rock
};

inline unsigned tileset(std::initializer_list<Tile> tiles)
{
    unsigned mask = 0;
    for (auto tile : tiles)
        mask |= 1u << static_cast<int>(tile);
    return mask;
}

class Terrain {
private:
    std::vector<Tile> tiles_;
    int width_;
    int height_;

    void copyFromUnchecked(const Terrain &other,
                           int srcx, int srcy,
                           int destx, int desty,
                           int width, int height);

public:
    Terrain();
    Terrain(int width, int height);

    int width() const { return width_; }
    int height() const { return height_; }

    Tile get(int x, int y) const;
    void set(int x, int y, Tile tile);

    void copyFrom(const Terrain &other,
                  int srcx, int srcy,
                  int destx, int desty,
                  int width, int height);

    const std::vector<Tile> &tiles() const;
};

inline Terrain::Terrain()
    : width_(0), height_(0)
{
}

inline Tile Terrain::get(int x, int y) const
{
    if (x < 0)
        x = 0;
    else if (x >= width_)
        x = width_;
    if (y < 0)
        y = 0;
    else if (y >= height_)
        y = height_;
    return tiles_[y * width_ + x];
}

inline void Terrain::set(int x, int y, Tile tile)
{
    if (x < 0 || x >= width_ || y < 0 || y >= height_)
        return;
    tiles_[y * width_ + x] = tile;
}

const std::vector<Tile> &Terrain::tiles() const
{
    return tiles_;
}

class PathMap {
private:
    std::vector<unsigned char> passable_;
    int width_;
    int height_;
    bool dirty_;

public:
    explicit PathMap(const Terrain &terrain, unsigned tileset);

    void block(Vec2 loc);
    bool passable(Vec2 loc);
    std::vector<Vec2> findPath(Vec2 source, Vec2 dest);
};

}
