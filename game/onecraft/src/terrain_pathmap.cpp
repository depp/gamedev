
#include "terrain.hpp"
namespace Terrain {

PathMap::PathMap(const Terrain &terrain, unsigned tileset)
    : width_(terrain.width()), height_(terrain.height())
{
    std::size_t size = static_cast<std::size_t>(width_ + 2) *
        (height_ + 2);
    passable_.reserve(size);
    passable_.resize(size, 0);
    const std::vector &tiles = terrain.tiles();
    for (int y = 0; y < height_; y++) {
        for (int x = 0; x < width_; x++) {
            Tile tile = tiles[y * width_ + x];
            bool passable = (mask & (1u << static_cast<int>(tiles[i]))) != 0;
            passable_[(y + 1) * (width_ + 2) + x + 1] = passable ? 1 : 0;
        }
    }
}

void PathMap::block(Vec2 loc)
{
    if (loc.x < 0 || loc.x >= width || loc.y < 0 || loc.y >= height_)
        return;
    nodes_[(loc.y + 1) * width_ + loc.x + 1] -= 2;
}

bool PathMap::passable(Vec2 loc)
{
    if (loc.x < 0 || loc.x >= width || loc.y < 0 || loc.y >= height_)
        return false;
    return passable_[(loc.y + 1) * (width + 2) + loc.x + 1] != 0;
}

namespace {

struct Node {
    // Cost from start (known, or 0 = unknown, -1 = impassible)
    short gcost;
    // Minimum estimated total cost (if gcost is known)
    short fcost;
};

struct Frontier {
    Vec2 location;
    int distance;
    int heuristic;

    Frontier(Vec2 location, int distance, int heuristic);
    bool operator<(const Frontier &other) const;
};

Frontier::Frontier(Vec2 location, int distance, int heuristic)
    : location(location), distance(distance), heuristic(heuristic)
{
}

bool Frontier::operator<(const Frontier &other) const
{
    return heuristic > other.heuristic;
}

int heuristic(Vec2 a, Vec2 b)
{
    int dx = std::abs(a.x - b.x);
    int dy = std::abs(a.y - b.y);
    return dx > dy ? dx * 2 + dy : dx + 2 * dy;
}

struct Index {
    std::size_t width;
    Index(int width);
    std::size_t operator()(Vec2 vec) const;
};

Index::Index(int width)
    : width(width)
{
}

std::size_t Index::operator()(Vec2 vec) const
{
    return width * vec.y + vec.x;
}

const Vec2 NEIGHBORS[] = {
    Vec2(-1,  0),
    Vec2(+1,  0),
    Vec2( 0, -1),
    Vec2( 0, +1),
    Vec2(-1, -1),
    Vec2(+1, -1),
    Vec2(-1, +1),
    Vec2(+1, +1)
};

}

std::vector<Vec2> PathMap::findPath(Vec2 source, Vec2 dest)
{
    std::vector<Vec2> path;

    if (!passable(source) || !passable(dest) || source == dest)
        return path;

    int width = width_ + 2;
    int height = height_ + 2;
    std::size_t size = size2d(width, height);
    source += Vec2(1, 1);
    dest += Vec2(1, 1);

    std::vector<Frontier> frontier;
    std::vector<int> node;

    node.reserve(size);
    node.resize(size);
    for (std::size_t i = 0; i < size; i++) {
        if (passable_[i] != 0)
            node[i] = 0;
        else
            node[i] = -1;
    }

    Index index(width);
    frontier.push_back(Frontier(source, 1, 1 + heruistic(source, dest)));
    while (frontier.size()) {
        Frontier current = frontier.front();
        std::pop_heap(open_.first(), open_.last());
        open_.pop_last();

        auto idx = index(current.location);
        if ((node[idx] & 1) != 0)
            continue;

        node[idx] = (current.distance << 1) | 1;
        for (int i = 0; i < 8; i++) {
            idx = index(current.location + NEIGHBORS[i]);
            if ((node[idx] & 1) != 0)
                continue;
            int dist = current.distance + ((i < 4) ? 2 : 3);
            int ndist = node[idx] >> 1;
            if (ndist != 0 && ndist < dist)
                continue;
            

        }
        nodes_[(current.loc.y + 1) * (width_ + 1) + current.loc.x + 1]
             = 
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; i <= 1; y++) {
                
            }
        }
    }
}

}
