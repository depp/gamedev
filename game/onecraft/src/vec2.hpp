#include <cmath>

struct Vec2 {
    int x;
    int y;

    Vec2() = default;
    Vec2(int x, int y);

    bool operator==(Vec2 const &other) const;
    bool operator!=(Vec2 const &other) const;
    Vec2 operator*(int scale) const;
    Vec2 &operator*=(int scale);
    Vec2 operator+(Vec2 other) const;
    Vec2 &operator+=(Vec2 other);
    Vec2 operator-(Vec2 other) const;
    Vec2 &operator-=(Vec2 other);

    static Vec2 lerp(Vec2 a, Vec2 b, double t);
};

inline Vec2::Vec2(int x, int y)
    : x(x), y(y)
{
}

inline bool Vec2::operator==(Vec2 const &other) const
{
    return x == other.x && y == other.y;
}

inline bool Vec2::operator!=(Vec2 const &other) const
{
    return x != other.x || y != other.y;
}

inline Vec2 Vec2::operator*(int scale) const
{
    return Vec2(x * scale, y * scale);
}

inline Vec2 &Vec2::operator*=(int scale)
{
    x *= scale;
    y *= scale;
    return *this;
}

inline Vec2 Vec2::operator+(Vec2 other) const
{
    return Vec2(x + other.x, y + other.y);
}

inline Vec2 &Vec2::operator+=(Vec2 other)
{
    x += other.x;
    y += other.y;
    return *this;
}

inline Vec2 Vec2::operator-(Vec2 other) const
{
    return Vec2(x - other.x, y - other.y);
}

inline Vec2 &Vec2::operator-=(Vec2 other)
{
    x -= other.x;
    y -= other.y;
    return *this;
}

inline Vec2 Vec2::lerp(Vec2 a, Vec2 b, double t)
{
    return Vec2(a.x + (int) std::floor((b.x - a.x) * t),
                a.y + (int) std::floor((b.y - a.y) * t));
}
