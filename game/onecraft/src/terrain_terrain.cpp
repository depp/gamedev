#include "terrain.hpp"
#include <stdexcept>
namespace Terrain {

Terrain::Terrain(int width, int height)
    : width_(width), height_(height)
{
    std::size_t size = static_cast<std::size_t>(width) *
        static_cast<std::size_t>(height);
    tiles_.reserve(size);
    tiles_.resize(size, Tile::Dirt);
}

void Terrain::copyFrom(const Terrain &other,
                       int srcx, int srcy,
                       int destx, int desty,
                       int width, int height)
{
    if (width < 0 || width > width_ || width > other.width_ ||
        height < 0 || height > height_ || height > other.height_ ||
        srcx < 0 || srcx > other.width_ - width ||
        srcy < 0 || srcy > other.height_ - height ||
        destx < 0 || destx > width_ - width ||
        desty < 0 || desty > height_ - height)
        throw std::invalid_argument(__FUNCTION__);
    if (width == 0 || height == 0)
        return;
    if (this == &other) {
        Terrain tmp(width, height);
        tmp.copyFromUnchecked(*this, srcx, srcy, 0, 0, width, height);
        copyFromUnchecked(tmp, 0, 0, destx, desty, width, height);
    } else {
        copyFromUnchecked(other, srcx, srcy, destx, desty, width, height);
    }
}

void Terrain::copyFromUnchecked(const Terrain &other,
                       int srcx, int srcy,
                       int destx, int desty,
                       int width, int height)
{
    for (int y = 0; y < height; y++)
        for (int x = 0; x < width; x++)
            tiles_[(y + desty) * width_ + x + destx] =
                other.tiles_[(y + srcy) * other.width_ + x + srcx];
}

}
