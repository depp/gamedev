/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef ONECRAFT_TEST_HPP
#define ONECRAFT_TEST_HPP

namespace Test {

void testShader(int width, int height);

void testSprite(int width, int height);

void testSpriteSheet(int width, int height);

};

#endif
