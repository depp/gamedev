#include "vec2.hpp"
#include <vector>

namespace Actor {

enum class State {
    Idle,
    Move
};

enum class Animation {
    Idle,
    Move
};

enum class Team {
    Player, Enemy
};

// The location of a unit as it moves around.
class Location {
private:
    // Current position, or next position if we are between positions.
    Vec2 pos_;

    // Previous two positions
    Vec2 prevpos_[2];

    // Ticks between the corresponding prevpos_ and the next position.
    int prevtime_[2];

    // Path, in reverse order
    std::vector<Vec2> path_;

    // Ticks until we reach pos_.
    int curtime_;

    // Time required to move one tile.
    int speed_;

    // Whether this object is moving.
    bool moving_;

public:
    Location();
    Location(const Location &other) = delete;
    Location(Location &&other) = default;
    Location &operator=(const Location &other) = delete;
    Location &operator=(Location &&other) = default;

    void setPos(Vec2 pos);
    void setPath(const std::vector<Vec2> &path);
    void setPath(std::vector<Vec2> &&path);
    void update();

    Vec2 animPos(int delta) const;
    Vec2 unitPos() const;
    bool moving() const;
};

inline Vec2 Location::unitPos() const
{
    return pos_;
}

bool Location::moving() const
{
    return moving_;
}

};
