/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#ifndef ONECRAFT_SHADER_HPP
#define ONECRAFT_SHADER_HPP
#include "opengl.hpp"
#include <cstddef>
#include <string>

namespace Shader {

struct Field {
    const char *name;
    std::size_t offset;
};

GLuint loadProgram(const std::string &vertexshader,
                   const std::string &fragmentshader,
                   const Field *uniforms,
                   const Field *attributes,
                   void *object);

template<class T>
class Program {
private:
    GLuint prog_;
    T fields_;

public:
    Program(const std::string &vertexshader,
           const std::string &fragmentshader);
    Program(const Program &) = delete;
    ~Program();
    Program &operator =(const Program &) = delete;

    const T *operator->() const { return &fields_; }
    GLuint prog() const { return prog_; }

};

template<class T>
Program<T>::Program(const std::string &vertexshader,
                    const std::string &fragmentshader)
    : prog_(0), fields_()
{
    prog_ = loadProgram(
        vertexshader, fragmentshader,
        T::UNIFORMS, T::ATTRIBUTES,
        &fields_);
}

template<class T>
Program<T>::~Program()
{
    glDeleteProgram(prog_);
}

struct Plain {
    static const Field UNIFORMS[];
    static const Field ATTRIBUTES[];

    GLint a_vert;
    GLint u_vertmat;
    GLint u_vertoff;
    GLint u_color;
};

struct Sprite {
    static const Field UNIFORMS[];
    static const Field ATTRIBUTES[];

    GLint a_vert;
    GLint u_vertoff;
    GLint u_vertscale;
    GLint u_texscale;
    GLint u_texture;
};

}

#endif
