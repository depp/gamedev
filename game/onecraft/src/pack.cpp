/* Copyright 2013 Dietrich Epp.
   This file is part of Onecraft II.  Onecraft II is licensed under the terms
   of the 2-clause BSD license.  For more information, see LICENSE.txt. */
#include <algorithm>
#include <cstdlib>
#include <vector>
#include <assert.h>
#include "defs.hpp"
#include "pack.hpp"
namespace Pack {

// Rect, sorts from biggest to smallest.
struct Rect {
    Size size;
    std::size_t index;

    bool operator<(const Rect &other) const;
};

bool Rect::operator<(const Rect &other) const
{
    if (size.width > other.size.width)
        return true;
    if (size.width < other.size.width)
        return false;
    return size.height > other.size.width;
}

// Metrics for how good a packing is.
struct Metrics {
    int area;
    int nonsquare;

    Metrics();
    Metrics(const Size &size);
    bool operator<(const Metrics &other) const;
};

Metrics::Metrics()
{
}

Metrics::Metrics(const Size &size)
    : area(size.width * size.height),
      nonsquare(std::abs(size.width - size.height))
{
}

bool Metrics::operator<(const Metrics &other) const
{
    if (area < other.area)
        return true;
    if (area > other.area)
        return false;
    return nonsquare < other.nonsquare;
}

// Packing state.
struct Packer {
    std::vector<int> frontier;
    Packing packing;
    std::vector<Rect> rects;

    bool tryPack(int width, int height);
};

bool Packer::tryPack(int width, int height)
{
    frontier.clear();
    frontier.resize(height, 0);
    packing.locations.resize(rects.size());
    packing.size.width = width;
    packing.size.height = height;
    int ypos = 0;
    for (auto rect : rects) {
        int sw = rect.size.width, sh = rect.size.height;
        std::size_t index = rect.index;
        if (ypos + sw > height)
            ypos = 0;
        while (ypos + sh <= height && frontier[ypos] + sw > width)
            ypos++;
        if (ypos + sh > height)
            return false;
        int xpos = frontier[ypos];
        for (int i = 0; i < ypos + sh; i++) {
            if (frontier[i] < xpos + sw)
                frontier[i] = xpos + sw;
        }
        assert(0 <= ypos && ypos <= height - sh);
        assert(0 <= xpos && xpos <= width - sw);
        packing.locations[index].x = xpos;
        packing.locations[index].y = ypos;
        ypos += sh;
    }
    return true;
}

Packing pack(const std::vector<Size> &rects)
{
    Packer packer;
    std::size_t rectarea = 0;
    packer.rects.reserve(rects.size());
    for (std::size_t i = 0; i < rects.size(); i++) {
        Rect r;
        r.size = rects[i];
        r.index = i;
        packer.rects.push_back(r);
        rectarea += static_cast<std::size_t>(rects[i].width) *
            static_cast<std::size_t>(rects[i].height);
    }
    std::sort(packer.rects.begin(), packer.rects.end());

    bool havePacking = false;
    Packing bestPacking;
    for (int i = 4; i < 10; i++) {
        int height = 1 << i;
        bool success = false;
        for (int j = 4; j < 10; j++) {
            int width = 1 << j;
            if (rectarea > static_cast<std::size_t>(1) << (i + j))
                continue;
            if (packer.tryPack(width, height)) {
                success = true;
                break;
            }
        }
        if (!success)
            continue;
        if (havePacking) {
            if (Metrics(packer.packing.size) < Metrics(bestPacking.size)) {
                Packing temp(std::move(bestPacking));
                bestPacking = std::move(packer.packing);
                packer.packing = std::move(temp);
            }
        } else {
            bestPacking = std::move(packer.packing);
            havePacking = true;
        }
    }
    if (!havePacking)
        Main::fail("Could not pack sprites.");
    return bestPacking;
}

}
