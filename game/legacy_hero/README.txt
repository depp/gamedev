Cruel Legacy
============

This is my entry for Ludum Dare 25, "You are the Villain".

Gameplay idea: it's a platformer.  You destroy each level in some way:
for example, you might flood the level with water.  Your goal is to
try and make it impossible for the heroes to win against you, so you
have to make the levels impossible to beat.

It's framed in a narrative device where you are recounting the story
to someone over the phone.  You're telling them how you saved the
world by destroying the dungeons containing the crystals of power.
The "villain" was going to use the crystals to take over the world,
reputedly, and by collapsing the dungeons you were saving the world.
Nobody would ever be threatened again by the crystals of power, or so
you said.  In reality, destroying the dungeons allowed you to extract
the crystals' power.  Each level you play in the flashbacks, you get
some more magical power.  In the final cutscene, you finish your phone
call and go to your dark throne, where you instruct your lieutenant to
"begin".

Each level is a platformer.  You destroy the level, unleashing some
particle effects everywhere.  You have a limited number of magic
spells you can cast.  Each level, you get more powerful -- you absorb
the energy released in the level's destruction.
