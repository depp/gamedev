#!/bin/sh
if test -z "$PYTHON" ; then
    PYTHON=`which python3`
fi
if test -z "$PYTHON" ; then
    echo 'error: could not find Python 3 executable' 1>&2
    exit 1
fi
exec "$PYTHON" sglib/scripts/init.py config "$@"
