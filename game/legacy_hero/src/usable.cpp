/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "usable.hpp"
namespace LD25 {

Usable::~Usable()
{ }

void Usable::update(State &st)
{
    (void) &st;
}

void Usable::draw(Graphics &gr, int delta)
{
    (void) &gr;
    (void) &delta;
}

}
