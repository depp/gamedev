/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_SHADER_HPP
#define LD25_SHADER_HPP
#include "sg/opengl.h"
namespace LD25 {

/* Compile the shader and return it.  */
GLuint load_shader(GLenum type, const char *path);

/* Link the program.  */
void link_program(GLuint prog, const char *name);

}
#endif
