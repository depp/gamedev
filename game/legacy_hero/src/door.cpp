/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "door.hpp"
#include "state.hpp"
namespace LD25 {

enum {
    DOOR_OPEN = 4*16 + 2,
    DOOR_CLOSED = 6*16 + 2
};

Door::Door(Vec bb0, Vec bb1, std::string target, State &st, bool locked)
    : Usable(bb0.tileToWorld(), bb1.tileToWorld()),
      m_target(target), m_locked(locked)
{
    int base = locked ? DOOR_CLOSED : DOOR_OPEN;
    st.background.set(bb0.x, bb0.y, base + 16);
    st.background.set(bb0.x, bb0.y + 1, base);
}

Door::~Door()
{ }

void Door::use(State &st)
{
    st.gotoSector(m_target);
}

}
