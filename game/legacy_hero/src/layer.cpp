/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "layer.hpp"
#include "tagfile.hpp"
#include "libpce/binary.h"
#include "libpce/util.h"
#include <cstring>
#include <cstdlib>
#include <stdexcept>
using namespace LD25;

void Layer::reserve(std::size_t size)
{
    if (size <= m_alloc)
        return;
    std::size_t alloc = size > m_alloc * 2 ?
        size : pce_round_up_pow2(size);
    if (m_data)
        delete[] m_data;
    m_data = NULL;
    m_alloc = 0;
    m_data = new unsigned char[alloc];
    m_alloc = alloc;
}


void Layer::load(int width, int height,
                 const void *data, std::size_t length)
{
    if (!width || width > MAX_SIZE || !height || height > MAX_SIZE)
        throw std::runtime_error("Layer");
    std::size_t size = static_cast<size_t>(width) * height;
    if (size != length)
        throw std::runtime_error("Layer");
    reserve(size);
    std::memcpy(m_data, data, size);
    m_width = width;
    m_height = height;
}

Layer::Layer(const Layer &s)
    : m_data(0), m_width(0), m_height(0), m_alloc(0)
{
    int width = s.m_width, height = s.m_height;
    std::size_t size = static_cast<size_t>(width) * height;
    reserve(size);
    std::memcpy(m_data, s.m_data, size);
    m_width = s.m_width;
    m_height = s.m_height;
}

Layer &Layer::operator=(const Layer &s)
{
    if (this == &s)
        return *this;

    int width = s.m_width, height = s.m_height;
    std::size_t size = static_cast<size_t>(width) * height;
    reserve(size);
    std::memcpy(m_data, s.m_data, size);
    m_width = width;
    m_height = height;

    return *this;
}

Layer::~Layer()
{
    if (m_data)
        delete[] m_data;
}
