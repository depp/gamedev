/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "tagfile.hpp"
#include "libpce/binary.h"
#include <stdexcept>
#include <cstring>
using namespace LD25;

TagFile::TagFile(const void *ptr, std::size_t length)
{
    const char *p = static_cast<const char *>(ptr);
    std::size_t pos = 0;
    while (pos < length) {
        if (length - pos < 8)
            throw std::runtime_error("TagFile");
        Tag t;
        memcpy(t.name, p + pos, 4);
        t.ptr = p + pos + 8;
        t.length = pce_read_bu32(p + pos + 4);
        pos += 8;
        if (length - pos < t.length)
            throw std::runtime_error("TagFile");
        pos += t.length;
        tags.push_back(t);
    }
}

TagFile::Tag TagFile::find(const char *name) const
{
    if (std::strlen(name) != 4)
        throw std::invalid_argument("TagFile::find");
    for (std::vector<Tag>::const_iterator
             i = tags.begin(), e = tags.end();
         i != e; i++)
    {
        if (i->is_named(name))
            return *i;
    }
    Tag t;
    std::memset(t.name, '\0', 4);
    t.ptr = 0;
    t.length = 0;
    return t;
}
