/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_PLAYER_HPP
#define LD25_PLAYER_HPP
#include "biped.hpp"
namespace LD25 {

class Player : public Biped::Controller {
    bool m_use;

public:
    Player();
    virtual ~Player();

    virtual void update(State &st, Biped &bp);
};

}
#endif
