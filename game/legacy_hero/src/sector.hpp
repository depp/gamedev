/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_SECTOR_HPP
#define LD25_SECTOR_HPP
#include <string>
#include "layer.hpp"
#include "proplist.hpp"
#include "vec.hpp"
namespace LD25 {

class Sector {
public:
    class Object {
    public:
        PropList properties;
        std::string name;
        Vec bb0, bb1;

        Object()
        { }

        Object(Object &&obj)
            : properties(std::move(obj.properties)),
              name(std::move(obj.name)),
              bb0(obj.bb0), bb1(obj.bb1)
        { }
    };

    static const int MAX_SIZE = 256;

    std::string name;

    Layer foreground;
    Layer background;

    int width, height;

    std::vector<Object> objects;

    Sector(const void *data, std::size_t length);

    Sector(const Sector &) = delete;
    Sector &operator=(const Sector &) = delete;
};

}
#endif
