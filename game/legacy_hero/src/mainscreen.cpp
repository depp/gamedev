/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "mainscreen.hpp"
#include "biped.hpp"
#include "defs.hpp"
#include "sgpp/viewport.hpp"
#include "sg/audio_source.h"
#include "keycode/keycode.h"
#include "sgpp/ui/event.hpp"
namespace LD25 {

const unsigned char MainScreen::KEY_MAP[] = {
    // KEY_W,      Control::UP,
    KP_8,       Control::UP,
    KEY_Up,     Control::UP,

    // KEY_A,      Control::LEFT,
    KP_4,       Control::LEFT,
    KEY_Left,   Control::LEFT,

    // KEY_S,      Control::DOWN,
    KP_5,       Control::DOWN,
    KEY_Down,   Control::DOWN,

    // KEY_D,      Control::RIGHT,
    KP_6,       Control::RIGHT,
    KEY_Right,  Control::RIGHT,

    KEY_X,      Control::FIRE,
    KP_Point,   Control::FIRE,

    KEY_Z,      Control::JUMP,
    KP_0,       Control::JUMP,

    255
};

MainScreen::MainScreen()
    : m_keys(KEY_MAP)
{ }

MainScreen::~MainScreen()
{ }

void MainScreen::handleEvent(const UI::Event &evt)
{
    switch (evt.type) {
    case UI::KeyDown:
    case UI::KeyUp:
        m_keys.handleKeyEvent(evt.keyEvent());
        break;

    default:
        break;
    }
}

unsigned MainScreen::getControls()
{
    unsigned x = 0;
    for (int i = 0; i < 6; i++)
        if (m_keys.inputState(i))
            x |= 1u << i;
    return x;
}

void MainScreen::update(unsigned msec)
{
    if (!m_init) {
        m_tickref = msec;
        m_init = true;
        m_delta = 0;
    } else {
        unsigned delta = msec - m_tickref;
        if (delta > LAG_THRESHOLD) {
            state.controls.flags = getControls();
            m_tickref = msec;
            m_delta = 0;
            state.advance(msec);
        } else {
            if (delta >= FRAME_TIME) {
                state.controls.flags = getControls();
                unsigned frames = delta / FRAME_TIME;
                unsigned ref = m_tickref;
                for (unsigned i = 0; i < frames; i++)
                    state.advance(ref + (i + 1) * FRAME_TIME);
                m_tickref = ref + FRAME_TIME * frames;
                delta -= FRAME_TIME * frames;
            }
            m_delta = delta;
        }
    }
    sg_audio_source_commit(msec, msec);
}

void MainScreen::draw(Viewport &v, unsigned msec)
{
    if (!graphics)
        graphics = std::unique_ptr<Graphics>(new Graphics);
    Graphics &gr = *graphics;
    gr.setSize(v.width(), v.height());
    gr.clearSprites();
    state.draw(gr, m_delta);
    gr.draw();

    (void) msec;
}

}
