/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_LAYER_HPP
#define LD25_LAYER_HPP
#include <cstddef>
namespace LD25 {
struct TagFile;

class Layer {
    unsigned char *m_data;
    int m_width;
    int m_height;
    std::size_t m_alloc;

    void reserve(std::size_t size);

public:
    static const int MAX_SIZE = 256;

    Layer()
        : m_data(0), m_width(0), m_height(0), m_alloc(0)
    { }

    Layer(Layer &&s)
        : m_data(s.m_data), m_width(s.m_width),
          m_height(s.m_height), m_alloc(s.m_alloc)
    {
        s.m_data = 0;
        s.m_width = 0;
        s.m_height = 0;
        s.m_alloc = 0;
    }

    Layer(const Layer &s);
    ~Layer();

    Layer &operator=(const Layer &s);

    void load(int width, int height,
              const void *data, std::size_t length);

    int get_unchecked(int x, int y) const
    {
        return m_data[y * m_width + x];
    }

    int get(int x, int y) const
    {
        if (x < 0 || x >= m_width || y < 0 || y >= m_height)
            return 0;
        return m_data[y * m_width + x];
    }

    void set(int x, int y, int t)
    {
        if (x < 0 || x >= m_width || y < 0 || y >= m_height)
            return;
        m_data[y * m_width + x] = t;
    }

    int width() const { return m_width; }
    int height() const { return m_height; }
};

}
#endif
