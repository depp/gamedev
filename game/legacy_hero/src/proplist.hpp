/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_PROPLIST_HPP
#define LD25_PROPLIST_HPP
#include <vector>
#include <string>
#include <cstddef>
namespace LD25 {

class PropList {
    struct Property {
        Property(const std::string &name, const std::string &value)
            : name(name), value(value)
        { }

        std::string name;
        std::string value;
    };

    std::vector<Property> m_vec;

public:
    PropList()
    { }

    PropList(PropList &&x)
        : m_vec(std::move(x.m_vec))
    { }

    PropList(const PropList &) = delete;
    PropList &operator=(const PropList &) = delete;

    // Read a property list and return how many bytes read
    std::size_t read(const void *ptr, std::size_t length);

    // Return the property, or an empty string if not present
    std::string get(const std::string &name) const;

    // Return a boolean, defaulting to false
    bool getbool(const std::string &name) const;
};

}
#endif
