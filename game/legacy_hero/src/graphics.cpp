/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "graphics.hpp"
#include "layer.hpp"
#include "prog.hpp"
namespace LD25 {

// ============================================================
// Here be macros...

#define PF_U(x) { ProgField::UNIFORM, offsetof(PF_T, x), #x }
#define PF_A(x) { ProgField::ATTRIBUTE, offsetof(PF_T, x), #x }
#define PF_END { ProgField::END, 0, "" }

#define PF_T Graphics::TileProg
const ProgField Graphics::TileProg::FIELDS[] = {
    PF_U(u_tex), PF_U(u_vertoff), PF_U(u_vertscale), PF_U(u_texscale),
    PF_A(a_loc), PF_A(a_texcoord),
    PF_END
};
#undef PF_T
const char Graphics::TileProg::NAME[] = "tile";

#define PF_T Graphics::SpriteProg
const ProgField Graphics::SpriteProg::FIELDS[] = {
    PF_U(u_tex), PF_U(u_vertoff), PF_U(u_vertscale), PF_U(u_texscale),
    PF_A(a_loc), PF_A(a_texcoord),
    PF_END
};
#undef PF_T
const char Graphics::SpriteProg::NAME[] = "sprite";

#undef PF_U
#undef PF_A
#undef PF_END

// End of macros
// ============================================================

Graphics::Graphics()
    : m_width(1), m_height(1), m_cam_x(0), m_cam_y(0)
{
    GLuint vert, frag;

    // ========================================
    // Tile

    vert = load_shader(GL_VERTEX_SHADER, "tile_vert");
    frag = load_shader(GL_FRAGMENT_SHADER, "tile_frag");
    load_prog(tile.prog, vert, frag);

    tile.tex = Texture::file("gfx/tile");
    glGenVertexArrays(1, &tile.arr);
    glBindVertexArray(tile.arr);
    tile.a_vert.vertexAttrib(tile.prog.a_loc);
    tile.a_tex.vertexAttrib(tile.prog.a_texcoord);
    glBindVertexArray(0);

    glDeleteShader(vert);
    glDeleteShader(frag);

    // ========================================
    // Sprite

    vert = load_shader(GL_VERTEX_SHADER, "sprite_vert");
    frag = load_shader(GL_FRAGMENT_SHADER, "sprite_frag");
    load_prog(sprite.prog, vert, frag);

    sprite.tex = Texture::file("gfx/sprite");
    glGenVertexArrays(1, &sprite.arr);
    glBindVertexArray(sprite.arr);
    sprite.a_vert.vertexAttrib(sprite.prog.a_loc);
    sprite.a_tex.vertexAttrib(sprite.prog.a_texcoord);
    glBindVertexArray(0);

    glDeleteShader(vert);
    glDeleteShader(frag);
}

Graphics::~Graphics()
{ }

void Graphics::draw()
{
    const double SCALE = 1.0f / 65536.0;
    float vertoff[2] =
        { (float) (-SCALE * m_cam_x),
          (float) (-SCALE * m_cam_y) };
    float vertscale[2] = { 32 * 2.0f / m_width, 32 * 2.0f / m_height };

    glClearColor(1.0, 1.0, 1.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    if (tile.count) {
        float texscale[2] = { 1.0f/16, 1.0f/16 };

        tile.a_vert.upload(GL_STREAM_DRAW);
        tile.a_tex.upload(GL_STREAM_DRAW);
        glUseProgram(tile.prog.prog);
        glUniform1i(tile.prog.u_tex, 0);
        glUniform2fv(tile.prog.u_vertoff, 1, vertoff);
        glUniform2fv(tile.prog.u_vertscale, 1, vertscale);
        glUniform2fv(tile.prog.u_texscale, 1, texscale);

        glActiveTexture(GL_TEXTURE0);
        tile.tex->bind();
        glBindVertexArray(tile.arr);
        glDrawArrays(GL_QUADS, 0, tile.count * 4);
        glUseProgram(0);
        glBindVertexArray(0);
    }

    if (sprite.count) {
        float texscale[2] = { 1.0f/16, 1.0f/8 };

        sprite.a_vert.upload(GL_STREAM_DRAW);
        sprite.a_tex.upload(GL_STREAM_DRAW);
        glUseProgram(sprite.prog.prog);
        glUniform1i(sprite.prog.u_tex, 0);
        glUniform2fv(sprite.prog.u_vertoff, 1, vertoff);
        glUniform2fv(sprite.prog.u_vertscale, 1, vertscale);
        glUniform2fv(sprite.prog.u_texscale, 1, texscale);

        glActiveTexture(GL_TEXTURE0);
        sprite.tex->bind();
        glBindVertexArray(sprite.arr);
        glDrawArrays(GL_QUADS, 0, sprite.count * 4);
        glUseProgram(0);
    }

    glDisable(GL_BLEND);
}

void Graphics::clearTiles()
{
    tile.a_vert.clear();
    tile.a_tex.clear();
    tile.count = 0;
}

void Graphics::addTiles(const Layer &layer)
{
    int count = tile.count;
    for (int y = 0, h = layer.height(); y < h; y++) {
        for (int x = 0, w = layer.width(); x < w; x++) {
            int t = layer.get_unchecked(x, y);
            if (!t)
                continue;
            count++;
            t -= 1;
            int tx = t & 15, ty = (t >> 4) & 15;
            tile.a_vert.quad(x, (x + 1),
                             y, (y + 1));
            tile.a_tex.quad(tx, tx + 1, ty + 1, ty);
        }
    }
    tile.count = count;
}

void Graphics::clearSprites()
{
    sprite.a_vert.clear();
    sprite.a_tex.clear();
    sprite.count = 0;
}

void Graphics::addSprite(Vec pos, Vec spos, Vec size, int flags)
{
    const float SCALE = 1.0f / 32.0f;
    Vec ppos = pos.worldToPixel();
    sprite.a_vert.quad(
        SCALE * ppos.x,
        SCALE * ppos.x + size.x,
        SCALE * ppos.y,
        SCALE * ppos.y + size.y);
    if (flags & FLIP_X) {
        spos.x += size.x;
        size.x = -size.x;
    }
    if (flags & FLIP_Y) {
        spos.y += size.y;
        size.y = -size.y;
    }
    sprite.a_tex.quad(
        spos.x,
        spos.x + size.x,
        spos.y + size.y,
        spos.y);
    sprite.count++;
}

}
