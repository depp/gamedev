/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "level.hpp"
#include "sgpp/file.hpp"
#include "tagfile.hpp"
#include <cstdio>
#include <stdexcept>
namespace LD25 {

Level::Level(const char *name)
{
    static const char MAGIC[24] = "Legacy of a Hero level";

    char fullpath[64];
    snprintf(fullpath, sizeof(fullpath), "level/%s", name);
    FBuffer buf(fullpath, 0, "dat", 1024 * 1024);

    if (buf.size() < 24 || std::memcmp(buf.get(), MAGIC, 24 != 0))
        throw std::runtime_error("Level");

    TagFile tags(buf.getUC() + 24, buf.size() - 24);
    for (std::vector<TagFile::Tag>::const_iterator
             i = tags.tags.begin(), e = tags.tags.end(); i != e; i++) {
        if (i->is_named("sect")) {
            std::unique_ptr<Sector> sector(new Sector(i->ptr, i->length));
            std::string name = sector->name;
            auto r = m_sectors.insert(
                SectorMap::value_type(name, std::move(sector)));
            if (!r.second)
                throw std::runtime_error("Level");
        }
    }
}

Level::~Level()
{ }

const Sector &Level::getSector(const std::string &name) const
{
    auto i = m_sectors.find(name);
    if (i == m_sectors.end())
        throw std::runtime_error("Level::getSector");
    return *i->second;
}

}
