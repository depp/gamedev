/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_MAINSCREEN_HPP
#define LD25_MAINSCREEN_HPP
#include "sgpp/ui/keymanager.hpp"
#include "sgpp/ui/screen.hpp"
#include "graphics.hpp"
#include "state.hpp"
#include <memory>
#include <vector>
namespace LD25 {

class MainScreen : public UI::Screen {
public:
    MainScreen();
    virtual ~MainScreen();

    virtual void handleEvent(const UI::Event &evt);
    virtual void update(unsigned msec);
    virtual void draw(Viewport &v, unsigned msec);

private:
    static const unsigned char KEY_MAP[];

    bool m_init;
    unsigned m_tickref;
    unsigned m_delta;

    std::unique_ptr<Graphics> graphics;
    State state;

    UI::KeyManager m_keys;

    unsigned getControls();
};

}
#endif
