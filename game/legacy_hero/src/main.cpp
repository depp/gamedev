/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "mainscreen.hpp"
#include "sgpp/ui/screen.hpp"
using namespace LD25;

UI::Screen *getMainScreen()
{
    return new MainScreen;
}
