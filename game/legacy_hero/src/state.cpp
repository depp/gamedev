/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "state.hpp"
#include "biped.hpp"
#include "player.hpp"
#include "graphics.hpp"
#include "door.hpp"
#include "bat.hpp"
#include <stdexcept>
namespace LD25 {

State::State()
{
    startLevel("sea_shrine");
}

State::~State()
{ }

void State::startLevel(const std::string &name)
{
    level = std::unique_ptr<Level>(new Level(name.c_str()));
    m_nextlevel.clear();
    startSector("entrance");
}

void State::startSector(const std::string &name)
{
    std::string lastsector = m_sectorname;
    m_sectorname = name;

    const Sector &sector = level->getSector(m_sectorname);
    m_nextsector.clear();
    entities.clear();

    foreground = sector.foreground;
    background = sector.background;
    width = sector.width;
    height = sector.height;

    const Sector::Object *start = 0;
    for (auto i = sector.objects.begin(), e = sector.objects.end();
         i != e; i++)
    {
        const Sector::Object &obj = *i;
        if (obj.name == "door") {
            std::string target(obj.properties.get("target"));
            if (target == lastsector)
                start = &obj;
            entities.push_back(
                std::unique_ptr<Entity>(
                    new Door(obj.bb0, obj.bb1, target, *this,
                             obj.properties.getbool("locked"))));
        } else if (obj.name == "levelstart") {
            if (lastsector.empty())
                start = &obj;
        } else if (obj.name == "bat") {
            Vec pos = obj.bb0.tileToWorld() +
                Vec(16 * PIXEL_SIZE, 16 * PIXEL_SIZE);
            entities.push_back(
                std::unique_ptr<Entity>(new Bat(pos)));
        }
    }

    if (!start)
        throw std::runtime_error("State::enterSector");

    entities.push_back(
        std::unique_ptr<Entity>(
            new Biped(Entity::MAT_PLAYER, Biped::SP_YOU,
                      Vec(
                          (start->bb0.x + start->bb1.x) << 15,
                          start->bb0.y << 16),
                      std::unique_ptr<Player>(new Player))));
}

void State::advance(unsigned msec)
{
    if (!m_nextlevel.empty())
        startLevel(m_nextlevel);
    else if (!m_nextsector.empty())
        startSector(m_nextsector);
    player_pos0 = player_pos1; // no jitter on death
    this->msec = msec;
    Entities::iterator i, e;
    for (i = entities.begin(), e = entities.end(); i != e; i++)
        (**i).update(*this);
}

void State::draw(Graphics &gr, int delta)
{
    gr.clearTiles();
    gr.addTiles(background);
    gr.addTiles(foreground);

    Entities::iterator i, e;
    for (i = entities.begin(), e = entities.end(); i != e; i++)
        (**i).draw(gr, delta);

    {
        Vec ppos= Vec::interpolate(player_pos0, player_pos1, delta);
        int gw = gr.width() * PIXEL_SIZE,
            gh = gr.height() * PIXEL_SIZE;
        int x0 = gw / 2, x1 = width * TILE_SIZE + x0 - gw;
        int y0 = gh / 2, y1 = height * TILE_SIZE + y0 - gh;
        int x, y;
        if (x0 >= x1) {
            x = width * TILE_SIZE / 2;
        } else {
            x = ppos.x;
            if (x < x0)
                x = x0;
            if (x > x1)
                x = x1;
        }
        if (y0 >= y1) {
            y = height * TILE_SIZE / 2;
        } else {
            y = ppos.y + gh / 4;
            if (y < y0)
                y = y0;
            if (y > y1)
                y = y1;
        }
        x = ((x + PIXEL_SIZE / 2) & ~(PIXEL_SIZE - 1)) +
            (gr.width() & 1) * PIXEL_SIZE / 2;
        y = ((y + PIXEL_SIZE / 2) & ~(PIXEL_SIZE - 1)) +
            (gr.height() & 1) * PIXEL_SIZE / 2;
        gr.setCamera(x, y);
    }
}

bool State::hitTest(Vec bb0, Vec bb1)
{
    int x0 = bb0.x >> 16, x1 = (bb1.x + 0xffff) >> 16;
    int y0 = bb0.y >> 16, y1 = (bb1.y + 0xffff) >> 16;
    if (x0 < 0)
        x0 = 0;
    if (y0 < 0)
        y0 = 0;
    if (x1 > foreground.width())
        x1 = foreground.width();
    if (y1 > foreground.height())
        y1 = foreground.height();
    for (int y = y0; y < y1; y++) {
        for (int x = x0; x < x1; x++) {
            int t = foreground.get_unchecked(x, y);
            if (t != 0)
                return true;
        }
    }
    return false;
}

Vec State::hitScan(Vec bb0, Vec bb1, Vec d0, Vec d1)
{
    Vec delta = d1 - d0;
    int dist = delta.magnitude();
    if (dist <= TRACE_MAX_STEP) {
        if (!hitTest(bb0 + d1, bb1 + d1))
            return d1;
        if (dist <= TRACE_GRANULARITY)
            return d0;
        Vec d2(d0.x + delta.x / 2, d0.y + delta.y / 2);
        if (hitTest(bb0 + d2, bb1 + d2))
            return hitScan(bb0, bb1, d0, d2);
        else
            return hitScan(bb0, bb1, d2, d1);
    } else {
        // distance is too large, split into two segments
        Vec d2(d0.x + delta.x / 2, d0.y + delta.y / 2);
        Vec r = hitScan(bb0, bb1, d0, d2);
        return r == d2 ? hitScan(bb0, bb1, d2, d1) : r;
    }
}

Entity *State::entityScan(Vec bb0, Vec bb1, int mat_flags)
{
    for (auto i = entities.begin(), e = entities.end(); i != e; i++) {
        Entity &ent = **i;
        if ((1u << ent.material) & mat_flags) {
            if (bb0.x < ent.bbox[1].x &&
                bb0.y < ent.bbox[1].y &&
                bb1.x > ent.bbox[0].x &&
                bb1.y > ent.bbox[0].y)
                return &ent;
        }
    }
    return 0;
}

}
