/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_TAGFILE_HPP
#define LD25_TAGFILE_HPP
#include <vector>
#include <cstddef>
#include <cstring>
namespace LD25 {

class TagFile {
public:
    struct Tag {
        char name[4];
        const char *ptr;
        std::size_t length;

        operator bool() const
        {
            return ptr != 0;
        }

        bool is_named(const char *str) const
        {
            return std::memcmp(str, name, 4) == 0;
        }
    };

    std::vector<Tag> tags;

    TagFile(const void *ptr, std::size_t length);
    Tag find(const char *name) const;
};

}
#endif
