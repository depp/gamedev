/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "bat.hpp"
#include "sgpp/rand.hpp"
#include "state.hpp"
#include "graphics.hpp"
#include <cstdio>
namespace LD25 {

static const int BAT_SIZE = 24 * PIXEL_SIZE;
static const int BAT_WANDERSPEED = TILE_PER_SEC * 3;
static const int BAT_WANDER_MIN = 30 * 1;
static const int BAT_WANDER_MAX = 30 * 3;

Bat::Bat(Vec pos)
    : Entity(MAT_ENEMY),
      m_timer(0), m_pos0(pos), m_pos1(pos), m_state(Wander)
{
    wander();
    bbox[0] = pos - Vec(BAT_SIZE / 2, BAT_SIZE / 2);
    bbox[1] = bbox[0] + Vec(BAT_SIZE, BAT_SIZE);
}

Bat::~Bat()
{ }

void Bat::wander()
{
    {
        unsigned r = Rand::girand();
        int r1 = (int) ((r >> 8) & 0xff) - 128;
        int r2 = (int) ((r >> 16) & 0xff) - 128;
        m_vel = Vec(
            fixed_multiply(r1, BAT_WANDERSPEED),
            fixed_multiply(r2, BAT_WANDERSPEED));
        std::printf("vel: %f, %f\n",
                    (1.0 / TILE_PER_SEC) * m_vel.x,
                    (1.0 / TILE_PER_SEC) * m_vel.y);
    }

    {
        unsigned r = Rand::girand();
        int r1 = (int) ((r >> 8) & 0xffff);
        m_timer = BAT_WANDER_MIN +
            ((r1 * (BAT_WANDER_MAX - BAT_WANDER_MIN)) >> 16);
        std::printf("wandertime: %f\n", 0.032 * m_timer);
    }
}

void Bat::update(State &st)
{
    Vec bb0(BAT_SIZE / 2, BAT_SIZE / 2);
    Vec bb1(bbox[0] + Vec(BAT_SIZE, BAT_SIZE));
    Vec p0(m_pos1), p1(p0 + m_vel);
    Vec t = st.hitScan(bb0, bb1, p0, p1);
    if (t != p1)
        std::puts("Bat hit");
    m_pos0 = p0;
    m_pos1 = t;
    m_vel = t - p0;

    bbox[0] = t - Vec(BAT_SIZE / 2, BAT_SIZE / 2);
    bbox[1] = bbox[0] + Vec(BAT_SIZE, BAT_SIZE);

    m_timer -= 1;
    if (!m_timer) {
        wander();
    }
}

void Bat::draw(Graphics &gr, int delta)
{
    int sprite;
    int flags = 0;
    switch (m_state) {
    case Wander:
        sprite = 2 + ((m_timer / 5) & 1);
        break;

    case Attack:
        sprite = 0 + ((m_timer / 5) & 1);
        if (m_vel.x < 0)
            flags |= Graphics::FLIP_X;
        break;
    }

    gr.addSprite(
        Vec::interpolate(m_pos0, m_pos1, delta) -
        Vec(16 * PIXEL_SIZE, 16 * PIXEL_SIZE),
        Vec(4 + sprite, 5),
        Vec(1, 1),
        flags);
}

}
