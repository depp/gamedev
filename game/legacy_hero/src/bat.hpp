/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_BAT_HPP
#define LD25_BAT_HPP
#include "entity.hpp"
#include "defs.hpp"
namespace LD25 {

class Bat : public Entity {
    typedef enum {
        Wander,
        Attack,
    } BatState;

    int m_timer;
    Vec m_vel, m_pos0, m_pos1;
    BatState m_state;

    void wander();
    void attack(Vec target);

public:
    Bat(Vec pos);
    virtual ~Bat();

    virtual void update(State &st);
    virtual void draw(Graphics &gr, int delta);
};

}
#endif
