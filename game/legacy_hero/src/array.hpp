/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_ARRAY_HPP
#define LD25_ARRAY_HPP
#include <cstdlib>
#include <new>
#include "libpce/util.h"
#include "sg/opengl.h"
namespace LD25 {

template<typename T>
struct ArrayType { };

// ========================================

template<>
struct ArrayType<short> {
    static const GLenum TYPE = GL_SHORT;
    static const int SIZE = 1;
};

template<>
struct ArrayType<short[2]> {
    static const GLenum TYPE = GL_SHORT;
    static const int SIZE = 2;
};

template<>
struct ArrayType<short[3]> {
    static const GLenum TYPE = GL_SHORT;
    static const int SIZE = 3;
};

template<>
struct ArrayType<short[4]> {
    static const GLenum TYPE = GL_SHORT;
    static const int SIZE = 4;
};

// ========================================

template<>
struct ArrayType<float> {
    static const GLenum TYPE = GL_FLOAT;
    static const int SIZE = 1;
};

template<>
struct ArrayType<float[2]> {
    static const GLenum TYPE = GL_FLOAT;
    static const int SIZE = 2;
};

template<>
struct ArrayType<float[3]> {
    static const GLenum TYPE = GL_FLOAT;
    static const int SIZE = 3;
};

template<>
struct ArrayType<float[4]> {
    static const GLenum TYPE = GL_FLOAT;
    static const int SIZE = 4;
};

// ========================================

template<typename T>
class Array {
    T *m_data;
    std::size_t m_alloc;
    std::size_t m_count;
    GLuint m_buffer;

    Array(const Array &);
    Array &operator=(const Array &);

public:
    Array()
        : m_data(0), m_alloc(0), m_count(0)
    {
        glGenBuffers(1, &m_buffer);
    }

    ~Array()
    {
        std::free(m_data);
    }

    void clear()
    {
        m_count = 0;
    }

    GLuint size()
    {
        return static_cast<GLuint>(m_count);
    }

    T *insert(std::size_t n)
    {
        std::size_t c = m_count;
        if (c + n > m_alloc) {
            std::size_t nalloc = pce_round_up_pow2(c + n);
            T *narr = static_cast<T *>(
                std::realloc(m_data, sizeof(T) * nalloc));
            if (!narr)
                throw std::bad_alloc();
            m_data = narr;
        }
        T *p = m_data + c;
        m_count = c + n;
        return p;
    }

    void upload(GLenum usage)
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
        glBufferData(GL_ARRAY_BUFFER, m_count * sizeof(T), m_data, usage);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    void vertexAttrib(GLuint attr)
    {
        glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
        glVertexAttribPointer(
            attr, ArrayType<T>::SIZE, ArrayType<T>::TYPE,
            GL_FALSE, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glEnableVertexAttribArray(attr);
    }

    bool empty() const
    {
        return m_count == 0;
    }
};

template<typename T>
class Array2D : public Array<T[2]> {
public:
    void quad(T x0, T x1, T y0, T y1)
    {
        T (*p)[2] = Array<T[2]>::insert(4);
        p[0][0] = x0; p[0][1] = y0;
        p[1][0] = x0; p[1][1] = y1;
        p[2][0] = x1; p[2][1] = y1;
        p[3][0] = x1; p[3][1] = y0;
    }
};

}
#endif
