/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_DOOR_HPP
#define LD25_DOOR_HPP
#include "usable.hpp"
#include <string>
namespace LD25 {

class Door : public Usable {
    std::string m_target;
    bool m_locked;

public:
    Door(Vec bb0, Vec bb1, std::string target, State &st, bool locked);
    virtual ~Door();

    void use(State &st);
};

}

#endif
