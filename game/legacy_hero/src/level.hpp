/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_LEVEL_HPP
#define LD25_LEVEL_HPP
#include <memory>
#include <map>
#include "sector.hpp"
namespace LD25 {

class Sector;

class Level {
    Level(const Level &);
    Level &operator=(const Level&);

    typedef std::map<std::string, std::unique_ptr<Sector> > SectorMap;
    SectorMap m_sectors;

public:
    Level(const char *name);
    ~Level();

    const Sector &getSector(const std::string &name) const;
};

}
#endif
