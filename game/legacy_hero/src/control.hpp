/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_CONTROL_HPP
#define LD25_CONTROL_HPP
namespace LD25 {

struct Control {
    typedef enum {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        FIRE,
        JUMP
    } Button;

    Control()
    { }

    Control(unsigned flags)
        : flags(flags)
    { }

    unsigned flags;

    bool get(Button b)
    {
        return (flags & (1 << (int) b)) != 0;
    }

    bool left() { return get(LEFT); }
    bool right() { return get(RIGHT); }
    bool up() { return get(UP); }
    bool down() { return get(DOWN); }
    bool fire() { return get(FIRE); }
    bool jump() { return get(JUMP); }
};

}
#endif
