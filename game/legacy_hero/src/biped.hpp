/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_BIPED_HPP
#define LD25_BIPED_HPP
#include "entity.hpp"
#include "defs.hpp"
#include <memory>
namespace LD25 {

class Biped : public Entity {
public:
    static const int STEP_DISTANCE = PIXEL_SIZE * 4;
    static const int GRAVITY = TILE_PER_SEC2 * 64;

    class Controller {
    public:
        Controller()
            : walk_speed(0), jump_velocity(0),
              attack_frame(0), walk_dir(0), jump(0)
        { }

        virtual ~Controller()
        { }

        virtual void update(State &st, Biped &bp) = 0;

        // full walking speed of this character
        int walk_speed;

        // jump velocity of this character
        int jump_velocity;

        // frame of attack: 1 or 2 if attacking, 0 if not
        int attack_frame;

        // walking speed (-65536..65536 fixed point scale)
        int walk_dir;

        // whether to jump: 0 does not jump, 1 jumps only if the jump
        // was reset to 0 since the last jump, and 2 to jump always
        int jump;
    };

    typedef enum {
        FR_STAND,
        FR_RECOIL,
        FR_WALK1,
        FR_WALK2,
        FR_ATTACK1,
        FR_ATTACK2,
        FR_DEAD
    } Frame;

    typedef enum {
        SP_YOU,
        SP_ROBOT,
        SP_BUD,
        SP_SARA,
        SP_TORVUS,
        SP_YUKIKO
    } Sprite;

private:
    struct SpriteInfo;
    struct FrameInfo;

    static const SpriteInfo SPRITE[];
    static const FrameInfo FRAME[];

    const Sprite m_sprite;
    Frame m_frame;
    Vec m_pos0, m_pos1, m_vel;
    std::unique_ptr<Controller> m_controller;

public:
    Biped(Material material, Sprite sprite, Vec pos,
          std::unique_ptr<Controller> &&controller);

    virtual ~Biped();

    virtual void update(State &st);
    virtual void draw(Graphics &gr, int delta);

    // Set state to dead
    void die();

    Vec getPos0() const { return m_pos0; }
    Vec getPos1() const { return m_pos1; }
};

}
#endif
