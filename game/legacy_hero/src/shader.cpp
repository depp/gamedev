/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "shader.hpp"
#include "sgpp/file.hpp"
#include <stdio.h>
#include <stdexcept>
using namespace LD25;

GLuint LD25::load_shader(GLenum type, const char *path)
{
    char fullpath[64];
    snprintf(fullpath, sizeof(fullpath), "shader/%s", path);
    FBuffer buf(fullpath, 0, "glsl", 1024 * 1024);

    const char *darr[1];
    GLint larr[1];

    GLuint shader = glCreateShader(type);
    darr[0] = static_cast<const char *>(buf.get());
    larr[0] = buf.size();
    glShaderSource(shader, 1, darr, larr);
    glCompileShader(shader);
    GLint flag;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &flag);
    if (flag)
        return shader;

    GLint loglen;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &loglen);
    fprintf(stderr, "%s: compilation failed\n", path);
    if (loglen > 0) {
        char *log = new char[loglen];
        glGetShaderInfoLog(shader, loglen, NULL, log);
        fputs(log, stderr);
        delete[] log;
    }
    glDeleteShader(shader);
    throw std::runtime_error("load_shader");
}

void LD25::link_program(GLuint prog, const char *name)
{
    GLint flag, loglen;
    char *log;

    glLinkProgram(prog);
    glGetProgramiv(prog, GL_LINK_STATUS, &flag);
    if (flag)
        return;

    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &loglen);
    fprintf(stderr, "%s: linking failed", name);
    if (loglen > 0) {
        log = new char[loglen];
        glGetProgramInfoLog(prog, loglen, NULL, log);
        fputs(log, stderr);
    }
    throw std::runtime_error("link_program");
}
