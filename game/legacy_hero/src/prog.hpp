/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_PROG_HPP
#define LD25_PROG_HPP
#include "shader.hpp"
#include <cstddef>
namespace LD25 {

struct ProgField {
    typedef enum {
        END,
        UNIFORM,
        ATTRIBUTE
    } Type;

    Type type;
    std::size_t offset;
    char name[12];
};

GLuint load_prog_func(
    char *obj, const char *objname, const ProgField *fields,
    GLuint vert, GLuint frag);

template<typename T>

void load_prog(T& obj, GLuint vert, GLuint frag)
{
    obj.prog = load_prog_func(
        reinterpret_cast<char *>(&obj), T::NAME, T::FIELDS, vert, frag);
}

}
#endif
