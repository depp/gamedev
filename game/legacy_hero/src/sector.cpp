/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "sector.hpp"
#include "tagfile.hpp"
#include "libpce/binary.h"
#include <cstring>
#include <stdexcept>
namespace LD25 {

Sector::Sector(const void *data, std::size_t length)
{
    const char
        *ptr = static_cast<const char *>(data),
        *se = static_cast<const char *>(
            std::memchr(ptr, '\0', length));
    if (!se)
        throw std::runtime_error("Sector");
    std::size_t len = se - ptr;

    name = std::string(ptr, se);

    TagFile tags(se + 1, length - len - 1);

    {
        TagFile::Tag info = tags.find("info");
        if (!info || info.length != 4)
            throw std::runtime_error("Sector");
        width = pce_read_bu16(info.ptr + 0);
        height = pce_read_bu16(info.ptr + 2);
        if (width > MAX_SIZE || height > MAX_SIZE || !width || !height)
            throw std::runtime_error("Sector");
    }

    {
        TagFile::Tag tlbg = tags.find("tlbg");
        if (tlbg)
            background.load(width, height, tlbg.ptr, tlbg.length);
    }

    {
        TagFile::Tag tlfg = tags.find("tlfg");
        if (tlfg)
            foreground.load(width, height, tlfg.ptr, tlfg.length);
    }

    {
        TagFile::Tag objs = tags.find("objs");
        if (objs) {
            const char *ptr = objs.ptr, *end = ptr + objs.length;
            while (ptr != end) {
                if (end - ptr < 8)
                    throw std::runtime_error("Sector");
                Object obj;
                obj.bb0 = Vec(pce_read_bu16(ptr + 0),
                              pce_read_bu16(ptr + 2));
                obj.bb1 = obj.bb0 +
                    Vec(pce_read_bu16(ptr + 4),
                        pce_read_bu16(ptr + 6));
                ptr += 8;

                const char *te = static_cast<const char *>(
                    std::memchr(ptr, '\0', end - ptr));
                if (!te || te == ptr)
                    throw std::runtime_error("Sector");
                obj.name = std::string(ptr, te);
                ptr = te + 1;
                ptr += obj.properties.read(ptr, end - ptr);
                objects.push_back(std::move(obj));
            }
        }
    }
}

}
