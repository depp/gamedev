/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "prog.hpp"
namespace LD25 {

GLuint load_prog_func(
    char *obj, const char *objname, const ProgField *fields,
    GLuint vert, GLuint frag)
{
    GLuint prog = glCreateProgram();
    glAttachShader(prog, vert);
    glAttachShader(prog, frag);
    link_program(prog, objname);
    for (int i = 0; ; i++) {
        if (fields[i].type == ProgField::END)
            return prog;

        GLuint *ptr = reinterpret_cast<GLuint *>(obj + fields[i].offset);

        switch (fields[i].type) {
        case ProgField::END:
            break;

        case ProgField::UNIFORM:
            *ptr = glGetUniformLocation(prog, fields[i].name);
            break;

        case ProgField::ATTRIBUTE:
            *ptr = glGetAttribLocation(prog, fields[i].name);
            break;
        }
    }
}

}
