/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "player.hpp"
#include "control.hpp"
#include "state.hpp"
#include "defs.hpp"
#include "usable.hpp"
namespace LD25 {

Player::Player()
    : m_use(true)
{
    walk_speed = TILE_PER_SEC * 10;
    jump_velocity = TILE_PER_SEC * 25;
}

Player::~Player()
{ }

void Player::update(State &st, Biped &bp)
{
    Control ctl(st.controls);

    walk_dir = 0;
    if (ctl.left())
        walk_dir -= 65536;
    if (ctl.right())
        walk_dir += 65536;

    if (ctl.jump())
        jump = 1;
    else
        jump = 0;

    st.player_pos0 = bp.getPos0();
    st.player_pos1 = bp.getPos1();

    {
        bool do_use;
        if (ctl.down()) {
            do_use = !m_use;
            m_use = true;
        } else {
            do_use = false;
            m_use = false;
        }
        if (do_use) {
            Entity *e = st.entityScan(
                bp.bbox[0], bp.bbox[1], 1u << Entity::MAT_USABLE);
            Usable *u = e ? dynamic_cast<Usable *>(e) : 0;
            if (u) {
                if (do_use)
                    u->use(st);
            }
        }
    }
}

}

