/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_USABLE_HPP
#define LD25_USABLE_HPP
#include "entity.hpp"
namespace LD25 {

class Usable : public Entity {
public:
    Usable(Vec bb0, Vec bb1)
        : Entity(MAT_USABLE)
    {
        bbox[0] = bb0;
        bbox[1] = bb1;
    }

    virtual ~Usable();

    virtual void update(State &st);
    virtual void draw(Graphics &gr, int delta);

    virtual void use(State &st) = 0;
};

}
#endif
