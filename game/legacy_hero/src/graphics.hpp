/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_GRAPHICS_HPP
#define LD25_GRAPHICS_HPP
#include "array.hpp"
#include "vec.hpp"
#include "sgpp/texture.hpp"
namespace LD25 {
class Layer;
struct ProgField;

class Graphics {
    struct TileProg {
        static const ProgField FIELDS[];
        static const char NAME[];

        GLuint prog;
        GLint u_tex;
        GLint u_vertoff;
        GLint u_vertscale;
        GLint u_texscale;
        GLint a_loc;
        GLint a_texcoord;
    };

    struct {
        Texture::Ref tex;

        TileProg prog;
        Array2D<short> a_vert;
        Array2D<short> a_tex;
        GLuint arr;
        int count;
    } tile;

    struct SpriteProg {
        static const ProgField FIELDS[];
        static const char NAME[];

        GLuint prog;
        GLint u_tex;
        GLint u_vertoff;
        GLint u_vertscale;
        GLint u_texscale;
        GLint a_loc;
        GLint a_texcoord;
    };

    struct {
        SpriteProg prog;
        Texture::Ref tex;
        Array2D<float> a_vert;
        Array2D<short> a_tex;
        GLuint arr;
        int count;
    } sprite;

    int m_width;
    int m_height;
    int m_cam_x;
    int m_cam_y;

public:
    enum {
        FLIP_X = 1 << 0,
        FLIP_Y = 1 << 1
    };

    Graphics();
    ~Graphics();

    void draw();

    void clearTiles();
    void addTiles(const Layer &layer);

    void clearSprites();
    void addSprite(Vec pos, Vec spos, Vec size, int flags);

    // Size, in PIXELS
    void setSize(int width, int height)
    {
        m_width = width;
        m_height = height;
    }

    // Camera, in WORLD COORDINATES
    void setCamera(int x, int y)
    {
        m_cam_x = x;
        m_cam_y = y;
    }

    int width() const { return m_width; }
    int height() const { return m_height; }
};

}
#endif
