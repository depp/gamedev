/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "proplist.hpp"
#include <cstring>
#include <stdexcept>
namespace LD25 {

std::size_t PropList::read(const void *ptr, std::size_t length)
{
    const char *start = static_cast<const char *>(ptr),
        *pos = start, *end = start + length;
    while (1) {
        if (pos == end)
            throw std::runtime_error("PropList::read");
        if (!*pos)
            return pos + 1 - start;
        const char *ks = pos;
        const char *ke = static_cast<const char *>(
            std::memchr(ks, '\0', end - ks));
        if (!ke)
            throw std::runtime_error("PropList::read");
        const char *vs = ke + 1;
        const char *ve = static_cast<const char *>(
            std::memchr(vs, '\0', end - vs));
        if (!ve)
            throw std::runtime_error("PropList::read");
        pos = ve + 1;
        m_vec.push_back(
            Property(std::string(ks, ke), std::string(vs, ve)));
    }
}

std::string PropList::get(const std::string &name) const
{
    for (auto i = m_vec.begin(), e = m_vec.end(); i != e; i++)
        if (i->name == name)
            return i->value;
    return std::string();
}

bool PropList::getbool(const std::string &name) const
{
    std::string p = get(name);
    return p == "true";
}

}
