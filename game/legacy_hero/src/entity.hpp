/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_ENTITY_HPP
#define LD25_ENTITY_HPP
#include "vec.hpp"
namespace LD25 {
class Graphics;
class State;

class Entity {
public:
    typedef enum {
        MAT_NONE,
        MAT_SOLID,
        MAT_PLAYER,
        MAT_ENEMY,
        MAT_USABLE
    } Material;

    bool remove;
    Material material;
    Vec bbox[2];

    Entity(Material material)
        : remove(false), material(material)
    { }

    virtual ~Entity()
    { }

    virtual void update(State &st) = 0;
    virtual void draw(Graphics &gr, int delta) = 0;
};

}
#endif
