/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_STATE_HPP
#define LD25_STATE_HPP
#include "entity.hpp"
#include "layer.hpp"
#include "level.hpp"
#include "vec.hpp"
#include "control.hpp"
#include <memory>
#include <vector>
#include <string>
namespace LD25 {

class State {
    std::string m_nextlevel;
    std::string m_nextsector;
    std::string m_sectorname;

    void startLevel(const std::string &name);
    void startSector(const std::string &name);

public:
    typedef std::vector<std::unique_ptr<Entity> > Entities;

    std::unique_ptr<Level> level;
    Layer foreground;
    Layer background;
    Entities entities;
    unsigned msec;
    Control controls;
    int width, height;

    Vec player_pos0, player_pos1;

    State();
    ~State();

    void gotoLevel(const std::string &name)
    {
        m_nextlevel = name;
    }

    void gotoSector(const std::string &name)
    {
        m_nextsector = name;
    }

    void advance(unsigned msec);
    void draw(Graphics &gr, int delta);

    // Determine if the bounding box bb0..bb1 contains any solid
    // tiles.
    bool hitTest(Vec bb0, Vec bb1);

    // Trace the bounding box from bb0..bb1 from location d0 to d1.
    // Return the last location which returns fals from a hitTest.  If
    // this is equal to d1, then nothing was hit.
    Vec hitScan(Vec bb0, Vec bb1, Vec d0, Vec d1);

    // Scan for an entity in the given bbox with the given material
    // flags
    Entity *entityScan(Vec bb0, Vec bb1, int mat_flags);
};

}
#endif
