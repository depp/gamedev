/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_VEC_HPP
#define LD25_VEC_HPP
#include "defs.hpp"
#include <algorithm>
#include <cstdlib>
namespace LD25 {

struct Vec {
    int x, y;

    Vec()
        : x(0), y(0)
    { }

    Vec(int x, int y)
        : x(x), y(y)
    { }

    Vec operator+(const Vec &v) const
    {
        return Vec(x + v.x, y + v.y);
    }

    Vec operator-(const Vec &v) const
    {
        return Vec(x - v.x, y - v.y);
    }

    Vec operator-()
    {
        return Vec(-x, -y);
    }

    static Vec interpolate(Vec v0, Vec v1, int delta)
    {
        return Vec(
            v0.x + (v1.x - v0.x) * delta / FRAME_TIME,
            v0.y + (v1.y - v0.y) * delta / FRAME_TIME);
    }

    bool operator==(const Vec &v) const
    {
        return x == v.x && y == v.y;
    }

    bool operator!=(const Vec &v) const
    {
        return x != v.x || y != v.y;
    }

    // Calculate the L_infinity norm of the vector.
    int magnitude() const
    {
        return std::max(std::abs(x), std::abs(y));
    }

    Vec worldToPixel() const
    {
        return Vec(
            (x + PIXEL_SIZE / 2) >> 11,
            (y + PIXEL_SIZE / 2) >> 11);
    }

    Vec tileToWorld() const
    {
        return Vec(x << 16, y << 16);
    }
};

inline int clip_range(int x, int min, int max)
{
    if (x < min)
        return min;
    if (x > max)
        return max;
    return x;
}

inline int fixed_multiply(int x, int y)
{
    return static_cast<int>(
        (static_cast<long long>(x) *
         static_cast<long long>(y)) >> 16);
}

}
#endif
