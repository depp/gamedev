/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#include "biped.hpp"
#include "graphics.hpp"
#include "state.hpp"
#include <cstdio>
namespace LD25 {

struct Biped::SpriteInfo {
    unsigned char x, y;
};

// The "origin" (ox, oy) is relative to the sprite lower left and
// counted in units of 32 per tile.
struct Biped::FrameInfo {
    unsigned char x, y, w, h, ox, oy, inx0, inx1, iny0, iny1;

    Vec offset() const
    {
        return Vec(-(ox << 11), -(oy << 11));
    }

    Vec tsize() const
    {
        return Vec(w, h);
    }

    Vec wsize() const
    {
        return Vec(w << 16, h << 16);
    }

    Vec bb0() const
    {
        return Vec(
            (inx0 - ox) << 11,
            (iny0 - oy) << 11);
    }

    Vec bb1() const
    {
        return Vec(
            (w << 16) - ((inx1 + ox) << 11),
            (h << 16) - ((iny1 + oy) << 11));
    }
};

const Biped::SpriteInfo Biped::SPRITE[] = {
    { 0, 0 },  // You
    { 0, 4 },  // Robot
    { 8, 0 },  // Bud
    { 12, 0 }, // Sara
    { 8, 4 },  // Torvus
    { 12, 4 }  // Yukiko
};

const Biped::FrameInfo Biped::FRAME[] = {
    { 0, 0, 1, 2, 16, 4, 6, 4, 6, 4 }, // stand
    { 1, 0, 1, 2, 16, 4, 6, 4, 6, 4 }, // recoil
    { 2, 0, 1, 2, 16, 4, 6, 4, 6, 4 }, // walk1
    { 3, 0, 1, 2, 16, 4, 6, 4, 6, 4 }, // walk2
    { 0, 2, 1, 2, 16, 4, 6, 4, 6, 4 }, // attack1
    { 1, 2, 1, 2, 16, 4, 6, 4, 6, 4 }, // attack2
    { 2, 2, 2, 1, 32, 4, 6, 4, 6, 4 }  // dead
};

Biped::Biped(Material material, Sprite sprite, Vec pos,
             std::unique_ptr<Controller> &&controller)
    : Entity(material),
      m_sprite(sprite), m_frame(FR_STAND),
      m_pos0(pos), m_pos1(pos),
      m_vel(0, 0),
      m_controller(std::move(controller))
{ }

Biped::~Biped()
{ }

void Biped::update(State &st)
{
    // Get bounding box
    const FrameInfo &fr = FRAME[m_frame];
    Vec bb0 = fr.bb0(), bb1 = fr.bb1();

    // Update movement
    Controller &c = *m_controller;

    bool on_ground = m_vel.y <= 0 && st.hitTest(
        Vec(bb0.x, bb0.y - TRACE_GRANULARITY) + m_pos0,
        Vec(bb1.x, bb0.y) + m_pos0);
    c.update(st, *this);

    Vec vel;
    if (on_ground) {
        if (c.jump)
            vel.y = c.jump_velocity;
        else
            vel.y = 0;
    } else {
        vel = m_vel;
        vel.y -= GRAVITY;
    }

    int target_walk = fixed_multiply(
        clip_range(c.walk_dir, -0x10000, 0x10000), c.walk_speed);
    vel.x = target_walk;

    // Do hit scan along trajectory
    Vec p0 = m_pos1, p1 = p0 + vel;
    Vec t = st.hitScan(bb0, bb1, p0, p1);
    if (t != p1 && vel.y > 0) {
        // hit something, try a step
        Vec s(0, STEP_DISTANCE);
        Vec s0 = p0 + s, s1 = p1 + s;
        Vec t2 = st.hitScan(bb0, bb1, s0, s1);
        if ((t2 - s0).magnitude() > (t - p0).magnitude()) {
            p0 = s0;
            p1 = s1;
            t = t2;
        }
    }
    if (t != p1) {
        // hit something, try sliding
        bool have_soln = false;
        if (!have_soln && vel.x) {
            Vec p2(p1.x, t.y);
            Vec t3 = st.hitScan(bb0, bb1, t, p2);
            if (t3 != t) {
                t = t3;
                have_soln = true;
            }
        }
        if (!have_soln && vel.y) {
            Vec p2(t.x, p1.y);
            Vec t3 = st.hitScan(bb0, bb1, t, p2);
            if (t3 != t) {
                t = t3;
                have_soln = true;
            }
        }
    }
    m_pos0 = m_pos1;
    m_pos1 = t;
    m_vel = t - p0; // p0, not m_pos0, so step doesn't grant velocity

    bbox[0] = bb0 + t;
    bbox[1] = bb1 + t;
}

void Biped::draw(Graphics &gr, int delta)
{
    const SpriteInfo &sp = SPRITE[m_sprite];
    const FrameInfo &fr = FRAME[m_frame];
    int flags = 0;
    gr.addSprite(
        Vec::interpolate(m_pos0, m_pos1, delta) + fr.offset(),
        Vec(fr.x + sp.x, fr.y + sp.y),
        fr.tsize(), flags);
}

}
