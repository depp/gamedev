/* Copyright 2012 Dietrich Epp <depp@zdome.net> */
#ifndef LD25_DEFS_HPP
#define LD25_DEFS_HPP
namespace LD25 {

static const int LAG_THRESHOLD = 250;
static const int FRAME_TIME = 32;

// The size of a "pixel" if the grid is 32
static const int PIXEL_SIZE = 1 << 11;

static const int TILE_SIZE = 1 << 16;

// This is actually 1000/1024 tiles per sec
// It is a COINCIDENCE that this is PIXEL_SIZE
// because about 32 frames/sec, and about 32 pixels/tile
static const int TILE_PER_SEC = 1 << 11;

// tile per second squared, unit of acceleration
static const int TILE_PER_SEC2 = 1 << 6;

// The resolution of the hit scan
static const int TRACE_GRANULARITY = PIXEL_SIZE;

// The maximum step of the hit scan
static const int TRACE_MAX_STEP = PIXEL_SIZE * 8;

}
#endif
