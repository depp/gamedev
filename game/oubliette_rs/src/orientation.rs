/// Orthogonal orientations for 2D sprites.
#[deriving(Show)]
pub enum Orientation {
    Normal,
    Rotate90,
    Rotate180,
    Rotate270,
    FlipVertical,
    Transpose2,
    FlipHorizontal,
    Transpose,
}

fn from_int(n: uint) -> Orientation {
    match n {
        0 => Normal,
        1 => Rotate90,
        2 => Rotate180,
        3 => Rotate270,
        4 => FlipVertical,
        5 => Transpose2,
        6 => FlipHorizontal,
        7 => Transpose,
        _ => Normal,
    }
}

impl Mul<Orientation, Orientation> for Orientation {
    fn mul(&self, rhs: &Orientation) -> Orientation {
        let (xi, yi) = (*self as uint, *rhs as uint);
        let (xm, ym) = (xi & 4, yi & 4);
        let (xr, yr) = (xi & 3, yi & 3);
        let xr2 = if ym != 0 { xr * 3 } else { xr };
        from_int((xm ^ ym) | ((xr2 + yr) & 3))
    }
}
