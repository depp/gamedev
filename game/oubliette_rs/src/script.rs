extern crate collections;
use std::result::Result;
use std::str::CharSplits;
use collections::hashmap::HashMap;

/// The type for a line in the script.
pub enum LineType {
    GameText,
    SpeakerA,
    SpeakerB,
}

/// A line of text in the script.
pub struct Line {
    line_type: LineType,
    text: ~str,
}

/// Script section, a collection of lines.
pub struct Section {
    lines: Vec<Line>,
}

/// Game script, a collection of sections.
pub struct Script {
    sections: HashMap<~str, Section>,
}

/// A script parsing error.
pub struct ParseError {
    lineno: uint,
    message: &'static str,
}

/// Lexical token in the script.
enum Token<'a> {
    EndOfFile,
    ErrorToken,
    StartSection(&'a str),
    StartLine(LineType, &'a str),
    ContinueLine(&'a str),
}

/// Parse a line with a speaker.
fn parse_speaker<'a>(gtype: LineType, line: &'a str) -> Token<'a> {
    if !line.starts_with(": ") {
        ErrorToken
    } else {
        StartLine(gtype, line.slice_from(2).trim_right())
    }
}

/// Tokenize a physical line of text in the file.
fn parse_line<'a>(line: &'a str) -> Option<Token<'a>> {
    Some(match line.slice_shift_char() {
        (None, _) => return None,
        (Some('#'), _) => return None,
        (Some('@'), rest) => StartSection(rest.trim()),
        (Some('G'), rest) => parse_speaker(GameText, rest),
        (Some('A'), rest) => parse_speaker(SpeakerA, rest),
        (Some('B'), rest) => parse_speaker(SpeakerB, rest),
        (Some(' '), rest) => if !rest.starts_with("  ") {
            ErrorToken
        } else {
            ContinueLine(rest.slice_from(2).trim_right())
        },
        (Some(_), _) => ErrorToken,
    })
}

/// Tokenizer for a script.
struct Lexer<'a> {
    text: CharSplits<'a, char>,
    lineno: uint,
    token: Token<'a>,
}

impl<'a> Lexer<'a> {
    /// Get the next token in the script.
    fn next(&mut self) {
        loop {
            self.lineno += 1;
            match self.text.next() {
                None => {
                    self.token = EndOfFile;
                    return;
                },
                Some(line) => {
                    match parse_line(line) {
                        None => (),
                        Some(token) => {
                            self.token = token;
                            return;
                        }
                    };
                }
            };
        }
    }
}

/// Parse a section of the script.
fn parse_section(lexer: &mut Lexer) -> Option<(~str, Section)> {
    let sec_name = match lexer.token {
        StartSection(sec_name) => sec_name,
        _ => return None,
    };
    lexer.next();
    let mut lines = Vec::new();
    let mut segments = Vec::new();
    loop {
        segments.clear();
        let (line_type, seg) = match lexer.token {
            StartLine(line_type, seg) => (line_type, seg),
            _ => break,
        };
        segments.push(seg);
        lexer.next();
        loop {
            let seg = match lexer.token {
                ContinueLine(seg) => seg,
                _ => break,
            };
            segments.push(seg);
            lexer.next();
        }
        lines.push(Line {
            line_type: line_type,
            text: segments.connect("\n"),
        });
    }
    Some((sec_name.to_owned(), Section { lines: lines }))
}

/// Parse a script.
pub fn parse_script(script_text: &str) -> Result<Script, ParseError> {
    let mut script = Script { sections: HashMap::new() };
    let mut lexer = Lexer {
        text: script_text.lines(),
        lineno: 0,
        token: EndOfFile,
    };
    lexer.next();
    loop {
        match parse_section(&mut lexer) {
            None => break,
            Some((name, section)) =>
                script.sections.insert(name, section),
        };
    }
    match lexer.token {
        EndOfFile => Ok(script),
        ErrorToken => Err(ParseError {
            lineno: lexer.lineno,
            message: "Invalid line format",
        }),
        _ => Err(ParseError {
            lineno: lexer.lineno,
            message: "Unexpected line",
        }),
    }
}
