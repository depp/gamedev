use color::Color;
use ivec::{IVec, IRect};
use orientation::Orientation;
mod color;
mod ivec;
mod orientation;



pub trait Graphics {
    /// Add a sprite to the screen.
    fn add_sprite(&mut self, pos: IVec, orientation: Orientation,
                  screen_relative: bool);

    /// Set the camera target.
    fn set_camera_pos(&mut self, pos: IVec);

    /// Set the editor's selection.
    fn set_selection(&mut self, rect: &IRect);

    /// Set the blend color.
    fn set_blend_color(&mut self, color: &Color);

    /// Get the text layout object.
    fn text<'a>(&'a mut self) -> &'a mut TextLayout;
}

pub trait TextLayout {
    fn clear(&mut self);

    fn add_line(&mut self, origin: &IVec, text: &str) -> int;

    fn add_rect(&mut self, origin: &IVec) -> int;
}
fn main() {}