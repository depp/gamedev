static PALETTE: [[u8, ..4], ..16] = [
    [  20,  12,  28, 255],
    [  68,  36,  52, 255],
    [  48,  52, 109, 255],
    [  78,  74,  78, 255],
    [ 133,  76,  48, 255],
    [  52, 101,  36, 255],
    [ 208,  70,  72, 255],
    [ 117, 113,  97, 255],
    [  89, 125, 206, 255],
    [ 210, 125,  44, 255],
    [ 133, 149, 161, 255],
    [ 109, 170,  44, 255],
    [ 210, 170, 153, 255],
    [ 109, 194, 202, 255],
    [ 218, 212,  94, 255],
    [ 222, 238, 214, 255],
];

pub struct Color([f32, ..4]);

impl Index<uint, f32> for Color {
    fn index(&self, index: &uint) -> f32 {
        let Color(ref arr) = *self;
        arr[*index]
    }
}

impl Color {
    /// Create a color using the given function for each component.
    fn new(func: |uint| -> f32) -> Color {
        let mut result = [0.0, ..4];
        for i in range(0u, 4u) {
            result[i] = func(i);
        }
        Color(result)
    }

    /// Get the transparent color.
    pub fn transparent() -> Color {
        Color([0.0, 0.0, 0.0, 0.0])
    }

    /// Get a color from the palette.
    pub fn palette(index: int) -> Color {
        if index == -1 {
            Color::transparent()
        } else if index >= 0 && index < 16 {
            let scale: f32 = 1.0 / 255.0;
            let color = PALETTE[index as uint];
            Color::new(|i| { color[i] as f32 * scale })
        } else {
            fail!();
        }
    }

    // Generate a faded version of a color.
    pub fn fade(&self, alpha: f32) -> Color {
        Color::new(|i| { self[i] * alpha })
    }

    // Blend two colors together.
    pub fn blend(x: &Color, y: &Color, a: f32) -> Color {
        Color::new(|i| { x[i] * (1.0 - a) + y[i] * a })
    }
}
