use ivec::IVec;
use std::cmp::{max,min};
mod ivec;

/// A rectangle packing.
pub struct Packing {
    /// The total size of the packing.
    size: IVec,

    /// The locations of the packed rectangles.
    locations: Vec<IVec>,
}

/// A rectangle to be packed.
struct Rect {
    index: uint,
    size: IVec,
}

fn compare_rect(a: &Rect, b: &Rect) -> Ordering {
    let mut ord = b.size.x.cmp(&a.size.x);
    if ord == Equal { ord = b.size.y.cmp(&a.size.y); }
    ord
}

fn try_pack(width: uint, height: uint, rects: &[Rect],
            frontier: &mut Vec<uint>, locations: &mut Vec<IVec>) -> bool {
    frontier.clear();
    frontier.grow(height, &0u);
    let mut ypos = 0u;
    for rect in rects.iter() {
        let (rw, rh) = (rect.size.x as uint, rect.size.y as uint);
        if ypos + rw > height {
            ypos = 0;
        }
        while ypos + rh <= height && frontier.get(ypos) + rw > width {
            ypos += 1;
        }
        if ypos + rh > height {
            return false;
        }
        let xpos = *frontier.get(ypos);
        for i in range(0, ypos + rh) {
            let v = frontier.get_mut(i);
            *v = max(*v, xpos + rw);
        }
        assert!(ypos <= height - rh);
        assert!(xpos <= width - rw);
        *locations.get_mut(rect.index) = IVec {
            x: xpos as int,
            y: ypos as int,
        };
        ypos += rh;
    }
    true
}

static MIN_DIMENSION: int = 4;
static MAX_DIMENSION: int = 10;

/// Pack rectangles into an enclosing rectangle.  The enclosing rectangle will
/// have dimensions that are powers of two.
pub fn pack(sizes: &[IVec]) -> Option<Packing> {
    let mut object_area: u64 = 0;
    let mut rects = Vec::from_fn(sizes.len(), |index| {
        let size = sizes[index];
        object_area += size.x as u64 * size.y as u64;
        Rect { index: index, size: size }
    });
    rects.sort_by(compare_rect);

    let mut frontier = Vec::new();
    let mut locations = Vec::from_elem(sizes.len(), IVec::zero());
    for size in range(0, MAX_DIMENSION * 2 + 1) {
        if (1u64 << size) < object_area {
            continue;
        }
        let limit = min(size - MIN_DIMENSION, MAX_DIMENSION);
        for major in range((size + 1) / 2, limit + 1) {
            let minor = size - major;
            if try_pack(major as uint, minor as uint, rects.as_slice(),
                        &mut frontier, &mut locations) {
                return Some(Packing {
                    size: IVec { x: major, y: minor },
                    locations: locations,
                });
            }
            if minor != major &&
                try_pack(minor as uint, major as uint, rects.as_slice(),
                         &mut frontier, &mut locations) {
                return Some(Packing {
                    size: IVec { x: minor, y: major },
                    locations: locations,
                });
            }
        }
    }
    None
}
