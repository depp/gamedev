/// 2D vector with integer coordinates.
#[deriving(Eq, Clone)]
pub struct IVec {
    pub x: int,
    pub y: int,
}

impl IVec {
    /// The zero vector.
    pub fn zero() -> IVec {
        IVec { x: 0, y: 0 }
    }
}

impl Add<IVec, IVec> for IVec {
    fn add(&self, rhs: &IVec) -> IVec {
        IVec { x: self.x + rhs.x, y: self.y + rhs.y }
    }
}

impl Sub<IVec, IVec> for IVec {
    fn sub(&self, rhs: &IVec) -> IVec {
        IVec { x: self.x - rhs.x, y: self.y - rhs.y }
    }
}

impl Neg<IVec> for IVec {
    fn neg(&self) -> IVec {
        IVec { x: -self.x, y: -self.y }
    }
}

/// 2D rectangle with integer coordinates.
#[deriving(Eq, Clone)]
pub struct IRect {
    pub min: IVec,
    pub max: IVec,
}

impl IRect {
    /// Create a rectangle with the given coordinates.
    pub fn new(x0: int, y0: int, x1: int, y1: int) -> IRect {
        IRect {
            min: IVec { x: x0, y: y0 },
            max: IVec { x: x1, y: y1 },
        }
    }

    /// Create a rectangle centered on the origin.
    pub fn new_centered(width: int, height: int) -> IRect {
        IRect {
            min: IVec { x: -(width/2), y: -(height/2) },
            max: IVec { x: width - (width/2), y: height - (height/2) },
        }
    }

    /// Test whether a rectangle contains a point.
    pub fn contains(&self, pt: &IVec) -> bool {
        pt.x >= self.min.x && pt.x < self.max.x &&
        pt.y >= self.min.y && pt.y < self.max.y
    }

    /// Test whether two rects intersects.
    pub fn intersects(&self, r: &IRect) -> bool {
        self.min.x < r.max.x && self.max.x > r.min.x &&
        self.min.y < r.max.y && self.max.y > r.max.y
    }

    /// Expand a rectangle by a different amount in each direction.
    pub fn expand_rect(&self, r: &IRect) -> IRect {
        IRect { min: self.min + r.min, max: self.max + r.max }
    }

    /// Expand a rectangle.
    pub fn expand_xy(&self, x: int, y: int) -> IRect {
        let v = IVec { x: x, y: y };
        IRect { min: self.min - v, max: self.max + v }
    }

    /// Expand a rectangle uniformly.
    pub fn expand(&self, d: int) -> IRect {
        let v = IVec { x: d, y: d };
        IRect { min: self.min - v, max: self.max + v }
    }
}

impl Add<IVec, IRect> for IRect {
    fn add(&self, rhs: &IVec) -> IRect {
        IRect { min: self.min + *rhs, max: self.max + *rhs }
    }
}

impl Sub<IVec, IRect> for IRect {
    fn sub(&self, rhs: &IVec) -> IRect {
        IRect { min: self.min - *rhs, max: self.max - *rhs }
    }
}
