# The Oubliette Within (Rust port)

"The Oubliette Within" is a game I made for Ludum Dare 29, with the theme "beneath the surface".
This is a port of that game to Rust.
The original game is written in C++.
