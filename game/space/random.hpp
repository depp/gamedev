#ifndef GAME_SPACE_RANDOM_HPP
#define GAME_SPACE_RANDOM_HPP
namespace Space {

unsigned int statelessRandom(unsigned int seed, unsigned int data);

}
#endif
