#include "thinker.hpp"
namespace Space {

Thinker::~Thinker()
{ }

void Thinker::enterGame(World &)
{ }

void Thinker::leaveGame(World &)
{ }

}
