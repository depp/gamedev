/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

#include "config.h"
#include <stddef.h>
struct sg_data;
struct sg_error;
struct sg_image;
struct sg_pixbuf;

#if defined ENABLE_PNG

/* Load a PNG image.  */
struct sg_image *
sg_image_png(const struct sg_data *data, struct sg_error **err);

#endif

#if defined ENABLE_JPEG

/* Load a JPEG image.  */
struct sg_image *
sg_image_jpeg(const struct sg_data *data, struct sg_error **err);

#endif

/* Convert image data from non-premultiplied alpha to premultiplied
   alpha.  This only works on RGBA images.  */
void
sg_pixbuf_premultiply_alpha(struct sg_pixbuf *pbuf);
