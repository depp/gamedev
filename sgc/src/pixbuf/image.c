/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

/* Maximum size of an image file in bytes.  */
#define SG_IMAGE_MAXSIZE (16 * 1024 * 1024)

#include "config.h"
#include "sg/data.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/pixbuf.h"
#include "private.h"

#include <string.h>

const char SG_PIXBUF_IMAGE_EXTENSIONS[] = ""
#if defined ENABLE_PNG
    "png:"
#endif
#if defined ENABLE_JPEG
    "jpg:"
#endif
    ;

#if defined ENABLE_PNG
static const unsigned char SG_PIXBUF_PNGHEAD[8] = {
    137, 80, 78, 71, 13, 10, 26, 10
};
#endif

#if defined ENABLE_JPEG
static const unsigned char SG_PIXBUF_JPEGHEAD[2] = { 255, 216 };
#endif

struct sg_image *
sg_image_file(const char *path, struct sg_error **err)
{
    struct sg_data data;
    struct sg_image *image;
    int r;
    r = sg_load(&data, NULL, path, SG_PIXBUF_IMAGE_EXTENSIONS, SG_IMAGE_MAXSIZE,
                err);
    if (r)
        return NULL;
    image = sg_image_buffer(&data, err);
    sg_data_destroy(&data);
    return image;
}

struct sg_image *
sg_image_buffer(const struct sg_data *data, struct sg_error **err)
{
    const void *ptr = data->ptr;
    size_t size = data->size;

    (void) ptr;
    (void) size;

#if defined ENABLE_PNG
    if (size >= 8 && !memcmp(ptr, SG_PIXBUF_PNGHEAD, 8))
        return sg_image_png(data, err);
#endif

#if defined ENABLE_JPEG
    if (size >= 2 && !memcmp(ptr, SG_PIXBUF_JPEGHEAD, 2))
        return sg_image_jpeg(data, err);
#endif

    sg_error_set(err, SG_ERR_BADDATA);
    return NULL;
}

#if !defined ENABLE_PNG

int
sg_pixbuf_writepng(struct sg_pixbuf *pbuf, const char *path, size_t pathlen,
                   struct sg_error **err)
{
    (void) pbuf;
    (void) path;
    (void) pathlen;
    sg_error_disabled(err, "png");
    return -1;
}

#endif
