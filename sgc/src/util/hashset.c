/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/hash.h"
#include "sg/hashset.h"
#include "sg/util.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

/* Calculate capacity for hash with given number of values.  */
static size_t sg_hashset_margin(size_t x) {
    return x + (x >> 1);
}

static void sg_hashset_xfer(char **restrict d, size_t dsz, char **restrict s,
                            size_t ssz) {
    size_t i, j, pos;
    unsigned hash;
    char *key;
    for (i = 0; i < ssz; i++) {
        key = s[i];
        if (!key) continue;
        hash = sg_hash(key, strlen(key));
        for (j = 0; j < dsz; j++) {
            pos = (hash + j) & (dsz - 1);
            if (!d[pos]) {
                d[pos] = key;
                break;
            }
        }
        assert(j < dsz);
    }
}

void sg_hashset_init(struct sg_hashset *restrict sp) {
    sp->data = NULL;
    sp->size = 0;
    sp->capacity = 0;
}

void sg_hashset_destroy(struct sg_hashset *restrict sp) {
    free(sp->data);
}

int sg_hashset_test(struct sg_hashset *restrict sp, const char *restrict key) {
    char **es = sp->data;
    size_t i, pos = 0, n = sp->capacity, len = strlen(key);
    unsigned hash;
    hash = sg_hash(key, len);
    for (i = 0; i < n; i++) {
        pos = (i + hash) & (n - 1);
        if (!es[pos]) return 0;
        if (!strcmp(key, es[pos])) return 1;
    }
    return 0;
}

char **sg_hashset_insert(struct sg_hashset *restrict sp,
                         const char *restrict key) {
    char **es = sp->data, **nes;
    size_t i, pos = 0, n = sp->capacity, len = strlen(key), nn;
    unsigned hash;
    hash = sg_hash(key, len);
    for (i = 0; i < n; i++) {
        pos = (i + hash) & (n - 1);
        if (!es[pos]) break;
        if (!strcmp(key, es[pos])) return &es[pos];
    }
    nn = sg_round_up_pow2(sg_hashset_margin(sp->size + 1));
    if (nn <= n) return &es[pos];
    nes = calloc(nn, sizeof(*nes));
    if (!nes) return NULL;
    sg_hashset_xfer(nes, nn, es, n);
    sp->data = nes;
    sp->capacity = nn;
    free(es);
    es = nes;
    n = nn;
    for (i = 0; i < n; i++) {
        pos = (i + hash) & (n - 1);
        if (!es[pos]) break;
    }
    assert(i < n);
    return &es[pos];
}
