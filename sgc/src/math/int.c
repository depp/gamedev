/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/vecmath.h"

float iclamp(float x, float min_val, float max_val) {
    if (x >= min_val) {
        if (x <= max_val) {
            return x;
        } else {
            return max_val;
        }
    } else {
        return min_val;
    }
}
