/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/vecmath.h"

#include <math.h>

/*
 * Memory layout:
 *
 * [ 0  3  6
 *   1  4  7
 *   2  5  8 ]
 *
 * [ 11 21 31 12 22 32 13 23 33 ]
 */

void mat3_copy(mat3 *restrict out, const mat3 *restrict a) {
    *out = *a;
}

void mat3_identity(mat3 *out) {
    out->v[0] = 1.0f;
    out->v[1] = 0.0f;
    out->v[2] = 0.0f;
    out->v[3] = 0.0f;
    out->v[4] = 1.0f;
    out->v[5] = 0.0f;
    out->v[6] = 0.0f;
    out->v[7] = 0.0f;
    out->v[8] = 1.0f;
}

void mat3_scaling(mat3 *restrict out, vec3 scale) {
    out->v[0] = scale.v[0];
    out->v[1] = 0.0f;
    out->v[2] = 0.0f;
    out->v[3] = 0.0f;
    out->v[4] = scale.v[1];
    out->v[5] = 0.0f;
    out->v[6] = 0.0f;
    out->v[7] = 0.0f;
    out->v[8] = scale.v[2];
}

void mat3_axis_angle(mat3 *restrict out, vec3 axis, float angle) {
    float x = axis.v[0], y = axis.v[1], z = axis.v[2];
    float a = 1.0f / sqrtf(x * x + y * y + z * z);
    x *= a;
    y *= a;
    z *= a;
    float c = cosf(angle), s = sinf(angle);
    out->v[0] = c + x * x * (1.0f - c);
    out->v[1] = y * x * (1.0f - c) + z * s;
    out->v[2] = z * x * (1.0f - c) - y * s;
    out->v[3] = x * y * (1.0f - c) - z * s;
    out->v[4] = c + y * y * (1.0f - c);
    out->v[5] = z * y * (1.0f - c) + x * s;
    out->v[6] = x * z + (1.0f - c) + y * s;
    out->v[7] = y * z * (1.0f - c) - x * s;
    out->v[8] = c + z * z * (1.0f - c);
}

void mat3_quat(mat3 *restrict out, quat q) {
    float w = q.v[0], x = q.v[1], y = q.v[2], z = q.v[3];
    out->v[0] = 1.0f - 2.0f * y * y - 2.0f * z * z;
    out->v[1] = 2.0f * x * y + 2.0f * w * z;
    out->v[2] = 2.0f * z * x - 2.0f * w * y;
    out->v[3] = 2.0f * x * y - 2.0f * w * z;
    out->v[4] = 1.0f - 2.0f * x * x - 2.0f * y * y;
    out->v[5] = 2.0f * y * z + 2.0f * w * x;
    out->v[6] = 2.0f * z * x + 2.0f * w * y;
    out->v[7] = 2.0f * y * z - 2.0f * w * x;
    out->v[8] = 1.0f - 2.0f * x * x - 2.0f * z * z;
}

void mat3_mul(mat3 *out, const mat3 *a, const mat3 *b) {
    float m11, m21, m31, m12, m22, m32, m13, m23, m33;
    m11 = a->v[0] * b->v[0] + a->v[3] * b->v[1] + a->v[6] * b->v[2];
    m21 = a->v[1] * b->v[0] + a->v[4] * b->v[1] + a->v[7] * b->v[2];
    m31 = a->v[2] * b->v[0] + a->v[5] * b->v[1] + a->v[8] * b->v[2];
    m12 = a->v[0] * b->v[3] + a->v[3] * b->v[4] + a->v[6] * b->v[5];
    m22 = a->v[1] * b->v[3] + a->v[4] * b->v[4] + a->v[7] * b->v[5];
    m32 = a->v[2] * b->v[3] + a->v[5] * b->v[4] + a->v[8] * b->v[5];
    m13 = a->v[0] * b->v[6] + a->v[3] * b->v[7] + a->v[6] * b->v[8];
    m23 = a->v[1] * b->v[6] + a->v[4] * b->v[7] + a->v[7] * b->v[8];
    m33 = a->v[2] * b->v[6] + a->v[5] * b->v[7] + a->v[8] * b->v[8];
    out->v[0] = m11;
    out->v[1] = m21;
    out->v[2] = m31;
    out->v[3] = m12;
    out->v[4] = m22;
    out->v[5] = m32;
    out->v[6] = m13;
    out->v[7] = m23;
    out->v[8] = m33;
}

vec3 mat3_vec3(const mat3 *restrict a, vec3 v) {
    vec3 r;
    r.v[0] = a->v[0] * v.v[0] + a->v[3] * v.v[1] + a->v[6] * v.v[2];
    r.v[1] = a->v[1] * v.v[0] + a->v[4] * v.v[1] + a->v[7] * v.v[2];
    r.v[2] = a->v[2] * v.v[0] + a->v[5] * v.v[1] + a->v[8] * v.v[2];
    return r;
}
