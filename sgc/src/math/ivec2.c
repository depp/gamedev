/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/vecmath.h"

bool ivec2_eq(ivec2 x, ivec2 y) {
    return x.v[0] == y.v[0] && x.v[1] == y.v[1];
}

ivec2 ivec2_add(ivec2 x, ivec2 y) {
    ivec2 r;
    r.v[0] = x.v[0] + y.v[0];
    r.v[1] = x.v[1] + y.v[1];
    return r;
}

ivec2 ivec2_sub(ivec2 x, ivec2 y) {
    ivec2 r;
    r.v[0] = x.v[0] - y.v[0];
    r.v[1] = x.v[1] - y.v[1];
    return r;
}

ivec2 ivec2_neg(ivec2 x) {
    ivec2 r;
    r.v[0] = -x.v[0];
    r.v[1] = -x.v[1];
    return r;
}

ivec2 ivec2_mul(ivec2 x, ivec2 y) {
    ivec2 r;
    r.v[0] = x.v[0] * y.v[0];
    r.v[1] = x.v[1] * y.v[1];
    return r;
}

ivec2 ivec2_scale(ivec2 x, int a) {
    ivec2 r;
    r.v[0] = x.v[0] * a;
    r.v[1] = x.v[1] * a;
    return r;
}

ivec2 ivec2_min(ivec2 x, ivec2 y) {
    ivec2 r;
    r.v[0] = imin(x.v[0], y.v[0]);
    r.v[1] = imin(x.v[1], y.v[1]);
    return r;
}

ivec2 ivec2_max(ivec2 x, ivec2 y) {
    ivec2 r;
    r.v[0] = imax(x.v[0], y.v[0]);
    r.v[1] = imax(x.v[1], y.v[1]);
    return r;
}

ivec2 ivec2_clamp(ivec2 x, int min_val, int max_val) {
    ivec2 r;
    r.v[0] = iclamp(x.v[0], min_val, max_val);
    r.v[1] = iclamp(x.v[1], min_val, max_val);
    return r;
}

ivec2 ivec2_clampv(ivec2 x, ivec2 min_val, ivec2 max_val) {
    ivec2 r;
    r.v[0] = iclamp(x.v[0], min_val.v[0], max_val.v[0]);
    r.v[1] = iclamp(x.v[1], min_val.v[1], max_val.v[1]);
    return r;
}

int ivec2_length2(ivec2 x) {
    return x.v[0] * x.v[0] + x.v[1] * x.v[1];
}

int ivec2_distance2(ivec2 x, ivec2 y) {
    return ivec2_length2(ivec2_sub(x, y));
}
