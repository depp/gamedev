/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/vecmath.h"

#include <math.h>

quat quat_identity(void) {
    quat r;
    r.v[0] = 1.0f;
    r.v[1] = 0.0f;
    r.v[2] = 0.0f;
    r.v[3] = 0.0f;
    return r;
}

quat quat_conjugate(quat q) {
    quat r;
    r.v[0] = q.v[0];
    r.v[1] = -q.v[1];
    r.v[2] = -q.v[2];
    r.v[3] = -q.v[3];
    return r;
}

quat quat_axis_angle(vec3 axis, float angle) {
    float x = axis.v[0], y = axis.v[1], z = axis.v[2];
    float a = sinf(0.5f * angle) / sqrtf(x * x + y * y + z * z);
    quat r;
    r.v[0] = cosf(0.5f * angle);
    r.v[1] = a * x;
    r.v[2] = a * y;
    r.v[3] = a * z;
    return r;
}

quat quat_angles(vec3 angles) {
    return quat_mul(
        quat_rotate_z(angles.v[2]),
        quat_mul(quat_rotate_y(angles.v[1]), quat_rotate_x(angles.v[0])));
}

quat quat_rotate_x(float angle) {
    quat r;
    r.v[0] = cosf(0.5f * angle);
    r.v[1] = sinf(0.5f * angle);
    r.v[2] = 0.0f;
    r.v[3] = 0.0f;
    return r;
}

quat quat_rotate_y(float angle) {
    quat r;
    r.v[0] = cosf(0.5f * angle);
    r.v[1] = 0.0f;
    r.v[2] = sinf(0.5f * angle);
    r.v[3] = 0.0f;
    return r;
}

quat quat_rotate_z(float angle) {
    quat r;
    r.v[0] = cosf(0.5f * angle);
    r.v[1] = 0.0f;
    r.v[2] = 0.0f;
    r.v[3] = sinf(0.5f * angle);
    return r;
}

quat quat_mul(quat x, quat y) {
    float *a = x.v, *b = y.v;
    quat r;
    r.v[0] = a[0] * b[0] - a[1] * b[1] - a[2] * b[2] - a[3] * b[3];
    r.v[1] = a[0] * b[1] + a[1] * b[0] + a[2] * b[3] - a[3] * b[2];
    r.v[2] = a[0] * b[2] + a[2] * b[0] + a[3] * b[1] - a[1] * b[3];
    r.v[3] = a[0] * b[3] + a[3] * b[0] + a[1] * b[2] - a[2] * b[1];
    return r;
}

vec3 quat_vec3(quat q, vec3 v) {
    float w = q.v[0], x = q.v[1], y = q.v[2], z = q.v[3];
    vec3 r;
    r.v[0] = v.v[0] * (1.0f - 2.0f * y * y - 2.0f * z * z) +
             v.v[1] * (2.0f * x * y - 2.0f * w * z) +
             v.v[2] * (2.0f * z * x + 2.0f * w * y);
    r.v[1] = v.v[0] * (2.0f * x * y + 2.0f * w * z) +
             v.v[1] * (1.0f - 2.0f * z * z - 2.0f * x * x) +
             v.v[2] * (2.0f * y * z - 2.0f * w * x);
    r.v[2] = v.v[0] * (2.0f * z * x - 2.0f * w * y) +
             v.v[1] * (2.0f * y * z + 2.0f * w * x) +
             v.v[2] * (1.0f - 2.0f * x * x - 2.0f * y * y);
    return r;
}
