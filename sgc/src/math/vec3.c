/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/vecmath.h"

#include <math.h>

bool vec3_eq(vec3 x, vec3 y) {
    return x.v[0] == y.v[0] && x.v[1] == y.v[1] && x.v[2] == y.v[2];
}

vec3 vec3_add(vec3 x, vec3 y) {
    vec3 r;
    r.v[0] = x.v[0] + y.v[0];
    r.v[1] = x.v[1] + y.v[1];
    r.v[2] = x.v[2] + y.v[2];
    return r;
}

vec3 vec3_sub(vec3 x, vec3 y) {
    vec3 r;
    r.v[0] = x.v[0] - y.v[0];
    r.v[1] = x.v[1] - y.v[1];
    r.v[2] = x.v[2] - y.v[2];
    return r;
}

vec3 vec3_neg(vec3 x) {
    vec3 r;
    r.v[0] = -x.v[0];
    r.v[1] = -x.v[1];
    r.v[2] = -x.v[2];
    return r;
}

vec3 vec3_mul(vec3 x, vec3 y) {
    vec3 r;
    r.v[0] = x.v[0] * y.v[0];
    r.v[1] = x.v[1] * y.v[1];
    r.v[2] = x.v[2] * y.v[2];
    return r;
}

vec3 vec3_scale(vec3 x, float a) {
    vec3 r;
    r.v[0] = x.v[0] * a;
    r.v[1] = x.v[1] * a;
    r.v[2] = x.v[2] * a;
    return r;
}

vec3 vec3_min(vec3 x, vec3 y) {
    vec3 r;
    r.v[0] = fminf(x.v[0], y.v[0]);
    r.v[1] = fminf(x.v[1], y.v[1]);
    r.v[2] = fminf(x.v[2], y.v[2]);
    return r;
}

vec3 vec3_max(vec3 x, vec3 y) {
    vec3 r;
    r.v[0] = fmaxf(x.v[0], y.v[0]);
    r.v[1] = fmaxf(x.v[1], y.v[1]);
    r.v[2] = fmaxf(x.v[2], y.v[2]);
    return r;
}

vec3 vec3_clamp(vec3 x, float min_val, float max_val) {
    vec3 r;
    r.v[0] = fclampf(x.v[0], min_val, max_val);
    r.v[1] = fclampf(x.v[1], min_val, max_val);
    r.v[2] = fclampf(x.v[2], min_val, max_val);
    return r;
}

vec3 vec3_clampv(vec3 x, vec3 min_val, vec3 max_val) {
    vec3 r;
    r.v[0] = fclampf(x.v[0], min_val.v[0], max_val.v[0]);
    r.v[1] = fclampf(x.v[1], min_val.v[1], max_val.v[1]);
    r.v[2] = fclampf(x.v[2], min_val.v[2], max_val.v[2]);
    return r;
}

float vec3_length(vec3 x) {
    return sqrtf(vec3_length2(x));
}

float vec3_length2(vec3 x) {
    return x.v[0] * x.v[0] + x.v[1] * x.v[1] + x.v[2] * x.v[2];
}

float vec3_distance(vec3 x, vec3 y) {
    return sqrtf(vec3_distance2(x, y));
}

float vec3_distance2(vec3 x, vec3 y) {
    return vec3_length2(vec3_sub(x, y));
}

float vec3_dot(vec3 x, vec3 y) {
    return x.v[0] * y.v[0] + x.v[1] * y.v[1] + x.v[2] * y.v[2];
}

vec3 vec3_normalize(vec3 x) {
    return vec3_scale(x, 1.0f / vec3_length(x));
}

vec3 vec3_mix(vec3 x, vec3 y, float a) {
    vec3 r;
    r.v[0] = x.v[0] + a * (y.v[0] - x.v[0]);
    r.v[1] = x.v[1] + a * (y.v[1] - x.v[1]);
    r.v[2] = x.v[2] + a * (y.v[2] - x.v[2]);
    return r;
}
