/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/vecmath.h"

bool ivec3_eq(ivec3 x, ivec3 y) {
    return x.v[0] == y.v[0] && x.v[1] == y.v[1] && x.v[2] == y.v[2];
}

ivec3 ivec3_add(ivec3 x, ivec3 y) {
    ivec3 r;
    r.v[0] = x.v[0] + y.v[0];
    r.v[1] = x.v[1] + y.v[1];
    r.v[2] = x.v[2] + y.v[2];
    return r;
}

ivec3 ivec3_sub(ivec3 x, ivec3 y) {
    ivec3 r;
    r.v[0] = x.v[0] - y.v[0];
    r.v[1] = x.v[1] - y.v[1];
    r.v[2] = x.v[2] - y.v[2];
    return r;
}

ivec3 ivec3_neg(ivec3 x) {
    ivec3 r;
    r.v[0] = -x.v[0];
    r.v[1] = -x.v[1];
    r.v[2] = -x.v[2];
    return r;
}

ivec3 ivec3_mul(ivec3 x, ivec3 y) {
    ivec3 r;
    r.v[0] = x.v[0] * y.v[0];
    r.v[1] = x.v[1] * y.v[1];
    r.v[2] = x.v[2] * y.v[2];
    return r;
}

ivec3 ivec3_scale(ivec3 x, int a) {
    ivec3 r;
    r.v[0] = x.v[0] * a;
    r.v[1] = x.v[1] * a;
    r.v[2] = x.v[2] * a;
    return r;
}

ivec3 ivec3_min(ivec3 x, ivec3 y) {
    ivec3 r;
    r.v[0] = imin(x.v[0], y.v[0]);
    r.v[1] = imin(x.v[1], y.v[1]);
    r.v[2] = imin(x.v[2], y.v[2]);
    return r;
}

ivec3 ivec3_max(ivec3 x, ivec3 y) {
    ivec3 r;
    r.v[0] = imax(x.v[0], y.v[0]);
    r.v[1] = imax(x.v[1], y.v[1]);
    r.v[2] = imax(x.v[2], y.v[2]);
    return r;
}

ivec3 ivec3_clamp(ivec3 x, int min_val, int max_val) {
    ivec3 r;
    r.v[0] = iclamp(x.v[0], min_val, max_val);
    r.v[1] = iclamp(x.v[1], min_val, max_val);
    r.v[2] = iclamp(x.v[2], min_val, max_val);
    return r;
}

ivec3 ivec3_clampv(ivec3 x, ivec3 min_val, ivec3 max_val) {
    ivec3 r;
    r.v[0] = iclamp(x.v[0], min_val.v[0], max_val.v[0]);
    r.v[1] = iclamp(x.v[1], min_val.v[1], max_val.v[1]);
    r.v[2] = iclamp(x.v[2], min_val.v[2], max_val.v[2]);
    return r;
}

int ivec3_length2(ivec3 x) {
    return x.v[0] * x.v[0] + x.v[1] * x.v[1] + x.v[2] * x.v[2];
}

int ivec3_distance2(ivec3 x, ivec3 y) {
    return ivec3_length2(ivec3_sub(x, y));
}
