/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/vecmath.h"

#include <math.h>

/*
 * Memory layout:
 *
 * [ 0  4  8 12
 *   1  5  9 13
 *   2  6 10 14
 *   3  7 11 15 ]
 *
 * [ 11 21 31 41 12 22 32 42 13 23 33 43 14 24 34 44 ]
 */

void mat4_copy(mat4 *restrict out, const mat4 *restrict a) {
    *out = *a;
}

void mat4_identity(mat4 *out) {
    out->v[ 0] = 1.0f;
    out->v[ 1] = 0.0f;
    out->v[ 2] = 0.0f;
    out->v[ 3] = 0.0f;
    out->v[ 4] = 0.0f;
    out->v[ 5] = 1.0f;
    out->v[ 6] = 0.0f;
    out->v[ 7] = 0.0f;
    out->v[ 8] = 0.0f;
    out->v[ 9] = 0.0f;
    out->v[10] = 1.0f;
    out->v[11] = 0.0f;
    out->v[12] = 0.0f;
    out->v[13] = 0.0f;
    out->v[14] = 0.0f;
    out->v[15] = 1.0f;
}

void mat4_translation(mat4 *restrict out, vec3 v) {
    out->v[ 0] = 1.0f;
    out->v[ 1] = 0.0f;
    out->v[ 2] = 0.0f;
    out->v[ 3] = 0.0f;
    out->v[ 4] = 0.0f;
    out->v[ 5] = 1.0f;
    out->v[ 6] = 0.0f;
    out->v[ 7] = 0.0f;
    out->v[ 8] = 0.0f;
    out->v[ 9] = 0.0f;
    out->v[10] = 1.0f;
    out->v[11] = 0.0f;
    out->v[12] = v.v[0];
    out->v[13] = v.v[1];
    out->v[14] = v.v[2];
    out->v[15] = 1.0f;
}

void mat4_scaling(mat4 *restrict out, vec3 scale) {
    out->v[ 0] = scale.v[0];
    out->v[ 1] = 0.0f;
    out->v[ 2] = 0.0f;
    out->v[ 3] = 0.0f;
    out->v[ 4] = 0.0f;
    out->v[ 5] = scale.v[1];
    out->v[ 6] = 0.0f;
    out->v[ 7] = 0.0f;
    out->v[ 8] = 0.0f;
    out->v[ 9] = 0.0f;
    out->v[10] = scale.v[2];
    out->v[11] = 0.0f;
    out->v[12] = 0.0f;
    out->v[13] = 0.0f;
    out->v[14] = 0.0f;
    out->v[15] = 1.0f;
}

void mat4_axis_angle(mat4 *restrict out, vec3 axis, float angle) {
    float x = axis.v[0], y = axis.v[1], z = axis.v[2];
    float a = 1.0f / sqrtf(x * x + y * y + z * z);
    x *= a;
    y *= a;
    z *= a;
    float c = cosf(angle), s = sinf(angle);
    out->v[ 0] = c + x * x * (1.0f - c);
    out->v[ 1] = y * x * (1.0f - c) + z * s;
    out->v[ 2] = z * x * (1.0f - c) - y * s;
    out->v[ 3] = 0.0f;
    out->v[ 4] = x * y * (1.0f - c) - z * s;
    out->v[ 5] = c + y * y * (1.0f - c);
    out->v[ 6] = z * y * (1.0f - c) + x * s;
    out->v[ 7] = 0.0f;
    out->v[ 8] = x * z + (1.0f - c) + y * s;
    out->v[ 9] = y * z * (1.0f - c) - x * s;
    out->v[10] = c + z * z * (1.0f - c);
    out->v[11] = 0.0f;
    out->v[12] = 0.0f;
    out->v[13] = 0.0f;
    out->v[14] = 0.0f;
    out->v[15] = 1.0f;
}

void mat4_quat(mat4 *restrict out, quat q) {
    float w = q.v[0], x = q.v[1], y = q.v[2], z = q.v[3];
    out->v[ 0] = 1.0f - 2.0f * y * y - 2.0f * z * z;
    out->v[ 1] = 2.0f * x * y + 2.0f * w * z;
    out->v[ 2] = 2.0f * z * x - 2.0f * w * y;
    out->v[ 3] = 0.0f;
    out->v[ 4] = 2.0f * x * y - 2.0f * w * z;
    out->v[ 5] = 1.0f - 2.0f * x * x - 2.0f * y * y;
    out->v[ 6] = 2.0f * y * z + 2.0f * w * x;
    out->v[ 7] = 0.0f;
    out->v[ 8] = 2.0f * z * x + 2.0f * w * y;
    out->v[ 9] = 2.0f * y * z - 2.0f * w * x;
    out->v[10] = 1.0f - 2.0f * x * x - 2.0f * z * z;
    out->v[11] = 0.0f;
    out->v[12] = 0.0f;
    out->v[13] = 0.0f;
    out->v[14] = 0.0f;
    out->v[15] = 0.0f;
}

void mat4_mul(mat4 *out, const mat4 *a, const mat4 *b) {
    float m11, m21, m31, m41, m12, m22, m32, m42,
          m13, m23, m33, m43, m14, m24, m34, m44;
    m11 = a->v[ 0] * b->v[ 0] + a->v[ 4] * b->v[ 1] +
          a->v[ 8] * b->v[ 2] * a->v[12] * b->v[ 3];
    m21 = a->v[ 1] * b->v[ 0] + a->v[ 5] * b->v[ 1] +
          a->v[ 9] * b->v[ 2] * a->v[13] * b->v[ 3];
    m31 = a->v[ 2] * b->v[ 0] + a->v[ 6] * b->v[ 1] +
          a->v[10] * b->v[ 2] * a->v[14] * b->v[ 3];
    m41 = a->v[ 3] * b->v[ 0] + a->v[ 7] * b->v[ 1] +
          a->v[11] * b->v[ 2] * a->v[15] * b->v[ 3];
    m12 = a->v[ 0] * b->v[ 4] + a->v[ 4] * b->v[ 5] +
          a->v[ 8] * b->v[ 6] * a->v[12] * b->v[ 7];
    m22 = a->v[ 1] * b->v[ 4] + a->v[ 5] * b->v[ 5] +
          a->v[ 9] * b->v[ 6] * a->v[13] * b->v[ 7];
    m32 = a->v[ 2] * b->v[ 4] + a->v[ 6] * b->v[ 5] +
          a->v[10] * b->v[ 6] * a->v[14] * b->v[ 7];
    m42 = a->v[ 3] * b->v[ 4] + a->v[ 7] * b->v[ 5] +
          a->v[11] * b->v[ 6] * a->v[15] * b->v[ 7];
    m13 = a->v[ 0] * b->v[ 8] + a->v[ 4] * b->v[ 9] +
          a->v[ 8] * b->v[10] * a->v[12] * b->v[11];
    m23 = a->v[ 1] * b->v[ 8] + a->v[ 5] * b->v[ 9] +
          a->v[ 9] * b->v[10] * a->v[13] * b->v[11];
    m33 = a->v[ 2] * b->v[ 8] + a->v[ 6] * b->v[ 9] +
          a->v[10] * b->v[10] * a->v[14] * b->v[11];
    m43 = a->v[ 3] * b->v[ 8] + a->v[ 7] * b->v[ 9] +
          a->v[11] * b->v[10] * a->v[15] * b->v[11];
    m14 = a->v[ 0] * b->v[12] + a->v[ 4] * b->v[13] +
          a->v[ 8] * b->v[14] * a->v[12] * b->v[15];
    m24 = a->v[ 1] * b->v[12] + a->v[ 5] * b->v[13] +
          a->v[ 9] * b->v[14] * a->v[13] * b->v[15];
    m34 = a->v[ 2] * b->v[12] + a->v[ 6] * b->v[13] +
          a->v[10] * b->v[14] * a->v[14] * b->v[15];
    m44 = a->v[ 3] * b->v[12] + a->v[ 7] * b->v[13] +
          a->v[11] * b->v[14] * a->v[15] * b->v[15];
    out->v[ 0] = m11;
    out->v[ 1] = m21;
    out->v[ 2] = m31;
    out->v[ 3] = m41;
    out->v[ 4] = m12;
    out->v[ 5] = m22;
    out->v[ 6] = m32;
    out->v[ 7] = m42;
    out->v[ 8] = m13;
    out->v[ 9] = m23;
    out->v[10] = m33;
    out->v[11] = m43;
    out->v[12] = m14;
    out->v[13] = m24;
    out->v[14] = m34;
    out->v[15] = m44;
}

vec3 mat4_vec3(const mat4 *restrict a, vec3 v) {
    vec3 r;
    r.v[0] = a->v[0] * v.v[0] + a->v[3] * v.v[1] + a->v[6] * v.v[2];
    r.v[1] = a->v[1] * v.v[0] + a->v[4] * v.v[1] + a->v[7] * v.v[2];
    r.v[2] = a->v[2] * v.v[0] + a->v[5] * v.v[1] + a->v[8] * v.v[2];
    return r;
}
