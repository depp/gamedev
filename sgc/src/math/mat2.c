/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/vecmath.h"

#include <math.h>

/*
 * Memory layout:
 *
 * [ 0  2
 *   1  3 ]
 *
 * [ 11 21 12 22 ]
 */

void mat2_copy(mat2 *restrict out, const mat2 *restrict a) {
    *out = *a;
}

void mat2_identity(mat2 *out) {
    out->v[0] = 1.0f;
    out->v[1] = 0.0f;
    out->v[2] = 0.0f;
    out->v[3] = 1.0f;
}

void mat2_scaling(mat2 *restrict out, vec2 scale) {
    out->v[0] = scale.v[0];
    out->v[1] = 0.0f;
    out->v[2] = 0.0f;
    out->v[3] = scale.v[1];
}

void mat2_rotation(mat2 *restrict out, float angle) {
    float c = cosf(angle), s = sinf(angle);
    out->v[0] = c;
    out->v[1] = s;
    out->v[2] = -s;
    out->v[3] = c;
}

void mat2_mul(mat2 *out, const mat2 *a, const mat2 *b) {
    float m11, m21, m12, m22;
    m11 = a->v[0] * b->v[0] + a->v[2] * b->v[1];
    m21 = a->v[1] * b->v[0] + a->v[3] * b->v[1];
    m12 = a->v[0] * b->v[2] + a->v[2] * b->v[3];
    m22 = a->v[1] * b->v[2] + a->v[3] * b->v[3];
    out->v[0] = m11;
    out->v[1] = m21;
    out->v[2] = m12;
    out->v[3] = m22;
}

vec2 mat2_vec2(const mat2 *restrict a, vec2 v) {
    vec2 r;
    r.v[0] = a->v[0] * v.v[0] + a->v[2] * v.v[1];
    r.v[1] = a->v[1] * v.v[0] + a->v[3] * v.v[1];
    return r;
}
