/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/vecmath.h"

#include <math.h>

bool vec4_eq(vec4 x, vec4 y) {
    return x.v[0] == y.v[0] && x.v[1] == y.v[1] && x.v[2] == y.v[2] &&
           x.v[3] == y.v[3];
}

vec4 vec4_add(vec4 x, vec4 y) {
    vec4 r;
    r.v[0] = x.v[0] + y.v[0];
    r.v[1] = x.v[1] + y.v[1];
    r.v[2] = x.v[2] + y.v[2];
    r.v[3] = x.v[3] + y.v[3];
    return r;
}

vec4 vec4_sub(vec4 x, vec4 y) {
    vec4 r;
    r.v[0] = x.v[0] - y.v[0];
    r.v[1] = x.v[1] - y.v[1];
    r.v[2] = x.v[2] - y.v[2];
    r.v[3] = x.v[3] - y.v[3];
    return r;
}

vec4 vec4_neg(vec4 x) {
    vec4 r;
    r.v[0] = -x.v[0];
    r.v[1] = -x.v[1];
    r.v[2] = -x.v[2];
    r.v[3] = -x.v[3];
    return r;
}

vec4 vec4_mul(vec4 x, vec4 y) {
    vec4 r;
    r.v[0] = x.v[0] * y.v[0];
    r.v[1] = x.v[1] * y.v[1];
    r.v[2] = x.v[2] * y.v[2];
    r.v[3] = x.v[3] * y.v[3];
    return r;
}

vec4 vec4_scale(vec4 x, float a) {
    vec4 r;
    r.v[0] = x.v[0] * a;
    r.v[1] = x.v[1] * a;
    r.v[2] = x.v[2] * a;
    r.v[3] = x.v[3] * a;
    return r;
}

vec4 vec4_min(vec4 x, vec4 y) {
    vec4 r;
    r.v[0] = fminf(x.v[0], y.v[0]);
    r.v[1] = fminf(x.v[1], y.v[1]);
    r.v[2] = fminf(x.v[2], y.v[2]);
    r.v[3] = fminf(x.v[3], y.v[3]);
    return r;
}

vec4 vec4_max(vec4 x, vec4 y) {
    vec4 r;
    r.v[0] = fmaxf(x.v[0], y.v[0]);
    r.v[1] = fmaxf(x.v[1], y.v[1]);
    r.v[2] = fmaxf(x.v[2], y.v[2]);
    r.v[3] = fmaxf(x.v[3], y.v[3]);
    return r;
}

vec4 vec4_clamp(vec4 x, float min_val, float max_val) {
    vec4 r;
    r.v[0] = fclampf(x.v[0], min_val, max_val);
    r.v[1] = fclampf(x.v[1], min_val, max_val);
    r.v[2] = fclampf(x.v[2], min_val, max_val);
    r.v[3] = fclampf(x.v[3], min_val, max_val);
    return r;
}

vec4 vec4_clampv(vec4 x, vec4 min_val, vec4 max_val) {
    vec4 r;
    r.v[0] = fclampf(x.v[0], min_val.v[0], max_val.v[0]);
    r.v[1] = fclampf(x.v[1], min_val.v[1], max_val.v[1]);
    r.v[2] = fclampf(x.v[2], min_val.v[2], max_val.v[2]);
    r.v[3] = fclampf(x.v[3], min_val.v[3], max_val.v[3]);
    return r;
}

float vec4_length(vec4 x) {
    return sqrtf(vec4_length2(x));
}

float vec4_length2(vec4 x) {
    return x.v[0] * x.v[0] + x.v[1] * x.v[1] + x.v[2] * x.v[2] +
           x.v[3] * x.v[3];
}

float vec4_distance(vec4 x, vec4 y) {
    return sqrtf(vec4_distance2(x, y));
}

float vec4_distance2(vec4 x, vec4 y) {
    return vec4_length2(vec4_sub(x, y));
}

float vec4_dot(vec4 x, vec4 y) {
    return x.v[0] * y.v[0] + x.v[1] * y.v[1] + x.v[2] * y.v[2] +
           x.v[3] * y.v[3];
}

vec4 vec4_normalize(vec4 x) {
    return vec4_scale(x, 1.0f / vec4_length(x));
}

vec4 vec4_mix(vec4 x, vec4 y, float a) {
    vec4 r;
    r.v[0] = x.v[0] + a * (y.v[0] - x.v[0]);
    r.v[1] = x.v[1] + a * (y.v[1] - x.v[1]);
    r.v[2] = x.v[2] + a * (y.v[2] - x.v[2]);
    r.v[3] = x.v[3] + a * (y.v[3] - x.v[3]);
    return r;
}
