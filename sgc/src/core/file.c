/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "file_impl.h"

#include "sg/data.h"
#include "sg/error.h"
#include "sg/file.h"

#include <stdlib.h>
/* FIXME: Necessary? */
#include <string.h>

int sg_reader_read_data(struct sg_reader *rp, struct sg_data *data, size_t size,
                        struct sg_error **err) {
    char *buf;
    size_t pos;
    int r;
    int (*read)(struct sg_reader *, void *, size_t, struct sg_error **) =
        rp->read_buf;
    if (!size) {
        data->ptr = NULL;
        data->size = 0;
        data->owner = NULL;
        return 0;
    }
    buf = malloc(size);
    if (!buf) {
        sg_error_nomem(err);
        return -1;
    }
    pos = 0;
    while (pos < size) {
        r = read(rp, buf + pos, size - pos, err);
        if (r > 0) {
            pos += r;
        } else if (r == 0) {
            buf = realloc(buf, pos);
            break;
        } else {
            free(buf);
            return -1;
        }
    }
    r = sg_data_from_alloc(data, buf, pos, err);
    if (r) {
        free(buf);
        return -1;
    }
    return 0;
}

void sg_fileinfo_init(struct sg_fileinfo *restrict d) {
    d->size = 0;
    d->mtime = 0;
    d->path = NULL;
}

void sg_fileinfo_destroy(struct sg_fileinfo *restrict d) {
    free(d->path);
}

/* FIXME: Delete this function, remove all callers.  */
void sg_fileinfo_copy(struct sg_fileinfo *restrict d,
                      const struct sg_fileinfo *restrict s) {
    char *path;
    size_t len;
    if (s->path) {
        len = strlen(s->path);
        path = malloc(len + 1);
        if (!path) abort();
        memcpy(path, s->path, len + 1);
    }
    *d = *s;
    d->path = path;
}

int sg_load(struct sg_data *restrict data, struct sg_fileinfo *restrict info,
            const char *restrict path, const char *restrict extensions,
            size_t max_size, struct sg_error **err) {
    int r = sg_load_if_changed(data, info, path, extensions, max_size, 0, err);
    return r >= 0 ? 0 : -1;
}

int sg_load_if_changed(struct sg_data *restrict data,
                       struct sg_fileinfo *restrict info,
                       const char *restrict path,
                       const char *restrict extensions, size_t max_size,
                       int64_t mtime, struct sg_error **err) {
    struct sg_reader *rp;
    int r;
    rp = sg_reader_open(path, extensions, err);
    if (!rp) return -1;
    if (mtime != 0 && mtime == rp->info.mtime) {
        rp->close(rp);
        return 0;
    }
    uint64_t size = rp->info.size;
    if (size > max_size) {
        rp->close(rp);
        sg_error_set(err, SG_ERR_TOOBIG);
        return 0;
    }
    r = rp->read_data(rp, data, size, err);
    if (r) {
        rp->close(rp);
        return -1;
    }
    /* We steal rp->info, which is okay.  */
    if (info) {
        *info = rp->info;
        sg_fileinfo_init(&rp->info);
    }
    rp->close(rp);
    return 1;
}
