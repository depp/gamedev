/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "private.h"
#include "../private.h"
#include "sg/clock.h"
#include "sg/entry.h"
#include "sg/event.h"
#include "sg/log.h"
#include "sggl/common.h"

struct sg_sdl sg_sdl;

static void
sg_sdl_create_window(const struct sg_game_info *gi)
{
    unsigned flags, glflags;
    int width, height, r, i, version, minor, major;

    /* Get window parameters */
    flags = SDL_WINDOW_OPENGL;
    if (sg_sys.hidpi.value &&
        (gi->flags & SG_GAME_ALLOW_HIDPI) != 0) {
        flags |= SDL_WINDOW_ALLOW_HIGHDPI;
    }
    if ((gi->flags & SG_GAME_ALLOW_RESIZE) != 0) {
        flags |= SDL_WINDOW_RESIZABLE;
    }
    sg_sys.vidsize.flags &= ~SG_CVAR_MODIFIED;
    r = sg_sys_getvidsize(&width, &height);
    if (r) {
        width = gi->default_width;
        height = gi->default_height;
    }
    sg_sdl.window = SDL_CreateWindow(
        gi->name,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        width, height, flags);
    if (!sg_sdl.window)
        sg_sdl_error("Could not open window.");

    /* Loop through OpenGL versions until we get one we like.  */
    glflags = 0;
    if (sg_cvar_developer.value)
        glflags |= SDL_GL_CONTEXT_DEBUG_FLAG;
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(
        SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    if ((gi->flags & SG_GAME_FRAMEBUFFER_SRGB) != 0)
        SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 1);
    if (glflags)
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, glflags);
    for (i = SG_GL_VERSIONCOUNT - 1; i >= 0; i--) {
        version = SG_GL_VERSIONS[i];
        if (version < gi->opengl_min_version || version < 0x32)
            break;
        major = version >> 4;
        minor = version & 15;
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor);

        sg_sdl.context = SDL_GL_CreateContext(sg_sdl.window);
        if (sg_sdl.context)
            break;
        /*
        sg_logf(SG_LOG_DEBUG, "Failed to create %d.%d context.",
                major, minor);
        */
    }

    if (!sg_sdl.context)
        sg_sdl_error("Could not create OpenGL context.");
}

static void
sg_sdl_init(int argc, char *argv[])
{
    union sg_event ee;
    struct sg_game_info gi;
    unsigned flags;

    flags = SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS;
    if (SDL_Init(flags))
        sg_sdl_error("could not initialize LibSDL");

    sg_sys_init(&gi, argc - 1, argv + 1);
    sg_sdl_create_window(&gi);
    sg_gl_init();
    sg_sdl_update_vsync();

    ee.common.time = sg_clock_get();
    ee.common.type = SG_EVENT_VINIT;
    sg_game_event(&ee);
}

int
main(int argc, char *argv[])
{
    int width, height;
    int last_time = SDL_GetTicks(), new_time, maxfps;
    sg_sdl_init(argc, argv);

    while (sg_sdl_handle_events()) {
        if ((sg_sys.vidsize.flags & SG_CVAR_MODIFIED) != 0) {
            int nwidth, nheight;
            sg_sys.vidsize.flags &= ~SG_CVAR_MODIFIED;
            sg_sys_getvidsize(&nwidth, &nheight);
            SDL_SetWindowSize(sg_sdl.window, nwidth, nheight);
        }

        SDL_GL_GetDrawableSize(sg_sdl.window, &width, &height);
        sg_sys_draw(width, height, sg_clock_get());
        SDL_GL_SwapWindow(sg_sdl.window);
        sg_sys_postdraw();

        new_time = SDL_GetTicks();
        maxfps = sg_sys.maxfps.value;
        if (maxfps > 0 && (new_time - last_time) * maxfps < 1000) {
            int delay;
            delay = 1000 / maxfps - (new_time - last_time);
            if (delay > 0)
                SDL_Delay(delay);
        }
        last_time = new_time;
    }

    SDL_DestroyWindow(sg_sdl.window);
    sg_game_destroy();
    return 0;
}
