/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "private.h"
#include "../clock_impl.h"
#include "sg/entry.h"
#include "sg/event.h"
#include "sg/log.h"

static void
sg_sdl_event_mousemove(SDL_MouseMotionEvent *e)
{
    union sg_event ee;
    int width, height;
    ee.mouse.time = sg_clock_convert_sdl(e->timestamp);
    if (sg_sdl.have_capture) {
        ee.mouse.type = SG_EVENT_MREL;
        ee.mouse.button = -1;
        ee.mouse.x = e->xrel;
        ee.mouse.y = -e->yrel;
    } else {
        SDL_GetWindowSize(sg_sdl.window, &width, &height);
        ee.mouse.type = SG_EVENT_MABS;
        ee.mouse.button = -1;
        ee.mouse.x = e->x;
        ee.mouse.y = height - 1 - e->y;
    }
    sg_game_event(&ee);
}

static void
sg_sdl_event_mousebutton(SDL_MouseButtonEvent *e)
{
    union sg_event ee;
    int width, height;
    SDL_GetWindowSize(sg_sdl.window, &width, &height);
    ee.mouse.time = sg_clock_convert_sdl(e->timestamp);
    ee.mouse.type = e->type == SDL_MOUSEBUTTONDOWN ?
        SG_EVENT_MDOWN : SG_EVENT_MUP;
    switch (e->button) {
    case SDL_BUTTON_LEFT:   ee.mouse.button = SG_BUTTON_LEFT;   break;
    case SDL_BUTTON_MIDDLE: ee.mouse.button = SG_BUTTON_MIDDLE; break;
    case SDL_BUTTON_RIGHT:  ee.mouse.button = SG_BUTTON_RIGHT;  break;
    default:
        ee.mouse.button = SG_BUTTON_OTHER + e->button - 4;
        break;
    }
    ee.mouse.x = e->x;
    ee.mouse.y = height - 1 - e->y;
    sg_game_event(&ee);
}

static void
sg_sdl_event_key(SDL_KeyboardEvent *e)
{
    union sg_event ee;
    ee.key.time = sg_clock_convert_sdl(e->timestamp);
    if (e->type == SDL_KEYDOWN)
        ee.key.type = e->repeat ? SG_EVENT_KREPEAT : SG_EVENT_KDOWN;
    else
        ee.key.type = SG_EVENT_KUP;
    /* SDL scancodes are just HID codes, which is what we use.  */
    ee.key.key = e->keysym.scancode;
    sg_game_event(&ee);
}

#if 0

struct sg_sdl_event_wtype {
    short type;
    char name[14];
};

#define TYPE(x) { SDL_WINDOWEVENT_ ## x, #x }
static const struct sg_sdl_event_wtype SG_SDL_EVENT_WTYPE[] = {
    TYPE(SHOWN),
    TYPE(HIDDEN),
    TYPE(EXPOSED),
    TYPE(MOVED),
    TYPE(RESIZED),
    TYPE(MINIMIZED),
    TYPE(MAXIMIZED),
    TYPE(RESTORED),
    TYPE(ENTER),
    TYPE(LEAVE),
    TYPE(FOCUS_GAINED),
    TYPE(FOCUS_LOST),
    TYPE(CLOSE)
};
#undef TYPE

static void
sg_sdl_event_wtype_showname(int wtype)
{
    const struct sg_sdl_event_wtype *tp = SG_SDL_EVENT_WTYPE,
        *te = tp + sizeof(SG_SDL_EVENT_WTYPE) / sizeof(*SG_SDL_EVENT_WTYPE);
    for (; tp != te; tp++) {
        if (tp->type == wtype) {
            sg_logf(SG_LOG_DEBUG,
                    "Window event: %s", tp->name);
            return;
        }
    }
    sg_logf(SG_LOG_DEBUG,
            "Window event: unknown (%d)", wtype);
}

#else

#define sg_sdl_event_wtype_showname(x) (void) 0

#endif

static void
sg_sdl_event_window(SDL_WindowEvent *e)
{
    union sg_event ee;
    unsigned oldstatus, newstatus;
    sg_sdl_event_wtype_showname(e->event);
    oldstatus = newstatus = sg_sdl.window_status;

    switch (e->event) {
    case SDL_WINDOWEVENT_SHOWN:
        newstatus |= SG_WSTATUS_VISIBLE;
        break;

    case SDL_WINDOWEVENT_HIDDEN:
        newstatus &= ~SG_WSTATUS_VISIBLE;
        break;

    case SDL_WINDOWEVENT_FOCUS_GAINED:
        newstatus |= SG_WSTATUS_FOCUSED;
        break;

    case SDL_WINDOWEVENT_FOCUS_LOST:
        newstatus &= ~SG_WSTATUS_FOCUSED;
        break;

    default:
        break;
    }

    if (oldstatus == newstatus)
        return;

    sg_sdl.window_status = newstatus;
    sg_sdl_update_capture();
    ee.status.time = sg_clock_convert_sdl(e->timestamp);
    ee.status.type = SG_EVENT_WSTATUS;
    ee.status.status = newstatus;
    sg_game_event(&ee);
}

int
sg_sdl_handle_events(void)
{
    SDL_Event e;
    SDL_PumpEvents();

    while (SDL_PollEvent(&e)) {
        switch (e.type) {
        case SDL_QUIT:
            return 0;

        case SDL_MOUSEMOTION:
            // printf("MOUSEMOVE\n");
            sg_sdl_event_mousemove(&e.motion);
            break;

        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
            sg_sdl_event_mousebutton(&e.button);
            break;

        case SDL_KEYDOWN:
        case SDL_KEYUP:
            sg_sdl_event_key(&e.key);
            break;

        case SDL_WINDOWEVENT:
            sg_sdl_event_window(&e.window);
            break;

        default:
            // printf("Unknown event: %u\n", e.type);
            break;
        }
    }

    return 1;
}
