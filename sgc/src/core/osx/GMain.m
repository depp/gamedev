/* Copyright 2012 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include <AppKit/AppKit.h>

int main(int argc, const char *argv[])
{
    return NSApplicationMain(argc, argv);
}
