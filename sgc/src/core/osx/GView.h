/* Copyright 2012 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

#import <Cocoa/Cocoa.h>
@class GDisplay;

@interface GView : NSView {
@public
    GDisplay *display_;
}

@end
