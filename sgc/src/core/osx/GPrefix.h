/* Copyright 2012 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

#if defined(__OBJC__)
#include <Cocoa/Cocoa.h>
#endif

#define HAVE_COREGRAPHICS 1
#define HAVE_CORETEXT 1
