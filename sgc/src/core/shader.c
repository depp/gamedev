/* Copyright 2013-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/data.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/log.h"
#include "sg/opengl.h"
#include "sg/shader.h"
#include "sggl/2_0.h"

#include <stdlib.h>

#define SG_SHADER_MAXSIZE (1024 * 64)

#if 0

static const struct {
    char ext[5];
    GLenum type;
} SG_SHADER_EXTENSION[] = {
    { "comp", 0x91B9 /* GL_COMPUTE_SHADER */ },
    { "vert", 0x8B31 /* GL_VERTEX_SHADER */ },
    { "tesc", 0x8E88 /* GL_TESS_CONTROL_SHADER */ },
    { "tese", 0x8E87 /* GL_TESS_EVALUATION_SHADER */ },
    { "geom", 0x8DD9 /* GL_GEOMETRY_SHADER */ },
    { "frag", 0x8B30 /* GL_FRAGMENT_SHADER */ }
};

const char *
sg_shader_file_extension(GLenum type)
{
    int i, n = sizeof(SG_SHADER_EXTENSION) / sizeof(*SG_SHADER_EXTENSION);
    for (i = 0; i < n; i++)
        if (SG_SHADER_EXTENSION[i].type == type)
            return SG_SHADER_EXTENSION[i].ext;
    return NULL;
}

#endif

GLuint sg_shader_file(const char *path, GLenum type, struct sg_error **err) {
    struct sg_data data;
    int r;
    GLuint shader;
    r = sg_load(&data, NULL, path, NULL, SG_SHADER_MAXSIZE, err);
    if (r) return 0;
    shader = sg_shader_buffer(data.ptr, data.size, type, err);
    sg_data_destroy(&data);
    return shader;
}

GLuint sg_shader_buffer(const void *data, size_t size, GLenum type,
                        struct sg_error **err) {
    const char *src[1];
    char *log;
    GLuint shader;
    GLint srclen[1], flag, loglen;

    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "sg_shader_buffer");

    shader = glCreateShader(type);
    if (!shader) {
        sg_error_opengl(err);
        goto done;
    }
    src[0] = data;
    srclen[0] = size;
    glShaderSource(shader, 1, src, srclen);
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &flag);
    if (!flag) {
        sg_logf(SG_LOG_ERROR, "Compilation failed.");
    }
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &loglen);
    if (loglen > 1) {
        log = malloc(loglen + 1);
        if (!log) {
            sg_error_nomem(err);
            glDeleteShader(shader);
            shader = 0;
            goto done;
        }
        glGetShaderInfoLog(shader, loglen, NULL, log);
        log[loglen] = '\0';
        sg_logs(SG_LOG_WARN, log);
        free(log);
    }
    if (!flag) {
        sg_error_opengl(err);
        glDeleteShader(shader);
        shader = 0;
    }

done:
    sg_gl_debugf.pop();
    return shader;
}

int sg_shader_link(GLuint prog, const char *name,
                   const struct sg_shader_loc *attrib, struct sg_error **err) {
    char *log;
    GLint flag, loglen;
    int retval;

    if (attrib) {
        for (int i = 0; attrib[i].name; i++) {
            glBindAttribLocation(prog, attrib[i].offset, attrib[i].name);
        }
    }

    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "sg_shader_link");

    glLinkProgram(prog);
    glGetProgramiv(prog, GL_LINK_STATUS, &flag);
    if (!flag) {
        sg_logf(SG_LOG_ERROR, "%s: Linking failed.", name);
    }
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &loglen);
    if (loglen > 1) {
        log = malloc(loglen + 1);
        if (!log) {
            sg_error_nomem(err);
            retval = -1;
            goto done;
        }
        glGetProgramInfoLog(prog, loglen, NULL, log);
        log[loglen] = '\0';
        sg_logs(SG_LOG_WARN, log);
        free(log);
    }
    retval = flag ? 0 : -1;

done:
    sg_gl_debugf.pop();
    return retval;
}

void sg_shader_getuniform(GLuint prog, const char *name, void *obj,
                          const struct sg_shader_loc *uniform) {
    (void)name;
    for (int i = 0; uniform[i].name; i++) {
        GLint uloc = glGetUniformLocation(prog, uniform[i].name);
        GLint *ulocp = (void *)((char *)obj + uniform[i].offset);
        *ulocp = uloc;
    }
}
