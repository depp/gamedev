/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/data.h"
#include "sg/data_owner.h"
#include "sg/error.h"

#include <stdlib.h>

void sg_data_owner_incref(struct sg_data_owner *p) {
    if (!p) return;
    sg_atomic_inc(&p->refcount);
}

void sg_data_owner_decref(struct sg_data_owner *p) {
    if (!p) return;
    if (sg_atomic_fetch_add_release(&p->refcount, -1) == 1) {
        p->free(p);
    }
}

void sg_data_init(struct sg_data *restrict d) {
    d->ptr = NULL;
    d->size = 0;
    d->owner = NULL;
}

void sg_data_destroy(struct sg_data *restrict d) {
    sg_data_owner_decref(d->owner);
}

void sg_data_copy(struct sg_data *restrict d,
                  const struct sg_data *restrict s) {
    *d = *s;
    sg_data_owner_incref(d->owner);
}

struct sg_data_alloc {
    struct sg_data_owner obj;
    void *ptr;
};

static void sg_data_alloc_free(struct sg_data_owner *owner) {
    struct sg_data_alloc *obj = (struct sg_data_alloc *)owner;
    free(obj->ptr);
    free(obj);
}

int sg_data_from_alloc(struct sg_data *restrict d, void *ptr, size_t size,
                       struct sg_error **err) {
    struct sg_data_alloc *obj;
    if (!ptr) {
        d->ptr = NULL;
        d->size = 0;
        d->owner = NULL;
        return 0;
    }
    obj = malloc(sizeof(*obj));
    if (!obj) {
        sg_error_nomem(err);
        return -1;
    }
    sg_atomic_set(&obj->obj.refcount, 1);
    obj->obj.free = sg_data_alloc_free;
    obj->ptr = ptr;
    d->ptr  = ptr;
    d->size = size;
    d->owner = &obj->obj;
    return 0;
}
