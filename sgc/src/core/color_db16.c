/* Copyright 2014, 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "sg/color.h"

// DawnBringer's 16-color palette.
const unsigned char PAL_DB16[16][3] = {
    {  20,  12,  28 },
    {  68,  36,  52 },
    {  48,  52, 109 },
    {  78,  74,  78 },
    { 133,  76,  48 },
    {  52, 101,  36 },
    { 208,  70,  72 },
    { 117, 113,  97 },
    {  89, 125, 206 },
    { 210, 125,  44 },
    { 133, 149, 161 },
    { 109, 170,  44 },
    { 210, 170, 153 },
    { 109, 194, 202 },
    { 218, 212,  94 },
    { 222, 238, 214 },
};

vec4 sg_color_db16(int index) {
    return sg_color_palette(PAL_DB16, 16, index);
}
