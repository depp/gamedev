/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "file_impl.h"

#include "sg/error.h"
#include "sg/file.h"
#include "sg/util.h"

#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

void sg_filelistb_init(struct sg_filelistb *restrict bp) {
    bp->file = NULL;
    bp->filesize = 0;
    bp->filealloc = 0;
    bp->dir = NULL;
    bp->dirsize = 0;
    bp->diralloc = 0;
    sg_hashset_init(&bp->set);
}

void sg_filelistb_destroy(struct sg_filelistb *restrict bp) {
    char **p, **e;
    for (p = bp->set.data, e = p + bp->set.capacity; p != e; p++) {
        if (*p) free(*p);
    }
    free(bp->file);
    free(bp->dir);
    sg_hashset_destroy(&bp->set);
}

int sg_filelistb_finish(struct sg_filelistb *restrict bp,
                        struct sg_filelist *list, struct sg_error **err) {
    if (bp->filesize > INT_MAX || bp->dirsize > INT_MAX) {
        sg_error_nomem(err);
        sg_filelistb_destroy(bp);
        return -1;
    }
    list->filecount = bp->filesize;
    list->dircount = bp->dirsize;
    list->file = bp->file;
    list->dir = bp->dir;
    sg_hashset_destroy(&bp->set);
    return 0;
}

int sg_filelistb_hasname(struct sg_filelistb *restrict bp,
                         const char *restrict name) {
    return sg_hashset_test(&bp->set, name);
}

static char *sg_filelistb_addname(struct sg_filelistb *restrict bp,
                                  const char *restrict name) {
    char **p, *v;
    size_t len;
    p = sg_hashset_insert(&bp->set, name);
    if (!p) return NULL;
    assert(!*p);
    len = strlen(name);
    v = malloc(len + 1);
    if (!v) return NULL;
    memcpy(v, name, len + 1);
    bp->set.size++;
    *p = v;
    return v;
}

int sg_filelistb_addfile(struct sg_filelistb *restrict bp,
                         const struct sg_fileinfo *restrict info,
                         struct sg_error **err) {
    struct sg_fileinfo *narr, *e;
    size_t nalloc;
    char *path;
    if (1 > bp->filealloc - bp->filesize) {
        nalloc = sg_round_up_pow2(bp->filealloc + 1);
        if (!nalloc) goto nomem;
        narr = realloc(bp->file, sizeof(*narr) * nalloc);
        if (!narr) goto nomem;
        bp->file = narr;
        bp->filealloc = nalloc;
    }
    path = sg_filelistb_addname(bp, info->path);
    if (!path) goto nomem;
    e = &bp->file[bp->filesize++];
    e->size = info->size;
    e->mtime = info->mtime;
    e->path = path;
    return 0;
nomem:
    sg_error_nomem(err);
    return -1;
}

int sg_filelistb_adddir(struct sg_filelistb *restrict bp, const char *name,
                        struct sg_error **err) {
    char **narr;
    size_t nalloc;
    char *path;
    if (1 > bp->diralloc - bp->dirsize) {
        nalloc = sg_round_up_pow2(bp->diralloc + 1);
        if (!nalloc) goto nomem;
        narr = realloc(bp->dir, sizeof(*narr) * nalloc);
        if (!narr) goto nomem;
        bp->dir = narr;
        bp->diralloc = nalloc;
    }
    path = sg_filelistb_addname(bp, name);
    if (!path) goto nomem;
    bp->dir[bp->dirsize++] = path;
    return 0;
nomem:
    sg_error_nomem(err);
    return -1;
}

void sg_filelist_init(struct sg_filelist *restrict list) {
    list->filecount = 0;
    list->dircount = 0;
    list->file = NULL;
    list->dir = NULL;
}

void sg_filelist_destroy(struct sg_filelist *restrict list) {
    struct sg_fileinfo *fp, *fe;
    char **dp, **de;
    for (fp = list->file, fe = fp + list->filecount; fp != fe; fp++) {
        free(fp->path);
    }
    free(list->file);
    for (dp = list->dir, de = dp + list->dircount; dp != de; dp++) {
        free(*dp);
    }
    free(list->dir);
}
