/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "file_impl.h"

#include "sg/error.h"

static void sg_path_error(struct sg_error **err, int code) {
    (void)code;
    sg_error_set(err, SG_ERR_BADPATH);
}

int sg_path_file(char *restrict buf, const char *restrict path,
                 struct sg_error **err) {
    int r;
    r = sg_path_norm(buf, path);
    if (r < 0) {
        sg_path_error(err, -r);
        return -1;
    }
    if (buf[r-1] == '/') {
        sg_error_set(err, SG_ERR_ISDIR);
        return -1;
    }
    return r;
}

int sg_path_dir(char *restrict buf, const char *restrict path,
                struct sg_error **err) {
    int r;
    r = sg_path_norm(buf, path);
    if (r < 0) {
        sg_path_error(err, -r);
        return -1;
    }
    if (buf[r-1] != '/') {
        if (r + 1 >= SG_PATH_MAX) {
            sg_path_error(err, SG_PATH_ETOOLONG);
            return -1;
        }
        buf[r++] = '/';
        buf[r] = '\0';
    }
    return r;
}
