/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "opengl_private.h"
#include "sg/entry.h"
#include "sg/opengl.h"
#include "sggl/1_1.h"
#include <stdlib.h>
#include <string.h>

static void SG_GL_API
sg_gl_dummy_push(
    GLenum source,
    GLuint id,
    GLsizei length,
    const char *message)
{
    (void) source;
    (void) id;
    (void) length;
    (void) message;
}

static void SG_GL_API
sg_gl_dummy_pop(void)
{ }

struct sg_gl_debugf sg_gl_debugf = {
    0,
    sg_gl_dummy_push,
    sg_gl_dummy_pop
};

#if defined SG_GL_KHR_debug
# include "sg/log.h"
# include "sg/thread.h"
# include "sggl/KHR_debug.h"

static const unsigned short SOURCE_TOKEN[] = {
    GL_DEBUG_SOURCE_API,
    GL_DEBUG_SOURCE_SHADER_COMPILER,
    GL_DEBUG_SOURCE_WINDOW_SYSTEM,
    GL_DEBUG_SOURCE_THIRD_PARTY,
    GL_DEBUG_SOURCE_APPLICATION,
    GL_DEBUG_SOURCE_OTHER
};

static const char SOURCE_NAME[] =
    "API\0"
    "SHADER_COMPILER\0"
    "WINDOW_SYSTEM\0"
    "THIRD_PARTY\0"
    "APPLICATION\0"
    "OTHER\0"
    ;

static const unsigned short TYPE_TOKEN[] = {
    GL_DEBUG_TYPE_ERROR,
    GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR,
    GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR,
    GL_DEBUG_TYPE_PERFORMANCE,
    GL_DEBUG_TYPE_PORTABILITY,
    GL_DEBUG_TYPE_OTHER,
    GL_DEBUG_TYPE_MARKER,
    GL_DEBUG_TYPE_PUSH_GROUP,
    GL_DEBUG_TYPE_POP_GROUP
};

static const char TYPE_NAME[] =
    "ERROR\0"
    "DEPRECATED_BEHAVIOR\0"
    "UNDEFINED_BEHAVIOR\0"
    "PERFORMANCE\0"
    "PORTABILITY\0"
    "OTHER\0"
    "MARKER\0"
    "PUSH_GROUP\0"
    "POP_GROUP\0"
    ;

#if 0
static const unsigned short SEVERITY_TOKEN[] = {
    GL_DEBUG_SEVERITY_HIGH,
    GL_DEBUG_SEVERITY_MEDIUM,
    GL_DEBUG_SEVERITY_LOW,
    GL_DEBUG_SEVERITY_NOTIFICATION
};

static const char SEVERITY_NAME[] =
    "HIGH\0"
    "MEDIUM\0"
    "LOW\0"
    "NOTIFICATION\0"
    ;
#endif

struct sg_gl_debuginfo {
    int stackpos;
    int namepos;
    int stack[32];
    char name[1024];
};

static struct sg_gl_debuginfo *sg_gl_debuginfo;

static const char *
sg_gl_lookup_name(
    const unsigned short *token_table,
    const char *name_table,
    GLenum token)
{
    const unsigned short *tp = token_table;
    const char *np = name_table;
    while (*np) {
        if (*tp == token)
            return np;
        tp += 1;
        np += strlen(np) + 1;
    }
    return NULL;
}

static void SG_GL_API
sg_gl_khrdebug_cb(
    GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar *message,
    const void *userParam)
{
    const char *nsrc, *ntype;
    sg_log_level_t level;
    struct sg_gl_debuginfo *di = (void *) userParam;
    int stacksz = sizeof(di->stack) / sizeof(*di->stack);

    (void) id;

    switch (type) {
    case GL_DEBUG_TYPE_PUSH_GROUP:
        di->stackpos++;
        if (di->stackpos > stacksz)
            return;
        di->stack[di->stackpos - 1] = di->namepos;
        if (length + 2 > (int) sizeof(di->name) - di->namepos)
            return;
        if (di->stackpos > 1)
            di->name[di->namepos++] = ' ';
        memcpy(di->name + di->namepos, message, length);
        di->namepos += length;
        di->name[di->namepos] = '\0';
        return;

    case GL_DEBUG_TYPE_POP_GROUP:
        if (!di->stackpos)
            return;
        di->stackpos--;
        if (di->stackpos >= stacksz)
            return;
        di->namepos = di->stack[di->stackpos];
        di->name[di->namepos] = '\0';
        return;

    default:
        break;
    };

    nsrc = sg_gl_lookup_name(SOURCE_TOKEN, SOURCE_NAME, source);
    ntype = sg_gl_lookup_name(TYPE_TOKEN, TYPE_NAME, type);
    /* nsev = sg_gl_lookup_name(SEVERITY_TOKEN, SEVERITY_NAME, severity); */

    switch (severity) {
    default:
    case GL_DEBUG_SEVERITY_HIGH:         level = SG_LOG_ERROR; break;
    case GL_DEBUG_SEVERITY_MEDIUM:       level = SG_LOG_WARN;  break;
    case GL_DEBUG_SEVERITY_LOW:          level = SG_LOG_INFO;  break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: level = SG_LOG_DEBUG; break;
    }

    sg_logf(level, "GL: %s: %s %s %s",
            di->name[0] ? di->name : "<root>", nsrc, ntype, message);
}

static void
sg_gl_khrdebug_init(void)
{
    struct sg_gl_debuginfo *di;
    void *func[2];

    sg_logs(SG_LOG_DEBUG, "Enabling KHR_debug");

    sg_gl_getfuncs(
        "PushDebugGroup\0"
        "PopDebugGroup\0",
        func,
        2);
    sg_gl_debugf.enabled = 1;
    sg_gl_debugf.push = func[0];
    sg_gl_debugf.pop = func[1];

    di = malloc(sizeof(*di));
    if (!di)
        sg_sys_abort("Out of memory.");
    di->stackpos = 0;
    di->namepos = 0;
    di->name[0] = '\0';
    sg_gl_debuginfo = di;

    glDebugMessageCallback(sg_gl_khrdebug_cb, di);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE,
                          0, NULL, GL_TRUE);
    glEnable(GL_DEBUG_OUTPUT);
}

#endif

void
sg_gl_debug_init(void)
{
#if defined SG_GL_KHR_debug
    if (SG_GL_KHR_debug)
        sg_gl_khrdebug_init();
#endif
}
