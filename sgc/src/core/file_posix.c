/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

/*
 * POSIX file / path code.  Used on Linux, BSD, Mac OS X.
 */

/* This gives us 64-bit file offsets on 32-bit Linux */
#define _FILE_OFFSET_BITS 64

#include "file_impl.h"

#include "sg/error.h"
#include "sg/file.h"
#include "sg/log.h"
#include "sg/native_path.h"
#include "sg/util.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/* Maximum length of search paths, if a limit exists.  */
#define SG_PATH_BUFSZ 4096

/*
 * Initialize an sg_fileinfo struct from a stat struct.  The path will
 * be initialized to NULL.
 */
static void sg_file_mkinfo(struct sg_fileinfo *info, const struct stat *st) {
    info->size = st->st_size;
#if defined __linux__
    info->mtime = sg_pack64(st->st_mtim.tv_sec, st->st_mtim.tv_nsec);
#elif defined __APPLE__
    info->mtime = sg_pack64(st->st_mtimespec.tv_sec, st->st_mtimespec.tv_nsec);
#else
#warn "Unknown platform"
    info->mtime = st->st_mtime;
#endif
    info->path = NULL;
}

struct sg_reader_posix {
    struct sg_reader obj;
    int fdes;
};

static void sg_reader_posix_close(struct sg_reader *rp) {
    struct sg_reader_posix *obj = (struct sg_reader_posix *)rp;
    close(obj->fdes);
    sg_fileinfo_destroy(&obj->obj.info);
    free(obj);
}

static int sg_reader_posix_read_buf(struct sg_reader *rp, void *data,
                                    size_t size, struct sg_error **err) {
    struct sg_reader_posix *obj = (struct sg_reader_posix *)rp;
    size_t amt = size > INT_MAX ? INT_MAX : size;
    ssize_t r;
    int fdes = obj->fdes, ecode;
again:
    r = read(fdes, data, amt);
    if (r < 0) {
        ecode = errno;
        if (ecode == EINTR) goto again;
        sg_error_errno(err, ecode);
        return -1;
    }
    return r;
}

static int64_t sg_reader_posix_seek(struct sg_reader *rp, int64_t offset,
                                    sg_seekpos_t point, struct sg_error **err) {
    struct sg_reader_posix *obj = (struct sg_reader_posix *)rp;
    int fdes = obj->fdes;
    off_t r;
    r = lseek(fdes, offset, (int)point);
    if (r < 0) sg_error_errno(err, errno);
    return r;
}

int sg_reader_open_native(struct sg_reader **rp, const char *path,
                          struct sg_error **err) {
    struct sg_reader_posix *obj;
    int fdes, ecode, r;
    struct stat st;
again:
    fdes = open(path, O_RDONLY | O_CLOEXEC);
    if (fdes < 0) {
        ecode = errno;
        switch (ecode) {
        case EINTR:
            goto again;
        case ENOENT:
            return SG_FSTATUS_NOTFOUND;
        default:
            sg_error_errno(err, ecode);
            return SG_FSTATUS_ERR;
        }
    }
    r = fstat(fdes, &st);
    if (r < 0) {
        sg_error_errno(err, ecode);
        close(fdes);
        return SG_FSTATUS_ERR;
    }
    if (!S_ISREG(st.st_mode)) {
        close(fdes);
        if (S_ISDIR(st.st_mode)) {
            sg_error_set(err, SG_ERR_ISDIR);
            return SG_FSTATUS_ERR;
        }
        return SG_FSTATUS_NOTFOUND;
    }
    obj = malloc(sizeof(*obj));
    if (!obj) {
        sg_error_nomem(err);
        close(fdes);
        return SG_FSTATUS_ERR;
    }
    sg_file_mkinfo(&obj->obj.info, &st);
    obj->obj.close = sg_reader_posix_close;
    obj->obj.read_buf = sg_reader_posix_read_buf;
    obj->obj.read_data = sg_reader_read_data;
    obj->obj.seek = sg_reader_posix_seek;
    obj->fdes = fdes;
    *rp = &obj->obj;
    return SG_FSTATUS_OK;
}

/*
 * See Theo Ts'o's blog post for more information about the semantics
 * of writing files.  Basically, instead of writing to "out.txt", we
 * write to ".out.txt.tmp", then fsync() the data, then we rename
 * ".out.txt.tmp" to "out.txt", overwriting the old file, if any.
 *
 * http://thunk.org/tytso/blog/2009/03/15/dont-fear-the-fsync/
 */

int sg_writer_open(struct sg_writer *restrict wp, const char *restrict path,
                   struct sg_error **err) {
    char *destpath, *temppath, *ptr;
    const char *p;
    size_t destlen, pos;
    int fdes, pflags, mode, ecode, r;

    destpath = sg_path_writable(path, err);
    if (!destpath) return -1;

    destlen = strlen(destpath);
    temppath = malloc(destlen + 6);
    if (!temppath) {
        sg_error_nomem(err);
        free(destpath);
        return -1;
    }
    p = strrchr(destpath, '/');
    pos = p ? p + 1 - destpath : 0;
    ptr = temppath;
    memcpy(ptr, destpath, pos);
    ptr += pos;
    *ptr++ = '.';
    memcpy(ptr, destpath + pos, destlen - pos);
    ptr += destlen - pos;
    memcpy(ptr, ".tmp", 5);

    pflags = O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC;
    mode = 0666;
again_1:
    fdes = open(temppath, pflags, mode);
    if (fdes < 0) {
        ecode = errno;
        if (ecode != ENOENT) goto error_errno;
        if (ecode == EINTR) goto again_1;
        r = sg_path_mkpardir(destpath, err);
        if (r) goto error;
    again_2:
        fdes = open(temppath, pflags, mode);
        if (fdes < 0) {
            ecode = errno;
            if (ecode == EINTR) goto again_2;
            goto error_errno;
        }
    }

    wp->fdes = fdes;
    wp->destpath = destpath;
    wp->temppath = temppath;
    return 0;

error_errno:
    sg_error_errno(err, ecode);
    goto error;

error:
    free(destpath);
    free(temppath);
    return -1;
}

/*
 * Clean up a writer after the file has already been closed, zeroing
 * the data structure to indicate that the file is closed.
 */
static void sg_writer_cleanup(struct sg_writer *restrict wp) {
    free(wp->destpath);
    free(wp->temppath);
    wp->fdes = -1;
    wp->destpath = NULL;
    wp->temppath = NULL;
}

void sg_writer_close(struct sg_writer *restrict wp) {
    if (wp->fdes == -1) return;
    close(wp->fdes);
    unlink(wp->temppath);
    sg_writer_cleanup(wp);
}

int sg_writer_commit(struct sg_writer *restrict wp, struct sg_error **err) {
    int fdes, r, ecode;
    fdes = wp->fdes;
    if (fdes == -1) {
        sg_error_set(err, SG_ERR_INVAL);
        return -1;
    }
    r = fsync(fdes);
    if (r) {
        ecode = errno;
        goto error_errno;
    }
    r = close(fdes);
    if (r) goto error_errno;
    fdes = -1;
#if defined __APPLE__
    /* This preserves metadata, other than modification time.  */
    r = exchangedata(wp->temppath, wp->destpath, 0);
    if (r) {
        ecode = errno;
        switch (ecode) {
        case ENOTSUP:
        case ENOENT:
            break;
        default:
            goto error_ecode;
        }
    }
#endif
    r = rename(wp->temppath, wp->destpath);
    if (r) goto error_errno;
    sg_writer_cleanup(wp);
    return 0;

error_errno:
    ecode = errno;
    goto error_ecode;

error_ecode:
    if (fdes != -1) close(fdes);
    sg_error_errno(err, ecode);
    sg_writer_cleanup(wp);
    return -1;
}

int sg_writer_write(struct sg_writer *restrict wp, const void *restrict data,
                    size_t size, struct sg_error **err) {
    size_t pos;
    ssize_t r;
    int fdes = wp->fdes, ecode;
    if (fdes == -1) {
        sg_error_set(err, SG_ERR_INVAL);
        return -1;
    }
    pos = 0;
    while (pos < size) {
        r = write(fdes, (const char *)data + pos, size - pos);
        if (r < 0) {
            ecode = errno;
            if (ecode != EINTR) {
                sg_error_errno(err, ecode);
                return -1;
            }
        }
        pos += r;
    }
    return 0;
}

int64_t sg_writer_seek(struct sg_writer *restrict wp, int64_t offset,
                       sg_seekpos_t point, struct sg_error **err) {
    int r;
    r = lseek(wp->fdes, offset, (int)point);
    if (r < 0) sg_error_errno(err, errno);
    return r;
}

int sg_path_mkpardir(const char *restrict path, struct sg_error **err) {
    char *p, *buf;
    size_t sz;
    int r, ecode;

    p = strrchr(path, '/');
    if (!p || p == path) {
        sg_error_set(err, SG_ERR_BADPATH);
        return -1;
    }
    sz = p - path;
    buf = malloc(sz + 1);
    if (!buf) {
        sg_error_nomem(err);
        return -1;
    }
    memcpy(buf, path, sz);
    buf[sz] = '\0';
    p = buf + sz;
    while (1) {
        r = mkdir(buf, 0777);
        if (!r) break;
        ecode = errno;
        if (ecode != ENOENT) {
            if (ecode == EEXIST) break;
            goto err;
        }
        p = strrchr(buf, '/');
        if (!p || p == buf) {
            ecode = ENOENT;
            goto err;
        }
        *p = '\0';
    }
    while (p != buf + sz) {
        *p++ = '/';
        r = mkdir(buf, 0777);
        if (r) {
            ecode = errno;
            if (ecode != EEXIST) goto err;
        }
        p += strlen(p);
    }
    free(buf);
    return 0;

err:
    sg_error_errno(err, ecode);
    free(buf);
    return -1;
}

static int sg_filelist_add_stat(const char *dirpath, int fdes, char *name,
                                struct sg_filelistb *restrict bp,
                                sg_filelist_info_t info,
                                struct sg_error **err) {
    struct stat st;
    int r;
    struct sg_error *e = NULL;
    struct sg_fileinfo file;

    r = fstatat(fdes, name, &st, 0);
    if (r) {
        sg_error_errno(&e, errno);
        sg_logerrf(SG_LOG_ERROR, e, "%s%s", dirpath, name);
        sg_error_clear(&e);
        return 0;
    }
    if (S_ISREG(st.st_mode)) {
        if (info != SG_FILELIST_NOINFO) {
            sg_file_mkinfo(&file, &st);
        } else {
            file.size = 0;
            file.mtime = 0;
        }
        file.path = name;
        return sg_filelistb_addfile(bp, &file, err);
    } else if (S_ISDIR(st.st_mode)) {
        return sg_filelistb_adddir(bp, name, err);
    } else {
        return 0;
    }
}

static int sg_filelist_add_dirent(
    const char *dirpath, int fdes, struct dirent *restrict e,
    struct sg_filelistb *restrict bp, sg_filelist_info_t info,
    struct sg_error **err) {
    char *name;
    size_t len;
    struct sg_fileinfo file;

    name = e->d_name;
    len = strlen(name);
    if (sg_filelistb_hasname(bp, name) || sg_path_checkpart(name, len))
        return 0;
    switch (e->d_type) {
    case DT_DIR:
        return sg_filelistb_adddir(bp, name, err);
    case DT_REG:
        if (info != SG_FILELIST_NOINFO)
            return sg_filelist_add_stat(dirpath, fdes, name, bp, info, err);
        file.size = 0;
        file.mtime = 0;
        file.path = name;
        return sg_filelistb_addfile(bp, &file, err);
    case DT_LNK:
    case DT_UNKNOWN:
        return sg_filelist_add_stat(dirpath, fdes, name, bp, info, err);
    default:
        return 0;
    }
}

int sg_filelist_scan_native(const pchar *restrict path,
                            struct sg_filelistb *restrict bp,
                            sg_filelist_info_t info, struct sg_error **err) {
    DIR *dp = NULL;
    int ecode, fdes, r;
    long name_max;
    struct dirent *buf = NULL, *res;

    dp = opendir(path);
    if (!dp) {
        ecode = errno;
        switch (ecode) {
        case ENOTDIR:
            return SG_FSTATUS_NOTDIR;
        case ENOENT:
            return SG_FSTATUS_NOTFOUND;
        default:
            sg_error_errno(err, ecode);
            return SG_FSTATUS_ERR;
        }
    }
    fdes = dirfd(dp);
    if (fdes == -1) goto err_errno;
    name_max = fpathconf(fdes, _PC_NAME_MAX);
    if (name_max < 0) {
        /* This "guess" is used in the man page.  */
        name_max = 255;
    }
    buf = malloc(offsetof(struct dirent, d_name) + name_max + 1);
    if (!buf) {
        sg_error_nomem(err);
        goto err;
    }
    while (1) {
        r = readdir_r(dp, buf, &res);
        if (r) goto err_errno;
        if (!res) break;
        r = sg_filelist_add_dirent(path, fdes, res, bp, info, err);
        if (r) goto err;
    }
    closedir(dp);
    free(buf);
    return SG_FSTATUS_OK;

err_errno:
    sg_error_errno(err, errno);
    goto err;

err:
    closedir(dp);
    free(buf);
    return SG_FSTATUS_ERR;
}

#if defined __APPLE__
#include <CoreFoundation/CFBundle.h>

void
sg_path_getdefaults(
    unsigned read_flags,
    unsigned write_flags)
{
    CFBundleRef bundle;
    CFURLRef url1, url2;
    CFStringRef relpath;
    Boolean r;
    const char *crelpath;
    int i;
    unsigned rflags, wflags;
    char *buf = NULL;

    bundle = CFBundleGetMainBundle();
    if (!bundle)
        goto error;
    url1 = CFBundleCopyBundleURL(bundle);
    if (!url1)
        goto error;
    url2 = CFURLCreateCopyDeletingLastPathComponent(NULL, url1);
    CFRelease(url1);
    if (!url2)
        goto error;
    for (i = 0; i < 3; i++) {
        rflags = sg_path_default[i].read_flags & ~read_flags;
        wflags = sg_path_default[i].write_flags & ~write_flags;
        if (!(rflags | wflags))
            continue;
        crelpath = sg_path_default[i].path;
        relpath = CFStringCreateWithBytes(
            kCFAllocatorDefault, (const UInt8 *) crelpath, strlen(crelpath),
            kCFStringEncodingASCII, false);
        if (!relpath)
            goto error;
        url1 = CFURLCreateCopyAppendingPathComponent(
            NULL, url2, relpath, true);
        CFRelease(relpath);
        if (!url1)
            goto error;
        if (!buf) {
            buf = malloc(SG_PATH_BUFSZ);
            if (!buf) {
                sg_path_initabort(SG_PATHERR_NOMEM);
                return;
            }
        }
        r = CFURLGetFileSystemRepresentation(
            url1, false, (UInt8 *) buf, SG_PATH_BUFSZ);
        CFRelease(url1);
        if (!r) {
            sg_path_initabort(SG_PATHERR_EXEPATHLONG);
            return;
        }
        sg_path_add(buf, strlen(buf), rflags, wflags);
    }
    CFRelease(url2);
    return;

error:
    sg_path_initabort(SG_PATHERR_EXEPATH);
}

size_t
sg_path_getdatapath(char *buf, size_t buflen)
{
    CFBundleRef bundle;
    CFURLRef url;
    Boolean r;
    bundle = CFBundleGetMainBundle();
    if (!bundle)
        return 0;
    url = CFBundleCopyBundleURL(bundle);
    if (!url)
        return 0;
    r = CFURLGetFileSystemRepresentation(url, false, (UInt8 *) buf, buflen);
    CFRelease(url);
    if (!r)
        return 0;
    return strlen(buf);
}

#elif defined __linux__
#include <unistd.h>

pchar *sg_path_getexedir(struct sg_error **err) {
    char *buf, *p;
    int r;
    buf = malloc(SG_PATH_BUFSZ);
    if (!buf) {
        sg_error_nomem(err);
        return NULL;
    }
    r = readlink("/proc/self/exe", buf, SG_PATH_BUFSZ);
    if (r < 0) {
        sg_error_errno(err, errno);
        free(buf);
        return NULL;
    }
    if (r >= SG_PATH_BUFSZ) goto err;
    buf[r] = '\0';
    p = strrchr(buf, '/');
    if (!p) goto err;
    p++;
    *p = '\0';
    return buf;
err:
    sg_error_set(err, SG_ERR_GENERAL);
    free(buf);
    return NULL;
}

#endif
