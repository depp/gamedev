/* Copyright 2012-2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

#include "sg/log.h"
#include <stddef.h>

struct sg_log_msg {
    const char *time;
    size_t timelen;
    const char *level;
    size_t levellen;
    const char *msg;
    size_t msglen;
    sg_log_level_t levelval;
};

typedef void (*sg_log_listener_t)(struct sg_log_msg *);

void
sg_log_listen(sg_log_listener_t listener);

void
sg_log_console_init(void);

void
sg_log_console_update(void);

void
sg_log_network_init(void);

void
sg_log_network_update(void);

void
sg_log_network_term(void);
