/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "file_impl.h"
#include "private.h"

#include "sg/cvar.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/log.h"
#include "sg/util.h"

#include <stdlib.h>
#include <string.h>

/*
 * The default path for data files, relative to the directory
 * containing the executable.
 */
#define SG_PATH_DATA "Data"

/*
 * The default path for user files, relative to the directory
 * containing the executable.
 */
#define SG_PATH_USER "User Data"

/*
 * The maximum number of search paths.
 */
#define SG_PATH_MAXCOUNT 16

/*
 * =====================================================================
 * About search paths
 * =====================================================================
 *
 * The main job of the virtual filesystem is to make it so we can
 * access files in disparate, platform-specific locations through a
 * simple interface, using the same virtual paths on all platforms.
 *
 * The three main types of files we access are data files,
 * configuration files, and cache files.  These are further divided
 * into system files and user files, although system cache files are
 * not supported.  These divisions exist on most modern platforms,
 * although the semantics can be quite different.
 *
 * There are two general ways to do this.  In a portable (standalone)
 * build, all files are located relative to the executable.  In a
 * system build, some files are located in special directories, such
 * as the user's home directory.
 */

/* Cvars for configuring the search paths. */
enum {
    SG_PATH_CVAR_USER,
    SG_PATH_CVAR_DATA,

    SG_PATH_CVAR_COUNT
};

struct sg_path_cvar {
    char name[12];
    const char *help;
};

static const struct sg_path_cvar SG_PATH_CVAR[SG_PATH_CVAR_COUNT] = {
    {"user", "The path where user data is stored."},
    {"data", "The path where system data is stored."},
};

/* A range of characters in the path data. */
struct sg_path_range {
    unsigned short offset;
    unsigned short length;
};

/* A search path maps virtual paths to native paths. */
struct sg_path_entry {
    /* Virtual path offset and length (see sg_paths).  */
    struct sg_path_range vpath;
    /* Native path offset and length (see sg_paths).  */
    struct sg_path_range npath;
    /* Whether files can be written to this path.  */
    int writable;
};

/* Global search paths structure. */
struct sg_paths {
    /* Array of path search entries, in search order.  */
    struct sg_path_entry entry[SG_PATH_MAXCOUNT];
    int entrycount;
    /* String data for virtual and native paths.  */
#if defined _WIN32
    char *pptr;
    wchar_t *nptr;
    unsigned short plen, palloc;
    unsigned short nlen, nalloc;
#else
    char *pptr;
    unsigned short plen, palloc;
#endif
    /* Maximum additional length of any path search entry.  */
    size_t maxlen;
    /* Cvars. */
    struct sg_cvar_string cvar[SG_PATH_CVAR_COUNT];
};

/* Global search paths. */
static struct sg_paths sg_paths;

/*
 * =====================================================================
 * Path initialization
 * =====================================================================
 */

/*
 * Add a virtual path to the search path string data.  A pointer to
 * storage for the pointer is returned, and range is initialized with
 * the reference to the string.  On failure, NULL is returned, and err
 * is initialized.
 */
static char *sg_path_vadd(struct sg_path_range *range, size_t len,
                          struct sg_error **err) {
    size_t n = len + 1, pos = sg_paths.plen;
    char *ptr = sg_paths.pptr;
    unsigned short nalloc;
    if (n >= sg_paths.palloc - pos) {
        nalloc = sg_round_up_pow2(sg_paths.plen + n);
        if (!nalloc) goto nomem;
        ptr = realloc(ptr, nalloc * sizeof(*ptr));
        if (!ptr) goto nomem;
        sg_paths.pptr = ptr;
        sg_paths.palloc = nalloc;
    }
    /* This is just to make debugging nicer.  */
    ptr += pos;
    if (pos > 0) ptr[-1] = ':';
    ptr[len] = '\0';
    range->offset = pos;
    range->length = len;
    sg_paths.plen += n;
    return ptr;
nomem:
    sg_error_nomem(err);
    return NULL;
}
/*
 * Add a native path to the search path string data.  A pointer to
 * storage for the pointer is returned, and range is initialized with
 * the reference to the string.  On failure, NULL is returned, and err
 * is initialized.
 */
static pchar *sg_path_nadd(struct sg_path_range *range, size_t len,
                           struct sg_error **err);

#if defined _WIN32

static wchar_t *sg_path_nadd(struct sg_path_range *range, size_t len,
                             struct sg_error **err) {
    size_t n = len + 1, pos = sg_paths.nlen;
    wchar_t *ptr = sg_paths.nptr;
    unsigned short nalloc;
    if (n >= sg_paths.nalloc - pos) {
        nalloc = sg_round_up_pow2(sg_paths.nlen + n);
        if (!nalloc) goto nomem;
        ptr = realloc(ptr, nalloc * sizeof(*ptr));
        if (!ptr) goto nomem;
        sg_paths.nptr = ptr;
        sg_paths.nalloc = nalloc;
    }
    /* This is just to make debugging nicer.  */
    ptr += pos;
    if (pos > 0) ptr[-1] = L';';
    ptr[len] = L'\0';
    range->offset = pos;
    range->length = len;
    sg_paths.nlen += n;
    return ptr;
nomem:
    sg_error_nomem(err);
    return NULL;
}

#else

static char *sg_path_nadd(struct sg_path_range *range, size_t len,
                             struct sg_error **err) {
    return sg_path_vadd(range, len, err);
}

#endif

/*
 * Add a virtual path to the search path string data, given the path
 * as a string.  The path should begin and end with a slash.  On
 * success, initialize range and return 0.  On failure, initialize err
 * and return -1.
 */
static int sg_path_vstr(struct sg_path_range *range, const char *path,
                        struct sg_error **err) {
    char *ptr;
    size_t len;
    len = strlen(path);
    ptr = sg_path_vadd(range, len, err);
    if (!ptr) return -1;
    memcpy(ptr, path, len);
    return 0;
}

/*
 * Add a search path.  On success, return 0.  On failure, set err and
 * return -1.
 */
static int sg_path_add(const struct sg_path_range *vpath,
                       const struct sg_path_range *npath, int writable,
                       struct sg_error **err) {
    struct sg_path_entry *e;
    if (sg_paths.entrycount >= SG_PATH_MAXCOUNT) {
        sg_error_set(err, SG_ERR_GENERAL);
        return -1;
    }
    e = &sg_paths.entry[sg_paths.entrycount++];
    e->vpath = *vpath;
    e->npath = *npath;
    e->writable = writable;
    return 0;
}

/*
 * Add a native path from a cvar to the search path string data.  If
 * successful, return 1.  If the cvar is unset, return 0.  If an error
 * occurs, return -1.
 */
static int sg_path_nvar(struct sg_path_range *npath, int var,
                        struct sg_error **err) {
    const char *val;
    pchar *ptr;
    size_t len;
    val = sg_paths.cvar[var].value;
    if (!*val) return 0;
    len = strlen(val);
    if (val[len-1] == SG_PATH_DIRSEP || val[len-1] == SG_PATH_DIRSEP2) len--;
    ptr = sg_path_nadd(npath, len + 1, err);
    if (!ptr) return -1;
    memcpy(ptr, val, len);
    ptr[len] = SG_PATH_DIRSEP;
    return 1;
}

/*
 * Add a native path relative to the directory containing the
 * executable to the search path string data.  The relative path
 * should not begin or end with a slash.  On success, initialize npath
 * and return 0.  On failure, return -1 and initialize err.
 */
static int sg_path_nexerel(struct sg_path_range *npath, pchar **exedir,
                           const char *path, struct sg_error **err) {
    size_t elen, plen;
    pchar *eptr = *exedir, *ptr;
    if (!eptr) {
        eptr = sg_path_getexedir(err);
        if (!eptr) return -1;
        *exedir = eptr;
    }
    elen = strlen(eptr);
    plen = strlen(path);
    ptr = sg_path_nadd(npath, elen + plen + (plen > 0 ? 1 : 0), err);
    if (!ptr) return -1;
    memcpy(ptr, eptr, elen);
    memcpy(ptr + elen, path, plen);
    if (plen > 0) ptr[elen + plen] = SG_PATH_DIRSEP;
    return 0;
}

/*
 * Initialize cvars used by the path system.
 */
static void sg_path_initcvar(void) {
    int i;
    for (i = 0; i < SG_PATH_CVAR_COUNT; i++) {
        sg_cvar_defstring("path", SG_PATH_CVAR[i].name, SG_PATH_CVAR[i].help,
                          &sg_paths.cvar[i], NULL, SG_CVAR_INITONLY);
    }
}

/*
 * Initialize search path structures, once cvars have been
 * initialized.
 */
static int sg_path_initsearch(struct sg_error **err) {
    struct sg_path_range vroot, npath;
    int r;
    pchar *exedir = NULL;

    /*
     * Get all of the virtual paths.  Note, I'm not crazy.  There used
     * to be separate /config/ and /cache/ paths, but those have been
     * removed for now because we only test portable builds.
     */
    r = sg_path_vstr(&vroot, "/", err);
    if (r) goto err;

    /* User data path.  */
    r = sg_path_nvar(&npath, SG_PATH_CVAR_USER, err);
    if (r == 0) {
        r = sg_path_nexerel(&npath, &exedir, SG_PATH_DATA, err);
    }
    if (r < 0) goto err;
    r = sg_path_add(&vroot, &npath, 1, err);
    if (r) goto err;

    /* System data path.  */
    r = sg_path_nvar(&npath, SG_PATH_CVAR_DATA, err);
    if (r == 0) {
        r = sg_path_nexerel(&npath, &exedir, SG_PATH_USER, err);
    }
    if (r < 0) goto err;
    r = sg_path_add(&vroot, &npath, 0, err);
    if (r) goto err;

    free(exedir);
    return 0;

err:
    free(exedir);
    return -1;
}

/*
 * Compute maxlen.
 */
static void sg_path_getmaxlen(void) {
    const struct sg_path_entry *e;
    size_t maxlen, len;
    int i;
    maxlen = 1;
    for (i = 0; i < sg_paths.entrycount; i++) {
        e = &sg_paths.entry[i];
        if (e->npath.length <= e->vpath.length) continue;
        len = e->npath.length - e->vpath.length;
        if (len > maxlen) maxlen = len;
    }
    sg_paths.maxlen = maxlen;
}

void sg_path_init(void) {
    struct sg_error *err = NULL;
    int r;
    sg_path_initcvar();
    r = sg_path_initsearch(&err);
    sg_path_getmaxlen();
    if (r) goto err;
    return;

err:
    sg_logerrs(SG_LOG_ERROR, err, "Could not initialize paths");
    sg_error_clear(&err);
    abort();
}

/*
 * =====================================================================
 * Public API
 * =====================================================================
 */

/* Maximum number of extensions to search.  */
#define SG_PATH_MAXEXTCOUNT 12
/* Maximum length of any extension, including period and NUL.  */
#define SG_PATH_MAXEXTLEN 8

/*
 * A parsed list of extensions.
 */
struct sg_path_extlist {
    int count;
    char ext[SG_PATH_MAXEXTCOUNT][SG_PATH_MAXEXTLEN];
};

/*
 * Parse a list of extensions.  The resulting list includes the
 * periods.  This always produces a result, for NULL extension lists,
 * it will produce a list containing only the empty string.
 */
static void sg_path_extlist(struct sg_path_extlist *restrict ext,
                             const char *restrict extensions) {
    const char *epos, *estart, *esep;
    int count;
    size_t elen;

    if (!extensions) {
        ext->count = 1;
        ext->ext[0][0] = '\0';
        return;
    }

    epos = extensions;
    count = 0;
    while (epos) {
        estart = epos;
        esep = strchr(epos, ':');
        if (!esep) {
            epos = NULL;
            elen = strlen(estart);
        } else {
            epos = esep + 1;
            elen = esep - estart;
        }
        if (!elen) continue;
        if (count >= SG_PATH_MAXEXTCOUNT) {
            sg_logf(SG_LOG_ERROR, "List of extensions is too long: %s",
                    extensions);
            break;
        }
        if (elen > SG_PATH_MAXEXTLEN - 2) {
            sg_logf(SG_LOG_ERROR, "Extension is too long: %s", extensions);
            continue;
        }
        ext->ext[count][0] = '.';
        memcpy(ext->ext[count] + 1, estart, elen);
        ext->ext[count][elen + 1] = '\0';
        count++;
    }
    ext->count = count;
}

struct sg_reader *sg_reader_open(const char *restrict path,
                                 const char *restrict extensions,
                                 struct sg_error **err) {
    char normpath[SG_PATH_MAX], *fullpath, *p, *vpath;
    const char *nptr, *vptr, *eptr;
    size_t plen, nlen, vlen, elen;
    struct sg_path_extlist ext;
    struct sg_reader *rp;
    const struct sg_path_entry *e;
    int i, j, r;

    r = sg_path_file(normpath, path, err);
    if (r < 0) return NULL;
    plen = r;

    sg_path_extlist(&ext, extensions);
    if (!ext.count) {
        sg_error_errno(err, SG_ERR_NOTFOUND);
        return NULL;
    }

    fullpath = malloc(sg_paths.maxlen + plen + SG_PATH_MAXEXTLEN);
    if (!fullpath) {
        sg_error_nomem(err);
        return NULL;
    }

    for (i = 0; i < ext.count; i++) {
        eptr = ext.ext[i];
        for (j = 0; j < sg_paths.entrycount; j++) {
            e = &sg_paths.entry[j];
            vptr = sg_paths.pptr + e->vpath.offset;
            vlen = e->vpath.length;
            nptr = sg_paths.pptr + e->npath.offset;
            nlen = e->npath.length;
            if (vlen >= plen || memcmp(vptr, normpath, vlen)) continue;
            p = fullpath;
            memcpy(p, nptr, nlen);
            p += nlen;
            memcpy(p, normpath + vlen, plen - vlen);
            p += plen - vlen;
            memcpy(p, eptr, SG_PATH_MAXEXTLEN);
            r = sg_reader_open_native(&rp, fullpath, err);
            if (r == SG_FSTATUS_OK)
                goto success;
            else if (r == SG_FSTATUS_ERR)
                goto error;
        }
    }

    sg_error_set(err, SG_ERR_NOTFOUND);
    goto error;

error:
    free(fullpath);
    return NULL;

success:
    free(fullpath);
    elen = strlen(eptr);
    vpath = malloc(plen + elen + 1);
    if (!vpath) {
        rp->close(rp);
        sg_error_nomem(err);
        return NULL;
    }
    memcpy(vpath, normpath, plen);
    memcpy(vpath + plen, eptr, elen + 1);
    rp->info.path = vpath;
    return rp;
}

char *sg_path_writable(const char *restrict path, struct sg_error **err) {
    char normpath[SG_PATH_MAX], *fullpath;
    const char *nptr, *vptr;
    size_t nlen, vlen, plen;
    const struct sg_path_entry *e;
    int i, r;

    r = sg_path_file(normpath, path, err);
    if (r < 0) return NULL;
    plen = r;
    for (i = 0; i < sg_paths.entrycount; i++) {
        e = &sg_paths.entry[i];
        if (!e->writable) continue;
        vptr = sg_paths.pptr + e->vpath.offset;
        vlen = e->vpath.length;
        nptr = sg_paths.pptr + e->npath.offset;
        nlen = e->npath.length;
        if (vlen >= plen || memcmp(vptr, normpath, vlen)) continue;
        fullpath = malloc(nlen + plen - vlen + 1);
        if (!fullpath) {
            sg_error_nomem(err);
            return NULL;
        }
        memcpy(fullpath, nptr, nlen);
        memcpy(fullpath + nlen, normpath + vlen, plen - vlen + 1);
        return fullpath;
    }

    sg_error_set(err, SG_ERR_PERM);
    return NULL;
}

int sg_filelist_scan(struct sg_filelist *restrict list,
                     const char *restrict path, sg_filelist_info_t info,
                     struct sg_error **err) {
    char normpath[SG_PATH_MAX], *fullpath;
    const char *nptr, *vptr;
    size_t plen, nlen, vlen;
    struct sg_filelistb listb;
    const struct sg_path_entry *e;
    int i, r, is_found;

    r = sg_path_dir(normpath, path, err);
    if (r < 0) return 0;
    plen = r;

    fullpath = malloc(sg_paths.maxlen + plen + SG_PATH_MAXEXTLEN);
    if (!fullpath) {
        sg_error_nomem(err);
        return -1;
    }

    is_found = 0;
    sg_filelistb_init(&listb);
    for (i = 0; i < sg_paths.entrycount; i++) {
        e = &sg_paths.entry[i];
        vptr = sg_paths.pptr + e->vpath.offset;
        vlen = e->vpath.length;
        nptr = sg_paths.pptr + e->npath.offset;
        nlen = e->npath.length;
        if (vlen >= plen || memcmp(vptr, normpath, vlen)) continue;
        memcpy(fullpath, nptr, nlen);
        memcpy(fullpath + nlen, normpath + vlen, plen - vlen + 1);
        r = sg_filelist_scan_native(fullpath, &listb, info, err);
        switch (r) {
        case SG_FSTATUS_OK:
            is_found = 1;
            break;
        case SG_FSTATUS_NOTFOUND:
            break;
        case SG_FSTATUS_NOTDIR:
            if (!is_found) {
                sg_error_set(err, SG_ERR_NOTDIR);
                goto err;
            }
            goto done;
        case SG_FSTATUS_ERR:
            goto err;
        }
    }

done:
    if (!is_found) {
        sg_error_set(err, SG_ERR_NOTFOUND);
        goto err;
    }
    free(fullpath);
    return sg_filelistb_finish(&listb, list, err);

err:
    sg_filelistb_destroy(&listb);
    free(fullpath);
    return -1;
}
