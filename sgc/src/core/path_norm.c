/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "file_impl.h"

#include <string.h>

/* Table for which characters are valid in pathnames.  The list of
   characters is very conservative: only alphanumeric characters, "-",
   ".", and "_" are allowed.  */
/*
Python:
import string
a = [0] * 8
for c in string.ascii_letters + string.digits + "-._":
    n = ord(c)
    a[n >> 5] |= 1 << (n & 31)
*/
static const unsigned SG_PATH_ALLOWED[8] = {
    0, 67067904, 2281701374, 134217726, 0, 0, 0, 0
};

/*
  References:
  http://msdn.microsoft.com/en-us/library/windows/desktop/aa365247(v=vs.85).aspx
*/

static int sg_path_isreserved(const char *restrict p, size_t len);

int sg_path_checkpart(const char *restrict p, size_t len) {
    unsigned allowed, c;
    size_t i;
    if (len > 0 && p[0] == '.') return SG_PATH_EPERIOD;
    allowed = 1;
    for (i = 0; i < len; i++) {
        c = (unsigned char)p[i];
        allowed &= SG_PATH_ALLOWED[c >> 5] >> (c & 31);
    }
    if (!allowed) return SG_PATH_EBADCHAR;
    return sg_path_isreserved(p, len) ? SG_PATH_ERESERVED : SG_PATH_OK;
}

static int sg_path_isreserved(const char *restrict p, size_t len) {
    char b[4];
    size_t blen, i;

    if (len > 3 && p[3] == '.')
        blen = 3;
    else if (len > 4 && p[4] == '.')
        blen = 4;
    else if (len < 3 || len > 4)
        return 0;
    else
        blen = len;

    for (i = 0; i < blen; i++) {
        if (p[i] >= 'A' && p[i] <= 'Z')
            b[i] = p[i] + 'a' - 'A';
        else
            b[i] = p[i];
    }

    switch (b[0]) {
    case 'a':
        if (blen == 3 && b[1] == 'u' && b[2] == 'x')
            return 1;
        break;

    case 'c':
        if (blen == 4 && b[1] == 'o' && b[2] == 'm' &&
            (b[3] >= '0' && b[3] <= '9'))
            return 1;
        if (blen == 3 && b[1] == 'o' && b[2] == 'n')
            return 1;
        break;

    case 'l':
        if (blen == 4 && b[1] == 'p' && b[2] == 't' &&
            (b[3] >= '0' && b[3] <= '9'))
            return 1;
        break;

    case 'n':
        if (blen == 3 && b[1] == 'u' && b[2] == 'l')
            return 1;
        break;

    case 'p':
        if (blen == 3 && b[1] == 'r' && b[2] == 'n')
            return 1;
        break;

    default:
        break;
    }
    return 0;
}

int sg_path_norm(char *restrict buf, const char *restrict path) {
    char const *ip = path, *sstart;
    char *op = buf, *oe = buf + SG_PATH_MAX;
    int r;
    size_t n;

    if (*path != '/') return -SG_PATH_ENOTABS;
    *op++ = '/';
    if (op == oe) return -SG_PATH_ETOOLONG;
    while (1) {
        while (*ip == '/')
            ip++;
        if (!*ip) break;
        sstart = ip++;
        while (*ip && *ip != '/')
            ip++;
        n = ip - sstart;
        if (n == 1 && ip[0] == '.') {
            if (!*ip) break;
        } else if (n == 2 && ip[0] == '.' && ip[1] == '.') {
            if (op == buf + 1) return -SG_PATH_EESCAPE;
            op--;
            while (op[-1] != '/')
                op--;
            if (!*ip) break;
        } else {
            if (n + 1 > (size_t)(oe - op)) return -SG_PATH_ETOOLONG;
            r = sg_path_checkpart(sstart, n);
            if (r) return -r;
            if (sg_path_isreserved(sstart, n)) return -SG_PATH_ERESERVED;
            memcpy(op, sstart, n);
            op += n;
            if (!*ip) break;
            if (2 > (size_t)(oe - op)) return -SG_PATH_ETOOLONG;
            *op++ = '/';
        }
    }
    *op = '\0';
    return (int)(op - buf);
}
