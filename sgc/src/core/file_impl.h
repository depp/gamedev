/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

/*
 * Internal file / path subsystem interface.
 */

#include "sg/defs.h"
#include "sg/hashset.h"
#include "sg/file.h"
#include "sg/native_path.h"

#include <stddef.h>

struct sg_data;
struct sg_error;
struct sg_filelist;
struct sg_reader;

enum {
    /* Opening the file succeeded.  */
    SG_FSTATUS_OK,
    /* File not found.  */
    SG_FSTATUS_NOTFOUND,
    /* Part of the path is not a directory.  */
    SG_FSTATUS_NOTDIR,
    /* An error occurred.  */
    SG_FSTATUS_ERR
};

enum {
    /* The path is OK.  */
    SG_PATH_OK,
    /* A path component begins with a period.  */
    SG_PATH_EPERIOD,
    /* The path contains an illegal character.  */
    SG_PATH_EBADCHAR,
    /* A path component is a reserved name.  */
    SG_PATH_ERESERVED,
    /* The path is too long.  */
    SG_PATH_ETOOLONG,
    /* The path is not absolute.  */
    SG_PATH_ENOTABS,
    /* The path tries to escape root.  */
    SG_PATH_EESCAPE,
};

/*
 * Normalize a path.  Stores the path in buf, which must have size
 * SG_PATH_MAX.  Returns the length of the path, or a negative number
 * corresponding to the SG_PATH_EXXX error codes above.
 */
int sg_path_norm(char *restrict buf, const char *restrict path);

/*
 * Common implementation of the 'read_data' function for an sg_reader.
 */
int sg_reader_read_data(struct sg_reader *rp, struct sg_data *data, size_t size,
                        struct sg_error **err);

/*
 * Open a native file reader.  Returns SG_FSTATUS_OK,
 * SG_FSTATUS_NOTFOUND, or SG_FSTATUS_ERR.  This should initialize
 * (*rp)->path to NULL, it will be filled in by sg_reader_open().
 */
int sg_reader_open_native(struct sg_reader **rp, const char *path,
                          struct sg_error **err);

/*
 * Check that a path component is valid in the virtual filesystem.
 * Return 0 if valid, return a SG_PATH_EXXX error code if not.
 */
int sg_path_checkpart(const char *restrict p, size_t len);

/*
 * Get the path to the directory containing the executable, including
 * a trailing slash.  On success, return a pointer to the
 * NUL-terminated path, which must be freed with free().  On failure,
 * return NULL.
 */
pchar *sg_path_getexedir(struct sg_error **err);

/*
 * File list builder.
 */
struct sg_filelistb {
    /* List of all files.  */
    struct sg_fileinfo *file;
    size_t filesize;
    size_t filealloc;

    /* List of all directories.  */
    char **dir;
    size_t dirsize;
    size_t diralloc;

    /* Hash set of existing filenames.  */
    struct sg_hashset set;
};

/*
 * Initialize a file list builder.
 */
void sg_filelistb_init(struct sg_filelistb *restrict bp);

/*
 * Destroy a file list builder.
 */
void sg_filelistb_destroy(struct sg_filelistb *restrict bp);

/*
 * Finish constructing a file list.  The builder is destroyed, whether
 * or not this function succeeds.  On success, initialize the list and
 * return 0.  On failure, initialize err and return -1.
 */
int sg_filelistb_finish(struct sg_filelistb *restrict bp,
                        struct sg_filelist *list, struct sg_error **err);

/*
 * Test if a name is already present in the builder.  Return 1 if
 * present, 0 if not present.
 */
int sg_filelistb_hasname(struct sg_filelistb *restrict bp,
                         const char *restrict name);

/*
 * Add a file to the file list builder.  This will make a deep copy of
 * the file info structure.  On success, return 0.  On failure,
 * initialize err and return -1.
 */
int sg_filelistb_addfile(struct sg_filelistb *restrict bp,
                        const struct sg_fileinfo *restrict info,
                        struct sg_error **err);

/*
 * Add a directory to the file list builder.  This will copy the name.
 * On success, return 0.  On failure, initialize err and return -1.
 */
int sg_filelistb_adddir(struct sg_filelistb *restrict bp, const char *name,
                        struct sg_error **err);

/*
 * List the files in a native directory.  Returns SG_FSTATUS_OK,
 * SG_FSTATUS_NOTFOUND, SG_FSTATUS_NOTDIR, or SG_FSTATUS_ERR.
 */
int sg_filelist_scan_native(const pchar *restrict path,
                            struct sg_filelistb *restrict bp,
                            sg_filelist_info_t info, struct sg_error **err);
