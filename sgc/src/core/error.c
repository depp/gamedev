/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "private.h"
#include "sg/error.h"
#include "sg/log.h"
#include "sg/strbuf.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void sg_error_clobber(const struct sg_error_domain *dom, long code,
                             const char *msg) {
    struct sg_error err;
    err.domain = dom;
    err.msg = msg;
    err.code = code;
    sg_logerrs(SG_LOG_ERROR, &err, "Error discarded");
}

void sg_error_set(struct sg_error **err, sg_err_t code) {
    sg_error_sets(err, &SG_ERROR_STANDARD, (long)code, NULL);
}

sg_err_t sg_error_get(struct sg_error *err) {
    if (err && err->domain == &SG_ERROR_STANDARD)
        return (sg_err_t)err->code;
    return SG_ERR_GENERAL;
}

void sg_error_nomem(struct sg_error **err) {
    sg_error_set(err, SG_ERR_NOMEM);
}

void sg_error_setf(struct sg_error **err, const struct sg_error_domain *dom,
                   long code, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    sg_error_setv(err, dom, code, fmt, ap);
    va_end(ap);
}

void sg_error_setv(struct sg_error **err, const struct sg_error_domain *dom,
                   long code, const char *fmt, va_list ap) {
    char buf[512];
#if !defined(_WIN32)
    vsnprintf(buf, sizeof(buf), fmt, ap);
#else
    _vsnprintf_s(buf, sizeof(buf), _TRUNCATE, fmt, ap);
    buf[sizeof(buf) - 1] = '\0';
#endif
    sg_error_sets(err, dom, code, buf);
}

void sg_error_sets(struct sg_error **err, const struct sg_error_domain *dom,
                   long code, const char *msg) {
    struct sg_error *e;
    size_t len;
    if (!err || *err) {
        sg_error_clobber(dom, code, msg);
        return;
    }
    if (msg) {
        len = strlen(msg);
        e = malloc(sizeof(struct sg_error) + len + 1);
        if (!e)
            goto nomem;
        memcpy(e + 1, msg, len + 1);
        e->msg = (char *)(e + 1);
    } else {
        e = malloc(sizeof(struct sg_error));
        if (!e)
            goto nomem;
        e->msg = NULL;
    }
    e->domain = dom;
    e->code = code;
    *err = e;
    return;

nomem:
    fputs("error: out of memory in error handler\n", stderr);
    abort();
}

void sg_error_move(struct sg_error **dest, struct sg_error **src) {
    struct sg_error *d = *dest, *s = *src;
    if (d) {
        if (s) {
            sg_error_clobber(s->domain, s->code, s->msg);
            free(s);
            *src = NULL;
        }
    } else {
        *dest = s;
        *src = NULL;
    }
}

void sg_error_copy(struct sg_error **dest, const struct sg_error *src) {
    sg_error_sets(dest, src->domain, src->code, src->msg);
}

void sg_error_clear(struct sg_error **err) {
    free(*err);
    *err = NULL;
}

static const char *const SG_ERR_MESSAGE[SG_ERR_COUNT] = {
    "an unknown error occurred",
    "cannot allocate memory",
    "file has not changed",
    "no such file or directory",
    "not a directory",
    "is a directory",
    "invalid path",
    "invalid file contents",
    "file too large",
    "invalid argument",
    "feature disabled",
    "image too large",
    "operation not permitted",
};

static void sg_err_message(struct sg_strbuf *buf, const char *msg, long code) {
    (void)msg;
    const char *m;
    if (code < 0 || code >= SG_ERR_COUNT)
        code = 0;
    m = SG_ERR_MESSAGE[code];
    sg_strbuf_puts(buf, m);
}

const struct sg_error_domain SG_ERROR_STANDARD = {"standard", sg_err_message};

#if defined _WIN32
#define COBJMACROS 1
#include <WS2tcpip.h>
#include <Windows.h>
//#include <OAIdl.h>

extern const struct sg_error_domain SG_ERROR_WINDOWS = { "windows" };

static void
sg_error_setw(struct sg_error **err, const struct sg_error_domain *dom,
              long code, const wchar_t *wtext, int wlen)
{
    struct sg_error *e = NULL;
    int alen, r;
    char *atext;

    alen = WideCharToMultiByte(CP_UTF8, 0, wtext, wlen, NULL, 0, NULL, NULL);
    if (!alen)
        goto error;
    e = malloc(sizeof(*e) + alen + 1);
    if (!e)
        goto error;
    atext = (char *) (e + 1);
    r = WideCharToMultiByte(CP_UTF8, 0, wtext, wlen, atext, alen, NULL, NULL);
    if (!r)
        goto error;
    atext[alen] = '\0';

    if (!err || *err) {
        sg_error_clobber(dom, code, atext);
    } else {
        e->refcount = 1;
        e->domain = dom;
        e->msg = atext;
        e->code = code;
        *err = e;
    }
    return;

error:
    fputs("error: out of memory in error handler\n", stderr);
    abort();
}

void
sg_error_win32(struct sg_error **err, unsigned long code)
{
    LPWSTR wtext;
    int len;

    len = FormatMessageW(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        code,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPWSTR) &wtext,
        0, NULL);
    if (!len) {
        sg_error_sets(err, &SG_ERROR_WINDOWS, code,
                      "<error in error handler>");
    } else {
        sg_error_setw(err, &SG_ERROR_WINDOWS, code, wtext, len);
        LocalFree(wtext);
    }
}

/*
    Yes, this is a pain.
    http://stackoverflow.com/questions/4597932/how-can-i-is-there-a-way-to-convert-an-hresult-into-a-system-specific-error-me
    http://stackoverflow.com/questions/455434/how-should-i-use-formatmessage-properly-in-c
*/
void
sg_error_hresult(struct sg_error **err, long code)
{
    IErrorInfo *iei;
    HRESULT hr;
    BSTR bstr;
    WORD facility;

    facility = HRESULT_FACILITY(code);
    hr = GetErrorInfo(0, &iei);
    if (SUCCEEDED(hr) && iei) {
        IErrorInfo_GetDescription(iei, &bstr);
        sg_error_setw(err, &SG_ERROR_WINDOWS, code, bstr + 1, *bstr);
        SysFreeString(bstr);
        IUnknown_Release(iei);
        return;
    }
    if (facility == FACILITY_ITF) {
        sg_error_sets(err, &SG_ERROR_WINDOWS, code,
                      "unknown HRESULT (FACILITY_ITF)");
        return;
    }
    sg_error_win32(err, code);
}

#endif

#if !defined _WIN32

#include <errno.h>
static void sg_error_errno_msg(struct sg_strbuf *buf, const char *msg,
                               long code) {
    char m[256];
    int r;
    (void)msg;
    r = strerror_r(code, m, sizeof(m));
    if (r)
        sg_strbuf_puts(buf, "Unknown error");
    else
        sg_strbuf_puts(buf, m);
}

const struct sg_error_domain SG_ERROR_ERRNO = {"errno", sg_error_errno_msg};

void sg_error_errno(struct sg_error **err, int code) {
    sg_err_t e = SG_ERR_GENERAL;
    switch (code) {
    case ENOMEM:
        e = SG_ERR_NOMEM;
        break;
    case ENOENT:
        e = SG_ERR_NOTFOUND;
        break;
    case ENOTDIR:
        e = SG_ERR_NOTDIR;
        break;
    case EISDIR:
        e = SG_ERR_ISDIR;
        break;
    }
    if (e != SG_ERR_GENERAL)
        sg_error_sets(err, &SG_ERROR_STANDARD, (long)e, NULL);
    else
        sg_error_sets(err, &SG_ERROR_ERRNO, code, NULL);
}

#endif

#if defined _WIN32

void sg_error_gai(struct sg_error **err, int code) {
    const wchar_t *desc = gai_strerror(code);
    sg_error_setw(err, &SG_ERROR_GETADDRINFO, code, desc, wcslen(desc));
}

#else
#include <netdb.h>

static void sg_error_gai_msg(struct sg_strbuf *buf, const char *msg,
                             long code) {
    const char *desc;
    (void) msg;
    desc = gai_strerror(code);
    sg_strbuf_puts(buf, desc);
}

#endif

const struct sg_error_domain SG_ERROR_GETADDRINFO = {"getaddrinfo",
                                                     sg_error_gai_msg};

void sg_error_gai(struct sg_error **err, int code) {
    sg_error_sets(err, &SG_ERROR_GETADDRINFO, code, NULL);
}
