/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

#include <stddef.h>

/* Get OpenGL function pointers.  */
void
sg_gl_getfuncs(const char *names, void **funcs, size_t count);

/* Set up OpenGL debug output.  */
void
sg_gl_debug_init(void);
