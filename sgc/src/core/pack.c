/* Copyright 2013-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#if !defined TEST_PACK || defined PERFTEST
# define NDEBUG 1
#endif

#include "sg/pack.h"
#include "sg/util.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

/*
 * Rectangle packing
 *
 * From Jukka Jylänki's "A Thousand Ways to Pack the Bin", we use the
 * "skyline" algorithm, with bottom-left heuristic for placing the
 * rects, and sorting the rects by width and then by height, with
 * biggest rects placed first.  No waste management is performed.
 */

/*
 * A node in the skyline.  Each node designates a used rectangle,
 * extending from the previous node's x (or 0, for the first node) to
 * this node's x, and extending from y = 0 to this node's y value.
 * The final node will have x equal to the width of the rectangle.
 * There must always be at least one node.
 */
struct sg_packing_node {
    uint16_t x, y;
};

/*
 * A reference to a rectangle.  Used so we can pack an array of
 * rectangles sorted by size.
 */
struct sg_packing_ref {
    struct sg_packing_rect r;
    uint32_t idx;
};

/*
 * Information about rectangles to be packed.
 */
struct sg_packing_info {
    uint32_t min_area;
    ivec2 min_size;
    struct sg_packing_ref *refs;
    size_t count;
};

/*
 * Get the ordering of two rectangles, which should be packed first.
 */
static int sg_packing_refcmp(const struct sg_packing_ref *x,
                             const struct sg_packing_ref *y) {
    if (x->r.w < y->r.w) return 1;
    if (x->r.w > y->r.w) return -1;
    if (x->r.h < y->r.h) return 1;
    if (x->r.h > y->r.h) return -1;
    return x->idx < y->idx ? -1 : 1;
}

/*
 * Sort a list of rectangles.
 */
static void sg_packing_sort(struct sg_packing_ref *restrict refs, size_t count,
                            int depth) {
    if (count <= 10 || depth >= 8) {
        /* Insertion sort */
        for (size_t i = 1; i < count; i++) {
            for (size_t j = 0; j < i; j++) {
                if (sg_packing_refcmp(&refs[i], &refs[j]) < 0) {
                    struct sg_packing_ref t = refs[i];
                    for (size_t k = i; k > j; k--)
                        refs[k] = refs[k - 1];
                    refs[j] = t;
                    break;
                }
            }
        }
    } else {
        /* Quicksort */
        struct sg_packing_ref pivot = refs[count / 2];
        struct sg_packing_ref *p1 = refs, *p2 = refs + count - 1;
        refs[count / 2] = *p2;
        while (1) {
            while (p1 != p2 && sg_packing_refcmp(&pivot, p1) > 0)
                p1++;
            while (p1 != p2 && sg_packing_refcmp(&pivot, p2 - 1) < 0)
                p2--;
            if (p1 == p2) break;
            struct sg_packing_ref t = *p1;
            p2--;
            *p1 = *p2;
            *p2 = t;
            p1++;
        }
        if (p1 != refs) {
            sg_packing_sort(refs, p1 - refs, depth + 1);
        }
        if (p2 != refs + count - 1) {
            refs[count - 1] = *p2;
            sg_packing_sort(p2 + 1, refs + count - 1 - p2, depth + 1);
        }
        *p2 = pivot;
    }
}

/*
 * Prepare rectangles to add to a packing.  Calculate the minimum
 * bounds, the total area, and a sorted version of the array.  Return
 * -1 for failure, 0 if packing is impossible, 1 if successful.
 */
static int sg_packing_prepare(struct sg_packing *restrict pp,
                              struct sg_packing_info *restrict info,
                              struct sg_packing_rect *restrict rects,
                              size_t count) {
    if (count > (unsigned)-1) return 0;
    if (!pp->nodealloc) {
        size_t nalloc = 32;
        pp->node = malloc(sizeof(*pp->node) * nalloc);
        if (!pp->node) return -1;
        pp->nodealloc = nalloc;
    }
    struct sg_packing_ref *refs = malloc(sizeof(*refs) * count);
    if (!refs) return -1;
    uint32_t area = 0, maxw = 0, maxh = 0;
    size_t n = 0;
    for (size_t i = 0; i < count; i++) {
        uint32_t w = rects[i].w, h = rects[i].h;
        if (!w || !h) {
            rects[i].x = 0;
            rects[i].y = 0;
            continue;
        }
        refs[n].r = rects[i];
        refs[n].idx = i;
        n++;
        uint32_t a = w * h;
        if (a > (uint32_t)-1 - area) {
            free(refs);
            return 0;
        }
        area += a;
        maxw = imax(maxw, w);
        maxh = imax(maxh, h);
    }
    info->min_area = area;
    info->min_size.v[0] = maxw;
    info->min_size.v[1] = maxh;
    info->refs = refs;
    info->count = n;
    sg_packing_sort(refs, n, 0);
#if defined TEST_PACK && !defined PERFTEST
    for (size_t i = 0; i + 1 < n; i++) {
        assert(sg_packing_refcmp(&refs[i], &refs[i + 1]) < 0);
    }
    for (size_t i = 0; i < n; i++) {
        if (!rects[i].w || !rects[i].h) continue;
        size_t j = 0;
        while (j < n && refs[j].idx != i)
            j++;
        assert(j < n);
    }
#endif
    return 1;
}

/*
 * Add rectangles to the packing.
 */
static int sg_packing_add2(struct sg_packing *restrict pp,
                           struct sg_packing_ref *restrict refs,
                           struct sg_packing_rect *restrict rects,
                           size_t count) {
    struct sg_packing_node *restrict node = pp->node;
    size_t n = pp->nodesize;
    for (size_t i = 0; i < count; i++) {
        int size_x = refs[i].r.w, size_y = refs[i].r.h;
        int x = 0, max_x = pp->size.v[0] - size_x;
        int best_x = -1, best_y = pp->size.v[1] + 1 - size_y;
        size_t j = 0, first = n, last = n;
        for (; x <= max_x; x = node[j].x, j++) {
            assert(j < n);
            int y = node[j].y;
            if (y >= best_y) continue;
            size_t k = j;
            if (node[j].x < x + size_x) {
                bool success = false;
                for (k++; k < n; k++) {
                    int kx = node[k].x, ky = node[k].y;
                    if (ky > y) {
                        if (ky >= best_y) break;
                        y = ky;
                    }
                    if (kx >= x + size_x) {
                        success = true;
                        break;
                    }
                }
                if (!success) continue;
            }
            first = j;
            last = k;
            best_x = x;
            best_y = y;
        }
        if (first == n) return 0;
        refs[i].r.x = best_x;
        refs[i].r.y = best_y;
        int new_x = best_x + size_x, new_y = best_y + size_y;
        if (best_x < 0) return 0;
        if (first > 0 && node[first].y == new_y) first--;
        if (last < n && node[last].x == new_x) last++;
        if (last >= n || node[last].y != new_y) {
            /* Insert new node. */
            size_t nn = n + 1 - (last - first);
            if (nn > pp->nodealloc) {
                size_t nalloc = pp->nodealloc * 2;
                if (!nalloc) return -1;
                node = realloc(node, nalloc * sizeof(*node));
                if (!node) return -1;
                pp->node = node;
                pp->nodealloc = nalloc;
            }
            memmove(node + first + 1, node + last, sizeof(*node) * (n - last));
            node[first].x = new_x;
            node[first].y = new_y;
            n = nn;
        } else {
            /* Delete existing nodes */
            memmove(node + first, node + last, sizeof(*node) * (n - last));
            n -= last - first;
        }
        pp->nodesize = n;
    }
    for (size_t i = 0; i < count; i++) {
        size_t j = refs[i].idx;
        rects[j].x = refs[i].r.x;
        rects[j].y = refs[i].r.y;
    }
    return 1;
}

void sg_packing_init(struct sg_packing *restrict pp) {
    pp->node = NULL;
    pp->nodesize = 0;
    pp->nodealloc = 0;
    pp->size.v[0] = 0;
    pp->size.v[1] = 0;
}

void sg_packing_destroy(struct sg_packing *restrict pp) {
    free(pp->node);
}

void sg_packing_reset(struct sg_packing *restrict pp, ivec2 size) {
    pp->nodesize = 0;
    pp->size = size;
}

int sg_packing_add(struct sg_packing *restrict pp,
                   struct sg_packing_rect *restrict rects, size_t count) {
    struct sg_packing_info info;
    int r;
    r = sg_packing_prepare(pp, &info, rects, count);
    if (r <= 0) return r;
    if (!pp->nodesize) {
        pp->node[0].x = pp->size.v[0];
        pp->node[0].y = 0;
        pp->nodesize = 1;
    }
    if (info.min_size.v[0] > pp->size.v[0] ||
        info.min_size.v[1] > pp->size.v[1]) {
        free(info.refs);
        return 0;
    }
    r = sg_packing_add2(pp, info.refs, rects, info.count);
    free(info.refs);
    return r;
}

int sg_packing_autopack(struct sg_packing *restrict pp,
                        struct sg_packing_rect *restrict rects, size_t count,
                        ivec2 min_size, ivec2 max_size) {
    if (!count) {
        sg_packing_reset(pp, min_size);
        return 1;
    }
    struct sg_packing_info info;
    int r;
    r = sg_packing_prepare(pp, &info, rects, count);
    if (r <= 0) return r;
    int minsz = imax(ilog2(imax(min_size.v[0], info.min_size.v[0])) * 2,
                     ilog2(imax(min_size.v[1], info.min_size.v[1])) * 2 - 1);
    int maxsz = imin(ilog2(max_size.v[0]) * 2 + 1, ilog2(max_size.v[1]) * 2);
    r = 0;
    for (int i = minsz; i <= maxsz; i++) {
        ivec2 sz = {{1 << (i / 2), 1 << ((i + 1) / 2)}};
        pp->node[0].x = sz.v[0];
        pp->node[0].y = 0;
        pp->nodesize = 1;
        pp->size = sz;
        r = sg_packing_add2(pp, info.refs, rects, info.count);
        if (r) {
            free(info.refs);
            return 1;
        }
    }
    pp->nodesize = 0;
    pp->size.v[0] = 0;
    pp->size.v[1] = 0;
    free(info.refs);
    return 0;
}
