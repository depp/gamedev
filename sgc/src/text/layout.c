/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "private.h"
#include "sg/error.h"
#include "sg/text.h"
#include "sg/util.h"

#include <limits.h>
#include <stdlib.h>
#include <string.h>

static struct sg_textlayout *sg_textlayout_first;

struct sg_textlayout *sg_textlayout_new(struct sg_error **err) {
    struct sg_textlayout *lp = calloc(1, sizeof(*lp));
    if (!lp) {
        sg_error_nomem(err);
        return NULL;
    }
    lp->next = sg_textlayout_first;
    sg_textlayout_first = lp;
    return lp;
}

void sg_textlayout_free(struct sg_textlayout *lp) {
    struct sg_textlayout **p = &sg_textlayout_first;
    while (*p != lp) {
        p = &(*p)->next;
    }
    *p = lp->next;
    free(lp->ptr);
    free(lp);
}

void sg_textlayout_clear(struct sg_textlayout *lp) {
    memset(&lp->metrics, 0, sizeof(lp->metrics));
    lp->size = 0;
    lp->quadsize = 0;
}

const struct sg_textmetrics *sg_textlayout_metrics(struct sg_textlayout *lp) {
    return &lp->metrics;
}

int sg_textlayout_reserve(struct sg_textlayout *restrict lp, int size,
                          struct sg_error **err) {
    if (size <= lp->alloc - lp->size) return 0;
    int maxsize = 1 << 30;
    if (size > maxsize || size > maxsize - lp->size) goto nomem;
    unsigned n = sg_round_up_pow2_32(lp->size + size);
    size_t sz = sizeof(int) * 2 + sizeof(i16vec4) * 2 + sizeof(unsigned short);
    void *ptr = malloc(n * sz);
    if (!ptr) goto nomem;
    char *p = ptr;
    i16vec4 *pos = (void *)p;
    p += sizeof(*pos) * n;
    i16vec4 *sty = (void *)p;
    p += sizeof(*sty) * n;
    int *advance = (void *)p;
    p += sizeof(*advance) * n;
    int *cluster = (void *)p;
    p += sizeof(*cluster) * n;
    unsigned short *bitmap = (void *)p;
    p += sizeof(*bitmap) * n;
    unsigned m = lp->size;
    memcpy(pos, lp->pos, sizeof(*pos) * m);
    memcpy(sty, lp->sty, sizeof(*sty) * m);
    memcpy(advance, lp->advance, sizeof(*advance) * m);
    memcpy(cluster, lp->cluster, sizeof(*cluster) * m);
    memcpy(bitmap, lp->bitmap, sizeof(*bitmap) * m);
    free(lp->ptr);
    lp->pos = pos;
    lp->sty = sty;
    lp->advance = advance;
    lp->cluster = cluster;
    lp->bitmap = bitmap;
    lp->alloc = n;
    return 0;
nomem:
    sg_error_nomem(err);
    return -1;
}

void sg_textlayout_agebitmap(unsigned short *restrict age) {
    for (struct sg_textlayout *p = sg_textlayout_first; p; p = p->next) {
        const unsigned short *restrict b = p->bitmap;
        int n = p->size;
        for (int i = 0; i < n; i++) {
            age[b[i]] = 1;
        }
    }
}
