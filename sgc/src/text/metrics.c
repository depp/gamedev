/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/text.h"

ivec2 sg_textmetrics_anchor(const struct sg_textmetrics *restrict mp,
                            sg_halign_t h, sg_valign_t v) {
    ivec2 r;
    const ibox2 *lb = &mp->logical_bounds;
    switch (h) {
    case SG_HALIGN_LEFT:
        r.v[0] = lb->mins.v[0];
        break;
    case SG_HALIGN_CENTER:
        r.v[0] = lb->mins.v[0] + (lb->maxs.v[0] - lb->mins.v[0]) / 2;
        break;
    case SG_HALIGN_RIGHT:
        r.v[0] = lb->maxs.v[0];
        break;
    }
    switch (v) {
    case SG_VALIGN_BASELINE:
        r.v[1] = mp->baseline;
        break;
    case SG_VALIGN_TOP:
        r.v[1] = lb->maxs.v[1];
        break;
    case SG_VALIGN_CENTER:
        r.v[1] = lb->mins.v[1] + (lb->maxs.v[1] - lb->mins.v[1]) / 2;
        break;
    case SG_VALIGN_BOTTOM:
        r.v[1] = lb->mins.v[1];
        break;
    }
    return r;
}
