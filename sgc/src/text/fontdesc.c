/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/log.h"
#include "sg/text.h"

#include <string.h>

void sg_fontdesc_default(struct sg_fontdesc *desc) {
    memset(desc->family, '\0', sizeof(desc->family));
    desc->size = 16.0f;
    desc->weight = 400;
    desc->style = SG_FONTSTYLE_NORMAL;
}

void sg_fontdesc_setfamily(struct sg_fontdesc *desc, const char *family) {
    memset(desc->family, '\0', sizeof(desc->family));
    size_t namelen = strlen(family);
    if (namelen >= sizeof(desc->family)) {
        sg_logf(SG_LOG_ERROR, "font family too long: %s", family);
        return;
    }
    memcpy(desc->family, family, namelen);
}
