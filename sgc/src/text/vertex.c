/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "private.h"

#include "sg/error.h"
#include "sg/log.h"
#include "sg/pack.h"
#include "sg/text.h"
#include "sg/text_render.h"
#include "sg/util.h"

#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

enum {
    /* Maximum number of text batches.  */
    SG_TEXTBATCH_MAX = 1 << 8,
    /* Maximum number of text groups.  */
    SG_TEXTGROUP_MAX = 1 << 10,
};

struct sg_textvert {
    /*
     * Whether the vertex data has changed since the last update.
     */
    bool dirty;

    /*
     * Index of next element in output buffer.
     */
    int bufpos;

    /*
     * All quads in all groups.  The quad array is split into two, the
     * bitmap index is in quadbitmap and the remaining data is in
     * quadvertex.  The quadsize field records the total number of
     * quads in use, quadpos is where new quads are written, and
     * quadalloc is the end of the array.  At all times, quadsize <=
     * quadpos <= quadalloc.
     */
    unsigned short *quadbitmap;
    struct sg_textvertex_point *quadvertex;
    int quadsize;
    int quadpos;
    int quadalloc;

    /*
     * Information about each segment.  Segments associate groups with
     * quads.
     */
    struct sg_textsegment *segment;
    int segmentsize;
    int segmentalloc;

    /*
     * Information about each group.  This is a sparse array, unused
     * groups will have the batch set to -1.  Handles to groups are
     * indexes into this array.
     */
    struct sg_textgroup *group;
    int groupsize;
    int groupalloc;

    /*
     * Information about each batch.  This is a sparse array, unused
     * batches will have the 'used' field set to 0.  Handles to
     * batches are indexes into this array.
     */
    struct sg_textbatch *batch;
    struct sg_crange *batchrange;
    int batchsize;
    int batchalloc;
};

static struct sg_textvert sg_textvert;

struct sg_textsegment {
    /* Index of the first quad in the core quad array.  */
    int srcoffset;
    /* Index of the first element in the output data.  */
    int destoffset;
    /* Number of quads in this part of the segment.  */
    int size;
    /* ID of the group containing this segment.  */
    int group;
    /* ID of the batch containing the group containing this segment.  */
    int batch;
    /* Whether the segment texture coordinates need to be updated.  */
    bool dirtytex;
};

struct sg_textgroup {
    /* ID of the batch containing this group, or -1.  */
    int batch;
    /* Current offset applied to glyphs in this group.  */
    ivec2 offset;
    /* Temporary value.  */
    int temp;
};

struct sg_textbatch {
    /* Whether this batch is in use.  */
    bool used;
    /* Whether the vertex data for this batch must be updated.  */
    bool dirty;
    /* Number of quads in this batch.  */
    int size;
    /* Temporary value.  */
    int temp;
};

/*
 * =====================================================================
 * Quad management
 * =====================================================================
 */

/*
 * Sort segments by batch, then group.
 */
static int sg_textvert_sortsegment(struct sg_error **err) {
    struct sg_textbatch *restrict batch = sg_textvert.batch;
    struct sg_textgroup *restrict group = sg_textvert.group;
    struct sg_textsegment *restrict segment = sg_textvert.segment;
    int nbatch = sg_textvert.batchalloc, ngroup = sg_textvert.groupalloc,
        nsegment = sg_textvert.segmentsize;
    for (int i = 0; i < nbatch; i++) {
        batch[i].temp = 0;
    }
    for (int i = 0; i < ngroup; i++) {
        group[i].temp = 0;
    }
    for (int i = 0; i < nsegment; i++) {
        group[segment[i].group].temp++;
    }
    for (int i = 0; i < ngroup; i++) {
        if (group[i].batch >= 0) {
            batch[group[i].batch].temp += group[i].temp;
        }
    }
    int pos = 0;
    for (int i = 0; i < nbatch; i++) {
        int n = batch[i].temp;
        batch[i].temp = pos;
        pos += n;
    }
    for (int i = 0; i < ngroup; i++) {
        int b = group[i].batch;
        if (b < 0) continue;
        int n = group[i].temp;
        int pos = batch[b].temp;
        group[i].temp = pos;
        batch[b].temp = pos + n;
    }
    struct sg_textsegment *restrict segment2 =
        malloc(sg_textvert.segmentalloc * sizeof(*segment2));
    if (!segment2) goto nomem;
    for (int i = 0; i < nsegment; i++) {
        int g = segment[i].group;
        int pos = group[g].temp++;
        segment2[pos] = segment[i];
    }
    free(segment);
    sg_textvert.segment = segment2;
    return 0;
nomem:
    sg_error_nomem(err);
    return -1;
}

/*
 * Garbage collect the text quads.  If necessary, this will compact
 * the array of quads to reclaim unused memory.  At least minspace
 * quads will be available at the end of the array when this returns.
 */
static int sg_textvert_gcquad(int minspace, struct sg_error **err) {
    int r;
    r = sg_textvert_sortsegment(err);
    if (r) return -1;
    int qsize = sg_textvert.quadsize, newqsize = qsize + minspace,
        qalloc = sg_textvert.quadalloc;
    /* Heuristic: increase size if free space < 1/4 total space.  */
    if (newqsize > qalloc - (qalloc >> 2)) {
        unsigned x = sg_round_up_pow2_32(newqsize + (newqsize >> 2));
        if ((unsigned)newqsize > x - (x >> 2)) x *= 2;
        if (!x || x > INT_MAX) goto nomem;
        qalloc = x;
    }
    sg_logf(SG_LOG_DEBUG, "GC text vertex: size: %d -> %d, alloc %d -> %d",
            sg_textvert.quadpos, sg_textvert.quadsize, sg_textvert.quadalloc,
            qalloc);
    unsigned short *restrict qb2 = malloc(qalloc * sizeof(*qb2));
    if (!qb2) goto nomem;
    struct sg_textvertex_point *restrict qv2 = malloc(qalloc * sizeof(*qv2));
    if (!qv2) {
        free(qb2);
        goto nomem;
    }
    struct sg_textsegment *restrict segment = sg_textvert.segment;
    unsigned short *restrict qb1 = sg_textvert.quadbitmap;
    struct sg_textvertex_point *restrict qv1 = sg_textvert.quadvertex;
    int pos = 0, csize = 0, nsegment = sg_textvert.segmentsize, j = 0;
    bool cdirtytex = false;
    for (int i = 0; i < nsegment; i++) {
        int size = segment[i].size;
        bool dirtytex = segment[i].dirtytex;
        memcpy(qb2 + pos, qb1 + segment[i].srcoffset, size * sizeof(*qb2));
        memcpy(qv2 + pos, qv1 + segment[i].srcoffset, size * sizeof(*qv2));
        if (j > 0 && segment[j - 1].group == segment[i].group) {
            csize += size;
            cdirtytex |= dirtytex;
            segment[j - 1].size = csize;
            segment[j - 1].dirtytex = cdirtytex;
        } else {
            csize = size;
            cdirtytex = dirtytex;
            if (j < i) {
                segment[j] = segment[i];
            }
            segment[j].srcoffset = pos;
            j++;
        }
        pos += size;
    }
    sg_textvert.segmentsize = j;
    assert(pos == qsize);
    free(qb1);
    free(qv1);
    sg_textvert.quadbitmap = qb2;
    sg_textvert.quadvertex = qv2;
    sg_textvert.quadpos = qsize;
    sg_textvert.quadalloc = qalloc;
    return 0;
nomem:
    sg_error_nomem(err);
    return -1;
}

/*
 * =====================================================================
 * Renderer / update functions
 * =====================================================================
 */

/*
 * Update the position of all dirty batches, and mark them as clean.
 * The 'qelem' parameter is the number of array elements per quad.
 */
static void sg_textrender_updatebatch(int qelem) {
    int bpos = sg_textvert.bufpos;
    {
        struct sg_textbatch *restrict b = sg_textvert.batch;
        struct sg_crange *restrict br = sg_textvert.batchrange;
        for (int i = 0; i < sg_textvert.batchalloc; i++) {
            if (b->dirty) {
                int n = b[i].size * qelem;
                b[i].temp = bpos;
                br[i].first = bpos;
                br[i].count = n;
                bpos += n;
            }
        }
    }
    for (int i = 0; i < sg_textvert.segmentsize; i++) {
        struct sg_textsegment *s = &sg_textvert.segment[i];
        struct sg_textbatch *b = &sg_textvert.batch[s->batch];
        if (!b->dirty) continue;
        int spos = b->temp;
        s->destoffset = spos;
        b->temp = spos + s->size;
    }
    sg_textvert.bufpos = bpos;
}

int sg_textrender_updatevertex(struct sg_textrender_vertex *restrict vp,
                               sg_textvertex_t format, bool full_update,
                               int buf_size, int align) {
    vp->batch_count = sg_textvert.batchalloc;
    vp->batch = sg_textvert.batchrange;
    if (!full_update && !sg_textvert.dirty) {
        vp->byte_changed.first = 0;
        vp->byte_changed.count = 0;
        return 1;
    }
    int elemsize = 0, qelem = 0;
    switch (format) {
    case SG_TEXTVERTEX_TRI:
        elemsize = sizeof(struct sg_textvertex_tri);
        qelem = 6;
        break;
    case SG_TEXTVERTEX_POINT:
        elemsize = sizeof(struct sg_textvertex_point);
        qelem = 1;
        break;
    }
    bool success = false;
    int byte0, byte1;
    if (!full_update) {
        byte0 = sg_align(sg_textvert.bufpos * elemsize, align);
        sg_textvert.bufpos = (byte0 + elemsize - 1) / elemsize;
        sg_textrender_updatebatch(qelem);
        success = sg_textvert.bufpos * elemsize <= buf_size;
    }
    if (!success) {
        byte0 = 0;
        sg_textvert.bufpos = 0;
        for (int i = 0; i < sg_textvert.batchalloc; i++) {
            sg_textvert.batch[i].dirty = true;
        }
        sg_textrender_updatebatch(qelem);
        success = sg_textvert.bufpos * elemsize <= buf_size;
    }
    byte1 = sg_align(sg_textvert.bufpos * elemsize, align);
    for (int i = 0; i < sg_textvert.batchalloc; i++) {
        sg_textvert.batch[i].dirty = false;
    }
    sg_textvert.dirty = false;
    vp->byte_changed.first = byte0;
    vp->byte_changed.count = imin(byte1, buf_size) - byte0;
    return success ? 1 : 0;
}

void sg_textrender_vertexdata(void *restrict ptr, sg_textvertex_t format,
                              size_t offset, size_t size) {
    size_t elemsize = 0;
    switch (format) {
    case SG_TEXTVERTEX_TRI:
        elemsize = sizeof(struct sg_textvertex_tri);
        break;
    case SG_TEXTVERTEX_POINT:
        elemsize = sizeof(struct sg_textvertex_point);
        break;
    }
    int voffset = (offset + elemsize - 1) / elemsize,
        vsize = (offset + size) / elemsize - voffset;
    void *vert = (char *)ptr + (voffset * elemsize - offset);
    const struct sg_textvertex_point *restrict quad = sg_textvert.quadvertex;
    const struct sg_textsegment *restrict seg = sg_textvert.segment;
    for (int i = 0, n = sg_textvert.segmentsize; i < n; i++) {
        int segsrc = seg[i].srcoffset, segdest = seg[i].destoffset,
            segsize = seg[i].size;
        if (segdest < voffset) {
            int delta = voffset - segdest;
            segsrc += delta;
            segdest = 0;
            segsize -= delta;
        } else {
            segdest -= voffset;
        }
        if (segdest >= vsize) continue;
        if (segsize > vsize - segdest) {
            segsize = vsize - segdest;
        }
        if (segsize <= 0) continue;
        switch (format) {
        case SG_TEXTVERTEX_TRI:
            for (int j = 0; j < segsize; j++) {
                struct sg_textvertex_tri *v =
                    (struct sg_textvertex_tri *)vert + segdest + j * 6;
                const struct sg_textvertex_point *restrict q =
                    quad + segsrc + j;

                v[0].pos.v[0] = q->pos.v[0];
                v[0].pos.v[1] = q->pos.v[1];
                v[0].tex.v[0] = q->tex.v[0];
                v[0].tex.v[1] = q->tex.v[1];
                v[0].sty = q->sty;

                v[1].pos.v[0] = q->pos.v[2];
                v[1].pos.v[1] = q->pos.v[1];
                v[1].tex.v[0] = q->tex.v[2];
                v[1].tex.v[1] = q->tex.v[1];
                v[1].sty = q->sty;

                v[2].pos.v[0] = q->pos.v[0];
                v[2].pos.v[1] = q->pos.v[3];
                v[2].tex.v[0] = q->tex.v[0];
                v[2].tex.v[1] = q->tex.v[3];
                v[2].sty = q->sty;

                v[3].pos.v[0] = q->pos.v[0];
                v[3].pos.v[1] = q->pos.v[3];
                v[3].tex.v[0] = q->tex.v[0];
                v[3].tex.v[1] = q->tex.v[3];
                v[3].sty = q->sty;

                v[4].pos.v[0] = q->pos.v[2];
                v[4].pos.v[1] = q->pos.v[1];
                v[4].tex.v[0] = q->tex.v[2];
                v[4].tex.v[1] = q->tex.v[1];
                v[4].sty = q->sty;

                v[5].pos.v[0] = q->pos.v[2];
                v[5].pos.v[1] = q->pos.v[3];
                v[5].tex.v[0] = q->tex.v[2];
                v[5].tex.v[1] = q->tex.v[3];
                v[5].sty = q->sty;
            }
            break;
        case SG_TEXTVERTEX_POINT:
            memcpy((struct sg_textvertex_point *)vert + segdest, quad + segsrc,
                   segsize * sizeof(struct sg_textvertex_point));
            break;
        }
    }
}

void sg_textvert_updatebitmap(const struct sg_packing_rect *restrict bitmap) {
    for (int i = 0; i < sg_textvert.segmentsize; i++) {
        if (!sg_textvert.segment[i].dirtytex) continue;
        int off = sg_textvert.segment[i].srcoffset;
        int n = sg_textvert.segment[i].size;
        struct sg_textvertex_point *restrict qv = sg_textvert.quadvertex + off;
        const unsigned short *restrict qb = sg_textvert.quadbitmap + off;
        for (int i = 0; i < n; i++) {
            struct sg_packing_rect r = bitmap[qb[i]];
            qv[i].tex.v[0] = r.x;
            qv[i].tex.v[1] = r.y + r.h;
            qv[i].tex.v[2] = r.x + r.w;
            qv[i].tex.v[3] = r.y;
        }
        sg_textvert.segment[i].dirtytex = false;
    }
}

void sg_textvert_mapbitmap(const unsigned short *restrict map) {
    sg_textvert.dirty = true;
    for (int i = 0; i < sg_textvert.batchalloc; i++) {
        sg_textvert.batch[i].dirty = true;
    }
    for (int i = 0; i < sg_textvert.segmentsize; i++) {
        sg_textvert.segment[i].dirtytex = true;
        int off = sg_textvert.segment[i].srcoffset;
        int n = sg_textvert.segment[i].size;
        unsigned short *restrict qb = sg_textvert.quadbitmap + off;
        for (int i = 0; i < n; i++) {
            qb[i] = map[qb[i]];
        }
    }
}

void sg_textvert_agebitmap(unsigned short *restrict age) {
    for (int i = 0; i < sg_textvert.segmentsize; i++) {
        int off = sg_textvert.segment[i].srcoffset;
        int n = sg_textvert.segment[i].size;
        const unsigned short *restrict qb = sg_textvert.quadbitmap + off;
        for (int i = 0; i < n; i++) {
            age[qb[i]] = 0;
        }
    }
}

/*
 * =====================================================================
 * Text batch functions
 * =====================================================================
 */

static bool sg_textbatch_valid(int batch) {
    return batch >= 0 && batch < sg_textvert.batchalloc &&
           sg_textvert.batch[batch].used;
}

int sg_textbatch_new(struct sg_error **err) {
    int n = sg_textvert.batchalloc, i;
    struct sg_textbatch *b = sg_textvert.batch;
    for (i = 0; i < n; i++) {
        if (!b[i].used) {
            break;
        }
    }
    if (i == n) {
        if (n == SG_TEXTBATCH_MAX) goto nomem;
        int nalloc = n ? n * 2 : 1;
        b = realloc(b, nalloc * sizeof(*b));
        if (!b) goto nomem;
        sg_textvert.batch = b;
        struct sg_crange *br =
            realloc(sg_textvert.batchrange, nalloc * sizeof(*br));
        if (!br) goto nomem;
        sg_textvert.batchrange = br;
        sg_textvert.batchalloc = nalloc;
        memset(b + n, 0, sizeof(*b) * (nalloc - n));
        memset(br + n, 0, sizeof(*br) * (nalloc - n));
    }
    b[i].used = true;
    sg_textvert.batchsize++;
    return i;
nomem:
    sg_error_nomem(err);
    return -1;
}

void sg_textbatch_free(int batch) {
    if (!sg_textbatch_valid(batch)) {
        /* FIXME: error */
        return;
    }
    /* Free the batch.  */
    sg_textvert.batch[batch].used = false;
    sg_textvert.batch[batch].size = 0;
    /* Free all groups in this batch.  */
    {
        struct sg_textgroup *s = sg_textvert.group;
        int n = sg_textvert.groupalloc, count = sg_textvert.groupsize;
        for (int i = 0; i < n; i++) {
            if (s[i].batch == batch) {
                s[i].batch = -1;
                count--;
            }
        }
        sg_textvert.groupsize = count;
    }
    /* Free all segments in this batch.  */
    {
        struct sg_textsegment *s = sg_textvert.segment,
                              *e = s + sg_textvert.segmentsize;
        while (s != e && s->batch != batch) {
            s++;
        }
        struct sg_textsegment *p = s;
        for (; p != e; p++) {
            if (p->batch != batch) {
                *s = *p;
                s++;
            }
        }
        sg_textvert.segmentsize = s - sg_textvert.segment;
    }
}

/*
 * =====================================================================
 * Text group functions
 * =====================================================================
 */

static bool sg_textgroup_valid(int group) {
    return group >= 0 && group < sg_textvert.groupalloc &&
           sg_textvert.group[group].batch != -1;
}

int sg_textgroup_new(int batch, struct sg_error **err) {
    if (batch < 0) {
        sg_error_set(err, SG_ERR_INVAL);
        return -1;
    }
    int n = sg_textvert.groupalloc, i;
    struct sg_textgroup *s = sg_textvert.group;
    for (i = 0; i < n; i++) {
        if (s[i].batch == -1) {
            break;
        }
    }
    if (i == n) {
        if (n == SG_TEXTGROUP_MAX) goto nomem;
        int nalloc = n ? n * 2 : 1;
        s = realloc(s, nalloc * sizeof(*s));
        if (!s) goto nomem;
        sg_textvert.group = s;
        sg_textvert.groupalloc = nalloc;
        for (int j = n + 1; j < nalloc; j++) {
            s[j].batch = -1;
        }
    }
    s[i].batch = batch;
    s[i].offset.v[0] = 0;
    s[i].offset.v[1] = 0;
    sg_textvert.groupsize++;
    return i;
nomem:
    sg_error_nomem(err);
    return -1;
}

void sg_textgroup_free(int group) {
    if (!sg_textgroup_valid(group)) {
        /* FIXME: error */
        return;
    }
    /* Free the group.  */
    struct sg_textbatch *batch =
        &sg_textvert.batch[sg_textvert.group[group].batch];
    sg_textvert.dirty = true;
    sg_textvert.group[group].batch = -1;
    sg_textvert.groupsize--;
    /* Free the segments in this group.  */
    int size = batch->size;
    {
        struct sg_textsegment *s = sg_textvert.segment,
                              *e = s + sg_textvert.segmentsize;
        while (s != e && s->group != group) {
            s++;
        }
        struct sg_textsegment *p = s;
        for (; p != e; p++) {
            if (p->group == group) {
                size -= p->size;
            } else {
                *s = *p;
                s++;
            }
        }
        sg_textvert.segmentsize = s - sg_textvert.segment;
    }
    batch->dirty = true;
    batch->size = size;
}

void sg_textgroup_setpos(int group, ivec2 offset) {
    if (!sg_textgroup_valid(group)) {
        /* FIXME: error */
        return;
    }
    struct sg_textgroup *g = &sg_textvert.group[group];
    int dx = offset.v[0] - g->offset.v[0], dy = offset.v[1] - g->offset.v[1];
    if (dx == 0 && dy == 0) {
        return;
    }
    g->offset = offset;
    sg_textvert.batch[g->batch].dirty = true;
    sg_textvert.dirty = true;
    struct sg_textvertex_point *qv = sg_textvert.quadvertex;
    for (struct sg_textsegment *s = sg_textvert.segment,
                               *e = s + sg_textvert.segmentsize;
         s != e; s++) {
        if (s->group != group) continue;
        int off = s->srcoffset, size = s->size;
        for (int i = 0; i < size; i++) {
            qv[i + off].pos.v[0] += dx;
            qv[i + off].pos.v[1] += dy;
            qv[i + off].pos.v[2] += dx;
            qv[i + off].pos.v[3] += dy;
        }
    }
}

int sg_textgroup_add(int group, const struct sg_textlayout *restrict layout,
                     ivec2 offset, struct sg_error **err) {
    if (!sg_textgroup_valid(group)) {
        sg_error_set(err, SG_ERR_INVAL);
        return -1;
    }
    int nquad = layout->quadsize, nglyph = layout->size;
    if (!nquad) {
        return 0;
    }
    int batch = sg_textvert.group[group].batch;
    if (nquad > sg_textvert.quadalloc - sg_textvert.quadpos) {
        int r = sg_textvert_gcquad(nquad, err);
        if (r) return -1;
    }
    if (sg_textvert.segmentsize >= sg_textvert.segmentalloc) {
        unsigned nalloc = sg_round_up_pow2_32(sg_textvert.segmentalloc + 1);
        if (!nalloc || nalloc > INT_MAX) goto nomem;
        struct sg_textsegment *narr =
            realloc(sg_textvert.segment, nalloc * sizeof(*narr));
        if (!narr) goto nomem;
        sg_textvert.segment = narr;
        sg_textvert.segmentalloc = nalloc;
    }
    struct sg_textsegment *s = &sg_textvert.segment[sg_textvert.segmentsize++];
    int qoffset = sg_textvert.quadpos;
    s->srcoffset = qoffset;
    s->destoffset = -1;
    s->size = nquad;
    s->group = group;
    s->batch = batch;
    s->dirtytex = true;
    const unsigned short *restrict gbit = layout->bitmap;
    const i16vec4 *restrict gpos = layout->pos;
    const i16vec4 *restrict gsty = layout->sty;
    struct sg_textvertex_point *restrict qv = sg_textvert.quadvertex + qoffset;
    unsigned short *restrict qb = sg_textvert.quadbitmap + qoffset;
    int dx = offset.v[0] + sg_textvert.group[group].offset.v[0],
        dy = offset.v[1] + sg_textvert.group[group].offset.v[1];
    for (int i = 0; i < nglyph; i++) {
        int bi = gbit[i];
        if (bi == SG_TEXTBITMAP_ZERO) {
            continue;
        }
        qv->pos.v[0] = gpos[i].v[0] + dx;
        qv->pos.v[1] = gpos[i].v[1] + dy;
        qv->pos.v[2] = gpos[i].v[2] + dx;
        qv->pos.v[3] = gpos[i].v[3] + dy;
        qv->sty = gsty[i];
        *qb = bi;
        qv++;
        qb++;
    }
    assert(qv == sg_textvert.quadvertex + qoffset + nquad &&
           qb == sg_textvert.quadbitmap + qoffset + nquad);
    sg_textvert.batch[batch].dirty = true;
    sg_textvert.batch[batch].size += nquad;
    sg_textvert.dirty = true;
    sg_textvert.quadpos = qoffset + nquad;
    sg_textvert.quadsize += nquad;
    return 0;
nomem:
    sg_error_nomem(err);
    return -1;
}
