/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

/*
 * This is a separate file from private.h in order to avoid
 * accidentally calling these functions instead of the appropriate
 * sg_font functions, and to avoid including "config.h" everywhere.
 */

#include "config.h"
#include "sg/u32buf.h"

struct sg_error;
struct sg_fontdesc;
struct sg_fontref;
struct sg_fontmetrics;
struct sg_textlayout;

#if defined ENABLE_FREETYPE

/*
 * =====================================================================
 * FreeType fonts
 * =====================================================================
 */

/* FreeType version of sg_font_mapbitmap for one font.  */
void sg_ftfont_mapbitmap(void *font, const unsigned short *restrict map);

/*
 * Get the description of the font in the given data buffer.  On
 * success, fill desc and return 0.  On failure, set err and return
 * -1.
 */
int sg_ftfont_getdesc(struct sg_fontdesc *desc, const void *data, size_t size,
                      struct sg_error **err);

/*
 * Load a FreeType font with the given data.  The data must last as
 * long as the font lasts.  On success, return a FreeType font.  On
 * failure, set err and return NULL.
 */
void *sg_ftfont_load(const void *data, size_t size, struct sg_error **err);

/*
 * Initialize a font ref to match the given specification.  The
 * FreeType font will already match the family, style, and weight in
 * the description.  On success, initialize the ref and return 0.  On
 * failure, initialize err and return -1.
 */
int sg_ftfont_get(void *ftfont, struct sg_fontref *font,
                  const struct sg_fontdesc *desc, struct sg_error **err);

/* FreeType version of sg_font_getmetrics.  */
int sg_ftfont_getmetrics(const struct sg_fontref *restrict font,
                         struct sg_fontmetrics *mp, struct sg_error **err);

/* FreeType version of sg_font_shape().  */
int sg_ftfont_shape(struct sg_fontref *restrict font,
                    struct sg_textlayout *restrict layout,
                    const char32_t *restrict text, int size,
                    struct sg_error **err);

/* Set the error to a FreeType error code.  */
void sg_fterror(struct sg_error **err, int code);

#endif
