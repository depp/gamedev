/* Copyright 2015-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "attr.h"
#include "private.h"
#include "sg/error.h"
#include "sg/range.h"
#include "sg/text.h"
#include "sg/util.h"
#include "sg/u32buf.h"
#include "sg/util.h"

#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

/*
 * Typesetting penalties: We choose the set of line breaks that
 * globally minimizes the sum of all line break penalties.  These
 * scaling constants affect the computation of line break penalties.
 */

/*
 * Penalty for each line break.  Each line break incurs this penalty.
 */
static const float PENALTY_BREAK = 1.0f;

/*
 * Penalty for ragged lines not matching the target width.  Let A be
 * the difference between the line width and target width, measured in
 * line heights.  That difference is squared and multiplied by this
 * penalty.
 */
static const float PENALTY_RAGGED = 20.0f;

/*
 * Penalty for overwide lines.  Each line break wider than the target
 * width incurs this penalty.
 */
static const float PENALTY_OVERWIDE = 2000.0f;

/*
 * Text builder object.
 */
struct sg_textbuilder {
    /* The first error that occurred while content was added.  */
    struct sg_error *err;

    /* Text contents in UTF-32.  */
    struct sg_u32buf text;

    /* Style runs.  */
    struct sg_textrun *run;
    int runsize;
    int runalloc;

    /* Array for line break attributes for each character.  */
    unsigned char *attr;
    int attralloc;

    /* Width, in pixels, of the layout being built.  */
    int width;

    /* Line break opportunity array.  */
    struct sg_textbreak *tbreak;
    int tbreaksize;
    int tbreakalloc;
};

/*
 * Vertical text metrics.  See CSS 2.1:
 *
 * https://www.w3.org/TR/CSS2/visudet.html#propdef-line-height
 */
struct sg_textvmetrics {
    /* Font metrics (descender, ascender).  */
    int font_y0, font_y1;
    /* Line metrics, including line height.  */
    int line_y0, line_y1;
};

/*
 * A run of text with the same style.
 */
struct sg_textrun {
    /* Range of code points in the run, end not always valid.  */
    int tstart, tend;
    /* Range of glyphs in the run.  */
    int gstart, gend;
    /* User-defined style information.  */
    int style;
    /* The font used for this run.  */
    struct sg_fontdesc fdesc;
    struct sg_fontref font;
    /* Vertical metrics for this run.  */
    struct sg_textvmetrics metrics;
};

/*
 * Types of line break opportunities.
 */
typedef enum {
    /*
     * The line break is optional, and starts a new line if chosen.
     */
    SG_TEXTBREAK_OPTIONAL,

    /*
     * The line break is mandatory, and starts a new line.
     */
    SG_TEXTBREAK_LINE,

    /*
     * The line break is mandatory, and starts a new paragraph.
     */
    SG_TEXTBREAK_PARAGRAPH
} sg_textbreaktype_t;

/*
 * A line break opportunity in a text builder.
 */
struct sg_textbreak {
    /* Type of line break opportunity.  */
    sg_textbreaktype_t type;

    /* Index into glyph array of the first glyph after the break.  */
    int glyph_index;

    /*
     * Pen advance since the last break opportunity, and advance since
     * the last break opportunity with trailing white space trimmed.
     */
    int advance, advance_trimmed;

    /*
     * Index in the line break array of the previous line break that
     * should be chosen, if this break is chosen.
     */
    int previous;

    /*
     * Total penalty for choosing this line break, including penalties
     * for previous line breaks.
     */
    float penalty;

    /* Index of the run containing the previous and next character.  */
    int rprev, rnext;

    /* Vertical extent of the line.  */
    int y0, y1;

    /* Initial location of the pen for the previous line.  */
    ivec2 pos;
};

static struct sg_textrun *sg_textbuilder_newrun(struct sg_textbuilder *restrict
                                                    bp) {
    if (bp->err) {
        return NULL;
    }
    struct sg_textrun *rp = &bp->run[bp->runsize - 1];
    size_t pos = sg_u32buf_len(&bp->text);
    if ((size_t)rp->tstart != pos) {
        if (pos > INT_MAX) {
            sg_error_nomem(&bp->err);
            return NULL;
        }
        if (bp->runsize >= bp->runalloc) {
            unsigned nalloc = sg_round_up_pow2_32(bp->runsize + 1);
            if (!nalloc || nalloc > INT_MAX) {
                sg_error_nomem(&bp->err);
                return NULL;
            }
            struct sg_textrun *na = realloc(bp->run, nalloc * sizeof(*na));
            if (!na) {
                sg_error_nomem(&bp->err);
                return NULL;
            }
            bp->run = na;
            bp->runalloc = nalloc;
        }
        struct sg_textrun *op = &bp->run[bp->runsize - 1];
        rp = op + 1;
        *rp = *op;
        bp->runsize++;
    }
    return rp;
}

struct sg_textbuilder *sg_textbuilder_new(struct sg_error **err) {
    struct sg_textbuilder *restrict bp = calloc(1, sizeof(*bp));
    if (!bp) {
        sg_error_nomem(err);
        return NULL;
    }
    struct sg_textrun *restrict tr = malloc(sizeof(*tr));
    if (!tr) {
        free(bp);
        sg_error_nomem(err);
        return NULL;
    }
    sg_u32buf_init(&bp->text);
    bp->run = tr;
    bp->runsize = 1;
    bp->runalloc = 1;
    tr->tstart = 0;
    tr->style = 0;
    sg_fontdesc_default(&tr->fdesc);
    memset(&tr->font, 0, sizeof(tr->font));
    return bp;
}

void sg_textbuilder_free(struct sg_textbuilder *restrict bp) {
    sg_error_clear(&bp->err);
    sg_u32buf_destroy(&bp->text);
    free(bp->run);
    free(bp->attr);
    free(bp);
}

void sg_textbuilder_clear(struct sg_textbuilder *restrict bp) {
    sg_error_clear(&bp->err);
    sg_u32buf_clear(&bp->text);
    bp->width = 0;
    bp->runsize = 1;
    struct sg_textrun *restrict tr = bp->run;
    tr->tstart = 0;
    tr->style = 0;
    sg_fontdesc_default(&tr->fdesc);
}

void sg_textbuilder_write(struct sg_textbuilder *restrict bp, const char *data,
                          size_t size) {
    if (bp->err) return;
    int r = sg_u32buf_write(&bp->text, data, size);
    if (r < 0) {
        sg_error_nomem(&bp->err);
    }
}

void sg_textbuilder_putc(struct sg_textbuilder *restrict bp, int c) {
    if (bp->err) return;
    int r = sg_u32buf_putc(&bp->text, c);
    if (r < 0) {
        sg_error_nomem(&bp->err);
    }
}

void sg_textbuilder_puts(struct sg_textbuilder *restrict bp, const char *text) {
    sg_textbuilder_write(bp, text, strlen(text));
}

void sg_textbuilder_endline(struct sg_textbuilder *restrict bp) {
    /* U+2028 LINE SEPARATOR */
    sg_textbuilder_putc(bp, 0x2028);
}

void sg_textbuilder_endparagraph(struct sg_textbuilder *restrict bp) {
    /* U+2029 PARAGRAPH SEPARATOR */
    sg_textbuilder_putc(bp, 0x2029);
}

void sg_textbuilder_setfont(struct sg_textbuilder *restrict bp,
                            const struct sg_fontdesc *restrict font) {
    struct sg_textrun *r = sg_textbuilder_newrun(bp);
    if (r) {
        r->fdesc = *font;
    }
}

void sg_textbuilder_setwidth(struct sg_textbuilder *restrict bp, int width) {
    bp->width = width;
}

void sg_textbuilder_setstyle(struct sg_textbuilder *restrict bp, int style) {
    struct sg_textrun *r = sg_textbuilder_newrun(bp);
    if (r) {
        r->style = style;
    }
}

/*
 * Append a line break opportunity onto the end of the line break
 * opportunity array.  On success, return 0.  On failure, set err and
 * return -1.
 */
static int sg_textbuilder_pushbreak(struct sg_textbuilder *restrict bp,
                                    const struct sg_textbreak *restrict br,
                                    struct sg_error **err) {
    if (bp->tbreaksize >= bp->tbreakalloc) {
        unsigned nalloc = bp->tbreakalloc ? (unsigned)bp->tbreakalloc * 2 : 4u;
        if (nalloc > INT_MAX) goto nomem;
        struct sg_textbreak *narr = realloc(bp->tbreak, nalloc * sizeof(*narr));
        if (!narr) goto nomem;
        bp->tbreak = narr;
        bp->tbreakalloc = nalloc;
    }
    bp->tbreak[bp->tbreaksize++] = *br;
    return 0;
nomem:
    sg_error_nomem(err);
    return 1;
}

/*
 * Solve for line breaks.  Break candidates in the half-open range
 * first-last are analyzed.  The first and last candidates are always
 * chosen.  A subset of the line breaks between are chosen, and the
 * rest are removed from the array.  This uses the algorithm from
 * Knuth and Plass (1981) which uses dynamic programming.
 */
static void sg_textbuilder_breaksolve(struct sg_textbuilder *restrict bp,
                                      int first) {
    int target_width = bp->width;
    struct sg_textbreak *restrict br = bp->tbreak;
    const struct sg_textrun *restrict run = bp->run;
    int last = bp->tbreaksize;
    for (int i = first + 1; i < last; i++) {
        int width = br[i].advance_trimmed, ridx = br[i].rprev,
            y0 = run[ridx].metrics.line_y0, y1 = run[ridx].metrics.line_y1;
        float best_penalty = INFINITY;
        for (int j = i - 1; j >= first; j--) {
            for (int nridx = br[j].rnext; ridx > nridx; ridx--) {
                const struct sg_textvmetrics *restrict mp =
                    &run[ridx - 1].metrics;
                y0 = imin(y0, mp->line_y0);
                y1 = imax(y1, mp->line_y1);
            }
            float penalty = br[j].penalty + PENALTY_BREAK;
            float delta = (float)(width - target_width) / (float)(y1 - y0);
            if (br[i].type == SG_TEXTBREAK_PARAGRAPH) {
                if (width > target_width) {
                    penalty += delta * delta * PENALTY_RAGGED;
                    penalty += PENALTY_OVERWIDE;
                }
            } else {
                penalty += delta * delta * PENALTY_RAGGED;
                if (width > target_width) {
                    penalty += PENALTY_OVERWIDE;
                }
            }
            if (penalty < best_penalty || j == i - 1) {
                best_penalty = penalty;
                br[i].previous = j;
                br[i].penalty = penalty;
                br[i].y0 = y0;
                br[i].y1 = y1;
                br[i].pos.v[0] = 0;
                br[i].pos.v[1] = br[j].pos.v[1] + br[i].y0 - br[i].y1;
            }
            width += br[j].advance;
        }
    }
    for (int i = br[last - 1].previous; i > first; i = br[i].previous) {
        br[i].type = SG_TEXTBREAK_LINE;
    }
    int pos = first + 1, advance = 0;
    for (int i = first + 1; i < last; i++) {
        if (br[i].type == SG_TEXTBREAK_OPTIONAL) {
            advance += br[i].advance;
        } else {
            br[pos] = br[i];
            br[pos].advance = advance + br[pos].advance;
            br[pos].advance_trimmed = advance + br[pos].advance_trimmed;
            pos++;
            advance = 0;
        }
    }
    bp->tbreaksize = pos;
}

/*
 * Calculate where the line breaks should be.
 */
static int sg_textbuilder_break(struct sg_textbuilder *restrict bp,
                                struct sg_textlayout *restrict lp,
                                struct sg_error **err) {
    int r;
    {
        size_t n = sg_u32buf_len(&bp->text);
        if ((size_t)bp->attralloc < n + 1) {
            size_t nalloc = sg_round_up_pow2(n + 1);
            if (!nalloc || nalloc > INT_MAX) goto nomem;
            free(bp->attr);
            bp->attr = NULL;
            bp->attralloc = 0;
            unsigned char *narr = malloc(nalloc);
            if (!narr) goto nomem;
            bp->attr = narr;
            bp->attralloc = nalloc;
        }
        sg_linebreak_analyze(bp->attr, bp->text.s, n);
        bp->attr[n] = LB_MUST_BREAK | LB_PARAGRAPH;
    }
    bp->tbreaksize = 0;
    struct sg_textbreak br;
    memset(&br, 0, sizeof(br));
    br.type = SG_TEXTBREAK_PARAGRAPH;
    r = sg_textbuilder_pushbreak(bp, &br, err);
    if (r) return -1;
    const struct sg_textrun *restrict run = bp->run;
    int advance = 0, advance_trimmed = 0, lastbreak = 0, lastcluster = 0,
        bflag = LB_MUST_BREAK, ridx = 0;
    if (bp->width > 0) {
        bflag |= LB_CAN_BREAK;
    }
    for (int i = 0; i <= lp->size; i++) {
        int cluster = lp->cluster[i];
        int attr = bp->attr[cluster];
        if ((attr & bflag) != 0 && cluster != lastcluster) {
            sg_textbreaktype_t btype =
                attr & LB_MUST_BREAK
                    ? (attr & LB_PARAGRAPH ? SG_TEXTBREAK_PARAGRAPH
                                           : SG_TEXTBREAK_LINE)
                    : SG_TEXTBREAK_OPTIONAL;
            br.type = btype;
            br.glyph_index = i;
            br.advance = advance;
            br.advance_trimmed = advance_trimmed;
            while (run[ridx].gend < i) {
                run++;
            }
            br.rprev = ridx;
            if (run[ridx].gend == i) {
                run++;
            }
            br.rnext = ridx;
            r = sg_textbuilder_pushbreak(bp, &br, err);
            if (r) return -1;
            if (btype != SG_TEXTBREAK_OPTIONAL) {
                sg_textbuilder_breaksolve(bp, lastbreak);
                lastbreak = bp->tbreaksize - 1;
            }
            advance = 0;
            advance_trimmed = 0;
        }
        advance += lp->advance[i];
        if ((attr & LB_WHITE) == 0) {
            advance_trimmed = advance;
        }
    }
    assert(br.glyph_index == lp->size);
    return 0;
nomem:
    sg_error_nomem(err);
    return -1;
}

int sg_textbuilder_typeset(struct sg_textbuilder *restrict bp,
                           struct sg_textlayout *restrict lp,
                           struct sg_error **err) {
    int r;
    sg_textlayout_clear(lp);
    if (bp->err) {
        sg_error_copy(err, bp->err);
        return -1;
    }
    int nchar;
    {
        size_t n = sg_u32buf_len(&bp->text);
        if (!n) return 0;
        if (n > INT_MAX) {
            sg_error_nomem(err);
            return -1;
        }
        nchar = (int)n;
    }
    for (int i = bp->runsize, pos = nchar; i > 0; i--) {
        bp->run[i - 1].tend = pos;
        pos = bp->run[i - 1].tstart;
    }
    for (int i = 0; i < bp->runsize; i++) {
        bp->run[i].gstart = lp->size;
        if (bp->run[i].tstart != bp->run[i].tend) {
            r = sg_font_get(&bp->run[i].font, &bp->run[i].fdesc, err);
            if (r) return -1;
            struct sg_fontmetrics fm;
            r = sg_font_getmetrics(&bp->run[i].font, &fm, err);
            if (r) return -1;
            struct sg_textvmetrics *vm = &bp->run[i].metrics;
            vm->font_y0 = fm.descender;
            vm->font_y1 = fm.ascender;
            int vextra = fm.height - (fm.ascender - fm.descender);
            vm->line_y0 = fm.descender - (vextra >> 1);
            vm->line_y1 = vm->line_y0 + fm.height;
            r = sg_font_shape(&bp->run[i].font, lp,
                              bp->text.s + bp->run[i].tstart,
                              bp->run[i].tend - bp->run[i].tstart, err);
            if (r) return -1;
        }
        bp->run[i].gend = lp->size;
    }
    if (!lp->size) return 0;
    r = sg_textlayout_reserve(lp, 1, err);
    if (r) return -1;
    lp->cluster[lp->size] = nchar;
    r = sg_textbuilder_break(bp, lp, err);
    if (r) return -1;
    {
        i16vec4 *restrict gpos = lp->pos;
        const int *restrict gadv = lp->advance;
        for (int i = 1; i < bp->tbreaksize; i++) {
            int gfirst = bp->tbreak[i - 1].glyph_index,
                glast = bp->tbreak[i].glyph_index, x = bp->tbreak[i].pos.v[0],
                y = bp->tbreak[i].pos.v[1];
            for (int j = gfirst; j < glast; j++) {
                gpos[j].v[0] += x;
                gpos[j].v[1] += y;
                gpos[j].v[2] += x;
                gpos[j].v[3] += y;
                x += gadv[j];
            }
        }
    }
    for (int i = 0; i < bp->runsize; i++) {
        struct sg_textrun *r = &bp->run[i];
        i16vec4 style = {{(short)r->style, 0, 0, 0}};
        for (int i = r->gstart, e = r->gend; i < e; i++) {
            lp->sty[i] = style;
        }
    }
    {
        const i16vec4 *restrict gpos = lp->pos;
        int n = lp->size, i = 0;
        while (i < n && gpos[i].v[0] == gpos[i].v[2]) {
            i++;
        }
        if (i < n) {
            int px0 = gpos[i].v[0], py0 = gpos[i].v[1], px1 = gpos[i].v[2],
                py1 = gpos[i].v[3];
            for (i++; i < n; i++) {
                if (gpos[i].v[0] == gpos[i].v[2]) continue;
                px0 = imin(px0, gpos[i].v[0]);
                py0 = imin(py0, gpos[i].v[1]);
                px1 = imax(px1, gpos[i].v[2]);
                py1 = imax(py1, gpos[i].v[3]);
            }
            ibox2 *b = &lp->metrics.pixel_bounds;
            b->mins.v[0] = px0;
            b->mins.v[1] = py0;
            b->maxs.v[0] = px1;
            b->maxs.v[1] = py1;
        }
    }
    {
        const struct sg_textbreak *restrict br = bp->tbreak;
        int px0 = br[1].pos.v[0], px1 = br[1].pos.v[0] + br[1].advance_trimmed,
            py0 = br[1].pos.v[1] + br[1].y0, py1 = br[1].pos.v[1] + br[1].y1;
        for (int i = 2, n = bp->tbreaksize; i < n; i++) {
            px0 = imin(px0, br[i].pos.v[0]);
            px1 = imin(px1, br[i].pos.v[0] + br[i].advance_trimmed);
            py0 = imax(py0, br[i].pos.v[1] + br[i].y0);
            py1 = imax(py1, br[i].pos.v[1] + br[i].y1);
        }
        ibox2 *b = &lp->metrics.logical_bounds;
        b->mins.v[0] = px0;
        b->mins.v[1] = py0;
        b->maxs.v[0] = px1;
        b->maxs.v[1] = py1;
        lp->metrics.baseline = br[1].pos.v[1];
    }
    return 0;
}
