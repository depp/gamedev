/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

/*
 * Sort 'val' to be ascending, permute 'idx' array in parallel.
 */
void sg_sort_u16(unsigned short *restrict val, unsigned short *restrict idx,
                 int count);
