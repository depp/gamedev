/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

#include "sg/text.h"
#include "sg/u32buf.h"
#include "sg/vec.h"
#include <stdbool.h>
#include <stddef.h>

struct sg_error;
struct sg_fontdesc;
struct sg_packing_rect;

enum {
    /*
     * An uninitialized bitmap reference, with no metrics.
     */
    SG_TEXTBITMAP_UNINITIALIZED,

    /*
     * A missing bitmap, not present in the glyph texture.
     */
    SG_TEXTBITMAP_MISSING,

    /*
     * An empty bitmap.  Quads with empty bitmaps are not emitted.
     */
    SG_TEXTBITMAP_ZERO,

    /*
     * Number of statically defined bitmap references.
     */
    SG_TEXTBITMAP_STATICCOUNT
};

/*
 * Font metrics for a font at a particular size.
 */
struct sg_fontmetrics {
    /*
     * The ascender and descender, Y coordinates.  The ascender is
     * normally positive and the descender is normally negative.
     */
    int ascender, descender;

    /*
     * The vertical distance between two lines of text.
     */
    int height;
};

/*
 * A text layout.
 */
struct sg_textlayout {
    /*
     * Linked list of all text layouts.
     */
    struct sg_textlayout *next;

    /*
     * Metrics for this layout.
     */
    struct sg_textmetrics metrics;

    /*
     * Array of glyphs.  The arrays have size N+1 to simplify
     * indexing (so cluster[size] is valid).
     *
     * ptr: Memory region containing all arrays.
     * advance: Relative advance for each glyph.
     * cluster: Index of first character in each glyph's cluster.
     * pos: Vertex coordinates for each glyph, (X0, Y0, X1, Y1).
     * sty: Style for each glyph.
     * bitmap: Bitmap index for each glyph.
     */
    void *ptr;
    int *advance;
    int *cluster;
    i16vec4 *pos;
    i16vec4 *sty;
    unsigned short *bitmap;
    int size;
    int alloc;

    /*
     * Number of quads (glyphs which aren't empty).
     */
    int quadsize;
};

/*
 * A reference to a font with size information.
 */
struct sg_fontref {
    /* Pointer to the font.  */
    void *font;
    /* Font instance index, if font has multiple instances.  */
    int index;
    /* Scaling factor to apply to the font.  */
    float scale;
};

/*
 * =====================================================================
 * Layout
 * =====================================================================
 */

/*
 * Expand a layout so it has enough room to hold the given number of
 * extra glyphs.  On success, return 0.  On failure, return -1 and set
 * err.
 */
int sg_textlayout_reserve(struct sg_textlayout *restrict lp, int size,
                          struct sg_error **err);

/*
 * Map the bitmap indexes to new values.
 */
void sg_textlayout_mapbitmap(const unsigned short *restrict map);

/*
 * Mark the age of all active bitmaps as 1.
 */
void sg_textlayout_agebitmap(unsigned short *restrict age);

/*
 * =====================================================================
 * Fonts
 * =====================================================================
 */

/*
 * Map the bitmap indexes in all fonts to new values.
 */
void sg_font_mapbitmap(const unsigned short *restrict map);

/*
 * Get the font which most closely matches the given font description.
 * On success, return 0.  On failure, initialize err and return -1.
 */
int sg_font_get(struct sg_fontref *font, const struct sg_fontdesc *desc,
                struct sg_error **err);

/*
 * Get the metrics for a font instance.  On success, fill mp and
 * return 0.  On failure, set err and return -1.
 */
int sg_font_getmetrics(const struct sg_fontref *restrict font,
                       struct sg_fontmetrics *mp, struct sg_error **err);

/*
 * Shape text using the given font, converting the given UTF-32 text
 * to a sequence of glyphs.  Everything but the glyph style is added
 * to the layout.  On success, return 0.  On failure, set err and
 * return -1.
 */
int sg_font_shape(struct sg_fontref *restrict font,
                  struct sg_textlayout *restrict layout,
                  const char32_t *restrict text, int size,
                  struct sg_error **err);

/*
 * =====================================================================
 * Texture manager
 * =====================================================================
 */

/*
 * Add a bitmap to the glyph texture.  On success, return a positive
 * bitmap index, and the buffer will be freed with free() if
 * free_buffer is set.  On failure, return -1 and leave the buffer
 * untouched.
 */
int sg_texttex_addbitmap(int width, int height, size_t rowbytes, void *ptr,
                         bool free_buffer, struct sg_error **err);

/*
 * =====================================================================
 * Vertex manager
 * =====================================================================
 */

/*
 * Update the locations of bitmaps in the glyph texture.  This will be
 * called once per frame after the texture coordinates are updated but
 * before the vertex data is updated.
 */
void sg_textvert_updatebitmap(const struct sg_packing_rect *restrict bitmap);

/*
 * Map the bitmap indexes to new values.  This will also invalidate
 * all cached bitmap locations.
 */
void sg_textvert_mapbitmap(const unsigned short *restrict map);

/*
 * Marke the age of all active bitmaps as 0.
 */
void sg_textvert_agebitmap(unsigned short *restrict age);
