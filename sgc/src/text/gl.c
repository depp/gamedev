/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/error.h"
#include "sg/gl_text.h"
#include "sg/log.h"
#include "sg/vecmath.h"
#include "sggl/3_0.h"

struct sg_text_glglobal {
    /* Glyph texture.  */
    GLuint tex;
    ivec2 texsize;
    /* VBO, VAO, and format.  */
    GLuint buf;
    GLuint array;
    sg_textvertex_t format;
};

static struct sg_text_glglobal sg_text_glglobal;

static int sg_text_glupdate_tex(struct sg_text_gl *restrict up,
                                struct sg_error **err) {
    struct sg_text_glglobal *restrict g = &sg_text_glglobal;
    struct sg_textrender_texture t;
    int max_size = 2048;
    int r = sg_textrender_updatetexture(&t, !g->tex, max_size, err);
    if (r) return -1;
    if (t.rows_changed.count) {
        if (!g->tex) {
            glGenTextures(1, &g->tex);
            glBindTexture(GL_TEXTURE_2D, g->tex);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                            GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        } else {
            glBindTexture(GL_TEXTURE_2D, g->tex);
        }
        if (!ivec2_eq(t.size, g->texsize)) {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, t.size.v[0], t.size.v[1], 0,
                         GL_RED, GL_UNSIGNED_BYTE, t.data);
            g->texsize = t.size;
        } else {
            glTexSubImage2D(
                GL_TEXTURE_2D, 0, 0, t.rows_changed.first, t.size.v[0],
                t.rows_changed.count, GL_RED, GL_UNSIGNED_BYTE,
                (const char *)t.data + t.size.v[0] * t.rows_changed.first);
        }
        glGenerateMipmap(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    up->texture = g->tex;
    up->texscale = t.invsize;
    return 0;
}

static int sg_text_glupdate_vert(struct sg_text_gl *restrict up,
                                 sg_textvertex_t format,
                                 struct sg_error **err) {
    struct sg_text_glglobal *restrict g = &sg_text_glglobal;
    struct sg_textrender_vertex v;
    size_t max_size = 1024 * 1024;
    int r = sg_textrender_updatevertex(&v, format, true, max_size, 16);
    if (!r) {
        sg_logs(SG_LOG_ERROR, "Vertex update failed.");
        sg_error_set(err, SG_ERR_GENERAL);
        return -1;
    }
    if (v.byte_changed.count) {
        if (!g->buf) {
            glGenBuffers(1, &g->buf);
            glBindBuffer(GL_ARRAY_BUFFER, g->buf);
            glBufferData(GL_ARRAY_BUFFER, max_size, NULL, GL_DYNAMIC_DRAW);
        } else {
            glBindBuffer(GL_ARRAY_BUFFER, g->buf);
        }
        while (true) {
            char *ptr = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
            if (!ptr) {
                sg_error_set(err, SG_ERR_GENERAL);
                r = -1;
                break;
            }
            sg_logf(SG_LOG_DEBUG, "Bytes: %d, %d", v.byte_changed.first,
                    v.byte_changed.count);
            sg_textrender_vertexdata(ptr + v.byte_changed.first, format,
                                     v.byte_changed.first,
                                     v.byte_changed.count);
            if (glUnmapBuffer(GL_ARRAY_BUFFER)) {
                break;
            }
        }
        if (format != g->format) {
            if (!g->array) {
                glGenVertexArrays(1, &g->array);
            }
            glBindVertexArray(g->array);
            switch (format) {
            case SG_TEXTVERTEX_TRI:
                glEnableVertexAttribArray(0);
                glEnableVertexAttribArray(1);
                glEnableVertexAttribArray(2);
                glVertexAttribPointer(0, 2, GL_SHORT, GL_FALSE, 16, (void *)0);
                glVertexAttribPointer(1, 2, GL_SHORT, GL_FALSE, 16, (void *)4);
                glVertexAttribPointer(2, 4, GL_SHORT, GL_FALSE, 16, (void *)8);
                break;
            case SG_TEXTVERTEX_POINT:
                glEnableVertexAttribArray(0);
                glEnableVertexAttribArray(1);
                glEnableVertexAttribArray(2);
                glVertexAttribPointer(0, 4, GL_SHORT, GL_FALSE, 24, (void *)0);
                glVertexAttribPointer(1, 4, GL_SHORT, GL_FALSE, 24, (void *)8);
                glVertexAttribPointer(2, 4, GL_SHORT, GL_FALSE, 24, (void *)16);
                break;
            }
        }
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    up->array = g->array;
    up->batch_count = v.batch_count;
    up->batch = v.batch;
    return 0;
}

int sg_text_glupdate(struct sg_text_gl *restrict up, sg_textvertex_t format,
                     struct sg_error **err) {
    int r;
    r = sg_text_glupdate_tex(up, err);
    if (r) return -1;
    r = sg_text_glupdate_vert(up, format, err);
    if (r) return -1;
    return 0;
}
