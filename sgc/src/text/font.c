/* Copyright 2015-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "font.h"
#include "private.h"
#include "sg/data.h"
#include "sg/error.h"
#include "sg/file.h"
#include "sg/log.h"
#include "sg/text.h"
#include "sg/util.h"

#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define SG_FONT_PATHLEN 32

static const size_t SG_FONT_MAXSZ = 16 * 1024 * 1024;
static const char SG_FONT_DIR[] = "/font/";
static const char SG_FONT_EXTENSIONS[][5] = {"otf", "ttf", "woff"};
static const char SG_FONT_STYLENAME[3][8] = {
    "normal",
    "italic",
    "oblique"
};

struct sg_fontglobal {
    /* Whether the font directory has been scanned. */
    bool scanned;

    /* All available fonts.  */
    struct sg_font *font;
    int fontsize;
};

static struct sg_fontglobal sg_fontglobal;

/*
 * A font file, not necessarily loaded.
 */
struct sg_font {
    /* Whether the font is loaded into memory (even if failed).  */
    bool is_loaded;

    /* Description of the font.  Size is ignored.  */
    struct sg_fontdesc desc;

    /* Path to the font file, relative to the font directory.  */
    char path[SG_FONT_PATHLEN];

    /* The font file data.  */
    struct sg_data data;

    /* The FreeType font structure, or NULL if not loaded.  */
    void *font;
};

/*
 * Test whether the given filename has an extension used for fonts.
 */
static bool sg_font_isfname(const char *path) {
    const char *p;
    p = strchr(path, '/');
    if (!p) p = path;
    p = strchr(path, '.');
    if (!p) return false;
    p++;
    int n = sizeof(SG_FONT_EXTENSIONS) / sizeof(*SG_FONT_EXTENSIONS);
    for (int i = 0; i < n; i++) {
        if (!strcmp(p, SG_FONT_EXTENSIONS[i])) {
            return true;
        }
    }
    return false;
}

/*
 * Scan for available fonts.
 */
static void sg_font_scan(void) {
    sg_fontglobal.scanned = true;
    struct sg_error *err = NULL;
    int r;

    struct sg_filelist list;
    sg_filelist_init(&list);
    r = sg_filelist_scan(&list, SG_FONT_DIR, SG_FILELIST_NOINFO, &err);
    if (r) {
        sg_logerrs(SG_LOG_ERROR, err, NULL);
        sg_error_clear(&err);
        sg_filelist_destroy(&list);
        return;
    }

    struct sg_font *font = NULL;
    int falloc = 0, fsize = 0;
    struct sg_font rec;
    size_t dirlen = sizeof(SG_FONT_DIR) - 1;
    rec.is_loaded = false;
    memcpy(rec.path, SG_FONT_DIR, dirlen);
    sg_data_init(&rec.data);
    rec.font = NULL;
    for (int i = 0; i < list.filecount; i++) {
        const char *fname = list.file[i].path;
        if (!sg_font_isfname(fname)) continue;
        size_t namelen = strlen(fname);
        if (dirlen + namelen >= sizeof(rec.path)) {
            sg_logf(SG_LOG_ERROR, "%s: font filename too long", fname);
            continue;
        }
        memcpy(rec.path + dirlen, fname, namelen);
        memset(rec.path + dirlen + namelen, '\0',
               sizeof(rec.path) - dirlen - namelen);
        struct sg_data data;
        r = sg_load(&data, NULL, rec.path, NULL, SG_FONT_MAXSZ, &err);
        if (r) {
            sg_logerrs(SG_LOG_ERROR, err, fname);
            sg_error_clear(&err);
            continue;
        }
        r = sg_ftfont_getdesc(&rec.desc, data.ptr, data.size, &err);
        sg_data_destroy(&data);
        if (r) {
            sg_logerrs(SG_LOG_ERROR, err, fname);
            sg_error_clear(&err);
            continue;
        }
        if (fsize >= falloc) {
            unsigned nalloc = sg_round_up_pow2_32(fsize + 1);
            if (!nalloc || nalloc > INT_MAX) goto nomem;
            struct sg_font *narr = realloc(font, nalloc * sizeof(*narr));
            if (!narr) goto nomem;
            falloc = nalloc;
            font = narr;
        }
        font[fsize++] = rec;
        sg_logf(SG_LOG_DEBUG, "font: path=%s, family=%s, style=%s, weight=%d",
                rec.path, rec.desc.family, SG_FONT_STYLENAME[rec.desc.style],
                rec.desc.weight);
    }

    if (!fsize) {
        sg_logs(SG_LOG_WARN, "no fonts found");
    }

    sg_fontglobal.font = font;
    sg_fontglobal.fontsize = fsize;
    return;

nomem:
    free(font);
    sg_error_nomem(&err);
    sg_logerrs(SG_LOG_ERROR, err, "fonts");
    sg_error_clear(&err);
    return;
}

void sg_font_mapbitmap(const unsigned short *restrict map) {
    for (int i = 0; i < sg_fontglobal.fontsize; i++) {
        struct sg_font *fp = &sg_fontglobal.font[i];
        if (fp->font) {
            sg_ftfont_mapbitmap(fp->font, map);
        }
    }
}

enum {
    SG_FONT_SEARCHCOUNT = 32
};

/* Filter font set by matching family.  */
static int sg_font_matchfamily(struct sg_font **font, const char *family) {
    int n = 0;
    for (int i = 0; i < sg_fontglobal.fontsize; i++) {
        struct sg_font *fp = &sg_fontglobal.font[i];
        if (!strcmp(family, fp->desc.family)) {
            font[n++] = fp;
            if (n == SG_FONT_SEARCHCOUNT) break;
        }
    }
    return n;
}

/* Filter font set by matching style.  */
static int sg_font_matchstyle(struct sg_font **font, int n,
                              sg_fontstyle_t style) {
    static const sg_fontstyle_t styles[3][3] = {
        { SG_FONTSTYLE_NORMAL, SG_FONTSTYLE_OBLIQUE, SG_FONTSTYLE_ITALIC },
        { SG_FONTSTYLE_ITALIC, SG_FONTSTYLE_OBLIQUE, SG_FONTSTYLE_NORMAL },
        { SG_FONTSTYLE_OBLIQUE, SG_FONTSTYLE_ITALIC, SG_FONTSTYLE_NORMAL },
    };
    for (int i = 0; i < 3; i++) {
        int m = 0;
        for (int j = 0; j < n; j++) {
            if (font[j]->desc.style == styles[style][i]) {
                font[m++] = font[j];
            }
        }
        if (m) return m;
    }
    return 0;
}

/* Filter font set by matching weight exactly.  */
static int sg_font_matchweight_eq(struct sg_font **font, int n, int weight) {
    int m = 0;
    for (int i = 0; i < n; i++) {
        if (font[i]->desc.weight == weight) {
            font[m++] = font[i];
        }
    }
    return m;
}

/* Filter font set by matching maximum weight in range.  */
static int sg_font_matchweight_max(struct sg_font **font, int n, int minw,
                                   int maxw) {
    int target = minw;
    for (int i = 0; i < n; i++) {
        int fweight = font[i]->desc.weight;
        if (fweight >= minw && fweight <= maxw && fweight > target) {
            target = fweight;
        }
    }
    return sg_font_matchweight_eq(font, n, target);
}

/* Filter font set by matching minimum weight in range.  */
static int sg_font_matchweight_min(struct sg_font **font, int n, int minw,
                                   int maxw) {
    int target = maxw;
    for (int i = 0; i < n; i++) {
        int fweight = font[i]->desc.weight;
        if (fweight >= minw && fweight <= maxw && fweight < target) {
            target = fweight;
        }
    }
    return sg_font_matchweight_eq(font, n, target);
}

/* Filter font set by matching weight.  */
static int sg_font_matchweight(struct sg_font **font, int n, int weight) {
    int m;
    if (weight <= 450) {
        if (weight >= 400) {
            m = sg_font_matchweight_min(font, n, weight, 500);
            if (m) return m;
        }
        m = sg_font_matchweight_max(font, n, INT_MIN, weight);
        if (m) return m;
        return sg_font_matchweight_min(font, n, weight, INT_MAX);
    } else {
        if (weight <= 500) {
            m = sg_font_matchweight_max(font, n, 400, weight);
            if (m) return m;
        }
        m = sg_font_matchweight_min(font, n, weight, INT_MAX);
        if (m) return m;
        return sg_font_matchweight_max(font, n, INT_MIN, weight);
    }
}

/* Load the given font.  The font must not already be loaded.  */
static void sg_font_load(struct sg_font *fp) {
    fp->is_loaded = true;
    struct sg_error *err = NULL;
    int r;
    struct sg_data data;
    r = sg_load(&data, NULL, fp->path, NULL, SG_FONT_MAXSZ, &err);
    if (r) goto failed;
    void *font = sg_ftfont_load(data.ptr, data.size, &err);
    if (!font) {
        sg_data_destroy(&data);
        goto failed;
    }
    fp->data = data;
    fp->font = font;
    return;
failed:
    sg_logerrs(SG_LOG_ERROR, err, fp->path);
    sg_error_clear(&err);
}

int sg_font_get(struct sg_fontref *font, const struct sg_fontdesc *desc,
                struct sg_error **err) {
    /* See: https://www.w3.org/TR/css3-fonts/#font-matching-algorithm */
    if (!sg_fontglobal.scanned) {
        sg_font_scan();
    }
    font->font = NULL;
    font->index = 0;
    font->scale = desc->size;
    struct sg_font *fset[SG_FONT_SEARCHCOUNT];
    int n;
    n = sg_font_matchfamily(fset, desc->family);
    n = sg_font_matchstyle(fset, n, desc->style);
    n = sg_font_matchweight(fset, n, desc->weight);
    if (!n) return 0;
    struct sg_font *fp = fset[0];
    if (!fp->is_loaded) {
        sg_font_load(fp);
    }
    if (!fp->font) {
        sg_error_set(err, SG_ERR_GENERAL);
        return -1;
    }
    return sg_ftfont_get(fp->font, font, desc, err);
}

int sg_font_getmetrics(const struct sg_fontref *restrict font,
                       struct sg_fontmetrics *mp, struct sg_error **err) {
    if (font->font) {
        return sg_ftfont_getmetrics(font, mp, err);
    } else {
        mp->ascender = imax(1, (int)font->scale);
        mp->descender = imin(-1, (int)(font->scale * -0.25f));
        mp->height = mp->ascender - mp->descender;
        return 0;
    }
}

int sg_font_shape(struct sg_fontref *restrict font,
                  struct sg_textlayout *restrict layout,
                  const char32_t *restrict text, int size,
                  struct sg_error **err) {
    if (font->font) {
        return sg_ftfont_shape(font, layout, text, size, err);
    } else {
        return 0;
    }
}
