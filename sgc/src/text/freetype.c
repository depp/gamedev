/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "private.h"
#include "font.h"
#include "sg/error.h"
#include "sg/text.h"
#include "sg/util.h"

#include <assert.h>
#include <math.h>
#include <string.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_TRUETYPE_TABLES_H

#include <hb.h>
#include <hb-ft.h>

static FT_Library sg_ftlib;
static hb_buffer_t *sg_fthbuf;

/*
 * An instance of a FreeType font at a particular size.
 */
struct sg_ftfontinst {
    /* The size, in FreeType units (26.6 fixed point).  */
    int size;

    /* Pointer to free everything.  */
    void *ptr;

    /*
     * The index of the bitmap for each glyph in the glyph texture.
     * This is initially SG_TEXTBITMAP_MISSING.  If the glyph is
     * empty, then SG_TEXTBITMAP_ZERO is used.
     */
    unsigned short *bitmap;

    /*
     * The position of each glyph quad, relative to the pen position.
     * Stored as (X0, Y0, X1, Y1).
     */
    vec4 *pos;
};

struct sg_ftfont {
    /* The FreeType font face.  */
    FT_Face face;

    /* Array of all instantiated sizes.  */
    struct sg_ftfontinst *inst;
    int instsize;
    int installoc;

    /* Number of glyphs in the font.  */
    int numglyph;

    /* HarfBuzz font.  */
    hb_font_t *hbfont;
};

static int sg_ftfont_loadlib(struct sg_error **err) {
    if (sg_ftlib) return 0;
    FT_Error ferr;
    ferr = FT_Init_FreeType(&sg_ftlib);
    if (ferr) {
        sg_fterror(err, ferr);
        return -1;
    }
    return 0;
}

void sg_ftfont_mapbitmap(void *font, const unsigned short *restrict map) {
    struct sg_ftfont *restrict fp = font;
    for (int i = 0; i < fp->instsize; i++) {
        unsigned short *restrict b = fp->inst[i].bitmap;
        if (!b) continue;
        for (int j = 0; j < fp->numglyph; j++) {
            b[j] = map[b[j]];
        }
    }
}

int sg_ftfont_getdesc(struct sg_fontdesc *desc, const void *data, size_t size,
                      struct sg_error **err) {
    int r;
    r = sg_ftfont_loadlib(err);
    if (r) return -1;
    FT_Error ferr;
    FT_Face face;
    ferr = FT_New_Memory_Face(sg_ftlib, data, size, 0, &face);
    if (ferr) {
        sg_fterror(err, ferr);
        return -1;
    }

    sg_fontdesc_setfamily(desc, face->family_name);
    desc->size = 0.0f;

    {
        long flags = face->style_flags;
        desc->style = (flags & FT_STYLE_FLAG_ITALIC) != 0 ? SG_FONTSTYLE_ITALIC
                                                          : SG_FONTSTYLE_NORMAL;
        desc->weight = (flags & FT_STYLE_FLAG_BOLD) != 0 ? 700 : 400;
    }

    {
        const TT_OS2 *os2 = FT_Get_Sfnt_Table(face, ft_sfnt_os2);
        if (os2) {
            desc->weight = os2->usWeightClass;
        }
    }

    FT_Done_Face(face);
    return 0;
}

void *sg_ftfont_load(const void *data, size_t size, struct sg_error **err) {
    int r;
    r = sg_ftfont_loadlib(err);
    if (r) return NULL;
    FT_Error ferr;
    FT_Face face = NULL;
    hb_font_t *hb = NULL;
    ferr = FT_New_Memory_Face(sg_ftlib, data, size, 0, &face);
    if (ferr) {
        sg_fterror(err, ferr);
        goto error;
    }
    hb = hb_ft_font_create(face, NULL);
    if (!hb) {
        sg_error_set(err, SG_ERR_GENERAL);
        goto error;
    }
    struct sg_ftfont *font = malloc(sizeof(*font));
    if (!font) {
        FT_Done_Face(face);
        sg_error_nomem(err);
        return NULL;
    }
    font->face = face;
    font->inst = NULL;
    font->instsize = 0;
    font->installoc = 0;
    font->numglyph = face->num_glyphs;
    font->hbfont = hb;
    return font;
error:
    if (hb) hb_font_destroy(hb);
    if (face) FT_Done_Face(face);
    return NULL;
}

int sg_ftfont_get(void *ftfont, struct sg_fontref *font,
                  const struct sg_fontdesc *desc, struct sg_error **err) {
    struct sg_ftfont *restrict fp = ftfont;
    int size = (int)(desc->size * 64.0f + 0.5f);
    for (int i = 0; i < fp->instsize; i++) {
        if (fp->inst[i].size == size) {
            font->font = fp;
            font->index = i;
            font->scale = 1.0f;
            return 0;
        }
    }
    if (fp->instsize >= fp->installoc) {
        unsigned nalloc = sg_round_up_pow2_32(fp->instsize + 1);
        if (!nalloc || nalloc > INT_MAX) goto nomem;
        struct sg_ftfontinst *inst = realloc(fp->inst, nalloc * sizeof(*inst));
        if (!inst) goto nomem;
        fp->inst = inst;
        fp->installoc = nalloc;
    }
    int idx = fp->instsize++;
    struct sg_ftfontinst *ip = &fp->inst[idx];
    ip->size = size;
    ip->ptr = NULL;
    ip->bitmap = NULL;
    ip->pos = NULL;
    font->font = fp;
    font->index = idx;
    font->scale = 1.0f;
    return 0;
nomem:
    sg_error_nomem(err);
    return -1;
}

static int sg_ftfont_setsize(struct sg_ftfont *restrict fp,
                             struct sg_ftfontinst *restrict ip,
                             struct sg_error **err) {
    FT_Error ferr;
    ferr = FT_Set_Char_Size(fp->face, 0, ip->size, 72, 72);
    if (ferr) {
        sg_fterror(err, ferr);
        return -1;
    }
    return 0;
}

int sg_ftfont_getmetrics(const struct sg_fontref *restrict font,
                         struct sg_fontmetrics *mp, struct sg_error **err) {
    int r;
    struct sg_ftfont *restrict fp = font->font;
    assert(font->index >= 0 && font->index < fp->instsize);
    struct sg_ftfontinst *restrict ip = &fp->inst[font->index];
    float factor = font->scale * (1.0f / 64.0f);
    r = sg_ftfont_setsize(fp, ip, err);
    if (r) return -1;
    const FT_Size_Metrics *fm = &fp->face->size->metrics;
    mp->ascender = (int)floorf((float)fm->ascender * factor + 0.5f);
    mp->descender = (int)floorf((float)fm->descender * factor + 0.5f);
    mp->height = (int)floorf((float)fm->height * factor + 0.5f);
    return 0;
}

/*
 * Render the bitmap for a glyph and calculate the bitmap metrics.  On
 * success, return the bitmap index, a non-negative number.  On
 * failure, set err and return -1.
 */
static int sg_ftfont_render(struct sg_ftfont *restrict fp,
                            struct sg_ftfontinst *restrict ip, int glyph,
                            struct sg_error **err) {
    int r;
    FT_Error ferr;
    r = sg_ftfont_setsize(fp, ip, err);
    if (r) return -1;
    ferr = FT_Load_Glyph(fp->face, glyph, FT_LOAD_TARGET_NORMAL);
    if (ferr) {
        sg_fterror(err, ferr);
        return -1;
    }
    FT_GlyphSlot slot = fp->face->glyph;
    ferr = FT_Render_Glyph(slot, FT_RENDER_MODE_NORMAL);
    if (ferr) {
        sg_fterror(err, ferr);
        return -1;
    }
    int sx = slot->bitmap.width, sy = slot->bitmap.rows;
    if (!sx || !sy) {
        ip->bitmap[glyph] = SG_TEXTBITMAP_ZERO;
        return SG_TEXTBITMAP_ZERO;
    }
    int ox = slot->bitmap_left, oy = slot->bitmap_top;
    size_t rb = slot->bitmap.pitch;
    ip->bitmap[glyph] = SG_TEXTBITMAP_MISSING;
    ip->pos[glyph].v[0] = 64.0f * (float)ox;
    ip->pos[glyph].v[1] = 64.0f * (float)(oy - sy);
    ip->pos[glyph].v[2] = 64.0f * (float)(ox + sx);
    ip->pos[glyph].v[3] = 64.0f * (float)oy;
    void *p = malloc(rb * sy);
    if (!p) {
        sg_error_nomem(err);
        return -1;
    }
    memcpy(p, slot->bitmap.buffer, rb * sy);
    r = sg_texttex_addbitmap(sx, sy, rb, p, true, err);
    if (r == -1) {
        free(p);
        return -1;
    }
    ip->bitmap[glyph] = r;
    return r;
}

int sg_ftfont_shape(struct sg_fontref *restrict font,
                    struct sg_textlayout *restrict layout,
                    const char32_t *restrict text, int size,
                    struct sg_error **err) {
    int r;
    struct sg_ftfont *restrict fp = font->font;
    assert(font->index >= 0 && font->index < fp->instsize);
    struct sg_ftfontinst *restrict ip = &fp->inst[font->index];
    r = sg_ftfont_setsize(fp, ip, err);
    if (r) return -1;
    if (!ip->ptr) {
        int nglyph = fp->numglyph;
        void *ptr = calloc(nglyph, (sizeof(unsigned short) + sizeof(vec4)));
        if (!ptr) goto nomem;
        ip->ptr = ptr;
        char *p = ptr;
        ip->pos = (void *)p;
        p += sizeof(*ip->pos) * nglyph;
        ip->bitmap = (void *)p;
    }
    hb_buffer_t *hbuf = sg_fthbuf;
    hb_font_t *hfont = fp->hbfont;
    if (!hbuf) {
        sg_fthbuf = hbuf = hb_buffer_create();
    }
    hb_buffer_clear_contents(hbuf);
    hb_buffer_set_direction(hbuf, HB_DIRECTION_LTR);
    hb_buffer_set_script(hbuf, HB_SCRIPT_LATIN);
    hb_buffer_set_language(hbuf, hb_language_from_string("en", 2));
    hb_buffer_add_codepoints(hbuf, text, size, 0, size);
    hb_shape(hfont, hbuf, NULL, 0);
    unsigned nglyph;
    const hb_glyph_info_t *restrict glyph_info;
    const hb_glyph_position_t *restrict glyph_pos;
    /* FIXME: no error signaling here?  */
    glyph_info = hb_buffer_get_glyph_infos(hbuf, &nglyph);
    glyph_pos = hb_buffer_get_glyph_positions(hbuf, &nglyph);
    if (nglyph > INT_MAX) goto nomem;
    int n = nglyph;
    r = sg_textlayout_reserve(layout, n, err);
    if (r) return -1;
    int oldn = layout->size;
    i16vec4 *restrict gpos = layout->pos + oldn;
    int *restrict gadv = layout->advance + oldn;
    int *restrict gcls = layout->cluster + oldn;
    unsigned short *restrict gbit = layout->bitmap + oldn;
    float scale = (1.0f / 64.0f) * font->scale;
    int nquad = 0;
    for (int i = 0; i < n; i++) {
        int gi = glyph_info[i].codepoint, bi = ip->bitmap[gi];
        if (bi == SG_TEXTBITMAP_UNINITIALIZED) {
            bi = sg_ftfont_render(fp, ip, gi, err);
            if (bi < 0) return -1;
        }
        if (bi != SG_TEXTBITMAP_ZERO) {
            float xo = (float)glyph_pos[i].x_offset,
                  yo = (float)glyph_pos[i].y_offset;
            vec4 rect = ip->pos[gi];
            gpos[i].v[0] = (int)floorf((rect.v[0] + xo) * scale + 0.5f);
            gpos[i].v[1] = (int)floorf((rect.v[1] + yo) * scale + 0.5f);
            gpos[i].v[2] = (int)floorf((rect.v[2] + xo) * scale + 0.5f);
            gpos[i].v[3] = (int)floorf((rect.v[3] + yo) * scale + 0.5f);
            if (gpos[i].v[0] >= gpos[i].v[2] || gpos[i].v[1] >= gpos[i].v[3]) {
                bi = SG_TEXTBITMAP_ZERO;
            } else {
                nquad++;
            }
        }
        if (bi == SG_TEXTBITMAP_ZERO) {
            gpos[i].v[0] = 0;
            gpos[i].v[1] = 0;
            gpos[i].v[2] = 0;
            gpos[i].v[3] = 0;
        }
        gadv[i] = (int)floorf((float)glyph_pos[i].x_advance * scale + 0.5f);
        gcls[i] = glyph_info[i].cluster;
        gbit[i] = bi;
    }
    layout->size = oldn + n;
    layout->quadsize += nquad;
    return 0;
nomem:
    sg_error_nomem(err);
    return -1;
}
