/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "font.h"
#include "sg/error.h"
#include "sg/strbuf.h"

#include <ft2build.h>
#include <string.h>

#define FT_ERRORDEF(e, v, s) s "\0"
#define FT_ERROR_START_LIST static const char SG_FREETYPE_ERRMSG[] =
#define FT_ERROR_END_LIST ;
#include FT_ERRORS_H

#undef __FTERRORS_H__

#define FT_ERRORDEF(e, v, s) v,
#define FT_ERROR_START_LIST static const unsigned char SG_FREETYPE_ERRCODE[] = {
#define FT_ERROR_END_LIST };
#include FT_ERRORS_H

static const char *sg_freetype_errtext(long code) {
    const char *p = SG_FREETYPE_ERRMSG;
    int n = sizeof(SG_FREETYPE_ERRCODE);
    for (int i = 0; i < n; i++) {
        if (SG_FREETYPE_ERRCODE[i] == code) {
            return p;
        }
        p += strlen(p) + 1;
    }
    return NULL;
}

static void sg_freetype_errmsg(struct sg_strbuf *buf, const char *msg,
                               long code) {
    (void)msg;
    const char *text = sg_freetype_errtext(code);
    if (text) {
        sg_strbuf_puts(buf, text);
    } else {
        sg_strbuf_printf(buf, "(error %ld)", code);
    }
}

static const struct sg_error_domain SG_FREETYPE_DOMAIN = {"freetype",
                                                          sg_freetype_errmsg};

void sg_fterror(struct sg_error **err, int code) {
    sg_error_sets(err, &SG_FREETYPE_DOMAIN, code, NULL);
}
