/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

/*
 * Text system texture management.  New bitmaps are added to a list,
 * and during an update, those bitmaps are packed into the glyph
 * texture.  When there isn't enough space in the texture, some glyphs
 * are discarded and the remaining glyphs are copied into a new
 * texture.
 */

#include "private.h"
#include "sort.h"
#include "sg/error.h"
#include "sg/log.h"
#include "sg/pack.h"
#include "sg/text_render.h"
#include "sg/util.h"

#include <limits.h>
#include <stdlib.h>
#include <string.h>

/*
 * "Box" bitmap for glyphs that get purged from the glyph texture
 * while they are still in use.
 */

enum {
    SG_TEXTTEX_BOXW = 8,
    SG_TEXTTEX_BOXH = 8,
};

#define x 255
const unsigned char SG_TEXTTEX_BOX[SG_TEXTTEX_BOXW * SG_TEXTTEX_BOXH] = {
    x, x, x, x, x, x, x, x,
    x, 0, 0, 0, 0, 0, 0, x,
    x, 0, 0, 0, 0, 0, 0, x,
    x, 0, 0, 0, 0, 0, 0, x,
    x, 0, 0, 0, 0, 0, 0, x,
    x, 0, 0, 0, 0, 0, 0, x,
    x, 0, 0, 0, 0, 0, 0, x,
    x, x, x, x, x, x, x, x,
};
#undef x

struct sg_texttex {
    /*
     * Next bitmap index to allocate.
     */
    int nextbitmap;

    /*
     * Record of glyphs missing from the glyph texture.  Whenever the
     * text system is updated, all missing glyphs will be added the
     * glyph texture.
     */
    struct sg_glyphmissing *missing;
    int missingsize;
    int missingalloc;

    /*
     * The location and size of each bitmap already present in the
     * glyph texture.  The first three bitmaps are fixed.  The
     * bitmapage array tracks the number of updates since each bitmap
     * was used.
     */
    struct sg_packing_rect *bitmap;
    unsigned short *bitmapage;
    int bitmapsize;
    int bitmapalloc;

    /*
     * Packing for the glyph texture.
     */
    struct sg_packing packing;

    /*
     * Glyph pixel buffer.  Data is single-channel U8.  Width and
     * height are powers of two.
     */
    unsigned char *buf;
    int bufw, bufh;

};

static struct sg_texttex sg_texttex;

/*
 * A record of a missing glyph, which will be added to the texture in
 * the next update.
 */
struct sg_glyphmissing {
    short w, h;
    int bitmap;
    bool free_buffer;
    size_t rowbytes;
    void *ptr;
};

static int sg_texttex_addmissing(const struct sg_glyphmissing *m,
                                 struct sg_error **err) {
    if (sg_texttex.missingsize >= sg_texttex.missingalloc) {
        int nalloc = sg_round_up_pow2_32(sg_texttex.missingsize + 1);
        struct sg_glyphmissing *na =
            realloc(sg_texttex.missing, nalloc * sizeof(*na));
        if (!na) {
            sg_error_nomem(err);
            return -1;
        }
        sg_texttex.missing = na;
        sg_texttex.missingalloc = nalloc;
    }
    sg_texttex.missing[sg_texttex.missingsize++] = *m;
    return 0;
}

int sg_texttex_addbitmap(int width, int height, size_t rowbytes, void *ptr,
                         bool free_buffer, struct sg_error **err) {
    int r;
    if (!sg_texttex.nextbitmap) {
        struct sg_glyphmissing m;
        m.w = SG_TEXTTEX_BOXW;
        m.h = SG_TEXTTEX_BOXH;
        m.bitmap = SG_TEXTBITMAP_MISSING;
        m.free_buffer = false;
        m.rowbytes = SG_TEXTTEX_BOXW;
        m.ptr = (void *)SG_TEXTTEX_BOX;
        r = sg_texttex_addmissing(&m, err);
        if (r) return -1;
        sg_texttex.nextbitmap = SG_TEXTBITMAP_STATICCOUNT;
    }
    int bi = sg_texttex.nextbitmap;
    if (bi > USHRT_MAX) {
        sg_error_nomem(err);
        return -1;
    }
    {
        struct sg_glyphmissing m;
        m.w = width;
        m.h = height;
        m.bitmap = bi;
        m.free_buffer = free_buffer ? 1 : 0;
        m.rowbytes = rowbytes;
        m.ptr = ptr;
        r = sg_texttex_addmissing(&m, err);
        if (r) return -1;
    }
    sg_texttex.nextbitmap = bi + 1;
    return bi;
}

/*
 * Update the age of all the bitmaps.  The age of a static bitmap or a
 * bitmap in the text vertex system is 0.  The age of other bitmaps in
 * a layout is 1.  Remaining bitmaps age by 2 each tick.
 */
static void sg_texttex_updateage(void) {
    int size = sg_texttex.bitmapsize;
    unsigned short *restrict age = sg_texttex.bitmapage;
    for (int i = 0; i < size; i++) {
        int x = age[i] + 2;
        if (x > USHRT_MAX) x = USHRT_MAX;
        age[i] = x;
    }
    for (int i = 0; i < SG_TEXTBITMAP_STATICCOUNT; i++) {
        age[i] = 0;
    }
    sg_textlayout_agebitmap(age);
    sg_textvert_agebitmap(age);
}

/*
 * Copy image data from a rectangle in one buffer to another.
 */
static void sg_texttex_blit(const void *restrict src, size_t srcrowbytes,
                            void *restrict dest, size_t destrowbytes, int sx,
                            int sy, int dx, int dy, int w, int h) {
    const unsigned char *sptr = src;
    unsigned char *dptr = dest;
    for (int y = 0; y < h; y++) {
        memcpy(dptr + (dy + y) * destrowbytes + dx,
               sptr + (sy + y) * srcrowbytes + sx, w);
    }
}

/*
 * Get the range of rows spanned by an array of rects.
 */
static void sg_texttex_rectrows(struct sg_crange *restrict rows,
                                const struct sg_packing_rect *rect, int count) {
    if (!count) {
        rows->first = 0;
        rows->count = 0;
        return;
    }
    int y0 = rect[0].y, y1 = rect[0].y + rect[0].h;
    for (int i = 1; i < count; i++) {
        y0 = imin(y0, rect[i].y);
        y1 = imax(y1, rect[i].y + rect[i].h);
    }
    rows->first = y0;
    rows->count = y1 - y0;
}

/*
 * Update the glyph texture.  This will set the range of rows changed.
 * The 'maxsize' is the maximum width or height of the glyph texture.
 * On success, return 0.  On failure, set err and return -1.
 */
static int sg_texttex_updateglyphs(struct sg_crange *restrict rows_changed,
                                   int max_size, struct sg_error **err) {
    /*
     * Yeah.  So, normally I try to make code "obvious" so it doesn't
     * need commenting.  Unfortunately, this is one of those nasty
     * exceptions.  To compensate, these comments are a bit more
     * explicit than usual.
     */
    const int nstatic = SG_TEXTBITMAP_STATICCOUNT;
    int r;
    rows_changed->first = 0;
    rows_changed->count = 0;
    if (!sg_texttex.missingsize) {
        sg_texttex_updateage();
        return 0;
    }

    int oldnbitmap = sg_texttex.bitmapsize, nbitmap = sg_texttex.nextbitmap;
    if (nbitmap > sg_texttex.bitmapalloc) {
        int nalloc = sg_round_up_pow2_32(nbitmap);
        struct sg_packing_rect *nb =
            realloc(sg_texttex.bitmap, nalloc * sizeof(*nb));
        if (!nb) goto nomem;
        sg_texttex.bitmap = nb;
        unsigned short *na =
            realloc(sg_texttex.bitmapage, nalloc * sizeof(*na));
        if (!na) goto nomem;
        sg_texttex.bitmapage = na;
        sg_texttex.bitmapalloc = nalloc;
    }
    memset(sg_texttex.bitmap + oldnbitmap, 0,
           (nbitmap - oldnbitmap) * sizeof(*sg_texttex.bitmap));
    for (int i = oldnbitmap; i < nbitmap; i++) {
        sg_texttex.bitmapage[i] = USHRT_MAX;
    }
    for (int i = 0; i < sg_texttex.missingsize; i++ ) {
        int bi = sg_texttex.missing[i].bitmap;
        sg_texttex.bitmap[bi].w = sg_texttex.missing[i].w;
        sg_texttex.bitmap[bi].h = sg_texttex.missing[i].h;
    }

    sg_texttex_updateage();

    /* Try to pack new glyphs into the existing glyph texture.  */
    bool is_packed = oldnbitmap > 0;
    if (is_packed) {
        r = sg_packing_add(&sg_texttex.packing, sg_texttex.bitmap + oldnbitmap,
                           nbitmap - oldnbitmap);
        if (r > 0) {
            sg_texttex_rectrows(rows_changed, sg_texttex.bitmap + oldnbitmap,
                                nbitmap - oldnbitmap);
        } else if (r == 0) {
            is_packed = false;
        } else {
            goto nomem;
        }
    }

    /* Couldn't add to existing glyph texture, so GC and repack.  */
    if (!is_packed) {
        /*
         * We reorder the bitmaps so the bitmap age increases as
         * bitmap indexes increases.
         *
         * aptr: One allocation for everything.
         * orect: Old bitmap positions, indexed by old indexes.
         * nrect: New bitmap positions, indexed by new indexes.
         * age: Age of new bitmaps, indexed by new indexes.
         * map: Map from old indexes to new indexes.
         * rmap: Map from new indexes to old indexes.
         */
        void *aptr;
        struct sg_packing_rect *restrict orect, *restrict nrect;
        unsigned short *restrict age, *restrict map, *restrict rmap;
        {
            aptr = malloc(nbitmap * (sizeof(*nrect) + sizeof(*map) +
                                     sizeof(*rmap) + sizeof(*age)));
            if (!aptr) {
                sg_error_nomem(err);
                return -1;
            }
            char *p = aptr;
            orect = sg_texttex.bitmap;
            nrect = (void *)p;
            p += nbitmap * sizeof(*nrect);
            age = (void *)p;
            p += nbitmap * sizeof(*age);
            map = (void *)p;
            p += nbitmap * sizeof(*map);
            rmap = (void *)p;
        }
        memcpy(age, sg_texttex.bitmapage, nbitmap * sizeof(*age));
        for (int i = 0; i < nbitmap; i++) {
            rmap[i] = i;
        }
        sg_sort_u16(age + nstatic, rmap + nstatic, nbitmap - nstatic);
        for (int i = 0; i < nbitmap; i++) {
            map[rmap[i]] = i;
        }
        for (int i = 0; i < nbitmap; i++) {
            nrect[map[i]] = orect[i];
        }
        /*
         * Conditions to satisfy (most important first):
         *
         * - New size is no larger than maximum.
         * - New size is no smaller than minimum.
         * - New texture is at least 1/4 empty.
         * - All active bitmaps are in the texture.
         * - New texture is at least 1/2 empty.
         * - Size does not change by more than a factor of two.
         * - At least 1/2 of the used space is inactive bitmaps.
         * - Area is as small as possible.
         */
        /* Number of active (age == 0) bitmaps and their total area.  */
        int actcount = 0, actarea = 0;
        while (actcount < nstatic ||
               (actcount < nbitmap && sg_texttex.bitmapage[actcount] == 0)) {
            actarea += nrect[actcount].w * nrect[actcount].h;
            actcount++;
        }
        /* Base-two logarithm of new texture area.  */
        int nsize, maxnsize = imin(30, 2 * ilog2(max_size));
        {
            int cur = ilog2(sg_texttex.bufw * sg_texttex.bufh);
            int act = ilog2(actarea);
            if (act + 2 < cur - 1) {
                nsize = cur - 1;
            } else if (act + 2 <= cur + 1) {
                nsize = act + 2;
            } else {
                nsize = act + 1;
            }
            if (nsize > maxnsize) {
                nsize = maxnsize;
            } else if (nsize < 8) {
                nsize = 8;
            } else {
                while (nsize < maxnsize && actarea > (3 << (nsize - 2))) {
                    nsize++;
                }
            }
        }
        /* Number of bitmaps to save and their total area.  */
        int savcount = actcount, savarea = actarea;
        if (actarea <= (3 << (nsize - 2))) {
            int limit = 1 << (nsize - 1);
            while (savcount < nbitmap) {
                int narea = savarea + nrect[savcount].w * nrect[savcount].h;
                if (narea > limit) break;
                savarea = narea;
                savcount++;
            }
        } else {
            int limit = 3 << (nsize - 2);
            while (savcount > nstatic && savarea > limit) {
                savcount--;
                savarea  -= nrect[savcount].w * nrect[savcount].h;
            }
        }
        for (int i = savcount; i < nbitmap; i++) {
            map[rmap[i]] = SG_TEXTBITMAP_MISSING;
        }
        /* Repack.  */
        int nbufw, nbufh;
        unsigned char *nbuf;
        {
            struct sg_packing pack;
            sg_packing_init(&pack);
            ivec2 pmin_size = {{1 << (nsize / 2), 1 << ((nsize + 1) / 2)}},
                  pmax_size = {{max_size, max_size}};
            r = sg_packing_autopack(&pack, nrect, savcount, pmin_size,
                                    pmax_size);
            if (r <= 0) {
                sg_packing_destroy(&pack);
                free(aptr);
                if (r < 0) {
                    sg_error_nomem(err);
                } else {
                    sg_error_set(err, SG_ERR_GENERAL);
                }
                return -1;
            }
            nbufw = pack.size.v[0];
            nbufh = pack.size.v[1];
            nbuf = calloc(nbufw, nbufh);
            if (!nbuf) {
                sg_packing_destroy(&pack);
                free(aptr);
                sg_error_nomem(err);
                return -1;
            }
            sg_packing_destroy(&sg_texttex.packing);
            sg_texttex.packing = pack;
        }
        {
            int kept = 0, added = 0;
            for (int i = 0; i < oldnbitmap; i++) {
                kept += rmap[i] < savcount;
            }
            for (int i = oldnbitmap; i < nbitmap; i++) {
                added += rmap[i] < savcount;
            }
            sg_logf(SG_LOG_DEBUG,
                    "Glyphs repacked: texture: %dx%d -> %dx%d, kept: %d, "
                    "removed: %d, added: %d, skipped: %d",
                    sg_texttex.bufw, sg_texttex.bufh, nbufw, nbufh, kept,
                    oldnbitmap - kept, added, nbitmap - oldnbitmap - kept);
        }
        for (int i = 0; i < sg_texttex.missingsize; i++) {
            int bi = sg_texttex.missing[i].bitmap, nbi = map[bi];
            if (bi >= nstatic && nbi < nstatic) {
                nbi = -1;
            }
            sg_texttex.missing[i].bitmap = nbi;
        }
        sg_font_mapbitmap(map);
        sg_textvert_mapbitmap(map);
        {
            unsigned char *obuf = sg_texttex.buf;
            int obufw = sg_texttex.bufw;
            for (int i = 0; i < oldnbitmap; i++) {
                int si = rmap[i];
                if (si >= savcount) continue;
                sg_texttex_blit(obuf, obufw, nbuf, nbufw, orect[si].x,
                                orect[si].y, nrect[i].x, nrect[i].y, nrect[i].w,
                                nrect[i].h);
            }
            free(obuf);
        }
        nbitmap = savcount;
        sg_texttex.buf = nbuf;
        sg_texttex.bufw = nbufw;
        sg_texttex.bufh = nbufh;
        memcpy(sg_texttex.bitmapage, age, nbitmap * sizeof(*age));
        memcpy(orect, nrect, savcount * sizeof(*nrect));
        sg_texttex_rectrows(rows_changed, orect, nbitmap);
        free(aptr);
    }

    /* Draw the new glyphs.  */
    {
        const struct sg_packing_rect *restrict rect = sg_texttex.bitmap;
        const struct sg_glyphmissing *restrict p = sg_texttex.missing,
                                               *e = p + sg_texttex.missingsize;
        unsigned char *buf = sg_texttex.buf;
        int bufw = sg_texttex.bufw;
        for (; p != e; p++) {
            int bi = p->bitmap;
            if (bi >= 0) {
                sg_texttex_blit(p->ptr, p->rowbytes, buf, bufw, 0, 0,
                                rect[bi].x, rect[bi].y, rect[bi].w, rect[bi].h);
            }
            if (p->free_buffer) {
                free(p->ptr);
            }
        }
    }

    sg_texttex.nextbitmap = nbitmap;
    sg_texttex.missingsize = 0;
    sg_texttex.bitmapsize = nbitmap;

    return 0;

nomem:
    sg_error_nomem(err);
    return -1;
}

int sg_textrender_updatetexture(struct sg_textrender_texture *restrict tp,
                                bool full_update, int max_size,
                                struct sg_error **err) {
    (void)full_update;
    int r;
    r = sg_texttex_updateglyphs(&tp->rows_changed, max_size, err);
    if (r) return -1;
    sg_textvert_updatebitmap(sg_texttex.bitmap);
    int w = sg_texttex.bufw, h = sg_texttex.bufh;
    tp->data = sg_texttex.buf;
    tp->size.v[0] = w;
    tp->size.v[1] = h;
    tp->invsize.v[0] = 1.0f / (float)w;
    tp->invsize.v[1] = 1.0f / (float)h;
    return 0;
}
