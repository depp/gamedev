/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sort.h"

static void sg_sort_u16_2(unsigned short *restrict val,
                          unsigned short *restrict idx, int count, int depth) {
    if (count <= 10 || depth >= 8) {
        /* Insertion sort */
        for (int i = 1; i < count; i++) {
            for (int j = 0; j < i; j++) {
                if (val[i] < val[j]) {
                    int ta = val[i], ti = idx[i];
                    for (int k = i; k > j; k--) {
                        val[k] = val[k - 1];
                        idx[k] = idx[k - 1];
                    }
                    val[j] = ta;
                    idx[j] = ti;
                    break;
                }
            }
        }
    } else {
        /* Quicksort with Bentley McIlroy 3-way partitioning */
        int pval = val[count / 2], pidx = idx[count / 2], tval, tidx;
        int i = 0, ie = i, j = count - 1, je = j;
        val[count / 2] = val[j];
        idx[count / 2] = idx[j];
        val[j] = pval;
        idx[j] = pidx;
        while (1) {
            for (; i != j && pval >= val[i]; i++) {
                if (pval == val[i]) {
                    tval = val[i];
                    tidx = idx[i];
                    val[i] = val[ie];
                    idx[i] = idx[ie];
                    val[ie] = tval;
                    idx[ie] = tidx;
                    ie++;
                }
            }
            for (; i != j && pval <= val[j - 1]; j--) {
                if (pval == val[j - 1]) {
                    tval = val[j - 1];
                    tidx = idx[j - 1];
                    val[j - 1] = val[je - 1];
                    idx[j - 1] = idx[je - 1];
                    val[je - 1] = tval;
                    idx[je - 1] = tidx;
                    je--;
                }
            }
            if (i == j) break;
            j--;
            tval = val[i];
            tidx = idx[i];
            val[i] = val[j];
            idx[i] = idx[j];
            val[j] = tval;
            idx[j] = tidx;
            i++;
        }
        int ni = i - ie;
        for (int n = ni < ie ? ni : ie; n > 0; n--) {
            tval = val[n];
            tidx = idx[n];
            val[n] = val[i - n - 1];
            idx[n] = idx[i - n - 1];
            val[i - n - 1] = tval;
            idx[i - n - 1] = tidx;
        }
        int nj = je - j;
        for (int n = nj < count - je ? nj : count - je; n > 0; n--) {
            tval = val[j + n];
            tidx = idx[j + n];
            val[j + n] = val[count - n - 1];
            idx[j + n] = idx[count - n - 1];
            val[count - n - 1] = tval;
            idx[count - n - 1] = tidx;
        }
        if (ni > 1) {
            sg_sort_u16_2(val, idx, ni, depth + 1);
        }
        if (nj > 1) {
            sg_sort_u16_2(val + (count - nj), idx + (count - nj), nj,
                          depth + 1);
        }
    }
}

void sg_sort_u16(unsigned short *restrict val, unsigned short *restrict idx,
                 int count) {
    sg_sort_u16_2(val, idx, count, 0);
}
