/* Copyright 2015-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/u32buf.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

static const unsigned char SG_UTF8_NBYTE[256] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
    4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0
};

/*
 * Increase the buffer size in a UTF-32 text buffer.  On success,
 * there will be at least one code unit between the current position
 * and the end of the buffer.  Return 0 if successful, -1 if out of
 * memory.
 */
static int sg_u32buf_expand(struct sg_u32buf *restrict b) {
    size_t cursize = b->p - b->s, curalloc = b->e - b->s,
           nalloc = curalloc ? curalloc * 2 : 8;
    if (!nalloc) return -1;
    unsigned *narr = realloc(b->s, sizeof(*b->s) * nalloc);
    if (!narr) return -1;
    b->s = narr;
    b->p = narr + cursize;
    b->e = narr + nalloc;
    return 0;
}

void sg_u32buf_init(struct sg_u32buf *restrict b) {
    b->s = NULL;
    b->p = NULL;
    b->e = NULL;
}

void sg_u32buf_destroy(struct sg_u32buf *restrict b) {
    free(b->s);
}

void sg_u32buf_clear(struct sg_u32buf *restrict b) {
    b->p = b->s;
}

size_t sg_u32buf_len(struct sg_u32buf *restrict b) {
    return b->p - b->s;
}

int sg_u32buf_write(struct sg_u32buf *restrict b, const char *restrict data,
                    size_t size) {
    const unsigned char *uptr = (const unsigned char *)data,
                        *uend = uptr + size;
    bool no_errors = true;

    while (1) {
        unsigned uc;
        if (SG_LIKELY(uend - uptr >= 4)) {
            unsigned c0 = uptr[0];
            if (c0 < 0x80) {
                /* 0xxxxxxx: 1 byte */
                uc = c0;
                uptr += 1;
            } else {
                unsigned c1 = uptr[1], c2 = uptr[2], c3 = uptr[3];
                if (c0 < 0xe0) {
                    if (SG_UNLIKELY(c0 < 0xc0)) {
                        /* 10xxxxxx: continuation */
                        no_errors = false;
                        uc = 0xfffd;
                        uptr += 1;
                    } else {
                        /* 110xxxxx: 2 byte */
                        uc = ((c0 & 0x1f) << 6) | (c1 & 0x3f);
                        if (SG_LIKELY((c1 & 0xc0) == 0x80 && uc >= 0x80)) {
                            uptr += 2;
                        } else {
                            goto slow;
                        }
                    }
                } else {
                    if (c0 < 0xf0) {
                        /* 1110xxxx: 3 byte */
                        uc = ((c0 & 0x0f) << 12) | ((c1 & 0x3f) << 6) |
                             (c2 & 0x3f);
                        unsigned cm = c1 | (c2 << 8);
                        if (SG_LIKELY(((cm & 0xc0c0) == 0x8080 && uc >= 0x800 &&
                                       (uc & 0xf800) != 0xd800))) {
                            uptr += 3;
                        } else {
                            goto slow;
                        }
                    } else {
                        /* 11110xxx: 4 byte */
                        uc = ((c0 & 0x07) << 18) | ((c1 & 0x3f) << 12) |
                             ((c2 & 0x3f) << 6) | (c3 & 0x3f);
                        unsigned cm = c1 | (c2 << 8) |
                            (c3 << 16) | (c0 << 24);
                        if (SG_LIKELY((cm & 0xf8c0c0c0) == 0xf0808080 &&
                                      uc >= 0x10000 && uc < 0x110000)) {
                            uptr += 4;
                        } else {
                            goto slow;
                        }
                    }
                }
            }
        } else {
        slow:
            if (uptr == uend) {
                break;
            }
            unsigned c0 = uptr[0];
            int n = SG_UTF8_NBYTE[c0], i = 1;
            uc = c0;
            for (; i < n && uptr + i != uend; i++) {
                unsigned cc = uptr[i];
                if ((cc & 0xc0) != 0x80) {
                    break;
                }
                uc = (uc << 6) | (cc & 0x3f);
            }
            uptr += i;
            if (i != n) {
                no_errors = false;
                uc = 0xfffd;
            } else {
                switch (n) {
                default:
                case 1:
                    break;

                case 2:
                    uc &= 0x7ff;
                    if (uc < 0x80) {
                        no_errors = false;
                        uc = 0xfffd;
                    }
                    break;

                case 3:
                    uc &= 0xffff;
                    if (uc < 0x800 || ((uc & 0xf800) == 0xd800)) {
                        no_errors = false;
                        uc = 0xfffd;
                    }
                    break;

                case 4:
                    if (uc < 0x10000 || uc >= 0x110000) {
                        no_errors = false;
                        uc = 0xfffd;
                    }
                    break;
                }
            }
        }
        if (b->p == b->e) {
            if (sg_u32buf_expand(b)) return -1;
        }
        *b->p++ = uc;
    }

    return no_errors ? 0 : 1;
}

int sg_u32buf_putc(struct sg_u32buf *restrict b, char32_t c) {
    if (b->p == b->e) {
        int r = sg_u32buf_expand(b);
        if (r) return -1;
    }
    *b->p++ = c;
    return 0;
}

int sg_u32buf_puts(struct sg_u32buf *restrict b, const char *restrict text) {
    return sg_u32buf_write(b, text, strlen(text));
}
