#!/usr/bin/env python3
# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
"""SGLib configuration script.

Run this script to generate the build script.
"""
from sglib.build import run
run()
