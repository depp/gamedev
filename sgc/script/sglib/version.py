# Copyright 2013-2014 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import collections
import io
from pathlib import Path
import sys
import re
from d4build.error import format_block
from d4build.file import update_text
from d4build.generatedfiles import GeneratedFiles, NOTICE
from d4build.log import logfile
from d4build.shell import get_output

def warn(*msg):
    print('Warning:', *msg, file=logfile(1))

# 'number' is a triplet (x, y, z)
# 'desc' is the Git description 'vX.Y.Z-1-gSHA1'
# 'sha1' is the Git SHA-1
Version = collections.namedtuple('Version', 'number desc sha1')

EMPTY_VERSION = Version((0, 0, 0), 'v0.0.0', '')

VERSION_STRING = re.compile(
    r'v(\d+)(?:\.(\d+)(?:\.(\d+))?)?(?:-\d+(?:-.*)?)?')

def git_dir(git, path):
    """Get the git dir for the given path."""
    stdout, stderr, retcode = get_output(
        [git, 'rev-parse', '--git-dir'], cwd=str(path))
    if retcode:
        warn(
            'Could not find git repository: {}'
            .format(path), format_block(stderr))
        return None
    return path.joinpath(stdout.rstrip())

def version_dependency(path):
    """Get the dependency file for a repository version number.

    This is the file which changes when the version number changes.
    """
    return Path(path, 'logs', 'HEAD')

def get_version(git, path):
    """Get the of the given Git repository."""
    if path is None:
        return EMPTY_VERSION
    gitdir = '--git-dir=' + str(path)

    stdout, stderr, retcode = get_output(
        [git, gitdir, 'rev-parse', 'HEAD'])
    if retcode:
        warn(
            'Could not get SHA-1 for repository: {}'.format(path),
            format_block(stderr))
        return EMPTY_VERSION
    sha1 = stdout.strip()

    stdout, stderr, retcode = get_output(
        [git, gitdir, 'describe'])
    if retcode:
        warn(
            'Could not get version number for repository: {}'.format(path),
            format_block(stderr))

        stdout, stderr, retcode = get_output(
            [git, gitdir, 'rev-list', 'HEAD'])
        if retcode:
            warn(
                'Could not get list of git revisions for repository: {}'
                .format(path), format_block(stderr))
            version = 'v0.0.0'
        else:
            nrev = len(stdout.splitlines())
            version = 'v0.0.0-{}-g{}'.format(nrev, sha1[:7])
    else:
        version = stdout.strip()

    m = VERSION_STRING.match(version)
    if not m:
        warn('Could not parse version number: {}'.format(version))
        number = 0, 0, 0
    else:
        number = tuple(int(x) if x is not None else 0 for x in m.groups())
    return Version(number, version, sha1)

class VersionInfo(GeneratedFiles):
    __slots__ = ['git', 'repos', 'output']

    def __init__(self, build, repos):
        self.git = 'git'
        self.repos = []
        self.output = build.config.buildsrc.joinpath('version_data.c')
        builddir = build.config.builddir
        for name, path in repos:
            path = git_dir(self.git, path)
            if path is not None:
                path = path.relative_to(builddir)
            self.repos.append((name, path))

    def inputs(self, builddir):
        paths = set()
        for name, path in self.repos:
            if path is None:
                continue
            path = version_dependency(builddir / path)
            if path in paths:
                continue
            yield path
            paths.add(path)

    def outputs(self, builddir):
        yield builddir / self.output

    def generate(self, builddir, progress):
        fp = io.StringIO()
        fp.write('/* {} */\n'.format(NOTICE))
        for name, path in self.repos:
            if path is not None:
                path = builddir / path
            version = get_version(self.git, path)
            fp.write(
                'const char SG_{name}_VERSION[] = "{version.desc}";\n'
                'const char SG_{name}_COMMIT[] = "{version.sha1}";\n'
                .format(name=name, version=version))
        path = builddir / self.output
        progress(path)
        update_text(path, fp.getvalue())
