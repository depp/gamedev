# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.generatedfiles import json_file, CopyFile

SIZES = [16, 32, 48, 64, 128, 256, 512, 1024]
OSX_SIZES = [16, 32, 128, 256, 512]
OSX_SCALES = [1, 2]

def osx_icon(build, icon):
    builddir = build.config.builddir
    assets = build.config.buildsrc.joinpath('Media.xcassets')
    iconset = assets.joinpath('AppIcon.iconset')
    images = []
    sizes = {}
    for size in OSX_SIZES:
        for scale in OSX_SCALES:
            imgsize = scale * size
            image = {
                'size': '{0}x{0}'.format(size),
                'idiom': 'mac',
                'scale': '{}x'.format(scale),
            }
            images.append(image)
            try:
                fname = sizes[size]
            except KeyError:
                fpath = icon.sizes.get(imgsize)
                if fpath is None:
                    continue
                fname = 'icon{}.png'.format(imgsize)
                build.files.append(CopyFile(
                    fpath.relative_to(builddir), iconset.joinpath(fname)))
                sizes[imgsize] = fname
            image['filename'] = fname
    info = {'version': 1, 'author': 'sglib'}
    build.files.append(json_file(iconset / 'Contents.json', {
        'images': images,
        'info': info,
    }))
    build.files.append(json_file(assets / 'Contents.json', {
        'info': info,
    }))
    # {'ASSETCATALOG_COMPILER_APPICON_NAME': 'AppIcon'}
    return iconset.stem

def windows_icon(self, build):
    mod = build.target.module()
    from d4build.generatedsource.simplefile import SimpleFile
    if self.ico is None:
        return None
    rcpath = sglib_path('resources/resources.rc')
    mod.add_generated_source(
        SimpleFile(rcpath, '1 ICON "{}"\n'.format(self.ico)))
    mod.add_source(rcpath, sourcetype='rc')
    return mod
