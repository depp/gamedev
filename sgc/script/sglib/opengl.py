# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from pathlib import Path
from d4build.generatedfiles import GeneratedFiles
from d4build.file import update_text
from glgen.loader import LoaderSpec, Api, Version, create_loader, loader_inputs

class OpenGLLoader(GeneratedFiles):
    __slots__ = [
        'includedir',
        'sourcedir',
        'spec',
        'loader',
    ]

    def __init__(self, build, opengl):
        self.includedir = build.config.buildinclude / 'sggl'
        self.sourcedir = build.config.buildsrc
        self.spec = LoaderSpec(
            build.config.target,
            Api('gl', 'core'),
            Version(*opengl.version),
            sorted(set(opengl.extensions)),
        )
        value = build.cache.get('sglib.opengl')
        if value is not None:
            cached_spec, loader = value
            if cached_spec == self.spec:
                self.loader = loader
                return
        self.loader = create_loader(self.spec)
        build.cache.set('sglib.opengl', (self.spec, self.loader))

    def inputs(self, builddir):
        return loader_inputs(self.spec)

    def outputs(self, builddir):
        # Headers
        dirpath = builddir / self.includedir
        for source in self.loader.headers:
            yield dirpath / source.name
        # Sources
        dirpath = builddir / self.sourcedir
        for source in self.loader.sources:
            yield dirpath / source.name

    def generate(self, builddir, progress):
        # Headers
        dirpath = builddir / self.includedir
        extra = {path for path in dirpath.glob('*.h')
                 if not path.name.startswith('.')}
        for source in self.loader.headers:
            path = dirpath / source.name
            progress(path)
            update_text(path, source.contents)
            extra.discard(path)
        for path in extra:
            path.unlink()
        # Sources
        dirpath = builddir / self.sourcedir
        for source in self.loader.sources:
            path = dirpath / source.name
            progress(path)
            update_text(path, source.contents)
