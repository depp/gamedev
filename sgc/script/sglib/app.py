# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.error import ConfigError
from d4build.target import Executable, ApplicationBundle
from d4build.template import TemplateFile
from .config import sglib_config
from .icon import osx_icon, windows_icon
from .info_plist import InfoPlist
from .module import sglib_module

def build_app(build, app, sglib):
    builddir = build.config.builddir
    cfg = sglib_config(build)

    mod_sglib = sglib_module(build, app, sglib, cfg)
    mod_app = build.source_module(app.sources, [])
    mod_app.add('public', mod_sglib)

    arguments = [
        ('path.data', build.runpath(app.paths.data)),
        ('path.user', build.runpath(app.paths.user)),
        ('developer', 'yes'),
    ]

    if build.config.target == 'linux':
        exe = Executable(app.info.name.replace(' ', '_'))
    elif build.config.target == 'osx':
        main_nib = None
        icon = None
        if app.icon is not None:
            icon = osx_icon(build, app.icon)
        if cfg.frontend == 'cocoa':
            src = (sglib.root.joinpath('src/core/osx/MainMenu.xib')
                   .relative_to(builddir))
            dest = build.config.buildsrc.joinpath(src.name)
            files = TemplateFile(
                dest, src, {'EXE_NAME': app.info.name})
            build.files.append(files)
            # mod.add_sources(files.outputs(builddir))
            main_nib = dest.stem
        files = InfoPlist(build, app, icon, main_nib)
        build.files.append(files)
        exe = ApplicationBundle(app.info.name, files.output)
    elif build.config.target == 'windows':
        exe = Executable(app.info.name)
        if app.icon is not None:
            pass
        arguments.append(('log.winconsole', 'yes'))
    else:
        raise ConfigError(
            'Unsupported target: {}'.format(buildf.config.target))

    exe.modules.append(mod_app)
    exe.default_arguments.extend('{}={}'.format(k, v) for k, v in arguments)
    build.targets.append(exe)
