# Copyright 2014 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.package import ExternalPackage

def pkg_config(build):
    mod = build.pkg_config('gtkglext-1.0')
    mod.link_env.libs = [flag for flag in mod.link_env.libs
                         if flag != '-Wl,--export-dynamic']
    return None, mod

module = ExternalPackage(
    [pkg_config],
    name='Gtk+ OpenGL Extension',
    packages={
        'deb': 'libgtkglext1-dev',
        'rpm': 'gtkglext-devel',
        'gentoo': 'x11-libs/gtkglext',
        'arch': 'gtkglext',
    }
)
