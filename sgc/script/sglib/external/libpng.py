# Copyright 2014 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.package import ExternalPackage

def pkg_config(build):
    return None, build.pkg_config('libpng12')

module = ExternalPackage(
    [pkg_config],
    name='LibPNG',
    packages={
        'deb': 'libpng-dev',
        'rpm': 'libpng-devel',
        'gentoo': 'media-libs/libpng',
        'arch': 'libpng',
    }
)
