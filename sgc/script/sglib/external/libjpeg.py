# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.package import ExternalPackage
from d4build.error import ConfigError

# stdio.h is necessary
TEST_SOURCE = '''\
#include <stdio.h>
#include "jpeglib.h"
int main(int argc, char **argv) {
    int version;
    struct jpeg_decompress_struct cinfo;
    (void) argc;
    (void) argv;
    version = JPEG_LIB_VERSION;
    jpeg_create_decompress(&cinfo);
    return version;
}
'''

def test(build):
    mod = build.module()
    mod.link_env.libs.append('-ljpeg')
    value = build.cache.get('sglib.libjpeg')
    if value is None:
        value = build.test_compile(TEST_SOURCE, 'c', mod)
        build.cache.set('sglib.libjpeg', bool(value), [__file__])
        if not value:
            raise ConfigError('Test program failed to compile and link.')
    return None, mod

module = ExternalPackage(
    [test],
    name='LibJPEG',
    packages={
        'deb': 'libjpeg62-turbo-dev',
        'rpm': 'libjpeg-turbo',
        'gentoo': 'media-libs/libjpeg-turbo',
        'arch': 'libjpeg-turbo',
    }
)
