# Copyright 2014 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.package import ExternalPackage

def pkg_config(build):
    return None, build.pkg_config('pangocairo')

module = ExternalPackage(
    [pkg_config],
    name='Pango',
    packages={
        'deb': 'libpango1.0-dev',
        'rpm': 'pango-devel',
        'gentoo': 'x11-libs/pango',
        'arch': 'pango',
    }
)
