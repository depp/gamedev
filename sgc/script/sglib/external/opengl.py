# Copyright 2014 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.package import ExternalPackage
from d4build.error import ConfigError
import os

def framework(build):
    return None, build.frameworks(['OpenGL'])

def pkg_config(build):
    return None, build.pkg_config('gl')

def windows(build):
    build.check_platform('windows')
    return None, build.target.module().add_library('opengl32.lib')

module = ExternalPackage(
    [framework, pkg_config, windows],
    name='OpenGL',
    packages={
        'deb': 'libgl-dev',
        'rpm': 'mesa-libGL-devel',
        'gentoo': 'virtual/opengl',
        'arch': 'libgl',
    }
)
