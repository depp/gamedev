# Copyright 2014 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.package import ExternalPackage

def pkg_config(build):
    return None, build.pkg_config('alsa')

module = ExternalPackage(
    [pkg_config],
    name='ALSA',
    packages={
        'deb': 'libasound2-dev',
        'rpm': 'alsa-lib-devel',
        'gentoo': 'media-libs/alsa-lib',
        'arch': 'alsa-lib',
    }
)
