# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.cache import cached
from d4build.error import ConfigError
from d4build.package import ExternalPackage

TEST_SOURCE = '''\
#include <math.h>
#include <stdlib.h>
int main(int argc, char **argv) {
    (void) argv;
    return (int) exp(argc);
}
'''

def not_needed(build):
    if build.config.target in ('windows', 'osx'):
        return 'not needed', None
    raise ConfigError('must test for math library on this platform')

@cached(name='sglib.libm', deps=[__file__])
def _libs_flag(build):
    for libs in ['', '-lm']:
        mod = build.module()
        mod.link_env.libs = libs.split()
        if build.test_compile(TEST_SOURCE, 'c', mod):
            return libs
    return None

def lib(build):
    libs = _libs_flag(build)
    if libs is None:
        raise ConfigError('cannot find math library')
    if not libs:
        return 'not needed', None
    mod = build.module()
    mod.link_env.libs = libs.split()
    return libs, mod

module = ExternalPackage(
    [not_needed, lib],
    name='Math library',
)
