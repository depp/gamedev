# Copyright 2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.error import ConfigError
from d4build.package import ExternalPackage

def pkg_config(build):
    return None, build.pkg_config('harfbuzz-gobject')

module = ExternalPackage(
    [pkg_config],
    name='HarfBuzz library',
    packages={
        'deb': 'libharfbuzz-dev',
        'rpm': 'harfbuzz-devel',
        'gentoo': 'media-libs/harfbuzz',
        'arch': 'harfbuzz',
    }
)
