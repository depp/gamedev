# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.log import logfile, Feedback
from d4build.environment import Environment, EnvOptions, EnvBoolean
from d4build.error import ConfigError
from d4build.posix import PosixToolchain

class BaseEnvironment(Environment):
    warnings = EnvBoolean('enable warnings')
    asan = EnvBoolean('enable address sanitizer')
    lto = EnvBoolean('enable link-time optimization')
    werror = EnvBoolean('treat warnings as errors')

# Source code for testing compiler flags
SOURCE = '''\
int main(int argc, char **argv) {
    (void)argc;
    (void)argv;
    return 0;
}
'''

WARNING_FLAGS = [('c', 'cwarn', '''
-Wall
-Wextra
-Wpointer-arith
-Wwrite-strings
-Wmissing-prototypes
-Werror=implicit-function-declaration
-Wdouble-promotion
-Winit-self
-Wstrict-prototypes
'''.split()),
('c++', 'cxxwarn', '''
-Wall
-Wextra
-Wpointer-arith
'''.split())]

def posix_vars(build, *, langs=('c',), configs=None,
               cache_key=None, **kw):
    """Test flags and merge them into the base environment if they work."""
    valid = None
    if cache_key is not None:
        cache_key = 'sglib.base.' + cache_key
        valid = build.cache.get(cache_key)
    if valid is None:
        mod = build.module()
        mod.update_map(kw)
        valid = bool(all(
            build.test_compile(SOURCE, sourcetype, mod)
            for sourcetype in langs))
        if cache_key is not None:
            build.cache.set(cache_key, valid, [__file__])
    if valid:
        build.base.update(**kw)

def posix_warnings(build, langs=('c',), *, werror):
    """Test for supported warning flags."""
    for sourcetype, varname, wflags in WARNING_FLAGS:
        if sourcetype not in langs:
            continue
        mod = build.module()
        mod.update(cwarn=['-Werror'])
        has_werror = build.test_compile(
            SOURCE, sourcetype, mod, link=False, external=False)
        for wflag in wflags:
            cache_key = 'sglib.base.warn.{}.{}'.format(sourcetype, wflags)
            valid = build.cache.get(cache_key)
            if valid is None:
                flags = ['-Werror', wflag] if has_werror else [wflag]
                mod = build.module()
                mod.update(cwarn=flags)
                valid = build.test_compile(
                    SOURCE, sourcetype, mod, link=False, external=False)
                build.cache.set(cache_key, valid, [__file__])
            if valid:
                build.base.update(cwarn=[wflag])
        if werror and has_werror:
            build.base.update(cwarn=['-Werror'])

def posix_base(build):
    """Add standard options to a POSIX-like toolchain."""

    base = BaseEnvironment(
        warnings=True,
        asan=False,
        lto=False,
        werror=False,
    )
    base.update_env(build.config.variables)

    with Feedback('Checking for threading support...') as fb:
        value = ['-pthread']
        posix_vars(build, cflags=value, ldflags=value,
                   cache_key='pthread')

    if base.warnings:
        with Feedback('Checking for warning flags...') as fb:
            posix_warnings(build, werror=base.werror)

    if base.lto:
        try:
            with Feedback('Checking for link-time optimization...') as fb:
                posix_vars(
                    build,
                    cflags=['-g0', '-flto'],
                    ldflags=['-flto'],
                    cache_key='lto')
        except ConfigError:
            pass

    sanitizers = [
        ('asan', base.asan, 'Checking for address sanitizer...',
         ['-fsanitize=address']),
    ]
    for key, enabled, message, flags in sanitizers:
        if not enabled:
            continue
        with Feedback(message) as fp:
            posix_vars(build, cflags=flags, ldflags=flags,
                       cache_key='sanitize.' + key)
            break

    log = logfile(2)
    print('Base compilation variables:', file=log)
    build.base.public_env.dump(file=log, indent='  ')
    print('Base linking variables:', file=log)
    build.base.link_env.dump(file=log, indent='  ')

def update_base(build):
    """Add standard options to the build."""
    if isinstance(build.toolchain, PosixToolchain):
        posix_base(build)
