# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
"""Utilities for parsing JSON/YAML objects."""
import inspect
import os.path as ospath
from pathlib import Path

class ParseError(ValueError):
    __slots__ = ['loc', 'msg']
    def __init__(self, loc, msg):
        super(ParseError, self).__init__(loc, msg)
        self.loc = loc
        self.msg = msg
    def __str__(self):
        return '{}: {}'.format(self.loc, self.msg)

class ExtraKeys(ValueError):
    def __init__(self, keys):
        super(ValueError, self).__init__(
            'Unknown keys: {}'.format(
                ', '.join(repr(x) for x in sorted(keys))))

class BaseField(object):
    __slots__ = []

class RecordField(BaseField):
    __slots__ = ['_type']
    def __init__(self, type):
        self._type = type
    def init(self, root):
        return self._type(root)
    def read(self, value, data, root):
        value.read(data, root)
        return value

class Field(BaseField):
    __slots__ = ['_type', '_parse', '_default']
    def __init__(self, type, *, parse=None, default=None):
        self._type = type
        self._parse = parse
        self._default = default
    def init(self, root):
        if callable(self._default):
            return self._default()
        return self._default
    def read(self, value, data, root):
        if not isinstance(data, self._type):
            raise ValueError('Incorrect type.')
        if self._parse is not None:
            return self._parse(data)
        return data

class PathField(BaseField):
    __slots__ = ['_default']
    def __init__(self, *, default=None):
        self._default = default
    def init(self, root):
        if self._default is None:
            return None
        return Path(ospath.normpath(str(root / self._default)))
    def read(self, value, data, root):
        if not isinstance(data, str):
            raise ValueError('Must be a string.')
        return Path(ospath.normpath(str(root / data)))

class RecordType(type):
    __slots__ = []
    def __new__(class_, name, bases, namespace):
        flat_fields = {}
        class_fields = {}
        ns = {}
        slots = []

        for key, value in namespace.items():
            if isinstance(value, BaseField):
                class_fields[key] = value
                slots.append(key)
            elif key == '__slots__':
                slots.extend(value)
            else:
                ns[key] = value

        ns['_class_fields'] = class_fields
        ns['_fields'] = flat_fields
        record_class = type.__new__(class_, name, bases, ns)
        for base in inspect.getmro(record_class):
            if base is object:
                continue
            for key, value in base._class_fields.items():
                if key in flat_fields:
                    raise ValueError('Duplicate field: {!r}'.format(key))
                flat_fields[key] = value
        return record_class

class Record(object, metaclass=RecordType):
    def __init__(self, root):
        for key, field in self._fields.items():
            setattr(self, key, field.init(root))

    def read(self, data, root):
        """Read from generic JSON/YAML structures."""
        if not isinstance(data, dict):
            raise ValueError('Must be a dictionary.')
        data = dict(data)
        for key, field in self._fields.items():
            dkey = key.replace('_', '-')
            try:
                new_value = data.pop(dkey)
            except KeyError:
                continue
            old_value = getattr(self, key)
            try:
                new_value = field.read(old_value, new_value, root)
            except ParseError as ex:
                raise ParseError('{}.{}'.format(dkey, ex.loc), ex.msg)
            except ValueError as ex:
                raise ParseError(dkey, str(ex))
            setattr(self, key, new_value)
        if data:
            raise ExtraKeys(data.keys())
