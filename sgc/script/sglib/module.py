# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from pathlib import Path
from d4build.source import SourceSet, TagSource

SGLIB_TAGS = '''
frontend-cocoa
frontend-gtk
frontend-sdl
frontend-windows
audio-alsa
audio-coreaudio
audio-directsound
audio-sdl
image-coregraphics
image-libjpeg
image-libpng
image-wincodec
freetype
harfbuzz
ogg
opus
sdl
video-recording
vorbis
'''.split()

def sglib_module(build, app, sglib, cfg):
    sources = SourceSet(sglib.sources)

    from d4build.configheader import config_header
    from .version import VersionInfo
    from .opengl import OpenGLLoader
    generated = [
        config_header(build, cfg),
        VersionInfo(build, [
            ('SG', sglib.root),
            ('APP', app.paths.root),
        ]),
        OpenGLLoader(build, app.opengl),
    ]
    for files in generated:
        build.files.append(files)
        for source in files.outputs(build.config.builddir):
            sources.sources.append(TagSource(source, ()))

    mod = build.source_module(sources, SGLIB_TAGS)

    from .external import math, opengl
    mod.add('public', opengl.module(build), math.module(build))
    mod.add('sdl')

    if cfg.frontend == 'cocoa':
        mod.add('frontend-cocoa', build.frameworks(
            'AppKit Foundation CoreVideo OpenGL Carbon'))
    elif cfg.frontend == 'gtk':
        from .external import gtk, gtkglext
        mod.add('frontend-gtk', gtkglext.module(build), gtk.module(build))
    elif cfg.frontend == 'sdl':
        from .external import sdl
        mod.add('sdl', sdl.version_2(build))
        mod.add('frontend-sdl')
    elif cfg.frontend == 'windows':
        mod.add('frontend-windows')
    else:
        raise ConfigError('Invalid frontend flag: {!r}'.format(cfg.frontend))

    if cfg.audio == 'alsa':
        from .external import alsa
        mod.add('audio-alsa', alsa.module(build))
    elif cfg.audio == 'coreaudio':
        mod.add('audio-coreaudio', build.frameworks(
            'CoreAudio AudioUnit'.split()))
    elif cfg.audio == 'directsound':
        mod.add('audio-directsound')
    elif cfg.audio == 'sdl':
        mod.add('audio-sdl')
    elif flags['audio'] == 'none':
        pass
    else:
        raise ConfigError('Invalid audio flag: {!r}'.format(cfg.audio))

    enable_ogg = False
    if cfg.vorbis:
        from .external import vorbis
        mod.add('vorbis', vorbis.module(build))
        enable_ogg = True
    if cfg.opus:
        from .external import opus
        mod.add('opus', opus.module(build))
        enable_ogg = True
    if enable_ogg:
        from .external import ogg
        mod.add('ogg', ogg.module(build))

    enable_coregraphics = False
    enable_wincodec = False

    if cfg.jpeg == 'coregraphics':
        enable_coregraphics = True
    elif cfg.jpeg == 'libjpeg':
        from .external import libjpeg
        mod.add('image-libjpeg', libjpeg.module(build))
    elif cfg.jpeg == 'wincodec':
        enable_wincodec = True
    elif cfg.jpeg == 'none':
        pass
    else:
        raise ConfigError('Invalid jpeg flag: {!r}'.format(cfg.jpeg))

    if cfg.png == 'coregraphics':
        enable_coregraphics = True
    elif cfg.png == 'libpng':
        from .external import libpng
        mod.add('image-libpng', libpng.module(build))
    elif cfg.png == 'wincodec':
        enable_wincodec = True
    elif cfg.png == 'none':
        pass
    else:
        raise ConfigError('Invalid png flag: {!r}'.format(cfg.png))

    if enable_coregraphics:
        mod.add('image-coregraphics', build.frameworks('CoreGraphics'))
    if enable_wincodec:
        mod.add('image-wincodec')

    if cfg.freetype:
        from .external import harfbuzz, freetype
        mod.add('harfbuzz', harfbuzz.module(build))
        mod.add('freetype', freetype.module(build))

    if cfg.video_recording:
        mod.add('video-recording')

    return mod
