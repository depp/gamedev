# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
"""Application information.

The main class here is App.  It represents the data in an application's
app.yaml file.
"""
import collections
import uuid
from .record import ExtraKeys, Record, RecordField, Field, PathField
from .source import SourceInfo

def parse_gl_version(obj):
    i = obj.find('.')
    if i < 0:
        raise ValueError('Invalid OpenGL version.')
    major = obj[:i]
    minor = obj[i+1:]
    if not major.isdigit() and minor.isdigit():
        raise ValueError('Invalid OpenGL version.')
    return int(major), int(minor)

def parse_gl_extensions(obj):
    for x in obj:
        if not isinstance(x, str):
            raise ValueError('Invalid OpenGL extension.')
    return obj

class Paths(Record):
    """Application paths.

    Properties:
    data -- Path to the application data directory.
    user -- Path to the user data directory.
    """
    root = PathField(default='.')
    data = PathField()
    user = PathField()

class Icon(object):
    """Information about the application icon.

    Properties:
    ico -- Path to the .ico icon.
    sizes -- Map from size to icon path.
    """
    __slots__ = ['ico', 'sizes']
    def __init__(self, root):
        self.ico = None
        self.sizes = {}
    def read(self, data, root):
        if not isinstance(data, dict):
            raise ValueError('Icon must be a dictionary.')
        base = data.get('path', '')
        if not isinstance(base, str):
            raise ValueError('Icon path must be a string.')
        base = (root / base).resolve()
        try:
            files = data['files']
        except KeyError:
            raise ValueError('Icon must have files key.')
        if not isinstance(files, dict):
            raise ValueError('Icon files must be a dictionary.')
        for name, path in files.items():
            if not isinstance(path, str):
                raise ValueError('Icon path must be a string.')
            path = base / path
            if name == 'ico':
                self.ico = path
            elif isinstance(name, int):
                self.sizes[name] = path
            else:
                raise ValueError('Unknown icon name: {!r}'.format(name))
        keys = set(data.keys())
        keys.difference_update({'path', 'files'})
        if keys:
            raise ExtraKeys(keys)

class Info(Record):
    """General information about the app.

    Properties:
    name -- Application name (human readable).
    email -- Developer email address.
    uri -- Application web site URI.
    copyright -- Application copyright notice.
    uuid -- UUID identifying this application.  This is mainly used for
        Visual Studio project generation.
    """
    name = Field(str)
    email = Field(str)
    uri = Field(str)
    copyright = Field(str)
    uuid = Field(str, parse=uuid.UUID)

class OpenGL(Record):
    """OpenGL capabilities information.

    Properties:
    version -- Tuple with maximum understood OpenGL version.
    extensions -- List of understood OpenGL extension names.
    """
    version = Field(str, parse=parse_gl_version, default=(3, 3))
    extensions = Field(list, parse=parse_gl_extensions,
                       default=lambda: ['KHR_debug'])

class AppInfo(SourceInfo):
    """App project description.

    Properties:
    info -- Info object, general application information.
    info_plist -- Dictionary with extra application bundle property list keys.
        Used on Darwin platforms.
    paths -- Paths object with application paths.
    opengl -- OpenGL object, containing OpenGL capabilities.
    icon -- Icon object, information about applictation icon.
    """
    info = RecordField(Info)
    apple_plist = Field(dict)
    paths = RecordField(Paths)
    opengl = RecordField(OpenGL)
    icon = RecordField(Icon)

class SGLibInfo(SourceInfo):
    """SGLib library description.

    Properties:
    root -- Root SGLib path.
    """
    root = PathField(default='.')
