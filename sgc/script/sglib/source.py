# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import collections
import re
from .record import ExtraKeys, ParseError, Record
from d4build.source import SourceSet, TagSource

VALID_TAG = re.compile('[a-z0-9]+(-[a-z0-9]+)*')
SAFE_FILE = re.compile('[-_A-Za-z0-9][-._A-Za-z0-9]*')

def read_tags(data, base):
    """Read source code tags from a YAML object."""
    tags = list(base)
    for tag in data:
        if tag.startswith('-'):
            remove = True
            tag = tag[1:]
        else:
            remove = False
        if not VALID_TAG.fullmatch(tag):
            raise ValueError('Invalid tag: {!r}'.format(tag))
        if remove:
            try:
                i = tags.index(tag)
            except ValueError:
                raise ValueError('Cannot remove tag: {}'.format(tag))
            del tags[i]
        else:
            if tag in tags:
                raise ValueError('Duplicate tag: {!r}'.format(tag))
            tags.append(tag)
    return tuple(tags)

class SourceInfo(Record):
    """Information about a source module.

    Properties:
    sources -- SourceSet.
    """
    __slots__ = ['sources']

    def __init__(self, root):
        super(SourceInfo, self).__init__(root)
        self.sources = SourceSet()

    def read(self, data, root):
        """Read from generic JSON/YAML structures."""
        if not isinstance(data, dict):
            raise ValueError('Not a dictionary.')
        data = dict(data)

        for vis in ['public', 'private']:
            key = vis + '-header-paths'
            try:
                value = data.pop(key)
            except KeyError:
                continue
            if not isinstance(value, list):
                raise ParseError(key, 'Not a list.')
            result = getattr(self.sources, vis + '_ipaths')
            for n, item in enumerate(value):
                if not isinstance(item, str):
                    raise ParseError(
                        '{}[{}]'.format(key, n), 'Not a string.')
                result.append(root / item)

        key = 'sources'
        value = data.pop(key, [])
        if not isinstance(value, list):
            raise ParseError(key, 'Not a list.')
        result = self.sources.sources
        for ngroup, group in enumerate(value):
            def gerr(subkey, msg):
                loc = '{}[{}]'.format(key, ngroup)
                if subkey is not None:
                    loc = '{}.{}'.format(loc, subkey)
                raise ParseError(loc, msg)
            if not isinstance(group, dict):
                gerr(None, 'Not a dictionary.')
            group = dict(group)
            base = group.pop('path', '')
            if not isinstance(base, str):
                gerr('path', 'Not a string.')
            base = (root / base).resolve()
            try:
                tags = group.pop('tags')
            except KeyError:
                tags = ()
            else:
                if not isinstance(tags, str):
                    gerr('tags', 'Not a string.')
                try:
                    tags = read_tags(tags.split(), ())
                except ValueError as ex:
                    gerr('tags', ex)
            try:
                gfiles = group.pop('files')
            except KeyError:
                raise ParseError(None, 'No files.')
            if not isinstance(gfiles, list):
                raise ValueError(gkey + '.list', 'Not a list.')
            for nitem, item in enumerate(gfiles):
                def ferr(msg):
                    raise ParseError('{}.files.[{}]'.format(gkey, nitem), msg)
                if not isinstance(item, str):
                    ferr('Not a string.')
                fields = item.split()
                if not fields:
                    raise ValueError('Empty file.')
                fname = fields[0]
                if not SAFE_FILE.fullmatch(fname):
                    raise ValueError('Bad filename: {!r}'.format(fname))
                fpath = base / fname
                ftags = read_tags(fields[1:], tags)
                result.append(TagSource(fpath, ftags))
            if group:
                raise gerr(None, ExtraKeys(group))

        read = super(SourceInfo, self).read(data, root)
