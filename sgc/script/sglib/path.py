# Copyright 2014-2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.source import join_path
import os.path

_BASE = join_path(os.path.relpath(__file__), '../..')
def sglib_path(*paths):
    """Get a path relative to the SGLib root directory."""
    return join_path(_BASE, *paths)
