# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from pathlib import Path

class BuildScriptFactory(object):
    __slots__ = [
        'app_path',
        'sglib_path',
    ]

    def __init__(self, app_path, sglib_path):
        self.app_path = app_path
        self.sglib_path = sglib_path

    def load(self):
        import yaml
        from d4build.error import ConfigError
        from .info import AppInfo, SGLibInfo
        try:
            app = AppInfo(self.app_path.parent)
            with open(str(self.app_path)) as fp:
                data = yaml.safe_load(fp)
            app.read(data, self.app_path.parent)
        except ValueError as ex:
            raise ConfigError('{}: {}'.format(self.app_path, ex))
        try:
            sglib = SGLibInfo(self.sglib_path.parent)
            with open(str(self.sglib_path)) as fp:
                data = yaml.safe_load(fp)
            sglib.read(data, self.sglib_path.parent)
        except ValueError as ex:
            raise ConfigError('{}: {}'.format(self.sglib_path, ex))
        return BuildScript(self, app, sglib)

class BuildScript(object):
    __slots__ = [
        'factory',
        'app',
        'sglib',
    ]

    def __init__(self, factory, app, sglib):
        self.factory = factory
        self.app = app
        self.sglib = sglib

    def sources(self):
        """Iterate over all source files in the project."""
        for src in self.app.sources.sources:
            yield src.path
        for src in self.sglib.sources.sources:
            yield src.path

    def build(self, build):
        """Create the build script."""
        build.inputs.extend([
            self.factory.app_path,
            self.factory.sglib_path,
        ])
        build.inputs.extend(
            path for path in Path(__file__).resolve().parent.rglob('*.py')
            if not path.name.startswith('.'))
        from .app import build_app
        from .base import update_base
        update_base(build)
        build_app(build, self.app, self.sglib)

    @property
    def srcdir(self):
        return self.app.paths.root

def run():
    """Run the top-level configuration script."""
    import sys
    from d4build.tool import run
    if len(sys.argv) < 2:
        print('Error: Usage: {} app.yaml [options...]', file=sys.stderr)
        raise SystemExit(1)
    sglib_path = (Path(__file__).resolve()
                  .parent.parent.parent.joinpath('sources.yaml'))
    app_path = Path(sys.argv[1]).resolve()
    run(BuildScriptFactory(app_path, sglib_path), sys.argv[2:])
