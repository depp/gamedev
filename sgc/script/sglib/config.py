# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.environment import Environment, EnvOptions, EnvBoolean

class ConfigEnvironment(Environment):
    frontend = EnvOptions('Application front end', [
        ('cocoa', 'Use Cocoa front end.'),
        ('gtk', 'Use Gtk front end.'),
        ('sdl', 'Use LibSDL front end.'),
        ('windows', 'Use Windows front end.'),
    ])
    audio = EnvOptions('Audio interface', [
        ('alsa', 'Use ALSA.'),
        ('coreaudio', 'Use Core Audio.'),
        ('directsound', 'Use DirectSound.'),
        ('sdl', 'Use LibSDL.'),
        ('none', 'Disable audio.'),
    ])
    vorbis = EnvBoolean('Vorbis decoding')
    opus = EnvBoolean('Opus decoding')
    jpeg = EnvOptions('JPEG decoding', [
        ('libjpeg', 'Use LibJPEG.'),
        ('coregraphics', 'Use Core Graphics.'),
        ('wincodec', 'Use WinCodec.'),
        ('none', 'Disable JPEG decoding.'),
    ])
    png = EnvOptions('PNG decoding and encoding', [
        ('libpng', 'Use LibPNG.'),
        ('coregraphics', 'Use Core Graphics.'),
        ('wincodec', 'Use WinCodec.'),
        ('none', 'Disable PNG decoding and encoding.'),
    ])
    freetype = EnvBoolean('Type rendering')
    video_recording = EnvBoolean('Video recording')

COMMON_DEFAULTS = ConfigEnvironment(
    vorbis=True,
    opus=True,
    freetype=True,
    video_recording=False,
)

PLATFORM_DEFAULTS = {
    'linux': ConfigEnvironment(
        jpeg='libjpeg',
        png='libpng',
        frontend='sdl',
        audio='alsa',
    ),
    'osx': ConfigEnvironment(
        jpeg='coregraphics',
        png='coregraphics',
        frontend='cocoa',
        audio='coreaudio',
    ),
    'windows': ConfigEnvironment(
        jpeg='wincodec',
        png='wincodec',
        frontend='windows',
        audio='directsound',
    ),
}

def sglib_config(build):
    cfg = ConfigEnvironment()
    cfg.update(COMMON_DEFAULTS, PLATFORM_DEFAULTS[build.config.target])
    cfg_user = ConfigEnvironment()
    cfg_user.update_env(build.config.variables)
    if (cfg_user.frontend or cfg.frontend) == 'sdl':
        cfg.audio = 'sdl'
    cfg.update(cfg_user)
    return cfg
