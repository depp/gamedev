# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from d4build.file import update_bytes
from d4build.generatedfiles import GeneratedFiles
from d4build.plist.xml import dump
from .version import git_dir, version_dependency, get_version

class InfoPlist(GeneratedFiles):
    """Generator for application info plist file (Apple targets)."""
    __slots__ = [
        'git',
        'gitdir',
        'output',
        'values',
        'copyright',
        'icon',
        'main_nib',
    ]

    def __init__(self, build, app, icon, main_nib):
        self.git = 'git'
        path = git_dir(self.git, app.paths.root)
        if path is not None:
            path = path.relative_to(build.config.builddir)
        self.gitdir = path
        self.output = build.config.buildsrc.joinpath('Info.plist')
        self.values = app.apple_plist
        self.copyright = app.info.copyright
        self.icon = icon
        self.main_nib = main_nib

    def inputs(self, builddir):
        if self.gitdir is not None:
            yield version_dependency(builddir / self.gitdir)

    def outputs(self, builddir):
        yield builddir / self.output

    def generate(self, builddir, progress):
        path = self.gitdir
        if path is not None:
            path = builddir / path
        ver = get_version(self.git, path)

        if self.copyright is not None:
            getinfo = '{}, {}'.format(ver.desc, self.copyright)
        else:
            getinfo = ver.desc

        plist = {
            'CFBundleDevelopmentRegion': 'English',
            'CFBundleExecutable': '$(EXECUTABLE_NAME)',
            'CFBundleGetInfoString': getinfo,
            # CFBundleName
            'CFBundleIconFile': self.icon,
            'CFBundleInfoDictionaryVersion': '6.0',
            'CFBundlePackageType': 'APPL',
            'CFBundleShortVersionString': ver.desc,
            'CFBundleSignature': '????',
            'CFBundleVersion': '{}.{}.{}'.format(*ver.number),
            # LSArchicecturePriority
            # LSFileQuarantineEnabled
            'LSMinimumSystemVersion': '10.7.0',
            'NSMainNibFile': self.main_nib,
            'NSPrincipalClass': 'GApplication',
            # NSSupportsAutomaticTermination
            # NSSupportsSuddenTermination
        }
        plist.update(self.values)
        plist = {k: v for k, v in plist.items() if v is not None}
        path = builddir / self.output
        progress(path)
        update_bytes(path, dump(plist))
