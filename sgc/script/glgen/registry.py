# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import collections
import io
from pathlib import Path
import re
import xml.etree.ElementTree as etree
from .api import Version

def registry_path():
    """"Get the path to the default registry file."""
    return Path(__file__).parent.joinpath('gl.xml')

Type = collections.namedtuple('Type', 'name definition requires')
Function = collections.namedtuple('Function', 'proto params')
Parameter = collections.namedtuple('Parameter', 'name fulltype typename')

def flat_text(node, tagmap={}):
    """Flatten XML tree, returning just the text."""
    fp = io.StringIO()
    def visit(node):
        try:
            fp.write(tagmap[node.tag])
            return
        except KeyError:
            pass
        if node.text:
            fp.write(node.text)
        for child in node:
            visit(child)
            if child.tail:
                fp.write(child.tail)
    visit(node)
    return fp.getvalue()

def xml_info(node):
    """Print a lists of tags and attributes in an XML tree."""
    tags = set()
    attrib = set()
    def scan(node):
        tags.add(node.tag)
        attrib.update(node.attrib)
        for child in node:
            scan(child)
    scan(node)
    print('Tags:', ' '.join(sorted(tags)))
    print('Attributes:', ' '.join(sorted(attrib)))

def get_param(node):
    """Get a type/name from an XML tree."""
    if len(node) == 1:
        name, = node
        fulltype = [node.text]
        typename = None
    elif len(node) == 2:
        ptype, name = node
        assert ptype.tag == 'ptype'
        typename = ptype.text
        fulltype = [node.text, typename, ptype.tail]
    else:
        assert False
    fulltype = ''.join(x or '' for x in fulltype).strip()
    assert name.tag == 'name'
    name = name.text
    return Parameter(name, fulltype, typename)

def cmd_name(name):
    """Shorten a command name."""
    assert name.startswith('gl')
    return name[2:]

def enum_name(name):
    """Shorten an enumeration name."""
    assert name.startswith('GL_')
    return name[3:]

Interface = collections.namedtuple('Interface', [
    # Set of names of exposed commands.
    'commands',
    # Set of names of exposed enums.
    'enums',
])

class Interface(object):
    """Subset of the OpenGL interface.

    Attributes:
    commands -- Set of exposed command names.
    enums -- Set of exposed enum names.
    """
    __slots__ = ['commands', 'enums']

    def __init__(self):
        self.commands = set()
        self.enums = set()

    def _add(self, node):
        for child in node:
            if child.tag == 'command':
                self.commands.add(cmd_name(child.attrib['name']))
            elif child.tag == 'enum':
                self.enums.add(enum_name(child.attrib['name']))
            else:
                assert child.tag == 'type'

class ParseState(object):
    __slots__ = ['remove']

class Registry(object):
    """OpenGL API registry.

    Attributes:
    api -- The OpenGL api, an Api object.
    types -- List of defined types, Type objects.
    groups -- Map from group name to list of enum names.
    enums -- Map from enum names to integer values.
    commands -- Map from function names to Function objects.
    versions -- Map from version numbers to Interface objects.
    extensions -- Map from extension names to Interface objects.
    """
    __slots__ = [
        'api',
        'types',
        'groups',
        'enums',
        'commands',
        'versions',
        'extensions',
    ]

    def __init__(self, api):
        self.api = api
        self.types = []
        self.groups = {}
        self.enums = {}
        self.commands = {}
        self.versions = {}
        self.extensions = {}

    def read(self, path):
        """Load an OpenGL API registry file."""
        tree = etree.parse(str(path))
        state = ParseState()
        state.remove = Interface()
        for e in tree.getroot():
            try:
                func = getattr(self, '_read_' + e.tag)
            except AttributeError:
                raise ValueError('Unknown tag: {}'.format(e.tag))
            func(e, state)
        commands = set(state.remove.commands)
        enums = set(state.remove.enums)
        for version, iface in sorted(self.versions.items()):
            iface.commands.difference_update(commands)
            iface.enums.difference_update(enums)
            commands.update(iface.commands)
            enums.update(iface.enums)

    def _read_comment(self, node, state):
        """Read a top-level comment tag."""
        pass

    def _read_types(self, node, state):
        """Read a top-level types tag."""
        tagmap = {'apientry': 'SG_GL_API'}
        for child in node:
            assert child.tag == 'type'
            if child.attrib.get('api', 'gl') != self.api.api:
                continue
            try:
                name = child.attrib['name']
            except KeyError:
                for e in child:
                    if e.tag == 'name':
                        name = e.text
                        break
                else:
                    assert False
            self.types.append(Type(
                name,
                flat_text(child, tagmap),
                child.attrib.get('requires')))

    def _read_groups(self, node, state):
        """Read a top-level groups tag."""
        for child in node:
            assert child.tag == 'group'
            gname = child.attrib['name']
            assert gname not in self.groups
            group = []
            self.groups[gname] = group
            for child2 in child:
                group.append(enum_name(child2.attrib['name']))

    def _read_enums(self, node, state):
        """Read a top-level enums tag."""
        gname = (node.attrib['namespace'], node.attrib.get('group', '-'))
        try:
            gdata = self.enums[gname]
        except KeyError:
            gdata = {}
            self.enums[gname] = gdata
        for child in node:
            if child.tag != 'enum':
                assert child.tag == 'unused'
                continue
            value = int(child.attrib['value'], 0)
            gdata[enum_name(child.attrib['name'])] = value
        self.enums.update(gdata)

    def _read_commands(self, node, state):
        """Read a top-level commands tag."""
        for child in node:
            assert child.tag == 'command'
            assert len(child) > 0
            assert child[0].tag == 'proto'
            proto = get_param(child[0])
            params = []
            for child2 in child[1:]:
                if child2.tag == 'param':
                    params.append(get_param(child2))
                else:
                    assert child2.tag in ('glx', 'vecequiv', 'alias')
            name = cmd_name(proto.name)
            assert name not in self.commands
            self.commands[name] = Function(proto, params)

    def _read_feature(self, node, state):
        """Read a top-level feature tag."""
        if node.attrib['api'] != self.api.api:
            return
        version = Version.parse(node.attrib['number'])
        iface = Interface()
        for child in node:
            prof = child.attrib.get('profile')
            if prof is not None and prof != self.api.profile:
                continue
            if child.tag == 'require':
                iface._add(child)
            elif child.tag == 'remove':
                state.remove._add(child)
            else:
                assert False
        self.versions[version] = iface

    def _read_extensions(self, node, state):
        """Read a top-level extensions tag."""
        for child in node:
            supported = child.attrib['supported'].split('|')
            if self.api.api not in supported:
                continue
            name = enum_name(child.attrib['name'])
            iface = Interface()
            for child2 in child:
                if child2.tag == 'require':
                    iface._add(child2)
                else:
                    assert False
            self.extensions[name] = iface
