# Copyright 2015-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import argparse
import os
import platform
import subprocess
import sys
from .loader import LoaderSpec, registry_loader
from .registry import Registry, registry_path
from .api import Api, Version

def cmd_emit(reg, args):
    """Run the command-line 'emit' command."""
    spec = LoaderSpec(
        args.platform,
        args.api,
        args.max_version,
        args.extensions,
    )
    loader = registry_loader(reg, spec)
    sources = []
    sources.extend(loader.headers)
    sources.extend(loader.sources)
    for source in sources:
        path = os.path.join(args.out, source.name)
        print('Creating {}...'.format(path))
        with open(path, 'w') as fp:
            fp.write(source.contents)

def cmd_scan(reg, args):
    """Run the command-line 'scan' command."""
    if args.library is None:
        system = platform.system()
        if system == 'Darwin':
            path = ('/System/Library/Frameworks/OpenGL.framework/Versions'
                    '/Current/Libraries/libGL.dylib')
        else:
            print('Specify --library', file=sys.stderr)
            raise SystemExit(1)
    else:
        path = args.library
    functions = set()
    for func in library_functions(path):
        if func.startswith('gl'):
            functions.add(func[2:])
    dfunctions = set()
    max_version = 0, 0
    for version, iface in sorted(reg.versions.items()):
        if not (functions.issuperset(iface.commands)):
            break
        max_version = version
        dfunctions.update(iface.commands)
    print('Maximum version: {}'.format(version))
    nsup, nuns, nzer, nred = 0, 0, 0, 0
    print('Extensions:')
    for name, iface in sorted(reg.extensions.items()):
        if not iface.commands:
            nzer += 1
        elif dfunctions >= iface.commands:
            nred += 1
        elif not (functions >= iface.commands):
            nuns += 1
        else:
            nsup += 1
            print('  ' + name)
    print('Extensions supported: {} ({} not redundant)'
          .format(nsup + nred, nsup))
    print('Extensions unsupported:', nuns)
    print('Extensions without entry points:', nzer)

def library_functions(path):
    """Get a list of functions defined in a library."""
    strip_underscore = False
    args = ['nm']
    system = platform.system()
    print('System', system)
    if system == 'Darwin':
        strip_underscore = True
        args.append('-U')
    elif system == 'Linux':
        args.extend(('--dynamic', '--defined-only'))
    args.append(path)
    stdout = subprocess.check_output(args)
    for line in stdout.splitlines():
        fields = line.split()
        if fields[-2] != b'T':
            continue
        name = fields[2]
        if strip_underscore:
            if name.startswith(b'_'):
                name = name[1:]
            else:
                continue
        name = name.decode('ASCII')
        yield name

def main():
    import argparse
    p = argparse.ArgumentParser()

    p.add_argument(
        '--reg-path',
        help='Path to the OpenGL XML registry file')
    p.add_argument(
        '--api',
        type=Api.parse,
        choices=Api.APIS,
        default=Api('gl', 'core'),
        help='The OpenGL API')

    ss = p.add_subparsers()

    pp = ss.add_parser('emit', help='emit OpenGL headers and data')
    pp.add_argument(
        '--out', default='.',
        help='Output directory')
    pp.add_argument(
        'platform', choices=('windows', 'osx', 'linux'))
    pp.add_argument(
        'max_version', type=Version.parse,
        help='Maximum version to emit, e.g., 3.2')
    pp.add_argument(
        'extensions', nargs='*',
        help='Extensions to emit')
    pp.set_defaults(cmd='emit')

    pp = ss.add_parser('scan', help='calculate supported OpenGL version')
    pp.set_defaults(cmd='scan')
    pp.add_argument(
        '--library', help='library to scan')

    args = p.parse_args()
    reg = Registry(args.api)
    reg_path = args.reg_path
    if reg_path is None:
        reg_path = registry_path()
    reg.read(reg_path)
    if args.cmd == 'scan':
        cmd_scan(reg, args)
    elif args.cmd == 'emit':
        cmd_emit(reg, args)
    else:
        assert False

if __name__ == '__main__':
    main()
