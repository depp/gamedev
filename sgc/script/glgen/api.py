# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.

class Api(object):
    """An OpenGL API.

    Attributes:
    api -- The API name: gl, gles1, or gles2.
    profile -- The API profile: None, core, or compatibility.
    """
    __slots__ = ['api', 'profile']

    def __init__(self, api, profile):
        self.api = api
        self.profile = profile

    def __eq__(self, other):
        if isinstance(other, Api):
            return self.api == other.api and self.profile == other.profile
        return NotImplemented

    def __hash__(self):
        return hash((self.api, self.profile))

    def __str__(self):
        if self.profile is None:
            return self.api
        return '{0.api}:{0.profile}'.format(self)

    def __repr__(self):
        return 'Api({0.api!r}, {0.profile!r})'.format(self)

    @classmethod
    def parse(class_, value):
        """Parse an OpenGL API name."""
        i = value.find(':')
        if i >= 0:
            api = class_(value[:i], value[i+1:])
        else:
            api = class_(value, None)
        if api not in class_.APIS:
            raise ValueError('Unknown API.')
        return api

    APIS = []

Api.APIS.extend([
    Api('gl', 'core'),
    Api('gl', 'compatibility'),
    Api('gles1', None),
    Api('gles2', None),
])

class Version(object):
    """An OpenGL version.

    Attributes:
    major -- The major version number.
    minor -- The minor version number.
    """
    __slots__ = ['major', 'minor']

    def __init__(self, major, minor):
        self.major = major
        self.minor = minor

    def __eq__(self, other):
        if isinstance(other, Version):
            return self.major == other.major and self.minor == other.minor
        return NotImplemented

    def __lt__(self, other):
        if not isinstance(other, Version):
            raise TypeError('Not a version.')
        return (self.major, self.minor) < (other.major, other.minor)
    def __le__(self, other):
        if not isinstance(other, Version):
            raise TypeError('Not a version.')
        return (self.major, self.minor) <= (other.major, other.minor)
    def __gt__(self, other):
        if not isinstance(other, Version):
            raise TypeError('Not a version.')
        return (self.major, self.minor) > (other.major, other.minor)
    def __ge__(self, other):
        if not isinstance(other, Version):
            raise TypeError('Not a version.')
        return (self.major, self.minor) >= (other.major, other.minor)

    def __hash__(self):
        return hash((self.major, self.minor))

    def __str__(self):
        return '{0.major}.{0.minor}'.format(self)

    def __repr__(self):
        return 'Version({0.major!r}, {0.minor!r})'.format(self)

    @classmethod
    def parse(class_, value):
        """Parse an OpenGL version number."""
        i = value.index('.')
        major = int(value[:i])
        minor = int(value[i+1:])
        if major <= 0 or minor < 0:
            raise ValueError('Invalid version.')
        return class_(major, minor)
