# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import collections.abc
import pickle

class PickleDictLoadError(ValueError):
    pass

class PickleDict(collections.abc.MutableMapping):
    """Pickled object dictionary.

    Like an ordinary dictionary, but the values are stored pickled.  This
    allows the entire dictionary to be pickled and unpickled even if
    some of the objects cannot be unpickled.
    """
    __slots__ = [
        # The dictionary data.
        'data',
        # Whether the data has been modified since this flag was last cleared.
        'dirty',
        # Data version number.
        'version',
    ]

    def __init__(self, *, version):
        self.data = {}
        self.dirty = False
        self.version = version

    def load(self, fp):
        """Load the dictionary from a file."""
        try:
            data_version, data = pickle.load(fp)
        except (pickle.UnpicklingError, ValueError):
            raise PickleDictLoadError()
        if data_version != self.version:
            raise PickleDictLoadError()
        self.data = data

    def dump(self, fp):
        """Save the dictionary to a file."""
        pickle.dump((self.version, self.data), fp,
                    protocol=pickle.HIGHEST_PROTOCOL)

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        return iter(self.data)

    def __contains__(self, key):
        return key in self.data

    def __getitem__(self, key):
        if not isinstance(key, str):
            raise TypeError('key must be string')
        return pickle.loads(self.data[key])

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError('key must be string')
        self.data[key] = pickle.dumps(value, protocol=pickle.HIGHEST_PROTOCOL)
        self.dirty = True

    def __delitem__(self, key):
        del self.data[key]
        self.dirty = True
