# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import importlib
from pathlib import Path
import tempfile
from .cache import Cache
from .configuration import Configuration
from .error import ConfigError
from .log import Log, logfile
from .module import Module, SourceModule
from .storage import load_storage, save_storage

LOG_PATH = 'config.log'

def configure(*, factory=None, variables=None, verbosity=1, clear_cache=False):
    """Configure the project and generate the build script."""
    storage = load_storage()
    builddir = Path.cwd()
    if factory is not None:
        storage['factory'] = factory
    else:
        factory = storage['factory']
    if variables is not None:
        storage['variables'] = variables
    else:
        variables = storage['variables']
    discard_keys = [key for key in storage.keys() if key.startswith('regen:')]
    for key in discard_keys:
        del storage[key]
    if clear_cache:
        try:
            del storage['cache']
        except KeyError:
            pass
    with Log(LOG_PATH, verbosity):
        print('Configuring', file=logfile(2))
        try:
            script = factory.load()
            with tempfile.TemporaryDirectory() as tempdir:
                config = Configuration(
                    variables, script.srcdir, builddir, Path(tempdir))
                build = Build(storage, config)
                build.inputs.extend(
                    path for path in
                    Path(__file__).resolve().parent.rglob('*.py')
                    if not path.name.startswith('.'))
                build.sources.extend(script.sources())
                script.build(build)
                build.generator.generate(build)
                logfp = logfile(1)
                builddir = config.builddir
                def progress(path):
                    path = path.relative_to(builddir)
                    print('Creating {}'.format(path), file=logfp)
                for files in build.files:
                    if files.do_regenerate:
                        name = 'regen:' + str(
                            next(iter(files.outputs(Path('.')))))
                        storage[name] = files
                    if files.do_generate:
                        files.generate(builddir, progress)
        except ConfigError as ex:
            ex.write(logfile(None))
            raise
    save_storage(storage)

class Build(object):
    """A build script invocation.

    Attributes:
    storage -- Persistent storage object, dictionary-like object.
    cache -- Build cache, dictionary-like object.
    config -- Build configuration, instance of Configuration.
    toolchain -- The toolchain, instance of Toolchain.
    generator -- The build script generator, instance of Generator.
    sources -- List of input sources files, absolute paths.
    inputs -- List of build script dependencies, absolute paths.
    files -- List of generated files, instances of GeneratedFiles.
    targets -- List of targets, instances of Target.
    """
    __slots__ = [
        'storage',
        'cache',
        'config',
        'toolchain',
        'generator',
        'sources',
        'inputs',
        'files',
        'targets',
    ]

    def __init__(self, storage, config):
        self.storage = storage
        self.cache = Cache()
        self.cache.load(storage.get('cache'))
        self.config = config
        module = importlib.import_module('.' + config.toolchain, __package__)
        self.toolchain = module.TOOLCHAIN(config)
        self.toolchain.base.update_env(config.variables)
        module = importlib.import_module('.' + config.generator, __package__)
        self.generator = module.GENERATOR(config)
        self.sources = []
        self.inputs = []
        self.files = []
        self.targets = []

    def check_platform(self, platform):
        """Raise a build error if the target platform is not the given one."""
        if self.config.target != platform:
            raise ConfigError('Unsupported platform.')

    # Target methods

    @property
    def base(self):
        """Get the base module."""
        return self.toolchain.base

    def module(self):
        """Create a module."""
        return Module(self.toolchain)

    def source_module(self, sources, tags):
        """Create a source module."""
        return SourceModule(self.toolchain, sources, tags)

    def frameworks(self, frameworks):
        """Create a module which links with the given frameworks.

        Arguments:
        frameworks -- List of framework names.
        """
        return self.toolchain.frameworks(frameworks)

    def test_compile(self, source, sourcetype, mod, *,
                     external=True, link=True):
        """Try to compile a source file.

        Returns True if successful, False otherwise.

        Arguments:
        source -- Contents of the source to compile, a string.
        sourcetype -- Type of the source file.
        mod -- The module to use as a dependency for testing.
        external -- Whether to compile as an external / third-party source.
        link -- Whether to also link the file.
        """
        return self.toolchain.test_compile(
            source, sourcetype, mod, external=external, link=link)

    def pkg_config(self, spec):
        """Create a module from a pkg-config package.

        Arguments:
        spec -- The package specification, a string to pass to pkg-config.
        """
        return self.toolchain.pkg_config(spec)

    def sdl_config(self, version):
        """Create a module from a package found with sdl-config.

        Arguments:
        version -- The SDL major version, either 1 or 2.
        """
        return self.toolchain.sdl_config(version)

    # Generator methods

    def runpath(self, path):
        """Convert a path to a string path that gives the path at runtime."""
        return self.generator.runpath(path)
