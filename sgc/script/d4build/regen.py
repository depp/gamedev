# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
"""Build system regeneration.

This is how the build system calls back into the configuration system.  It
is used for regenerating makefiles, project files, built sources, et cetera
as needed by the generated build system.
"""
import sys
from .storage import load_storage, save_storage

def die(msg):
    print('Error:', msg, file=sys.stderr)
    raise SystemExit(1)

def regen():
    """Regenerate a file built by a Python script."""
    from pathlib import Path
    if len(sys.argv) < 3:
        die('Invalid usage.')
    storage = load_storage()
    path = sys.argv[2]
    files = storage.get('regen:' + sys.argv[2])
    if files is None:
        die('Missing configuration data: {!r}'.format(sys.argv[2]))
    def progress(path):
        pass
    files.generate(Path('.'), progress)

def run():
    """Run a command from the build system."""
    if len(sys.argv) < 2:
        die('Invalid usage.')
    cmd = sys.argv[1]
    if cmd == 'regen':
        regen()
    elif cmd == 'reconfigure':
        from .build import configure
        configure()
    elif cmd == 'reconfigure-full':
        from .build import configure
        configure(clear_cache=True)
    else:
        die('Invalid usage.')

if __name__ == '__main__':
    run()
