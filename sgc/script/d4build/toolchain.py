# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from .error import ConfigError
from .module import Module

class Toolchain(object):
    """Abstract base class for platform toolchains.

    Attributes:
    config -- The build configuration.
    base -- Base settings module, first dependency for all targets.
    """
    __slots__ = [
        'config',
        'base',
    ]

    def __init__(self, config):
        self.config = config
        self.base = Module(self)

    def compile_env(self):
        """Create an empty compilation environment."""
        raise NotImplementedError()

    def link_env(self):
        """Create an empty linking environment."""
        raise NotImplementedError()

    def frameworks(self, frameworks):
        """Create a module which links with the given frameworks.

        Arguments:
        frameworks -- List of framework names.
        """
        raise ConfigError('This target does not support frameworks.')

    def test_compile(self, source, sourcetype, mod, *,
                     external=True, link=True):
        """Try to compile a source file.

        Returns True if successful, False otherwise.

        Arguments:
        source -- Contents of the source to compile, a string.
        sourcetype -- Type of the source file.
        mod -- The module to use as a dependency for testing.
        external -- Whether to compile as an external / third-party source.
        link -- Whether to also link the file.
        """
        raise ConfigError('This target does not support test compilation.')

    def pkg_config(self, spec):
        """Create a module from a pkg-config package.

        Arguments:
        spec -- The package specification, a string to pass to pkg-config.
        """
        raise ConfigError('This target does not support pkg-config.')

    def sdl_config(self, version):
        """Create a module from a package found with sdl-config.

        Arguments:
        version -- The SDL major version, either 1 or 2.
        """
        raise ConfigError('This target does not support sdl-config.')
