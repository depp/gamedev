# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from .environment import Environment, EnvString, EnvFlags, EnvPaths
from .error import ConfigError, format_block
from .log import logfile
from .module import Module
from .toolchain import Toolchain
from .shell import escape_cmd, get_output
from .source import SRCTYPE_EXT

class PosixCompileEnvironment(Environment):
    """Environment for compiling on POSIX-like toolchains."""
    cc = EnvString('C compiler')
    ipaths = EnvPaths('Header search paths', private=True)
    cppflags = EnvFlags('C preprocessor flags')
    cflags = EnvFlags('C compilation flags')
    cwarn = EnvFlags('C warning flags')

    def cc_command(self, output, source, sourcetype, *,
                   depfile=None, external=False):
        """Get the command to compile a source file.

        Arguments:
        output -- Path to the output file.
        source -- Path to the source file.
        sourcetype -- The source file type
        depfile -- Path to the generated dependency file.
        external -- Whether the source file is an external / third-party file.
        """
        cmd = [self.cc, '-o', str(output), str(source), '-c']
        if depfile is not None:
            cmd.extend(['-MF', str(depfile), '-MMD', '-MP'])
        cmd.extend('-iquote' + str(ipath) for ipath in self.ipaths)
        cmd.extend(self.cppflags)
        cmd.extend(self.cflags)
        if not external:
            cmd.extend(self.cwarn)
        return cmd

class PosixLinkEnvironment(Environment):
    """Environment for linking on POSIX-like toolchains."""
    cc = EnvString('C compiler')
    ldflags = EnvFlags('Linker flags')
    libs = EnvFlags('Linker flags specifying libraries')

    def ld_command(self, output, sources, sourcetypes):
        """Get the command to link object files.

        Arguments:
        output -- Path to the output file.
        sources -- Paths to the input objects.
        sourcetypes -- Set of types of source files.
        """
        cmd = [self.cc]
        cmd.extend(self.ldflags)
        cmd.extend(['-o', str(output)])
        cmd.extend(str(source) for source in sources)
        cmd.extend(self.libs)
        return cmd

class PosixToolchain(Toolchain):
    """Toolchain for POSIX-like build environments."""
    __slots__ = []

    def __init__(self, config):
        super(PosixToolchain, self).__init__(config)
        self.base.update(
            cc='cc',
            cflags=['-O2'],
            ipaths=[config.builddir / config.buildinclude],
        )

    def compile_env(self):
        """Create an empty compilation environment."""
        return PosixCompileEnvironment()

    def link_env(self):
        """Create an empty linking environment."""
        return PosixLinkEnvironment()

    def test_compile(self, source, sourcetype, mod, *,
                     external=True, link=True):
        """Try to compile a source file.

        Returns True if successful, False otherwise.

        Arguments:
        source -- Contents of the source to compile, a string.
        sourcetype -- Type of the source file.
        mod -- The module to use as a dependency for testing.
        external -- Whether to compile as an external / third-party source.
        link -- Whether to also link the file.
        """
        log = logfile(2)
        print('Testing compilation.', file=log)
        print('Source type:', sourcetype, file=log)
        log.write(format_block(source))
        dirpath = self.config.tempdir
        file_src = dirpath / ('config' + SRCTYPE_EXT[sourcetype])
        file_obj = dirpath / 'config.o'
        file_out = dirpath / 'out'
        with open(str(file_src), 'w') as fp:
            fp.write(source)
        cc_env = mod.flat_compile_env(self, [self.base, mod])
        if link:
            ld_env = mod.flat_link_env(self, [self.base, mod])
        # for variant in self.schema.get_variants(configs, archs)
        # print('Variant:', variant, file=log)
        cmds = [cc_env.cc_command(
            file_obj, file_src, sourcetype, external=external)]
        if link:
            cmds.append(ld_env.ld_command(
                file_out, [file_obj], [sourcetype]))
        for cmd in cmds:
            print('Command:', escape_cmd(cmd), file=log)
            try:
                stdout, retcode = get_output(cmd, combine_output=True)
            except ConfigError as ex:
                print(ex, file=log)
                return False
            log.write(format_block(stdout))
            if retcode != 0:
                print('Command failed.', file=log)
                return False
            print('Command succeeded.', file=log)
        return True

    def pkg_config(self, spec):
        """Create a module from a pkg-config package.

        Arguments:
        spec -- The package specification, a string to pass to pkg-config.
        """
        cmdname = 'pkg-config'
        results = []
        for arg in ('--libs', '--cflags'):
            stdout, stderr, retcode = get_output(
                [cmdname, '--silence-errors', arg, spec])
            if retcode:
                stdout, retcode = get_output(
                    [cmdname, '--print-errors', '--exists', spec],
                    combine_output=True)
                raise ConfigError(
                    '{} failed (spec: {})'.format(cmdname, spec),
                    details=stdout)
            results.append(stdout.split())
        mod = Module(self)
        mod.link_env.update(libs=results[0])
        mod.public_env.update(cflags=results[1])
        return mod

    def sdl_config(self, version):
        """Create a module from a package found with sdl-config.

        Arguments:
        version -- The SDL major version, either 1 or 2.
        """
        if version == 1:
            cmdname = 'sdl-config'
        elif version == 2:
            cmdname = 'sdl2-config'
        else:
            raise ValueError('unknown SDL version: {!r}'.format(version))
        results = []
        for arg in ('--libs', '--cflags'):
            stdout, stderr, retcode = get_output([cmdname, arg])
            if retcode:
                raise ConfigError('{} failed'.format(cmdname), details=stderr)
            results.append(stdout.split())
        mod = Module(self)
        mod.link_env.update(libs=results[0])
        mod.public_env.update(cflags=results[1])
        return mod

TOOLCHAIN = PosixToolchain
