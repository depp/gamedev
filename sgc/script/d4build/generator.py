# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from .log import logfile

class Generator(object):
    """Abstract base class for build script generators."""
    __slots__ = []

    def generate(self, build):
        """Generate the build script.

        Arguments:
        build -- The Build instance.
        """
        raise NotImplementedError()

    def runpath(self, path):
        """Convert a path to a string path that gives the path at runtime."""
        raise NotImplementedError()
