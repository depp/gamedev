# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from pathlib import Path
import platform
import shlex
from .error import ConfigError
from .log import logfile

# Map from platform.system() to platform name
SYSTEM_PLATFORMS = {
    'Darwin': 'osx',
    'Linux': 'linux',
    'Windows': 'windows',
}

# Map from platform name to (toolchain, generator)
PLATFORMS = {
    'linux': ('posix', 'ninja'),
    'osx': ('darwin', 'xcode'),
    'windows': ('msvc', 'msvc'),
}

# Set of all generators
GENERATORS = {'gnumake', 'xcode', 'msvc', 'ninja'}

# Set of all toolchains
TOOLCHAINS = {'posix', 'darwin'}

# Set of POSIX-like platforms
POSIX_PLATFORMS = {'linux', 'osx'}

class Configuration(object):
    """The build configuration.

    Properties:
    variables -- Configuration variable dictionary.
    srcdir -- Root source directory.
    builddir -- Root build directory.
    buildsrc -- Built sources directory, relative to builddir.
    buildinclude -- Built headers directory, relative to builddir.
    tempdir -- Path to a temporary directory for configuration files.
    toolchain -- The toolchain name.
    generator -- The generator name.
    host -- The host platform name.
    target -- The target platform name.
    tags -- Tags for this platform.
    cache -- The build cache.
    """
    __slots__ = [
        'variables',
        'srcdir',
        'builddir',
        'buildsrc',
        'buildinclude',
        'tempdir',
        'toolchain',
        'generator',
        'host',
        'target',
        'tags',
        'cache',
    ]

    def __init__(self, variables, srcdir, builddir, tempdir):
        self.variables = dict(variables)
        self.srcdir = srcdir
        self.builddir = builddir
        self.buildsrc = Path('build', 'src')
        self.buildinclude = Path('build', 'include')
        self.tempdir = tempdir
        logfp = logfile(2)
        if not variables:
            logfp.write('Arguments: none\n')
        else:
            logfp.write('Arguments:\n')
            for name, value in sorted(variables.items()):
                logfp.write('  {}={}\n'.format(name, shlex.quote(value)))
        system = platform.system()
        host = SYSTEM_PLATFORMS.get(system)
        target = self.variables.get('platform', host)
        if host is None:
            host = 'unknown-' + system
        print('Host:', host, file=logfp)
        if target is None:
            raise ConfigError('Unsupported system: {!r}'.format(system))
        try:
            toolchain, generator = PLATFORMS[target]
        except KeyError:
            raise ConfigError('Unsupported platform: {!r}'.format(target))
        print('Target:', target, file=logfp)
        generator = self.variables.get('generator', generator)
        if generator not in GENERATORS:
            raise ConfigError('Unsupported generator: {!r}'.format(generator))
        print('Toolchain:', toolchain, file=logfp)
        print('Generator:', generator, file=logfp)
        self.toolchain = toolchain
        self.generator = generator
        self.host = host
        self.target = target
        self.tags = {target}
        if target in POSIX_PLATFORMS:
            self.tags.add('posix')
