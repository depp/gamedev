# Copyright 2015-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from pathlib import Path
import io
import os

class AtomicFile(object):
    """Context object for atomically writing to a file."""
    __slots__ = ['_tmppath', '_destpath', '_fp', '_executable']

    def __init__(self, path, mode, *, executable=False):
        if 'w' not in mode:
            raise ValueError('Mode must be writable')
        self._tmppath = path.with_name('.{}.tmp'.format(path.name))
        self._destpath = path
        if executable and os.name == 'posix':
            def opener(path, flags):
                return os.open(path, flags, 0o777)
        else:
            opener = None
        self._fp = open(str(self._tmppath), mode, opener=opener)
        self._executable = executable

    def __enter__(self):
        return self._fp

    def __exit__(self, type, value, traceback):
        self._fp.close()
        if type is None:
            self._tmppath.replace(self._destpath)
        else:
            self._tmppath.unlink()

def update_text(path, text, *, executable=False):
    """Write text to a file only if this would change the file.

    Arguments:
    path -- Path to the file.
    text -- New contents of the file.
    executable -- Whether to make the file executable.
    """
    update_bytes(path, text.encode('UTF-8'), executable=executable)

def update_bytes(path, data, *, executable=False):
    """Write data to a file only if this would change the file.

    Arguments:
    path -- Path to the file.
    data -- New contents of the file.
    executable -- Whether to make the file executable.
    """
    try:
        fp = open(str(path), 'rb')
    except FileNotFoundError:
        pass
    else:
        with fp:
            cur_data = fp.read()
        if cur_data == data:
            return
    path.parent.mkdir(parents=True, exist_ok=True)
    with AtomicFile(path, 'wb', executable=executable) as fp:
        fp.write(data)
