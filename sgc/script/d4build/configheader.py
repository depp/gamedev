# Copyright 2013-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import io
from .generatedfiles import TextFile, NOTICE
from .environment import EnvOptions, EnvBoolean

BAR = '=' * 65

HEAD = '''\
/* {notice} */
#pragma once
'''

GROUP = '''\
/*
 * {bar}
 * {doc}
 * {bar}
 */
'''

def config_header(build, cfg):
    """Create the config.h generator."""
    fp = io.StringIO()
    fp.write(HEAD.format(notice=NOTICE))
    def writevar(name, value):
        s = '#define ENABLE_{} 1'.format(name)
        if value:
            fp.write('{}\n'.format(s))
        else:
            fp.write('/* {} */\n'.format(s))
    for key, var in sorted(cfg._variables.items()):
        value = getattr(cfg, key)
        fp.write('\n')
        fp.write(GROUP.format(bar=BAR, doc=var.doc))
        if isinstance(var, EnvBoolean):
            fp.write('\n')
            writevar(key.upper(), value)
        elif isinstance(var, EnvOptions):
            fp.write('\n')
            if var.optional:
                writevar(key.upper(), value != 'none')
            for name, description in var.options:
                if name == 'none':
                    continue
                fp.write('\n/* {} */\n'.format(description))
                writevar(
                    '{}_{}'.format(key.upper(), name.upper()),
                    value == name)
        else:
            assert False
    return TextFile(
        build.config.buildinclude.joinpath('config.h'),
        fp.getvalue())
