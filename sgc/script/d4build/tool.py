# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import argparse
import re
import sys
from .error import ConfigError

VARIABLE_NAME = re.compile('[_A-Za-z][_A-Za-z0-9]*')

def parse_variable(var):
    i = var.index('=')
    name = var[:i]
    value = var[i+1:]
    if not VARIABLE_NAME.fullmatch(name):
        raise ValueError('invalid variable name')
    return name.lower(), value

def run(factory, args):
    """Run the top-level configuration script.

    Arguments:
    factory -- An object with a load() method for loading the build script.
    args -- Command-line configuration arguments.
    """
    p = argparse.ArgumentParser()
    p.set_defaults(
        action=action_configure,
        verbosity=1,
    )
    p.add_argument(
        'variables', nargs='*', metavar='VAR=VALUE', type=parse_variable,
        help='build variables')
    p.add_argument(
        '-q', dest='verbosity', action='store_const', const=0,
        help='suppress messages')
    p.add_argument(
        '-v', dest='verbosity', action='count',
        help='verbose messages')
    p.set_defaults(action=action_configure)
    p.add_argument(
        '--scan',
        action='store_const', const=action_scan, dest='action',
        help='scan for extra or missing source files')

    args = p.parse_args(args)
    try:
        args.action(factory, args)
    except ConfigError as ex:
        if args.verbosity >= 3:
            import traceback
            exc_type, exc_value, exc_tb = sys.exc_info()
            traceback.print_tb(exc_tb)
        ex.write(sys.stderr)
        raise SystemExit(1)

def action_configure(factory, args):
    from .build import configure
    configure(
        factory=factory,
        variables=dict(args.variables),
        verbosity=args.verbosity,
    )

def action_scan(factory, args):
    from .source import EXT_SRCTYPE
    dirs = set()
    listed = set()
    actual = set()
    for src in factory.load().sources():
        listed.add(src)
        dirname = src.parent
        if dirname not in dirs:
            dirs.add(dirname)
            actual.update(
                path for path in dirname.iterdir()
                if not path.name.startswith('.')
                and path.suffix in EXT_SRCTYPE)
    mismatch = listed.symmetric_difference(actual)
    if not mismatch:
        print('No discrepancies found.')
        return
    for path in sorted(mismatch):
        if path in listed:
            status = '-'
        else:
            status = '+'
        print(status, path)
    raise SystemExit(1)
