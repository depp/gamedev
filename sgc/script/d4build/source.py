# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import collections
from .error import ConfigError

_SRCTYPE_EXTS = {
    'c': 'c',
    'c++': 'cpp cp cxx',
    'h': 'h',
    'h++': 'hpp hxx',
    'objc': 'm',
    'objc++': 'mm',
    'rc': 'rc',
    'xib': 'xib',
}

EXT_SRCTYPE = {
    '.' + ext: type
    for type, exts in _SRCTYPE_EXTS.items()
    for ext in exts.split()
}

SRCTYPE_EXT = {
    type: '.' + exts.split()[0]
    for type, exts in _SRCTYPE_EXTS.items()
}

del _SRCTYPE_EXTS

# Set of source types which are compiled as code.
COMPILED_TYPES = frozenset({'c', 'c++', 'objc', 'objc++'})
# Set of header file source types.
HEADER_TYPES = frozenset({'h', 'h++'})

class SourceSet(object):
    """A collection of source files.

    This can be combined with a TagSet to create a Module.

    Attributes:
    public_ipaths -- List of public header search paths.
    private_ipaths -- List of private header search paths.
    sources -- List of TagSource instances.
    """
    __slots__ = [
        'public_ipaths',
        'private_ipaths',
        'sources',
    ]

    def __init__(self, base=None):
        self.public_ipaths = []
        self.private_ipaths = []
        self.sources = []
        if base is not None:
            self.public_ipaths.extend(base.public_ipaths)
            self.private_ipaths.extend(base.private_ipaths)
            self.sources.extend(base.sources)

TagSource = collections.namedtuple(
    'Source', 'path tags')
EnvSource = collections.namedtuple(
    'CommonEnvSource', 'path type env')

def filter_sources(sources, *groups):
    """Filter sources into groups by types.

    Each group is a set of types.  Returns a tuple with a list of sources for
    each group of types.
    """
    maps = {}
    results = []
    for group in groups:
        result = []
        results.append(result)
        for type in group:
            try:
                maps[type].append(result)
            except KeyError:
                maps[type] = [result]
    for source in sources:
        try:
            rlist = maps[source.type]
        except KeyError:
            raise ConfigError(
                'Cannot process source file: {!r}'.format(source.path.name))
        for result in rlist:
            result.append(source)
    return tuple(results)
