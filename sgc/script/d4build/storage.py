# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from pathlib import Path
from .file import AtomicFile
from .pickledict import PickleDict, PickleDictLoadError

DATA_VERSION = 1
DATA_PATH = Path('config.dat')

def load_storage():
    """Load the configuration storage database."""
    storage = PickleDict(version=DATA_VERSION)
    try:
        with open(str(DATA_PATH), 'rb') as fp:
            storage.load(fp)
    except (PickleDictLoadError, FileNotFoundError):
        pass
    return storage

def save_storage(storage):
    """Save the configuration storage database."""
    with AtomicFile(DATA_PATH, 'wb') as fp:
        storage.dump(fp)
