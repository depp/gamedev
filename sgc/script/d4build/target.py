# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.

class Target(object):
    """A top-level target to be built.

    Attributes:
    name -- The target filename, not including extension.
    """
    __slots__ = ['name']

    def __init__(self, name):
        self.name = name

class ModuleTarget(Target):
    """A target which is created from source modules.

    Attributes:
    modules -- A list of modules compiled into the target.
    """
    __slots__ = ['modules']

    def __init__(self, name):
        super(ModuleTarget, self).__init__(name)
        self.modules = []

class Executable(ModuleTarget):
    """An executable to be built.

    Attributes:
    default_arguments -- A list of default arguments to pass to the executable
        at runtime.
    """
    __slots__ = ['default_arguments']

    def __init__(self, name):
        super(Executable, self).__init__(name)
        self.default_arguments = []

class ApplicationBundle(Executable):
    """An application bundle to be built, for Darwin platforms.

    Attributes:
    info_plist -- Path to the info plist file.
    """
    __slots__ = [
        'info_plist',
    ]

    def __init__(self, name, info_plist):
        super(ApplicationBundle, self).__init__(name)
        self.info_plist = info_plist
