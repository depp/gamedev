# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import io
from pathlib import Path
import shlex
import sys
from .generatedfiles import NOTICE, TextFile
from .log import logfile
from .module import Module
from .ninja_syntax import Writer, escape_path
from .runscript import run_script
from .source import (
    EXT_SRCTYPE, COMPILED_TYPES, HEADER_TYPES,
    filter_sources,
)
from .target import ModuleTarget, Executable

def quote(flags):
    return ' '.join(shlex.quote(flag) for flag in flags)

class NinjaWriter(object):
    """Writer for Ninja build files.

    Attributes:
    builddir -- The build directory.
    fp -- StringIO containing the Ninja file contents.
    w -- The ninja syntax Writer.
    """
    __slots__ = [
        'build',
        'builddir',
        'fp',
        'w',
        'targets',
    ]

    def __init__(self, build):
        self.build = build
        self.builddir = build.config.builddir
        self.fp = io.StringIO()
        self.w = Writer(self.fp)
        self.w.comment(NOTICE)
        self.targets = []

    def cflags(self, env):
        flags = []
        flags.extend('-iquote' + str(ipath.relative_to(self.builddir))
                     for ipath in env.ipaths)
        flags.extend(env.cppflags)
        flags.extend(env.cflags)
        flags.extend(env.cwarn)
        return flags

    def section(self, name):
        self.w.newline()
        self.w.comment(name)

    def emit_vars(self):
        """Emit build variables."""
        w = self.w
        self.section('Variables')
        base = self.build.toolchain.base
        env = base.public_env
        w.variable('cc', quote([env.cc]))
        w.variable('cflags', quote(self.cflags(env)))
        env = base.link_env
        w.variable('ldflags', quote(env.ldflags))
        w.variable('libs', quote(env.libs))
        py_path = Path(__file__).parent.parent
        py_exe = sys.executable
        w.variable(
            'regen',
            'PYTHONPATH={}:$$PYTHONPATH {} -m d4build.regen'.format(
                shlex.quote(str(py_path.relative_to(self.builddir))),
                shlex.quote(py_exe),
            ))

    def emit_rules(self):
        """Emit build rules."""
        w = self.w
        self.section('Rules')
        w.rule('regen', '$regen regen $out', restat=True)
        w.rule('reconfig', '$regen reconfigure', generator=True)
        w.rule(
            'cc',
            '$cc -o $out -c $in -MF $out.d -MMD -MP $cflags',
            depfile='$out.d')
        w.rule(
            'link',
            '$cc $ldflags -o $out $in $libs')

    def emit_reconfig(self):
        self.section('Reconfigure')
        self.w.build(
            ['build.ninja'],
            'reconfig',
            [str(path.relative_to(self.builddir))
             for path in self.build.inputs])

    def emit_sources(self):
        self.section('Sources')
        self.w.build(
            ['sources'],
            'phony',
            [str(path.relative_to(self.builddir))
             for files in self.build.files
             for path in files.outputs(self.builddir)])

    def compile(self, sources):
        """Compile all the soucres in a list.

        Returns a list of the output object file paths.

        Arguments:
        sources -- A sequence of EnvSource instances.
        """
        sources_c, sources_h = filter_sources(
            sources, COMPILED_TYPES, HEADER_TYPES)
        objects = []
        objdir = self.builddir.joinpath('build', 'obj')
        srcdir = self.build.config.srcdir
        for source in sources_c:
            env = source.env
            assert env is not None
            srcrel = source.path.relative_to(srcdir)
            objpath = objdir.joinpath(srcrel).with_suffix('.o')
            deppath = objpath.with_name(objpath.name + '.o')
            variables = []
            cflags = self.cflags(env)
            if env.cc is not None:
                variables.append(('cc', quote([env.cc])))
            if cflags:
                variables.append(('cflags', '$cflags ' + quote(cflags)))
            self.w.build(
                [str(objpath.relative_to(self.builddir))],
                'cc',
                [str(srcrel)],
                order_only=['sources'],
                variables=variables)
            objects.append(objpath)
        return objects

    def link(self, name, objects, env):
        """Link object files into an executable.

        Returns the executable path.

        Arguments:
        name -- Name of the executable to link.
        objects -- Sequence of inputs, which are paths to object files.
        env -- Linking environment variables.
        """
        variables = []
        exepath = self.builddir.joinpath('build', 'products', name)
        if env.cc is not None:
            variables.append(('cc', quote([env.cc])))
        if env.ldflags:
            variables.append(('ldflags', '$ldflags ' + quote(env.ldflags)))
        if env.libs:
            variables.append(('libs', '$libs ' + quote(env.libs)))
        self.w.build(
            [str(exepath.relative_to(self.builddir))],
            'link',
            [str(path.relative_to(self.builddir)) for path in objects],
            variables=variables)
        return exepath

    def emit_targets(self):
        self.section('Targets')
        toolchain = self.build.toolchain
        for target in self.build.targets:
            result = None
            ttype = type(target)
            if issubclass(ttype, ModuleTarget):
                objects = self.compile(
                    Module.flat_sources(toolchain, [], target.modules))
                if issubclass(ttype, Executable):
                    result = self.link(
                        target.name,
                        objects,
                        Module.flat_link_env(toolchain, target.modules),
                    )
                    if ttype == Executable:
                        scriptpath = self.builddir.joinpath(result.name)
                        self.build.files.append(run_script(
                            Path(result.name),
                            result.relative_to(self.builddir),
                            'make',
                            target.default_arguments))
                        result = scriptpath
                    else:
                        result = None
            if result is None:
                raise ConfigError(
                    'Unsupported target type: {}.{}'
                    .format(ttype.__module__, ttype.__qualname__))
            self.targets.append(result)

    def emit_files(self):
        self.section('Generated files')
        for files in self.build.files:
            self.w.build(
                [str(path.relative_to(self.builddir))
                 for path in files.outputs(self.builddir)],
                'regen',
                [str(path.relative_to(self.builddir))
                 for path in files.inputs(self.builddir)])

    def emit_footer(self):
        self.section('All targets')
        self.w.build(
            ['all'],
            'phony',
            [str(path.relative_to(self.builddir))
             for path in self.targets])
        self.w.default(['all'])

class NinjaGenerator(object):
    """Abstract base class for build script generators."""
    __slots__ = ['builddir']

    def __init__(self, config):
        self.builddir = config.builddir

    def generate(self, build):
        """Generate the build script.

        Arguments:
        build -- The Build instance.
        """
        w = NinjaWriter(build)
        w.emit_vars()
        w.emit_rules()
        w.emit_reconfig()
        w.emit_sources()
        w.emit_targets()
        w.emit_files()
        w.emit_footer()
        build.files.append(TextFile(
            Path('build.ninja'),
            w.fp.getvalue(),
            do_regenerate=False))

    def runpath(self, path):
        """Convert a path to a string path that gives the path at runtime."""
        return path.relative_to(self.builddir)

GENERATOR = NinjaGenerator
