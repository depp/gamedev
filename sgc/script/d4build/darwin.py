# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from .environment import EnvUniqueList
from .module import Module
from .posix import PosixCompileEnvironment, PosixLinkEnvironment, PosixToolchain

class DarwinCompileEnvironment(PosixCompileEnvironment):
    """Environment for compiling on Darwin."""
    frameworks = EnvUniqueList('Set of frameworks to use', private=True)

    def cc_command(self, *args, **kw):
        cmd = super(DarwinCompileEnvironment, self).cc_command(*args, **kw)
        for framework in self.frameworks:
            cmd.extend(['-framework', framework])
        return cmd

class DarwinLinkEnvironment(PosixLinkEnvironment):
    """Environment for linking on Darwin."""
    frameworks = EnvUniqueList('Set of frameworks to use', private=True)

    def cc_command(self, *args, **kw):
        cmd = super(DarwinCompileEnvironment, self).cc_command(*args, **kw)
        for framework in self.frameworks:
            cmd.extend(['-framework', framework])
        return cmd

class DarwinToolchain(PosixToolchain):
    """Toolchain for Darwin build environments."""
    __slots__ = []

    def compile_env(self):
        """Create an empty compilation environment."""
        return DarwinCompileEnvironment()

    def link_env(self):
        """Create an empty linking environment."""
        return DarwinLinkEnvironment()

    def frameworks(self, frameworks):
        """Create a module which links with the given frameworks.

        Arguments:
        frameworks -- List of framework names.
        """
        mod = Module(self)
        frameworks = list(frameworks)
        mod.link_env.update(frameworks=frameworks)
        mod.link_env.update(frameworks=frameworks)
        return mod

TOOLCHAIN = DarwinToolchain
