# Copyright 2013-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
"""Script for running executables with default arguments.

This just makes it easier to run programs from the command line.
"""
from .generatedfiles import TextFile, NOTICE
import shlex

RUN_SCRIPT_TEMPLATE = '''\
#!/bin/sh
# {notice}
exe={exe}
name={name}
if test ! -x "$exe" ; then
  cat 1>&2 <<EOF
error: $exe does not exist
Did you remember to run {buildcmd}?
EOF
  exit 1
fi
case "$1" in
  --gdb)
    shift
    exec gdb --args "$exe" {args} "$@"
    ;;
  --valgrind)
    shift
    exec valgrind -- "$exe" {args} "$@"
    ;;
  --help)
    cat <<EOF
Usage: $name [--help | --gdb | --valgrind | --] [options...]
EOF
    exit 0
    ;;
  --)
    shift
    exec "$exe" {args} "$@"
    ;;
  *)
    exec "$exe" {args} "$@"
    ;;
esac
'''

def run_script(script, exe, buildcmd, arguments):
    """Create the generator for a run script."""
    return TextFile(script, RUN_SCRIPT_TEMPLATE.format(
        notice=NOTICE,
        exe=shlex.quote(str(exe)),
        name=shlex.quote(script.name),
        buildcmd=buildcmd,
        args=' '.join(shlex.quote(arg) for arg in arguments),
    ), dependencies=[exe], executable=True)
