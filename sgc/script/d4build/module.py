# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import collections
from .error import ConfigError
from .configuration import PLATFORMS
from .source import EXT_SRCTYPE, COMPILED_TYPES, EnvSource

DEP_TYPES = frozenset({
    'public',
    'private',
    'link',
})

class Module(object):
    """A module in a larger build target.

    Modules can contain source files and build settings, and they can
    depend on other modules.

    Attributes:
    public_env -- Enviroment for compiling with this module.
    private_env -- Environment for compiling this module's sources.
    link_env -- Environment for linking with this module.
    errors -- List of errors in this module.
    """
    __slots__ = [
        '_visit_state',
        'public_env',
        'private_env',
        'link_env',
        'errors',
    ]

    def __init__(self, toolchain):
        self._visit_state = 0
        self.public_env = toolchain.compile_env()
        self.private_env = None
        self.link_env = toolchain.link_env()
        self.errors = []

    def deps(self, deptype):
        """Iterate over direct module dependencies.

        Arguments:
        deptype -- 'public', 'private', or 'link'.
        """
        if deptype not in DEP_TYPES:
            raise ValueError('Invalid dependency type.')
        return iter(())

    def sources(self, toolchain, base_modules):
        """Iterate over the sources in this module.

        Yields EnvSource objects.

        Arguments:
        toolchain -- The toolchain.
        base_modules -- List of base module dependencies.
        """
        return iter(())

    @classmethod
    def flat_deps(class_, modules, deptype):
        """Flatten a list of modules and their dependencies.

        Arguments:
        modules -- Sequence of modules.
        deptype -- 'public', 'private', or 'link'.
        """
        stack = [(0, module) for module in modules]
        result = []
        reset_list = []
        try:
            while stack:
                action, module = stack.pop()
                if module is None:
                    continue
                if action:
                    module._visit_state = 1
                    result.append(module)
                if module._visit_state:
                    if module._visit_state != 1:
                        raise ValueError('Circular module dependency.')
                    continue
                reset_list.append(module)
                stack.append((1, module))
                module._visit_state = 2
                stack.extend((0, module) for module in module.deps(deptype))
        finally:
            for module in reset_list:
                module._visit_state = 0
        result.reverse()
        return result

    @classmethod
    def flat_sources(class_, toolchain, base_modules, modules):
        """Iterate over all sources in modules and their dependencies.

        Yields EnvSource objects.

        Arguments:
        toolchain -- The toolchain, a Toolchain instance.
        base_modules -- List of modules, implicit base dependencies.
        modules -- Sequence of modules.
        """
        for module in class_.flat_deps(modules, 'link'):
            yield from module.sources(toolchain, base_modules)

    @classmethod
    def flat_compile_env(class_, toolchain, modules):
        """Get the compilation environment modules and their dependencies.

        Note that each source will also have its own complete compilation
        environment, and the full flat environment is only necessary if the
        generator does not support per-source compilation environments.

        Arguments:
        toolchain -- The toolchain, a Toolchain instance.
        modules -- Sequence of modules.
        """
        envs = []
        for mod in class_.flat_deps(modules, 'private'):
            envs.append(mod.private_env)
            envs.append(mod.public_env)
        env = toolchain.compile_env()
        env.update(*envs)
        return env

    @classmethod
    def flat_link_env(class_, toolchain, modules):
        """Get the linking environment for this module and dependencies.

        Arguments:
        toolchain -- The toolchain, a Toolchain instance.
        modules -- Sequence of modules.
        """
        envs = []
        for mod in class_.flat_deps(modules, 'link'):
            envs.append(mod.link_env)
        env = toolchain.link_env()
        env.update(*envs)
        return env

    def update_env(self, env):
        """Update environment variables from strings in a mapping."""
        self.public_env.update_env(env)
        self.link_env.update_env(env)

    def update_map(self, mapping):
        """Update environment variables from values in a mapping."""
        self.public_env.update_map(mapping)
        self.link_env.update_map(mapping)

    def update(self, **kw):
        """Update environment variables."""
        extra = set(kw.keys())
        extra.difference_update(self.public_env.keys())
        extra.difference_update(self.link_env.keys())
        if extra:
            raise ValueError('Unknown environment keys: {}'.format(
                ', '.join(repr(x) for x in extra)))
        self.update_map(kw)

STANDARD_TAGS = frozenset(
    set(PLATFORMS.keys()) | {'posix', 'exclude'}
)

class ModSource(object):
    """A source in a SourceModule."""
    __slots__ = ['path', 'deps', 'enabled']
    def __init__(self, path):
        self.path = path
        self.deps = []

class ModTag(object):
    """A tag in a SourceModule."""
    __slots__ = ['sources', 'enabled']
    def __init__(self):
        self.sources = []
        self.enabled = False

class SourceModule(Module):
    """A module in a larger build target containing source files to compile."""
    __slots__ = [
        '_sources',
        '_tags',
        '_public_deps',
        '_private_deps',
        '_tag_deps',
        '_link_deps',
    ]

    def __init__(self, toolchain, sourceset, tags):
        super(SourceModule, self).__init__(toolchain)
        self.private_env = toolchain.compile_env()
        self._sources = []
        self._tags = {tag: ModTag() for tag in tags}
        for tag in STANDARD_TAGS:
            self._tags[tag] = ModTag()
        undefined = set()
        for source in sourceset.sources:
            obj = ModSource(source.path)
            self._sources.append(obj)
            for tag in source.tags:
                try:
                    tagobj = self._tags[tag]
                except KeyError:
                    undefined.add(tag)
                else:
                    tagobj.sources.append(obj)
        unused = {tag for tag, value in self._tags.items()
                  if not value.sources}
        undefined.difference_update(STANDARD_TAGS)
        unused.difference_update(STANDARD_TAGS)
        errors = [
            ('Unused source tags: {}', unused),
            ('Undefined source tags: {}', undefined),
        ]
        errors = [
            msg.format(', '.join(repr(x) for x in sorted(tags)))
            for msg, tags in errors if tags
        ]
        if errors:
            raise ConfigError('; '.join(errors))
        self._public_deps = []
        self._private_deps = []
        self._tag_deps = []
        self._link_deps = []
        self.public_env.update(
            ipaths=sourceset.public_ipaths,
        )
        self.private_env.update(
            ipaths=sourceset.private_ipaths,
        )
        for tag in toolchain.config.tags:
            self._tags[tag].enabled = True

    def deps(self, deptype):
        """Iterate over direct module dependencies.

        Arguments:
        deptype -- 'public', 'private', or 'link'.
        """
        if deptype == 'public':
            yield from self._public_deps
        elif deptype == 'private':
            yield from self._private_deps
            yield from self._tag_deps
        elif deptype == 'link':
            yield from self._link_deps
        else:
            raise ValueError('Invalid dependency type.')

    def sources(self, toolchain, base_modules):
        """Iterate over the sources in this module.

        Yields EnvSource objects.

        Arguments:
        toolchain -- The toolchain.
        base_modules -- List of base module dependencies.
        """
        for source in self._sources:
            source.enabled = True
        for tag in self._tags.values():
            if not tag.enabled:
                for source in tag.sources:
                    source.enabled = False
        for source in self._sources:
            if not source.enabled:
                continue
            try:
                type = EXT_SRCTYPE[source.path.suffix]
            except KeyError:
                raise ConfigError(
                    'Unknown file type: {!r}'.format(source.path.name))
            if type in COMPILED_TYPES:
                envs = []
                deps = []
                deps.extend(base_modules)
                deps.append(self)
                deps.extend(self._private_deps)
                deps.extend(source.deps)
                for mod in self.flat_deps(deps, 'public'):
                    if mod is self:
                        envs.append(mod.private_env)
                    envs.append(mod.public_env)
                env = toolchain.compile_env()
                env.update(*envs)
            else:
                env = None
            yield EnvSource(source.path, type, env)

    def add(self, tag, *deps):
        """Enable a tag and add dependencies to it.

        If the tag is 'public', then the module will be used to compile and
        link all files in this module and in modules which depend on it.

        If the tag is 'private', then the module will be used to compile and
        link all files in this module.

        If the tag is 'link', then the module will be used to link this module.
        """
        for dep in deps:
            if not isinstance(dep, Module) and dep is not None:
                raise TypeError('Dependency is not a module.')
        if tag in DEP_TYPES:
            if tag == 'public':
                self._public_deps.extend(deps)
                self._private_deps.extend(deps)
            elif tag == 'private':
                self._private_deps.extend(deps)
        else:
            try:
                tag = self._tags[tag]
            except KeyError:
                raise ConfigError('No such tag: {!r}'.format(tag))
            tag.enabled = True
            for src in tag.sources:
                src.deps.extend(deps)
            self._tag_deps.extend(deps)
        self._link_deps.extend(deps)
