# Copyright 2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import inspect
import shlex
import sys
from .error import ConfigError

class Variable(object):
    __slots__ = ['doc', 'private']
    def __init__(self, doc, *, private=False):
        self.doc = doc
        self.private = private
    def combine(self, x, y):
        if y is None:
            return x
        return y

class EnvString(Variable):
    """Environment variable which holds a string."""
    __slots__ = []
    def initial(self):
        return None
    def read(self, x):
        return x
    def format(self, x):
        if x is None:
            return ''
        return shlex.quote(x)

class EnvFlags(Variable):
    """Environment variable for a list of program flags."""
    __slots__ = []
    def initial(self):
        return []
    def read(self, x):
        return x.split()
    def combine(self, x, y):
        x.extend(y)
        return x
    def format(self, x):
        return ' '.join(shlex.quote(item) for item in x)

class EnvPaths(Variable):
    """Environment variable for a list of paths."""
    __slots__ = []
    def initial(self):
        return []
    def combine(self, x, y):
        for item in y:
            if item not in x:
                x.append(item)
        return x
    def format(self, x):
        return ' '.join(shlex.quote(str(item)) for item in x)

class EnvUniqueList(Variable):
    """Environment variable for a list of unique items.."""
    __slots__ = []
    def initial(self):
        return []
    def combine(self, x, y):
        for item in y:
            if item not in x:
                x.append(item)
        return x
    def format(self, x):
        return ' '.join(shlex.quote(item) for item in x)

BOOL_FALSE = ('0', 'no', 'off', 'false')
BOOL_TRUE = ('1', 'yes', 'on', 'true')

class EnvOptions(EnvString):
    """Environment variable which holds one of several values."""
    __slots__ = ['options', 'optional']
    def __init__(self, doc, options, *, private=False):
        super(EnvOptions, self).__init__(doc, private=private)
        self.options = options
        self.optional = any(
            key == 'none' for key, value in options)
    def initial(self):
        return None
    def read(self, x):
        xx = x.lower()
        for key, value in self.options:
            if xx == key:
                return xx
        if self.optional:
            if xx in BOOL_FALSE:
                return 'none'
        raise ConfigError('Unknown option: {!r}'.format(x))

class EnvBoolean(Variable):
    """Environment variable which holds a boolean."""
    __slots__ = []
    def initial(self):
        return None
    def read(self, x):
        xx = x.lower()
        if xx in BOOL_FALSE:
            return False
        if xx in BOOL_TRUE:
            return True
        raise ConfigError('Invalid boolean: {!r}'.format(x))
    def format(self, x):
        if x is None:
            return ''
        if x:
            return 'yes'
        return 'no'

class EnvironmentType(type):
    """Metaclass for environment types.

    An environment type should have a collection of variables in it.
    """
    def __new__(class_, name, bases, namespace):
        flat_variables = {}
        class_variables = {}
        ns = {}
        slots = []

        for key, value in namespace.items():
            if isinstance(value, Variable):
                class_variables[key] = value
                slots.append(key)
            else:
                ns[key] = value

        ns['_class_variables'] = class_variables
        ns['_variables'] = flat_variables
        env_class = type.__new__(class_, name, bases, ns)
        for base in inspect.getmro(env_class):
            if base is object:
                continue
            for key, value in base._class_variables.items():
                if key in flat_variables:
                    raise ValueError('Duplicate variable: {!r}'.format(key))
                flat_variables[key] = value
        return env_class

class Environment(object, metaclass=EnvironmentType):
    """Base class for environments.

    An environment is a collection of variables and their values.
    """

    def __init__(self, **kw):
        super(Environment, self).__init__()
        for key, var in self._variables.items():
            try:
                val = kw[key]
            except KeyError:
                val = var.initial()
            setattr(self, key, val)

    @classmethod
    def env_keys(class_):
        """Iterate over the public keys in this class."""
        for key, var in class_._variables.items():
            if not var.private:
                yield key

    @classmethod
    def keys(class_):
        """Iterate over all keys in this class."""
        return iter(class_._variables.keys())

    def update_env(self, env):
        """Update the environment by merging strings in a dictionary."""
        for key, var in self._variables.items():
            if var.private:
                continue
            try:
                text = env[key]
            except KeyError:
                continue
            nvalue = var.read(text)
            value = getattr(self, key)
            nvalue = var.combine(value, nvalue)
            setattr(self, key, nvalue)

    def update_map(self, mapping):
        """Update the environment by merging values in a dictionary."""
        for key, var in self._variables.items():
            try:
                nvalue = mapping[key]
            except KeyError:
                continue
            value = getattr(self, key)
            nvalue = var.combine(value, nvalue)
            setattr(self, key, nvalue)

    def update(self, *args, **kw):
        """Update the environment by merging new values into it."""
        extra = set(kw.keys())
        for key, var in self._variables.items():
            value = getattr(self, key)
            for arg in args:
                if arg is None:
                    continue
                nvalue = getattr(arg, key)
                value = var.combine(value, nvalue)
            try:
                nvalue = kw[key]
            except KeyError:
                pass
            else:
                extra.discard(key)
                value = var.combine(value, nvalue)
            setattr(self, key, value)
        if extra:
            raise TypeError(
                'Unexpected keyword arguments: {}'
                .format(', '.join(repr(x) for x in extra)))

    def dump(self, file=None, indent='  '):
        """Dump the environment variables to a file."""
        if file is None:
            file = sys.stdout
        for key, var in sorted(self._variables.items()):
            value = getattr(self, key)
            file.write('{}{}: {}\n'.format(indent, key, var.format(value)))
