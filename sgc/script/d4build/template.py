# Copyright 2013-2014 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import re
from .generatedfiles import GeneratedFiles
from .error import ConfigError
from .file import update_text

class TemplateFile(GeneratedFiles):
    __slots__ = ['target', 'source', 'tvars']

    def __init__(self, target, source, tvars):
        self.target = target
        self.source = source
        self.tvars = tvars

    def inputs(self, builddir):
        yield builddir / self.source

    def outputs(self, builddir):
        yield builddir / self.target

    def generate(self, builddir, progress):
        """Write the generated source to the given file."""
        path = builddir / self.source
        with open(str(path)) as infp:
            srcdata = infp.read()
        output = re.sub(r'\$\{(\w+)\}', self._expand, srcdata)
        path = builddir / self.target
        progress(path)
        update_text(path, output)

    def _expand(self, m):
        """Expand a single variable."""
        k = m.group(1)
        try:
            v = self.tvars[k]
        except KeyError:
            raise ConfigError('unknown variable in template: {}'
                              .format(m.group(0)))
        return v
