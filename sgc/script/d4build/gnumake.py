# Copyright 2014-2016 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
import io
from pathlib import Path
import re
import sys
from .error import ConfigError
from .file import AtomicFile
from .generator import Generator
from .generatedfiles import NOTICE, TextFile
from .module import Module
from .runscript import run_script
from .shell import escape
from .source import (
    EXT_SRCTYPE, COMPILED_TYPES, HEADER_TYPES,
    filter_sources,
)
from .target import ModuleTarget, Executable

# What to print out when compiling various source types
COMPILE_TYPES = {
    'c': 'C',
    'c++': 'C++',
    'objc': 'ObjC',
    'objc++': 'ObjC++',
}

# Escape special characters in makefiles
MK_SPECIAL = re.compile('[^-_.+/A-Za-z0-9]')
def mk_escape1(x):
    c = x.group(0)
    if c == ' ':
        return '\\ '
    raise ValueError('invalid character: {!r}'.format(c))
def mk_escape(x):
    if not isinstance(x, str):
        raise TypeError('expected string, got {!r}'.format(x))
    try:
        return MK_SPECIAL.sub(mk_escape1, x)
    except ValueError:
        raise ValueError('invalid character in {!r}'.format(x))

MAKEFILE_TEMPLATE = '''\
# {notice}
all: {default_targets}
.PHONY: {phony_targets}
script := PYTHONPATH={python_path}:$$PYTHONPATH {python_exe} -m d4build.regen
{mktest_begin}\
Makefile: {inputs}
\t$(script) reconfigure
{mktest_end}\
config:
\t$(script) reconfigure-full
clean:
\trm -rf {clean_targets}
distclean:
\trm -rf {distclean_targets}
sources: {built_sources}
FORCE:

ifndef V
{quiet}\
endif

{includes}

{rules}\
'''

class MakefileWriter(object):
    """Writer for GNU Make makefiles.

    Attributes:
    build -- The Build instance.
    builddir -- The build directory.
    default_targets -- Set of default targets.
    clean_targets -- Set of targets to clean.
    phony_targets -- Set of phony targets.
    includes -- List of makefiles to include, if they exist.
    rulefp -- StringIO for writing the Makefile rules.
    qnames -- Map from quiet rule names to variable names.
    """
    __slots__ = [
        'build',
        'builddir',
        'default_targets',
        'clean_targets',
        'phony_targets',
        'built_sources',
        'includes',
        'rulefp',
        'qnames',
    ]

    def __init__(self, build):
        self.build = build
        self.builddir = build.config.builddir
        self.default_targets = set()
        self.clean_targets = {self.builddir.joinpath('build')}
        self.phony_targets = {'all', 'clean', 'FORCE', 'config', 'sources'}
        self.built_sources = set()
        self.includes = []
        self.rulefp = io.StringIO()
        self.qnames = {}

    def get_text(self):
        """Get the Makefile contents."""
        def targets(target_list):
            items = []
            for target in target_list:
                if isinstance(target, Path):
                    target = str(target.relative_to(self.builddir))
                elif not isinstance(target, str):
                    raise TypeError('Bad target type.')
                items.append(mk_escape(target))
            items.sort()
            return ' '.join(items)
        special = ['config', 'clean', 'distclean']
        quiet = io.StringIO()
        for k, v in sorted(self.qnames.items()):
            if k:
                qv = "@echo '    {} $@';".format(k)
            else:
                qv = '@'
            quiet.write('quiet{} = {}\n'.format(v, qv))
        quiet = quiet.getvalue()
        includes = ''
        if self.includes:
            includes = '-include {}'.format(targets(self.includes))
        return MAKEFILE_TEMPLATE.format(
            notice=NOTICE,
            default_targets=targets(self.default_targets),
            clean_targets=targets(self.clean_targets),
            distclean_targets=targets(self.clean_targets.union([
                'Makefile',
                'config.dat',
                'config.log',
            ])),
            phony_targets=targets(self.phony_targets),
            built_sources=targets(self.built_sources),
            python_exe=sys.executable,
            python_path=mk_escape(str(
                Path(__file__).parent.parent.relative_to(self.builddir))),
            includes=includes,
            inputs=targets(self.build.inputs),
            mktest_begin=''.join(
                'ifneq ({},$(MAKECMDGOALS))\n'.format(x) for x in special),
            mktest_end=''.join('endif\n' for x in special),
            quiet=quiet,
            rules=self.rulefp.getvalue(),
        )

    def add_rule(self, target, sources, cmds, *, qname=None):
        """Add a rule to the makefile.

        The commands should be a list of commands, and each command is
        a list of arguments.
        """
        write = self.rulefp.write
        dirpath = target.parent.relative_to(self.builddir)
        write(mk_escape(str(target.relative_to(self.builddir))))
        write(':')
        for source in sources:
            write(' ')
            if isinstance(source, Path):
                write(mk_escape(str(source.relative_to(self.builddir))))
            else:
                write(source)
        write('\n')

        if dirpath != Path('.'):
            write('\t@mkdir -p {}\n'.format(escape(str(dirpath))))

        for n, cmd in enumerate(cmds):
            write('\t')
            if qname is not None:
                write('$(quiet{})'.format(
                    self.get_qname('' if n else qname)))
            for m, arg in enumerate(cmd):
                if m:
                    write(' ')
                write(escape(arg))
            write('\n')

    def compile(self, sources):
        """Compile all the sources in a list.

        Returns a list of output object file paths.

        Arguments:
        sources -- A sequence of EnvSource instances.
        """
        sources_c, sources_h = filter_sources(
            sources, COMPILED_TYPES, HEADER_TYPES)
        objects = []
        objdir = self.builddir.joinpath('build', 'obj')
        srcdir = self.build.config.srcdir
        for source in sources_c:
            srcpath = source.path
            ctype = COMPILE_TYPES[source.type]
            env = source.env
            assert env is not None
            srcrel = srcpath.relative_to(srcdir)
            objpath = objdir.joinpath(srcrel).with_suffix('.o')
            deppath = objpath.with_suffix('.d')
            self.includes.append(deppath)
            cmd = [
                env.cc,
                '-o', str(objpath.relative_to(self.builddir)),
                str(srcpath.relative_to(self.builddir)), '-c',
                '-MF', str(deppath.relative_to(self.builddir)), '-MMD', '-MP',
            ]
            cmd.extend('-iquote' + str(ipath.relative_to(self.builddir))
                       for ipath in env.ipaths)
            cmd.extend(env.cppflags)
            cmd.extend(env.cflags)
            cmd.extend(env.cwarn)
            self.add_rule(objpath, [srcpath], [cmd], qname=ctype)
            objects.append(objpath)
        return objects

    def get_qname(self, text):
        try:
            return self.qnames[text]
        except KeyError:
            n = len(self.qnames)
            self.qnames[text] = n
            return n

    def link(self, name, objects, env):
        """Link object files into an executable.

        Returns the executable path.

        Arguments:
        name -- Name of the executable to link.
        objects -- Sequence of inputs, which are paths to object files.
        env -- Linking environment variables.
        """
        exepath = self.builddir.joinpath('build', 'products', name)
        cmd = [env.cc]
        cmd.extend(env.ldflags)
        cmd.extend(['-o', str(exepath.relative_to(self.builddir))])
        cmd.extend(str(obj.relative_to(self.builddir)) for obj in objects)
        cmd.extend(env.libs)
        self.add_rule(exepath, objects, [cmd], qname='Link')
        return exepath

    def add_target(self, target):
        """Add a target to be built to the build system.

        Returns the path to the built target.

        Arguments:
        target -- The target, an instance of Target.
        """
        toolchain = self.build.toolchain
        result = None
        ttype = type(target)
        if issubclass(ttype, ModuleTarget):
            objects = self.compile(
                Module.flat_sources(
                    toolchain, [toolchain.base], target.modules))
            if issubclass(ttype, Executable):
                result = self.link(
                    target.name,
                    objects,
                    Module.flat_link_env(
                        toolchain, [toolchain.base] + target.modules),
                )
                if ttype == Executable:
                    scriptpath = self.builddir.joinpath(result.name)
                    self.build.files.append(run_script(
                        Path(result.name),
                        result.relative_to(self.builddir),
                        'make',
                        target.default_arguments))
                    result = scriptpath
                else:
                    result = None
        if result is None:
            raise ConfigError(
                'Unsupported target type: {}.{}'
                .format(ttype.__module__, ttype.__qualname__))
        self.default_targets.add(result)

    def add_files(self, files):
        """Generate a set of files.

        Arguments:
        files -- A GeneratedFiles instance.
        """
        write = self.rulefp.write
        outputs = list(files.outputs(self.builddir))
        if not outputs:
            return
        buildbuild = self.builddir.joinpath('build')
        for output in outputs:
            if output.suffix in EXT_SRCTYPE:
                self.built_sources.add(output)
            try:
                output.relative_to(buildbuild)
            except ValueError:
                self.clean_targets.add(output)
        self.built_sources.update(
            output for output in outputs if output.suffix in EXT_SRCTYPE)
        outputs = [mk_escape(str(output.relative_to(self.builddir)))
                   for output in outputs]
        write(outputs[0])
        write(':')
        for source in files.inputs(self.builddir):
            write(' ')
            write(mk_escape(str(source.relative_to(self.builddir))))
        write('\n')
        write('\t$(quiet{})$(script) regen {}\n'.format(
            self.get_qname('Regen'), outputs[0]))
        for output in outputs[1:]:
            write('{}: {}\n'.format(outputs[0], output))

class GnuMakeGenerator(Generator):
    """Generator for GNU Make makefiles."""
    __slots__ = ['builddir']

    def __init__(self, config):
        self.builddir = config.builddir

    def generate(self, build):
        """Generate the build script.

        Arguments:
        build -- The Build instance.
        """
        writer = MakefileWriter(build)
        for target in build.targets:
            writer.add_target(target)
        for files in build.files:
            writer.add_files(files)
        build.files.append(TextFile(
            Path('Makefile'),
            writer.get_text(),
            do_regenerate=False))

    def runpath(self, path):
        """Convert a path to a string path that gives the path at runtime."""
        return path.relative_to(self.builddir)

GENERATOR = GnuMakeGenerator
