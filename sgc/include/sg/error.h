/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include <stdarg.h>
#include "defs.h"
struct sg_strbuf;

/**
 * @file sg/error.h
 * @brief Error handling.
 *
 * Functions which can signal errors often take a double pointer to
 * ::sg_error as a final argument.  This argument can either be NULL,
 * or it can point to a ::sg_error pointer.
 *
 * If the argument is NULL, then there is no place for the function to
 * store the error information, so the error information will get
 * logged instead.  If you are only going to log the error anyway, it
 * makes sense to pass NULL.
 *
 * If the argument is not NULL, then the function will report error
 * information by storing a pointer to a ::sg_error in the value
 * pointed to by the argument.  This will only happen if that pointer
 * is NULL, otherwise, a warning will be logged that an error occurred
 * and was discarded.  This means that the first error is the only
 * error that will get reported, if you reuse the same function.
 *
 * Errors can be moved and cleared using the ::sg_error_move and
 * ::sg_error_clear functions.
 *
 * By convention, most functions which signal errors have the error as
 * the last argument, and return either NULL, return a nonzero value,
 * or return a negative value to signal that an error has occurred.
 *
 * Example functions:
 *
 @code
 // Returns NULL to signal errors.
 void *func1(int x, int y, struct sg_error **err);

 // Returns nonzero to signal errors.
 int func2(char *str, struct sg_error **err);

 void explicit_error_handling(void) {
     // Must initialize to NULL.
     struct sg_error *err = NULL;

     void *ptr = func1(15, 32, &err);
     if (!ptr) {
         // Ignore the error.
         puts("Error occurred, ignoring.");
         sg_error_clear(&err);
     }

     int r = func2("Hello", &err);
     if (!ptr) {
         // Critical error, abort.
         printf("Error occurred: %s\n", (*err)->msg);
         abort();
     }
 }

 void implicit_error_handling(void) {
     void *ptr = func1(15, 32, NULL);
     // Error is automatically logged.
     if (!ptr)
         abort();
 }
 @endcode
 */

/**
 * @brief An error.
 *
 * Errors can be identified by their domain and code.  Errors should
 * be freed by calling sg_error_clear().
 */
struct sg_error {
    /**
     * @brief The error domain.
     *
     * This allows code to identify the error.
     */
    const struct sg_error_domain *domain;

    /**
     * @brief The error message.
     *
     * This is a human-readable message.
     */
    const char *msg;

    /**
     * @brief The error code.
     *
     * The meaning of this field is domain-specific.
     */
    long code;
};

/**
 * @brief An error domain.
 */
struct sg_error_domain {
    /**
     * @brief The name of the error domain.
     */
    char name[16];

    /**
     * @brief Format an error as a human-readable message.
     *
     * @param buf The string buffer to write into.
     * @param msg The error's message.
     * @param code The error's error code.
     */
    void (*message)(struct sg_strbuf *buf, const char *msg, long code);
};

/**
 * @brief Standard error codes.
 */
typedef enum {
    /**
     * @brief General error.
     */
    SG_ERR_GENERAL,

    /**
     * @brief Memory allocation failed.
     */
    SG_ERR_NOMEM,

    /**
     * @brief The file has not changed.  This is not an error.
     */
    SG_ERR_NOTCHANGED,

    /**
     * @brief File or directory not found.
     */
    SG_ERR_NOTFOUND,

    /**
     * @brief A component of a path is not a directory.
     */
    SG_ERR_NOTDIR,

    /**
     * @brief The object is a directory, not a file.
     */
    SG_ERR_ISDIR,

    /**
     * @brief Invalid path.
     */
    SG_ERR_BADPATH,

    /**
     * @brief File contains invalid data.
     */
    SG_ERR_BADDATA,

    /**
     * @brief File is too large.
     */
    SG_ERR_TOOBIG,

    /**
     * @brief Invalid argument.
     */
    SG_ERR_INVAL,

    /**
     * @brief Feature is disabled (compiled out).
     */
    SG_ERR_DISABLED,

    /**
     * @brief Image or pixel buffer dimensions are too extreme.
     */
    SG_ERR_EDIM,

    /**
     * @brief Operation not permitted.
     */
    SG_ERR_PERM,
} sg_err_t;

/**
 * @brief The number of standard error codes.
 */
#define SG_ERR_COUNT ((int)SG_ERR_PERM + 1)

/**
 * @brief Standard error code domain.
 *
 * Error codes are ::sg_err_t.
 */
extern const struct sg_error_domain SG_ERROR_STANDARD;

/**
 * @brief Set an error using a standard error code.
 *
 * @param err The error to set, may be NULL.
 * @param code The error code.
 */
void sg_error_set(struct sg_error **err, sg_err_t code);

/**
 * @brief Get the standard error code for an error.
 *
 * If the error is not a standard error, then ::SG_ERR_GENERAL is
 * returned.
 *
 * @param err The error to query.
 * @return The error code.
 */
sg_err_t sg_error_get(struct sg_error *err);

/**
 * @brief Create an error indicating a memory allocation failure.
 *
 * @param err The error to initialize, can be NULL.
 */
void sg_error_nomem(struct sg_error **err);

/**
 * @brief Set an error using a format string.
 *
 * This function should not be called directly, unless you are
 * creating your own error domain.
 *
 * @param err The error to set, may be NULL.
 * @param dom The error domain.
 * @param code The error code.
 * @param fmt The error message, a format string.
 * @param ... The format arguments.
 */
SG_ATTR_FORMAT(printf, 4, 5)
void sg_error_setf(struct sg_error **err, const struct sg_error_domain *dom,
                   long code, const char *fmt, ...);

/**
 * @brief Set an error using a format string and argument list.
 *
 * This function should not be called directly, unless you are
 * creating your own error domain.
 *
 * @param err The error to set, may be NULL.
 * @param dom The error domain.
 * @param code The error code.
 * @param fmt The error message, a format string.
 * @param ap The argument list.
 */
void sg_error_setv(struct sg_error **err, const struct sg_error_domain *dom,
                   long code, const char *fmt, va_list ap);

/**
 * @brief Set an error using a string.
 *
 * This function should not be called directly, unless you are
 * creating your own error domain.
 *
 * @param err The error to set, may be NULL.
 * @param dom The error domain.
 * @param code The error code.
 * @param msg The error message, this will be copied verbatim.
 */
void sg_error_sets(struct sg_error **err, const struct sg_error_domain *dom,
                   long code, const char *msg);

/**
 * @brief Move an error from one location to another.
 *
 * This will give warnings for discarded errors as expected.
 *
 * @param dest The destination error location, can be NULL.
 * @param src The source error location, will be set to NULL.
 */
void sg_error_move(struct sg_error **dest, struct sg_error **src);

/**
 * @brief Copy an error and store it in the given location.
 *
 * This will give warnings for discarded errors as expected.
 *
 * @param dest The destination error location, can be NULL.
 * @param src The error to copy.
 */
void sg_error_copy(struct sg_error **dest, const struct sg_error *src);

/**
 * @brief Clear an error location.
 *
 * @param err The error to clear.
 */
void sg_error_clear(struct sg_error **err);

#if defined _WIN32 || defined DOXYGEN

/**
 * @brief Domain for Windows system errors.
 */
extern const struct sg_error_domain SG_ERROR_WINDOWS;

/**
 * @brief Create an error from a Windows error code.
 *
 * @param err The error to initialize, can be NULL.
 * @param code The Windows error code.
 */
void sg_error_win32(struct sg_error **err, unsigned long code);

/**
 * @brief Create an error from an <tt>HRESULT</tt>.
 *
 * @param err The error to initialize, can be NULL.
 * @param code The HRESULT code.
 */
void sg_error_hresult(struct sg_error **err, long code);

#endif

#if !defined _WIN32 || defined DOXYGEN

/**
 * @brief Domain for POSIX error codes.
 */
extern const struct sg_error_domain SG_ERROR_ERRNO;

/**
 * @brief Create an error from a POSIX error code.
 *
 * @param err The error to initialize, can be null.
 * @param code The error code.
 */
void sg_error_errno(struct sg_error **err, int code);

#endif

/**
 * @brief Domain for errors from getaddrinfo.
 */
extern const struct sg_error_domain SG_ERROR_GETADDRINFO;

/**
 * @brief Create a getaddrinfo domain error.
 *
 * @param err The error to initialize, can be null.
 * @param code The getaddrinfo error code.
 */
void sg_error_gai(struct sg_error **err, int code);
