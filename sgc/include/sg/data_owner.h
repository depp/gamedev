/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include "atomic.h"

/**
 * @file sg/data_owner.h
 * @brief Reference-counted data buffer owners.
 */

/**
 * @brief Data buffer owner.
 *
 * This is a virtual base class, it should not be copied or created
 * unless you are implementing a concrete subclass.  This is
 * thread-safe, and buffers with the same owner can be used from
 * different threads simultaneously.
 */
struct sg_data_owner {
    /**
     * @private @brief The reference count.
     */
    sg_atomic_t refcount;

    /**
     * @private @brief Free the buffer and owner.
     */
    void (*free)(struct sg_data_owner *owner);
};

/**
 * @brief Increment the reference count for a data buffer owner.
 *
 * This is thread-safe, and can be called on the same object from
 * multiple threads simultaneously.
 *
 * @param p The data buffer owner, may be NULL.
 */
void sg_data_owner_incref(struct sg_data_owner *p);

/**
 * @brief Increment the reference count for a data buffer owner.
 *
 * This is thread-safe, and can be called on the same object from
 * multiple threads simultaneously.
 *
 * @param p The data buffer owner, may be NULL.
 */
void sg_data_owner_decref(struct sg_data_owner *p);
