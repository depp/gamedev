/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

#include "defs.h"

#include <stddef.h>

/*
 * Define char32_t, using C11 if possible.
 */

#if defined __STDC_VERSION__
# if __STDC_VERSION__ >= 201112L
#  include <uchar.h>
#  define SG_HAS_UCHAR 1
# endif
#endif
#if !defined SG_HAS_UCHAR && !defined DOXYGEN
typedef unsigned char32_t;
#endif
#undef SG_HAS_UCHAR

/**
 * @file sg/u32buf.h
 * @brief UTF-32 text buffers.
 *
 * This is modeled after the interface in sg/strbuf.h, but using
 * UTF-32 instead of UTF-8 as the encoding for the internal buffer.
 * However, this interface does not always NUL-terminate its strings,
 * since UTF-32 is not used for system calls.
 */

/**
 * @brief A buffer of UTF-32 text data.
 */
struct sg_u32buf {
    /**
     * @brief Pointer to the start of the buffer.
     */
    char32_t *s;

    /**
     * @brief Pointer to the current buffer position.
     */
    char32_t *p;

    /**
     * @brief Pointer to the end of the buffer.
     */
    char32_t *e;
};

/**
 * @brief Initialize a UTF-32 text buffer.
 *
 * @param b The text buffer.
 */
SG_ATTR_NONNULL((1))
void sg_u32buf_init(struct sg_u32buf *restrict b);

/**
 * @brief Destroy a UTF-32 text buffer and free associated memory.
 *
 * @param b The text buffer.
 */
SG_ATTR_NONNULL((1))
void sg_u32buf_destroy(struct sg_u32buf *restrict b);

/**
 * @brief Set the UTF-32 text buffer to be empty, but don't free
 * memory.
 *
 * @param b The text buffer.
 */
SG_ATTR_NONNULL((1))
void sg_u32buf_clear(struct sg_u32buf *restrict b);

/**
 * @brief Get the length of the string in the buffer.
 */
SG_ATTR_NONNULL((1))
size_t sg_u32buf_len(struct sg_u32buf *restrict b);

/**
 * @brief Write UTF-8 data to the buffer.
 *
 * Any invalid input sequences get replaced with U+FFFD, and
 * conversion continues.
 *
 * @param b The text buffer.
 * @param data The UTF-8 data to append to the buffer.
 * @param size The size of the UTF-8 data, in bytes.
 * @return 0 if there were no errors, 1 if there were some conversion
 * errors, and -1 if out of memory.
 */
SG_ATTR_NONNULL((1, 2))
int sg_u32buf_write(struct sg_u32buf *restrict b, const char *restrict data,
                    size_t size);

/**
 * @brief Write a single code point to the buffer.
 *
 * @param b The text buffer.
 * @param c The code point to append to the buffer.
 * @return 0 if there were no errors, 1 if there were some conversion
 * errors, and -1 if out of memory.
 */
SG_ATTR_NONNULL((1))
int sg_u32buf_putc(struct sg_u32buf *restrict b, char32_t c);

/**
 * @brief Write a string to the buffer.
 *
 * The string must be encoded usign UTF-8.  Any invalid input
 * sequences get replaced with U+FFFD, and conversion continues.
 *
 * @param b The text buffer.
 * @param text The string to append, NUL-terminated UTF-8.
 * @return 0 if there were no errors, 1 if there were some conversion
 * errors, and -1 if out of memory.
 */
SG_ATTR_NONNULL((1, 2))
int sg_u32buf_puts(struct sg_u32buf *restrict b, const char *restrict text);
