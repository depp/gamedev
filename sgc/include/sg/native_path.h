/* Copyright 2013-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include "defs.h"
struct sg_error;

/**
 * @file sg/native_path.h
 * @brief Native (platform-specific) path functionality.
 */

#if defined DOXYGEN

/**
 * @brief Native path character type.
 */
typedef platform_specific pchar;

/**
 * @brief Native path directory separator.
 */
#define SG_PATH_DIRSEP platform_specific

/**
 * @brief Secondary native path directory separator.
 *
 * If there is only one directory separator, then this is equal to
 * ::SG_PATH_DIRSEP.
 */
#define SG_PATH_DIRSEP2 platform_specific

/**
 * @brief Native search path separator.
 */
#define SG_PATH_PATHSEP platform_specific

#elif defined _WIN32

typedef wchar_t pchar;
#define SG_PATH_DIRSEP '\\'
#define SG_PATH_DIRSEP2 '/'
#define SG_PATH_PATHSEP ';'

#else

typedef char pchar;
#define SG_PATH_DIRSEP '/'
#define SG_PATH_DIRSEP2 '/'
#define SG_PATH_PATHSEP ':'

#endif

/**
 * @brief Get the native path where a new file would be created.
 *
 * Note that the parent directory may not exist yet, which is why
 * sg_path_mkpardir() exists.
 *
 * @param path The relative path.
 * @param err The path, which should be freed with free(), or NULL on
 * failure.
 */
SG_ATTR_NONNULL((1))
pchar *sg_path_writable(const char *restrict path, struct sg_error **err);

/**
 * @brief Make the parent directory for a given path.
 *
 * This will recursively construct all necessary parent directories.
 * To be clear, this function ignores everything after the last slash
 * in the path, so if you want to use this function to create a
 * directory, the path should end with a slash.
 *
 * @param path The path, whose parent directory should be created.
 * @param err On failure, the error.
 * @return Zero for success, nonzero for failure.
 */
SG_ATTR_NONNULL((1))
int sg_path_mkpardir(const char *restrict path, struct sg_error **err);
