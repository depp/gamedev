/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include <stddef.h>
#include <stdint.h>
#include "vec.h"
struct sg_error;

/**
 * @file sg/pack.h
 *
 * @brief Rectangle packing.
 */

/**
 * @brief A rectangle with integer coordinates.
 */
struct sg_packing_rect {
    /** @brief The rectangle width.  */
    uint16_t w;
    /** @brief The rectangle height.  */
    uint16_t h;
    /** @brief The X coordinate of the rectangle lower left.  */
    uint16_t x;
    /** @brief The Y coordinate of the rectangle lower left.  */
    uint16_t y;
};

/**
 * @brief A packing of rectangles in a larger rectangle.
 *
 * The packing keeps track of the free space available, so new
 * rectangles can be added to the packing dynamically.
 *
 * This must be initialized with sg_packing_init() and destroyed with
 * sg_packing_destroy().
 */
struct sg_packing {
    /** @private */
    struct sg_packing_node *node;
    /** @private */
    size_t nodesize;
    /** @private */
    size_t nodealloc;
    /** @brief Size of the enclosing rectangle.  */
    ivec2 size;
};

/**
 * @brief Initialize an empty rectangle packing.
 * @param pp The packing to initialize.
 */
void sg_packing_init(struct sg_packing *restrict pp);

/**
 * @brief Destroy a rectangle packing.
 * @param pp The packing to destroy.
 */
void sg_packing_destroy(struct sg_packing *restrict pp);

/**
 * @brief Clear a packing.
 * @param pp The packing to clear.
 * @param size The new packing size.
 */
void sg_packing_reset(struct sg_packing *restrict pp, ivec2 size);

/**
 * @brief Add rectangles to a packing.
 *
 * The coordinates of the rectangles will be initialized, if
 * successful.  If the packing operation fails, the packing structure
 * may still be modified to account for the space of some of the
 * rectangles in the packing.
 *
 * Rectangles with zero area are placed at the origin, and are
 * permitted to extend beyond the bounds of the packing.
 *
 * @param pp The packing to modify.
 * @param rects Array of rectangles to add.
 * @param count Number of rectangles to add.
 * @return 1 for success, 0 if packing failed, and -1 if out of
 * memory.
 */
int sg_packing_add(struct sg_packing *restrict pp,
                   struct sg_packing_rect *restrict rects, size_t count);

/**
 * @brief Pack rectangles, choosing a size automatically.
 *
 * This will clear any existing data in the packing.
 *
 * Rectangles with zero area are placed at the origin, and are
 * permitted to extend beyond the bounds of the packing.
 *
 * @param pp The packing to be modified.
 * @param rects Array of rectangles to pack.
 * @param count Number of rectangles to pack.
 * @param min_size Minimum size of the packing.
 * @param max_size Maximum size of the packing.
 * @return 1 for success, 0 if packing failed, and -1 if out of
 * memory.
 */
int sg_packing_autopack(struct sg_packing *restrict pp,
                        struct sg_packing_rect *restrict rects, size_t count,
                        ivec2 min_size, ivec2 max_size);
