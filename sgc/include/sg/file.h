/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include "sg/defs.h"
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
struct sg_data;
struct sg_error;

/**
 * @file sg/file.h
 * @brief File IO.
 *
 * The file IO interface presents a virtual filesystem which masks
 * platform differences and sandboxes the application.
 */

enum {
    /**
     * @brief Maximum length of a virtual path.
     *
     * This includes the NUL terminator.
     */
    SG_PATH_MAX = 80
};

/**
 * @brief Normalize and verify the virtual path to a file.
 *
 * This will verify that the path is a valid virtual path to a file,
 * and it will remove any <code>".."</code> and <code>"."</code>
 * components.
 *
 * @param buf Destination buffer, with ::SG_PATH_MAX bytes.
 * @param path The path to normalize and verify.
 * @param err On failure, the error.
 * @return The length of the normalized path, in bytes, if successful.
 * A negative number on failure.
 */
int sg_path_file(char *restrict buf, const char *restrict path,
                 struct sg_error **err);

/**
 * @brief Normalize and verify the virtual path to a directory.
 *
 * This will verify that the path is a valid virtual path, and it will
 * remove any <code>".."</code> and <code>"."</code> components.  The
 * resulting normalized path will end with a slash.
 *
 * @param buf Destination buffer, with ::SG_PATH_MAX bytes.
 * @param path The path to normalize and verify.
 * @param err On failure, the error.
 * @return The length of the normalized path, in bytes, if successful.
 * A negative number on failure.
 */
int sg_path_dir(char *restrict buf, const char *restrict path,
                struct sg_error **err);

/**
 * @brief Reference points for seeking in an open file.
 */
typedef enum {
    /**
     * @brief Seek from the beginning of the file.
     */
    SG_SEEK_SET = 0,

    /**
     * @brief Seek from the current position.
     */
    SG_SEEK_CUR = 1,

    /**
     * @brief Seek from the end of the file.
     */
    SG_SEEK_END = 2
} sg_seekpos_t;

/**
 * @brief Information about a file.
 */
struct sg_fileinfo {
    /**
     * @brief The file size, in bytes.
     */
    int64_t size;

    /**
     * @brief The modification time.
     *
     * The exact interpretation of this field is unspecified and
     * platform-dependent.
     */
    int64_t mtime;

    /**
     * @brief The full virtual path to the file.
     */
    char *path;
};

/**
 * @brief Initialize a file info record with zeroes.
 */
SG_ATTR_NONNULL((1))
void sg_fileinfo_init(struct sg_fileinfo *restrict d);

/**
 * @brief Destroy a file info record.
 */
SG_ATTR_NONNULL((1))
void sg_fileinfo_destroy(struct sg_fileinfo *restrict d);

/**
 * @brief Copy a file info record.
 *
 * @param d The destination reference, to be overwritten.
 * @param s The source reference.
 */
SG_ATTR_NONNULL((1, 2))
void sg_fileinfo_copy(struct sg_fileinfo *restrict d,
                      const struct sg_fileinfo *restrict s);

/**
 * @brief An input stream for reading a file.
 *
 * Files are not thread-safe and should not be used from multiple
 * threads simultaneously.  This is an abstract base class, it should
 * not be copied or created unless you are implementing a concrete
 * subclass.
 */
struct sg_reader {
    /**
     * @brief Information about the file.
     */
    struct sg_fileinfo info;

    /**
     * @brief Close the reader.
     *
     * @param rp The reader.
     */
    void (*close)(struct sg_reader *restrict rp);

    /**
     * @brief Read data into a buffer.
     *
     * @param rp The reader.
     * @param data Pointer to the buffer.
     * @param size Requested number of bytes to read.
     * @param err On failure, the error.
     * @return If successful, the number of bytes read, a positive
     * number.  If the end of the file has already been reached, zero.
     * If an error occurred, a negative number.
     */
    int (*read_buf)(struct sg_reader *restrict rp, void *restrict data,
                    size_t size, struct sg_error **err);

    /**
     * @brief Read data into a shared buffer object.
     *
     * This may be equivalent to ::sg_reader::read_buf, but it may do
     * something else, like map the file into memory.
     *
     * @param rp The reader.
     * @param data The shared data object to initialize.
     * @param size The requested number of bytes to read.
     * @param err On failure, the error.
     * @return If successful, 0.  If an error occurred, a negative
     * number.
     */
    int (*read_data)(struct sg_reader *restrict rp,
                     struct sg_data *restrict data, size_t size,
                     struct sg_error **err);

    /**
     * @brief Seek to a position in the file.
     *
     * @param rp The reader.
     * @param offset The offset to seek to, relative to the reference.
     * @param point The reference point for seeking.
     * @param err On failure, the error.
     * @return The new location, relative to the file start, or a
     * negative number for failure.
     */
    int64_t (*seek)(struct sg_reader *restrict rp, int64_t offset,
                    sg_seekpos_t point, struct sg_error **err);
};

/**
 * @brief Open a file for reading.
 *
 * If the extension list is not NULL, then each extension will be
 * added to the path in turn.  The extension list should be a string
 * containing each extension to try, separated by <code>':'</code>.
 * The extensions should not begin with periods.
 *
 * @param path Path to the file.
 * @param extensions Extensions to search, or NULL.
 * @param err On failure, the error.
 * @return The file if successful or NULL on failure.
 */
SG_ATTR_NONNULL((1))
struct sg_reader *sg_reader_open(const char *restrict path,
                                 const char *restrict extensions,
                                 struct sg_error **err);

/**
 * @brief Load an entire file into memory.
 *
 * See sg_reader_open() for more information about
 * <code>extensions</code>.
 *
 * @param data The data buffer to initialize.
 * @param info The file information record to initialize, may be NULL.
 * @param path Path to the file.
 * @param extensions Extensions to search, or NULL.
 * @param max_size Maximum size of the file.
 * @param err On failure, the error.
 * @return Zero for success, a negative number for failure.
 */
SG_ATTR_NONNULL((1, 3))
int sg_load(struct sg_data *restrict data, struct sg_fileinfo *restrict info,
            const char *restrict path, const char *restrict extensions,
            size_t max_size, struct sg_error **err);

/**
 * @brief Load an entire file into memory, if it has changed.
 *
 * If the modification time given in <code>mtime</code> is not 0, then
 * it is compared against the file's modification time.  If the
 * modification times are exactly equal, then the file is not loaded
 * and this function returns zero.  If the modification times are not
 * equal, or if <code>mtime</code> is 0, then the file is loaded and
 * this function returns 1.
 *
 * See sg_reader_open() for more information about
 * <code>extensions</code>.
 *
 * @param data The data buffer to initialize.
 * @param info The file information record to initialize, may be NULL.
 * @param path Path to the file.
 * @param extensions Extensions to search, or NULL.
 * @param max_size Maximum size of the file.
 * @param mtime The modification time to use for comparison, or 0.
 * @param err On failure, the error.
 * @return Zero if the file has not changed, a positive number if the
 * file has changed and was successfully read, a negative number for
 * failure.
 */
SG_ATTR_NONNULL((1, 3))
int sg_load_if_changed(struct sg_data *restrict data,
                       struct sg_fileinfo *restrict info,
                       const char *restrict path,
                       const char *restrict extensions, size_t max_size,
                       int64_t mtime, struct sg_error **err);

/**
 * @brief An output stream for writing data to a file.
 *
 * Files are not thread-safe and should not be used from multiple
 * threads simultaneously.
 */
struct sg_writer {
    /* We could make this opaque but this is simpler.  */
#if defined DOXYGEN
#elif defined _WIN32
    void *handle;
    wchar_t *destpath;
    wchar_t *temppath;
#else
    int fdes;
    char *destpath;
    char *temppath;
#endif
};

/**
 * @brief Open a file for writing.
 *
 * Either sg_writer_commit() or sg_writer_close() must be called to
 * close the writer.
 *
 * @param wp The writer to initialize.
 * @param path Path where the file will be written.
 * @param err On failure, the error.
 * @return Zero for success, nonzero on failure.
 */
SG_ATTR_NONNULL((1, 2))
int sg_writer_open(struct sg_writer *restrict wp, const char *restrict path,
                   struct sg_error **err);

/**
 * @brief Close an output stream.
 *
 * Unless sg_writer_commit() was previously called, this loses all
 * data written to the stream, leaving the filesystem untouched.  If
 * the output stream is already closed, this function does nothing.
 *
 * @param wp The writer.
 */
SG_ATTR_NONNULL((1))
void sg_writer_close(struct sg_writer *restrict wp);

/**
 * @brief Commit data written to an output stream and close it.
 *
 * This will atomically move all data written to the chosen location
 * and wait until all data has been written to disk.  After calling
 * this function, sg_writer_close() will have no effect.
 *
 * @param wp The writer.
 * @param err On failure, the error.
 * @return Zero for success, nonzero on failure.
 */
SG_ATTR_NONNULL((1))
int sg_writer_commit(struct sg_writer *restrict wp, struct sg_error **err);

/**
 * @brief Write data to an output stream.
 *
 * This will either write the entire buffer or it will fail.  If it
 * fails, an unknown amount of data may have been written to the
 * stream.
 *
 * @param wp The writer.
 * @param data Pointer to the data to write.
 * @param size The number of bytes to write.
 * @param err On failure, the error.
 * @return Zero for success, nonzero for failure.
 */
SG_ATTR_NONNULL((1))
int sg_writer_write(struct sg_writer *restrict wp, const void *restrict data,
                    size_t size, struct sg_error **err);

/**
 * @brief Seek to a location in the file.
 *
 * @param wp The writer.
 * @param offset The offset to seek to, relative to the reference.
 * @param point The reference point for seeking.
 * @param err On failure, the error.
 * @return The new location, relative to the file start, or a negative
 * number for failure.
 */
SG_ATTR_NONNULL((1))
int64_t sg_writer_seek(struct sg_writer *restrict wp, int64_t offset,
                       sg_seekpos_t point, struct sg_error **err);

/**
 * @brief An output stream for writing text to a file.
 *
 * This object will track errors itself, to simplify usage.  Either
 * sg_textwriter_commit() or sg_textwriter_close() must be called to
 * close the stream.
 *
 * Files are not thread-safe and should not be used from multiple
 * threads simultaneously.
 */
struct sg_textwriter {
    /** @private */
    struct sg_writer fp;
    /** @private */
    char *buf;
    /** @private */
    char *ptr;
    /**
     * @brief The error from the first failed operation on this
     * stream.
     *
     * This is NULL when the file is opened.
     */
    struct sg_error *err;
};

/**
 * @brief Open a file for writing text data.
 *
 * @param wp The file handle to initialize.
 * @param path Path where the file will be written.
 * @param err On failure, the error.
 * @return Zero if successful, nonzero on failure.
 */
SG_ATTR_NONNULL((1, 2))
int sg_textwriter_open(struct sg_textwriter *restrict wp, const char *path,
                       struct sg_error **err);

/**
 * @brief Close a text output stream.
 *
 * Unless sg_textwriter_commit() was previously called, this loses all
 * data written to the stream, leaving the filesystem untouched.  If
 * the output stream is already closed, this function does nothing.
 *
 * @param wp The file.
 */
SG_ATTR_NONNULL((1))
void sg_textwriter_close(struct sg_textwriter *restrict wp);

/**
 * @brief Commit changes to the file and close it.
 *
 * If an error previously occurred when writing to this stream, this
 * operation will fail and that error will be returned.  Otherwise,
 * this will atomically move all data written to the chosen location
 * and wait until all data has been written to disk.  After calling
 * this function, sg_textwriter_close() will have no effect.
 *
 * @param wp The file.
 * @param err On failure, the error.
 * @return Zero if successful, nonzero on failure.
 */
SG_ATTR_NONNULL((1))
int sg_textwriter_commit(struct sg_textwriter *restrict wp,
                         struct sg_error **err);

/**
 * @brief Write a single character to a file.
 *
 * @param wp The file.
 * @param c The character to write, a Unicode code point.
 * @return Zero if successful, nonzero on failure.
 */
int sg_textwriter_putc(struct sg_textwriter *restrict wp, int c);

/**
 * @brief Write a NUL-terminated string to a file.
 *
 * @param wp The file.
 * @param str The NUL-terminated string to write, encoded in UTF-8.
 * @return Zero if successful, nonzero on failure.
 */
SG_ATTR_NONNULL((1, 2))
int sg_textwriter_puts(struct sg_textwriter *restrict wp,
                       const char *restrict str);

/**
 * @brief Write a string to a file.
 *
 * @param wp The file.
 * @param ptr The string to write, encoded in UTF-8.
 * @param len The length of the string in bytes.
 * @return Zero if successful, nonzero on failure.
 */
SG_ATTR_NONNULL((1))
int sg_textwriter_putmem(struct sg_textwriter *restrict wp,
                         const char *restrict ptr, size_t len);

/**
 * @brief Write a formatted string to a file.
 *
 * @param wp The file.
 * @param fmt The format string.
 * @return Zero if successful, nonzero on failure.
 */
SG_ATTR_FORMAT(printf, 2, 3)
SG_ATTR_NONNULL((1, 2))
int sg_textwriter_putf(struct sg_textwriter *restrict wp,
                       const char *restrict fmt, ...);

/**
 * @brief Write a formatted string to a file.
 *
 * @param wp The file.
 * @param fmt The format string.
 * @param ap The format arguments.
 * @return Zero if successful, nonzero on failure.
 */
SG_ATTR_NONNULL((1, 2))
int sg_textwriter_putv(struct sg_textwriter *restrict wp,
                       const char *restrict fmt, va_list ap);

/**
 * @brief A list of files in a directory.
 *
 * This object can be freely copied.
 */
struct sg_filelist {
    /**
     * @brief The number of files.
     */
    int filecount;

    /**
     * @brief The number of directories.
     */
    int dircount;

    /**
     * @brief Array of information for each file in the dircetory.
     */
    struct sg_fileinfo *file;

    /**
     * @brief Array of names for each directory in the directory.
     */
    char **dir;
};

/**
 * @brief Filelist options.
 */
typedef enum {
    /**
     * @brief Don't get information about each file.
     */
    SG_FILELIST_NOINFO,

    /**
     * @brief Get information about each file.
     */
    SG_FILELIST_GETINFO,
} sg_filelist_info_t;

/**
 * @brief Scan a directory for files.
 *
 * If successful, this will initialize the list structure, which must
 * then be destroyed with sg_filelist_destroy().
 *
 * @param list The file list to initialize with the results.
 * @param path The path to the directory to scan.
 * @param info Whether to get info for each file.
 * @param err On failure, the error.
 * @return Zero for success, nonzero for failure.
 */
int sg_filelist_scan(struct sg_filelist *restrict list,
                     const char *restrict path, sg_filelist_info_t info,
                     struct sg_error **err);

/**
 * @brief Initialize an empty list of files.
 *
 * @param list The list to initialize.
 */
void sg_filelist_init(struct sg_filelist *restrict list);

/**
 * @brief Destroy a list of files.
 *
 * @param list The list to destroy.
 */
void sg_filelist_destroy(struct sg_filelist *restrict list);
