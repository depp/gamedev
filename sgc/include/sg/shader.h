/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include "sggl/common.h"
#include <stddef.h>
struct sg_error;

/**
 * @brief Correspondence between a name and a location.
 */
struct sg_shader_loc {
    /**
     * @brief The name of the uniform or attribute.
     */
    const char *name;

    /**
     * @brief The location of the uniform or attribute.
     */
    size_t offset;
};

/**
 * @brief Get the preferred file extension for a shader type.
 *
 * @param type The shader type.
 * @return The preferred file extension, or NULL if the type is unknown.
 */
const char *
sg_shader_file_extension(GLenum type);

/**
 * @brief Load an OpenGL shader.
 *
 * @param path The path to the shader.
 * @param type The shader type.
 * @param err On failure, the error.
 * @return The shader, or 0 if the operation failed.
 */
GLuint sg_shader_file(const char *path, GLenum type, struct sg_error **err);

/**
 * @brief Load an OpenGL shader.
 *
 * @param data Pointer to the shader source code.
 * @param size Size of the shader source code, in bytes.
 * @param type The shader type.
 * @param err On failure, the error.
 * @return The shader, or 0 if the operation failed.
 */
GLuint sg_shader_buffer(const void *data, size_t size, GLenum type,
                        struct sg_error **err);

/**
 * @brief Link an OpenGL shader program.
 *
 * @param obj Object to hold program.  This should be a structure
 * containing GLuint fields.  The first field contains the program
 * object, and other fields contain uniform locations.  The program
 * field must be the first field in the structure.
 * @param name The name of the shader program for diagnostic messages.
 * @param attrib Map from attribute names to locations, zero
 * terminated.
 * @param err On failure, the error.
 * @return Zero for success, nonzero on failure.
 */
int sg_shader_link(GLuint prog, const char *name,
                   const struct sg_shader_loc *attrib, struct sg_error **err);

/**
 * @brief Get OpenGL shader uniform locations.
 *
 * @param prog The shader program.
 * @param name The name of the shader program for diagnostic messages.
 * @param obj Object to store the uniform locations.  This should
 * contain GLuint fields for the uniform locations which will be
 * initialized by this function.
 * @param uniform Map from uniform names to offsets in obj for the
 * GLuint field where the corresponding uniform location will be
 * stored.
 */
void sg_shader_getuniform(GLuint prog, const char *name, void *obj,
                          const struct sg_shader_loc *uniform);
