/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include "defs.h"
#include <stddef.h>
struct sg_error;

/**
 * @file sg/data.h
 * @brief Reference-counted data buffers.
 */

/**
 * @brief Reference-counted, immutable data buffer object.
 *
 * This object may be freely moved or copied, but the data owner
 * reference count should be updated when it is copied or destroyed.
 */
struct sg_data {
    /**
     * @brief Pointer to the data buffer.
     */
    const void *ptr;

    /**
     * @brief Size of the buffer, in bytes.
     */
    size_t size;

    /**
     * @brief The owner of this data buffer.
     */
    struct sg_data_owner *owner;
};

/**
 * @brief Initialize an empty data buffer reference.
 */
void sg_data_init(struct sg_data *restrict d);

/**
 * @brief Destroy a shared data buffer reference.
 *
 * This is equivalent to calling sg_data_owner_decref() on the owner.
 *
 * @param d The data buffer reference to destroy.
 */
void sg_data_destroy(struct sg_data *restrict d);

/**
 * @brief Copy a data buffer reference.
 *
 * This is equivalent to copying the structure and then calling
 * sg_data_owner_incref() on the owner.
 *
 * @param d The destination reference, to be overwritten.
 * @param s The source reference.
 */
void sg_data_copy(struct sg_data *restrict d, const struct sg_data *restrict s);

/**
 * @brief Create a shared data object that will be freed with free().
 *
 * @param d The data object to initialize.
 * @param ptr Pointer to the data region, to be freed with free().
 * @param err On failure, the error.
 * @return Zero on success, nonzero on failure.
 */
int sg_data_from_alloc(struct sg_data *restrict d, void *ptr, size_t size,
                       struct sg_error **err);
