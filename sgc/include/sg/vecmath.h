/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include <stdbool.h>
#include "defs.h"
#include "vec.h"

/**
 * @file sg/vecmath.h
 * @brief Vector math functions.
 */

/*
 * =====================================================================
 * float
 * =====================================================================
 *
 * Note: The scalar version of function named func() will be named
 * ffuncf() here to match the naming convention in <math.h>, which
 * provides fminf() and fmaxf().
 */

/** @brief Clamp a scalar to the given range.  */
float fclampf(float x, float min_val, float max_val);
/** @brief Interpolate between two scalars.  */
float fmixf(float x, float y, float a);

/*
 * =====================================================================
 * vec2
 * =====================================================================
 */

/** @brief Test if two vectors are equal.  */
bool vec2_eq(vec2 x, vec2 y);
/** @brief Add two vectors.  */
vec2 vec2_add(vec2 x, vec2 y);
/** @brief Subtract a vector from another vector.  */
vec2 vec2_sub(vec2 x, vec2 y);
/** @brief Negate a vector.  */
vec2 vec2_neg(vec2 x);
/** @brief Mustiply two vectors componentwise.  */
vec2 vec2_mul(vec2 x, vec2 y);
/** @brief Multiply a vector by a scalar.  */
vec2 vec2_scale(vec2 x, float a);
/** @brief Get the componentwise minimum of two vectors.  */
vec2 vec2_min(vec2 x, vec2 y);
/** @brief Get the componentwise maximum of two vectors.  */
vec2 vec2_max(vec2 x, vec2 y);
/** @brief Clamp the components in a vector to the given range.  */
vec2 vec2_clamp(vec2 x, float min_val, float max_val);
/** @brief Clamp the components in a vector to the given range.  */
vec2 vec2_clampv(vec2 x, vec2 min_val, vec2 max_val);
/** @brief Get the length of a vector.  */
float vec2_length(vec2 x);
/** @brief Get the squared length of a vector.  */
float vec2_length2(vec2 x);
/** @brief Get the distance between two vectors.  */
float vec2_distance(vec2 x, vec2 y);
/** @brief Get the squared distance between two vectors.  */
float vec2_distance2(vec2 x, vec2 y);
/** @brief Get the dot product of two vectors.  */
float vec2_dot(vec2 x, vec2 y);
/** @brief Normalize a vector.  */
vec2 vec2_normalize(vec2 x);
/** @brief Interpolate between two vectors.  */
vec2 vec2_mix(vec2 x, vec2 y, float a);

/*
 * =====================================================================
 * vec3
 * =====================================================================
 */

/** @brief Test if two vectors are equal.  */
bool vec3_eq(vec3 x, vec3 y);
/** @brief Add two vectors.  */
vec3 vec3_add(vec3 x, vec3 y);
/** @brief Subtract a vector from another vector.  */
vec3 vec3_sub(vec3 x, vec3 y);
/** @brief Negate a vector.  */
vec3 vec3_neg(vec3 x);
/** @brief Mustiply two vectors componentwise.  */
vec3 vec3_mul(vec3 x, vec3 y);
/** @brief Multiply a vector by a scalar.  */
vec3 vec3_scale(vec3 x, float a);
/** @brief Get the componentwise minimum of two vectors.  */
vec3 vec3_min(vec3 x, vec3 y);
/** @brief Get the componentwise maximum of two vectors.  */
vec3 vec3_max(vec3 x, vec3 y);
/** @brief Clamp the components in a vector to the given range.  */
vec3 vec3_clamp(vec3 x, float min_val, float max_val);
/** @brief Clamp the components in a vector to the given range.  */
vec3 vec3_clampv(vec3 x, vec3 min_val, vec3 max_val);
/** @brief Get the length of a vector.  */
float vec3_length(vec3 x);
/** @brief Get the squared length of a vector.  */
float vec3_length2(vec3 x);
/** @brief Get the distance between two vectors.  */
float vec3_distance(vec3 x, vec3 y);
/** @brief Get the squared distance between two vectors.  */
float vec3_distance2(vec3 x, vec3 y);
/** @brief Get the dot product of two vectors.  */
float vec3_dot(vec3 x, vec3 y);
/** @brief Normalize a vector.  */
vec3 vec3_normalize(vec3 x);
/** @brief Interpolate between two vectors.  */
vec3 vec3_mix(vec3 x, vec3 y, float a);

/*
 * =====================================================================
 * vec4
 * =====================================================================
 */

/** @brief Test if two vectors are equal.  */
bool vec4_eq(vec4 x, vec4 y);
/** @brief Add two vectors.  */
vec4 vec4_add(vec4 x, vec4 y);
/** @brief Subtract a vector from another vector.  */
vec4 vec4_sub(vec4 x, vec4 y);
/** @brief Negate a vector.  */
vec4 vec4_neg(vec4 x);
/** @brief Mustiply two vectors componentwise.  */
vec4 vec4_mul(vec4 x, vec4 y);
/** @brief Multiply a vector by a scalar.  */
vec4 vec4_scale(vec4 x, float a);
/** @brief Get the componentwise minimum of two vectors.  */
vec4 vec4_min(vec4 x, vec4 y);
/** @brief Get the componentwise maximum of two vectors.  */
vec4 vec4_max(vec4 x, vec4 y);
/** @brief Clamp the components in a vector to the given range.  */
vec4 vec4_clamp(vec4 x, float min_val, float max_val);
/** @brief Clamp the components in a vector to the given range.  */
vec4 vec4_clampv(vec4 x, vec4 min_val, vec4 max_val);
/** @brief Get the length of a vector.  */
float vec4_length(vec4 x);
/** @brief Get the squared length of a vector.  */
float vec4_length2(vec4 x);
/** @brief Get the distance between two vectors.  */
float vec4_distance(vec4 x, vec4 y);
/** @brief Get the squared distance between two vectors.  */
float vec4_distance2(vec4 x, vec4 y);
/** @brief Get the dot product of two vectors.  */
float vec4_dot(vec4 x, vec4 y);
/** @brief Normalize a vector.  */
vec4 vec4_normalize(vec4 x);
/** @brief Interpolate between two vectors.  */
vec4 vec4_mix(vec4 x, vec4 y, float a);

/*
 * =====================================================================
 * int
 * =====================================================================
 */

#if !defined SG_IMIN_IMAX
#define SG_IMIN_IMAX

/**
 * @brief Get the maximum of two integers.
 */
SG_ATTR_CONST SG_INLINE
int imax(int x, int y) {
    return x > y ? x : y;
}

/**
 * @brief Get the minimum of two integers.
 */
SG_ATTR_CONST SG_INLINE
int imin(int x, int y) {
    return x < y ? x : y;
}

#endif

/** @brief Clamp a scalar to the given range.  */
float iclamp(float x, float min_val, float max_val);

/*
 * =====================================================================
 * ivec2
 * =====================================================================
 */

/** @brief Test if two vectors are equal.  */
bool ivec2_eq(ivec2 x, ivec2 y);
/** @brief Add two vectors.  */
ivec2 ivec2_add(ivec2 x, ivec2 y);
/** @brief Subtract a vector from another vector.  */
ivec2 ivec2_sub(ivec2 x, ivec2 y);
/** @brief Negate a vector.  */
ivec2 ivec2_neg(ivec2 x);
/** @brief Mustiply two vectors componentwise.  */
ivec2 ivec2_mul(ivec2 x, ivec2 y);
/** @brief Multiply a vector by a scalar.  */
ivec2 ivec2_scale(ivec2 x, int a);
/** @brief Get the componentwise minimum of two vectors.  */
ivec2 ivec2_min(ivec2 x, ivec2 y);
/** @brief Get the componentwise maximum of two vectors.  */
ivec2 ivec2_max(ivec2 x, ivec2 y);
/** @brief Clamp the components in a vector to the given range.  */
ivec2 ivec2_clamp(ivec2 x, int min_val, int max_val);
/** @brief Clamp the components in a vector to the given range.  */
ivec2 ivec2_clampv(ivec2 x, ivec2 min_val, ivec2 max_val);
/** @brief Get the squared length of a vector.  */
int ivec2_length2(ivec2 x);
/** @brief Get the squared distance between two vectors.  */
int ivec2_distance2(ivec2 x, ivec2 y);

/*
 * =====================================================================
 * ivec3
 * =====================================================================
 */

/** @brief Test if two vectors are equal.  */
bool ivec3_eq(ivec3 x, ivec3 y);
/** @brief Add two vectors.  */
ivec3 ivec3_add(ivec3 x, ivec3 y);
/** @brief Subtract a vector from another vector.  */
ivec3 ivec3_sub(ivec3 x, ivec3 y);
/** @brief Negate a vector.  */
ivec3 ivec3_neg(ivec3 x);
/** @brief Mustiply two vectors componentwise.  */
ivec3 ivec3_mul(ivec3 x, ivec3 y);
/** @brief Multiply a vector by a scalar.  */
ivec3 ivec3_scale(ivec3 x, int a);
/** @brief Get the componentwise minimum of two vectors.  */
ivec3 ivec3_min(ivec3 x, ivec3 y);
/** @brief Get the componentwise maximum of two vectors.  */
ivec3 ivec3_max(ivec3 x, ivec3 y);
/** @brief Clamp the components in a vector to the given range.  */
ivec3 ivec3_clamp(ivec3 x, int min_val, int max_val);
/** @brief Clamp the components in a vector to the given range.  */
ivec3 ivec3_clampv(ivec3 x, ivec3 min_val, ivec3 max_val);
/** @brief Get the squared length of a vector.  */
int ivec3_length2(ivec3 x);
/** @brief Get the squared distance between two vectors.  */
int ivec3_distance2(ivec3 x, ivec3 y);

/*
 * =====================================================================
 * mat2
 * =====================================================================
 */

/** @brief Copy a matrix. */
void mat2_copy(mat2 *restrict out, const mat2 *restrict a);
/** @brief Create the identity matrix.  */
void mat2_identity(mat2 *out);
/** @brief Create a matrix which scales vectors.  */
void mat2_scaling(mat2 *restrict out, vec2 scale);
/** @brief Create a rotation matrix.  */
void mat2_rotation(mat2 *restrict out, float angle);
/** @brief Multiply two matrixes.  */
void mat2_mul(mat2 *out, const mat2 *a, const mat2 *b);
/** @brief Transform a vector using a matrix.  */
vec2 mat2_vec2(const mat2 *restrict a, vec2 v);

/*
 * =====================================================================
 * mat3
 * =====================================================================
 */

/** @brief Copy a matrix. */
void mat3_copy(mat3 *restrict out, const mat3 *restrict a);
/** @brief Create the identity matrix.  */
void mat3_identity(mat3 *out);
/** @brief Create a matrix which scales vectors.  */
void mat3_scaling(mat3 *restrict out, vec3 scale);
/** @brief Create a rotation matrix.  */
void mat3_axis_angle(mat3 *restrict out, vec3 axis, float angle);
/** @brief Create a rotation matrix from a quaternion.  */
void mat3_quat(mat3 *restrict out, quat q);
/** @brief Multiply two matrixes.  */
void mat3_mul(mat3 *out, const mat3 *a, const mat3 *b);
/** @brief Transform a vector using a matrix.  */
vec3 mat3_vec3(const mat3 *restrict a, vec3 v);

/*
 * =====================================================================
 * mat4
 * =====================================================================
 */

/** @brief Copy a matrix. */
void mat4_copy(mat4 *restrict out, const mat4 *restrict a);
/** @brief Create the identity matrix.  */
void mat4_identity(mat4 *out);
/** @brief Create a matrix which translates vectors.  */
void mat4_translation(mat4 *restrict out, vec3 v);
/** @brief Create a matrix which scales vectors.  */
void mat4_scaling(mat4 *restrict out, vec3 scale);
/** @brief Create a rotation matrix.  */
void mat4_axis_angle(mat4 *restrict out, vec3 axis, float angle);
/** @brief Create a rotation matrix from a quaternion.  */
void mat4_quat(mat4 *restrict out, quat q);
/** @brief Multiply two matrixes.  */
void mat4_mul(mat4 *out, const mat4 *a, const mat4 *b);
/** @brief Transform a vector using a matrix.  */
vec3 mat4_vec3(const mat4 *restrict a, vec3 v);

/*
 * =====================================================================
 * quat
 * =====================================================================
 */

/** @brief Get the identity quaternion. */
quat quat_identity(void);
/** @brief Get the conjugate of a quaternion.  */
quat quat_conjugate(quat q);
/** @brief Create a quaternion for a rotation.  */
quat quat_axis_angle(vec3 axis, float angle);
/** @brief Create a quaternion from roll, pitch, yaw vector.  */
quat quat_angles(vec3 angles);
/** @brief Create a quaternion to rotate about the X axis.  */
quat quat_rotate_x(float angle);
/** @brief Create a quaternion to rotate about the Y axis.  */
quat quat_rotate_y(float angle);
/** @brief Create a quaternion to rotate about the Z axis.  */
quat quat_rotate_z(float angle);
/** @brief Multiply two quaternions.  */
quat quat_mul(quat x, quat y);
/** @brief Transform a vector using a quaternion.  */
vec3 quat_vec3(quat q, vec3 v);
