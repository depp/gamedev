/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "vec.h"
struct sg_error;

/**
 * @file sg/text.h
 * @brief Text layout.
 *
 * The text layout system converts text and style information into
 * textured quads which can be rendered.  There is one global text
 * layout system, multiple instances cannot be created.
 *
 * The text layout system is rendering engine agnostic.  It produces
 * raw vertex and texture data, which can be used by specific
 * rendering systems.
 */

struct sg_textbuilder;
struct sg_textlayout;

/**
 * @brief Font styles.
 */
typedef enum {
    /* Note: do not rearrange */
    /** @brief Normal font style.  */
    SG_FONTSTYLE_NORMAL,
    /** @brief Italic font style.  */
    SG_FONTSTYLE_ITALIC,
    /** @brief Oblique font style.  */
    SG_FONTSTYLE_OBLIQUE
} sg_fontstyle_t;

/**
 * @brief Text alignment options.
 */
typedef enum {
    /** @brief Align text to the left with a ragged right.  */
    SG_TEXTALIGN_LEFT,
    /** @brief Align text to the right with a ragged left.  */
    SG_TEXTALIGN_RIGHT,
    /** @brief Center each line of text.  */
    SG_TEXTALIGN_CENTER,
    /** @brief Justify the text.  */
    SG_TEXTALIGN_JUSTIFY
} sg_textalign_t;

/**
 * @brief Vertical box alignment options.
 */
typedef enum {
    /** @brief Anchor to the baseline of the first line of text.  */
    SG_VALIGN_BASELINE,
    /** @brief Anchor to the top of the box.  */
    SG_VALIGN_TOP,
    /** @brief Anchor to the center of the box.  */
    SG_VALIGN_CENTER,
    /** @brief Anchor to the bottom of the box.  */
    SG_VALIGN_BOTTOM
} sg_valign_t;

/**
 * @brief Horizontal box alignment options.
 */
typedef enum {
    /** @brief Anchor to the left edge of the box.  */
    SG_HALIGN_LEFT,
    /** @brief Anchor to the center of the box.  */
    SG_HALIGN_CENTER,
    /** @brief Anchor to the right edge of the box.  */
    SG_HALIGN_RIGHT
} sg_halign_t;

/**
 * @brief Description of a font, used to specify an actual font.
 *
 * Fonts are matched in a manner similar to the font matching
 * algorithm in CSS Fonts Module Level 3.  The full algorithm is not
 * implemented.
 */
struct sg_fontdesc {
    /**
     * @brief The font family name, e.g., "Helvetica".
     */
    char family[32];

    /**
     * @brief The font style.
     */
    sg_fontstyle_t style;

    /**
     * @brief The font weight, 100-900.
     */
    int weight;

    /**
     * @brief The font size, in pixels.
     */
    float size;
};

/**
 * @brief Metrics for a text layout.
 */
struct sg_textmetrics {
    /**
     * @brief The logical bounds of the text.
     */
    ibox2 logical_bounds;

    /**
     * @brief The pixel bounds of the text bitmap.
     *
     * This may be smaller or larger than the logical bounds.
     */
    ibox2 pixel_bounds;

    /**
     * @brief The location of the baseline.
     */
    int baseline;
};

/*
 * =====================================================================
 * Miscellaneous functions
 * =====================================================================
 */

/**
 * @brief Get the default font description.
 * @param desc On return, the default font description.
 */
void sg_fontdesc_default(struct sg_fontdesc *desc);

/**
 * @brief Set the font family of a font description.
 * @param desc The font description to modify.
 * @param family The new font family.
 */
void sg_fontdesc_setfamily(struct sg_fontdesc *desc, const char *family);

/**
 * @brief Get the location of an anchor point in a text layout.
 *
 * @param mp The text layout metrics.
 * @param h The horizontal anchor position.
 * @param v The vertical anchor position.
 * @return The anchor coordinates.
 */
ivec2 sg_textmetrics_anchor(const struct sg_textmetrics *restrict mp,
                            sg_halign_t h, sg_valign_t v);

/*
 * =====================================================================
 * Text batch
 * =====================================================================
 */

/**
 * @brief Create a new batch of text.
 *
 * @return On success, a batch number, which is not negative.  On
 * failure, -1.
 */
int sg_textbatch_new(struct sg_error **err);

/**
 * @brief Free a batch of text.
 *
 * This also frees all groups belonging to the batch.
 *
 * @param batch The batch to free.
 */
void sg_textbatch_free(int batch);

/*
 * =====================================================================
 * Text group
 * =====================================================================
 */

/**
 * @brief Create a new group of text.
 *
 * @param batch The batch to contain the new group.
 * @param err On failure, the error.
 * @return On success, a group number, which is not negative.  On
 * failure, -1.
 */
int sg_textgroup_new(int batch, struct sg_error **err);

/**
 * @brief Free a group of text.
 *
 * @param group The group to free.
 */
void sg_textgroup_free(int group);

/**
 * @brief Set the position of a group.
 *
 * This invalidates the group data, so it will have to be uploaded
 * to the renderer again.
 *
 * @param group The group to modify.
 * @param offset The group's new offset.
 */
void sg_textgroup_setpos(int group, ivec2 offset);

/**
 * @brief Add text to a text group.
 *
 * @param group The segement to modify.
 * @param layout The text layout to add.
 * @param offset The offset to add to the layout position.
 * @param err On failure, the error.
 * @return 0 on success, -1 on failure.
 */
int sg_textgroup_add(int group, const struct sg_textlayout *restrict layout,
                     ivec2 offset, struct sg_error **err);

/*
 * =====================================================================
 * Text layout
 * =====================================================================
 */

/**
 * @brief Create a new text layout.
 * @param err On failure, the error.
 * @return A new text layout, or NULL for failure.
 */
struct sg_textlayout *sg_textlayout_new(struct sg_error **err);

/**
 * @brief Free a text layout.
 * @param lp The layout to free.
 */
void sg_textlayout_free(struct sg_textlayout *lp);

/**
 * @brief Clear a text layout.
 * @param lp The layout to clear.
 */
void sg_textlayout_clear(struct sg_textlayout *lp);

/**
 * @brief Get the metrics for a text layout.
 * @param lp The text layout.
 * @return Pointer to the metrics, owned by the text layout.
 */
const struct sg_textmetrics *sg_textlayout_metrics(struct sg_textlayout *lp);

/*
 * =====================================================================
 * Text builder
 * =====================================================================
 */

/**
 * @brief Create a new text builder.
 *
 * @param err On failure, the error.
 * @return A new text builder, or NULL for failure.
 */
struct sg_textbuilder *sg_textbuilder_new(struct sg_error **err);

/**
 * @brief Free a text builder.
 *
 * @param bp The builder.
 */
void sg_textbuilder_free(struct sg_textbuilder *restrict bp);

/**
 * @brief Clear a text builder, resetting it to its initial state.
 *
 * @param bp The builder.
 */
void sg_textbuilder_clear(struct sg_textbuilder *restrict bp);

/**
 * @brief Append UTF-8 data to a text builder.
 *
 * @param bp The builder.
 * @param data The UTF-8 data to append to the buffer.
 * @param size The size of the UTF-8 data, in bytes.
 */
void sg_textbuilder_write(struct sg_textbuilder *restrict bp, const char *data,
                          size_t size);

/**
 * @brief Append a character to the text builder.
 *
 * @param bp The builder.
 * @param c The unicode code point to append.
 */
void sg_textbuilder_putc(struct sg_textbuilder *restrict bp, int c);

/**
 * @brief Append a UTF-8 string to a text builder.
 *
 * @param bp The builder.
 * @param text The UTF-8, NUL-terminated text to add.
 */
void sg_textbuilder_puts(struct sg_textbuilder *restrict bp, const char *text);

/**
 * @brief End the current line in a text builder.
 * @param bp The builder.
 */
void sg_textbuilder_endline(struct sg_textbuilder *restrict bp);

/**
 * @brief End the current paragraph in a text builder.
 * @param bp The builder.
 */
void sg_textbuilder_endparagraph(struct sg_textbuilder *restrict bp);

/**
 * @brief Set the current font in a text builder.
 * @param bp The builder.
 * @param font The font description for the new font.
 */
void sg_textbuilder_setfont(struct sg_textbuilder *restrict bp,
                            const struct sg_fontdesc *restrict font);

/**
 * @brief Set the frame width of a text builder.
 * @param bp The builder.
 * @param width The new width.
 */
void sg_textbuilder_setwidth(struct sg_textbuilder *restrict bp, int width);

/**
 * @brief Set the current style in a text builder.
 * @param bp The builder.
 * @param style The new style.
 */
void sg_textbuilder_setstyle(struct sg_textbuilder *restrict bp, int style);

/**
 * @brief Typeset the contents of a text builder.
 *
 * This will replace the contents of an existing layout object.  On
 * failure, the layout object will be cleared.
 *
 * Earlier errors from building the text layout may be signaled here.
 * For example, if sg_textbuilder_puts() runs out of memory, then the
 * memory error will be returned here.
 *
 * @param bp The builder.
 * @param lp The text layout.
 * @param err On failure, the error.
 * @return 0 for success, -1 on failure.
 */
int sg_textbuilder_typeset(struct sg_textbuilder *restrict bp,
                           struct sg_textlayout *restrict lp,
                           struct sg_error **err);
