/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include "sg/defs.h"
#include <stddef.h>

/**
 * @file sg/hashset.h
 * @brief Hash set
 *
 * Simple hash set for storing strings.  You are responsible for
 * managing storage for the keys in the hash set.
 */

/**
 * @brief A hash set.
 */
struct sg_hashset {
    /**
     * @brief An array of entries in the hash set.
     */
    char **data;

    /**
     * @brief The number of entries in the array which have data.
     */
    size_t size;

    /**
     * @brief The size of the array.
     */
    size_t capacity;
};

/**
 * @brief Initialize a hash set.
 *
 * @param sp The hash set.
 */
SG_ATTR_NONNULL((1))
void sg_hashset_init(struct sg_hashset *restrict sp);

/**
 * @brief Destroy a hash set.
 *
 * Note that this does not free the keys, you must do that yourself.
 *
 * @param sp The hash set.
 */
SG_ATTR_NONNULL((1))
void sg_hashset_destroy(struct sg_hashset *restrict sp);

/**
 * @brief Test if a key is present in the hash set.
 *
 * @param sp The hash set.
 * @param key The key.
 * @return 1 if the key is present, 0 if the key is not present.
 */
int sg_hashset_test(struct sg_hashset *restrict sp, const char *restrict key);

/**
 * @brief Insert a key into the hash set.
 *
 * Returns the new location for the key, or NULL if out of memory.  If
 * the key is already present, the location will already contain a
 * non-NULL pointer.  This does not update the <code>size</code> field
 * in the has set, you must do that yourself after setting the key.
 *
 * @param sp The hash set.
 * @param key The new key.
 * @return A pointer to where the key would be inserted, or NULL if
 * out of memory.
 */
SG_ATTR_NONNULL((1, 2))
char **sg_hashset_insert(struct sg_hashset *restrict sp,
                         const char *restrict key);
