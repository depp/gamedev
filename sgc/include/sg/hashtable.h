/* Copyright 2009-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include <stddef.h>

/**
 * @file sg/hashtable.h
 * @brief Hash table
 *
 * Simple hash table with linear probing.  You are responsible for
 * managing storage for the keys.
 */

/**
 * @brief A hash table.
 */
struct sg_hashtable {
    /**
     * @brief An array of hash table entries.
     */
    struct sg_hashtable_entry *contents;

    /**
     * @brief The number of entries in the array which have data.
     */
    size_t size;

    /**
     * @brief The size of the array.
     */
    size_t capacity;
};

/**
 * @brief An entry in a hash table.
 */
struct sg_hashtable_entry {
    /**
     * @brief The key.
     *
     * Storage for the key is not managed by the hash table.  The key
     * will be NULL if this entry contains no data.
     */
    char *key;

    /**
     * @brief The value.
     */
    void *value;

    /**
     * @brief The hash of the key.
     */
    unsigned hash;
};

/**
 * @brief Initialize a hash table.
 */
void sg_hashtable_init(struct sg_hashtable *d);

/**
 * @brief Destroy a hash table.
 *
 * @param d The hash table.
 * @param vfree Function to free each entry, may be NULL.
 */
void sg_hashtable_destroy(struct sg_hashtable *d,
                          void (*vfree)(struct sg_hashtable_entry *));

/**
 * @brief Fetch the entry for the given key.
 *
 * Returns NULL if the key is not found.  If an entry is returned, the
 * entry's key will be non-NULL and be equal to the search key, as
 * measured by strcmp.
 *
 * @param d The hash table.
 * @param key The key to look up.
 * @return The entry, or NULL if not found.
 */
struct sg_hashtable_entry *sg_hashtable_get(struct sg_hashtable *d,
                                            char const *key);

/**
 * @brief Insert a key into the hash table.
 *
 * Returns the new hash table entry, or NULL if out of memory.  If the
 * key is already present in the hash table, that entry will be
 * returned.  Otherwise, a new entry will be created and its key will
 * be set to the key passed to this function, and its value will be
 * set to NULL.
 *
 * If the key needs to be copied when it is inserted, you will need to
 * copy the key yourself.
 *
 * @param d The hash table.
 * @param key The key to insert or find.
 * @return The entry, or NULL if out of memory.
 */
struct sg_hashtable_entry *sg_hashtable_insert(struct sg_hashtable *d,
                                               char *key);

/**
 * @brief Erase the given entry.
 *
 * @param d The hash table.
 * @param e The entry to erase.
 */
void sg_hashtable_erase(struct sg_hashtable *d, struct sg_hashtable_entry *e);

/**
 * @brief Compact the hash table.
 *
 * @param d The hash table.
 * @return 0 on success, -1 if out of memory.
 */
int sg_hashtable_compact(struct sg_hashtable *d);

/**
 * @brief Reserve space in a hash table.
 *
 * @param d The hash table.
 * @param n The total number of keys to reserve space for.
 * @return 0 on success, -1 if out of memory.
 */
int sg_hashtable_reserve(struct sg_hashtable *d, size_t n);
