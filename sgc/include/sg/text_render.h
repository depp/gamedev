/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include <stdbool.h>
#include <stddef.h>
#include "range.h"
#include "vec.h"
struct sg_error;

/**
 * @file sg/text_render.h
 * @brief Text rendering.
 *
 * This is the low-level text rendering interface.  This is renderer
 * agnostic, and should only be used by the text rendering engine
 * itself.
 *
 * Note that there is only one global text system.  The renderer data
 * functions in this file get deltas to use for rendering, so only one
 * renderer should use these functions at a time.
 */

/**
 * @brief Vertex formats for text data.
 */
typedef enum {
    /**
     * @brief Triangle format.
     *
     * Each quad in the text is represented with six vertexes, forming
     * two triangles.  This is primarily for renderers which lack
     * geometry shaders.  The format for each vertex is in
     * ::sg_textvertex_tri.
     */
    SG_TEXTVERTEX_TRI = 1,

    /**
     * @brief Point format.
     *
     * Each quad in the text is represented with a single vertex.  The
     * format for each vertex is in ::sg_textvertex_point.
     */
    SG_TEXTVERTEX_POINT
} sg_textvertex_t;

/**
 * @brief Triangle vertex format for text.
 */
struct sg_textvertex_tri {
    /**
      * @brief The vertex position.
      */
    i16vec2 pos;

    /**
     * @brief The vertex texture coordinate.
     */
    i16vec2 tex;

    /**
     * @brief User style data.
     */
    i16vec4 sty;
};

/**
 * @brief Point vertex format for text.
 */
struct sg_textvertex_point {
    /**
     * @brief The quad position.
     *
     * This is stored as (X0, Y0, X1, Y1).
     */
    i16vec4 pos;

    /**
     * @brief The quad texture coordinates.
     *
     * This is stored as (U0, V0, U1, V1).
     */
    i16vec4 tex;

    /**
     * @brief User style data.
     */
    i16vec4 sty;
};

/**
 * @brief Text system texture data.
 */
struct sg_textrender_texture {
    /**
     * @brief Pointer to the glyph texture data, in R8 format.
     *
     * If there is no glyph data to upload, this will be null.
     */
    const void *data;

    /**
     * @brief Size of the glyph texture.
     */
    ivec2 size;

    /**
     * @brief Range of rows which have changed.
     */
    struct sg_crange rows_changed;

    /**
     * @brief Inverse of the glyph texture size.
     */
    vec2 invsize;
};

/**
 * @brief Text system vertex data.
 */
struct sg_textrender_vertex {
    /**
     * @brief Range of data bytes that have changed.
     */
    struct sg_crange byte_changed;

    /**
     * @brief Number of batches of text.
     */
    int batch_count;

    /**
     * @brief The range of vertexes for each batch.
     */
    const struct sg_crange *batch;
};

/**
 * @brief Update the text system texture data.
 *
 * This should be called once per frame, after all text objects have
 * been changed, but before any text objects have been rendered.  This
 * should be called before updating the vertex data, since vertex data
 * contains texture coordinates.
 *
 * @param[out] tp On return, the texture data.
 * @param full_update Whether the update should be a full update,
 * instead of an incremental update.
 * @param max_size The maximum texture size (width or height).
 * @param err On failure, the error.
 * @return 0 for success, -1 on failure.
 */
int sg_textrender_updatetexture(struct sg_textrender_texture *restrict tp,
                                bool full_update, int max_size,
                                struct sg_error **err);

/**
 * @brief Update the text system vertex data.
 *
 * This should be called once per frame, after text objects have been
 * changed and the texture data has been updated.  It may be called
 * multiple times per frame if necessary, if the buffer needs to be
 * resized.
 *
 * @param[out] vp On return, information about the vertex data.
 * @param format The vertex format.
 * @param full_update Whether the update should be a full update,
 * instead of an incremental update.
 * @param buf_size The buffer size, in bytes.
 * @param align The minimum alignment for updates, a power of two.
 * @return 1 if the vertex data fits within the given buffer size, 0
 * if the vertex data does not fit.
 */
int sg_textrender_updatevertex(struct sg_textrender_vertex *restrict vp,
                               sg_textvertex_t format, bool full_update,
                               int buf_size, int align);

/**
 * @brief Get the text system vertex data.
 *
 * This should be called after sg_textrender_updatevertex(), and must
 * use the same format.
 *
 * @param[out] ptr Pointer to where the vertex data should be stored.
 * @param format The vertex format.
 * @param offset Byte offset of the first vertex to copy.
 * @param size Number of bytes of vertex data to copy.
 */
void sg_textrender_vertexdata(void *restrict ptr, sg_textvertex_t format,
                              size_t offset, size_t size);
