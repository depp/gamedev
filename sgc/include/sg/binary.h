/* Copyright 2009-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include <stdint.h>
#include "defs.h"

/**
 * @file sg/binary.h
 * @brief Binary serialization / deserialization
 *
 * Functions for serializing / deserializing binary data.  Each
 * function is named using the following parts:
 *
 * - read/write
 * - b/l: big/little endian
 * - s/u: signed/unsigned integer
 */

/** @brief Read an unsigned 8-bit integer.  */
SG_INLINE uint8_t sg_read_u8(const void *ptr) {
    return *(const uint8_t *)ptr;
}

/** @brief Read a signed 8-bit integer.  */
SG_INLINE int8_t sg_read_s8(const void *ptr) {
    return *(int8_t const *)ptr;
}

/** @brief Read a big endian unsigned 16-bit integer.  */
SG_INLINE uint16_t sg_read_bu16(const void *ptr) {
    const uint8_t *p = (const uint8_t *)ptr;
    uint16_t r = ((uint16_t)p[0] << 8) | (uint16_t)p[1];
    return r;
}

/** @brief Read a little endian unsigned 16-bit integer.  */
SG_INLINE uint16_t sg_read_lu16(const void *ptr) {
    const uint8_t *p = (const uint8_t *)ptr;
    uint16_t r = ((uint16_t)p[1] << 8) | (uint16_t)p[0];
    return r;
}

/** @brief Read a big endian signed 16-bit integer.  */
SG_INLINE int16_t sg_read_bs16(const void *ptr) {
    return (int16_t)sg_read_bu16(ptr);
}

/** @brief Read a little endian signed 16-bit integer.  */
SG_INLINE int16_t sg_read_ls16(const void *ptr) {
    return (int16_t)sg_read_lu16(ptr);
}

/** @brief Read big endian unsigned 32-bit integer.  */
SG_INLINE uint32_t sg_read_bu32(const void *ptr) {
    const uint8_t *p = (const uint8_t *)ptr;
    uint32_t r = ((uint32_t)p[0] << 24) | ((uint32_t)p[1] << 16) |
                 ((uint32_t)p[2] << 8) | (uint32_t)p[3];
    return r;
}

/** @brief Read a little endian unsigned 32-bit integer.  */
SG_INLINE uint32_t sg_read_lu32(const void *ptr) {
    const uint8_t *p = (const uint8_t *)ptr;
    uint32_t r = ((uint32_t)p[3] << 24) | ((uint32_t)p[2] << 16) |
                 ((uint32_t)p[1] << 8) | (uint32_t)p[0];
    return r;
}

/** @brief Read a big endian signed 32-bit integer.  */
SG_INLINE int32_t sg_read_bs32(const void *ptr) {
    return (int32_t)sg_read_bu32(ptr);
}

/** @brief Read a little endian signed 32-bit integer.  */
SG_INLINE int32_t sg_read_ls32(const void *ptr) {
    return (int32_t)sg_read_lu32(ptr);
}

/** @brief Read big endian unsigned 64-bit integer.  */
SG_INLINE uint64_t sg_read_bu64(const void *ptr) {
    const uint8_t *p = (const uint8_t *)ptr;
    return ((uint64_t)sg_read_bu32(p) << 32) | sg_read_bu32(p + 4);
}

/** @brief Read big endian unsigned 64-bit integer.  */
SG_INLINE uint64_t sg_read_lu64(const void *ptr) {
    const uint8_t *p = (const uint8_t *)ptr;
    return ((uint64_t)sg_read_lu32(p + 4) << 32) | sg_read_lu32(p);
}

/** @brief Read a big endian signed 64-bit integer.  */
SG_INLINE int64_t sg_read_bs64(const void *ptr) {
    return (int64_t)sg_read_bu64(ptr);
}

/** @brief Read a little endian signed 64-bit integer.  */
SG_INLINE int64_t sg_read_ls64(const void *ptr) {
    return (int64_t)sg_read_lu64(ptr);
}

/** @brief Write an unsigned 8-bit integer.  */
SG_INLINE void sg_write_u8(void *ptr, uint8_t v) {
    *(uint8_t *)ptr = v;
}

/** @brief Write an unsigned 8-bit integer.  */
SG_INLINE void sg_write_s8(void *ptr, int8_t v) {
    *(int8_t *)ptr = v;
}

/** @brief Write a big endian unsigned 16-bit integer.  */
SG_INLINE void sg_write_bu16(void *ptr, uint16_t v) {
    uint8_t *p = (uint8_t *)ptr;
    p[0] = (uint8_t)(v >> 8);
    p[1] = (uint8_t)v;
}

/** @brief Write a little endian unsigned 16-bit integer.  */
SG_INLINE void sg_write_lu16(void *ptr, uint16_t v) {
    uint8_t *p = (uint8_t *)ptr;
    p[0] = (uint8_t)v;
    p[1] = (uint8_t)(v >> 8);
}

/** @brief Write a big endian signed 16-bit integer.  */
SG_INLINE void sg_write_bs16(void *ptr, int16_t v) {
    sg_write_bu16(ptr, v);
}

/** @brief Write a little endian signed 16-bit integer.  */
SG_INLINE void sg_write_ls16(void *ptr, int16_t v) {
    sg_write_lu16(ptr, v);
}

/** @brief Write a big endian unsigned 32-bit integer.  */
SG_INLINE void sg_write_bu32(void *ptr, uint32_t v) {
    uint8_t *p = (uint8_t *)ptr;
    p[0] = v >> 24;
    p[1] = v >> 16;
    p[2] = v >> 8;
    p[3] = v;
}

/** @brief Write a little endian unsigned 32-bit integer.  */
SG_INLINE void sg_write_lu32(void *ptr, uint32_t v) {
    uint8_t *p = (uint8_t *)ptr;
    p[0] = v;
    p[1] = v >> 8;
    p[2] = v >> 16;
    p[3] = v >> 24;
}

/** @brief Write a big endian signed 32-bit integer.  */
SG_INLINE void sg_write_bs32(void *ptr, int32_t v) {
    sg_write_bu32(ptr, v);
}

/** @brief Write a little endian signed 32-bit integer.  */
SG_INLINE void sg_write_ls32(void *ptr, int32_t v) {
    sg_write_lu32(ptr, v);
}

/** @brief Write a big endian unsigned 64-bit integer.  */
SG_INLINE void sg_write_bu64(void *ptr, uint64_t v) {
    uint8_t *p = (uint8_t *)ptr;
    sg_write_bu32(p, (uint32_t)(v >> 32));
    sg_write_bu32(p + 4, (uint32_t)v);
}

/** @brief Write a little endian unsigned 64-bit integer.  */
SG_INLINE void sg_write_lu64(void *ptr, uint64_t v) {
    uint8_t *p = (uint8_t *)ptr;
    sg_write_lu32(p, (uint32_t)v);
    sg_write_lu32(p + 4, (uint32_t)(v >> 32));
}

/** @brief Write a big endian signed 64-bit integer.  */
SG_INLINE void sg_write_bs64(void *ptr, int64_t v) {
    sg_write_bu64(ptr, v);
}

/** @brief Write a little endian signed 64-bit integer.  */
SG_INLINE void sg_write_ls64(void *ptr, int64_t v) {
    sg_write_lu64(ptr, v);
}
