/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

/**
 * @file sg/thread.h
 * @brief Thread synchronization objects.
 */

#if defined __linux__ || defined __APPLE__
# define SG_HAVE_RWLOCK 1
# define SG_THREAD_PTHREAD 1
# include <pthread.h>
#elif defined _WIN32
# define SG_THREAD_WINDOWS 1
# include <windows.h>
#endif

#if defined SG_THREAD_PTHREAD

struct sg_lock {
    pthread_mutex_t m;
};

struct sg_reclock {
    pthread_mutex_t m;
};

struct sg_rwlock {
    pthread_rwlock_t l;
};

struct sg_evt {
    pthread_mutex_t m;
    pthread_cond_t c;
    int is_signaled;
};

#elif defined SG_THREAD_WINDOWS

struct sg_lock {
    CRITICAL_SECTION s;
};

struct sg_reclock {
    CRITICAL_SECTION s;
};

/* FIXME: Windows Vista has a RW lock
   (and condition variables, and everything else that is good...) */

struct sg_evt {
    HANDLE e;
};

#elif defined DOXYGEN

/**
 * @brief A lock, also known as a critical section.
 *
 * Recursive locking is not supported.
 */
struct sg_lock { };

/**
 * @brief A reader-writer lock.
 *
 * Can be locked by any number of readers, or by one writer.
 */
struct sg_rwlock { };

/**
 * @brief An event, or binary semaphore.
 *
 * The event has two states: signaled and not signaled.  Signaling the
 * event causes it to become signaled.  Waiting for the event waits
 * for it to become signaled, and atomically resets it on wake.  At
 * most one thread will be awoken per signal.
 */
struct sg_evt { };

#else
# error "No threading implementation"
#endif

#if !defined DOXYGEN && !defined SG_HAVE_RWLOCK
struct sg_rwlock {
    struct sg_lock lock;
};
#endif

/**
 * @brief Types of locks.
 */
typedef enum {
    /**
     * @brief Non-recursive lock, cannot be held multiple times by the
     * same thread.
     */
    SG_LOCK_NORMAL,

    /**
     * @brief Recursive lock, can be held multiple times by the same
     * thread.
     */
    SG_LOCK_RECURSIVE
} sg_locktype_t;

/*
 * =====================================================================
 * Simple locks
 * =====================================================================
 */

/**
 * @brief Initialize a lock.
 * @param p The lock.
 * @param type The lock type.
 */
void sg_lock_init(struct sg_lock *p, sg_locktype_t type);

/**
 * @brief Destroy a lock.
 * @param p The lock.
 */
void sg_lock_destroy(struct sg_lock *p);

/**
 * @brief Acquire a lock, blocking if necessary.
 * @param p The lock.
 */
void sg_lock_acquire(struct sg_lock *p);

/**
 * @brief Try to acquire a lock without blocking.
 * @param p The lock.
 * @return Nonzero if the lock is acquired, zero otherwise.
 */
int sg_lock_try(struct sg_lock *p);

/**
 * @brief Release a lock.
 * @param p The lock.
 */
void sg_lock_release(struct sg_lock *p);

/*
 * =====================================================================
 * Reader writer locks
 * =====================================================================
 */

/**
 * @brief Initialize a reader-writer lock.
 * @param p The lock.
 */
void sg_rwlock_init(struct sg_rwlock *p);

/**
 * @brief Destroy a reader-writer lock.
 * @param p The lock.
 */
void sg_rwlock_destroy(struct sg_rwlock *p);

/**
 * @brief Acquire a reader-writer lock for writing, blocking if
 * necessary.
 * @param p The lock.
 */
void sg_rwlock_wracquire(struct sg_rwlock *p);

/**
 * @brief Try to acquire a reader-writer lock for writing without
 * blocking.
 * @param p The lock.
 * @return Nonzero if the lock is acquired, zero otherwise.
 */
int sg_rwlock_wrtry(struct sg_rwlock *p);

/**
 * @brief Release a writing lock on a reader-writer lock.
 * @param p The lock.
 */
void sg_rwlock_wrrelease(struct sg_rwlock *p);

/**
 * @brief Acquire a reader-writer lock for reading, blocking if
 * necessary.
 * @param p The lock.
 */
void sg_rwlock_rdacquire(struct sg_rwlock *p);

/**
 * @brief Try to acquire a reader-writer lock for reading without
 * blocking.
 * @param p The lock.
 * @return Nonzero if the lock is acquired, zero otherwise.
 */
int sg_rwlock_rdtry(struct sg_rwlock *p);

/**
 * @brief Release a reading lock on a reader-writer lock.
 * @param p The lock.
 */
void sg_rwlock_rdrelease(struct sg_rwlock *p);

/*
 * =====================================================================
 * Events
 * =====================================================================
 */

/**
 * @brief Initialize an event.
 * @param p The event.
 */
void sg_evt_init(struct sg_evt *p);

/**
 * @brief Destroy an event.
 * @param p The event.
 */
void sg_evt_destroy(struct sg_evt *p);

/**
 * @brief Signal an event.
 * @param p The event.
 */
void sg_evt_signal(struct sg_evt *p);

/**
 * @brief Wait until an event is signaled.
 * @param p The event.
 */
void sg_evt_wait(struct sg_evt *p);
