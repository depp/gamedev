/* Copyright 2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

/**
 * @brief A range of integers, given as a lower and upper limit.
 */
struct sg_range {
    /**
     * @brief The lowest integer in the range.
     */
    int first;

    /**
     * @brief The number after the last integer in the range.
     */
    int last;
};

/**
 * @brief A range of integers, given as a lower limit and count.
 */
struct sg_crange {
    /**
     * @brief The lowest integer in the range.
     */
    int first;

    /**
     * @brief The number of integers in the range.
     */
    int count;
};
