/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

/**
 * @file sg/cpu.h
 * @brief CPU feature detection.
 *
 * For each CPU feature, there is a corresponding flag starting with
 * @c SG_CPUF.  Each flag is only defined on architectures where it is
 * available.  The set of features enabled at runtime is stored in
 * ::sg_cpu_features.
 */

#if defined DOXYGEN
/** @brief Defined if this target has any optional features */
# define SG_CPU_HASFEATURES
/** @brief x86 MMX feature */
# define SG_CPUF_MMX
/** @brief x86 SSE feature */
# define SG_CPUF_SSE
/** @brief x86 SSE 2 feature */
# define SG_CPUF_SSE2
/** @brief x86 SSE 3 feature */
# define SG_CPUF_SSE3
/** @brief x86 SSSE 3 feature */
# define SG_CPUF_SSSE3
/** @brief x86 SSE 4.1 feature */
# define SG_CPUF_SSE4_1
/** @brief x86 SSE 4.2 feature */
# define SG_CPUF_SSE4_2
/** @brief PowerPC AltiVec / VMX feature */
# define SG_CPUF_ALTIVEC
#endif

#if defined SG_CPU_X86
# define SG_CPU_HASFEATURES 1
# define SG_CPUF_MMX       (1u << 0)
# define SG_CPUF_SSE       (1u << 1)
# define SG_CPUF_SSE2      (1u << 2)
# define SG_CPUF_SSE3      (1u << 3)
# define SG_CPUF_SSSE3     (1u << 4)
# define SG_CPUF_SSE4_1    (1u << 5)
# define SG_CPUF_SSE4_2    (1u << 6)
#endif

#if defined SG_CPU_PPC
# define SG_CPU_HASFEATURES 1
# define SG_CPUF_ALTIVEC   (1u << 0)
#endif

#if defined SG_CPU_HASFEATURES

/**
 * @brief The set of available CPU features.
 *
 * Only available if ::SG_CPU_HASFEATURES is defined.
 */
extern unsigned sg_cpu_features;

/**
 * @brief Information about a CPU feature.
 *
 * Only available if ::SG_CPU_HASFEATURES is defined.
 */
struct sg_cpu_feature {
    /**
     * @brief Lower-case version of the feature name.
     */
    char name[8];

    /**
     * @brief Feature flag.
     */
    unsigned feature;
};

/**
 * @brief Array of features that this architecture supports.
 *
 * Only available if ::SG_CPU_HASFEATURES is defined.  Terminated by a
 * zeroed entry.
 */
extern const struct sg_cpu_feature SG_CPU_FEATURE[];

#endif
