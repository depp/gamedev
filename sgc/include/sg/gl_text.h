/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include "text_render.h"
#include "sggl/common.h"
struct sg_error;

/**
 * @file sg/gl_text.h
 * @brief OpenGL text layout.
 *
 * This contains an OpenGL interface to the functionality in
 * sg/text.h and sg/text_render.h.
 */

/**
 * @brief Data and resources for OpenGL text rendering.
 *
 * After synchronizing the text system, this structure provides the
 * texture and vertex data necessary for drawing text using OpenGL.
 * This doesn't provide any shaders.
 */
struct sg_text_gl {
    /**
     * @brief The glyph texture.
     *
     * This texture is always a 2D, power-of-two, R8 texture with full
     * mipmaps.
     */
    GLuint texture;

    /**
     * @brief The scaling factor for texture coordinates.
     */
    vec2 texscale;

    /**
     * @brief The vertex array containing text layout data.
     *
     * The attributes will depend on the vertex format selected.  The
     * array will not be created if the OpenGL context does not
     * support vertex arrays.
     */
    GLuint array;

    /**
     * @brief The number of text batches.
     */
    int batch_count;

    /**
     * @brief The locations of each text batch in the vertex array.
     */
    const struct sg_crange *batch;
};

/**
 * @brief Update the OpenGL text renderer data.
 *
 * This will call the low-level text update functions, so you should
 * not call them separately.  Since the text system is global, this
 * should only be called once per frame, after the text objects are
 * updated but before they are drawn to the screen.
 *
 * @param[out] up The text renderer data.
 * @param format The vertex format to use.
 * @param err On failure, the error.
 * @return 0 for success, -1 on error.
 */
int sg_text_glupdate(struct sg_text_gl *restrict up, sg_textvertex_t format,
                     struct sg_error **err);
