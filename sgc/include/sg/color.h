/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

#include "sg/vec.h"

/**
 * @file sg/color.h
 * @brief Colors
 */

/**
 * @brief Get a color from DawnBringer's 16-color palette.
 *
 * @param index The palette color index.
 * @return The color, in linear RGBA.
 */
vec4 sg_color_db16(int index);

/**
 * @brief Get a color from DawnBringer's 32-color palette.
 *
 * @param index The palette color index.
 * @return The color, in linear RGBA.
 */
vec4 sg_color_db32(int index);

/**
 * @brief Get a color from a palette.
 *
 * @param palette The palette, in sRGB.
 * @param size The palette size.
 * @param index The palette color index.
 * @return The color, in linear RGBA.
 */
vec4 sg_color_palette(const unsigned char (*palette)[3], int size, int index);
