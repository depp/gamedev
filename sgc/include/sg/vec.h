/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

/**
 * @file sg/vec.h
 * @brief Vector types.
 */

/**
 * @brief 2D floating-point vector.
 */
typedef struct {
    /** @brief The vector components. */
    float v[2];
} vec2;

/**
 * @brief 3D floating-point vector.
 */
typedef struct {
    /** @brief The vector components. */
    float v[3];
} vec3;

/**
 * @brief 4D floating-point vector.
 */
typedef struct {
    /** @brief The vector components. */
    float v[4];
} vec4;

/**
 * @brief 2D integer vector.
 */
typedef struct {
    /** @brief The vector components. */
    int v[2];
} ivec2;

/**
 * @brief 3D integer vector.
 */
typedef struct {
    /** @brief The vector components. */
    int v[3];
} ivec3;

/**
 * @brief 4D integer vector.
 */
typedef struct {
    /** @brief The vector components. */
    int v[4];
} ivec4;

/**
 * @brief 2D 16-bit integer vector.
 */
typedef struct {
    /** @brief The vector components. */
    short v[2];
} i16vec2;

/**
 * @brief 3D 16-bit integer vector.
 */
typedef struct {
    /** @brief The vector components. */
    short v[3];
} i16vec3;

/**
 * @brief 4D 16-bit integer vector.
 */
typedef struct {
    /** @brief The vector components. */
    short v[4];
} i16vec4;

/**
 * @brief 2x2 floating-point matrix.
 */
typedef struct {
    /** @brief The matrix components, row major order.  */
    float v[4];
} mat2;

/**
 * @brief 3x3 floating-point matrix.
 */
typedef struct {
    /** @brief The matrix components, row major order.  */
    float v[9];
} mat3;

/**
 * @brief 4x4 floating-point matrix.
 */
typedef struct {
    /** @brief The matrix components, row major order.  */
    float v[16];
} mat4;

/**
 * @brief Floating-point quaternion.
 */
typedef struct {
    /** @brief The quaternion components, with real part first.  */
    float v[4];
} quat;

/**
 * @brief 2D axis-aligned box with floating-point coordinates.
 */
typedef struct {
    /** @brief Minimum coordinates of box.  */
    vec2 mins;
    /** @brief Maximum coordinates of box.  */
    vec2 maxs;
} box2;

/**
 * @brief 2D axis-aligned box with floating-point coordinates.
 */
typedef struct {
    /** @brief Minimum coordinates of box.  */
    vec3 mins;
    /** @brief Maximum coordinates of box.  */
    vec3 maxs;
} box3;

/**
 * @brief 2D axis-aligned box with integer coordinates.
 */
typedef struct {
    /** @brief Minimum coordinates of box.  */
    ivec2 mins;
    /** @brief Maximum coordinates of box.  */
    ivec2 maxs;
} ibox2;

/**
 * @brief 2D axis-aligned box with integer coordinates.
 */
typedef struct {
    /** @brief Minimum coordinates of box.  */
    ivec3 mins;
    /** @brief Maximum coordinates of box.  */
    ivec3 maxs;
} ibox3;
