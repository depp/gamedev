/* Copyright 2012-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once
#include <stddef.h>
#include "sg/defs.h"

/**
 * @file sg/hash.h
 * @brief Hash functions.
 */

/**
 * @brief Compute the hash of a byte string.
 *
 * The hash is not guaranteed to produce the same result on different
 * platforms or when using different versions of this library.  If you
 * want consistent output between platforms, use something like CRC.
 *
 * @param data Pointer to the data to hash.
 * @param len Number of bytes to hash.
 * @return The data hash
 */
SG_ATTR_PURE unsigned sg_hash(const void *data, size_t len);
