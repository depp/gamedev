/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#pragma once

#include "sg/vec.h"
#include "sggl/common.h"

#include <stdbool.h>

union sg_event;

struct demo_screen {
    bool (*load)(void);
    void (*draw)(ivec2 wsize, double time);
    void (*handle_event)(union sg_event *evt);
};

void demo_setscreen(const struct demo_screen *scr);

extern const struct demo_screen DEMO_MAIN;
extern const struct demo_screen DEMO_AUDIO;
extern const struct demo_screen DEMO_IMAGE;
extern const struct demo_screen DEMO_TYPE;

struct demo_prog_plain {
    GLuint prog;
    GLint VertOff, VertScale, Color;
};

struct demo_prog_background {
    GLuint prog;
    GLint TexMat, Texture, TexSel;
};

struct demo_prog_textured {
    GLuint prog;
    GLint View, Texture;
};

struct demo_prog_text {
    GLuint prog;
    GLint Colors, VertOff, VertScale, TexScale, Texture;
};

bool demo_prog_plain_load(struct demo_prog_plain *p);
bool demo_prog_background_load(struct demo_prog_background *p);
bool demo_prog_textured_load(struct demo_prog_textured *p);
bool demo_prog_text_load(struct demo_prog_text *p);

void demo_clear_color(int palette_index);
