/* Copyright 2015-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "defs.h"
#include "sg/color.h"
#include "sg/data.h"
#include "sg/file.h"
#include "sg/gl_text.h"
#include "sg/log.h"
#include "sg/opengl.h"
#include "sg/range.h"
#include "sg/text.h"
#include "sggl/3_2.h"

#include <stdlib.h>
#include <string.h>

enum {
    TEXT_COUNT = 6,
    LAYOUT_COUNT = 6,
};

const char *const TEXT[TEXT_COUNT] = {
    // Groups 0..3
    "Text Demo",

    // Group 4
    // Greek text with diacritical marks in decomposed form.  Without
    // HarfBuzz, this will display incorrectly, with rectangles in place
    // of some of the glyphs.
    "\xce\xa4\xce\xbf \xce\xa7\xce\xbf\xcc\x81\xce\xb2\xce\xb5\xcf\x81"
    "\xce\xba\xcf\x81\xce\xb1\xcc\x81\xcf\x86\xcf\x84 \xce\xbc\xce\xbf"
    "\xcf\x85 \xce\xb5\xce\xb9\xcc\x81\xce\xbd\xce\xb1\xce\xb9 \xce\xb3"
    "\xce\xb5\xce\xbc\xce\xb1\xcc\x81\xcf\x84\xce\xbf \xcf\x87\xce\xb5"
    "\xcc\x81\xce\xbb\xce\xb9\xce\xb1",

    // Group 4
    // Long text for testing word wrapping.
    "Solemnly he came forward and mounted the round gunrest. He faced about "
    "and "
    "blessed gravely thrice the tower, the surrounding land and the awaking "
    "mountains. Then, catching sight of Stephen Dedalus, he bent towards him "
    "and made rapid crosses in the air, gurgling in his throat and shaking his "
    "head. Stephen Dedalus, displeased and sleepy, leaned his arms on the top "
    "of the staircase and looked coldly at the shaking gurgling face that "
    "blessed him, equine in its length, and at the light untonsured hair, "
    "grained and hued like pale oak.",

    // Group 5
    // Instructions.
    "1: invert, 2: boxes",
};

struct demo_type_text {
    int group;
    ivec2 point;
};

struct demo_type {
    struct demo_prog_text prog;
    struct demo_type_text text[LAYOUT_COUNT];
    bool dark, boxes;
    int textbatch;
};

static struct demo_type demo_type;

static bool demo_type_load_text(void) {
    struct sg_fontdesc font;
    int r;

    demo_type.textbatch = sg_textbatch_new(NULL);
    if (demo_type.textbatch < 0) return false;
    for (int i = 0; i < TEXT_COUNT; i++) {
        int group = sg_textgroup_new(demo_type.textbatch, NULL);
        if (group < 0) return false;
        demo_type.text[i] = (struct demo_type_text){group, (ivec2){{0, 0}}};
    }

    struct sg_textbuilder *tb;
    tb = sg_textbuilder_new(NULL);
    if (!tb) return false;
    struct sg_textlayout *layout;
    layout = sg_textlayout_new(NULL);
    if (!layout) return false;
    const struct sg_textmetrics *tm = sg_textlayout_metrics(layout);

    {
        sg_textbuilder_clear(tb);
        sg_fontdesc_default(&font);
        sg_fontdesc_setfamily(&font, "Roboto");
        font.size = 32.0f;
        sg_textbuilder_setfont(tb, &font);
        sg_textbuilder_setstyle(tb, 0);
        sg_textbuilder_puts(tb, TEXT[0]);
        r = sg_textbuilder_typeset(tb, layout, NULL);
        if (r) return false;

        for (int i = 0; i < 4; i++) {
            sg_halign_t halign = (i & 1) ? SG_HALIGN_RIGHT : SG_HALIGN_LEFT;
            sg_valign_t valign = (i & 2) ? SG_VALIGN_TOP : SG_VALIGN_BOTTOM;
            ivec2 pos = sg_textmetrics_anchor(tm, halign, valign);
            r = sg_textgroup_add(demo_type.text[i].group, layout,
                                 (ivec2){{-pos.v[0], -pos.v[1]}}, NULL);
            if (r) return false;
        }
    }
    {
        sg_textbuilder_clear(tb);
        sg_fontdesc_default(&font);
        sg_fontdesc_setfamily(&font, "Noto Serif");
        font.size = 32.0f;
        sg_textbuilder_setfont(tb, &font);
        sg_textbuilder_setstyle(tb, 1);
        sg_textbuilder_puts(tb, TEXT[1]);
        r = sg_textbuilder_typeset(tb, layout, NULL);
        if (r) return false;

        ivec2 pos = sg_textmetrics_anchor(tm, SG_HALIGN_LEFT, SG_VALIGN_TOP);
        r = sg_textgroup_add(demo_type.text[4].group, layout,
                             (ivec2){{-pos.v[0], -pos.v[1]}}, NULL);
        if (r) return false;
    }
    {
        sg_textbuilder_clear(tb);
        sg_fontdesc_default(&font);
        sg_fontdesc_setfamily(&font, "Roboto");
        font.size = 12.0f;
        sg_textbuilder_setfont(tb, &font);
        sg_textbuilder_setstyle(tb, 2);
        sg_textbuilder_setwidth(tb, 405);
        {
            struct sg_data data;
            r = sg_load(&data, NULL, "/txt/mobydick", "txt", 1024 * 1024, NULL);
            if (r) return false;
            sg_textbuilder_write(tb, data.ptr, data.size);
            sg_data_destroy(&data);
        }
        r = sg_textbuilder_typeset(tb, layout, NULL);
        if (r) return false;

        ivec2 pos = sg_textmetrics_anchor(tm, SG_HALIGN_LEFT, SG_VALIGN_TOP);
        r = sg_textgroup_add(demo_type.text[4].group, layout,
                             (ivec2){{-pos.v[0], -pos.v[1] - 50}}, NULL);
        if (r) return false;
    }
    {
        sg_textbuilder_clear(tb);
        sg_fontdesc_default(&font);
        sg_fontdesc_setfamily(&font, "Roboto");
        font.size = 16.0f;
        sg_textbuilder_setfont(tb, &font);
        sg_textbuilder_setstyle(tb, 3);
        sg_textbuilder_puts(tb, TEXT[3]);
        r = sg_textbuilder_typeset(tb, layout, NULL);
        if (r) return false;

        ivec2 pos =
            sg_textmetrics_anchor(tm, SG_HALIGN_CENTER, SG_VALIGN_BOTTOM);
        r = sg_textgroup_add(demo_type.text[5].group, layout,
                             (ivec2){{-pos.v[0], -pos.v[1]}}, NULL);
        if (r) return false;
    }

    sg_textbuilder_free(tb);
    sg_textlayout_free(layout);
    return true;
}

static bool demo_type_load(void) {
    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "demo_type_load");
    bool success = true;

    if (!demo_prog_text_load(&demo_type.prog)) {
        success = false;
    }

    if (!demo_type_load_text()) {
        success = false;
    }

    sg_gl_debugf.pop();
    return success;
}

static void demo_type_setpos(int index, int posx, int posy) {
    sg_textgroup_setpos(demo_type.text[index].group, (ivec2){{posx, posy}});
}

static void demo_type_draw(ivec2 wsize, double time) {
    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "demo_type_draw");
    (void)time;
    glViewport(0, 0, wsize.v[0], wsize.v[1]);
    demo_clear_color(demo_type.dark ? 21 : 1);
    glClear(GL_COLOR_BUFFER_BIT);

    if (demo_type.prog.prog) {
        const struct demo_prog_text *prog = &demo_type.prog;
        demo_type_setpos(0, 10, 10);
        demo_type_setpos(1, wsize.v[0] - 10, 10);
        demo_type_setpos(2, 10, wsize.v[1] - 10);
        demo_type_setpos(3, wsize.v[0] - 10, wsize.v[1] - 10);
        demo_type_setpos(4, 10, wsize.v[1] - 50);
        demo_type_setpos(5, wsize.v[0] / 2, 10);
        struct sg_text_gl up;
        int r = sg_text_glupdate(&up, SG_TEXTVERTEX_POINT, NULL);
        if (r < 0) abort();
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        glUseProgram(prog->prog);

        glUniform2f(prog->VertScale, (float)(2.0 / wsize.v[0]),
                    (float)(2.0 / wsize.v[1]));
        glUniform1i(prog->Texture, 0);
        {
            const int N = 4;
            vec4 c[N];
            for (int i = 0; i < N; i++) {
                c[i] = sg_color_db32(21 - i);
            }
            glUniform4fv(prog->Colors, N, c[0].v);
        }
        float v[2] = {(float)wsize.v[0] * -0.5f, (float)wsize.v[1] * -0.5f};
        glUniform2fv(prog->VertOff, 1, v);
        glUniform2fv(prog->TexScale, 1, up.texscale.v);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, up.texture);

        struct sg_crange range = up.batch[demo_type.textbatch];
        glBindVertexArray(up.array);
        glDrawArrays(GL_POINTS, range.first, range.count);
        glBindVertexArray(0);
    }

    sg_gl_debugf.pop();
}

static void demo_type_handle_event(union sg_event *evt) {
    (void)evt;
}

const struct demo_screen DEMO_TYPE = {demo_type_load, demo_type_draw,
                                      demo_type_handle_event, NULL};
