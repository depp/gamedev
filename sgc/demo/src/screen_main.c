/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "defs.h"
#include "sg/color.h"
#include "sg/event.h"
#include "sg/gl_text.h"
#include "sg/keycode.h"
#include "sg/opengl.h"
#include "sg/range.h"
#include "sg/text.h"
#include "sggl/3_2.h"

struct demo_main_item {
    char name[16];
    const struct demo_screen *demo;
};

static const struct demo_main_item DEMO_MAIN_ITEMS[] = {
    { "Image", &DEMO_IMAGE },
    { "Typesetting", &DEMO_TYPE },
    { "Audio", &DEMO_AUDIO },
};

enum {
    NUM_TEST = sizeof(DEMO_MAIN_ITEMS) / sizeof(*DEMO_MAIN_ITEMS),
    BOX_MARGIN = 4,
    BOX_WIDTH = 100,
    BOX_HEIGHT = 18,
};

static const short BOX_VERTEX[] = {
    -BOX_MARGIN, -BOX_MARGIN,
    BOX_MARGIN + BOX_WIDTH, -BOX_MARGIN,
    BOX_MARGIN + BOX_WIDTH, BOX_MARGIN + BOX_HEIGHT,
    -BOX_MARGIN, BOX_MARGIN + BOX_HEIGHT
};

struct demo_main {
    int selection;
    struct demo_prog_plain prog_plain;
    struct demo_prog_text prog_text;
    GLuint buffer;
    GLuint array;
    int textbatch;
    struct sg_text_gl text;
};

static struct demo_main demo_main;

static bool demo_main_load_text(void) {
    int r;
    demo_main.textbatch = sg_textbatch_new(NULL);
    if (demo_main.textbatch < 0) return false;
    int grp = sg_textgroup_new(demo_main.textbatch, NULL);
    if (grp < 0) return false;

    struct sg_fontdesc font;
    sg_fontdesc_default(&font);
    sg_fontdesc_setfamily(&font, "Roboto");

    struct sg_textbuilder *tb;
    tb = sg_textbuilder_new(NULL);
    if (!tb) return false;
    struct sg_textlayout *layout = sg_textlayout_new(NULL);
    if (!layout) return false;
    const struct sg_textmetrics *tm = sg_textlayout_metrics(layout);
    for (int i = 0; i < NUM_TEST; i++) {
        sg_textbuilder_clear(tb);
        sg_textbuilder_setfont(tb, &font);
        sg_textbuilder_setstyle(tb, i);
        sg_textbuilder_puts(tb, DEMO_MAIN_ITEMS[i].name);
        r = sg_textbuilder_typeset(tb, layout, NULL);
        if (r) return false;
        ivec2 pos = sg_textmetrics_anchor(tm, SG_HALIGN_LEFT, SG_VALIGN_TOP);
        r = sg_textgroup_add(grp, layout,
                             (ivec2){{-pos.v[0], -pos.v[1] - 32 * i}}, NULL);
        if (r < 0) return false;
    }
    sg_textlayout_free(layout);
    sg_textbuilder_free(tb);

    r = sg_text_glupdate(&demo_main.text, SG_TEXTVERTEX_POINT, NULL);
    if (r) return false;

    glGenVertexArrays(1, &demo_main.array);
    glGenBuffers(1, &demo_main.buffer);

    glBindBuffer(GL_ARRAY_BUFFER, demo_main.buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(BOX_VERTEX), BOX_VERTEX,
                 GL_STATIC_DRAW);
    glBindVertexArray(demo_main.array);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_SHORT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return true;
}

static bool demo_main_load(void) {
    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "demo_main_load");
    bool success = true;

    if (!demo_prog_plain_load(&demo_main.prog_plain)) {
        success = false;
    }
    if (!demo_prog_text_load(&demo_main.prog_text)) {
        success = false;
    }
    if (!demo_main_load_text()) {
        return false;
    }

    sg_gl_debugf.pop();
    return success;
}

static void demo_main_draw(ivec2 wsize, double time) {
    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "demo_main_draw");

    (void) time;

    glViewport(0, 0, wsize.v[0], wsize.v[1]);
    demo_clear_color(1);
    glClear(GL_COLOR_BUFFER_BIT);

    float vertscale[2] = {2.0f / (float)wsize.v[0], 2.0f / (float)wsize.v[1]};

    if (demo_main.prog_plain.prog) {
        int i = demo_main.selection;
        const struct demo_prog_plain *restrict prog = &demo_main.prog_plain;
        glUseProgram(prog->prog);
        glBindVertexArray(demo_main.array);

        glUniform2f(
            prog->VertOff,
            32.0f - 0.5f * (float) wsize.v[0],
            0.5f * (float) wsize.v[1] - 32.0f * (float) (i + 1) - BOX_HEIGHT);
        glUniform2fv(prog->VertScale, 1, vertscale);
        glUniform4fv(prog->Color, 1, sg_color_db32(20).v);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    }

    if (demo_main.prog_text.prog) {
        const struct demo_prog_text *restrict prog = &demo_main.prog_text;
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
        glUseProgram(prog->prog);

        glUniform2f(
            prog->VertScale,
            (float) (2.0 / wsize.v[0]),
            (float) (2.0 / wsize.v[1]));
        glUniform1i(prog->Texture, 0);
        {
            vec4 c[NUM_TEST], z = sg_color_db32(20);
            for (int i = 0; i < NUM_TEST; i++) {
                c[i] = z;
            }
            c[demo_main.selection] = sg_color_db32(0);
            glUniform4fv(prog->Colors, NUM_TEST, c[0].v);
        }
        {
            vec2 voff = {{(float)(-wsize.v[0] + 64) * 0.5f,
                          (float)(+wsize.v[1] - 64) * 0.5f}};
            glUniform2fv(prog->VertOff, 1, voff.v);
        }
        glUniform2fv(prog->TexScale, 1, demo_main.text.texscale.v);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, demo_main.text.texture);

        const struct sg_crange range =
            demo_main.text.batch[demo_main.textbatch];
        glBindVertexArray(demo_main.text.array);
        glDrawArrays(GL_POINTS, range.first, range.count);
        glBindVertexArray(0);

        glDisable(GL_BLEND);
    }

    sg_gl_debugf.pop();
}

static void demo_main_set_selection(int selection) {
    if (selection < 0)
        selection += NUM_TEST;
    else if (selection >= NUM_TEST)
        selection -= NUM_TEST;
    demo_main.selection = selection;
}

static void demo_main_handle_event(union sg_event *evt) {
    switch (evt->common.type) {
    case SG_EVENT_KDOWN:
    case SG_EVENT_KREPEAT:
        switch (evt->key.key) {
        case KEY_Down:
            demo_main_set_selection(demo_main.selection + 1);
            break;

        case KEY_Up:
            demo_main_set_selection(demo_main.selection - 1);
            break;

        case KEY_Enter:
            if (evt->common.type == SG_EVENT_KDOWN) {
                demo_setscreen(DEMO_MAIN_ITEMS[demo_main.selection].demo);
            }
            break;

        default:
            break;
        }
        break;

    default:
        break;
    }
}

const struct demo_screen DEMO_MAIN = {demo_main_load, demo_main_draw,
                                      demo_main_handle_event};
