/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "defs.h"
#include "sg/color.h"
#include "sg/entry.h"
#include "sg/event.h"
#include "sg/keycode.h"
#include "sg/log.h"
#include "sg/mixer.h"
#include "sg/record.h"
#include "sggl/3_2.h"

#include <stdbool.h>

void demo_clear_color(int palette_index) {
    vec4 c = sg_color_db32(palette_index);
    glClearColor(c.v[0], c.v[1], c.v[2], c.v[3]);
}

static const struct demo_screen *demo_screen_cur, *demo_screen_next;
static bool demo_screen_loaded;

static void demo_screen_update(void) {
    if (demo_screen_cur != demo_screen_next) {
        demo_screen_cur = demo_screen_next;
        demo_screen_loaded = false;
    }
}

void demo_setscreen(const struct demo_screen *scr) {
    demo_screen_next = scr;
}

void sg_game_init(void) {
    sg_mixer_start();
    demo_screen_cur = demo_screen_next = &DEMO_MAIN;
}

void sg_game_destroy(void) {
}

void sg_game_getinfo(struct sg_game_info *info) {
    info->name = "SGLib Tests";
}

void sg_game_event(union sg_event *evt) {
    switch (evt->common.type) {
    case SG_EVENT_VINIT:
        glEnable(GL_FRAMEBUFFER_SRGB);
        break;

    case SG_EVENT_KDOWN:
        switch (evt->key.key) {
        case KEY_Backslash:
            sg_record_screenshot();
            return;

        case KEY_F10:
            sg_record_start(evt->common.time);
            return;

        case KEY_F11:
            sg_record_stop();
            return;

        case KEY_Escape:
            if (demo_screen_cur != &DEMO_MAIN) {
                demo_setscreen(&DEMO_MAIN);
                demo_screen_update();
            }
            return;
        }
        break;

    default:
        break;
    }

    demo_screen_cur->handle_event(evt);
    demo_screen_update();
}

void sg_game_draw(int width, int height, double time) {
    sg_mixer_settime(time);
    if (!demo_screen_loaded) {
        if (!demo_screen_cur->load()) {
            sg_logs(SG_LOG_ERROR, "Could not load screen resources.");
        }
        demo_screen_loaded = true;
    }
    demo_screen_cur->draw((ivec2){{width, height}}, time);
    demo_screen_update();
    sg_mixer_commit();
}
