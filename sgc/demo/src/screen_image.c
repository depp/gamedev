/* Copyright 2014-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "defs.h"
#include "sg/event.h"
#include "sg/pixbuf.h"
#include "sg/keycode.h"
#include "sg/log.h"
#include "sg/opengl.h"
#include "sg/vecmath.h"
#include "sggl/3_2.h"

#include <math.h>
#include <string.h>

struct demo_image_vertex {
    short x, y;
    unsigned char tex[4];
};

struct demo_image_info {
    char name[12];
    int x, y;
};

static int bufalign(int x) {
    return (int)(((unsigned)x + 63u) & ~63u);
}

enum {
    NUM_IMG = 15,
    IMG_X = 6,
    IMG_Y = 5,
    IMG_SPACING = 128,
    IMG_SIZE = 96,

    NUM_TEX = 3,
};

static const struct demo_image_info IMAGE_INFO[NUM_IMG] = {
    { "png_i4",     0, 2 },
    { "png_i8",     0, 3 },
    { "png_ia4",    1, 2 },
    { "png_ia8",    1, 3 },
    { "png_rgb8",   2, 3 },
    { "png_rgb16",  2, 4 },
    { "png_rgba8",  3, 3 },
    { "png_rgba16", 3, 4 },
    { "png_y1",     4, 0 },
    { "png_y2",     4, 1 },
    { "png_y4",     4, 2 },
    { "png_y8",     4, 3 },
    { "png_y16",    4, 4 },
    { "png_ya8",    5, 3 },
    { "png_ya16",   5, 4 },
};

static const char TEX_NAMES[NUM_TEX][12] = {"brick", "ivy", "roughstone"};

// A fullscreen triangle in clip space.
static const short BGDATA[] = {-1, -1, 3, -1, -1, 3};

// Load a 2D array texture.
static GLuint demo_image_load_2darray(const char *prefix,
                                      const char *const *paths) {
    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "demo_image_load_2darray");
    int n = 0;
    while (paths[n]) {
        n++;
    }
    GLuint buffer = 0;
    char *ptr = NULL;
    bool has_image = false;
    ivec2 size = {{0, 0}};
    int planesize = 0;
    for (int i = 0; i < n; i++) {
        char path[64];
        strcpy(path, prefix);
        strcat(path, paths[i]);
        struct sg_image *img = sg_image_file(path, NULL);
        if (!img) {
            continue;
        }
        ivec2 isize = {{img->width, img->height}};
        if (!has_image) {
            size = isize;
            has_image = true;
            planesize = isize.v[0] * isize.v[1] * 4;
            glGenBuffers(1, &buffer);
            glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer);
            glBufferData(GL_PIXEL_UNPACK_BUFFER, planesize * n, NULL,
                         GL_STREAM_DRAW);
            ptr = glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);
            if (!ptr) {
                return 0;
            }
        } else {
            if (!ivec2_eq(isize, size)) {
                sg_logf(SG_LOG_ERROR,
                        "%s: Mismatched size, expected %dx%d but got %dx%d.",
                        path, size.v[0], size.v[1], isize.v[0], isize.v[1]);
                continue;
            }
        }
        struct sg_pixbuf pb;
        pb.data = ptr + planesize * i;
        pb.format = SG_RGBA;
        pb.width = size.v[0];
        pb.height = size.v[1];
        pb.rowbytes = size.v[0] * 4;
        img->draw(img, &pb, 0, 0, NULL);
        img->free(img);
    }
    if (!has_image) {
        return 0;
    }
    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D_ARRAY, tex);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_SRGB8_ALPHA8, size.v[0], size.v[1],
                 n, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,
                    GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,
                    GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glDeleteBuffers(1, &buffer);
    return tex;
}

struct demo_image {
    struct demo_prog_background prog_bg;
    struct demo_prog_textured prog_tx;

    // Textures (both 2D arrays)
    GLuint fgtex, bgtex;
    // Buffer
    GLuint buffer;
    // Vertex arrays: background, foreground.
    GLuint arr[2];
};

static struct demo_image demo_image;

static bool demo_image_load_fgtex(void) {
    const char *paths[NUM_IMG + 1];
    for (int i = 0; i < NUM_IMG; i++) {
        paths[i] = IMAGE_INFO[i].name;
    }
    paths[NUM_IMG] = NULL;
    GLuint tex = demo_image_load_2darray("/imgtest/", paths);
    if (!tex) {
        return false;
    }
    demo_image.fgtex = tex;
    return true;
}

static bool demo_image_load_bgtex(void) {
    const char *paths[NUM_TEX + 1];
    for (int i = 0; i < NUM_TEX; i++) {
        paths[i] = TEX_NAMES[i];
    }
    paths[NUM_TEX] = NULL;
    GLuint tex = demo_image_load_2darray("/tex/", paths);
    if (!tex) {
        return false;
    }
    demo_image.bgtex = tex;
    return true;
}

static bool demo_image_load_buffer(void) {
    int bgpos = 0;
    int bgsize = sizeof(BGDATA);
    int fgpos = bgpos + bufalign(bgsize);
    int fgsize = sizeof(struct demo_image_vertex) * NUM_IMG * 6;
    int size = fgpos + bufalign(fgsize);

    glGenBuffers(1, &demo_image.buffer);
    glBindBuffer(GL_ARRAY_BUFFER, demo_image.buffer);
    glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_STATIC_DRAW);
    char *ptr = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

    memcpy(ptr, BGDATA, sizeof(BGDATA));

    struct demo_image_vertex *vert = (void *)(ptr + fgpos);
    for (int i = 0; i < NUM_IMG; i++) {
        struct demo_image_vertex *v = vert + i * 6;
        const struct demo_image_info *restrict ifo = &IMAGE_INFO[i];
        int x = (ifo->x * 2 + 1 - IMG_X) * IMG_SPACING / 2;
        int y = (ifo->y * 2 + 1 - IMG_Y) * IMG_SPACING / 2;
        short x0 = x - IMG_SIZE / 2, x1 = x0 + IMG_SIZE;
        short y0 = y - IMG_SIZE / 2, y1 = y0 + IMG_SIZE;
        unsigned char tz = i;
        v[0] = (struct demo_image_vertex){x0, y0, {0, 1, tz, 0}};
        v[1] = (struct demo_image_vertex){x1, y0, {1, 1, tz, 0}};
        v[2] = (struct demo_image_vertex){x0, y1, {0, 0, tz, 0}};
        v[3] = (struct demo_image_vertex){x0, y1, {0, 0, tz, 0}};
        v[4] = (struct demo_image_vertex){x1, y0, {1, 1, tz, 0}};
        v[5] = (struct demo_image_vertex){x1, y1, {1, 0, tz, 0}};
    }

    glUnmapBuffer(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return true;
}

static bool demo_image_load_arrays(void) {
    glBindBuffer(GL_ARRAY_BUFFER, demo_image.buffer);
    glGenVertexArrays(2, demo_image.arr);

    uintptr_t bgpos = 0;
    uintptr_t bgsize = sizeof(BGDATA);
    uintptr_t fgpos = bgpos + bufalign(bgsize);
    // int fgsize = sizeof(Vertex) * NUM_IMG * 6;

    // background
    glBindVertexArray(demo_image.arr[0]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_SHORT, GL_FALSE, 4, (void *)(bgpos));

    // textured
    glBindVertexArray(demo_image.arr[1]);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_SHORT, GL_FALSE, 8, (void *)(fgpos));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_UNSIGNED_BYTE, GL_FALSE, 8,
                          (void *)(fgpos + 4));

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return true;
}

static bool demo_image_load(void) {
    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "demo_image_load");
    bool success = true;

    if (!demo_prog_background_load(&demo_image.prog_bg)) {
        success = false;
    }
    if (!demo_prog_textured_load(&demo_image.prog_tx)) {
        success = false;
    }
    if (!demo_image_load_fgtex()) {
        success = false;
    }
    if (!demo_image_load_bgtex()) {
        success = false;
    }
    if (!demo_image_load_buffer()) {
        success = false;
    }
    if (!demo_image_load_arrays()) {
        success = false;
    }

    sg_gl_debugf.pop();
    return success;
}

static void demo_image_draw(ivec2 wsize, double time) {
    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "demo_image_draw");
    glViewport(0, 0, wsize.v[0], wsize.v[1]);
    glClearColor(0.05, 0.1, 0.2, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    // csi = clip space from image space
    // ics = image space from clip space
    float sci[2], isc[2];
    {
        double ssz[2] = {(double)wsize.v[0], (double)wsize.v[1]};
        double isz[2] = {IMG_SPACING * IMG_X, IMG_SPACING * IMG_Y};
        double ssi[2] = {1.0 / ssz[0], 1.0 / ssz[1]};
        double isi[2] = {1.0 / isz[0], 1.0 / isz[1]};

        // i = 1 if screen is wider, 0 if screen is taller
        int i = ssz[0] * isz[1] > ssz[1] * isz[0], j = !i;
        sci[i] = (float)(2.0 * isi[i]);
        sci[j] = (float)(2.0 * isi[i] * ssz[i] * ssi[j]);
        isc[i] = (float)(0.5 * isz[i]);
        isc[j] = (float)(0.5 * isz[i] * ssi[i] * ssz[j]);
    }

    if (demo_image.prog_bg.prog) {
        sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "background");
        const struct demo_prog_background *restrict prog = &demo_image.prog_bg;

        glDisable(GL_BLEND);
        glUseProgram(prog->prog);
        glBindVertexArray(demo_image.arr[0]);

        double t = fmod(time / 8.0, NUM_TEX);
        if (t < 0.0) {
            t += NUM_TEX;
        }
        int texidx = (int)t;
        if (texidx >= NUM_TEX || texidx < 0) {
            texidx = 0;
        }
        float t2 = (float) fmod(t, 1.0);

        float ts1 =
            1.0f / (IMG_SPACING * IMG_X) *
            expf(logf(4.0f) * (0.75f + cosf((8.0f * atanf(1.0f)) * t2)));
        float ts[2] = { ts1 * isc[0], ts1 * isc[1] };
        float texmat[3][2] = {
            { ts[0], 0.0f }, { 0.0f, ts[1] }, { 0.0f, 0.0f }
        };
        glUniformMatrix3x2fv(prog->TexMat, 1, GL_FALSE, &texmat[0][0]);
        glUniform1i(prog->Texture, 0);
        float texsel[3] = {
            (float) texidx,
            (float) ((texidx + 1) % NUM_TEX),
            (float) fmod(t, 1.0)
        };
        glUniform3fv(prog->TexSel, 1, texsel);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D_ARRAY, demo_image.bgtex);

        glDrawArrays(GL_TRIANGLES, 0, 3);
        sg_gl_debugf.pop();
    }

    if (demo_image.prog_tx.prog) {
        sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "background");
        const struct demo_prog_textured *restrict prog = &demo_image.prog_tx;

        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        glUseProgram(prog->prog);
        glBindVertexArray(demo_image.arr[1]);

        float view[3][2] = {{sci[0], 0.0f}, {0.0f, sci[1]}, {0.0f, 0.0f}};
        glUniformMatrix3x2fv(prog->View, 1, GL_FALSE, &view[0][0]);
        glUniform1i(prog->Texture, 1);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D_ARRAY, demo_image.fgtex);

        glDrawArrays(GL_TRIANGLES, 0, NUM_IMG * 6);
        sg_gl_debugf.pop();
    }

    sg_gl_debugf.pop();
}

static void demo_image_handle_event(union sg_event *evt) {
    (void)evt;
}

const struct demo_screen DEMO_IMAGE = {demo_image_load, demo_image_draw,
                                       demo_image_handle_event, NULL};
