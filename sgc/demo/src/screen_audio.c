/* Copyright 2013-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "defs.h"
#include "sg/color.h"
#include "sg/event.h"
#include "sg/keycode.h"
#include "sg/mixer.h"
#include "sg/opengl.h"
#include "sggl/3_2.h"

#include <string.h>

enum {
    NUM_CHANNEL = 10,
};

enum {
    AUDIO_CLANK,
    AUDIO_DONK,
    AUDIO_TINK,
    AUDIO_STEREO,
    AUDIO_LEFT,
    AUDIO_RIGHT,
    AUDIO_MUSIC,
    AUDIO_ALIEN,

    NUM_AUDIO
};

static const char AUDIO_NAME[NUM_AUDIO][10] = {
    "clank1",
    "donk1",
    "tink1",
    "stereo",
    "left",
    "right",
    "looptrack",
    "alien"
};

static const float DATA[] = {
    -0.9f, -0.9f,
    -0.9f, +0.9f,
    +0.9f, +0.9f,
    +0.9f, -0.9f
};

struct demo_audio_chan {
    int state;
    int mod;
    struct sg_mixer_channel *chan;
};

struct demo_audio {
    struct demo_prog_plain prog;
    GLuint buffer;
    GLuint array;
    struct sg_mixer_sound *audio[NUM_AUDIO];
    unsigned cmd;
    struct demo_audio_chan chan[4];
};

static struct demo_audio demo_audio;

static bool demo_audio_load(void) {
    sg_gl_debugf.push(SG_GL_SOURCE_APP, 0, -1, "demo_audio_load");
    bool success = true;

    char path[16];
    for (int i = 0; i < NUM_AUDIO; i++) {
        strcpy(path, "/fx/");
        strcat(path, AUDIO_NAME[i]);
        demo_audio.audio[i] = sg_mixer_sound_file(path, NULL);
    }

    if (!demo_prog_plain_load(&demo_audio.prog)) {
        success = false;
    }

    glGenBuffers(1, &demo_audio.buffer);
    glBindBuffer(GL_ARRAY_BUFFER, demo_audio.buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(DATA), DATA, GL_STATIC_DRAW);

    glGenVertexArrays(1, &demo_audio.array);
    glBindVertexArray(demo_audio.array);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    sg_gl_debugf.pop();
    return success;
}

static bool get_state(int idx) {
    return (demo_audio.cmd & (1u << idx)) != 0;
}

static void demo_audio_drawkey(int key, int state) {
    int n = NUM_CHANNEL;
    const struct demo_prog_plain *p = &demo_audio.prog;
    if (!p->prog) {
        return;
    }
    glUniform2f(p->VertOff, 2*key+1-n, 1-n);
    glUniform4fv(p->Color, 1, sg_color_db32(state ? 8 : 3).v);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glUniform4fv(p->Color, 1, sg_color_db32(5).v);
    glDrawArrays(GL_LINE_LOOP, 0, 4);
}

static void demo_audio_draw1(double time) {
    struct demo_audio_chan *ch = &demo_audio.chan[0];
    int state = get_state(0);

    if (state && !ch->state) {
        struct sg_mixer_sound *snd = NULL;
        switch (ch->mod) {
        case 0: snd = demo_audio.audio[AUDIO_CLANK]; break;
        case 1: snd = demo_audio.audio[AUDIO_DONK];  break;
        case 2: snd = demo_audio.audio[AUDIO_TINK];  break;
        }
        ch->mod = (ch->mod + 1) % 3;
        sg_mixer_channel_play(snd, time, SG_MIXER_FLAG_DETACHED);
    }

    ch->state = state;
    demo_audio_drawkey(0, state);
}

static void demo_audio_draw2(double time) {
    struct demo_audio_chan *ch = &demo_audio.chan[1];
    int state = get_state(1);
    float pan = 0.0f;

    if (state && !ch->state) {
        struct sg_mixer_sound *snd = NULL;
        switch (ch->mod / 3) {
        case 0: snd = demo_audio.audio[AUDIO_CLANK]; break;
        case 1: snd = demo_audio.audio[AUDIO_DONK];  break;
        case 2: snd = demo_audio.audio[AUDIO_TINK];  break;
        }
        switch (ch->mod % 3) {
        case 0: pan = 0.0f; break;
        case 1: pan = -1.0f; break;
        case 2: pan = 1.0f; break;
        }
        ch->mod = (ch->mod + 1) % 9;
        ch->chan = sg_mixer_channel_play(snd, time, SG_MIXER_FLAG_DETACHED);
        sg_mixer_channel_setparam(ch->chan, SG_MIXER_PARAM_PAN, pan);
    }

    ch->state = state;
    demo_audio_drawkey(1, state);
}

static void demo_audio_draw3(double time) {
    struct demo_audio_chan *ch = &demo_audio.chan[2];
    int state = get_state(2);

    if (state && !ch->state) {
        struct sg_mixer_sound *snd = NULL;
        float pan = 0.0f;
        switch (ch->mod) {
        case 0: snd = demo_audio.audio[AUDIO_STEREO]; pan =  0.00f; break;
        case 1: snd = demo_audio.audio[AUDIO_LEFT];   pan = -0.75f; break;
        case 2: snd = demo_audio.audio[AUDIO_RIGHT];  pan = +0.75f; break;
        }
        ch->mod = (ch->mod + 1) % 3;
        ch->chan = sg_mixer_channel_play(snd, time, SG_MIXER_FLAG_DETACHED);
        sg_mixer_channel_setparam(ch->chan, SG_MIXER_PARAM_PAN, pan);
    }

    ch->state = state;
    demo_audio_drawkey(2, state);
}

static void demo_audio_draw4(double time) {
    struct demo_audio_chan *ch = &demo_audio.chan[3];
    int state = get_state(3);

    if (ch->chan && sg_mixer_channel_isdone(ch->chan)) {
        sg_mixer_channel_stop(ch->chan);
        ch->chan = NULL;
    }

    switch (ch->state & 3) {
    case 0:
        if (state) {
            struct sg_mixer_sound *snd =
                demo_audio.audio[(ch->state & 4) ? AUDIO_ALIEN : AUDIO_MUSIC];
            ch->chan = sg_mixer_channel_play(snd, time, SG_MIXER_FLAG_LOOP);
            ch->state++;
        }
        break;

    case 2:
        if (state) {
            sg_mixer_channel_stop(ch->chan);
            ch->chan = NULL;
            ch->state++;
        }
        break;

    default:
        if (!state) {
            ch->state++;
        }
        break;
    }

    ch->state &= 7;
    demo_audio_drawkey(3, state);
}

static void demo_audio_draw(ivec2 wsize, double time) {
    demo_clear_color(14);
    glViewport(0, 0, wsize.v[0], wsize.v[1]);
    glClear(GL_COLOR_BUFFER_BIT);

    const struct demo_prog_plain *restrict p = &demo_audio.prog;
    if (p->prog) {
        glUseProgram(p->prog);
        glBindVertexArray(demo_audio.array);
        float sc = (float)(1.0 / NUM_CHANNEL);
        glUniform2f(p->VertScale, sc, sc);
    }
    demo_audio_draw1(time);
    demo_audio_draw2(time);
    demo_audio_draw3(time);
    demo_audio_draw4(time);
}

static void demo_audio_handle_event(union sg_event *evt) {
    int cmd;
    switch (evt->common.type) {
    case SG_EVENT_KDOWN:
    case SG_EVENT_KUP:
        cmd = -1;
        switch (evt->key.key) {
        case KEY_1: cmd = 0; break;
        case KEY_2: cmd = 1; break;
        case KEY_3: cmd = 2; break;
        case KEY_4: cmd = 3; break;
        case KEY_5: cmd = 4; break;
        case KEY_6: cmd = 5; break;
        case KEY_7: cmd = 6; break;
        case KEY_8: cmd = 7; break;
        case KEY_9: cmd = 8; break;
        case KEY_0: cmd = 9; break;
        default: break;
        }
        if (cmd >= 0) {
            if (evt->key.type == SG_EVENT_KDOWN) {
                demo_audio.cmd |= 1u << cmd;
            } else {
                demo_audio.cmd &= ~(1u << cmd);
            }
        }
        break;

    default:
        break;
    }

}

const struct demo_screen DEMO_AUDIO = {demo_audio_load, demo_audio_draw,
                                       demo_audio_handle_event, NULL};
