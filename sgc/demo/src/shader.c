/* Copyright 2015-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "defs.h"
#include "sg/shader.h"
#include "sggl/3_2.h"

#include <string.h>

#define F(name) {  "" #name,       offsetof(TYPE, name) }
#define A(name) {  "" #name "[0]", offsetof(TYPE, name) }

struct demo_shader_spec {
    const char *name;
    GLenum type;
};

struct demo_prog_spec {
    const char *name;
    const struct demo_shader_spec *shader;
    const struct sg_shader_loc *attrib;
    const struct sg_shader_loc *uniform;
};

static bool demo_prog_load(const struct demo_prog_spec *spec, void *p) {
    bool success = false;
    GLuint prog = 0, *progp = p;
    int r;
    prog = glCreateProgram();
    for (int i = 0; spec->shader[i].name; i++) {
        char buf[64];
        strcpy(buf, "/shader/");
        strcat(buf, spec->shader[i].name);
        GLuint s = sg_shader_file(buf, spec->shader[i].type, NULL);
        if (!s) {
            goto done;
        }
        glAttachShader(prog, s);
        glDeleteShader(s);
    }
    r = sg_shader_link(prog, spec->name, spec->attrib, NULL);
    if (r) goto done;
    sg_shader_getuniform(prog, spec->name, p, spec->uniform);
    success = true;
done:
    if (success) {
        *progp = prog;
        return true;
    } else {
        *progp = 0;
        glDeleteProgram(prog);
        return false;
    }
}

#define TYPE struct demo_prog_plain
bool demo_prog_plain_load(struct demo_prog_plain *p) {
    static const struct demo_shader_spec shader[] = {
        {"plain.vert", GL_VERTEX_SHADER},
        {"plain.frag", GL_FRAGMENT_SHADER},
        {NULL, 0},
    };
    static const struct sg_shader_loc attrib[] = {
        {"Loc", 0}, {NULL, 0},
    };
    static const struct sg_shader_loc uniform[] = {
        F(VertOff), F(VertScale), {NULL, 0},
    };
    static const struct demo_prog_spec spec = {"plain", shader, attrib,
                                               uniform};
    return demo_prog_load(&spec, p);
}
#undef TYPE

#define TYPE struct demo_prog_background
bool demo_prog_background_load(struct demo_prog_background *p) {
    static const struct demo_shader_spec shader[] = {
        {"background.vert", GL_VERTEX_SHADER},
        {"background.frag", GL_FRAGMENT_SHADER},
        {NULL, 0},
    };
    static const struct sg_shader_loc attrib[] = {
        {"Loc", 0}, {NULL, 0},
    };
    static const struct sg_shader_loc uniform[] = {
        F(TexMat), F(Texture), F(TexSel), {NULL, 0},
    };
    static const struct demo_prog_spec spec = {"background", shader, attrib,
                                               uniform};
    return demo_prog_load(&spec, p);
}
#undef TYPE

#define TYPE struct demo_prog_textured
bool demo_prog_textured_load(struct demo_prog_textured *p) {
    static const struct demo_shader_spec shader[] = {
        {"textured.vert", GL_VERTEX_SHADER},
        {"textured.frag", GL_FRAGMENT_SHADER},
        {NULL, 0},
    };
    static const struct sg_shader_loc attrib[] = {
        {"Loc", 0}, {"TexCoord", 1}, {NULL, 0},
    };
    static const struct sg_shader_loc uniform[] = {
        F(View), F(Texture), {NULL, 0},
    };
    static const struct demo_prog_spec spec = {"textured", shader, attrib,
                                               uniform};
    return demo_prog_load(&spec, p);
}
#undef TYPE

#define TYPE struct demo_prog_text
bool demo_prog_text_load(struct demo_prog_text *p) {
    static const struct demo_shader_spec shader[] = {
        {"text2.vert", GL_VERTEX_SHADER},
        {"text2.geom", GL_GEOMETRY_SHADER},
        {"text2.frag", GL_FRAGMENT_SHADER},
        {NULL, 0},
    };
    static const struct sg_shader_loc attrib[] = {
        {"VertPos", 0}, {"TexPos", 1}, {"Style", 2}, {NULL, 0},
    };
    static const struct sg_shader_loc uniform[] = {
        F(Colors), F(VertOff), F(VertScale), F(TexScale), F(Texture), {NULL, 0},
    };
    static const struct demo_prog_spec spec = {"text", shader, attrib, uniform};
    return demo_prog_load(&spec, p);
}
#undef TYPE
