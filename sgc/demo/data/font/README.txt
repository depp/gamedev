Fonts
=====

Roboto and Noto Serif are distributed under the terms of the Apache
license, see APACHE.txt.  Homenaje is distributed under the terms of the
SIL Open Font License, see OFL.txt.

These fonts were taken from the Google font directory.  Roboto and
Homenaje are Latin subsets, and Noto Serif is a Greek subset.  The goal
is to test font selection and fallback.
