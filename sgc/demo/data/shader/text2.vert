#version 150

in vec4 VertPos;
in vec4 TexPos;
in vec4 Style;

out Geom {
    vec4 TexPos;
    vec4 Color;
} Out;

uniform vec4 Colors[32];

void main() {
    Out.TexPos = TexPos;
    Out.Color = Colors[int(Style.x)];
    gl_Position = VertPos;
}
