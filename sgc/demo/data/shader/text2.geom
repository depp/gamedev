#version 150

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in Geom {
    vec4 TexPos;
    vec4 Color;
} In[];

out Frag {
    vec2 TexPos;
    vec2 QuadPos;
    flat vec4 Color;
} Out;

uniform vec2 VertOff;
uniform vec2 VertScale;
uniform vec2 TexScale;

void main() {
    vec4 vpos = gl_in[0].gl_Position;
    vec4 tpos = In[0].TexPos * vec4(TexScale, TexScale);
    vec4 color = In[0].Color;

    Out.TexPos = tpos.xy;
    Out.QuadPos = vec2(0.0, 0.0);
    Out.Color = color;
    gl_Position = vec4((vpos.xy + VertOff) * VertScale, 0.0, 1.0);
    EmitVertex();

    Out.TexPos = tpos.zy;
    Out.QuadPos = vec2(1.0, 0.0);
    Out.Color = color;
    gl_Position = vec4((vpos.zy + VertOff) * VertScale, 0.0, 1.0);
    EmitVertex();

    Out.TexPos = tpos.xw;
    Out.QuadPos = vec2(0.0, 1.0);
    Out.Color = color;
    gl_Position = vec4((vpos.xw + VertOff) * VertScale, 0.0, 1.0);
    EmitVertex();

    Out.TexPos = tpos.zw;
    Out.QuadPos = vec2(1.0, 1.0);
    Out.Color = color;
    gl_Position = vec4((vpos.zw + VertOff) * VertScale, 0.0, 1.0);
    EmitVertex();

    EndPrimitive();
}
