#version 150

in vec2 Loc;

uniform vec2 VertOff;
uniform vec2 VertScale;

void main() {
    gl_Position = vec4((Loc + VertOff) * VertScale, 0.0, 1.0);
}
