#version 150
precision highp float;

in Frag {
    vec2 TexPos;
    vec2 QuadPos;
    flat vec4 Color;
} In;

out vec4 OutColor;

uniform sampler2D Texture;

float borderDistance(vec2 v) {
    mat2 m = mat2(dFdx(v), dFdy(v));
    vec2 p = min(v, vec2(1.0) - v);
    // bool t = all(greaterThan(abs(inverse(m) * p), vec2(1.0)));
    vec2 s = (m[0] * m[0] + m[1] * m[1]).yx;
    vec2 t = abs(s * p * p);
    return sqrt(min(t.x, t.y)) / determinant(m);
}
/*
void main() {
    float b = borderDistance(In.QuadPos);
    float a = texture(Texture, In.TexPos).r;
    vec4 fg = a * In.Color;
    vec4 bg = mix(vec4(0.0), vec4(0.1), b < 1.0);
    OutColor = bg * (1.0 - a) + fg;
}
*/
void main() {
    float a = texture(Texture, In.TexPos).r;
    OutColor = a * In.Color;
}
