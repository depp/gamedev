# SGC

SGC is a framework, written in C, for making games.
SGC manages the rendering context, image loading, audio decoding, text rendering, and other common tasks.
A lot of the functionality in this framework is redundant with other frameworks like LibSDL, especially if you include related libraries like SDL\_image and SDL\_mixer.

Also refer to the SGPP project, which is very similar, but written in C++.  Both projects share common ancestry.

## Address Sanitizer Notes

You may want to set some options for the address sanitizer:

    export ASAN_OPTIONS=abort_on_error=true:leak_check_at_exit=false
