/* Copyright 2013-2016 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */

#include "sg/pack.h"
#include "sg/rand.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static struct sg_rand_state g_rand = {123456789, 987654321, 9999999};

static void die(const char *msg) {
    fprintf(stderr, "Error: %s\n", msg);
    exit(1);
}

static int randi(int min, int max) {
    if (min >= max) return min;
    unsigned v = (unsigned)-1 / (max + 1 - min);
    while (1) {
        int x = sg_irand(&g_rand) / v;
        if (x < max + 1 - min) return min + x;
    }
}

__attribute__((unused))
static int ispow2(int x) {
    return x > 0 && (x & (x - 1)) == 0;
}

static void test_pack(int count, int rsz, int iter, int perfiter) {
    (void)perfiter;
    ivec2 min_size, max_size;
    min_size.v[0] = 16;
    min_size.v[1] = 16;
    max_size.v[0] = 4096;
    max_size.v[1] = 4096;

    struct sg_packing_rect *rect;
    rect = malloc(count * sizeof(*rect));
    if (!rect) die("Out of memory");

    struct sg_packing p;
    sg_packing_init(&p);
#if defined PERFTEST
    double time = 0.0;
    double efficiency = 0.0;
#endif
    for (int iidx = 0; iidx < iter; iidx++) {
        unsigned rect_area = 0;
        for (int i = 0; i < count; i++) {
            int s = randi(1, rsz);
            rect[i].w = randi((s + 3) / 4, s);
            rect[i].h = randi((s + 3) / 4, s);
            rect[i].x = 0xffff;
            rect[i].y = 0xffff;
            rect_area += (unsigned)rect[i].w * (unsigned)rect[i].h;
        }
        rect[randi(0, count - 1)].w = 0;
        rect[randi(0, count - 1)].h = 0;

#if defined PERFTEST
        struct timespec t1, t2;
        clock_gettime(CLOCK_MONOTONIC, &t1);
        for (int i = 0; i < perfiter; i++) {
            int r = sg_packing_autopack(&p, rect, count, min_size, max_size);
            if (r < 0) die("Out of memory");
            if (r == 0) die("Packing failed");
        }
        clock_gettime(CLOCK_MONOTONIC, &t2);
        time += (double)(t2.tv_sec - t1.tv_sec) +
                (double)(t2.tv_nsec - t1.tv_nsec) * 1e-9;
        unsigned pack_area = (unsigned)p.size.v[0] * (unsigned)p.size.v[1];
        efficiency += (double)rect_area / (double)pack_area;
#else
        int r = sg_packing_autopack(&p, rect, count, min_size, max_size);
        if (r < 0) die("Out of memory");
        if (r == 0) die("Packing failed");

        int sx = p.size.v[0], sy = p.size.v[1];
        if (!ispow2(sx) || !ispow2(sy)) die("Invalid packing size");
        for (int i = 0; i < count; i++) {
            const struct sg_packing_rect r = rect[i];
            if (r.w > sx || r.h > sy || r.x > sx - r.w || r.y > sy - r.h)
                die("Invalid rectangle position");
            for (int j = 0; j < i; j++) {
                const struct sg_packing_rect rr = rect[j];
                if (r.x < rr.x + rr.w && r.y < rr.y + rr.h &&
                    rr.x < r.x + r.w && rr.y < r.y + r.h)
                    die("Collision");
            }
        }

        unsigned pack_area = (unsigned)p.size.v[0] * (unsigned)p.size.v[1];
        printf("Rects: %d; size: %d x %d; efficiency: %0.4f\n", count,
               p.size.v[0], p.size.v[1], (double)rect_area / (double)pack_area);
#endif
    }

    sg_packing_destroy(&p);
    free(rect);

#if defined PERFTEST
    printf(
        "Rects: %d\n"
        "Rect size: %d\n"
        "Mean efficiency: %0.4f\n"
        "Time per rect: %0.6f us\n"
        "Time per packing: %0.3f ms\n"
        "\n",
        count, rsz, efficiency / (double)iter,
        1e6 * time / ((double)iter * perfiter * count),
        1e3 * time / ((double)iter * perfiter));
#endif
}

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    test_pack(100, 50, 50, 1000);
    test_pack(1000, 8, 50, 100);
    test_pack(10000, 32, 10, 10);

    return 0;
}
