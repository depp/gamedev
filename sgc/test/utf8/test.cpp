/* Copyright 2015 Dietrich Epp.
   This file is part of SGLib.  SGLib is licensed under the terms of
   the MIT license.  For more information, see LICENSE.txt. */
#include "sgpp/utf8.hpp"
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <vector>

__attribute__((noreturn))
void die(const char *why) {
    std::fprintf(stderr, "Error: %s\n", why);
    std::exit(1);
}

void read_all(void *ptr, std::size_t size) {
    char *up = static_cast<char *>(ptr);
    std::size_t pos = 0;
    while (pos < size) {
        std::size_t amt = std::fread(up + pos, 1, size - pos, stdin);
        if (amt == 0) {
            if (std::feof(stdin)) {
                die("unexpected eof");
            }
            die("read error");
        }
        pos += amt;
    }
}

int main(int argc, char *argv[]) {
    (void) argc;
    (void) argv;

    unsigned insize, outsize;
    {
        unsigned sizes[2];
        read_all(sizes, sizeof(sizes));
        insize = sizes[0];
        outsize = sizes[1];
    }

    char *indata = new char[insize];
    read_all(indata, insize);

    char32_t *outdata = new char32_t[outsize];
    read_all(outdata, outsize * sizeof(*outdata));

    std::vector<char32_t> out;
    SG::Text::utf8_decode(out, indata, insize);

    std::size_t n = std::min(static_cast<std::size_t>(insize), out.size());
    int lineno = 1, errors = 0;
    for (std::size_t i = 0; i < n; i++) {
        if (outdata[i] != out[i]) {
            std::fprintf(stderr, "Line %d: expected %08X, got %08X\n",
                         lineno, outdata[i], out[i]);
            errors++;
            if (errors >= 10) {
                return 1;
            }
        }
        if (out[i] == '\n') {
            lineno++;
        }
    }
    if (out.size() != outsize) {
        std::fputs("Size mismatch\n", stderr);
        errors++;
    }

    return errors == 0 ? 0 : 1;
}
