#!/usr/bin/env python3
# Copyright 2015 Dietrich Epp.
# This file is part of SGLib.  SGLib is licensed under the terms of
# the MIT license.  For more information, see LICENSE.txt.
from base64 import b16decode
import io
import subprocess
import struct
import sys

def decode(s):
    fp = io.BytesIO()
    for x in s:
        fp.write(b' '.join(b16decode(y) for y in x.split()) + b'\n')
    return fp.getvalue()

TESTS = []

def test(func):
    TESTS.append(func)
    return func

def run_all():
    for test in TESTS:
        print(test.__name__)
        result = test()
        if not result:
            print('FAILED:', test.__name__)
            raise SystemExit(1)
    print('ok')

ENC = {'little': 'UTF-32LE', 'big': 'UTF-32BE'}

def run_test(indata, outdata):
    if not isinstance(indata, bytes):
        raise TypeError('input data must be bytes')
    if not isinstance(outdata, str):
        raise TypeError('output data must be string')
    fp = io.BytesIO()
    outdata2 = outdata.encode(ENC[sys.byteorder])
    fp.write(struct.pack('II', len(indata), len(outdata2) // 4))
    fp.write(indata)
    fp.write(outdata2)
    proc = subprocess.Popen(
        ['./utf8'],
        stdin=subprocess.PIPE)
    stdout, stderr = proc.communicate(fp.getvalue())
    if proc.returncode != 0:
        return False
    return True

HARD_VALID = '''\
utf-8
\x00 \x7f - one-byte
\u0080 \u07ff - two-byte
\u0800 \ud7ff \ue000 \uffff - three-byte
\U00010000 \U0010FFFF - four-byte
'''

@test
def hard_valid():
    return run_test(HARD_VALID.encode('UTF-8'), HARD_VALID)

# Credit due to Markus Kuhn for the original UTF-8 test

HARD_INVALID_IN = decode([
    # Unexpected continuation characters
    '8081828384858687',
    '88898A8B8C8D8E8F',
    '9091929394959697',
    '98999A9B9C9D9E9F',
    'A0A1A2A3A4A5A6A7',
    'A8A9AAABACADAEAF',
    'B0B1B2B3B4B5B6B7',
    'B8B9BABBBCBDBEBF',
    # Lonely start characters for two-char sequences
    'C0 C1 C2 C3 C4 C5 C6 C7',
    'C8 C9 CA CB CC CD CE CF',
    'D0 D1 D2 D3 D4 D5 D6 D7',
    'D8 D9 DA DB DC DD DE DF',
    # Lonely start characters for three-char sequences
    'E0 E1 E2 E3 E4 E5 E6 E7',
    'E8 E9 EA EB EC ED EE EF',
    # Lonely start characters for four-char sequences
    'F0 F1 F2 F3 F4 F5 F6 F7',
    # Bytes that are always invalid
    'F8 F9 FA FB FC FD FE FF',
    'FEFF FFFE FFFF FEFE',
    # Incomplete sequences
    'C2 DF E0A0 ED9F EE80 EFBF F09080 F48FBF',
    'C2DFE0A0ED9FEE80EFBFF09080F48FBF',
    # Overlong sequences
    'C080 C1BF',
    'E08080 E09FBF',
    'F0808080 F08FBFBF',
    # Single surrogates
    'EDA080 EDAFBF EDB080 EDBFBF',
    # Surrogate pairs
    'EDA080EDB080 EDA0B4EDB487',
])

HARD_INVALID_OUT = '\n'.join([
    # Unexpected continuation characters
    'xxxxxxxx',
    'xxxxxxxx',
    'xxxxxxxx',
    'xxxxxxxx',
    'xxxxxxxx',
    'xxxxxxxx',
    'xxxxxxxx',
    'xxxxxxxx',
    # Lonely start characters for two-char sequences
    'x x x x x x x x',
    'x x x x x x x x',
    'x x x x x x x x',
    'x x x x x x x x',
    # Lonely start characters for three-char sequences
    'x x x x x x x x',
    'x x x x x x x x',
    # Lonely start characters for four-char sequences
    'x x x x x x x x',
    # Bytes that are always invalid
    'x x x x x x x x',
    'xx xx xx xx',
    # Incomplete sequences
    'x x x x x x x x',
    'xxxxxxxx',
    # Overlong sequences
    'x x',
    'x x',
    'x x',
    # Single surrogates
    'x x x x',
    # Surrogate pairs
    'xx xx',
]).replace('x', '\ufffd') + '\n'

@test
def hard_invalid():
    return run_test(HARD_INVALID_IN, HARD_INVALID_OUT)

if __name__ == '__main__':
    run_all()
