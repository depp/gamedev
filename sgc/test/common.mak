LIBS :=
LDFLAGS :=
ifdef PERFTEST
CFLAGS := -O2 -DPERFTEST
else
ifdef RELEASE
CFLAGS := -O2
else
CFLAGS := -O0 -g
endif
endif
CWARN := -Werror -Wall -Wextra -Wpointer-arith -Wwrite-strings -Wmissing-prototypes

UNAME_S := $(shell uname -s)

ifndef PERFTEST
ifeq ($(UNAME_S),Linux)
ASAN := 1
endif
endif

ifeq ($(UNAME_S),Darwin)
override LIBS += -framework ApplicationServices
endif

ifdef ASAN
override CFLAGS += -fsanitize=address
override LDFLAGS += -fsanitize=address
endif

%.o: %.c
	$(CC) -MF $*.d -MMD -MP -I../.. -I../../include $(CWARN) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<

-include *.d

link = $(CC) $(LDFLAGS) -o $@ $^ $(LIBS)
