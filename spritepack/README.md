Sprite Packer
=============

This is a sprite packer.  It packs an arbitrary number of images into
a single image.

Currently, it only works offline.  It uses algorithms described in
Jukka Jylänki's 2010 paper, "A Thousand Ways to Pack the Bin - A
Practical Approach to Two-Dimensional Rectangle Bin Packing".
