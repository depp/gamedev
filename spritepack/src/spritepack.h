#ifndef SPRITEPACK_H
#define SPRITEPACK_H
#ifdef __cplusplus
extern "C" {
#endif

/* ========================================
   Low level packing interface
   ======================================== */

/* Result codes for the sprite packer */
enum {
    /* Sprite packing succeeded */
    SPRITEPACK_SUCCESS,

    /* The sprite packer ran out of memory */
    SPRITEPACK_NOMEM,

    /* The sprite packer could not find a solution */
    SPRITEPACK_FAILURE,

    /* A dimension is too large */
    SPRITEPACK_TOOLARGE
};

/* Function for packing sprites in a rectangle.  Produces sprite
   locations as an output, takes sprite sizes and enclosing rectangle
   size as an input.  Returns one of the result codes above.  */
typedef int (*spritepack_func_t)(
    const unsigned short sprite_size[][2], unsigned short sprite_loc[][2],
    unsigned sprite_count, unsigned width, unsigned height);

/* MAXRECTS-BSSF-GLOBAL algorithm, O(|F|^2 n^2) running time */
int spritepack_maxrects_bssf_global(
    const unsigned short sprite_size[][2], unsigned short sprite_loc[][2],
    unsigned sprite_count, unsigned width, unsigned height);

/* MAXRECTS-BSSF-DESCSS algorithm, O(|F|^2 n) running time */
int spritepack_maxrects_bssf_descss(
    const unsigned short sprite_size[][2], unsigned short sprite_loc[][2],
    unsigned sprite_count, unsigned width, unsigned height);

/* GUILLOTINE-BSSF-SAS-RM-DESCSS, O(n^3 log n) running time */
int spritepack_guillotine_bssf_sas_rm_descss(
    const unsigned short sprite_size[][2], unsigned short sprite_loc[][2],
    unsigned sprite_count, unsigned width, unsigned height);

/* Options for the sprite packer */
struct spritepack_options {
    /* Maximum dimensions of output texture */
    int max_width;
    int max_height;

    /* Allow non-power-of-two output textures */
    int allow_npot;
};

/* Find the smallest rectangle which can contain the given rectangles.
   Chooses rectangles according to the options specified (e.g., power
   of two dimensions only).  */
int spritepack_minrect(
    struct spritepack_options *opts, spritepack_func_t func,
    const unsigned short sprite_size[][2], unsigned short sprite_loc[][2],
    unsigned sprite_count, unsigned *widthp, unsigned *heightp);

/* ========================================
   High level packing interface
   ======================================== */

struct spritepack_args_sprite {
    char *file;
    char *name;
    int width;
    int height;
    int x;
    int y;
};

struct spritepack_args {
    struct args_sprite *sprite;
    size_t spritecount;
    size_t spritealloc;

    char *output;
};

void spritepack_args_init(struct spritepack_args *args);
q
void spritepack_args_destroy(struct spritepack_args *args);

static int spritepack_args_addsprite(
    struct spritepack_args *args,
    const char *file, const char *name);

#ifdef __cplusplus
}
#endif
#endif
