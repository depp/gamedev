#include "spritepack.h"
#include <stdlib.h>
#include <string.h>

int spritepack_minrect(
    struct spritepack_options *opts, spritepack_func_t func,
    const unsigned short sprite_size[][2], unsigned short sprite_loc[][2],
    unsigned sprite_count, unsigned *widthp, unsigned *heightp)
{
    unsigned min_size, i, psize, ssize, best_size, size;
    unsigned short min_width, min_height, max_width, max_height;
    unsigned short (*temp_loc)[2], width, height;
    unsigned short best_width, best_height, best_s0, s0;
    unsigned nwidth, nheight;
    int r;

    min_size = 0;
    min_width = 0;
    min_height = 0;
    for (i = 0; i < sprite_count; i++) {
        ssize = (unsigned) sprite_size[i][0] *
            (unsigned) sprite_size[i][1];
        psize = min_size;
        min_size += ssize;
        if (min_size < psize)
            return SPRITEPACK_FAILURE;
        if (min_width > sprite_size[i][0])
            min_width = sprite_size[i][0];
        if (min_height > sprite_size[i][1])
            min_height = sprite_size[i][1];
    }

    if (opts->max_width > 0 && opts->max_width < (unsigned short) -1)
        max_width = (unsigned short) opts->max_width;
    else
        max_width = (unsigned short) - 1;

    if (opts->max_height > 0 && opts->max_height < (unsigned short) -1)
        max_height = (unsigned short) opts->max_height;
    else
        max_height = (unsigned short) - 1;

    if (min_width > max_width ||
        min_height > max_height)
        return SPRITEPACK_FAILURE;

    temp_loc = malloc(sizeof(*temp_loc) * sprite_count);
    if (!temp_loc)
        return SPRITEPACK_NOMEM;

    best_size = (unsigned) -1;
    best_width = (unsigned short) -1;
    best_height = (unsigned short) -1;
    best_s0 = (unsigned short) -1;
    for (nwidth = 0; nwidth < 16; nwidth++) {
        width = (unsigned short) (1u << nwidth);
        if (width < min_width || width > max_width)
            continue;
        for (nheight = 0; nheight < 16; nheight++) {
            height = (unsigned short) (1u << nheight);
            if (height < min_height || height > max_height)
                continue;
            size = (unsigned) width * (unsigned) height;
            s0 = width > height ? width : height;
            if (size > best_size ||
                (size == best_size && s0 > best_s0))
                break;
            if (size < min_size)
                continue;
            r = func(sprite_size, temp_loc, sprite_count, width, height);
            switch (r) {
            case SPRITEPACK_SUCCESS:
                best_size = size;
                best_width = width;
                best_height = height;
                best_s0 = s0;
                memcpy(sprite_loc, temp_loc,
                       sizeof(*sprite_loc) * sprite_count);
                break;

            case SPRITEPACK_FAILURE:
            case SPRITEPACK_TOOLARGE:
                break;

            default:
                free(temp_loc);
                return r;
            }
        }
    }

    free(temp_loc);
    if (best_size != (unsigned) -1) {
        *widthp = best_width;
        *heightp = best_height;
        return SPRITEPACK_SUCCESS;
    } else {
        return SPRITEPACK_FAILURE;
    }
}
