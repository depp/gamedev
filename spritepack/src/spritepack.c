#include <assert.h>
#include <stdio.h>
#include <string.h>

void args_init(struct args *args)
{
    args->sprite = NULL;
    args->spritecount = 0;
    args->spritealloc = 0;
    args->output = NULL;
}

void args_destroy(struct args *args)
{
    struct args_sprite *sp, *se;
    for (sp = args->sprite, se = sp + args->spritecount; sp != se; sp++) {
        free(sp->file);
        free(sp->name);
    }
    free(args->sprite);
    free(args->output);
}

static int args_addsprite(struct args *args,
                          const char *file, const char *name)
{
    size_t nalloc;
    struct args_sprite *narr, *p;
    char *filep, *namep;
    size_t filelen, namelen;
    if (args->spritecount >= args->spritealloc) {
        nalloc = args->spritealloc ? args->spritealloc * 2 : 4;
        if (!nalloc)
            return SPRITEPACK_NOMEM;
        narr = realloc(args->sprite, sizeof(*args->sprite) * nalloc);
        if (!narr)
            return SPRITEPACK_NOMEM;
        args->sprite = narr;
        args->spritealloc = nalloc;
    }

    assert(file != NULL);
    filelen = strlen(file);
    filep = malloc(filelen + 1);
    if (!filep)
        return SPRITEPACK_NOMEM;
    memcpy(filep, file, filelen + 1);

    if (name) {
        namelen = strlen(name);
        namep = malloc(namelen + 1);
        if (!namep) {
            free(filep);
            return SPRITEPACK_NOMEM;
        }
        memcpy(namep, name, namelen + 1);
    }

    p = args->sprite[args->spritecount++];
    p->file = filep;
    p->name = namep;
    p->width = -1;
    p->height = -1;
    return 0;
}
