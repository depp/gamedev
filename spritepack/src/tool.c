#include "spritepack.h"


static void error_nomem(void)
{
    fputs("error: out of memory\n", stderr);
    exit();
}

int main(int argc, char *argv[])
{
    struct args args;
    int i, r;

    args_init(&args);

    for (i = 1; i < argc; i++) {
        if (argv[i][0] != '-') {
            r = args_addsprite(&args, argv[i], NULL);
            assert(!r);
        }
    }
}
