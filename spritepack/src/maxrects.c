#include "spritepack.h"
#include <stdlib.h>

struct spritepack_maxrects_rect {
    unsigned short x0, x1, y0, y1;
};

struct spritepack_maxrects {
    struct spritepack_maxrects_rect *free;
    unsigned freecount;
    unsigned freealloc;
};

static int spritepack_maxrects_init(
    struct spritepack_maxrects *sp, unsigned width, unsigned height)
{
    struct spritepack_maxrects_rect *rp;
    if (width > (unsigned short) -1 || height > (unsigned short) -1)
        return SPRITEPACK_TOOLARGE;
    rp = malloc(sizeof(*rp) * 4);
    if (!rp)
        return SPRITEPACK_NOMEM;
    sp->free = rp;
    sp->freecount = 1;
    sp->freealloc = 4;
    rp[0].x0 = 0;
    rp[0].x1 = (unsigned short) width;
    rp[0].y0 = 0;
    rp[0].y1 = (unsigned short) height;
    return 0;
}

static int spritepack_maxrects_add(
    struct spritepack_maxrects *sp,
    unsigned short x0, unsigned short x1,
    unsigned short y0, unsigned short y1)
{
    struct spritepack_maxrects_rect r, *rp, *re, *rnew;
    unsigned i, j, n, m, alloc, nalloc;
    unsigned short ix0, ix1, iy0, iy1;

    /* Split all rectangles intersecting the sprite */
    rp = sp->free;
    m = n = sp->freecount;
    alloc = sp->freealloc;
    for (i = 0; i < n; i++) {
        r = rp[i];
        ix0 = (x0 > r.x0) ? x0 : r.x0;
        ix1 = (x1 < r.x1) ? x1 : r.x1;
        iy0 = (y0 > r.y0) ? y0 : r.y0;
        iy1 = (y1 < r.y1) ? y1 : r.y1;
        if (!(ix0 < ix1 && iy0 < iy1))
            continue;
        if (m + 4 > alloc) {
            nalloc = alloc * 2;
            while (nalloc && m + 4 > nalloc)
                nalloc *= 2;
            if (!nalloc)
                return SPRITEPACK_NOMEM;
            rnew = realloc(rp, sizeof(*rp) * nalloc);
            if (!rnew)
                return SPRITEPACK_NOMEM;
            sp->free = rp = rnew;
            sp->freealloc = alloc = nalloc;
        }
        for (j = 0; j < 4; j++)
            rp[m+j] = r;
        if (x0 > r.x0)
            rp[m++].x1 = x0;
        if (x1 < r.x1)
            rp[m++].x0 = x1;
        if (y0 > r.y0)
            rp[m++].y1 = y0;
        if (y1 < r.y1)
            rp[m++].y0 = y1;
        rp[i].x0 = (unsigned short) -1;
    }

    /* Remove any rectangles contained in others */
    for (i = 0; i < m - 1; i++) {
        r = rp[i];
        if (r.x0 == (unsigned short) -1)
            continue;
        for (j = i + 1; j < m; j++) {
            if (rp[j].x0 == (unsigned short) -1)
                continue;
            if (r.x0 >= rp[j].x0 &&
                r.x1 <= rp[j].x1 &&
                r.y0 >= rp[j].y0 &&
                r.y1 >= rp[j].y1)
            {
                rp[i].x0 = (unsigned short) -1;
                break;
            }
            if (r.x0 <= rp[j].x0 &&
                r.x1 >= rp[j].x1 &&
                r.y0 <= rp[j].y0 &&
                r.y1 >= rp[j].y1)
            {
                rp[j].x0 = (unsigned short) -1;
                continue;
            }
        }
    }

    /* Compact the array */
    for (i = 0; i < m; i++)
        if (rp[i].x0 == (unsigned short) -1)
            break;
    j = 0;
    for (; i < m; i++) {
        if (rp[i].x0 == (unsigned short) -1)
            continue;
        rp[j] = rp[i];
        j++;
    }
    sp->freecount = j;
}

int spritepack_maxrects_bssf_global(
    const unsigned short sprite_size[][2], unsigned short sprite_loc[][2],
    unsigned sprite_count, unsigned width, unsigned height)
{
    struct spritepack_maxrects_rect *rp;
    unsigned sprite_rem, i, j, n, best_rect, best_sprite;
    unsigned short sw, sh, rw, rh, ss0, ss1, best_ss0, best_ss1, t;
    unsigned short x0, x1, y0, y1;
    struct spritepack_maxrects sp;
    int r;
    r = spritepack_maxrects_init(&sp, width, height);
    if (r)
        return r;

    for (i = 0; i < sprite_count; i++)
        sprite_loc[i][0] = (unsigned short) -1;

    for (sprite_rem = sprite_count; sprite_rem > 0; sprite_rem--) {
        /* Find best freerect, best sprite */
        rp = sp.free;
        n = sp.freecount;
        best_rect = -1;
        best_sprite = -1;
        best_ss0 = (unsigned short) -1;
        best_ss1 = (unsigned short) -1;
        for (i = 0; i < sprite_count; i++) {
            if (sprite_loc[i][0] != (unsigned short) -1)
                continue;
            sw = sprite_size[i][0];
            sh = sprite_size[i][1];
            for (j = 0; j < n; j++) {
                rw = rp[i].x1 - rp[i].x0;
                rh = rp[i].y1 - rp[i].y0;
                if (sw > rw || sh > rw)
                    continue;
                ss0 = rw - sw;
                ss1 = rh - sh;
                if (ss0 > ss1) {
                    t = ss0;
                    ss0 = ss1;
                    ss1 = t;
                }
                if (ss0 < best_ss0 || ss0 == best_ss0 && ss1 < best_ss1) {
                    ss0 = best_ss0;
                    ss1 = best_ss1;
                    best_rect = j;
                    best_sprite = i;
                }
            }
        }
        if (best_sprite == (unsigned) -1) {
            free(sp.free);
            return SPRITEPACK_FAILURE;
        }

        /* Split rect */
        x0 = sprite_loc[best_sprite][0] = rp[best_rect].x0;
        x1 = x0 + sprite_size[best_sprite][0];
        y0 = sprite_loc[best_sprite][1] = rp[best_rect].y0;
        y1 = y0 + sprite_size[best_sprite][1];
        r = spritepack_maxrects_add(&sp, x0, x1, y0, y1);
        if (r) {
            free(sp.free);
            return r;
        }
    }

    free(sp.free);
    return SPRITEPACK_SUCCESS;
}
