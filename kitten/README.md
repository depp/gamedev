# Kitten Teleporter

Kitten Teleporter is a game engine targeting WebGL.

## Development

The build script is written in Python.  In addition to Python 3, some third-party modules are required:

* NumPy
* SciPy
* Pillow
* FreeType-Py

The main application is written in TypeScript.  To get set up, run the following commands:

	npm install
	./node_modules/.bin/typings install

To run the development server:

    python -m tools serve

This will create a web server listening at http://localhost:8000/ which will automatically rebuild scripts and assets as necessary.

To build a package for deployment:

    python -m tools package

This will create a file named something like `my-app-name-1.0.tar.gz` which contains all the files necessary for deploying to a web server.

## Making a game

To build a game with this engine, fork the repo and start making changes.  Yes, that's a messy way to do things, but it works.  Remember to change the info in `config.yaml` to reflect your project.
