# Copyright 2016 Dietrich Epp.
#
# This file is part of Kitten Teleporter.  The Kitten Teleporter source
# code is distributed under the terms of the MIT license.
# See LICENSE.txt for details.
import collections
import io
import numpy
import os
import scipy.ndimage
from . import tilemap

class LevelError(Exception):
    pass

def convert_tile_data(arr):
    """Convert tile data to a string."""
    fp = io.StringIO()
    arr2 = numpy.array(arr)
    arr2 &= tilemap.TILE_MASK
    arr2 <<= 3
    arr2 |= arr >> (32 - 3)
    h, w = arr.shape
    for y in range(h):
        for x in range(w):
            fp.write(',')
            v = arr2[y,x]
            if v >= 8:
                fp.write(format(v - 8, 'x'))
    if not w or not h:
        return ''
    return fp.getvalue()[1:]

def make_mesh(layer):
    """Convert a tile layer to a mesh object (as a string)."""
    return '{0[0]:x},{0[1]:x}:{1}'.format(
        layer.size, convert_tile_data(layer.data))

TILE_LAVA = frozenset([22, 23, 30, 31])
TILE_WATER = frozenset([40, 48, 56])
TILE_SOLID = (frozenset(range(8 * 8))
              .difference(TILE_LAVA)
              .difference(TILE_WATER))

def dumparr(arr):
    w = len(str(numpy.max(arr))) + 1
    sep = '-' * (w * arr.shape[1])
    print(sep)
    for line in arr:
        print(''.join([str(x).rjust(w) for x in line]))
    print(sep)

def make_table(pairs):
    """Make a lookup table for tiles.

    The pairs should be (tiles, value), where tiles is a sequence of
    tile values, and value is the value they should be mapped to.
    """
    n = 0
    for tiles, value in pairs:
        n = max(n, max(tiles) + 1)
    table = numpy.zeros((n + 1,), numpy.uint32)
    for tiles, value in pairs:
        for tile in tiles:
            table[tile + 1] = value
    return table

def map_tiles(arr, table):
    """Apply a lookup table to a tile array."""
    return table[arr & tilemap.TILE_MASK]

class Body(object):
    """A compound physics body."""
    __slots__ = ['rects', 'planes']
    def __init__(self):
        self.rects = []
        self.planes = []

    def add_enclosed(self, arr):
        """Add an enclosed space to the body.

        The array should contain zeroes and ones, and it will be
        modified by this function to contain all zeroes.
        """
        # dumparr(arr)
        self.add_planes(arr)
        self.add_rects(arr)

    def add_planes(self, arr):
        """Add planes to cover extreme nonzero cells in arr.

        This will always add four planes, one in each direction.  The
        array should consist of zeroes and ones, and it will be
        modified by this function.
        """
        z = numpy.argwhere(arr == 0)
        y0, x0 = z.min(0)
        y1, x1 = z.max(0)
        arr[:y0] = 0
        arr[y1:] = 0
        arr[:,:x0] = 0
        arr[:,x1:] = 0
        self.planes = [x1, y1, x0, y0]

    def add_rects(self, arr):
        """Add rectangles to cover all nonzero cells in arr.

        The array should only contain zeroes and ones, and it will be
        modified by this function to contain all zeroes.
        """
        rem = numpy.sum(arr)
        ah, aw = arr.shape
        heights = numpy.zeros((aw,), numpy.uint32)
        rects = []
        while rem > 0:
            heights.fill(0)
            maxa = 0
            rect = None
            for y in range(ah):
                heights += 1
                heights *= arr[y]
                # Not the fastest algorithm, asymptotically...
                for x in range(aw):
                    rh = heights[x]
                    if not rh:
                        continue
                    x0 = x
                    while x0 > 0 and heights[x0 - 1] >= rh:
                        x0 -= 1
                    x1 = x
                    while x1 + 1 < aw and heights[x1 + 1] >= rh:
                        x1 += 1
                    rw = x1 + 1 - x0
                    if rw * rh > maxa:
                        maxa = rw * rh
                        rect = x0, y + 1 - rh, rw, rh
            assert rect is not None
            rx, ry, rw, rh = rect
            rem -= maxa
            arr[ry:ry+rh,rx:rx+rw] = 0
            rects.extend(rect)
        self.rects.extend(rects)

    def dump(self):
        """Dump the body to string format."""
        return ':'.join([
            ','.join([format(x,'x') for x in self.planes]),
            ','.join([format(x,'x') for x in self.rects]),
        ])

def get_solid(arr, coverage):
    """Get an array of places where a layer is solid.

    This fills in any unreachable areas.
    """
    nonsolid = 1 - map_tiles(arr, make_table([(TILE_SOLID, 1)]))
    labels, labelcount = scipy.ndimage.label(nonsolid)
    table = numpy.ones((labelcount + 1,), numpy.uint32)
    table[labels[coverage != 0]] = 0
    if table[0] == 0:
        print('Warning: object overlaps level')
        table[0] = 1
    if numpy.all(table == 1):
        print('Warning: no object coverage')
    return table[labels]

def make_phys(layer, coverage):
    """Convert a tile layer to a physics object in string format."""
    body = Body()
    body.add_enclosed(get_solid(layer.data, coverage))
    return body.dump()

def get_object_coverage(layer, levelsize, tilesize):
    """Get array marking cells partially or totally covered by objects."""
    w, h = levelsize
    arr = numpy.zeros((h, w), numpy.uint32)
    tx, ty = tilesize
    for obj in layer.objects:
        if obj.type in TYPE_IGNORE:
            continue
        px, py = obj.pos
        sx, sy = obj.size
        arr[py//ty:(py+sy+ty-1)//ty,px//tx:(px+sx+tx-1)//tx] = 1
    return arr

OBJ_PROP = {
    0: {'Type': 'Door'},
    4: {'Type': 'Save'},
    8: {'Type': 'Chest', 'Item': 'Heart'},
    9: {'Type': 'Chest', 'Item': 'Weapon'},
    10: {'Type': 'Chest', 'Item': 'Powerup'},
}
OBJ_PROP.update({
    n: {'Type': 'Sign', 'Sprite': n}
    for n in [2, 3, 5, 6, 7]
})
OBJ_MONSTER = {
    0: {'Type': 'Eye'},
}
OBJ_MAP = {
    'props': OBJ_PROP,
    'monsters': OBJ_MONSTER,
}
del OBJ_PROP
del OBJ_MONSTER

TYPE_IGNORE = {'Lava', 'Water'}

def make_spawn(layer, tilesets):
    spawn = []
    for obj in layer.objects:
        if obj.type is not None:
            if obj.type in TYPE_IGNORE:
                continue
            if obj.type == 'Player':
                odata = {'Type': 'Player'}
        elif obj.gid is not None:
            tile = obj.gid & tilemap.TILE_MASK
            for ts in tilesets:
                if ts.first <= tile < ts.last:
                    tname = ts.tileset.name
                    try:
                        odata = OBJ_MAP[tname]
                    except KeyError:
                        raise LevelError('no data for tileset: {}'
                                         .format(tname))
                    try:
                        odata = odata[tile - ts.first]
                    except KeyError:
                        raise LevelError('no data for tile: {}/{}'
                                         .format(tname, tile - ts.first))
                    break;
            else:
                raise LevelError('no tile set for tile: {}'.format(tile))
        else:
            print(obj)
            continue
        x, y = obj.pos
        w, h = obj.size
        x += w // 2
        y += h // 2
        odata['X'] = x
        odata['Y'] = y
        odata.update(obj.properties)
        spawn.append(odata)
    return spawn

def convert_level(path, minify=False):
    """Convert the given level to JSON format."""
    tmap = tilemap.TileMap.open(path)
    tmap.flip_y()
    main = tmap.get_layer('Main')
    if not main or not isinstance(main, tilemap.TileLayer):
        raise LevelError('No Main tile layer')
    objs = tmap.get_layer('Default')
    if not objs or not isinstance(objs, tilemap.ObjectLayer):
        raise LevelError('No Default object group')
    REMOVE = {'Lava', 'Water'}
    if False:
        objs.objects = [obj for obj in objs.ojbects
                        if obj.type not in REMOVE]
    cov = get_object_coverage(objs, tmap.size, tmap.tilesize)
    data = {
        'Mesh': make_mesh(main),
        'Body': make_phys(main, cov),
        'Spawn': make_spawn(objs, tmap.tilesets),
    }
    return data

def convert_all(path):
    import json
    import sys
    levels = {}
    for fname in os.listdir(path):
        if fname.startswith('_') or fname.startswith('.'):
            continue
        if not fname.endswith('.tmx'):
            continue
        fpath = os.path.join(path, fname)
        name = os.path.splitext(fname)[0]
        obj = convert_level(fpath)
        levels[name] = obj
    if False:
        json.dump(levels, sys.stdout, indent=2, sort_keys=True)
        sys.stdout.write('\n')
    else:
        json.dump(levels, sys.stdout, separators=(',', ':'))

if __name__ == '__main__':
    convert_all(
        os.path.join(
            os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
            'assets', 'levels'))
