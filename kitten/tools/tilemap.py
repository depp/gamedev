# Copyright 2016 Dietrich Epp.
#
# This file is part of Kitten Teleporter.  The Kitten Teleporter source
# code is distributed under the terms of the MIT license.
# See LICENSE.txt for details.
import json
import numpy
import os
import xml.etree.ElementTree as etree

class MapError(Exception):
    pass

FLIP_HORIZ = 0x80000000
FLIP_VERT  = 0x40000000
FLIP_DIAG  = 0x20000000
TILE_MASK  = 0x1FFFFFFF

def _attr2d(e, namex, namey):
    """Parse a 2D coordinate attribute."""
    return int(e.attrib[namex]), int(e.attrib[namey])

def _parse_version(s):
    """Parse a version number as a tuple."""
    f = s.split('.')
    v1 = int(f[0])
    v2 = int(f[1]) if len(f) > 0 else 0
    return v1, v2

def _parse_props(e):
    """Parse properties."""
    p = {}
    for c in e:
        if c.tag != 'property':
            raise MapError('expected <property> element')
        p[c.attrib['name']] = c.attrib['value']
    return p

def _flip2d(a, off=0):
    x, y = a
    return x, off - y

class Image(object):
    """Image reference (image tag)."""
    __slots__ = [
        # Path to the image file
        'source',
        # Size of the image, (width, height), in pixels
        'size',
    ]

    @classmethod
    def parse_xml(class_, e, basepath, *, cache):
        """Create an image reference from an XML element."""
        self = class_()
        # try: self.format = e.attrib['format']
        # except KeyError: self.format = None
        try: self.source = os.path.join(basepath, e.attrib['source'])
        except KeyError: self.source = None
        # try: self.trans = e.attrib['trans']
        # except KeyError: self.trans = None
        try: self.size = _attr2d(e, 'width', 'height')
        except KeyError: raise MapError('no image size specified')
        return self

class TileSet(object):
    """Tile set (tileset tag)."""
    __slots__ = [
        # Name of this tile set, or None
        'name',
        # Size of each tile, (width, height), in pixels
        'tilesize',
        # Size of the tilemap, (width, height), in tiles
        'size',
        # Spacing between adacent tiles, in pixels
        'spacing',
        # Margin between tiles and the image boundary, in pixels
        'margin',
        # Number of tiles
        'count',
        # Offset (not sure)
        'offset',
        # Image, or None
        'image',
        # Property dictionary, string -> string
        'properties',
    ]

    @classmethod
    def open(class_, path, *, cache):
        tree = etree.parse(path)
        return class_.parse_xml(tree.getroot(), os.path.dirname(path),
                                cache=cache)

    @classmethod
    def parse_xml(class_, e, basepath, *, cache):
        """Create a tile set from an XML element."""
        self = class_()
        if e.tag != 'tileset':
            raise MapError('invalid tag for tileset: {!r}'.format(e.tag))
        try: self.name = e.attrib['name']
        except KeyError: self.name = None
        try: self.tilesize = _attr2d(e, 'tilewidth', 'tileheight')
        except KeyError: raise MapError('missing tile size')
        try: self.spacing = int(e.attrib['spacing'])
        except KeyError: self.spacing = 0
        try: self.margin = int(e.attrib['margin'])
        except KeyError: self.margin = 0
        try: self.count = int(e.attrib['tilecount'])
        except KeyError: self.count = None
        # try: self.columns = int(e.attrib['columns'])
        # except KeyError: self.columns = None
        self.offset = 0, 0
        self.image = None
        self.properties = {}
        for c in e:
            if c.tag == 'tileoffset':
                try: self.offset = _attr2d(e, 'x', 'y')
                except KeyError: pass
            elif c.tag == 'image':
                self.image = Image.parse_xml(c, basepath, cache=cache)
            elif c.tag == 'properties':
                self.properties.update(_parse_props(c))
            elif c.tag == 'terraintype':
                pass
            elif c.tag == 'tile':
                pass
            else:
                raise MapError('unknown tag in tileset: {!r}'.format(c.tag))
        self.size = 0, 0
        if self.image is not None:
            iw, ih = self.image.size
            tw, th = self.tilesize
            margin = self.margin
            spacing = self.spacing
            if iw > 0 and ih > 0 and tw > 0 and th > 0:
                self.size = (
                    (iw - margin + spacing) // (tw + spacing),
                    (ih - margin + spacing) // (th + spacing),
                )
        w, h = self.size
        self.count = w * h
        return self

class TileSetRef(object):
    """Reference to a tileset (tileset tag)."""
    __slots__ = [
        # Inclusive lower GID bound
        'first',
        # Exclusive upper GID bound
        'last',
        # Tile set data, a TileSet
        'tileset',
    ]

    @classmethod
    def parse_xml(class_, e, basepath, *, cache):
        """Create a tile set from an XML element."""
        self = class_()
        try: self.first = int(e.attrib['firstgid'])
        except KeyError: raise MapError('missing firstgid')
        try: source = os.path.join(basepath, e.attrib['source'])
        except KeyError: source = None
        if source is None:
            self.tileset = TileSet.parse_xml(e, basepath, cache=cache)
        else:
            self.tileset = TileSet.open(source, cache=cache)
        self.last = self.first + self.tileset.count
        return self

class Layer(object):
    """Base layer class."""
    __slots__ = [
        # Layer name, or None
        'name',
        # Opacity, 0..1
        'opacity',
        # Whether the layer is visible, boolean
        'visible',
        # Offset to apply to layer, (x, y)
        'offset',
        # Dictionary str -> str
        'properties',
    ]

    @classmethod
    def parse_xml(class_, e, basepath, *, cache):
        """Create a layer from an XML element."""
        self = class_()
        try: self.name = e.attrib['name']
        except KeyError: self.name = None
        try: self.opacity = float(e.attrib['opacity'])
        except KeyError: self.opacity = 1
        try: self.visible = bool(int(e.attrib['visible']))
        except KeyError: self.visible = True
        try: self.offset = _attr2d(e, 'offsetx', 'offsety')
        except KeyError: self.offset = 0, 0
        self.properties = {}
        for c in e:
            if c.tag == 'properties':
                self.properties.update(_parse_props(c))
        return self

    def flip_y(self, height, theight):
        self.offset = _flip2d(self.offset)

class TileLayer(Layer):
    """Tile layer (layer tag)."""
    __slots__ = [
        # Attributes
        'size',
        # Data (2D numpy array of uint32)
        'data',
    ]

    @classmethod
    def parse_xml(class_, e, basepath, *, cache):
        self = super(TileLayer, class_).parse_xml(e, basepath, cache=cache)
        try: self.size = _attr2d(e, 'width', 'height')
        except KeyError: raise MapError('missing layer size')
        self.data = None
        for c in e:
            if c.tag == 'properties':
                pass
            elif c.tag == 'data':
                self._parse_data(c)
            else:
                raise MapError('unknown tag in layer: {!r}'.format(c.tag))
        if self.data is None:
            raise MapError('tile layer has no data')
        return self

    def _parse_data(self, e):
        if self.data is not None:
            raise MapError('extra data tag')
        try: enc = e.attrib['encoding']
        except KeyError: raise MapError('missing data encoding')
        w, h = self.size
        if enc == 'csv':
            values = e.text.split(',')
            if len(values) != w * h:
                raise MapError('CSV dimensions mismatch')
            values = numpy.array([int(x) for x in values], dtype=numpy.uint32)
            values = values.reshape((h, w))
            self.data = values
        else:
            raise MapError('tile encoding not supported: {!r}'
                           .format(enc))

    def flip_y(self, height, theight):
        super(TileLayer, self).flip_y(height, theight)
        self.data = self.data[::-1]

class ObjectLayer(Layer):
    """Object group (objectgroup tag)."""
    __slots__ = [
        # Color for objects in this layer
        'color',
        # List of Object
        'objects',
    ]

    @classmethod
    def parse_xml(class_, e, basepath, *, cache, tilesets):
        self = super(ObjectLayer, class_).parse_xml(e, basepath, cache=cache)
        try: self.color = e.attrib['color']
        except KeyError: pass
        self.objects = []
        for c in e:
            if c.tag == 'properties':
                self.properties.update(_parse_props(c))
            elif c.tag == 'object':
                self.objects.append(Object.parse_xml(
                    c, basepath, cache=cache, tilesets=tilesets))
            else:
                raise MapError('unknown tag in layer: {!r}'.format(c.tag))
        return self

    def flip_y(self, height, theight):
        super(ObjectLayer, self).flip_y(height, theight)
        for obj in self.objects:
            x, y = obj.pos
            obj.pos = x, height * theight - y

class Object(object):
    """Object (object tag)."""
    __slots__ = [
        # Unique identifier (generated by Tiled) or None
        'id',
        # Object name, or None
        'name',
        # Object type, or None
        'type',
        # Object position, (x, y)
        'pos',
        # Object size, (w, h)
        'size',
        # Object rotation, in degrees
        'rotation',
        # Object tile GID
        'gid',
        'visible',
        # Elements
        'properties',
        # <ellipse>, <polygon>, <polyline>, <image> trigger errors
    ]

    @classmethod
    def parse_xml(class_, e, basepath, *, cache, tilesets):
        self = class_()
        try: self.id = e.attrib['id']
        except KeyError: self.id = None
        try: self.name = e.attrib['name']
        except KeyError: self.name = None
        try: self.type = e.attrib['type']
        except KeyError: self.type = None
        try: self.pos = _attr2d(e, 'x', 'y')
        except KeyError: raise MapError('missing object position')
        try: self.gid = int(e.attrib['gid'])
        except KeyError: self.gid = None
        try: self.rotation = int(e.attrib['rotation'])
        except KeyError: self.rotation = 0
        try: self.visible = bool(int(e.attrib['visible']))
        except KeyError: self.visible = True
        try:
            self.size = _attr2d(e, 'width', 'height')
        except KeyError:
            if self.gid is None:
                raise MapError('no gid or size for object')
            tile = self.gid & TILE_MASK
            for ts in tilesets:
                if ts.first <= tile < ts.last:
                    self.size = ts.tileset.size
                    break
            else:
                raise MapError('cannot find tile {}'.format(tile))
        self.properties = {}
        for c in e:
            if c.tag == 'properties':
                self.properties.update(_parse_props(c))
            else:
                raise MapError('unknown tag in object: {!r}'.format(c.tag))
        return self

    def __str__(self):
        data = {}
        if self.id is not None:
            data['id'] = self.id
        if self.name is not None:
            data['name'] = self.name
        if self.type is not None:
            data['type'] = self.type
        if self.rotation:
            data['rotation'] = self.rotation
        if self.gid is not None:
            data['gid'] = self.gid
        if not self.visible:
            data['visible'] = False
        if self.properties:
            data['properties'] = self.properties
        return json.dumps(data, indent=2)

class TileMap(object):
    """A tile based map.

    This only supports orthogonal maps.  Image layers are not
    supported, and raise an exception.  Terrain types are silently
    discarded.
    """
    __slots__ = [
        # Map size, in tiles
        'size',
        # Tile size, in pixels
        'tilesize',
        # Global map properties, dictionary mapping strings to strings
        'properties',
        # List of tilesets
        'tilesets',
        # List of layers
        'layers',
    ]

    @classmethod
    def open(class_, path, *, cache=None):
        """Read a tile map from a Tiled XML file."""
        if cache is None:
            cache = {}
        tree = etree.parse(path)
        return class_.parse_xml(tree.getroot(), os.path.dirname(path),
                                cache=cache)

    @classmethod
    def parse_xml(class_, e, basepath, *, cache):
        self = class_()
        if e.tag != 'map':
            raise MapError('tag is not map')
        version = e.attrib.get('version')
        if version:
            if _parse_version(version)[0] != 1:
                raise MapError('unknown tile map version: {!r}'
                               .format(version))
        self.size = _attr2d(e, 'width', 'height')
        self.tilesize = _attr2d(e, 'tilewidth', 'tileheight')
        self.tilesets = []
        for c in e:
            if c.tag == 'tileset':
                self.tilesets.append(TileSetRef.parse_xml(
                    c, basepath, cache=cache))
        self.properties = {}
        self.layers = []
        for c in e:
            if c.tag == 'properties':
                self.properties.update(_parse_props(c))
            elif c.tag == 'layer':
                self.layers.append(TileLayer.parse_xml(
                    c, basepath, cache=cache))
            elif c.tag == 'objectgroup':
                self.layers.append(ObjectLayer.parse_xml(
                    c, basepath, cache=cache, tilesets=self.tilesets))
            elif c.tag == 'tileset':
                pass
            else:
                raise MapError('unexpected tag: {!r}'.format(c.tag))
        return self

    def get_layer(self, name):
        """Get the layer with the given name."""
        for layer in self.layers:
            if layer.name == name:
                return layer
        return None

    def flip_y(self):
        """Flip the Y coordinates.

        After flipping, the origin will be the lower left instead of
        the upper left.
        """
        for layer in self.layers:
            layer.flip_y(self.size[1], self.tilesize[1])

if __name__ == '__main__':
    ts = TileSet.open(
        os.path.join(
            os.path.dirname(os.path.dirname(__file__)),
            'assets', 'levels', 'monsters.tsx'))
    print('Size: {0[0]}x{0[1]}'.format(ts.size))
    print('Count: {}'.format(ts.count))
