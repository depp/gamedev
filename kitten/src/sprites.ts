/* Copyright 2015-2016 Dietrich Epp.

   This file is part of Kitten Teleporter.  The Kitten Teleporter source
   code is distributed under the terms of the MIT license.
   See LICENSE.txt for details. */

import * as shader from './shader';

// Whether the program is loaded
var Loaded: boolean = false;
// The shader program
var Program: shader.SpriteProgram = null;

export function init(gl: WebGLRenderingContext) {
	Loaded = true;
	Program = shader.spriteProgram(gl, 'plain', 'plain');
}

export interface Sprite {
	x: number;
	y: number;
}

/*
 * Sprite layer.
 */
export class SpriteLayer {
  // number of sprites
	private _count: number;
  // maximum number of sprites in array
	private _capacity: number;
  // position array
	private _vdata_pos: Int16Array;
  // color array
	private _vdata_color: Uint32Array;
  // offset of position data in buffer
	private _voff_pos: number;
  // offset of color data in buffer
	private _voff_color: number;
  // whether vertex data has changed
	private _vdirty: boolean;
  // whether index data has changed
	private _idirty: boolean;
  // GL array buffer
	private _vbuffer: WebGLBuffer;
  // GL element array buffer
	private _ibuffer: WebGLBuffer;

	constructor() {
		this._count = 0;
		this._capacity = 0;
		this._vdata_pos = null;
		this._vdata_color = null;
		this._voff_pos = 0;
		this._voff_color = 0;
		this._vdirty = true;
		this._idirty = true;
		this._vbuffer = null;
		this._ibuffer = null;
	}

	/*
	 * Draw the sprite layer to the screen.
	 * gl: WebGL context
	 */
	draw(gl: WebGLRenderingContext): void {
		var p = Program;
		if (!p) {
			return;
		}
		var i: number;

		if (this._vdirty) {
			if (!this._vbuffer) {
				this._vbuffer = gl.createBuffer();
			}
			this._voff_pos = 0;
			this._voff_color = this._count * 16;
			gl.bindBuffer(gl.ARRAY_BUFFER, this._vbuffer);
			gl.bufferData(gl.ARRAY_BUFFER, this._count * 32, gl.DYNAMIC_DRAW);
			gl.bufferSubData(
				gl.ARRAY_BUFFER,
				this._voff_pos,
				this._vdata_pos.subarray(0, this._count * 8));
			gl.bufferSubData(
				gl.ARRAY_BUFFER,
				this._voff_color,
				this._vdata_color.subarray(0, this._count * 4));
			gl.bindBuffer(gl.ARRAY_BUFFER, null);
			this._vdirty = false;
		}

		if (this._idirty) {
			if (!this._ibuffer) {
				this._ibuffer = gl.createBuffer();
			}
			var capacity = this._capacity;
			var idata = new Uint16Array(capacity * 6);
			for (i = 0; i < capacity; i++) {
				idata.set([
					i*4+0, i*4+1, i*4+2,
					i*4+2, i*4+1, i*4+3,
				], i * 6);
			}
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ibuffer);
			gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, idata, gl.STATIC_DRAW);
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
			this._idirty = false;
		}

		gl.useProgram(p.program);
		gl.uniformMatrix4fv(p.MVP, false, new Float32Array([
			2/800, 0, 0, 0,
			0, 2/450, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1,
		]));
		gl.bindBuffer(gl.ARRAY_BUFFER, this._vbuffer);
		gl.enableVertexAttribArray(0);
		gl.vertexAttribPointer(0, 2, gl.SHORT, false, 4, this._voff_pos);
		gl.enableVertexAttribArray(1);
		gl.vertexAttribPointer(1, 4, gl.UNSIGNED_BYTE, true, 4, this._voff_color);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ibuffer);

		gl.drawElements(gl.TRIANGLES, this._count * 6, gl.UNSIGNED_SHORT, 0);

		gl.disableVertexAttribArray(0);
		gl.disableVertexAttribArray(1);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
		gl.useProgram(null);
	}

	/*
	 * Add sprites to the sprite layer.
	 * arg.x: X coordinate
	 * arg.y: Y coordinate
	 */
	add(...sprites: Sprite[]): void {
		var i: number;

		var count = this._count, new_count = count + sprites.length;
		if (new_count > this._capacity) {
			var new_capacity = Math.max(this._capacity, 32);
			while (new_count > new_capacity) {
				new_capacity *= 2;
			}
			var new_vdata_pos = new Int16Array(new_capacity * 8);
			if (this._vdata_pos) {
				new_vdata_pos.set(this._vdata_pos);
			}
			this._vdata_pos = new_vdata_pos;
			var new_vdata_color = new Uint32Array(new_capacity * 4);
			if (this._vdata_color) {
				new_vdata_color.set(this._vdata_color);
			}
			this._vdata_color = new_vdata_color;
			this._capacity = new_capacity;
			this._idirty = true;
		}

		for (i = 0; i < sprites.length; i++) {
			var x = sprites[i].x, y = sprites[i].y;
			var x0 = x - 16, x1 = x + 16, y0 = y - 16, y1 = y + 16;
			var color = 0x3399ccff;
			this._vdata_pos.set([x0, y0, x1, y0, x0, y1, x1, y1], i * 8);
			this._vdata_color.set([color, color, color, color], i * 4);
		}

		this._count = new_count;
		this._vdirty = true;
	}

	/*
	 * Remove all sprites from the layer.
	 */
	clear(): void {
		this._count = 0;
		this._vdirty = true;
	}
}
