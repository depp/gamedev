/* Copyright 2016 Dietrich Epp.

   This file is part of Kitten Teleporter.  The Kitten Teleporter source
   code is distributed under the terms of the MIT license.
   See LICENSE.txt for details. */

/*
 * Tile renderer.
 *
 * Renders composite objects made up of tiles.
 */

import * as load from './load';
import * as shader from './shader';

// Attribute offsets
const APos = 0;       // u16 x 2
const ATexCoord = 4;  // u16 x 2
const ATotal = 8;

// Size of tiles, in pixels
const TileSize = 16;

// Whether the program is loaded
var Loaded: boolean = false;
// The shader program
var Program: shader.TileProgram = null;
// The tile sheet texture
var TileSheet: WebGLTexture = null;
// Number of columns in tile sheet
var TileColumns: number = 0;
// Scaling factor to apply to tile texture coordinates
var TexScale: Float32Array = new Float32Array(2);

export function init(gl: WebGLRenderingContext) {
	if (Loaded) {
		return;
	}
	Loaded = true;

	Program = shader.tileProgram(gl, 'tile', 'tile');

	var img = load.getImage('tiles');
	if (img) {
		TileSheet = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, TileSheet);
		gl.texImage2D(
			gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
		gl.texParameteri(
			gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(
			gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
		TileColumns = Math.floor(img.width / TileSize);
		TexScale[0] = TileSize / img.width;
		TexScale[1] = TileSize / img.height;
	}
}

export function clear() {
	WorldTiles.clear();
}

/*
 * Tile rendering layer.
 */
class TileLayer {
	private _count: number;
	private _capacity: number;
	private _dirty: boolean;
	private _vbuffer: WebGLBuffer;
	private _objects: TileObject[];

	constructor() {
		this._count = 0;
		this._capacity = 0;
		this._dirty = true;
		this._vbuffer = null;
		this._objects = [];
	}

	/*
	 * Draw the tile layer to the screen.
	 */
	render(gl: WebGLRenderingContext, camera: { uiMVP: Float32Array }): void {
		var p = Program;
		if (!p || !TileSheet) {
			return;
		}
		var i: number, offset: number, ocount: number;

		if (this._dirty) {
			this._dirty = false;
			this._count = 0;

			var count = 0;
			for (i = 0; i < this._objects.length; i++) {
				if (this._objects[i]._count < 0) {
					this._objects[i]._initMesh();
				}
				count += this._objects[i]._count;
			}
			if (!count) {
				return;
			}

			if (!this._vbuffer) {
				this._vbuffer = gl.createBuffer();
			}
			gl.bindBuffer(gl.ARRAY_BUFFER, this._vbuffer);
			gl.bufferData(gl.ARRAY_BUFFER, count * ATotal * 6, gl.STATIC_DRAW);
			offset = 0;
			for (i = 0; i < this._objects.length; i++) {
				ocount = this._objects[i]._count;
				if (!ocount) {
					continue;
				}
				gl.bufferSubData(
					gl.ARRAY_BUFFER,
					offset * ATotal * 6,
					this._objects[i]._i16);
				offset += ocount;
			}
			this._count = count;
		} else {
			if (!this._count) {
				return;
			}
		}

		gl.useProgram(p.program);
		gl.bindBuffer(gl.ARRAY_BUFFER, this._vbuffer);
		gl.enableVertexAttribArray(0);
		gl.vertexAttribPointer(0, 2, gl.SHORT, false, ATotal, APos);
		gl.enableVertexAttribArray(1);
		gl.vertexAttribPointer(1, 2, gl.SHORT, false, ATotal, ATexCoord);
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, TileSheet);

		gl.uniformMatrix4fv(p.MVP, false, camera.uiMVP);
		gl.uniform2f(p.VertScale, TileSize * 2, TileSize * 2);
		gl.uniform2fv(p.TexScale, TexScale);
		gl.uniform1i(p.TileSheet, 0);

		offset = 0;
		for (i = 0; i < this._objects.length; i++) {
			gl.uniform2fv(p.Offset, this._objects[i].position);
			ocount = this._objects[i]._count;
			gl.drawArrays(gl.TRIANGLES, offset * 6, ocount * 6);
			offset += ocount;
		}

		gl.disableVertexAttribArray(0);
		gl.disableVertexAttribArray(1);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);
		gl.useProgram(null);
	};

	/*
	 * Add a tile object to the tile layer.
	 */
	addObject(obj: TileObject): void {
		if (this._objects.indexOf(obj) === -1) {
			obj._parent = this;
			this._objects.push(obj);
			if (obj._count) {
				this._dirty = true;
			}
		}
	};

	/*
	 * Remove an object from the tile layer.
	 */
	removeObject(obj: TileObject): void {
		var idx = this._objects.indexOf(obj);
		if (idx !== -1) {
			obj._parent = null;
			this._objects.splice(idx, 1);
			if (obj._count) {
				this._dirty = true;
			}
		}
	};

	clear(): void {
		for (var i = 0; i < this._objects.length; i++) {
			this._objects[i]._parent = null;
		}
		this._objects.length = 0;
		this._count = 0;
		this._dirty = false;
	}
}

var WorldTiles = new TileLayer();
export { WorldTiles as world };

/********************************************************************/

/*
 * An object which is drawn using tiles on a grid.
 *
 * mesh.D: Mesh data, in string format
 * mesh.X: X offset for mesh, in tiles
 * mesh.Y: Y offset for mesh, in tiles
 * mesh.W: Mesh width, in tiles
 * mesh.H: Mesh height, in tiles
 * arg.position: (optional) Pixel offset for mesh
 */
export class TileObject {
	_parent: TileLayer;
	_i16: Int16Array;
	_count: number;
	_mesh: any;
	position: Float32Array;

	constructor(mesh: string, arg: { position?: GLM.IArray }) {
		this._parent = null;
		this._i16 = null;
		this._count = -1;
		this._mesh = mesh;
		this.position = new Float32Array(2);
		if (arg) {
			if (arg.position) {
				vec2.copy(this.position, arg.position);
			}
		}
	}

	/*
	 * Initialize the mesh data.  Called once TileColumns is known.
	 */
	_initMesh() {
		var i: number;
		this._count = 0;
		var mesh = this._mesh;
		if (!mesh) {
			return;
		}
		this._mesh = null;
		var mfields = mesh.split(':');
		var size = mfields[0].split(',');
		var w = parseInt(size[0], 16);
		var h = parseInt(size[1], 16);
		var xoff = -(w / 2) | 0, yoff = -(h / 2) | 0;
		xoff = 0;
		yoff = 0;
		var tilestr = mfields[1].split(',');
		if (tilestr.length != w * h) {
			console.error('invalid mesh');
			return;
		}
		var tarr = new Int32Array(w * h), count = 0;
		for (i = 0; i < w * h; i++) {
			var s = tilestr[i];
			if (!s.length) {
				tarr[i] = -1;
			} else {
				count++;
				tarr[i] = parseInt(s, 16);
			}
		}
		var x: number, y: number, tc = TileColumns;
		var i16 = new Int16Array(count * 6 * 4);
		i = 0;
		for (y = 0; y < h; y++) {
			for (x = 0; x < w; x++) {
				var v = tarr[y * w + x];
				if (v < 0) {
					continue;
				}
				var orient = v & 7, tx = (v >> 3) % tc, ty = (v >> 3) / tc | 0;
				var vx = x + xoff, vy = y + yoff;

				i16[i+ 0] = vx + 0; i16[i+ 1] = vy + 0;
				i16[i+ 2] = tx + 0; i16[i+ 3] = ty + 1;

				i16[i+ 4] = vx + 1; i16[i+ 5] = vy + 0;
				i16[i+ 6] = tx + 1; i16[i+ 7] = ty + 1;

				i16[i+ 8] = vx + 0; i16[i+ 9] = vy + 1;
				i16[i+10] = tx + 0; i16[i+11] = ty + 0;

				i16[i+12] = vx + 0; i16[i+13] = vy + 1;
				i16[i+14] = tx + 0; i16[i+15] = ty + 0;

				i16[i+16] = vx + 1; i16[i+17] = vy + 0;
				i16[i+18] = tx + 1; i16[i+19] = ty + 1;

				i16[i+20] = vx + 1; i16[i+21] = vy + 1;
				i16[i+22] = tx + 1; i16[i+23] = ty + 0;

				i += 24;
			}
		}
		this._count = count;
		this._i16 = i16;
	}
}
