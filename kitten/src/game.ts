/* Copyright 2015-2016 Dietrich Epp.

   This file is part of Kitten Teleporter.  The Kitten Teleporter source
   code is distributed under the terms of the MIT license.
   See LICENSE.txt for details. */

import * as control from './control';
import * as load from './load';
import * as param from './param';
import * as sprites from './sprites';
import * as state from './state';
import * as text from './text';
import * as tiles from './tiles';
import * as time from './time';

const MobyDick = 'Call me Ishmael. Some years ago—never mind how long precisely—having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen and regulating the circulation. Whenever I find myself growing grim about the mouth; whenever it is a damp, drizzly November in my soul; whenever I find myself involuntarily pausing before coffin warehouses, and bringing up the rear of every funeral I meet; and especially whenever my hypos get such an upper hand of me, that it requires a strong moral principle to prevent me from deliberately stepping into the street, and methodically knocking people’s hats off—then, I account it high time to get to sea as soon as I can. This is my substitute for pistol and ball. With a philosophical flourish Cato throws himself upon his sword; I quietly take to the ship. There is nothing surprising in this. If they but knew it, almost all men in their degree, some time or other, cherish very nearly the same feelings towards the ocean with me.\nThere now is your insular city of the Manhattoes, belted round by wharves as Indian isles by coral reefs—commerce surrounds it with her surf. Right and left, the streets take you waterward. Its extreme downtown is the battery, where that noble mole is washed by waves, and cooled by breezes, which a few hours previous were out of sight of land. Look at the crowds of water-gazers there.\nCircumambulate the city of a dreamy Sabbath afternoon. Go from Corlears Hook to Coenties Slip, and from thence, by Whitehall, northward. What do you see?—Posted like silent sentinels all around the town, stand thousands upon thousands of mortal men fixed in ocean reveries. Some leaning against the spiles; some seated upon the pier-heads; some looking over the bulwarks of ships from China; some high aloft in the rigging, as if striving to get a still better seaward peep. But these are all landsmen; of week days pent up in lath and plaster—tied to counters, nailed to benches, clinched to desks. How then is this? Are the green fields gone? What do they here?';

var camera = (function() {
	var tr = <Float32Array> vec3.fromValues(
			-param.Width / 2, -param.Height / 2, 0);
	var fovY = param.Height, fovX = param.Width; // use aspect
	var pr = <Float32Array> mat4.create();
	pr[0] = 2 / fovX;
	pr[5] = 2 / fovY;
	var mvp = <Float32Array> mat4.create();
	mat4.translate(mvp, pr, tr);
	return { uiMVP: mvp };
})();

/*
 * Calculate the interpolated position of a body.
 */
function bodyPos(body: p2.Body, frac: number) {
	// P2 can do this for us, but we will figure that out later.
	var p = body.position, pp = body.previousPosition;
	return [pp[0] + (p[0] - pp[0]) * frac, pp[1] + (p[1] - pp[1]) * frac];
}

/*
 * Main game screen.
 */
export class GameScreen implements state.Screen, time.TimeTarget {
	private time: time.Time;
	private sprites: sprites.SpriteLayer;
	private world: p2.World;
	private bbody: p2.Body;
	private gbody: p2.Body;

	constructor() {
		// Timing manager
		this.time = null;

		// Sprite layer
		this.sprites = new sprites.SpriteLayer();

		// Physics engine
		this.world = new p2.World({
			gravity: [0, -9.82],
		});

		// Occupants
		this.bbody = new p2.Body({
			mass: 5,
			position: [0, 5],
		});
		this.bbody.addShape(new p2.Circle({ radius: 1 }));
		this.world.addBody(this.bbody);
		this.gbody = new p2.Body({
			mass: 0,
			position: [0, -5],
		});
		this.gbody.addShape(new p2.Plane());
		this.world.addBody(this.gbody);

		var obj = new text.TextObject();
		var b = new text.Builder();
		b.add({
			font: text.findFont({ family: 'Alegreya' }),
			text: 'Blast Off!',
		});
		b.finish();
		obj.addLayout(b, { x: 20, y: 420 });
		b.clear();

		b.add({
			font: text.findFont({ family: 'Ropa Sans' }),
			text: MobyDick,
		});
		b.finish({
			width: 480,
			indent: 40,
			align: text.TextAlign.Justify,
		});
		obj.addLayout(b, { x: 100, y: 400 });

		// vec2.set(obj.position, param.Width / 2, param.Height / 2);
		text.ui.add(obj);
	}

	/*
	 * Initialize the screen.
	 */
	start(r: state.RenderContext, args: any): void {
		var gl = r.gl;
		// Initialize graphics layers and controls
		sprites.init(gl);
		text.init(gl);
		tiles.init(gl);
		control.game.enable();

		// Create level structures
		this.time = new time.Time(this, r.time);

		var obj = new tiles.TileObject(load.getLevel('bot1').Mesh, undefined);
		tiles.world.addObject(obj);
	}

	/*
	 * Destroy the screen
	 */
	stop(gl: WebGLRenderingContext): void {
		// Clean up graphics layers and controls
		text.ui.clear();
		tiles.clear();
		control.game.disable();

		// Zero level structures
		this.time = null;
	}

	/*
	 * Render the game screen, updating the game state as necessary.
	 */
	render(r: state.RenderContext): void {
		var gl = r.gl;
		this.time.update(r.time);
		var frac = this.time.frac;

		gl.clearColor(0.0, 0.0, 0.0, 1.0);
		gl.clear(gl.COLOR_BUFFER_BIT);

		this.sprites.clear();
		var pos = bodyPos(this.bbody, frac);
		this.sprites.add({
			x: pos[0] * 32,
			y: pos[1] * 32,
		});
		tiles.world.render(gl, camera);
		this.sprites.draw(gl);
		text.ui.render(gl, camera);
	}

	/*
	 * Advance the game by one frame.  Called by the timing manager.
	 *
	 * dt: The timestep, in s
	 */
	step(dt: number): void {
		var ctl = control.game;
		ctl.update();
		if (ctl.jump.press) {
			console.log('JUMP');
			// FIXME: update p2.d.ts
			(<any> this.bbody).applyImpulse([0, 50]);
		}
		var move = ctl.move.value;
		// FIXME: update p2.d.ts
		(<any> this.bbody).applyForce([move[0] * 50, move[1] * 50]);
		this.world.step(dt);
	}
}
