# Copyright 2016 Dietrich Epp.
# This file is part of the Moria Gamedev Repository.  This file is licensed
# under the terms of the MIT license.  For more information, see LICENSE.txt.
"""Unicode database interface."""
import io
import numpy
import zipfile
from gamedev.pyutil.version import Version

def parse_crange(crange):
    """Parse a Unicode character range."""
    i = crange.find('..')
    if i >= 0:
        first = int(crange[:i], 16)
        last = int(crange[i+2:], 16)
    else:
        first = int(crange, 16)
        last = first
    return first, last + 1

class UnicodeDatabase(object):
    """The Unicode Database."""
    __slots__ = [
        '_zip',
        '_properties',
        '_version',
    ]
    
    def __init__(self, file):
        self._zip = zipfile.ZipFile(file)

    def open(self, name):
        return io.TextIOWrapper(self._zip.open(name))

    def read_table(self, name):
        """Read a simple table."""
        with self.open(name) as fp:
            for line in fp:
                i = line.find('#')
                if i >= 0:
                    line = line[:i]
                line = line.rstrip()
                if not line:
                    continue
                yield [part.strip() for part in line.split(';')]

    def version(self):
        """Get the Unicode data version as a setuptools version object."""
        try: return self._version
        except AttributeError: pass
        age = self.property_info('age')
        self._version = max(
            Version.parse(value) for value in age.values.keys()
            if value != 'NA')
        return self._version
            
    def property_info(self, name):
        """Get information about a property.
        
        Arguments:
        name -- The short name of the property.
        """
        try: return self._properties[name]
        except AttributeError: pass
        properties = {}
        for row in self.read_table('PropertyAliases.txt'):
            prop = Property()
            prop.short, prop.long, *prop.other = row
            prop.values = {}
            properties[prop.short] = prop
        for row in self.read_table('PropertyValueAliases.txt'):
            pname = row[0]
            val = PropertyValue
            prop = properties[pname]
            if pname == 'ccc':
                _, val.numeric, val.short, val.long, *val.other = row
            else:
                _, val.short, val.long, *val.other = row
                val.numeric = len(prop.values)
            prop.values[val.short] = val
        self._properties = properties
        return properties[name]
        
    def property_data(self, name, values, dtype=numpy.int32):
        """Get the contents of a property table.
        
        Arguments:
        name -- The file name for the property data
        values -- Map from value names to numbers
        dtype -- Data type for the resulting array
        """
        missing = None
        unset = numpy.ones(0x110000, dtype=numpy.bool)
        data = numpy.zeros(0x110000, dtype=dtype)
        with self.open(name) as fp:
            for line in fp:
                if line.startswith('# @missing'):
                    i = line.index(':')
                    line = line[i+1:]
                    crange, value = line.split(';')
                    start, end = parse_crange(crange.strip())
                    value = values[value.strip()]
                    data[start:end][unset[start:end]] = value
                    continue
                i = line.find('#')
                if i >= 0:
                    line = line[:i]
                line = line.rstrip()
                if not line:
                    continue
                crange, value = line.split(';')
                start, end = parse_crange(crange.strip())
                value = values[value.strip()]
                data[start:end] = value
                unset[start:end] = False
        return data
        
    def main_column(self, index, values, dtype=numpy.int32):
        """Read a colomn from the main Unicode data table.
        
        Arguments:
        index -- Column index
        values -- Map from value names to numbers
        dtype -- Data type for the resulting array
        """
        data = numpy.zeros(0x110000, dtype=dtype)
        with self.open('UnicodeData.txt') as fp:
            lines = iter(fp)
            for line in lines:
                fields = line.split(';')
                value = values[fields[index]]
                codepoint = int(fields[0], 16)
                name = fields[1]
                if name.startswith('<') and name != '<control>':
                    assert name.endswith(', First>')
                    first_codepoint = codepoint
                    line = next(lines)
                    fields = line.split(';')
                    codepoint = int(fields[0], 16)
                    name = fields[1]
                    assert name.endswith(', Last>')
                    data[first_codepoint:codepoint+1] = value
                else:
                    data[codepoint] = value
        return data

class PropertyValue(object):
    """Information about a Unicode property value.
    
    Attributes:
    short -- Short value name (used in database)
    long -- Long value name
    numeric -- Numeric value
    """
    __slots__ = ['short', 'long', 'numeric', 'other']

class Property(object):
    """Information about a Unicode property.
    
    Attributes:
    short -- Short property name (used in database)
    long -- Long property name
    other -- List of other property names
    values -- Map from short value names to PropertyValue instances
    """
    __slots__ = ['short', 'long', 'other', 'values']

if __name__ == '__main__':
    import sys
    ucd = UnicodeDatabase(sys.argv[1])
    print(ucd.version())
