# Copyright 2015-2016 Dietrich Epp.
# This file is part of the Moria Gamedev Repository.  This file is licensed
# under the terms of the MIT license.  For more information, see LICENSE.txt.
"""Hierarchical lookup table generation.

A hierarchical table maps integers to integers by slicing the bits in the
input, and using each consecutive slice as an index into a sequence of tables.
A hierarchical table is used something like this in C:

    const int TABLE_1[];
    const int TABLE_2[];
    const int TABLE_3[];

    int lookup(int x) {
        // Slice x into segments, bitwise
        int x1 = (x >> 8) & 0x0f; // 4 bits
        int x2 = (x >> 3) & 0x1f; // 5 bits
        int x3 = (x >> 0) & 0x07; // 3 bits
        // Calculate index into each table
        int i1 = x1;
        int i2 = (TABLE_1[i1] << 5) + x2;
        int i3 = (TABLE_2[i2] << 3) + x3;
        // Look up value in final table
        return TABLE_3[i3];
    }

Hierarchical tables use less space in the binary, but they are more complicated
and introduce more data dependencies. Space is only saved if the original table
contians identical blocks of data that can be deduplicated.
"""
import numpy
from .util import print_array

# Map from Numpy types to C types
C_TYPES = {
    'int8': 'signed char',
    'uint8': 'unsigned char',
    'int16': 'short',
    'uint16': 'unsigned short',
    'int32': 'int',
    'uint32': 'unsigned',
}

class TableLayer(object):
    """A layer in a hierarchical table.
    
    If this is the top layer, then the top bits of the input are used to index
    this table, starting with `first_bit`.  The value of `bits` will be None.
    
    If this isa lower layer, then the table consists of a sequence of table
    slices. Each slice has size `2**bits`. The slice is chosen from the higher
    level tables, and the element in the slice is chosen by selecting a
    sequence of bits from the input from `first_bit` to `first_bit+bits`.
    
    Attributes:
    data -- The layer's data table, a numpy array.
    bits -- The number of bits used to index within slices in this table, or
        None if the remaining bits are used.
    first_bit -- The index of the lowest bit used to index within slices.
    """
    __slots__ = [
        'data',
        'bits',
        'first_bit',
    ]
    
    def __init__(self, data, bits, first_bit):
        self.data = data
        self.bits = bits
        self.first_bit = first_bit

class HierarchicalTable(object):
    """A hierarchical lookup table.

    If this is the base table, then the table data is just a raw array. If this
    is a higher level (indirect) table, then the table data is indexes which
    point to locations in the base table.

    Attributes:
    layers -- A list of the layers in the table, from top level to bottom
    """
    __slots__ = ['layers']

    def __init__(self, layers):
        self.layers = layers

    @classmethod
    def create(class_, data, *, max_layers=3):
        """Create an optimized hierarchical table.

        Arguments:
        data -- The table data, a numpy array.
        max_layers -- The maximum number of layers in the hierarchical table.
        """
        best_layers = None
        best_size = data.nbytes + 1
        layers = []
        def visit(data, size, bit_pos):
            nonlocal best_layers, best_size
            data_len = len(data)
            new_size = size + data.nbytes
            if new_size < best_size:
                layers.append(TableLayer(data, None, bit_pos))
                best_layers = list(layers)
                best_size = new_size
                layers.pop()
            if len(layers) + 1 >= max_layers:
                return
            for bits in range(3, 32):
                segment_len = 1 << bits
                if segment_len >= data_len:
                    break
                segments = []
                segment_idx = {}
                index_len = (data_len + segment_len - 1) >> bits
                indexes = numpy.empty(index_len, numpy.uint32)
                for i in range(index_len):
                    segment = data[i << bits:(i + 1) << bits]
                    segment_data = segment.tobytes()
                    try:
                        idx = segment_idx[segment_data]
                    except KeyError:
                        idx = len(segments)
                        segments.append(segment)
                        segment_idx[segment_data] = idx
                    indexes[i] = idx
                imax = len(segments) - 1
                if imax < 0x100:
                    indexes = indexes.astype(numpy.uint8)
                elif imax < 0x10000:
                    indexes = indexes.astype(numpy.uint16)
                layer = TableLayer(numpy.concatenate(segments), bits, bit_pos)
                layers.append(layer)
                visit(indexes, size + layer.data.nbytes, bit_pos + bits)
                layers.pop()
        visit(data, 0, 0)
        assert best_layers is not None
        best_layers.reverse()
        return HierarchicalTable(best_layers)

    def size(self):
        """Get the byte size of the table data."""
        return sum(layer.data.nbytes for layer in self.layers)
        
    def format_c(self, **kw):
        """Create a C definiton of the table."""
        return CHierarchicalTable(self.layers, **kw)

class CHierarchicalTable(object):
    """A hierarchical table written as a C structure."""

    def __init__(self, layers, *, function_name, struct_name, table_name,
                 output_type):
        self.layers = layers
        self.function_name = function_name
        self.struct_name = struct_name
        self.table_name = table_name
        self.output_type = output_type

    def define_struct(self, *, indent='', file):
        """Dump the table structure definition."""
        file.write('{0}struct {1} {{\n'.format(indent, self.struct_name))
        for n, layer in enumerate(self.layers):
            file.write('{0}    {1} layer{2}[{3}];\n'.format(
                indent, C_TYPES[layer.data.dtype.name], n, len(layer.data)))
        file.write('{0}}};\n'.format(indent))
        
    def declare_table(self, *, qualifiers='', indent='', file):
        """Dump the table declaration."""
        spec = 'struct {}'.format(self.struct_name)
        if qualifiers:
            spec = '{} {}'.format(qualifiers, spec)
        file.write('{0}{1} {2};\n'.format(indent, spec, self.table_name))
        
    def define_table(self, *, qualifiers='', indent='', file):
        """Dump the table definition."""
        spec = 'struct {}'.format(self.struct_name)
        if qualifiers:
            spec = '{} {}'.format(qualifiers, spec)
        file.write('{0}{1} {2} = {{{{\n'.format(indent, spec, self.table_name))
        for n, layer in enumerate(self.layers):
            if n:
                file.write('{0}}},{{\n'.format(indent))
            print_array(
                (str(x) for x in layer.data), indent=indent, compact=True,
                file=file)
        file.write('{0}}}}};\n'.format(indent))
        
    def _function_proto(self, qualifiers):
        """Get the function prototype."""
        spec = self.output_type
        if qualifiers:
            spec = '{} {}'.format(qualifiers, spec)
        return '{} {}(char32_t codepoint)'.format(spec, self.function_name)
    
    def declare_function(self, *, qualifiers='', indent='', file):
        """Dump the function declaration."""
        file.write('{0}{1};\n'.format(indent, self._function_proto(qualifiers)))

    def define_function(self, *, qualifiers='', indent='', file):
        """Dump the function definition."""
        file.write('{0}{1} {{\n'
            .format(indent, self._function_proto(qualifiers)))
        file.write('{0}    unsigned x = codepoint;\n'.format(indent))
        for n, layer in enumerate(self.layers):
            value = 'x'
            if layer.first_bit > 0:
                value = '({} >> {})'.format(value, layer.first_bit)
            if n:
                value = '(v{} << {}) | ({} & 0x{:x})'.format(
                    n - 1, layer.bits, value, (1 << layer.bits) - 1)
            file.write('{0}    unsigned v{1} = {2}.layer{1}[{3}];\n'
                .format(indent, n, self.table_name, value))
        file.write(
            '{0}    return v{1};\n'
            '{0}}}\n'
            .format(indent, len(self.layers) - 1))
