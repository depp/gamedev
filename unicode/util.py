# Copyright 2015-2016 Dietrich Epp.
# This file is part of the Moria Gamedev Repository.  This file is licensed
# under the terms of the MIT license.  For more information, see LICENSE.txt.

def print_array(xs, *, per_line=None, line_width=80,
                indent='    ', file, compact=False):
    """Print a C array, with commas and line breaking.
                
    This just prints the contents of the array, not any of the declaration or
    braces.
    
    Arguments:
    xs -- Literal values to put in the array, a list of strings
    per_line -- Number of values per line, or None for automatic line breaks
    width -- Maximum line width
    indent -- Indentation at the beginning of the line, a string
    file -- Output file
    compact -- Whether to omit spaces between members
    """
    write = file.write
    if per_line is None:
        xs = iter(xs)
        try:
            x = next(xs)
        except StopIteration:
            return
        write(indent)
        write(x)
        pos = len(indent) + len(x)
        try:
            lastx = next(xs)
        except StopIteration:
            write('\n')
            return
        write(',')
        pos += 1
        for x in xs:
            if not compact:
                pos += 1
            k = len(lastx) + 1
            if pos + k > line_width:
                write('\n')
                write(indent)
                pos = len(indent)
            else:
                if not compact:
                    write(' ')
            write(lastx)
            write(',')
            lastx = x
            pos += k
        if not compact:
            pos += 1
        k = len(lastx)
        if pos + k > line_width:
            write('\n')
            write(indent)
        else:
            if not compact:
                write(' ')
        write(lastx)
        write('\n')
    else:
        xs = iter(enumerate(xs))
        try:
            n, x = next(xs)
        except StopIteration:
            return
        write(indent)
        write(x)
        for n, x in xs:
            write(',')
            if (n % per_line) == 0:
                if n:
                    write('\n')
                write(indent)
            else:
                if not compact:
                    write(' ')
            write(x)
        write('\n')

if __name__ == '__main__':
    print_array((str(x) for x in range(200)))
    print_array((str(x) for x in range(200)), compact=True, per_line=10)
