// Copyright 2015-2016 Dietrich Epp.
// This file is part of the Moria Gamedev Repository.  This file is licensed
// under the terms of the MIT license.  For more information, see LICENSE.txt.

#include "linebreak.h"

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma GCC diagnostic ignored "-Wsign-compare"

static void print_breaks(int len, const unsigned *restrict text,
                         const unsigned char *breaks) {
    fputc('x', stderr);
    for (int i = 0; i < len; i++) {
        int b = breaks[i], symbol, white;
        switch (b & (kUcdLinebreakWhite | kUcdLinebreakExpandable)) {
        case 0:
            white = '-';
            break;
        case kUcdLinebreakWhite:
            white = 'W';
            break;
        case kUcdLinebreakWhite | kUcdLinebreakExpandable:
            white = 'E';
            break;
        default:
            white = '?';
            break;
        }
        switch (b & (kUcdLinebreakCanBreak | kUcdLinebreakMustBreak |
                     kUcdLinebreakParagraph)) {
        case 0:
            symbol = 'x';
            break;
        case kUcdLinebreakCanBreak:
            symbol = '-';
            break;
        case kUcdLinebreakCanBreak | kUcdLinebreakMustBreak:
            symbol = '!';
            break;
        case kUcdLinebreakCanBreak | kUcdLinebreakMustBreak |
            kUcdLinebreakParagraph:
            symbol = '@';
            break;
        default:
            symbol = '?';
            break;
        }
        fprintf(stderr, " %04X/%c %c", text[i], white, symbol);
    }
}

static void die_err_func(int line) {
    int ecode = errno;
    fprintf(stderr, "%s:%d: Error: %s\n", __FILE__, line, strerror(ecode));
    exit(1);
}

static void die_func(int line) {
    fprintf(stderr, "%s:%d: Error\n", __FILE__, line);
    exit(1);
}

#define die_err() die_err_func(__LINE__)
#define die() die_func(__LINE__)

int main() {
    int r;
    size_t buflen;
    void *buf;
    {
        FILE *fp = fopen("linebreak_testdata.dat", "rb");
        if (!fp) die_err();
        r = fseek(fp, 0, SEEK_END);
        if (r == -1) die_err();
        long pos = ftell(fp);
        if (pos == -1) die_err();
        if (pos >= (size_t)-1) die();
        buflen = pos;
        if ((buflen & (sizeof(unsigned) - 1)) != 0) die();
        buf = malloc(buflen);
        if (!buf) die_err();
        r = fseek(fp, 0, SEEK_SET);
        if (r == -1) die_err();
        char *cptr, *cend;
        cptr = buf;
        cend = cptr + buflen;
        while (cptr != cend) {
            size_t amt = fread(cptr, 1, cend - cptr, fp);
            if (!amt) {
                if (ferror(fp))
                    die_err();
                else
                    die();
            }
            cptr += amt;
        }
    }

    int failures = 0, count = 0;
    {
        const unsigned *restrict ptr = buf,
                                 *end = ptr + buflen / sizeof(unsigned);
        while (ptr != end) {
            // We allocate everything with malloc() to make the address
            // sanitizer
            // more effective.
            count++;

            if (end - ptr < 2) die();
            unsigned lineno = *ptr++;

            int len;
            {
                unsigned x = *ptr++;
                size_t maxlen = ((size_t)(end - ptr)) / 2;
                if (x < 2 || x > maxlen || x > INT_MAX) die();
                len = x;
            }

            unsigned *restrict input = malloc(len * sizeof(*input));
            if (!input) die_err();
            memcpy(input, ptr, len * sizeof(*input));
            ptr += len;

            unsigned char *restrict ref_output = malloc(len);
            if (!ref_output) die_err();
            for (int i = 0; i < len; i++) {
                ref_output[i] = ptr[i];
            }
            ptr += len;

            unsigned char *restrict test_output = malloc(len);
            if (!test_output) die_err();
            ucd_linebreak_analyze(test_output, input, len);

            if (memcmp(test_output, ref_output, len) != 0) {
                failures++;
                if (failures <= 10) {
                    fprintf(stderr, "Test %d failed\n", lineno);
                    fprintf(stderr, "  Expected: ");
                    print_breaks(len, input, ref_output);
                    fputc('\n', stderr);
                    fprintf(stderr, "  Output:   ");
                    print_breaks(len, input, test_output);
                    fputc('\n', stderr);
                    if (failures >= 10) {
                        fprintf(stderr, "Too many failures\n");
                    }
                }
            }

            free(input);
            free(ref_output);
            free(test_output);
        }
    }

    free(buf);

    if (failures) {
        fprintf(stderr, "Failed: %d / %d\n", failures, count);
        return 1;
    }

    fprintf(stderr, "Passed: %d / %d\n", count, count);
    return 0;
}
