# Copyright 2016 Dietrich Epp.
# This file is part of the Moria Gamedev Repository.  This file is licensed
# under the terms of the MIT license.  For more information, see LICENSE.txt.
import string
import unittest
from ..alphabet import Alphabet

NAMES = ['ascii_letters', 'ascii_lowercase', 'ascii_uppercase', 'digits',
         'hexdigits', 'octdigits', 'printable', 'punctuation', 'whitespace']

class TestAlphabet(unittest.TestCase):
    @classmethod
    def setUpClass(class_):
        print("SETUP")
        class_.alpha = Alphabet()
        class_.sets = []
        for name in NAMES:
            chars = frozenset(getattr(string, name))
            class_.sets.append((chars, class_.alpha.set(chars)))

    def test_universe(self):
        """Test the universal set."""
        for i, c in zip(range(0x110000), self.alpha.universe()):
            self.assertEqual(i, ord(c))

    def test_len(self):
        """Test set length."""
        for xc, xs in self.sets:
            self.assertEqual(len(xc), len(xs))

    def test_iter(self):
        """Test iteration over set contents."""
        for xc, xs in self.sets:
            self.assertEqual(sorted(xc), list(xs))

    def test_in(self):
        """Test 'in' operator."""
        for xc, xs in self.sets:
            for i in range(128):
                c = chr(i)
                self.assertEqual(c in xc, c in xs)

    def test_rel(self):
        """Test relational operators."""
        for xc, xs in self.sets:
            for yc, ys in self.sets:
                self.assertEqual(xc == yc, xs == ys)
                self.assertEqual(xc != yc, xs != ys)
                self.assertEqual(xc < yc, xs < ys)
                self.assertEqual(xc <= yc, xs <= ys)
                self.assertEqual(xc > yc, xs > ys)
                self.assertEqual(xc >= yc, xs >= ys)

    def test_union(self):
        """Test unions."""
        self.assertEqual(self.alpha.union([]), self.alpha.null())
        for i in range(1, 1 << 4):
            tsets = [elem for n, elem in enumerate(self.sets) if i & (1 << n)]
            cs = tsets[0][0].union(*(c for c, _ in tsets[1:]))
            ss = self.alpha.union([s for _, s in tsets])
            self.assertEqual(sorted(cs), list(ss))

    def test_intersection(self):
        """Test intersections."""
        self.assertEqual(self.alpha.intersection([]), self.alpha.universe())
        for i in range(1, 1 << 4):
            tsets = [elem for n, elem in enumerate(self.sets) if i & (1 << n)]
            cs = tsets[0][0].intersection(*(c for c, _ in tsets[1:]))
            ss = self.alpha.intersection([s for _, s in tsets])
            self.assertEqual(sorted(cs), list(ss))

    def test_difference(self):
        """Test differences."""
        for i in range(1, 1 << 4):
            tsets = [elem for n, elem in enumerate(self.sets) if i & (1 << n)]
            cs = tsets[0][0].difference(*(c for c, _ in tsets[1:]))
            ss = self.alpha.difference(tsets[0][1], [s for _, s in tsets[1:]])
            self.assertEqual(sorted(cs), list(ss))

if __name__ == '__main__':
    unittest.main()
