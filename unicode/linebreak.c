// Copyright 2015-2016 Dietrich Epp.
// This file is part of the Moria Gamedev Repository.  This file is licensed
// under the terms of the MIT license.  For more information, see LICENSE.txt.

#include "linebreak.h"

#include <stdbool.h>

#include "linebreak_data.h"

// Unicode line breaking algorithm: UAX #14 (revision 33)
void ucd_linebreak_analyze(unsigned char *attr, const char32_t *codepoint,
                           size_t size) {
    if (!size) {
        return;
    }
    int prev_break = kUcdLinebreakXX;
    bool prev_white;
    bool prev_lb21a = false;
    unsigned cur_state = 0;
    unsigned space_state =
        kUcdLinebreakPair[kUcdLinebreakEquiv[kUcdLinebreakSP]][1];
    char32_t prev_cp;
    {
        char32_t cp = codepoint[0];
        int cur_break = ucd_linebreak_class(cp);
        bool cur_white = (cur_break & 0x80) != 0;
        cur_break &= 0x7f;
        int equiv = kUcdLinebreakEquiv[cur_break];

        // Extended context + pair algorithm
        switch (cur_break) {
        // LB9:  Treat X CM* as if it were X
        // X is not BK, CR, LF, NL, SP, or ZW
        case kUcdLinebreakCM:
            cur_break = kUcdLinebreakAL;
            equiv = kUcdLinebreakEquiv[cur_break];
            cur_state = kUcdLinebreakPair[equiv][0];
            space_state = kUcdLinebreakPair[equiv][1];
            break;

        // LB7: x SP
        case kUcdLinebreakSP:
            cur_state |= space_state;
            break;

        default:
            cur_state = kUcdLinebreakPair[equiv][0];
            space_state = kUcdLinebreakPair[equiv][1];
            prev_break = cur_break;
            break;
        }

        prev_cp = cp;
        prev_white = cur_white;
    }
    for (size_t i = 1; i < size; i++) {
        char32_t cur_cp = codepoint[i];
        int cur_break = ucd_linebreak_class(cur_cp);
        bool cur_white = (cur_break & 0x80) != 0;
        bool cur_lb21a = false;
        cur_break &= 0x7f;
        int equiv = kUcdLinebreakEquiv[cur_break];
        unsigned prev_attr = 0;

        if (prev_white) {
            prev_attr |= kUcdLinebreakWhite;
            if (prev_cp == 0x0020 || prev_cp == 0x00a0) {
                prev_attr |= kUcdLinebreakExpandable;
            }
        }

        // Extended context + pair algorithm
        switch (prev_break) {
        // LB4: BK !
        // LB5 (1): LF !, NL !
        case kUcdLinebreakBK:
            prev_attr |= kUcdLinebreakCanBreak | kUcdLinebreakMustBreak |
                         kUcdLinebreakWhite;
            if (prev_cp == 0x2029) {
                prev_attr |= kUcdLinebreakParagraph;
            }
            goto special;
        case kUcdLinebreakLF:
            prev_attr |= kUcdLinebreakCanBreak | kUcdLinebreakMustBreak |
                         kUcdLinebreakWhite;
            goto special;
        case kUcdLinebreakNL:
            prev_attr |= kUcdLinebreakCanBreak | kUcdLinebreakMustBreak |
                         kUcdLinebreakWhite;
            goto special;

        // LB5 (2): CR x LF, CR !
        case kUcdLinebreakCR:
            if (cur_cp != '\n') {
                prev_attr |= kUcdLinebreakCanBreak | kUcdLinebreakMustBreak |
                             kUcdLinebreakWhite;
            }
            goto special;

        special:
            // LB10: Treate any remaining combining mark as AL.
            if (cur_break == kUcdLinebreakCM) {
                cur_break = kUcdLinebreakAL;
                equiv = kUcdLinebreakEquiv[cur_break];
            }
            cur_state = kUcdLinebreakPair[equiv][0];
            space_state = kUcdLinebreakPair[equiv][1];
            break;

        case kUcdLinebreakSP:
        case kUcdLinebreakZW:
            // LB10: Treate any remaining combining mark as AL.
            switch (cur_break) {
            case kUcdLinebreakCM:
                cur_break = kUcdLinebreakAL;
                equiv = kUcdLinebreakEquiv[cur_break];
                goto standard_pair;

            default:
                goto standard_pair;

            case kUcdLinebreakSP:
                break;
            }
            break;

        default:
            switch (cur_break) {
            // LB9:  Treat X CM* as if it were X
            // X is not BK, CR, LF, NL, SP, or ZW
            case kUcdLinebreakCM:
                if (prev_cp == 0x0020) {
                    cur_break = kUcdLinebreakAL;
                    equiv = kUcdLinebreakEquiv[cur_break];
                    if (cur_state & (1 << equiv)) {
                        prev_attr |= kUcdLinebreakCanBreak;
                    }
                    cur_state = kUcdLinebreakPair[equiv][0];
                    space_state = kUcdLinebreakPair[equiv][1];
                }
                cur_lb21a = prev_lb21a;
                break;

            // LB7: x SP
            case kUcdLinebreakSP:
                cur_state |= space_state;
                break;

            // LB21a: HL (HY | BA) x
            case kUcdLinebreakBA:
            case kUcdLinebreakHY:
                if (prev_break == kUcdLinebreakHL) {
                    cur_lb21a = true;
                }
                goto standard_pair;

            default:
                goto standard_pair;
            }
            break;

        standard_pair:
            if (cur_state & (1 << equiv) && !prev_lb21a) {
                prev_attr |= kUcdLinebreakCanBreak;
            }
            cur_state = kUcdLinebreakPair[equiv][0];
            space_state = kUcdLinebreakPair[equiv][1];
            break;
        }

        attr[i - 1] = prev_attr;
        prev_cp = cur_cp;
        prev_white = cur_white;
        prev_break = cur_break;
        prev_lb21a = cur_lb21a;
    }
    {
        unsigned prev_attr = kUcdLinebreakCanBreak | kUcdLinebreakMustBreak |
                             kUcdLinebreakParagraph;
        if (prev_white) {
            prev_attr |= kUcdLinebreakWhite;
            if (prev_cp == 0x0020 || prev_cp == 0x00a0) {
                prev_attr |= kUcdLinebreakExpandable;
            }
        } else {
            switch (prev_break) {
            case kUcdLinebreakBK:
            case kUcdLinebreakLF:
            case kUcdLinebreakNL:
            case kUcdLinebreakCR:
                prev_attr |= kUcdLinebreakWhite;
                break;
            }
        }
        attr[size - 1] = prev_attr;
    }
}
