# Copyright 2016 Dietrich Epp.
# This file is part of the Moria Gamedev Repository.  This file is licensed
# under the terms of the MIT license.  For more information, see LICENSE.txt.
"""Sets of Unicode characters.

Sets are stored as membership arrays of character classes, which makes the sets
very memory efficient and fairly fast to work with.
"""
import numpy

class Alphabet(object):
    """Unicode alphabet.

    This is basically a compression scheme for Unicode code point membership
    tables, since they can be quite large.

    Attributes:
    codepoint_class -- Array mapping codepoints to equivalency class indexes.
    class_count -- Number of equivalency classes.
    set_count -- Number of sets of characters (distinct SymbolSet instances).
    membership -- Codepoint class membership matrix, indexed by (set, class).
    """
    __slots__ = [
        'codepoint_class', 'class_count', 'set_count', 'membership'
    ]
    def __init__(self):
        self.codepoint_class = numpy.zeros(0x110000, numpy.uint16)
        self.class_count = 1
        self.set_count = 0
        self.membership = numpy.zeros((4, 4), numpy.bool)

    def null(self):
        """Create the null SymbolSet."""
        return self._resolve(numpy.zeros(self.membership.shape[1], numpy.bool))

    def universe(self):
        """Create the SymbolSet containing all characters."""
        classes = numpy.zeros(self.membership.shape[1], numpy.bool)
        classes[:self.class_count] = True
        return self._resolve(classes)

    def set(self, codepoints):
        """Create a new SymbolSet containing the given code points.

        Arguments:
        codepoints -- Boolean membership array for the code points to include
            in the set, or an iterable sequence of characters.
        """
        if not isinstance(codepoints, numpy.ndarray):
            membership = numpy.zeros(0x110000, numpy.bool)
            for c in codepoints:
                membership[ord(c)] = True
            codepoints = membership
        classset = set(numpy.unique(self.codepoint_class[codepoints]))
        classinv = set(numpy.unique(self.codepoint_class[~codepoints]))
        splits = classset.intersection(classinv)
        nset, ncls = self.membership.shape
        while ncls < len(splits) + self.class_count:
            ncls *= 2
        self._resize(nset, ncls)
        for cls in splits:
            newcls = self.class_count
            self.class_count = newcls + 1
            self.codepoint_class[
                (self.codepoint_class == cls) & codepoints] = newcls
            self.membership[:,newcls] = self.membership[:,cls]
            classset.remove(cls)
            classset.add(newcls)
        classes = numpy.zeros(ncls, numpy.bool)
        classes[numpy.array(list(classset), numpy.int)] = True
        return self._resolve(classes)

    def union(self, sets):
        """Compute the union of a sequence of SymbolSet."""
        classes = numpy.zeros(self.membership.shape[1], numpy.bool)
        for set in sets:
            classes |= self.membership[set.index]
        return self._resolve(classes)

    def intersection(self, sets):
        """Compute the intersection of a sequence of SymbolSet."""
        classes = numpy.zeros(self.membership.shape[1], numpy.bool)
        classes[:self.class_count] = True
        for set in sets:
            classes &= self.membership[set.index]
        return self._resolve(classes)

    def difference(self, base, sets):
        """Compute the difference of a sequence of SymbolSet."""
        classes = numpy.array(self.membership[base.index])
        for set in sets:
            classes &= ~self.membership[set.index]
        return self._resolve(classes)

    def _resolve(self, classes):
        """Resolve a boolean character class array as a SymbolSet."""
        clsidx, = numpy.where(numpy.all(self.membership == classes, axis=1))
        if clsidx.size:
            newset = int(clsidx[0])
        else:
            nset, ncls = self.membership.shape
            if nset < self.set_count + 1:
                nset *= 2
            self._resize(nset, ncls)
            newset = self.set_count
            self.set_count = newset + 1
            self.membership[newset] = classes
        return SymbolSet(self, newset)

    def _resize(self, nset, ncls):
        """Resize the membership array if necessary."""
        shape = nset, ncls
        if self.membership.size != shape:
            nset, ncls = self.membership.shape
            m = numpy.zeros(shape, numpy.bool)
            m[:nset,:ncls] = self.membership
            self.membership = m

class SymbolSet(object):
    """Set of Unicode characters.

    Attributes:
    alphabet -- The parent Alphabet object.
    index -- The set index within the Alphabet.
    """
    __slots__ = ['alphabet', 'index']
    def __init__(self, alphabet, index):
        self.alphabet = alphabet
        self.index = index

    def __len__(self):
        a = self.alphabet
        return numpy.count_nonzero(
            a.membership[self.index, a.codepoint_class])

    def __iter__(self):
        a = self.alphabet
        idxs, = numpy.nonzero(a.membership[self.index, a.codepoint_class])
        for idx in idxs:
            yield chr(idx)

    def __contains__(self, c):
        a = self.alphabet
        return a.membership[self.index, a.codepoint_class[ord(c)]]

    def __eq__(self, other):
        if not isinstance(other, SymbolSet):
            return NotImplemented
        if self.alphabet is not other.alphabet:
            return NotImplemented
        return self.index == other.index

    def __ne__(self, other):
        if not isinstance(other, SymbolSet):
            return NotImplemented
        if self.alphabet is not other.alphabet:
            return NotImplemented
        return self.index != other.index

    def __le__(self, other):
        if not isinstance(other, SymbolSet):
            return NotImplemented
        if self.alphabet is not other.alphabet:
            return NotImplemented
        if self.index == other.index:
            return True
        m = self.alphabet.membership
        ax = m[self.index]
        ay = m[other.index]
        return numpy.all(ax <= ay)

    def __ge__(self, other):
        if not isinstance(other, SymbolSet):
            return NotImplemented
        if self.alphabet is not other.alphabet:
            return NotImplemented
        if self.index == other.index:
            return True
        m = self.alphabet.membership
        ax = m[self.index]
        ay = m[other.index]
        return numpy.all(ax >= ay)

    def __lt__(self, other):
        if not isinstance(other, SymbolSet):
            return NotImplemented
        if self.alphabet is not other.alphabet:
            return NotImplemented
        if self.index == other.index:
            return False
        m = self.alphabet.membership
        ax = m[self.index]
        ay = m[other.index]
        return numpy.all(ax <= ay) and numpy.any(ax < ay)

    def __gt__(self, other):
        if not isinstance(other, SymbolSet):
            return NotImplemented
        if self.alphabet is not other.alphabet:
            return NotImplemented
        if self.index == other.index:
            return False
        m = self.alphabet.membership
        ax = m[self.index]
        ay = m[other.index]
        return numpy.all(ax >= ay) and numpy.any(ax > ay)
