// Copyright 2015-2016 Dietrich Epp.
// This file is part of the Moria Gamedev Repository.  This file is licensed
// under the terms of the MIT license.  For more information, see LICENSE.txt.
#pragma once

#include <stddef.h>

typedef unsigned char32_t;

// Line break character attributes.  Each character (code point) in a sequence
// will be assigned a mixture of these flags.
enum {
    // A line break may occur after this character.
    kUcdLinebreakCanBreak = 1u << 0,

    // A line break must occur after this character.  This implies
    // kUcdLinebreakCanBreak and kUcdLinebreakWhite.
    kUcdLinebreakMustBreak = 1u << 1,

    // The break before this character is a paragraph break.  This implies
    // kUcdLinebreakMustBreak and kUcdLinebreakCanBreak.  It also implies
    // kUcdLinebreakWhite, except at the end of the text.
    kUcdLinebreakParagraph = 1u << 2,

    // The character is whitespace, and therefore doesn't contribute to the
    // visual width of a line if it is at the end of the line.
    kUcdLinebreakWhite = 1u << 3,

    // The character is expandable whitespace.  Implies kUcdLinebreakWhite.
    kUcdLinebreakExpandable = 1u << 4,
};

// Analyze line break attributes.  This function always succeeds.  Both arrays
// have the same size.  Each entry in the attribute is filled with a combination
// of the line break attributes above.
void ucd_linebreak_analyze(unsigned char *restrict attr,
                           const char32_t *restrict codepoint, size_t size);
