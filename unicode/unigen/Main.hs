-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
module Main (main) where
import Control.Monad
import qualified Data.Map as Map
import qualified Data.Sequence as Seq
import System.IO
import Unigen.Alphabet
import Unigen.Property.GeneralCategory
import Unigen.Property.LineBreak
import Unigen.Property.GraphemeClusterBreak
import Unigen.GraphemeCluster
import Unigen.LineBreak
import Unigen.DFT
import Unigen.NFT
import Unigen.NFT.Builder (buildNft)

main :: IO ()
main = showDFT

showGC' :: GraphemeClusterOutput -> String
showGC' x = case x of
  NoBoundary -> "x"
  Boundary -> "-"

showGC :: (String, GraphemeClusterOutput) -> String
showGC (rule, x) = rule ++ ":" ++ showGC' x

showDFT :: IO ()
showDFT =
  let nft = buildNft (alphabet [minBound..maxBound]) graphemeCluster
      (dft, errs) = compile nft
  in do
    hPutStrLn stderr "Creating NFT..."
    writeFile "nft.dot" . graphNft show showGC $ nft
    hPutStrLn stderr "Creating DFT..."
    if not $ null errs
      then forM_ errs $ \e -> hPutStr stderr "Error: " >> hPutStrLn stderr e
      else hPutStrLn stderr "No errors."
    writeFile "dft.dot" . graphNft show showGC . dftToNft $ dft
    hPutStrLn stderr "Minimizing DFT..."
    writeFile "min.dot" . graphNft show showGC' . dftToNft .
      minimize . mapDft snd $ dft
    hPutStrLn stderr "Done"
