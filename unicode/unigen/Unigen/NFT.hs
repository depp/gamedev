-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
{-# LANGUAGE ScopedTypeVariables #-}
module Unigen.NFT (
  NFT(..), Edge(..), Input(..), Output(..), graphNft
) where
import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (mapMaybe, catMaybes)
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.Set (Set)
import qualified Data.Set as Set
import Unigen.Alphabet

-- | Nondeterministic finite transducer.
--
-- This maps a sequence of inputs, each with type 'a', to a sequence of outputs,
-- each with type 'b'.  Transducers can be composed in several ways: they can be
-- run in sequence, run in parallel, the output of one can be fed into another,
-- et cetera.
--
-- The initial state is always state 0.
data NFT a b = NFT {
    -- | The input symbol alphabet.
    nftInput :: Alphabet a,
    -- | The output symbol alphabet.
    nftOutput :: Alphabet b,
    -- | The number of states in the NFT.
    nftSize :: Int,
    -- | List of all transitions between states.
    nftEdge :: [Edge],
    -- | Set of final (accepting) states.
    nftFinal :: IntSet
  }

-- | Edge (state transition) in a nondeterministic finite transducer.
data Edge = Edge {
    -- | Source state.
    edSource :: Int,
    -- | Destination state.
    edDest :: Int,
    -- | Transition inputs.
    edInput :: Input,
    -- | Transition outputs.
    edOutput :: Maybe Output
  }

-- | Inputs for a transition in an NFT.
data Input
  -- | Empty string transition.
  = Epsilon
  -- | Empty string transition at start of input sequence.
  | Start
  -- | Empty string transition at end of input sequence.
  | End
  -- | Input symbol.
  | Input Int
  deriving (Show, Eq, Ord)

-- | Output with priority and value.
data Output = Output {
    -- | The relative priority of this output.
    --
    -- Lower values override higher ones.
    outPriority :: Int,
    -- | The output value.
    outValue :: Int
  } deriving (Show, Eq, Ord)

-- | Convert an NFT to a Graphviz graph file.
graphNft :: forall a b. (a -> String) -> (b -> String) -> NFT a b -> String
graphNft showIn showOut nft =
  showString "digraph finite_state_machine {\n" .
  showString "  rankdir=LR;\n" .
  (if IntSet.null (nftFinal nft)
    then id
    else showString "  node [shape = doublecircle];\n  " .
         showString (intercalate " " (map (\s -> "S" ++ show s)
                     (IntSet.toList (nftFinal nft)))) .
         showString ";\n") .
  showString "  node [shape = circle];\n" .
  (foldr (.) id . map showEdge . Map.toList . Map.fromListWith Set.union $
   [((s, d, o), Set.singleton i) | Edge s d i o <- nftEdge nft]) .
  showString "}\n" $ []
  where
    -- Set of all inputs.
    iall :: IntSet
    iall = IntSet.fromList (alphaIndices (nftInput nft))
    -- Size of input alphabet.
    isz :: Int
    isz = IntSet.size iall
    -- Show a set of input symbols without doing anything clever.
    ishow1 :: IntSet -> String
    ishow1 = intercalate "," . map (showIn . alphaValue (nftInput nft)) .
             IntSet.toList
    -- Show a set of input symbols.
    ishow :: IntSet -> Maybe String
    ishow iset =
      let n = IntSet.size iset
      in if n == 0
        then Nothing
        else if n * 2 <= isz
          then Just $ ishow1 iset
          else if n < isz
            then Just $ '!' : ishow1 (IntSet.difference iall iset)
            else Just "*"
    -- Show inputs
    showInput :: Set Input -> String
    showInput ii = intercalate "," . catMaybes $
      [if Set.member e ii then Just s else Nothing
       | (e, s) <- [(Epsilon, "e"), (Start, "^"), (End, "$")]] ++
      [ishow . IntSet.fromList . mapMaybe unbox . Set.toList $ ii]
    -- Show one group of edges in the graph.
    showEdge :: ((Int, Int, Maybe Output), Set Input) -> ShowS
    showEdge ((s, d, o), ii) =
      showString "  S" . shows s . showString " -> S" . shows d .
      showString  " [label = \"" . escape label . showString "\"];\n"
      where
        label :: String
        label = case o of
          Nothing -> showInput ii
          Just (Output prio value) ->
            showInput ii ++ " " ++ show prio ++ ":" ++
            (showOut . alphaValue (nftOutput nft) $ value)
    -- Extract the input symbol.
    unbox :: Input -> Maybe Int
    unbox (Input i) = Just i
    unbox _ = Nothing
    -- Escape the contents of a GraphViz string.
    escape :: String -> ShowS
    escape = foldr (.) id . map escape1
      where
        escape1 '"' = ("\""++)
        escape1 '\\' = ("\\"++)
        escape1 x = (x:)
