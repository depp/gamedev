-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
module Unigen.Property (Property(..), propSet, propSetNot, propAll) where
import qualified Data.Set as Set

-- | A set of non-alias property values.
newtype PropertySet p = PropertySet (Set.Set p)

-- | A Unicode property.
--
-- Each character has a value for each property.  These values are in the
-- Unicode database.
class (Show p, Eq p, Ord p, Enum p, Bounded p) => Property p where

  -- | Get the name of the property in the Unicode database.
  propName :: (t p) -> String

  -- | Expand property aliases to a list of non-aliased properties.
  --
  -- Non-aliased property values should return a list containing only the input
  -- value.  Aliased property values should return a list containing only
  -- other values, and all of those values should expand to themselves.
  --
  -- So `concatMap propExpand` should be idempotent.
  propExpand :: p -> [p]

-- | Convert a list of property values to p set of non-alias property values.
propSet :: Property p => [p] -> Set.Set p
propSet = Set.fromList . concatMap propExpand

-- | Convert a list of property values to a set of all other non-alias values.
--
-- This results in the complement of `propSet`, ignoring aliased values.
propSetNot :: Property p => [p] -> Set.Set p
propSetNot = Set.difference propAll . propSet

-- | Set of all non-alias property values for a given property.
propAll :: Property p => Set.Set p
propAll = propSet [minBound..maxBound]
