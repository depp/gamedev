-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
{-# LANGUAGE Rank2Types, FlexibleContexts, ParallelListComp,
    ScopedTypeVariables #-}
module Unigen.DFT (
  DFT, dftToNft, mapDft, compile, minimize
) where
import Control.Monad.State.Strict
import Control.Monad.Writer.Strict
import Data.Foldable (toList)
import Data.Functor.Identity
import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (mapMaybe, fromJust)
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import qualified Data.Set as Set
import qualified Data.Vector as Vec
import qualified Data.Vector.Unboxed as UVec
import Unigen.Alphabet
import Unigen.NFT

-- | Deterministic finite state transducer.
--
-- This maps a sequence of 'Int' to a sequence of 'b'.  Transducers consist of a
-- set of states.  From each state, each input symbol maps to an output and a
-- new state.
--
-- The array is indexed by @(state, input)@.  The starting state is 1.
data DFT a b = DFT {
    -- | The input symbol alphabet.
    dftInput :: Alphabet a,
    -- | The output symbol alphabet.
    dftOutput :: Alphabet b,
    -- | The number of states in the DFA.
    dftStateCount :: Int,
    -- | The number of input symbols in the DFA, counting EOT as a symbol.
    dftInputCount :: Int,
    -- | Map from (src * inputCount + input) to (dest, output).
    dftEdge :: UVec.Vector (Int, Int)
  }

-- | Get the list of (state, input) indexes in the DFT transition table.
dftIndexes :: DFT a b -> [(Int, Int)]
dftIndexes dft = [(s, i) | s <- [0..ns-1], i <- [0..ni-1]]
  where
    ns = dftStateCount dft
    ni = dftInputCount dft

-- | Get the transition edges in a DFT.
dftEdges :: DFT a b -> [Edge]
dftEdges dft =
  [Edge s d (if i == ni - 1 then End else Input i)
   (if o == -1 then Nothing else Just (Output 0 o))
   | s <- [0..ns-1], i <- [0..ni-1]
   | (d, o) <- UVec.toList (dftEdge dft)]
  where
    ns = dftStateCount dft
    ni = dftInputCount dft

-- | Convert a DFT to an NFT.
dftToNft :: DFT a b -> NFT a b
dftToNft dft = NFT (dftInput dft) (dftOutput dft) (dftStateCount dft)
               (dftEdges dft) IntSet.empty

-- | Map the output in a DFT.
mapDft :: Ord b2 => (b1 -> b2) -> DFT a b1 -> DFT a b2
mapDft f (DFT inp outp1 ns ni tr) = let
  outp2 = alphabet . Set.toList . Set.fromList . map f . alphaValues $ outp1
  mapping :: UVec.Vector Int
  mapping = UVec.replicate ni 0 UVec.//
    [(i, fromJust . alphaIndex outp2 . f $ x) | (i, x) <- alphaAssocs outp1]
  mapo :: Int -> Int
  mapo o = if o < 0 then -1 else mapping UVec.! o
  in DFT inp outp2 ns ni (UVec.map (\(s, o) -> (s, mapo o)) tr)

-- | NFA execution state set.
--
-- Contains one slot for each state in the NFA.  Each slot contains 0 if the
-- machine is not executing that state, 1 if the machine is executing that
-- state but has no output there, and 2 or more indicating the output at that
-- state.  Higher priority outputs get larger values.
newtype NState = NState (UVec.Vector Int) deriving (Show, Eq)

-- | The set of states in the NFA execution state set.
stateSet :: NState -> UVec.Vector Bool
stateSet (NState st) = UVec.map (0 /=) st

-- | The empty NFA execution state set.
emptyState :: Int -> NState
emptyState n = NState $ UVec.replicate n 0

-- | Compute the union of two NFA execution state sets.
unionState :: NState -> NState -> NState
unionState (NState x) (NState y) = NState $ UVec.zipWith max x y

-- | Remove output from the NFA execution state set.
clearOutput :: NState -> NState
clearOutput (NState st) = NState $ UVec.map (\x -> if x == 0 then 0 else 1) st

-- | Set of edges in an NFA, with no information about input.
--
-- Maps individual NFA states to new NFA execution state sets.
newtype EdgeSet = EdgeSet (Vec.Vector (Maybe NState)) deriving (Show, Eq)

-- | Compute the union of two NFA edge sets.
unionEdgeSet :: EdgeSet -> EdgeSet -> EdgeSet
unionEdgeSet (EdgeSet x) (EdgeSet y) = EdgeSet $ Vec.zipWith u x y
  where
    u :: Maybe NState -> Maybe NState -> Maybe NState
    u xx yy = case xx of
      Nothing -> yy
      Just xv -> case yy of
        Nothing -> xx
        Just yv -> Just $ unionState xv yv

-- | Traverse an edge set from an execution state set producing a new state set.
followEdges :: Int -> EdgeSet -> NState -> NState
followEdges n (EdgeSet smap) (NState st) =
  if null sts then emptyState n else foldr1 unionState sts
  where
    sts :: [NState]
    sts = mapMaybe t1 . zip [0..] . UVec.toList $ st
    t1 :: (Int, Int) -> Maybe NState
    t1 (s, o)
      | o == 0 = Nothing
      | o == 1 = smap Vec.! s
      | otherwise = case smap Vec.! s of
        Nothing -> Nothing
        Just (NState st') ->
          Just $ NState $ UVec.map (\o' -> if o' == 0 then 0 else max o o') st'

-- | Map between outputs and integers used in NFA execution state sets.
data OutputMap = OutputMap (Map Output Int) (Vec.Vector (Maybe Int))

-- | Create a map from outputs to integers used in NFA execution state sets.
outputMap :: NFT a b -> OutputMap
outputMap nft = let
  outs :: [Output]
  outs = Set.toDescList . Set.fromList . mapMaybe (\(Edge _ _ _ o) -> o) .
         nftEdge $ nft
  in OutputMap (Map.fromList $ zip outs [2..])
     (Vec.fromList $ Nothing : Nothing : map (\(Output _ x) -> Just x) outs)

-- | Convert an output to an NFA execution state value.
outputToIndex :: OutputMap -> Maybe Output -> Int
outputToIndex (OutputMap m _) x = case x of
  Nothing -> 1
  Just xv -> case Map.lookup xv m of
    Nothing -> error "outputToIndex"
    Just y -> y

-- | Convert an NFA execution state value to an output.
indexToOutput :: OutputMap -> Int -> Maybe Int
indexToOutput (OutputMap _ a) i = a Vec.! i

-- | Get the edge sets for an NFA.
--
-- Returns (epsilon, start, end, input) edge sets.
edgeSets :: NFT a b -> OutputMap ->
            (EdgeSet, EdgeSet, EdgeSet, Vec.Vector EdgeSet)
edgeSets nft omap = let
  -- Number of NFT states.
  size :: Int
  size = nftSize nft
  -- Map from Input to map from state to edges (dest, out) from that state.
  symMap :: Map Input (IntMap (Seq (Int, Int)))
  symMap = Map.fromListWith (IntMap.unionWith (Seq.><))
           [(i, IntMap.singleton s (Seq.singleton (d, (outputToIndex omap o))))
            | Edge s d i o <- nftEdge nft]
  -- Zero NFA execution state.
  zeroState :: UVec.Vector Int
  zeroState = UVec.replicate size 0
  -- Zero edge set.
  zeroEdge :: Vec.Vector (Maybe NState)
  zeroEdge = Vec.replicate size Nothing
  -- Identity edge set.
  identityEdge :: Vec.Vector (Maybe NState)
  identityEdge = Vec.generate size $ \i -> Just $ NState $
                 UVec.generate size $ \j -> if i == j then 1 else 0
  -- Create an NFA execution state set.
  mkNState :: Seq (Int, Int) -> NState
  mkNState = NState . (zeroState UVec.//) . IntMap.toList .
             IntMap.fromListWith max . toList
  -- Create an edge set from an entry in symMap.
  mkEdgeSet :: IntMap (Seq (Int, Int)) -> EdgeSet
  mkEdgeSet m = EdgeSet $ zeroEdge Vec.//
                [(j, Just $ mkNState x) | (j, x) <- IntMap.toList m]
  -- Map from input alphabet to edges.
  input :: Vec.Vector EdgeSet
  input = Vec.generate (alphaIndexSize (nftInput nft)) $ \i ->
    case Map.lookup (Input i) symMap of
      Nothing -> EdgeSet $ zeroEdge
      Just m -> mkEdgeSet m
  -- Create an edge set for a special input (one of the epsilon inputs).
  special :: [Input] -> EdgeSet
  special = closure . mkEdgeSet . IntMap.unionsWith (Seq.><) .
            mapMaybe (flip Map.lookup symMap)
  -- Calculate transitive closure of zero or more applications of edge set.
  closure :: EdgeSet -> EdgeSet
  closure = closure' . unionEdgeSet (EdgeSet identityEdge)
    where
      closure' :: EdgeSet -> EdgeSet
      closure' es@(EdgeSet smap) = let
        es' = EdgeSet $ Vec.map (fmap (followEdges size es)) smap
        in if es' == es then es else closure' es'
  in (special [Epsilon], special [Epsilon, Start],
      special [Epsilon, End], input)

data DEdge = DEdge !Int !Int !Int !Int deriving Show
data NQueue = NQueue !Int !Int !NState !NQueue | NEmpty
data Lookahead = AlwaysAccept | AlwaysReject | Ambiguous
type LookupCache = Map (UVec.Vector Bool) Lookahead
type ErrM = WriterT (Seq Err) Identity
type LookupM = StateT LookupCache ErrM

compile :: forall a b. (Show a, Show b) => NFT a b -> (DFT a b, [String])
compile nft = let
  -- Number of input symbols, not counting EOT.
  isize :: Int
  isize = alphaIndexSize (nftInput nft)
  -- Number of states in NFT.
  nsize :: Int
  nsize = nftSize nft
  -- Output symbol map.
  omap :: OutputMap
  omap = outputMap nft
  -- Sets of NFA edges.
  epsilon :: EdgeSet
  start :: EdgeSet
  end :: EdgeSet
  input :: Vec.Vector EdgeSet
  (epsilon, start, end, input) = edgeSets nft omap
  -- NFA final states
  nfinal :: UVec.Vector Bool
  nfinal = UVec.replicate nsize False UVec.//
          (zip (IntSet.toList $ nftFinal nft) (repeat True))
  -- Test if a state set is final.
  isFinal :: UVec.Vector Bool -> Bool
  isFinal = UVec.or . UVec.zipWith (&&) nfinal
  -- Get the output for an end of input state.
  evalEndSt :: NState -> Int
  evalEndSt (NState st) = max 1 . UVec.maximum .
            UVec.zipWith (\f n -> if f then n else 0) nfinal $ st
  -- Create the DFT.
  mkDft :: forall r m. Monad m =>
           NState -> Bool -> (Int -> Int -> NState -> m Int) ->
           (Int -> [Int] -> [DEdge] -> m r) -> m r
  mkDft initstate isStart evalSt k =
    mkDft1 (NQueue (-1) (-1) initstate NEmpty) Map.empty [] []
    where
      mkDft1 :: NQueue -> Map (UVec.Vector Bool) Int -> [Int] -> [DEdge] -> m r
      mkDft1 (NQueue qsrc qinput qstate qq) stmap dfinal dedge = let
        -- New NFA execution state set.
        state :: NState
        state = followEdges nsize
                (if qsrc < 0 && isStart then start else epsilon) .
                clearOutput $ qstate
        -- New NFA state set.
        sts :: UVec.Vector Bool
        sts = stateSet qstate
        -- New edge, given the current state.
        newedge :: Int -> m DEdge
        newedge stidx = liftM (DEdge qsrc stidx qinput)
                        (evalSt stidx qinput qstate)
        in case Map.lookup sts stmap of
          Just ndest -> do
            d <- newedge ndest
            mkDft1 qq stmap dfinal (d : dedge)
          Nothing -> let
            -- New DFA state.
            stidx :: Int
            stidx = Map.size stmap
            -- The state if the input ends here.
            endState :: NState
            endState = followEdges nsize end state
            in do
              d <- if qsrc < 0
                then return dedge
                else liftM (: dedge) (newedge stidx)
              let est = evalEndSt endState
              mkDft1
                (foldr (\i -> NQueue stidx i
                              (followEdges nsize (input Vec.! i) state))
                 qq [0..isize-1])
                (Map.insert sts stidx stmap)
                (if isFinal $ stateSet endState
                  then stidx : dfinal else dfinal)
                (DEdge stidx stidx isize est : d)
      mkDft1 _ m dfinal dedge = k (Map.size m) dfinal dedge
  -- Write an error message.
  err :: MonadWriter (Seq Err) m => Err -> m ()
  err = tell . Seq.singleton
  -- Calculate the lookahead for a set of states (cached).
  lookahead :: UVec.Vector Bool -> LookupM Lookahead
  lookahead st =
    state $ \cache -> case Map.lookup st cache of
      Just x -> (x, cache)
      Nothing ->
        runIdentity $
        mkDft (NState (UVec.map (\x -> if x then 1 else 0) st))
        False (\_ _ _ -> return 0) $ \n f e -> Identity $ let
          v = if null f
            then AlwaysReject
            else let f' = UVec.replicate n False UVec.//
                          zip f (repeat True)
                 in if UVec.and f' then AlwaysAccept else Ambiguous
          in (v, Map.insert st v cache)
  -- Map state to output value.
  mapOutput :: Int -> Int -> NState -> LookupM Int
  mapOutput stidx input (NState st) = let
    f :: UVec.Vector Int -> LookupM Int
    f a = if UVec.null a
      then return 1
      else do
        let v = UVec.maximum a
        la <- lookahead (UVec.map (v ==) st)
        case la of
          AlwaysAccept -> return v
          AlwaysReject -> return 1
          Ambiguous -> err (AmbiguousOutput stidx input v [] []) >> return v
    in f . UVec.filter (>= 2) $ st
  -- Create the actual DFT.
  dft :: DFT a b
  errs :: Seq Err
  (dft, errs) = runWriter $ do
    (dft, _) <- let
      initstate :: NState
      initstate = NState $ UVec.generate nsize (\i -> if i == 0 then 1 else 0)
      buildDft :: Int -> [Int] -> [DEdge] -> LookupM (DFT a b)
      buildDft dsize dfinal dedge = return $
        DFT (nftInput nft) (nftOutput nft) dsize (isize + 1) $
        UVec.replicate (dsize * (isize + 1)) (-1, -1) UVec.//
        [(s * (isize + 1) + i, (d, maybe (-1) id $ indexToOutput omap o))
         | DEdge s d i o <- dedge]
      in runStateT (mkDft initstate True mapOutput buildDft) Map.empty
    return dft
  in (dft, map (showError dft) . toList $ errs)

-- | A DFT compilation error.
data Err
  -- | Input is not accepted.  Field is the not accepting state.
  = RejectedInput Int
  -- | Different paths emit or do not emit output.
  --
  -- Fields are state, input, output, positive lookahead, negative lookahead
  | AmbiguousOutput Int Int Int [Int] [Int]

-- | Convert a DFT compilation error to human-readable foramt.
showError :: (Show a, Show b) => DFT a b -> Err -> String
showError dft e = case e of
  RejectedInput st ->
    showString "Input is rejected: " .
    showPath dft (findPath1 dft st) $ ""
  AmbiguousOutput st i o _ _ ->
    showString "Ambiguous output transition: context=" .
    showPath dft (findPath1 dft st) .
    showString " input=" .
    shows i .
    showString " output=" .
    shows o $ ""

-- Find the path to a specific DFA state.
findPath1 :: DFT a b -> Int -> Maybe [Int]
findPath1 dft goal = findPath dft (UVec.generate (dftStateCount dft) (goal ==))

-- Find the path to any DFA state in a set.
findPath :: DFT a b -> UVec.Vector Bool -> Maybe [Int]
findPath dft goal = let
  ssize = dftStateCount dft
  isize = dftInputCount dft
  nextSt :: Int -> Int -> Int
  nextSt st i = fst $ (dftEdge dft) UVec.! (st * isize + i)
  findPath' :: IntSet -> Seq (Int, [Int]) -> Maybe [Int]
  findPath' marked queue = case Seq.viewl queue of
    Seq.EmptyL -> Nothing
    (st, path) Seq.:< queue' ->
      if IntSet.member st marked
        then findPath' marked queue'
        else if goal UVec.! st
          then Just path
          else findPath' (IntSet.insert st marked) $ queue' Seq.><
               Seq.fromList [(nextSt st i, i : path) | i <- [0..isize-1]]
  in findPath' IntSet.empty (Seq.singleton (0, []))

showPath :: Show a => DFT a b -> Maybe [Int] -> ShowS
showPath dft path = case path of
  Nothing -> showString "(no path!)"
  Just inputs -> showString $ intercalate " " . map showIn $ inputs
    where
      showIn = show . alphaValue (dftInput dft)

minimize :: DFT a b -> DFT a b
minimize = undefined

{-
-- | Minimize a deterministic finite transducer.
minimize :: forall b . Ord b => DTransducer b -> DTransducer b
minimize (DTransducer arr) =
  groupStates $ mkGroups (listArray (s0, s1) (repeat 1))
  where
    ((s0, i0), (s1, i1)) = bounds arr
    mkGroups :: Array Int Int -> Array Int Int
    mkGroups groups = let
      cell :: Int -> Int -> (Int, Maybe b)
      cell s i = let (d, o) = arr ! (s, i) in (groups ! d, o)
      groups' :: Array Int Int
      groups' = listArray (s0, s1) $ split s0 Map.empty
      split :: Int -> Map (Array Int (Int, Maybe b)) Int -> [Int]
      split s m = let
        table :: Array Int (Int, Maybe b)
        table = listArray (i0, i1) $ map (cell s) [i0..i1]
        in case Map.lookup table m of
          Just g -> g : split (s + 1) m
          Nothing -> let g = Map.size m + 1
                     in g : split (s + 1) (Map.insert table g m)
      in if groups /= groups'
        then mkGroups groups'
        else groups
    groupStates :: Array Int Int -> DTransducer b
    groupStates groups = let
      g0 = minimum groups
      g1 = maximum groups
      igroups :: Array Int Int
      igroups = array (g0, g1) [(g, s) | (s, g) <- assocs groups]
      cell :: Int -> Int -> Int -> ((Int, Int), (Int, Maybe b))
      cell g s i = let (d, o) = arr ! (s, i) in ((g, i), (groups ! d, o))
      in DTransducer . array ((g0, i0), (g1, i1)) $
         [cell g s i | (g, s) <- assocs igroups, i <- [i0..i1]]
-}
