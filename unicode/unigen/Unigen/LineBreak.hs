-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
module Unigen.LineBreak (LineBreakOutput(..), lineBreak) where
import Prelude hiding (read)
import Unigen.NFT.Builder
import Unigen.Property.GeneralCategory (GeneralCategory(..))
import Unigen.Property.LineBreak (LineBreak(..))

-- | Attribute outputs for line breaking algorithm.
data LineBreakOutput
  -- | No break may appear before this character.
  = NoBreak
  -- | A break may appear before this character.
  | CanBreak
  -- | A break must appear before this character.
  | MustBreak
  deriving (Show, Eq, Ord)

infixr 6 ×, ÷, !

-- | Breaks are prohibited between the left and right expressions.
(×) :: NFTBuilder a LineBreakOutput -> NFTBuilder a LineBreakOutput ->
       NFTBuilder a LineBreakOutput
a × b = a ~> write NoBreak ~> b

-- | Breaks are permitted between the left and right expressions.
(÷) :: NFTBuilder a LineBreakOutput -> NFTBuilder a LineBreakOutput ->
       NFTBuilder a LineBreakOutput
a ÷ b = a ~> write CanBreak ~> b

-- | Breaks are mandatory between the left and right expressions.
(!) :: NFTBuilder a LineBreakOutput -> NFTBuilder a LineBreakOutput ->
       NFTBuilder a LineBreakOutput
a ! b = a ~> write MustBreak ~> b

-- | Match any number of spaces.
space :: NFTBuilder LineBreak a
space = many $ readElem [SP]

-- | A machine which breaks lines according UAX #14.
--
-- This is created using the tailorable rules as input.
lineBreak :: NFTBuilder LineBreak (String, LineBreakOutput) ->
             NFTBuilder LineBreak (String, LineBreakOutput)
lineBreak tailoredRules =
      rule "LB2" (start × empty)
  <+> rule "LB3" (empty ! end)
  <+> rule "LB4" (p [BK] ! empty)
  <+> rule "LB5" (p [CR] × p [LF] <+> p [CR, LF, NL] ! empty)
  <+> rule "LB6" (empty × p [BK, CR, LF, NL])
  <+> rule "LB7" (empty × p [SP, ZW])
  <+> rule "LB8" (p [ZW] ~> space ÷ empty)
  <+> rule "LB8a" (p [ZWJ] × p [ID, EB, EM])
  -- rule LB9, LB10
  <+> (((many readAny) ~>
        (    p [BK, CR, LF, NL, SP, ZW] ~> write (Just AL) ~> p [CM, ZWJ]
         <+> write Nothing ~> p [CM, ZWJ]
         <+> liftT Just) ~>
        (many readAny))
       >>> maybeInput (    rule "LB11" (empty × p [WJ] × empty)
                       <+> rule "LB12" (p [GL] × empty)
                       <+> tailoredRules))
  <+> rule "LB999" (write NoBreak)

-- | A machine which breaks lines according to the default tailorable rules.
lineBreakDefault :: NFTBuilder LineBreak (String, LineBreakOutput)
lineBreakDefault =
      rule "LB12a" (pNot [SP, BA, HY] × p [GL])
  <+> rule "LB13" (empty × p [CL, CP, EX, IS, SY])
  <+> rule "LB14" (p [OP] ~> space × empty)
  <+> rule "LB15" (p [QU] ~> space × p [OP])
  <+> rule "LB16" (p [CL, CP] ~> space × p [NS])
  <+> rule "LB17" (p [B2] ~> space × p [B2])
  <+> rule "LB18" (p [SP] ÷ empty)
  <+> rule "LB19" (empty × p [QU] × empty)
  <+> rule "LB20" (empty ÷ p [CB] ÷ empty)
  <+> rule "LB21" (empty × p [BA, HY, NS] <+> p [BB] × empty)
  <+> rule "LB21a" (p [HL] ~> p [HY, BA] × empty)
  <+> rule "LB21b" (p [SY] × p [HL])
  <+> rule "LB22" (p [AL, HL, EX, ID, EB, EM, IN, NU] × p [IN])
  <+> rule "LB23" (p [AL, HL] × p [NU] <+> p [NU] × p [AL, HL])
  <+> rule "LB23a" (p [PR] × p [ID, EB, EM] <+> p [ID, EB, EM] ~> p [PO])
  <+> rule "LB24" (p [PR, PO] × p [AL, HL] <+> p [AL, HL] × p [PR, PO])
  -- alternative numeric rule
  <+> (if False
       then rule "Numeric" $
         optional (p [PR, PO] × empty) ~>
         optional (p [OP, HY] × empty) ~>
         p [NU] ~>
         many (empty × p [NU, SY, IS]) ~>
         optional (empty × p [CL, CP]) ~>
         optional (empty × p [PR, PO])
       else empty)
  <+> rule "LB25" (    p [CL, CP, NU] × p [PO, PR]
                   <+> p [PO, PR] × p [OP, NU]
                   <+> p [HY, IS, NU, SY] × p [NU])
  <+> rule "LB26" (    p [JL] × p [JL, JV, H2, H3]
                   <+> p [JV, H2] × p [JV, JT]
                   <+> p [JT, H3] × p [JT])
  <+> rule "LB27" (    p [JL, JV, JT, H2, H3] × p [IN, PO]
                   <+> p [PR] × p [JL, JV, JT, H2, H3])
  <+> rule "LB28" (p [AL, HL] × p [AL, HL])
  <+> rule "LB29" (p [IS] × p [AL, HL])
  <+> rule "LB30" (p [AL, HL, NU] × p [OP] <+> p [CP] × p [AL, HL, NU])
  <+> rule "LB30a" ((start <+> pNot [RI]) ~> many (p [RI] ~> p [RI])
                    ~> p [RI] × p [RI])
  <+> rule "LB30b" (p [EB] × p [EM])
  <+> rule "LB31" (empty ÷ empty)

p :: [LineBreak] -> NFTBuilder LineBreak a
p = readElem

pNot :: [LineBreak] -> NFTBuilder LineBreak a
pNot = readNotElem
