-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
module Unigen.GraphemeCluster (
  GraphemeClusterOutput(..), graphemeCluster
) where
import Prelude hiding (read)
import Unigen.NFT.Builder
import Unigen.Property.GraphemeClusterBreak (GraphemeClusterBreak(..))

-- | Attribute outputs for a text segmentation analyzer.
data GraphemeClusterOutput
  -- | There is no boundary before this character.
  = NoBoundary
  -- | There is a boundary before this character.
  | Boundary
  deriving (Show, Eq, Ord)

infixr 6 ×, ÷

-- | No boundary appears between the left and right expressions.
(×) :: NFTBuilder a GraphemeClusterOutput ->
       NFTBuilder a GraphemeClusterOutput ->
       NFTBuilder a GraphemeClusterOutput
a × b = a ~> write NoBoundary ~> b

-- | A boundary appears between the left and right expressions.
(÷) :: NFTBuilder a GraphemeClusterOutput ->
       NFTBuilder a GraphemeClusterOutput ->
       NFTBuilder a GraphemeClusterOutput
a ÷ b = a ~> write Boundary ~> b

-- | The machine for marking grapheme cluster attributes.
graphemeCluster :: NFTBuilder GraphemeClusterBreak
                   (String, GraphemeClusterOutput)
graphemeCluster =
      rule "GB1" (start ÷ empty)
  <+> rule "GB2" (empty ÷ end)
  <+> rule "GB3" (p [CR] × p [LF])
  <+> rule "GB4" (p [CN, CR, LF] ÷ empty)
  <+> rule "GB5" (empty ÷ p [CN, CR, LF])
  <+> rule "GB6" (p [L] × p [L, V, LV, LVT])
  <+> rule "GB7" (p [LV, V] × p [V, T])
  <+> rule "GB8" (p [LVT, T] × p [T])
  <+> rule "GB9" (empty × p [EX, ZWJ])
  <+> rule "GB9a" (empty × p [SM])
  <+> rule "GB9b" (p [PP] × empty)
  <+> rule "GB10" (p [EB, EBG] ~> many (p [EX]) × p [EM])
  <+> rule "GB11" (p [ZWJ] × p [GAZ, EBG])
  <+> rule "GB12" (start ~> many (p [RI] ~> p [RI]) ~> p [RI] × p [RI])
  <+> rule "GB13" (pNot [RI] ~> many (p [RI] ~> p [RI]) ~> p [RI] × p [RI])
  <+> rule "GB999" (empty ÷ empty)

p :: [GraphemeClusterBreak] -> NFTBuilder GraphemeClusterBreak a
p = readElem

pNot :: [GraphemeClusterBreak] -> NFTBuilder GraphemeClusterBreak a
pNot = readNotElem
