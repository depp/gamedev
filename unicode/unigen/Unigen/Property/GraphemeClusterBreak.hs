-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
module Unigen.Property.GraphemeClusterBreak (GraphemeClusterBreak(..)) where
import Unigen.Property (Property(..))

-- | Unicode Grapheme Cluster Break property.
data GraphemeClusterBreak
  = CN | CR | EB | EBG | EM | EX | GAZ | L | LF | LV | LVT | PP | RI | SM | T
  | V | XX | ZWJ
  deriving (Show, Eq, Ord, Enum, Bounded)

instance Property GraphemeClusterBreak where
  propName _ = "GCB"
  propExpand x = [x]
