-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
module Unigen.Property.GeneralCategory (GeneralCategory(..)) where
import Unigen.Property (Property(..))

-- | Unicode General Category character property.
data GeneralCategory
  = C | Cc | Cf | Cn | Co | Cs | L | LC | Ll | Lm | Lo | Lt | Lu | M | Mc | Me
  | Mn | N | Nd | Nl | No | P | Pc | Pd | Pe | Pf | Pi | Po | Ps | S | Sc | Sk
  | Sm | So | Z | Zl | Zp | Zs
  deriving (Show, Eq, Ord, Enum, Bounded)

instance Property GeneralCategory where
  propName _ = "gc"
  propExpand x = case x of
    C -> [Cc, Cf, Cn, Co, Cs]
    L -> [Ll, Lm, Lo, Lt, Lu]
    LC -> [Ll, Lt, Lu]
    M -> [Mc, Me, Mn]
    N -> [Nd, Nl, No]
    P -> [Pc, Pd, Pe, Pf, Pi, Po, Ps]
    S -> [Sc, Sk, Sm, So]
    Z -> [Zl, Zp, Zs]
    v -> [v]
