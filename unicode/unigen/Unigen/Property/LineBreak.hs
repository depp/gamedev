-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
module Unigen.Property.LineBreak (LineBreak(..)) where
import Unigen.Property (Property(..))

-- | Unicode line breaking classes.
data LineBreak
  = AI | AL | B2 | BA | BB | BK | CB | CJ | CL | CM | CP | CR | EB | EM | EX
  | GL | H2 | H3 | HL | HY | ID | IN | IS | JL | JT | JV | LF | NL | NS | NU
  | OP | PO | PR | QU | RI | SA | SG | SP | SY | WJ | XX | ZW | ZWJ
  deriving (Show, Eq, Ord, Enum, Bounded)

instance Property LineBreak where
  propName _ = "lb"
  propExpand x = [x]
