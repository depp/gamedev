-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
module Unigen.Alphabet (
  Alphabet, alphabet, assocAlphabet,
  alphaIndexSize, alphaSize,
  alphaIndex, alphaValue, alphaValues, alphaAssocs, alphaIndices
) where
import Data.Vector (Vector, (//), (!))
import qualified Data.Vector as Vec
import Data.Map (Map)
import Data.Maybe (isJust)
import qualified Data.Map as Map

-- | An input or output symbol alphabet.
data Alphabet a = A (Map a Int) (Vector (Maybe a))

instance Show a => Show (Alphabet a) where
  showsPrec _ alpha = showString "assocAlphabet " . shows (alphaAssocs alpha)

-- | Create an alphabet from a list of symbols.
alphabet :: Ord a => [a] -> Alphabet a
alphabet xs = assocAlphabet (zip [0..] xs)

-- | Create an alphabet from a association between indices and symbols.
assocAlphabet :: Ord a => [(Int, a)] -> Alphabet a
assocAlphabet [] = A Map.empty Vec.empty
assocAlphabet xs = let
  n = length xs
  ni = maximum (map fst xs) + 1
  m = Map.fromList [(x, i) | (i, x) <- xs]
  a = Vec.replicate ni Nothing // [(i, Just x) | (i, x) <- xs]
  in if length (filter isJust (Vec.toList a)) /= n ||
        Map.size m /= n
    then error "assocAlphabet"
    else A m a

-- | Get the size of the index array for an alphabet.
--
-- This is the minimum size of a vector whose indexes are a subset of the
-- indexes used in the alphabet.
alphaIndexSize :: Alphabet a -> Int
alphaIndexSize (A _ a) = Vec.length a

-- | Get the number of symbols in an alphabet.
alphaSize :: Alphabet a -> Int
alphaSize (A m _) = Map.size m

-- | Map alphabet values to one-based integer indexes.
alphaIndex :: Ord a => Alphabet a -> a -> Maybe Int
alphaIndex (A m _) x = Map.lookup x m

-- | Map zero-based integer indexes to alphabet values.
alphaValue :: Alphabet a -> Int -> a
alphaValue (A _ a) i = case a ! i of
  Just x -> x
  Nothing -> error "alphaValue"

-- | Get a list of all values in the alphabet.
alphaValues :: Alphabet a -> [a]
alphaValues (A m _) = Map.keys m

-- | Get the associations between indexes and values in the alphabet.
alphaAssocs :: Alphabet a -> [(Int, a)]
alphaAssocs (A m _) = [(i, x) | (x, i) <- Map.toList m]

-- | Get a list of all indices in the alphabet.
alphaIndices :: Alphabet a -> [Int]
alphaIndices (A m _) = Map.elems m
