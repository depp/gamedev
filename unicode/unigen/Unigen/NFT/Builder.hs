-- Copyright 2016 Dietrich Epp.
-- This file is part of the Moria Gamedev Repository.  This file is licensed
-- under the terms of the MIT license.  For more information, see LICENSE.txt.
{-# LANGUAGE Rank2Types, ScopedTypeVariables, FlexibleContexts #-}
module Unigen.NFT.Builder (
  NFTBuilder,
  buildNft,
  empty, read, readAny, readElem, readNotElem, write,
  start, end, many, optional, liftT, maybeInput, (~>), (<+>), (>>>),
  rule,
) where
import Data.Foldable (toList)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (mapMaybe)
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Data.Vector.Unboxed (Vector, (//), (!))
import qualified Data.Vector.Unboxed as Vec
import Unigen.Alphabet
import Unigen.NFT

-- | Builder for a finite state transducer.
newtype NFTBuilder a b
  = T (forall r. Alphabet a -> St b -> (Seq Edge -> St b -> r) -> r)

-- | State of partially built nondeterministic finite transducer.
data St b = St {
    -- | Highest index assigned to any state.
    stState :: Int,
    -- | Highest priority value assigned to any output transition.
    stPriority :: Int,
    -- | Set of final states.
    --
    -- Note that in general, these states may have transitions to other states
    -- earlier in the machine.
    stFinal :: IntSet,
    -- | Map from output symbols to indexes.
    stOutput :: Map b Int
  }

-- | Run an NFT builder, creating the NFT.
buildNft :: Ord b => Alphabet a -> NFTBuilder a b -> NFT a b
buildNft inp (T t) =
  t inp (St 0 0 (IntSet.singleton 0) Map.empty) $
  \edge (St n _ fin om) ->
  NFT inp (assocAlphabet [(i, x) | (x, i) <- Map.toList om])
  (n + 1) (toList edge) fin

-- | Transducer that matches the empty string.
--
-- This is the identity for '~>', so @empty ~> x@ and @x ~> emtpy@ are both
-- equivalent to @x@.
empty :: NFTBuilder a b
empty = T $ \_ st k -> k Seq.empty st

-- | Create input transitions from a set of states to a single state.
inputEdge :: Input -> IntSet -> Int -> Seq Edge
inputEdge x ss n = Seq.fromList [Edge s n x Nothing | s <- IntSet.toList ss]

-- | Read input symbols with the given set of indexes in the alphabet.
readT :: IntSet -> St b -> (Seq Edge -> St b -> r) -> r
readT xx (St n p fin om) k =
  if IntSet.size fin == 1
    then let
      n2 = IntSet.findMin fin
      n3 = n + 1
      in k (Seq.fromList [Edge n2 n3 (Input i) Nothing
                          | i <- IntSet.toList xx])
           (St n3 p (IntSet.singleton n3) om)
    else let
      n2 = n + 1
      n3 = n + 2
      in k (inputEdge Epsilon fin n2 Seq.><
            Seq.fromList [Edge n2 n3 (Input i) Nothing
                          | i <- IntSet.toList xx])
           (St n3 p (IntSet.singleton n3) om)

-- | Read an input symbol that matches a predicate.
readFilter :: (a -> Bool) -> NFTBuilder a b
readFilter f = T $ readT . IntSet.fromList . map fst . filter (f . snd) .
               alphaAssocs

-- | Read any input symbol.
readAny :: NFTBuilder a b
readAny = T $ readT . IntSet.fromList . alphaIndices

-- | Read any input symbol from the given list.
readElem :: Ord a => [a] -> NFTBuilder a b
readElem xs = T $ readT . IntSet.fromList . flip mapMaybe xs . alphaIndex

-- | Read any input symbol not from the given list.
readNotElem :: Ord a => [a] -> NFTBuilder a b
readNotElem xs =
  T $ \alpha -> readT .
  IntSet.difference (IntSet.fromList . alphaIndices $ alpha) .
  IntSet.fromList . flip mapMaybe xs . alphaIndex $ alpha

-- | Write an output symbol.
write :: Ord b => b -> NFTBuilder a b
write x =
  T $ \inp st@(St n p fin om) k -> let
    n2 = n + 1
    n3 = n + 2
    p2 = p + 1
    (om', o) = case Map.lookup x om of
      Just i -> (om, i)
      Nothing -> let n = Map.size om + 1 in (Map.insert x n om, n)
    in k (inputEdge Epsilon fin n2 Seq.|>
          Edge n2 n3 Epsilon (Just (Output p2 o)))
         (St n3 p2 (IntSet.singleton n3) om')

-- | Append a new final state with input transitions from previous final states.
specialT :: Input -> St b -> (Seq Edge -> St b -> r) -> r
specialT x (St n p fin om) k = let
  n2 = n + 1
  in k (inputEdge x fin n2) (St n2 p (IntSet.singleton n2) om)

-- | Match the start of the input.
start :: NFTBuilder a b
start = T $ const (specialT Start)

-- | Match the end of the input.
end :: NFTBuilder a b
end = T $ const (specialT End)

-- | Execute a transducer zero or more times.
many :: NFTBuilder a b -> NFTBuilder a b
many (T t) =
  T $ \inp (St n1 p1 fin1 om1) k -> let
    n2 = n1 + 1
    in t inp (St n2 p1 (IntSet.singleton n2) om1) $ \edge st@(St _ _ fin2 _) ->
       k (inputEdge Epsilon fin1 n2 Seq.><
          inputEdge Epsilon fin2 n2 Seq.>< edge)
         st { stFinal = IntSet.singleton n2 }

-- | Execute a transducer zero or one times.
optional :: NFTBuilder a b -> NFTBuilder a b
optional (T t) =
  T $ \inp (St n1 p1 fin1 om) k -> let
    n2 = n1 + 1
    in t inp (St n2 p1 (IntSet.singleton n1) om) $ \edge st@(St _ _ fin2 _) ->
       k (inputEdge Epsilon fin1 n2 Seq.>< edge)
         st { stFinal = IntSet.insert n1 fin2 }

-- | Convert a function into a transducer.
--
-- The resulting transducer applies that function to each input and produces
-- an output for each result.
liftT :: Ord b => (a -> b) -> NFTBuilder a b
liftT f =
  T $ \inp (St n1 p1 fin1 om) k -> let
    n2 = n1 + 1
    p2 = p1 + 1
    (io, om') = iof (alphaAssocs inp) [] om where
      iof ((i, x) : ii) acc om = let
        y = f x
        in case Map.lookup y om of
          Just j -> iof ii ((i, j) : acc) om
          Nothing -> let
            j = Map.size om + 1
            in iof ii ((i, j) : acc) (Map.insert y j om)
      iof _ acc om = (acc, om)
  in k (inputEdge Epsilon fin1 n2 Seq.><
        Seq.fromList [Edge n2 n2 (Input i) (Just (Output p2 j)) | (i, j) <- io])
       (St n2 p2 (IntSet.singleton n2) om')

-- | Allow gaps in the input in a transducer.
--
-- Produces a transducer with the same functionality as the input transducer,
-- except that a 'Nothing' input is ignored.
maybeInput :: Ord a => NFTBuilder a b -> NFTBuilder (Maybe a) b
maybeInput (T t) =
  T $ \inp st1@(St n1 _ _ _) k -> let
    inp' = assocAlphabet . mapMaybe inf . alphaAssocs $ inp
    inf (i, Nothing) = Nothing
    inf (i, Just x) = Just (i, x)
  in t inp' st1 $ case alphaIndex inp Nothing of
    Nothing -> k
    Just i -> \edge st2@(St n2 _ _ _) ->
      k (Seq.fromList [Edge n n (Input i) Nothing | n <- [n1 + 1..n2]]) st2

infixr 6 ~>
infixr 5 <+>
infixr 4 >>>

-- | Concatenate two transducers so one runs after the other.
--
-- Outputs from the left transducer override outputs from the right one.
(~>) :: NFTBuilder a b -> NFTBuilder a b -> NFTBuilder a b
T ta ~> T tb =
  T $ \inp st1 k ->
  ta inp st1 $ \edgea st2 ->
  tb inp st2 $ \edgeb st3 ->
  k (edgea Seq.>< edgeb) st3

-- | Run two transducers in parallel.
--
-- Outputs from the left transducer override outputs from the right one.
(<+>) :: NFTBuilder a b -> NFTBuilder a b -> NFTBuilder a b
T ta <+> T tb =
  T $ \inp st1@(St _ _ fin1 _) k ->
  ta inp st1 $ \edgea st2@(St _ _ fin2 _) ->
  tb inp st2 { stFinal = fin1 } $ \edgeb st3@(St _ _ fin3 _) ->
  k (edgea Seq.>< edgeb) st3 { stFinal = IntSet.union fin2 fin3 }

-- | Feed the output of one transducer into another transducer.
(>>>) :: NFTBuilder a b -> NFTBuilder b c -> NFTBuilder a c
(>>>) = undefined

-- | Create a transducer which matches a rule anywhere in the input.
rule :: Ord b => String -> NFTBuilder a b -> NFTBuilder a (String, b)
rule label pat = many readAny ~> (mapBuilder ((,) label) pat) ~> many readAny

-- | Apply a function to the output of a transducer.
mapBuilder :: forall b1 b2 a. (Ord b1, Ord b2) => (b1 -> b2) ->
             NFTBuilder a b1 -> NFTBuilder a b2
mapBuilder f (T t) =
  T $ \inp st1@(St n1 p1 f1 om1) k ->
  t inp (St n1 p1 f1 Map.empty) $ \edge (St n2 p2 f2 om2) -> let
    g :: [(b1, Int)] -> Map b2 Int -> [(Int, Int)] -> (Map b2 Int, Vector Int)
    g ((x, i) : ii) om acc = let
      y = f x
      in case Map.lookup y om of
        Just j -> g ii om ((i, j) : acc)
        Nothing -> let
          j = Map.size om + 1
          in g ii (Map.insert y j om) ((i, j) : acc)
    g _ om acc = (om, if null acc
                      then Vec.empty
                      else Vec.replicate (maximum (map fst acc) + 1) 0 // acc)
    om3 :: Map b2 Int
    omap :: Vector Int
    (om3, omap) = g (Map.toList om2) om1 []
    efix e@(Edge s d i o) = case o of
      Nothing -> e
      Just (Output op ov) -> Edge s d i (Just (Output op (omap ! ov)))
    in k (fmap efix edge) (St n2 p2 f2 om3)
