# Copyright 2015-2016 Dietrich Epp.
# This file is part of the Moria Gamedev Repository.  This file is licensed
# under the terms of the MIT license.  For more information, see LICENSE.txt.
import array
import collections
import numpy
import struct
import sys
from gamedev.pyutil.version import Version
from .ucd import UnicodeDatabase
from .hiertable import HierarchicalTable
from .util import print_array

Rule = collections.namedtuple(
    'Rule', 'left_direct left_indirect right can_break')

class PairRules(object):
    """Line breaking pair rule set."""
    __slots__ = [
        'types',
        'type_set',
        'type_index',
        'table',
    ]

    def __init__(self, types):
        self.types = types
        self.type_set = set(types)
        self.type_index = {k: n for n, k in enumerate(types)}
        self.table = [[[None, None] for _ in types] for _ in types]

    @classmethod
    def create(class_, types, version):
        r = class_(types)
        v8_0 = version >= Version(8, 0)
        v9_0 = version >= Version(9, 0)

        # Rules 6-31
        r.add('LB6', 'x (BK CR LF NL)')
        r.add('LB7', 'x (SP)', 'x ZW')
        r.add('LB8', 'ZW SP* -')
        r.add('LB11', 'x WJ', 'WJ x')
        r.add('LB12', 'GL x')
        r.add('LB12a', '(^ SP BA HY) x GL')
        r.add('LB13', 'x CL', 'x CP', 'x EX', 'x IS', 'x SY')
        r.add('LB14', 'OP SP* x')
        r.add('LB15', 'QU SP* x OP')
        r.add('LB16', '(CL CP) SP* x NS')
        r.add('LB17', 'B2 SP* x B2')
        r.add('LB18', 'SP -')
        r.add('LB19', 'x QU', 'QU x')
        r.add('LB20', '- CB', 'CB -')
        r.add('LB21', 'x BA', 'x HY', 'x NS', 'BB x')
        # r.add('LB21a', 'HL (HY BA) x')
        if v8_0:
            r.add('LB21', 'SY x HL')
        r.add('LB22', '(AL HL) x IN', 'ID x IN', 'IN x IN', 'NU x IN')
        if v8_0:
            r.add('LB22', 'EX x IN')
        r.add('LB23', 'ID x PO', '(AL HL) x NU', 'NU x (AL HL)')
        r.add('LB24', 'PR x ID', 'PR x (AL HL)', 'PO x (AL HL)')
        r.add('LB25',
            'CL x PO', 'CP x PO', 'CL x PR', 'CP x PR', 'NU x PO',
            'NU x PR', 'PO x OP', 'PO x NU', 'PR x OP', 'PR x NU',
            'HY x NU', 'IS x NU', 'NU x NU', 'SY x NU')
        r.add('LB26', 'JL x (JL JV H2 H3)', '(JV H2) x (JV JT)', '(JT H3) x JT')
        r.add('LB27',
            '(JL JV JT H2 H3) x IN',
            '(JL JV JT H2 H3) x PO',
            'PR x (JL JV JT H2 H3)')
        r.add('LB28', '(AL HL) x (AL HL)')
        r.add('LB29', 'IS x (AL HL)')
        r.add('LB30', '(AL HL NU) x OP', 'CP x (AL HL NU)')
        r.add('LB30a', 'RI x RI')
        r.add('LB31', 'ALL -', '- ALL')
        
        # In the absence of appropriate context information, they are treated
        # as class AL
        r.assign('AI', 'AL')

        # The default behavior for this class is identical to class AL
        r.assign('XX', 'AL')

        # If such analysis is not available, it is recommended to treat them
        # as AL.
        r.assign('SA', 'AL')

        # Characters of this class may be treated as either NS or ID.
        #
        # (Test data assumes NS.)
        r.assign('CJ', 'NS')
        
        return r

    def parse_rule(self, rule):
        """Parse a line breaking rule.
    
        The rules are described in UAX #14. Each rule consists of context and a
        breaking marker, either `x` for prohibited or `-` for permitted. The
        context is a set of line breaking classes to the left, or a set of
        classes to the right, or one set for each. Line breaking classes can
        either be individual classes such as `GL` or parenthesized groups like
        `(JV H2)`, meaning that the character to the left of the analysis point
        can be in any of the classes in the group. If the group begins with
        `^`, as in `(^ SP BA HY)`, then the character can be in any class not
        in the group. The token `ALL` means any character. As a special case,
        `SP*` indicates a sequence of zero or more spaces, and must appear
        directly after the left context.
    
        This is much more clear if you read UAX #14. Also note that this is
        only used for rules 6 and later. Rules 1-5 are handled elsewhere.
        """
        xrule = rule
        pos = 0
        left_direct = None
        left_indirect = None
        can_break = None
        right = None
        while rule:
            assert pos < 4
            if rule[0] == '(':
                i = rule.index(')')
                part = rule[1:i]
                rule = rule[i+1:].lstrip()
                part = part.split()
                if part[0] == '^':
                    part = set(part[1:])
                    assert part <= self.type_set
                    part = list(self.type_set.difference(part))
                if pos == 0:
                    left_direct = part
                    pos = 1
                else:
                    right = part
                    pos = 4
                continue
            i = rule.find(' ')
            if i < 0:
                part = rule
                rule = None
            else:
                part = rule[:i]
                rule = rule[i+1:].lstrip()
            if part == 'SP':
                assert pos == 0
                left_direct = ['SP']
                left_indirect = self.types
                pos = 2
            elif part == 'SP*':
                assert left_direct
                assert pos == 1
                left_indirect = left_direct
                pos = 2
            elif part == 'x':
                assert pos <= 2
                pos = 3
                can_break = False
            elif part == '-':
                assert pos <= 2
                pos = 3
                can_break = True
            elif part == 'ALL':
                if pos == 0:
                    left_direct = self.types
                    left_indirect = self.types
                    pos = 1
                else:
                    right = self.types
                    pos = 3
            else:
                assert part in self.type_set
                if pos == 0:
                    left_direct = [part]
                    pos = 1
                else:
                    right = [part]
                    pos = 3
        assert can_break is not None
        if not left_direct and not left_indirect:
            left_direct = self.types
            left_indirect = self.types
        if not right:
            right = self.types
        return Rule(left_direct or [], left_indirect or [], right, can_break)

    def add(self, name, *rules):
        """Add rules to the pair table.
        
        The rules are as explained in UAX #14.  See parse_rule() for an
        explanation of the syntax.
        """
        for rule in rules:
            rule = self.parse_rule(rule)
            for right in rule.right:
                right_idx = self.type_index[right]
                for left in rule.left_direct:
                    left_idx = self.type_index[left]
                    cell = self.table[left_idx][right_idx]
                    if cell[0] is None:
                        cell[0] = rule.can_break
                for left in rule.left_indirect:
                    left_idx = self.type_index[left]
                    cell = self.table[left_idx][right_idx]
                    if cell[1] is None:
                        cell[1] = rule.can_break

    def assign(self, target, src):
        """Treat a character class as if it were a different class.
        
        This does not override any previous rules for the character class.  For
        example, `assign('AI', 'AL')` causes characters in the AI class to be
        treated as AL, assuming no other rules apply to it.
        """
        tidx = self.type_index[target]
        sidx = self.type_index[src]
        n = len(self.types)
        a = [self.table[i][sidx] for i in range(n)]
        b = [self.table[sidx][i] for i in range(n)]
        c = self.table[sidx][sidx]
        for i in range(n):
            self.table[i][tidx] = list(a[i])
            self.table[tidx][i] = list(b[i])
        self.table[sidx][tidx] = list(c)
        self.table[tidx][sidx] = list(c)
        self.table[tidx][tidx] = list(c)

    def dump_debug(self, file):
        """Dump the class pair table for debugging."""
        print('  ', ' '.join(self.types), file=file)
        def dcell(cell):
            if cell[0] is None or cell[1] is None:
                return '  '
            if cell[0]:
                return ' _'
            if cell[1]:
                return ' %'
            return ' ^'
        for t, row in zip(self.types, self.table):
            print(t, ' '.join(dcell(c) for c in row), file=file)

    def compile(self):
        """Compile the rule set to a table."""
        # Flatten cells to three values:
        # 0 = prohibited (^ in UAX 14)
        # 1 = indirect   (% in UAX 14)
        # 2 = direct     (_ in UAX 14)
        ntable = []
        for row in self.table:
            nrow = []
            ntable.append(nrow)
            for i in range(len(row)):
                cell = row[i]
                assert cell[0] is not None and cell[1] is not None
                if not cell[1]:
                    assert not cell[0]
                    cell = 0
                elif not cell[0]:
                    cell = 1
                else:
                    cell = 2
                nrow.append(cell)

        # Compute equivalence classes
        n = len(self.types)
        equiv_map = [None] * n
        # We don't care about CM, because it's handled specially
        equiv_map[self.type_index['CM']] = 0
        equiv_reps = []
        for i in range(n):
            if equiv_map[i] is not None:
                continue
            equiv_map[i] = len(equiv_reps)
            for j in range(i+1, n):
                if equiv_map[j] is not None:
                    continue
                if any(ntable[i][k] != ntable[j][k] or
                       ntable[k][i] != ntable[k][j]
                       for k in range(n)):
                    continue
                equiv_map[j] = len(equiv_reps)
            equiv_reps.append(i)

        # Compute output tables
        table = []
        for i, ii in enumerate(equiv_reps):
            sp0 = 0
            sp1 = 0
            for j, jj in enumerate(equiv_reps):
                cell = ntable[ii][jj]
                m = 1 << j
                if cell >= 1:
                    sp1 |= m
                if cell == 2:
                    sp0 |= m
            table.append((sp0, sp1))
        
        return PairTable(equiv_map, table)

class PairTable(object):
    """Line breaking pair rules compiled into a table."""
    __slots__ = ['equiv_map', 'table']
    
    def __init__(self, equiv_map, table):
        self.equiv_map = equiv_map
        self.table = table
    
    def dump_c(self, *, file):
        file.write(
            '\n'
            '// Map from line break class to equivalency class in table\n'
            'static const unsigned char kUcdLinebreakEquiv[{}] = {{\n'
            .format(len(self.equiv_map)))
        print_array((str(x) for x in self.equiv_map), file=file)
        file.write('};\n')

        file.write(
            '\n'
            '// Map from equvalency pairs to break info\n'
            'static const unsigned kUcdLinebreakPair[{}][2] = {{\n'
            .format(len(self.table)))
        print_array(
            ('{{0x{0[0]:08x}, 0x{0[1]:08x}}}'.format(x) for x in self.table),
            per_line=2, file=file)
        file.write('};\n')

class LineBreakTable(object):
    __slots__ = [
        'version',
        'types',
        'ctable',
        'pairs',
        'test_data',
    ]

    def __init__(self, version, types, ctable, pairs, test_data):
        self.version = version
        self.types = types
        self.ctable = ctable
        self.pairs = pairs
        self.test_data = test_data

    @classmethod
    def create(class_, ucd):
        version = ucd.version()
        types = sorted(ucd.property_info('lb').values.keys())
        if False:
            types.insert(0, types.pop(types.index('XX')))
        type_index = {k: n for n, k in enumerate(types)}
        ctypes = ucd.property_data('LineBreak.txt', type_index, numpy.uint8)
        cats = {cat: 0 for cat in ucd.property_info('gc').values.keys()}
        cats['Zs'] = 0x80
        ctypes |= ucd.main_column(2, cats, numpy.uint8)
        ctypes[[0x000a, 0x000c, 0x000d, 0x0085, 0x2028, 0x2029]] |= 0x80
        ctable = HierarchicalTable.create(ctypes, max_layers=3)
        pairs = PairRules.create(types, version).compile()
        test_data = class_.compile_test_data(ucd)
        return class_(version, types, ctable, pairs, test_data)
        
    def dump(self, *, file):
        ctable = self.ctable.format_c(
            struct_name='ucd_linebreak_data',
            table_name='kUcdLinebreakData',
            function_name='ucd_linebreak_class',
            output_type='unsigned',
        )
        
        file.write(
            '// Automatically generated.\n'
            '// Unicode version: {}\n'
            .format(self.version))

        # Write output
        file.write(
            '\n'
            '// Line break classes\n'
            'enum {\n')
        print_array(('kUcdLinebreak' + t for t in self.types), file=file)
        file.write('};\n')
        
        self.pairs.dump_c(file=file)
        
        file.write(
            '\n'
            '// Line break class character table\n')
        ctable.define_struct(file=file)
        ctable.declare_table(qualifiers='static const', file=file)

        file.write(
            '\n'
            '// Get the line breaking class for a code point.\n')
        ctable.define_function(qualifiers='static', file=file)

        file.write(
            '\n'
            '// Line break class character table data\n')
        ctable.define_table(qualifiers='static const', file=file)

    @classmethod
    def compile_test_data(class_, ucd):
        """Extract the Unicode line break test data."""
        CAN_BREAK = 1
        MUST_BREAK = 2
        PARAGRAPH = 4
        WHITE = 8
        EXPANDABLE = 16
        cats = {cat: 0 for cat in ucd.property_info('gc').values.keys()}
        cats['Zp'] = WHITE | MUST_BREAK | PARAGRAPH
        cats['Zl'] = WHITE | MUST_BREAK
        cats['Zs'] = WHITE
        cattr = ucd.main_column(2, cats, numpy.uint8)
        cattr[[0x000a, 0x000b, 0x000c, 0x000d, 0x0085]] = WHITE | MUST_BREAK
        cattr[[0x0020, 0x00a0]] = WHITE | EXPANDABLE
        breaktype = {'×': 0, '÷': CAN_BREAK}
        arr = array.array('I')
        with ucd.open('auxiliary/LineBreakTest.txt') as fp:
            for lineno, line in enumerate(fp, 1):
                if '[999.0]' in line:
                    # Ignore "customized" line breaking rules
                    continue
                i = line.find('#')
                if i >= 0:
                    line = line[:i]
                line = line.strip()
                if not line:
                    continue
                line = line.split()
                text = [int(x, 16) for x in line[1::2]]
                tbreaks = [breaktype[x] for x in line[2::2]]
                assert len(text) >= 2
                assert len(text) == len(tbreaks)
                for n, c in enumerate(text):
                    tbreaks[n] |= cattr[c]
                if 0x000d in text:
                    cc = 0
                    for n, c in enumerate(text):
                        if c == 0x000a and cc == 0x000d:
                            tbreaks[n-1] = WHITE;
                        cc = c
                tbreaks[-1] |= CAN_BREAK | MUST_BREAK | PARAGRAPH;
                arr.append(lineno)
                arr.append(len(text))
                arr.extend(text)
                arr.extend(tbreaks)
        return arr

def run():
    from os.path import abspath, dirname, join
    if len(sys.argv) != 2:
        sys.exit(1)
    root = dirname(abspath(__file__))
    ucd = UnicodeDatabase(sys.argv[1])

    lb = LineBreakTable.create(ucd)
    with open(join(root, 'linebreak_data.h'), 'w') as fp:
        lb.dump(file=fp)
    with open(join(root, 'linebreak_testdata.dat'), 'wb') as fp:
        lb.test_data.tofile(fp)

if __name__ == '__main__':
    run()
